# Order API

![Quality Gate](https://coringa.lojaskd.com.br/api/badges/gate?key=order-api&blinking=true)
![CE Activity](https://coringa.lojaskd.com.br/api/badges/ce_activity?key=order-api)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=lines)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=ncloc)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=comment_lines_density)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=function_complexity)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=test_errors)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=test_failures)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=skipped_tests)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=test_success_density)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=coverage)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_coverage)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=duplicated_lines_density)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_duplicated_lines_density)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=blocker_violations)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=critical_violations)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_blocker_violations)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_critical_violations)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=code_smells)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_code_smells)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=bugs)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_bugs)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=vulnerabilities)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_vulnerabilities)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=sqale_debt_ratio)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_sqale_debt_ratio)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_maintainability_rating)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_reliability_rating)
![Measure](https://coringa.lojaskd.com.br/api/badges/measure?key=order-api&metric=new_security_rating)

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
- [Docker](https://www.docker.com) at last version (Docker-ce 17.x)
- [Docker Compose](https://docs.docker.com/compose/) at least version (>1.16), for running containers locally
- [Start and running any LojasKD application](http://wiki.lojaskd.com.br/wiki/index.php/REPortalE_-_First_things_firt)

## Starting
```bash 
make up 
```
Get the order-api ip that will be printed and access on the browser
