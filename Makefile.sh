ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
$(eval $(ARGS):;@:)

MAKEFLAGS += --silent
DOCKER_COMPOSE = docker-compose -f docker/docker-compose.override.yml

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

#############################
# Docker machine states
#############################
dc:
	$(DOCKER_COMPOSE) $(ARGS)

up:

	cat docker/environments/$(ARGS).dist > .env
	chmod -R 777 var/*
	$(DOCKER_COMPOSE) up -d
	$(DOCKER_COMPOSE) exec -u $$(id -u):$$(id -g) order-api-php composer install
	chmod -R 777 var/*
	echo ------------------------------------------
	make ips

rebuild:
	$(DOCKER_COMPOSE) stop
	$(DOCKER_COMPOSE) build --no-cache --pull
	$(DOCKER_COMPOSE) up -d --force-recreate

logs:
	$(DOCKER_COMPOSE) logs --follow $(ARGS)

shell:
	$(DOCKER_COMPOSE) exec -u $$(id -u):$$(id -g) $(ARGS) /bin/sh

bash: shell

ips:
	$(DOCKER_COMPOSE) ps | sed -e '/Name/,+1d' | awk '{print $$1}' | while read f; do echo "$$f -> "$$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $$f); done


#############################
# Argument fix workaround
#############################
%:
@:
