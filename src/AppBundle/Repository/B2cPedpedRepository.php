<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;
use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use AppBundle\Entity\VO\Create\CreateShippingVO;
use AppBundle\Entity\VO\OrderVO;

/**
 * Class B2cPedpedRepository
 *
 * @package AppBundle\Repository
 */
class B2cPedpedRepository extends EntityRepository
{

    /**
     * @param $orderId
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get order
     */
    public function getOrder($orderId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('ped', 'prd', 'status')
            ->from('AppBundle:B2cPedped', 'ped')
            ->leftJoin('ped.products', 'prd')
            ->leftJoin('ped.status', 'status')
            ->where('ped.idPedped = :id_pedido')
            ->setParameter('id_pedido', $orderId)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param $limit
     *
     * @return array Get orders
     *
     * Get orders
     * @internal param $orderBy
     */
    public function getOrders($limit)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('ped.idPedped', 'ped.idClicli', 'ped.entNome', 'ped.entSobrenome', 'pedsit.name as situacao')
            ->from('AppBundle:B2cPedped', 'ped')
            ->join('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->orderBy('ped.pedDtaCompra', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param $orderId
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get marketplace order
     */
    public function getMarketPlaceOrder($orderId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('pedmkp.codOrder', 'mkpchannel.nome', 'mkpchannel.slug')
            ->from('AppBundle:B2cPedped', 'ped')
            ->innerJoin('AppBundle:B2cPedpedMarketplace', 'pedmkp', 'WITH', 'ped.idPedped = pedmkp.idPedped')
            ->innerJoin('AppBundle:B2cMarketplaceChannel', 'mkpchannel', 'WITH', 'pedmkp.idChannel = mkpchannel.id')
            ->where('ped.idPedped = :orderId')
            ->setParameter('orderId', $orderId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param $order
     * @param $marketPlaceInfo
     *
     * @return array
     *
     * Format order info
     */
    public function formatOrderInfo($order, $marketPlaceInfo = array())
    {
        $orderInfo = array();

        $orderInfo['id_pedped'] = $order['idPedped'];
        $orderInfo['ped_dta_compra'] = ($order['pedDtaCompra'] ? $order['pedDtaCompra']->format('Y-m-d H:i:s') : null);
        $orderInfo['ped_dta_atualizacao'] =
            ($order['pedDtaAtualizacao'] ? $order['pedDtaAtualizacao']->format('Y-m-d H:i:s') : null);
        $orderInfo['atendente'] = (isset($order['atendente']) ? $order['atendente'] : null);
        $orderInfo['ped_situacao'] = intval($order['pedSituacao']);
        $orderInfo['ped_situacao_desc'] = isset($order['situationName']) ? $order['situationName'] : '';
        $orderInfo['ped_staint_desc'] = isset($order['internalStatusName']) ? $order['internalStatusName'] : '';
        $orderInfo['ped_staint'] = ($order['pedStaint'] ? $order['pedStaint'] : 0);
        $orderInfo['ped_pend_canc'] = $order['pedPendCanc'];
        $orderInfo['id_clicli'] = $order['idClicli'];
        $orderInfo['cob_nome'] = $order['cobNome'];
        $orderInfo['cob_sobrenome'] = $order['cobSobrenome'];
        $orderInfo['cob_email'] = $order['cobEmail'];
        $orderInfo['cob_cpf_cgc'] = $order['cobCpfCgc'];
        $orderInfo['cob_rg_ie'] = $order['cobRgIe'];
        $orderInfo['cob_tipo_pessoa'] = $order['cobTipoPessoa'];
        $orderInfo['cob_sexo'] = $order['cobSexo'];
        $orderInfo['cob_dta_nasc'] = ($order['cobDtaNasc'] ? $order['cobDtaNasc']->format('Y-m-d') : null);
        $orderInfo['cob_contato'] = $order['cobContato'];
        $orderInfo['cob_endereco'] = $order['cobEndereco'];
        $orderInfo['cob_end_numero'] = $order['cobEndNumero'];
        $orderInfo['cob_end_comp'] = $order['cobEndComp'];
        $orderInfo['cob_end_bairro'] = $order['cobEndBairro'];
        $orderInfo['cob_end_cidade'] = $order['cobEndCidade'];
        $orderInfo['cob_end_uf'] = $order['cobEndUf'];
        $orderInfo['cob_end_cep'] = $order['cobEndCep'];
        $orderInfo['cob_end_provincia'] = $order['cobEndProvincia'];
        $orderInfo['cob_end_pais'] = $order['cobEndPais'];
        $orderInfo['cob_tel_a'] = $order['cobTelA'];
        $orderInfo['cob_tel_b'] = $order['cobTelB'];
        $orderInfo['ent_nome'] = $order['entNome'];
        $orderInfo['ent_sobrenome'] = $order['entSobrenome'];
        $orderInfo['ent_identificacao'] = $order['entIdentificacao'];
        $orderInfo['ent_endereco'] = $order['entEndereco'];
        $orderInfo['ent_end_numero'] = $order['entEndNumero'];
        $orderInfo['ent_end_comp'] = $order['entEndComp'];
        $orderInfo['ent_end_bairro'] = $order['entEndBairro'];
        $orderInfo['ent_end_cidade'] = $order['entEndCidade'];
        $orderInfo['ent_cod_ibge'] = $order['entCodIbge'];
        $orderInfo['ent_end_uf'] = $order['entEndUf'];
        $orderInfo['ent_end_cep'] = $order['entEndCep'];
        $orderInfo['ent_end_ref'] = $order['entEndRef'];
        $orderInfo['ent_end_provincia'] = $order['entEndProvincia'];
        $orderInfo['ent_end_pais'] = $order['entEndPais'];
        $orderInfo['ent_tel_a'] = $order['entTelA'];
        $orderInfo['ent_tel_b'] = $order['entTelB'];
        $orderInfo['ped_observacao'] = $order['pedObservacao'];
        $orderInfo['ped_tipo_frete'] = $order['pedTipoFrete'];
        $orderInfo['ped_transportadora'] = $order['pedTransportadora'];
        $orderInfo['ped_prazo_entrega'] = $order['pedPrazoEntrega'];
        $orderInfo['ped_prazo_transporte'] = $order['pedPrazoTransporte'];
        $orderInfo['ped_data_entrega'] = ($order['pedDataEntrega'] ? $order['pedDataEntrega']->format('Y-m-d') : null);
        $orderInfo['ped_entrega_regiao'] = $order['pedEntregaRegiao'];
        $orderInfo['ped_cod_envio'] = $order['pedCodEnvio'];
        $orderInfo['ped_montagem'] = (boolean)$order['pedMontagem'];
        $orderInfo['ped_valor_montagem'] = $order['pedValorMontagem'];
        $orderInfo['ped_valor_subtotal'] = $order['pedValorSubtotal'];
        $orderInfo['ped_valor_embalagem'] = $order['pedValorEmbalagem'];
        $orderInfo['ped_valor_custofrete'] = $order['pedValorCustofrete'];
        $orderInfo['ped_valor_frete'] = $order['pedValorFrete'];
        $orderInfo['ped_valor_desconto'] = $order['pedValorDesconto'];
        $orderInfo['ped_valor_desconto_loja'] = $order['pedValorDescontoLoja'];
        $orderInfo['ped_valor_valecompra'] = $order['pedValorValecompra'];
        $orderInfo['ped_valor_total'] = $order['pedValorTotal'];
        $orderInfo['id_pagpag_1'] = $order['idPagpag1'];
        $orderInfo['id_pagfor_1'] = $order['idPagfor1'];
        $orderInfo['ped_valor_total_1'] = $order['pedValorTotal1'];
        $orderInfo['ped_valor_real_1'] = $order['pedValorReal1'];
        $orderInfo['pag_num_parcelas_1'] = $order['pagNumParcelas1'];
        $orderInfo['pag_val_entrada_1'] = $order['pagValEntrada1'];
        $orderInfo['pag_val_parcela_1'] = $order['pagValParcela1'];
        $orderInfo['pag_indexador_1'] = $order['pagIndexador1'];
        $orderInfo['boleto_parcelado_1'] = $order['boletoParcelado1'];
        $orderInfo['boleto_vencimento_1'] =
            ($order['boletoVencimento1'] ? $order['boletoVencimento1']->format('Y-m-d') : null);
        $orderInfo['ped_tipo_pagamento_1'] = $order['pedTipoPagamento1'];
        $orderInfo['ped_forma_pagamento_1'] = $order['pedFormaPagamento1'];
        $orderInfo['ped_forma_pagdesc_1'] = $order['pedFormaPagdesc1'];
        $orderInfo['ped_valor_desconto_loja_1'] = $order['pedValorDescontoLoja1'];
        $orderInfo['contador_alteracao_desconto_loja_1'] =
            ($order['contadorAlteracaoDescontoLoja1'] ? $order['contadorAlteracaoDescontoLoja1'] : 0);
        $orderInfo['pagamento_confirmado_1'] = (boolean)$order['pagamentoConfirmado1'];
        $orderInfo['id_pagpag_2'] = $order['idPagpag2'];
        $orderInfo['id_pagfor_2'] = $order['idPagfor2'];
        $orderInfo['ped_valor_total_2'] = $order['pedValorTotal2'];
        $orderInfo['ped_valor_real_2'] = $order['pedValorReal2'];
        $orderInfo['pag_num_parcelas_2'] = $order['pagNumParcelas2'];
        $orderInfo['pag_val_entrada_2'] = $order['pagValEntrada2'];
        $orderInfo['pag_val_parcela_2'] = $order['pagValParcela2'];
        $orderInfo['pag_indexador_2'] = $order['pagIndexador2'];
        $orderInfo['boleto_parcelado_2'] = $order['boletoParcelado2'];
        $orderInfo['boleto_vencimento_2'] =
            ($order['boletoVencimento2'] ? $order['boletoVencimento2']->format('Y-m-d') : null);
        $orderInfo['ped_tipo_pagamento_2'] = $order['pedTipoPagamento2'];
        $orderInfo['ped_forma_pagamento_2'] = $order['pedFormaPagamento2'];
        $orderInfo['ped_forma_pagdesc_2'] = $order['pedFormaPagdesc2'];
        $orderInfo['ped_valor_desconto_loja_2'] = $order['pedValorDescontoLoja2'];
        $orderInfo['contador_alteracao_desconto_loja_2'] =
            ($order['contadorAlteracaoDescontoLoja2'] ? $order['contadorAlteracaoDescontoLoja2'] : 0);
        $orderInfo['pagamento_confirmado_2'] = (boolean)$order['pagamentoConfirmado2'];
        $orderInfo['ped_origem'] = $order['pedOrigem'];
        $orderInfo['ped_primeira_origem'] = $order['pedPrimeiraOrigem'];
        $orderInfo['ped_primeira_origem_data'] =
            ($order['pedPrimeiraOrigemData'] ? $order['pedPrimeiraOrigemData'] : null);
        $orderInfo['ped_referer'] = $order['pedReferer'];
        $orderInfo['ped_cupom'] = $order['pedCupom'];
        $orderInfo['ped_promocao'] = $order['pedPromocao'];
        $orderInfo['ip'] = $order['ip'];
        $orderInfo['cs_usuario'] = $order['csUsuario'];
        $orderInfo['cs_status'] = $order['csStatus'];
        $orderInfo['cs_score'] = $order['csScore'];
        $orderInfo['cs_diagnostico'] = $order['csDiagnostico'];
        $orderInfo['cs_solicitacao'] = $order['csSolicitacao'];
        $orderInfo['SeqPed'] = intval($order['seqped']);
        $orderInfo['exportado'] = $order['exportado'];
        $orderInfo['ped_transportadora_integrado'] =
            ($order['pedTransportadoraIntegrado'] ? $order['pedTransportadoraIntegrado'] : 0);
        $orderInfo['ped_valor_comissao'] = $order['pedValorComissao'];
        $orderInfo['ped_data_afiliado'] = $order['pedDataAfiliado'];
        $orderInfo['ped_afiliado'] = $order['pedAfiliado'];
        $orderInfo['ordem_geracao_boleto'] = $order['ordemGeracaoBoleto'];
        $orderInfo['analytics_negativado'] = (boolean)$order['analyticsNegativado'];
        $orderInfo['ped_entrega_data'] = ($order['pedEntregaData'] ? $order['pedEntregaData']->format('Y-m-d') : null);
        $orderInfo['ped_faturado'] = $order['pedFaturado'];
        $orderInfo['pag_tel_1'] = $order['pagTel1'];
        $orderInfo['pag_tel_2'] = $order['pagTel2'];
        $orderInfo['pag_titular_1'] = $order['pagTitular1'];
        $orderInfo['pag_titular_2'] = $order['pagTitular2'];
        $orderInfo['conta_contabil_1'] = $order['contaContabil1'];
        $orderInfo['conta_contabil_2'] = $order['contaContabil2'];
        $orderInfo['conta_contabil_nome_1'] = $order['contaContabilNome1'];
        $orderInfo['conta_contabil_nome_2'] = $order['contaContabilNome2'];
        $orderInfo['sf_opportunity'] = $order['sfOpportunity'];
        $orderInfo['sf_data_sync'] = ($order['sfDataSync'] ? $order['sfDataSync']->format('Y-m-d H:i:s') : null);
        $orderInfo['sf_custo_financeiro_sync'] = (boolean)$order['sfCustoFinanceiroSync'];
        $orderInfo['assistencia'] = (boolean)$order['assistencia'];
        $orderInfo['recompra'] = (boolean)$order['recompra'];
        $orderInfo['troca'] = (boolean)$order['troca'];
        $orderInfo['ped_nova_data_entrega'] = $order['pedNovaDataEntrega'];

        $orderInfo['tipo_pedido'] = 'Pedido';
        if ($order['tipoPedido'] == 'marketplace') {
            $orderInfo['tipo_pedido'] = 'MarketPlace';
        } elseif (strpos($order['pedCupom'], 'FUNC') || strpos($order['pedCupom'], 'DESC')) {
            $orderInfo['tipo_pedido'] = 'Funcionário';
        } elseif ($orderInfo['assistencia']) {
            $orderInfo['tipo_pedido'] = 'Assistência';
        } elseif ($orderInfo['recompra']) {
            $orderInfo['tipo_pedido'] = 'Recompra';
        } elseif ($orderInfo['troca']) {
            $orderInfo['tipo_pedido'] = 'Troca';
        }

        if (isset($order['status']) && count($order['status']) > 0) {
            foreach ($order['status'] as $status) {
                if ($status['pedSituacao'] == 3 && $status['pedStaint'] == 0) {
                    $orderInfo['previsao_aprovacao'] = $status['pedDtaSituacaoPrev'];
                    $orderInfo['realizada_aprovacao'] = $status['pedDtaSituacao'];
                } elseif ($status['pedSituacao'] == 4 && in_array($status['pedStaint'], array(0, 80, 81))) {
                    $orderInfo['previsao_compra'] = $status['pedDtaSituacaoPrev'];
                    $orderInfo['realizada_compra'] = $status['pedDtaSituacao'];
                } elseif ($status['pedSituacao'] == 4 && $status['pedStaint'] == 72) {
                    $orderInfo['previsao_nf'] = $status['pedDtaSituacaoPrev'];
                    $orderInfo['realizado_nf'] = $status['pedDtaSituacao'];
                } elseif ($status['pedSituacao'] == 4 && $status['pedStaint'] == 86) {
                    $orderInfo['previsao_entrada_digital'] = $status['pedDtaSituacaoPrev'];
                    $orderInfo['realizado_entrada_digital'] = $status['pedDtaSituacao'];
                } elseif ($status['pedSituacao'] == 4 && $status['pedStaint'] == 159) {
                    $orderInfo['previsao_faturamento_industria'] = $status['pedDtaSituacaoPrev'];
                    $orderInfo['realizado_faturamento_industria'] = $status['pedDtaSituacao'];
                } elseif ($status['pedSituacao'] == 4 && $status['pedStaint'] == 158) {
                    $orderInfo['previsao_entrada_fisica'] = $status['pedDtaSituacaoPrev'];
                    $orderInfo['realizado_entrada_fisica'] = $status['pedDtaSituacao'];
                } elseif ($status['pedSituacao'] == 5 && $status['pedStaint'] == 0) {
                    $orderInfo['previsao_embarque'] = $status['pedDtaSituacaoPrev'];
                    $orderInfo['realizado_embarque'] = $status['pedDtaSituacao'];
                } elseif ($status['pedSituacao'] == 6 && $status['pedStaint'] == 0) {
                    $orderInfo['previsao_entrega'] = $status['pedDtaSituacaoPrev'];
                    $orderInfo['realizada_entrega'] = $status['pedDtaSituacao'];
                }
            }
        }

        if (!empty($marketPlaceInfo)) {
            $orderInfo['marketplace_name'] = $marketPlaceInfo['nome'];
            $orderInfo['marketplace_slug'] = $marketPlaceInfo['slug'];
            $orderInfo['marketplace_order_id'] = $marketPlaceInfo['codOrder'];
        }

        return $orderInfo;
    }

    /**
     * @param CreateOrderVO $orderVO
     *
     * @return B2cPedped
     *
     * Create order on b2c_pedped
     */
    public function generateOrder(CreateOrderVO $orderVO)
    {
        try {
            $newOrder = new B2cPedped();
            $newOrder->setPedDtaCompra(new \DateTime('now'));
            $newOrder->setPedDtaAtualizacao(new \DateTime('now'));
            $newOrder->setPedSituacao(1);
            $newOrder->setPedPendCanc(0);
            $newOrder->setPedStaint($orderVO->getStatusInternal() ? $orderVO->getStatusInternal() : 0);
            $newOrder->setIdClicli($orderVO->getCustomer()->getId());
            $newOrder->setCobNome($orderVO->getDeliveryAddress()->getFirstName());
            $newOrder->setCobSobrenome($orderVO->getDeliveryAddress()->getLastname());
            $newOrder->setCobEmail($orderVO->getCustomer()->getEmail());
            $newOrder->setCobCpfCgc($orderVO->getCustomer()->getCpf());
            $newOrder->setCobRgIe($orderVO->getCustomer()->getRg());
            $newOrder->setCobTipoPessoa($orderVO->getCustomer()->getPersonType());
            $newOrder->setCobSexo($orderVO->getCustomer()->getGenre() ? $orderVO->getCustomer()->getGenre() : '');
            $newOrder->setCobDtaNasc(
                $orderVO->getCustomer()->getBirthday() ? $orderVO->getCustomer()->getBirthday() : new \DateTime('now')
            );
            $newOrder->setCobContato($orderVO->getCustomer()->getContact());
            $newOrder->setCobEndereco($orderVO->getBillingAddress()->getAddress());
            $newOrder->setCobEndNumero($orderVO->getBillingAddress()->getNumber());
            $newOrder->setCobEndComp($orderVO->getBillingAddress()->getComplement());
            $newOrder->setCobEndBairro($orderVO->getBillingAddress()->getNeighborhood());
            $newOrder->setCobEndCidade($orderVO->getBillingAddress()->getCity());
            $newOrder->setCobEndUf($orderVO->getBillingAddress()->getState());
            $newOrder->setCobEndCep($orderVO->getBillingAddress()->getZip());
            $newOrder->setCobEndProvincia(null);
            $newOrder->setCobEndPais($orderVO->getBillingAddress()->getCountry());
            $newOrder->setCobTelA($orderVO->getBillingAddress()->getTelephoneA());
            $newOrder->setCobTelB($orderVO->getBillingAddress()->getTelephoneB());
            $newOrder->setEntNome($orderVO->getDeliveryAddress()->getFirstName());
            $newOrder->setEntSobrenome($orderVO->getDeliveryAddress()->getLastname());
            $newOrder->setEntIdentificacao($orderVO->getDeliveryAddress()->getIdentification());
            $newOrder->setEntEndereco($orderVO->getDeliveryAddress()->getAddress());
            $newOrder->setEntEndNumero($orderVO->getDeliveryAddress()->getNumber());
            $newOrder->setEntEndComp($orderVO->getDeliveryAddress()->getComplement());
            $newOrder->setEntEndBairro($orderVO->getDeliveryAddress()->getNeighborhood());
            $newOrder->setEntEndCidade($orderVO->getDeliveryAddress()->getCity());
            $newOrder->setEntEndUf($orderVO->getDeliveryAddress()->getState());
            $newOrder->setEntEndCep($orderVO->getDeliveryAddress()->getZip());
            $newOrder->setEntEndProvincia(null);
            $newOrder->setEntEndPais($orderVO->getDeliveryAddress()->getCountry());
            $newOrder->setEntTelA($orderVO->getDeliveryAddress()->getTelephoneA());
            $newOrder->setEntTelB($orderVO->getDeliveryAddress()->getTelephoneB());
            $newOrder->setEntEndRef($orderVO->getDeliveryAddress()->getReference());
            $newOrder->setEntCodIbge($orderVO->getRegion()->getIbgeCode());
            $newOrder->setPedCodEnvio('');
            $newOrder->setPedObservacao($orderVO->getObservation());
            $newOrder->setPedTipoFrete($orderVO->getShipping()->getShippingType());
            $newOrder->setPedTransportadora($orderVO->getShipping()->getName());

            // Set order dates
            $newOrder->setPedPrazoEntrega($orderVO->getMaxDeliveryDeadline());
            $newOrder->setPedPrazoTransporte($orderVO->getMaxShippingDeadline());
            $newOrder->setPedDataEntrega($orderVO->getMaxDeliveryDate());

            $newOrder->setPedEntregaRegiao($orderVO->getRegion()->getName());
            $newOrder->setPedMontagem($orderVO->getShipping()->isMounting());
            $newOrder->setPedValorMontagem($orderVO->getShipping()->getMountingValue());
            $newOrder->setPedValorSubtotal($orderVO->getSubtotal());
            $newOrder->setPedValorComissao(0);
            $newOrder->setPedValorEmbalagem(0);
            $newOrder->setPedValorFrete($orderVO->getShipping()->getPrice());
            $newOrder->setPedValorDesconto($orderVO->getDiscount());
            $newOrder->setPedValorDescontoLoja(0);
            $newOrder->setPedValorTotal($orderVO->getTotal());
            $newOrder->setIdPagpag1($orderVO->getFirstPayment()->getId());
            $newOrder->setIdPagfor1($orderVO->getFirstPayment()->getPaymentFormId());
            $newOrder->setPedValorTotal1($orderVO->getFirstPayment()->getPaymentTotalAmount());
            $newOrder->setPagNumParcelas1($orderVO->getFirstPayment()->getInstalment());
            $newOrder->setPagValEntrada1(0);
            $newOrder->setPagValParcela1($orderVO->getFirstPayment()->getInstalmentValue());
            $newOrder->setPagIndexador1($orderVO->getFirstPayment()->getIndexer());
            $newOrder->setBoletoParcelado1(0);
            $newOrder->setBoletoVencimento1($orderVO->getFirstPayment()->getDeadline());
            $newOrder->setPedTipoPagamento1($orderVO->getFirstPayment()->getName());
            $newOrder->setPedFormaPagamento1($orderVO->getFirstPayment()->getPaymentFormDesc());
            $newOrder->setPedFormaPagdesc1($orderVO->getFirstPayment()->getDescription());
            $newOrder->setPedValorReal1($orderVO->getFirstPayment()->getPaymentTotalAmount());
            $newOrder->setPedValorDescontoLoja1(0);
            $newOrder->setContadorAlteracaoDescontoLoja1(0);
            $newOrder->setPedValorTotal2(0);
            $newOrder->setPedValorReal2(0);
            $newOrder->setPedValorDescontoLoja2(0);
            $newOrder->setContadorAlteracaoDescontoLoja2(0);
            if ($orderVO->getSecondPayment()) {
                $newOrder->setIdPagpag2($orderVO->getSecondPayment()->getId());
                $newOrder->setIdPagfor2($orderVO->getSecondPayment()->getPaymentFormId());
                $newOrder->setPedValorTotal2($orderVO->getSecondPayment()->getPaymentTotalAmount());
                $newOrder->setPagNumParcelas2($orderVO->getSecondPayment()->getInstalment());
                $newOrder->setPagValEntrada2(0);
                $newOrder->setPagValParcela2($orderVO->getSecondPayment()->getInstalmentValue());
                $newOrder->setPagIndexador2($orderVO->getSecondPayment()->getIndexer());
                $newOrder->setBoletoParcelado2(0);
                $newOrder->setBoletoVencimento2($orderVO->getSecondPayment()->getDeadline());
                $newOrder->setPedTipoPagamento2($orderVO->getSecondPayment()->getName());
                $newOrder->setPedFormaPagamento2($orderVO->getSecondPayment()->getPaymentFormDesc());
                $newOrder->setPedFormaPagdesc2($orderVO->getSecondPayment()->getDescription());
                $newOrder->setPedValorReal2($orderVO->getSecondPayment()->getPaymentTotalAmount());
                $newOrder->setPedValorDescontoLoja2(0);
                $newOrder->setContadorAlteracaoDescontoLoja2(0);
            }
            $newOrder->setPedAfiliado(null);
            $newOrder->setPedDataAfiliado(null);
            $newOrder->setPedOrigem($orderVO->getOrigin()->getSource());
            $newOrder->setPedPrimeiraOrigem('');
            $newOrder->setPedPrimeiraOrigemData('');
            $newOrder->setPedReferer(null);
            if ($orderVO->getCoupon()) {
                $newOrder->setPedValorValecompra($orderVO->getCoupon()->getDiscountValue());
                $newOrder->setPedCupom($orderVO->getCoupon()->getCode());
                $newOrder->setPedPromocao($orderVO->getCoupon()->getPromotionName());
            }
            $newOrder->setIP($orderVO->getCustomer()->getIp());
            $newOrder->setPedTransportadoraIntegrado(0);
            $newOrder->setPedValorCustofrete($orderVO->getShipping()->getCostPrice());
            $newOrder->setAnalyticsNegativado(0);
            $newOrder->setSfCustoFinanceiroSync(0);

            $newOrder->setAssistencia(0);
            $newOrder->setRecompra(0);
            $newOrder->setTroca(0);

            if ($orderVO->getMarketplace()) {
                $newOrder->setIdMarketplace($orderVO->getMarketplace()->getId());
                $newOrder->setPedNovaDataEntrega($orderVO->getMarketplace()->getNewDeliveryDate());
            }
            $newOrder->setTipoPedido($orderVO->getType());
            $newOrder->setDevice($orderVO->getDevice());

            if ($orderVO->getShipping()->getRuleFreight()) {
                $newOrder->setRuleFreight($orderVO->getShipping()->getRuleFreight());
            }
            if ($orderVO->isAssistance()) {
                $newOrder->setAssistencia(1);
            } elseif ($orderVO->isBuyback()) {
                $newOrder->setRecompra(1);
            } elseif ($orderVO->isExchange()) {
                $newOrder->setTroca(1);
            }
        } catch (\Exception $e) {
            throw new Exception('Error to insert order: '.$e->getMessage());
        }

        return $newOrder;
    }

    /**
     * @param $orderId
     * @param $billingAccount
     * @param $billingAccountName
     * @return bool
     *
     * Update order billing account
     */
    public function updateBillingAccount($orderId, $billingAccount, $billingAccountName)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedped', 'ped')
            ->set('ped.contaContabil1', $queryBuilder->expr()->literal($billingAccount))
            ->set('ped.contaContabilNome1', $queryBuilder->expr()->literal($billingAccountName))
            ->where('ped.idPedped = ?1')
            ->setParameter(1, $orderId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $orderId
     * @param $billingAccount
     * @param $billingAccountName
     * @return bool
     *
     * Update order billing account
     */
    public function updateSecondPaymentBillingAccount($orderId, $billingAccount, $billingAccountName)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedped', 'ped')
            ->set('ped.contaContabil2', $queryBuilder->expr()->literal($billingAccount))
            ->set('ped.contaContabilNome2', $queryBuilder->expr()->literal($billingAccountName))
            ->where('ped.idPedped = ?1')
            ->setParameter(1, $orderId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $orderId
     * @param $deliveryDeadline
     * @param $deliveryDate
     * @return bool
     *
     * Update order deadline
     */
    public function updateOrderDeadline($orderId, $deliveryDeadline, $deliveryDate)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedped', 'ped')
            ->set('ped.pedPrazoEntrega', $queryBuilder->expr()->literal($deliveryDeadline))
            ->set('ped.pedDataEntrega', $queryBuilder->expr()->literal($deliveryDate))
            ->where('ped.idPedped = ?1')
            ->setParameter(1, $orderId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param UpdateOrderStatusVO $updateOrderStatus
     * @return bool
     *
     * Update order status
     */
    public function updateOrderStatus(UpdateOrderStatusVO $updateOrderStatus)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedped', 'ped')
            ->set('ped.pedSituacao', $queryBuilder->expr()->literal($updateOrderStatus->getStatus()))
            ->set('ped.pedPendCanc', $queryBuilder->expr()->literal($updateOrderStatus->getOrderCancelled()))
            ->set('ped.atendente', $queryBuilder->expr()->literal($updateOrderStatus->getClerk()))
            ->set('ped.pedCodEnvio', $queryBuilder->expr()->literal(''))
            ->set('ped.pedPrimeiraOrigemData', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->set('ped.pedDtaAtualizacao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->where('ped.idPedped = ?1')
            ->setParameter(1, $updateOrderStatus->getOrderId())
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param UpdateOrderStatusVO $orderStatus
     * @return bool
     *
     * Update order status
     */
    public function updateOrderInternalStatus(UpdateOrderStatusVO $orderStatus)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedped', 'ped')
            ->set('ped.pedStaint', $queryBuilder->expr()->literal($orderStatus->getInternalStatus()))
            ->set('ped.pedDtaAtualizacao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->where('ped.idPedped = ?1')
            ->setParameter(1, $orderStatus->getOrderId())
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $orderId int
     * @param $newDeliveryDate \Datetime
     * @return bool
     *
     * Update new delivery date
     */
    public function updateNewDeliveryDate($orderId, $newDeliveryDate)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedped', 'ped')
            ->set('ped.pedNovaDataEntrega', $queryBuilder->expr()->literal($newDeliveryDate->format('Y-m-d H:i:s')))
            ->set('ped.pedDtaAtualizacao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->where('ped.idPedped = ?1')
            ->setParameter(1, $orderId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $term
     *
     * @return array
     *
     * Search orders from term
     */
    public function searchOrderFromTerm($term)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'ped.idPedped',
                'pedsit.name as situacao',
                'staint.nome as status_interno',
                'ped.pedDtaCompra',
                'ped.pedDtaAtualizacao',
                'ped.pedValorTotal',
                'ped.pedTipoPagamento1',
                'ped.pedFormaPagamento1',
                'ped.pedTipoPagamento2',
                'ped.pedFormaPagamento2',
                "CASE WHEN ped.tipoPedido = 'marketplace' THEN 'MarketPlace' ELSE (
                    CASE WHEN ped.assistencia = 1 THEN 'Assistência' ELSE (
                        CASE WHEN ped.recompra = 1 THEN 'Recompra' ELSE (
                            CASE WHEN ped.troca = 1 THEN 'Troca' ELSE 'Pedido' END) END) END) END as tipo"
            )
            ->from('AppBundle:B2cPedped', 'ped')
            ->join('ped.products', 'prd')
            ->join('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->where('ped.idPedped = :term OR ped.cobEmail LIKE :termLike OR prd.idNotafiscal = :term')
            ->leftJoin(
                'AppBundle:B2cStaint',
                'staint',
                'WITH',
                'ped.pedStaint = staint.id AND ped.pedSituacao = staint.situacao'
            )
            ->setParameter('term', $term)
            ->setParameter('termLike', '%'.$term.'%')
            ->orderBy('ped.idPedped', 'DESC')
            ->setMaxResults(20)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param $orderId
     *
     * @return array
     *
     * Search orders from order ID
     */
    public function searchFromOrderId($orderId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'ped.idPedped',
                'pedsit.name as situacao',
                'staint.nome as status_interno',
                'ped.pedDtaCompra',
                'ped.pedDtaAtualizacao',
                'ped.pedValorTotal',
                'ped.pedTipoPagamento1',
                'ped.pedFormaPagamento1',
                'ped.pedTipoPagamento2',
                'ped.pedFormaPagamento2',
                "CASE WHEN ped.tipoPedido = 'marketplace' THEN 'MarketPlace' ELSE (
                    CASE WHEN ped.assistencia = 1 THEN 'Assistência' ELSE (
                        CASE WHEN ped.recompra = 1 THEN 'Recompra' ELSE (
                            CASE WHEN ped.troca = 1 THEN 'Troca' ELSE 'Pedido' END) END) END) END as tipo"
            )
            ->from('AppBundle:B2cPedped', 'ped')
            ->join('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->leftJoin(
                'AppBundle:B2cStaint',
                'staint',
                'WITH',
                'ped.pedStaint = staint.id AND ped.pedSituacao = staint.situacao'
            )
            ->where('ped.idPedped = :orderId')
            ->setParameter('orderId', $orderId)
            ->orderBy('ped.idPedped', 'DESC')
            ->setMaxResults(20)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param $invoiceId
     *
     * @return array
     *
     * Search orders from invoice ID
     */
    public function searchFromInvoice($invoiceId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'ped.idPedped',
                'pedsit.name as situacao',
                'staint.nome as status_interno',
                'ped.pedDtaCompra',
                'ped.pedDtaAtualizacao',
                'ped.pedValorTotal',
                'ped.pedTipoPagamento1',
                'ped.pedFormaPagamento1',
                'ped.pedTipoPagamento2',
                'ped.pedFormaPagamento2',
                'nf.idFilial as idFilial',
                'filial.nome as nomeFilial',
                'filial.cnpj as cnpjFilial',
                "CASE WHEN ped.tipoPedido = 'marketplace' THEN 'MarketPlace' ELSE (
                    CASE WHEN ped.assistencia = 1 THEN 'Assistência' ELSE (
                        CASE WHEN ped.recompra = 1 THEN 'Recompra' ELSE (
                            CASE WHEN ped.troca = 1 THEN 'Troca' ELSE 'Pedido' END) END) END) END as tipo"
            )
            ->from('AppBundle:B2cPedped', 'ped')
            ->innerJoin('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->leftJoin('AppBundle:B2cNotafiscal', 'nf', 'WITH', 'ped.idPedped = nf.idPedped')
            ->leftJoin('AppBundle:B2cFilial', 'filial', 'WITH', 'filial.idFilial = nf.idFilial')
            ->leftJoin(
                'AppBundle:B2cStaint',
                'staint',
                'WITH',
                'ped.pedStaint = staint.id AND ped.pedSituacao = staint.situacao'
            )
            ->where('nf.idNotafiscal = :invoiceId')
            ->setParameter('invoiceId', $invoiceId)
            ->orderBy('ped.idPedped', 'DESC')
            ->setMaxResults(20)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param $email
     *
     * @return array
     *
     * Search orders from email
     */
    public function searchFromEmail($email)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'ped.idPedped',
                'pedsit.name as situacao',
                'staint.nome as status_interno',
                'ped.pedDtaCompra',
                'ped.pedDtaAtualizacao',
                'ped.pedValorTotal',
                'ped.pedTipoPagamento1',
                'ped.pedFormaPagamento1',
                'ped.pedTipoPagamento2',
                'ped.pedFormaPagamento2',
                "CASE WHEN ped.tipoPedido = 'marketplace' THEN 'MarketPlace' ELSE (
                    CASE WHEN ped.assistencia = 1 THEN 'Assistência' ELSE (
                        CASE WHEN ped.recompra = 1 THEN 'Recompra' ELSE (
                            CASE WHEN ped.troca = 1 THEN 'Troca' ELSE 'Pedido' END) END) END) END as tipo"
            )
            ->from('AppBundle:B2cPedped', 'ped')
            ->innerJoin('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->leftJoin(
                'AppBundle:B2cStaint',
                'staint',
                'WITH',
                'ped.pedStaint = staint.id AND ped.pedSituacao = staint.situacao'
            )
            ->where('ped.cobEmail LIKE :email')
            ->setParameter('email', $email.'%')
            ->orderBy('ped.idPedped', 'DESC')
            ->setMaxResults(20)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param $document
     *
     * @return array
     *
     * Search orders from document
     */
    public function searchFromDocument($document)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $document = str_replace(array('.', '-'), '', $document);

        return $queryBuilder
            ->select(
                'ped.idPedped',
                'pedsit.name as situacao',
                'staint.nome as status_interno',
                'ped.pedDtaCompra',
                'ped.pedDtaAtualizacao',
                'ped.pedValorTotal',
                'ped.pedTipoPagamento1',
                'ped.pedFormaPagamento1',
                'ped.pedTipoPagamento2',
                'ped.pedFormaPagamento2',
                "CASE WHEN ped.tipoPedido = 'marketplace' THEN 'MarketPlace' ELSE (
                    CASE WHEN ped.assistencia = 1 THEN 'Assistência' ELSE (
                        CASE WHEN ped.recompra = 1 THEN 'Recompra' ELSE (
                            CASE WHEN ped.troca = 1 THEN 'Troca' ELSE 'Pedido' END) END) END) END as tipo"
            )
            ->from('AppBundle:B2cPedped', 'ped')
            ->innerJoin('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->leftJoin(
                'AppBundle:B2cStaint',
                'staint',
                'WITH',
                'ped.pedStaint = staint.id AND ped.pedSituacao = staint.situacao'
            )
            ->where("replace(replace(ped.cobCpfCgc, '.', ''), '-', '') LIKE :document")
            ->setParameter('document', '%'.$document.'%')
            ->orderBy('ped.idPedped', 'DESC')
            ->setMaxResults(20)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param $phone
     *
     * @return array
     *
     * Search orders from phone
     */
    public function searchFromPhoneNumber($phone)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $phoneNumber = str_replace(array('-', ' ', '(', ')'), '', $phone);

        return $queryBuilder
            ->select(
                'ped.idPedped',
                'pedsit.name as situacao',
                'staint.nome as status_interno',
                'ped.pedDtaCompra',
                'ped.pedDtaAtualizacao',
                'ped.pedValorTotal',
                'ped.pedTipoPagamento1',
                'ped.pedFormaPagamento1',
                'ped.pedTipoPagamento2',
                'ped.pedFormaPagamento2',
                "CASE WHEN ped.tipoPedido = 'marketplace' THEN 'MarketPlace' ELSE (
                    CASE WHEN ped.assistencia = 1 THEN 'Assistência' ELSE (
                        CASE WHEN ped.recompra = 1 THEN 'Recompra' ELSE (
                            CASE WHEN ped.troca = 1 THEN 'Troca' ELSE 'Pedido' END) END) END) END as tipo"
            )
            ->from('AppBundle:B2cPedped', 'ped')
            ->innerJoin('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->leftJoin('AppBundle:B2cStaint', 'staint', 'WITH',
                'ped.pedStaint = staint.id AND ped.pedSituacao = staint.situacao')
            ->where("replace(replace(replace(replace(ped.cobTelA, '-', ''), ' ', ''), '(', ''), ')', '') LIKE :phoneNumber")
            ->where("replace(replace(replace(replace(ped.cobTelB, '-', ''), ' ', ''), '(', ''), ')', '') LIKE :phoneNumber")
            ->where("replace(replace(replace(replace(ped.entTelA, '-', ''), ' ', ''), '(', ''), ')', '') LIKE :phoneNumber")
            ->where("replace(replace(replace(replace(ped.entTelB, '-', ''), ' ', ''), '(', ''), ')', '') LIKE :phoneNumber")
            ->setParameter('phoneNumber', '%'.$phoneNumber.'%')
            ->orderBy('ped.idPedped', 'DESC')
            ->setMaxResults(20)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param $marketPlaceOrderId
     *
     * @return array
     *
     * Search orders from MarketPlace order ID
     */
    public function searchFromMarketPlaceOrder($marketPlaceOrderId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'ped.idPedped',
                'pedsit.name as situacao',
                'staint.nome as status_interno',
                'ped.pedDtaCompra',
                'ped.pedDtaAtualizacao',
                'ped.pedValorTotal',
                'ped.pedTipoPagamento1',
                'ped.pedFormaPagamento1',
                'ped.pedTipoPagamento2',
                'ped.pedFormaPagamento2',
                "CASE WHEN ped.tipoPedido = 'marketplace' THEN 'MarketPlace' ELSE (
                    CASE WHEN ped.assistencia = 1 THEN 'Assistência' ELSE (
                        CASE WHEN ped.recompra = 1 THEN 'Recompra' ELSE (
                            CASE WHEN ped.troca = 1 THEN 'Troca' ELSE 'Pedido' END) END) END) END as tipo"
            )
            ->from('AppBundle:B2cPedped', 'ped')
            ->innerJoin('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->innerJoin('AppBundle:B2cPedpedMarketplace', 'mkp', 'WITH', 'ped.idPedped = mkp.idPedped')
            ->leftJoin('AppBundle:B2cStaint', 'staint', 'WITH',
                'ped.pedStaint = staint.id AND ped.pedSituacao = staint.situacao')
            ->where('mkp.codOrder LIKE :marketPlaceOrderId')
            ->setParameter('marketPlaceOrderId', '%'.$marketPlaceOrderId.'%')
            ->orderBy('ped.idPedped', 'DESC')
            ->setMaxResults(20)
            ->getQuery()->getArrayResult();
    }


    /**
     * @param $name
     *
     * @return array
     *
     * Search Orders from Customer Name
     */
    public function searchFromCustomerName($name)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'ped.idPedped',
                'pedsit.name as situacao',
                'staint.nome as status_interno',
                'ped.pedDtaCompra',
                'ped.pedDtaAtualizacao',
                'ped.pedValorTotal',
                'ped.pedTipoPagamento1',
                'ped.pedFormaPagamento1',
                'ped.pedTipoPagamento2',
                'ped.pedFormaPagamento2',
                "CASE WHEN ped.tipoPedido = 'marketplace' THEN 'MarketPlace' ELSE (
                    CASE WHEN ped.assistencia = 1 THEN 'Assistência' ELSE (
                        CASE WHEN ped.recompra = 1 THEN 'Recompra' ELSE (
                            CASE WHEN ped.troca = 1 THEN 'Troca' ELSE 'Pedido' END) END) END) END as tipo"
            )
            ->from('AppBundle:B2cPedped', 'ped')
            ->innerJoin('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->leftJoin('AppBundle:B2cStaint', 'staint', 'WITH',
                'ped.pedStaint = staint.id AND ped.pedSituacao = staint.situacao')
            ->where("CONCAT(TRIM(ped.cobNome), ' ', ped.cobSobrenome) LIKE :customerName")
            ->setParameter('customerName', '%'.$name.'%')
            ->orderBy('ped.idPedped', 'DESC')
            ->setMaxResults(20)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param $customerId
     *
     * @return array
     *
     * Get customer orders
     */
    public function getOrdersByCustomer($customerId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'ped.idPedped',
                'pedsit.name as situacao',
                'staint.nome as status_interno',
                'ped.pedDtaCompra',
                'ped.pedDtaAtualizacao',
                'ped.pedValorTotal',
                'ped.pedTipoPagamento1',
                'ped.pedFormaPagamento1',
                'ped.pedTipoPagamento2',
                'ped.pedFormaPagamento2',
                "CASE WHEN ped.tipoPedido = 'marketplace' THEN 'MarketPlace' ELSE (
                    CASE WHEN ped.assistencia = 1 THEN 'Assistência' ELSE (
                        CASE WHEN ped.recompra = 1 THEN 'Recompra' ELSE (
                            CASE WHEN ped.troca = 1 THEN 'Troca' ELSE 'Pedido' END) END) END) END as tipo"
            )
            ->from('AppBundle:B2cPedped', 'ped')
            ->join('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->leftJoin('AppBundle:B2cStaint', 'staint', 'WITH',
                'ped.pedStaint = staint.id AND ped.pedSituacao = staint.situacao')
            ->where('ped.idClicli = :customerId')
            ->setParameter('customerId', $customerId)
            ->orderBy('ped.pedSituacao', 'ASC')
            ->addOrderBy('ped.idPedped', 'DESC')
            ->setMaxResults(50)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param int $customerId
     *
     * @return array
     *
     * Generate customer revenue report
     */
    public function getRevenueReportByCustomer($customerId)
    {
        $connection = $this->_em->getConnection();

        $dql = "SELECT
                    COUNT(*) AS sale_quantity,
                    not_sale.not_sale_quantity AS assistance_quantity,
                    SUM(ped_valor_total) AS sales_revenue,
                    MIN(ped_dta_compra) AS first_sale,
                    MAX(ped_dta_compra) AS last_sale
                FROM
                    b2c_pedped ped
                    LEFT JOIN (SELECT id_clicli, COUNT(*) as not_sale_quantity FROM b2c_pedped WHERE id_clicli = {$customerId} AND (assistencia = 1 OR recompra = 1) AND ped_situacao <> 7) AS not_sale ON (not_sale.id_clicli = ped.id_clicli)
                WHERE
                    ped.id_clicli = {$customerId} AND ped.ped_situacao <> 7
                        AND ped.assistencia <> 1
                        AND ped.recompra <> 1";

        $sth = $connection->prepare($dql);
        $sth->execute();

        return $sth->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param integer $orderId
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get current order status name
     */
    public function getSituation($orderId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $statusName = $queryBuilder
            ->select('pedsit.name', 'staint.nome AS staintNome')
            ->from('AppBundle:B2cPedped', 'ped')
            ->join('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
            ->leftJoin('AppBundle:B2cStaint', 'staint', 'WITH',
                'ped.pedSituacao = staint.situacao AND ped.pedStaint = staint.id')
            ->where('ped.idPedped = :orderId')
            ->setParameter('orderId', $orderId)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        $statusName['name'] = ($statusName['name'] ? $statusName['name'] : '');
        $statusName['internalStatusName'] =
            ($statusName['staintNome'] ? $statusName['staintNome'] : 'Sem status interno');

        return array($statusName['name'], $statusName['internalStatusName']);
    }

    /**
     * @param $orderId
     * @return bool
     *
     * Update order status
     */
    public function updateOrderUpdateDate($orderId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedped', 'ped')
            ->set('ped.pedDtaAtualizacao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->where('ped.idPedped = ?1')
            ->setParameter(1, $orderId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param OrderVO $orderVO
     * @param CreateShippingVO $shipping
     *
     * Update order Shipping
     * @return bool
     */
    public function updateOrderShipping(OrderVO $orderVO, CreateShippingVO $shipping)
    {
        $order = $this->_em->getRepository('AppBundle:B2cPedped')
            ->findOneBy(array('idPedped' => $orderVO->getOrderId()));

        if (empty($order)) {
            return true;
        }
        $order->setIdPedped($orderVO->getOrderId());
        $order->setPedTransportadora($shipping->getName());
        $order->setPedMontagem($shipping->isMounting());
        $order->setPedValorMontagem($shipping->getMountingValue());
        $order->setPedValorCustofrete($shipping->getCostPrice());
        $order->setPedEntregaRegiao($orderVO->getRegion()->getName());

        if (!$orderVO->getMarketplaceId()) {
            $order->setPedValorFrete($shipping->getPrice());
            $total = $order->getPedValorTotal() + $shipping->getPrice();
            $orderVO->setTotal($total);
        }

        if ($shipping->getRuleFreight()) {
            $order->setRuleFreight($shipping->getRuleFreight());
        }

        $this->_em->persist($order);
        $this->_em->flush();

        return true;
    }
}
