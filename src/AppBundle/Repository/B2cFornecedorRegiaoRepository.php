<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class B2cFornecedorRegiaoRepository
 * @package AppBundle\Repository
 */
class B2cFornecedorRegiaoRepository extends EntityRepository
{


    /**
     * Get destination for an item.
     *
     * @param $supplierId
     * @param $regionName
     * @return mixed
     * @throws \Exception
     */
    public function getDestinationBySupplierAndRegionName($supplierId, $regionName)
    {
        try {
            $queryBuilder = $this->_em->createQueryBuilder();
            return $queryBuilder
                ->select("forreg.destination")
                ->from('AppBundle:B2cFornecedorRegiao', 'forreg')
                ->leftJoin('AppBundle:B2cRegreg', 'reg', 'WITH', 'reg.id = forreg.idRegreg')
                ->where('forreg.idForfor = :supplierId')
                ->andWhere('reg.regiao = :regionName')
                ->setParameter('supplierId', $supplierId)
                ->setParameter('regionName', $regionName)
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult()['destination'];
        } catch (\Exception $e) {
            throw new \Exception('Status can not be updated because it does not have a registered destination. Error = ' . $e->getMessage());
        }
    }
}
