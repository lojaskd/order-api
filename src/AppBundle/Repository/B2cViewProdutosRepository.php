<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cPrdprd;
use AppBundle\Entity\VO\Create\CreateProductVO;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cViewProdutosRepository
 * @package AppBundle\Repository
 */
class B2cViewProdutosRepository extends EntityRepository
{

    /**
     * @param $productId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get min stock action
     */
    public function getMinStockAction($productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $sqtMinAcao = $queryBuilder
            ->select('prd.stqminAcao prdStq', 'mat.stqminAcao matStq')
            ->from('AppBundle:B2cViewProdutos', 'prd')
            ->leftJoin('AppBundle:B2cViewProdutos', 'mat', 'WITH', 'mat.id = prd.matriz')
            ->where('prd.id = :produto_id')
            ->setParameter('produto_id', $productId)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return ($sqtMinAcao['prdStq'] ? $sqtMinAcao['prdStq'] : $sqtMinAcao['matStq']);
    }

    /**
     * @param $productId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get product info from ID
     */
    public function getProductInfoFromId($productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'prd.nome',
                'prd.referencia',
                'prd.disponivel',
                'prd.esgotado',
                'prd.disponibilidade',
                'prd.destaque',
                'prd.estilo',
                'prd.bpromocao',
                'prd.descontoValor',
                'prd.descontoPerc',
                'prd.nomeCor',
                'prd.sexo',
                'prd.pvpromocao',
                'prd.promocaoini',
                'prd.promocaofim',
                'prd.categoria',
                'prd.tamanho',
                'prd.peso',
                'prd.cubagem',
                'prd.fornecedor',
                'prd.nomeFornecedor',
                'prd.prdPrazoFornecedor',
                'prd.prazoFornecedor',
                'prd.stqfornecedor',
                'prd.stqfornReservado',
                'prd.stqminAcao',
                'prd.stqminimo',
                'prd.acao',
                'prd.mostra',
                'prd.estoque',
                'prd.dispDias',
                'prd.pvpromocao',
                'prd.pvreal',
                'prd.custo',
                'prd.precoProduto',
                'prd.prazo'
            )
            ->from('AppBundle:B2cViewProdutos', 'prd')
            ->where('prd.id = :productId')
            ->setParameter('productId', $productId)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param $productId
     * @param $quantity
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get product stock and deadline from product ID and quantity
     */
    public function getProductStockAndDeadline($productId, $quantity)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'DISTINCT (CASE WHEN prdEstoque.qtd < :productQtd THEN
                    (CASE WHEN prd.prdPrazoFornecedor > 0 THEN prd.prdPrazoFornecedor ELSE prd.prazoFornecedor END)
                        ELSE prd.dispDias END) as deadline',
                'prdEstoque.qtd as stock',
                'prdEstoque.idFilial'
            )
            ->from('AppBundle:B2cViewProdutos', 'prd')
            ->leftJoin('AppBundle\Entity\B2cPrdestoque', 'prdEstoque', 'WITH', 'prdEstoque.idPrdprd = prd.id AND prdEstoque.qtd >= :productQtd ')
            ->leftJoin('AppBundle\Entity\B2cFilial', 'filial', 'WITH', 'filial.idFilial = prdEstoque.idFilial')
            ->where('prd.id = :productId')
            ->setParameter('productId', $productId)
            ->setParameter('productQtd', $quantity)
            ->orderBy('filial.prioridade')
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param $productId
     * @param $nextBusinessDay
     * @param $zipCode
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     *
     * Get product deadline
     */
    public function getProductDeadline($productId, $nextBusinessDay, $zipCode)
    {
        $connection = $this->_em->getConnection();

        $dql = "CALL spc_select_prazos_produto({$productId}, '{$nextBusinessDay}', '{$zipCode}')";

        $sth = $connection->prepare($dql);
        $sth->execute();

        return $sth->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $productId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get provider stock
     */
    public function getProviderStock($productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('(CASE WHEN prd.stqminAcao = 2 THEN prd.stqfornecedor ELSE 0 END) as stock', 'prd.unidadeFracionada')
            ->from('AppBundle:B2cViewProdutos', 'prd')
            ->where('prd.id = :productId')
            ->setParameter('productId', $productId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param CreateProductVO $product
     * @param B2cPrdprd $prdprd
     * @return bool
     *
     * Update product stock info
     */
    public function updateProductStockInfo(CreateProductVO $product, B2cPrdprd $prdprd)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cViewProdutos', 'viewprd')
            ->set('viewprd.estoque', $queryBuilder->expr()->literal($prdprd->getEstoque()))
            ->set('viewprd.stqfornecedor', $queryBuilder->expr()->literal($prdprd->getStqfornecedor()))
            ->set('viewprd.disponibilidade', $queryBuilder->expr()->literal($product->getAvailability()))
            ->set('viewprd.esgotado', $queryBuilder->expr()->literal($product->isOut()))
            ->set('viewprd.mostra', $queryBuilder->expr()->literal($product->isShow()))
            ->set('viewprd.prazo', $queryBuilder->expr()->literal($product->getProviderDeadline()))
            ->set('viewprd.acao', $queryBuilder->expr()->literal($product->getAction()))
            ->set('viewprd.dtaAlteracao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->set('viewprd.stqminAcao', $queryBuilder->expr()->literal($product->getMinStockAction()))
            ->where('viewprd.id = ?1')
            ->setParameter(1, $product->getId())
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param integer $productId
     * @param CreateProductVO $productAvailabilityInfo
     * @param string $user
     * @return bool
     *
     * Update product availability info
     */
    public function updateProductAvailabilityInfo($productId, CreateProductVO $productAvailabilityInfo, $user)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cViewProdutos', 'viewprd')
            ->set('viewprd.disponibilidade', $queryBuilder->expr()->literal($productAvailabilityInfo->getAvailability()))
            ->set('viewprd.esgotado', $queryBuilder->expr()->literal($productAvailabilityInfo->isOut()))
            ->set('viewprd.mostra', $queryBuilder->expr()->literal($productAvailabilityInfo->isShow()))
            ->set('viewprd.acao', $queryBuilder->expr()->literal($productAvailabilityInfo->getAction()))
            ->set('viewprd.usuarioAlteracao', $queryBuilder->expr()->literal($user . ' - por cancelamento de pedido'))
            ->set('viewprd.dtaAlteracao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->where('viewprd.id = ?1')
            ->setParameter(1, $productId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $kitId
     * @return array
     *
     * Get kit data
     */
    public function getKit($kitId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'prd.disponivel',
                'prd.estoque',
                'prd.stqminimo',
                'prd.stqminAcao',
                'prd.stqfornecedor',
                'kit.quantidade',
                'kit.desconto',
                'prd.pvreal',
                'prd.pvpromocao',
                'prd.promocaoini',
                'prd.promocaofim',
                'prd.mostra',
                'prd.dispDias',
                'prd.prazoFornecedor',
                'prd.fornecedor',
                'kit.obrigatorio'
            )
            ->from('AppBundle:B2cViewProdutos', 'prd')
            ->innerJoin('AppBundle:B2cProdutosKits', 'kit', 'WITH', 'kit.idProduto = prd.id')
            ->where('kit.idKit = :kitId')
            ->andWhere('kit.desmarcar = 0 AND prd.disponivel = 1 AND prd.pvreal > 0')
            ->andWhere('prd.estoque > 0 OR prd.stqminAcao IN (1,2) OR kit.obrigatorio = 1')
            ->setParameter('kitId', $kitId)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param CreateProductVO $product
     * @return bool
     *
     * Update kit stock
     */
    public function updateKitStock(CreateProductVO $product)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cViewProdutos', 'viewprd')
            ->set('viewprd.peso', $queryBuilder->expr()->literal(1))
            ->set('viewprd.estoque', $queryBuilder->expr()->literal($product->getProductStock()))
            ->set('viewprd.stqfornecedor', $queryBuilder->expr()->literal($product->getProviderStock()))
            ->set('viewprd.stqminimo', $queryBuilder->expr()->literal($product->getMinStock()))
            ->set('viewprd.stqminAcao', $queryBuilder->expr()->literal($product->getMinStockAction()))
            ->set('viewprd.pvreal', $queryBuilder->expr()->literal($product->getRealPrice()))
            ->set('viewprd.pvpromocao', $queryBuilder->expr()->literal($product->getPromotionPrice()))
            ->set('viewprd.dispDias', $queryBuilder->expr()->literal($product->getAvailabilityDays()))
            ->set('viewprd.prazoFornecedor', $queryBuilder->expr()->literal($product->getProviderDeadline()))
            ->set('viewprd.promocaoini', $queryBuilder->expr()->literal(
                !empty($product->getInitialPromotion()) ? $product->getInitialPromotion()->format('Y-m-d H:i:s') : ''
            ))
            ->set('viewprd.promocaofim', $queryBuilder->expr()->literal(
                !empty($product->getFinalPromotion()) ? $product->getFinalPromotion()->format('Y-m-d H:i:s') : ''
            ))
            ->set('viewprd.mostra', $queryBuilder->expr()->literal($product->isShow()))
            ->set('viewprd.bpromocao', $queryBuilder->expr()->literal($product->isPromotion()))
            ->set('viewprd.precoProduto', $queryBuilder->expr()->literal($product->getProductPrice()))
            ->set('viewprd.disponibilidade', $queryBuilder->expr()->literal($product->getAvailability()))
            ->set('viewprd.prazo', $queryBuilder->expr()->literal($product->getProductDeadline()))
            ->where('viewprd.id = ?1')
            ->setParameter(1, $product->getId())
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $date
     * @param $days
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     *
     * Get delivery date
     */
    public function getDateAddNextBusinessDay($date, $days)
    {
        $connection = $this->_em->getConnection();

        $dql = "SELECT DATE_ADD_UTIL('".$date."', (" . $days.")) as added_date from dual";

        $sth = $connection->prepare($dql);
        $sth->execute();
        $addedDate = $sth->fetch(\PDO::FETCH_ASSOC);

        return (isset($addedDate['added_date']) ? $addedDate['added_date'] : '');
    }
    
    /**
     * @param $date
     * @param $days
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     *
     * Get delivery date
     */
    public function getDateSubNextBusinessDay($date, $days)
    {
        $connection = $this->_em->getConnection();

        $dql = "SELECT DATE_SUB_UTIL('".$date."', (" . $days.")) as date from dual";

        $sth = $connection->prepare($dql);
        $sth->execute();
        $addedDate = $sth->fetch(\PDO::FETCH_ASSOC);

        return (isset($addedDate['date']) ? $addedDate['date'] : '');
    }

    /**
     * @param $date
     * @param $date2
     * @return string
     */
    public function getDateDiffBusinessDay($date, $date2)
    {
        $connection = $this->_em->getConnection();

        $dql = "SELECT GET_QTDE_DIAS_UTEIS('" . $date . "', '" . $date2 . "') as added_date from dual";

        $sth = $connection->prepare($dql);
        $sth->execute();
        $addedDate = $sth->fetch(\PDO::FETCH_ASSOC);

        return (isset($addedDate['added_date']) ? $addedDate['added_date'] : '');
    }

    /**
     * @param $productId
     * @param $quantity
     * @param $user
     * @return bool
     *
     * Increase provider stock
     */
    public function increaseProviderStock($productId, $quantity, $user)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cViewProdutos', 'viewPrd')
            ->set('viewPrd.stqfornecedor', 'viewPrd.stqfornecedor + ' . $quantity)
            ->set('viewPrd.dtaAlteracao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->set('viewPrd.usuarioAlteracao', $queryBuilder->expr()->literal($user . ' - por cancelamento de pedido'))
            ->where('viewPrd.id = ?1')
            ->setParameter(1, $productId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $productId
     * @param $quantity
     * @param $user
     * @return bool
     *
     * Increase store stock
     */
    public function increaseStoreStock($productId, $quantity, $user)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cViewProdutos', 'viewPrd')
            ->set('viewPrd.estoque', 'viewPrd.estoque + ' . $quantity)
            ->set('viewPrd.dtaAlteracao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->set('viewPrd.usuarioAlteracao', $queryBuilder->expr()->literal($user . ' - por cancelamento de pedido'))
            ->where('viewPrd.id = ?1')
            ->setParameter(1, $productId)
            ->getQuery()->execute();

        return true;
    }
}
