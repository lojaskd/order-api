<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cRegcepRepository
 * @package AppBundle\Repository
 */
class B2cRegcepRepository extends EntityRepository
{

    /**
     * @param $zip
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get region from zip code
     */
    public function getRegion($zip)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $zip = str_replace('-', '', $zip);

        return $queryBuilder
            ->select('reg.id', 'reg.regiao', 'cep.fimCep - :zip as dist', 'reg.clienteEscolhe')
            ->from('AppBundle:B2cRegreg', 'reg')
            ->leftJoin('reg.regCep', 'cep')
            ->where($queryBuilder->expr()->between(':zip', 'cep.iniCep', 'cep.fimCep'))
            ->setParameter('zip', $zip)
            ->having('dist >= 0')
            ->orderBy('dist')
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
