<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPromoValeRepository
 * @package AppBundle\Repository
 */
class B2cPromoValeRepository extends EntityRepository
{

    /**
     * @param $couponCode
     * @return bool
     *
     * Update coupon situation
     */
    public function updateCouponSituation($couponCode)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPromoVale', 'vale')
            ->set('vale.situacao', $queryBuilder->expr()->literal('U'))
            ->where('vale.situacao != ?1')
            ->andWhere('vale.codigo = ?2')
            ->setParameter(1, 'E')
            ->setParameter(2, $couponCode)
            ->getQuery()->execute();
        return true;
    }

    /**
     * @param $couponCode
     * @return boolean
     *
     * Update coupon utilization
     */
    public function updateCouponUtilization($couponCode)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPromoVale', 'vale')
            ->set('vale.valeUtilizado', 'vale.valeUtilizado + 1')
            ->where('vale.situacao = ?1')
            ->andWhere('vale.codigo = ?2')
            ->setParameter(1, 'E')
            ->setParameter(2, $couponCode)
            ->getQuery()->execute();
        return true;
    }
}
