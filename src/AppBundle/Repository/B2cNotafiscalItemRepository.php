<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cNotafiscalItem;
use AppBundle\Entity\Validate\Create\CreateValidateInvoiceProduct;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cNotafiscalItemRepository
 * @package AppBundle\Repository
 */
class B2cNotafiscalItemRepository extends EntityRepository
{

    /**
     * @param $orderId
     * @param $productReference
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getShipper($orderId, $productReference)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $shipper = $queryBuilder
            ->select(
                'nfi.idFilial',
                'nfi.idNotafiscal',
                'nfi.idPedped',
                'nfi.prdReferencia',
                'nfi.prdQtde',
                'nfi.dataCadastro',
                'nfi.dataDevolucao',
                'nfi.devolvido',
                'nfi.nomeTransportadora',
                'nfi.documentoTransportadora',
                'nfi.hub',
                'nf.pathXml',
                'nf.pathPdf'
            )
            ->from('AppBundle:B2cNotafiscalItem', 'nfi')
            ->innerJoin('AppBundle:B2cNotafiscal', 'nf', 'WITH', 'nf.idPedped = nfi.idPedped AND nf.idNotafiscal = nfi.idNotafiscal')
            ->where('nfi.idPedped = :orderId')
            ->andWhere('nfi.prdReferencia = :productReference')
            ->andWhere('nfi.devolvido = 0')
            ->setParameter('orderId', $orderId)
            ->setParameter('productReference', $productReference)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return $shipper;
    }

    /**
     * Create invoice product
     *
     * @param CreateValidateInvoiceProduct $createInvoiceProduct
     * @return B2cNotafiscalItem
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function createInvoiceProduct(CreateValidateInvoiceProduct $createInvoiceProduct)
    {
        $this->_em->getConnection()->beginTransaction();
        try {
            $invoiceItem = new B2cNotafiscalItem;
            $invoiceItem->setIdPedped($createInvoiceProduct->getOrderId());
            $invoiceItem->setIdNotafiscal($createInvoiceProduct->getInvoiceId());
            $invoiceItem->setIdFilial($createInvoiceProduct->getBranchId());
            $invoiceItem->setDataDevolucao($createInvoiceProduct->getReturnedDate());
            $invoiceItem->setDevolvido($createInvoiceProduct->isReturned() ?: 0);
            $invoiceItem->setDataCadastro($createInvoiceProduct->getCreatedDate() ?: new \DateTime('now'));
            $invoiceItem->setNomeTransportadora($createInvoiceProduct->getShipperName());
            $invoiceItem->setDocumentoTransportadora($createInvoiceProduct->getShipperDoc());
            $invoiceItem->setHub($createInvoiceProduct->getHub());
            $invoiceItem->setPrdQtde($createInvoiceProduct->getQty());
            $invoiceItem->setPrdReferencia($createInvoiceProduct->getProductReference());
            //save
            $this->_em->persist($invoiceItem);
            $this->_em->flush();
            // Try and commit the transaction
            $this->_em->getConnection()->commit();

            return $invoiceItem;
        } catch (\Exception $exception) {
            // Rollback the failed transaction attempt
            $this->_em->getConnection()->rollback();
            throw $exception;
        }
    }
}
