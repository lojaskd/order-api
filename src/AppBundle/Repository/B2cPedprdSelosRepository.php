<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;
use AppBundle\Entity\B2cPedprdSelos;

/**
 * Class B2cPedprdSelosRepository
 * @package AppBundle\Repository
 */
class B2cPedprdSelosRepository extends EntityRepository
{

    /**
     * Save Order Product Seals
     * @param $orderId
     * @param $seals
     * @return bool
     */
    public function insertOrderProductsSeals($orderId, $seals)
    {
        try {
            foreach ($seals as $prod) {
                foreach ($prod as $seal) {
                    $newPedprdSelos = new B2cPedprdSelos();

                    $newPedprdSelos->setIdPedped($orderId);
                    $newPedprdSelos->setIdPrdprd($seal['idPrdprd']);
                    $newPedprdSelos->setIdSelo($seal['id']);
                    $newPedprdSelos->setNome($seal['nome']);

                    // Persist the order log in database
                    $this->_em->persist($newPedprdSelos);
                    $this->_em->flush();
                }
            }
        } catch (\Exception $e) {
            throw new Exception('Error to insert PedPrdSelos order: '. $e->getMessage());
        }

        return true;
    }
}
