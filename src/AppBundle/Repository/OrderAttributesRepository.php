<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class OrderAttributesRepository
 * @package AppBundle\Repository
 */
class OrderAttributesRepository extends EntityRepository
{

    /**
     * @param $orderId
     * @param $attributeId
     * @return array
     *
     * Get order attributes
     */
    public function getOrderAttributes($orderId, $attributeId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $orderAttributes = $queryBuilder
            ->select('orderAttr.value')
            ->from('AppBundle:OrderAttributes', 'orderAttr')
            ->where('orderAttr.idAttr = :attributeId')
            ->andWhere('orderAttr.orderId = :orderId')
            ->setParameter('attributeId', $attributeId)
            ->setParameter('orderId', $orderId)
            ->getQuery()->getArrayResult();

        $orderAttrReturn = array();
        foreach ($orderAttributes as $orderAttribute) {
            $orderAttrReturn[] = $orderAttribute['value'];
        }

        return $orderAttrReturn;
    }
}
