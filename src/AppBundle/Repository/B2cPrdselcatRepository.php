<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPrdselcatRepository
 * @package AppBundle\Repository
 */
class B2cPrdselcatRepository extends EntityRepository
{

    /**
     * @param $productId
     * @return array
     *
     * Gets product related categories
     */
    public function getRelatedCategories($productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select("cat.idCat as category")
            ->from('AppBundle:B2cPrdselcat', 'cat')
            ->where('cat.idPrd = :productId')
            ->setParameter('productId', $productId)
            ->getQuery()->getResult();
    }
}
