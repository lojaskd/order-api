<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPrdestoqueRepository
 * @package AppBundle\Repository
 */
class B2cPrdestoqueRepository extends EntityRepository
{

    /**
     * @param $branchOfficeId
     * @param $productId
     * @param $newBranchOfficeStock
     * @return boolean
     *
     * Update branch office stock
     */
    public function updateBranchOfficeStock($branchOfficeId, $productId, $newBranchOfficeStock)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPrdestoque', 'stock')
            ->set('stock.qtd', $queryBuilder->expr()->literal($newBranchOfficeStock))
            ->where('stock.idPrdprd = ?1')
            ->andWhere('stock.idFilial = ?2')
            ->setParameter(1, $productId)
            ->setParameter(2, $branchOfficeId)
            ->getQuery()->execute();
        return true;
    }

    /**
     * @param $productId
     * @param $quantity
     * @param $branchStockId
     * @return boolean
     *
     * Increase product store stock
     */
    public function increaseStock($productId, $quantity, $branchStockId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPrdestoque', 'stock')
            ->set('stock.qtd', 'stock.qtd + ' . $quantity)
            ->where('stock.idPrdprd = ?1')
            ->andWhere('stock.idFilial = ?2')
            ->setParameter(1, $productId)
            ->setParameter(2, $branchStockId)
            ->getQuery()->execute();
        return true;
    }
}
