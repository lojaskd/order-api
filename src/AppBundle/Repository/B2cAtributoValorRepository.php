<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cAtributoValor;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class B2cAtributoValorRepository
 * @package AppBundle\Repository
 */
class B2cAtributoValorRepository extends EntityRepository
{

    /**
     * @param B2cAtributoValor $stockAttribute
     * @param $quantity
     * @return bool
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function updateMarketplaceStock($stockAttribute, $quantity)
    {
        $retry = 0;
        $saved = false;

        $this->_em->getConnection()->beginTransaction();

        while (!$saved and $retry < 3) {
            try {
                $stockAttribute->setRespostas($stockAttribute->getRespostas() - $quantity);

                // Save account
                $this->_em->persist($stockAttribute);
                $this->_em->flush();

                $this->_em->getConnection()->commit();

                $saved = true;
            } catch (Exception $e) {
                $this->_em->getConnection()->rollback();
                $retry++;
            }
        }

        return $saved;
    }
}
