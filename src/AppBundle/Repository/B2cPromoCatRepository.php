<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPromoCatRepository
 * @package AppBundle\Repository
 */
class B2cPromoCatRepository extends EntityRepository
{

    /**
     * @param $promoId
     * @param $categoryId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Validate category coupon
     */
    public function validateCategoryCoupon($promoId, $categoryId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('promoCat.idCategoria')
            ->from('AppBundle:B2cPromoCat', 'promoCat')
            ->where('promoCat.idPromo = :promoId')
            ->andWhere('promoCat.idCategoria = :categoryId')
            ->setParameter('promoId', $promoId)
            ->setParameter('categoryId', $categoryId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function getPromotionCategories($promotionId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select("GROUP_CONCAT(promoCat.idCategoria SEPARATOR ', ')")
            ->from('AppBundle:B2cPromoCat', 'promoCat')
            ->where('promoCat.idPromo = :promotionId')
            ->setParameter('promotionId', $promotionId)
            ->groupBy('promoCat.idPromo')
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();
    }
}
