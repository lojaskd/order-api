<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\B2cPedstatus;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPedstatusRepository
 * @package AppBundle\Repository
 */
class B2cPedstatusRepository extends EntityRepository
{

    /**
     * @param UpdateOrderStatusVO $orderStatusVO
     * @return bool
     */
    public function updateApprovementDate(UpdateOrderStatusVO $orderStatusVO)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedstatus', 'pedstatus')
                ->set('pedstatus.pedDtaSituacao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
                ->where('pedstatus.pedSituacao = 3 AND pedstatus.pedStaint = 0')
                ->andWhere('pedstatus.idPedped = ?1')
                ->setParameter(1, $orderStatusVO->getOrderId())
                ->getQuery()->execute();

        return true;
    }

    /**
     * @param UpdateOrderStatusVO $updateOrderStatusDateVO
     * @param bool|false $predict
     * @return bool
     */
    public function updateDate(UpdateOrderStatusVO $updateOrderStatusDateVO, $predict = false)
    {
        $pedStatus = $this->_em->getRepository('AppBundle:B2cPedstatus')->findOneBy(array(
            'idPedped' => $updateOrderStatusDateVO->getOrderId(),
            'pedSituacao' => $updateOrderStatusDateVO->getStatus(),
            'pedStaint' => $updateOrderStatusDateVO->getInternalStatus()
        ));

        if (!$pedStatus) {
            $pedStatus = new B2cPedstatus();
            /* @var $pedped B2cPedped */
            $pedped = $this->_em->getRepository('AppBundle:B2cPedped')->findOneBy(array(
                    'idPedped' => $updateOrderStatusDateVO->getOrderId(),
                )
            );
            $pedStatus->setIdPedped($pedped);
            $pedStatus->setPedDtaSituacao(null);
            $pedStatus->setPedDtaSituacaoPrev(null);

            $pedStatus->setPedSituacao($updateOrderStatusDateVO->getStatus());
            $pedStatus->setPedStaint($updateOrderStatusDateVO->getInternalStatus());
        }

        if ($predict && $pedStatus->getPedDtaSituacaoPrev() != $updateOrderStatusDateVO->getDate()) {
            $pedStatus->setPedDtaSituacaoPrev($updateOrderStatusDateVO->getDate());
        }

        if (!$predict && $pedStatus->getPedDtaSituacao() != $updateOrderStatusDateVO->getDate()) {
            $pedStatus->setPedDtaSituacao($updateOrderStatusDateVO->getDate());
        }

        $this->_em->persist($pedStatus);
        $this->_em->flush();

        return true;
    }
}
