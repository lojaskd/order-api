<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\VO\Create\CreateProductVO;

/**
 * Class B2cPrdprdRepository
 * @package AppBundle\Repository
 */
class B2cPrdprdRepository extends EntityRepository
{

    /**
     * @param $productId
     * @param $newStock
     * @return bool
     *
     * Update provider stock
     */
    public function updateProviderStock($productId, $newStock)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPrdprd', 'prd')
            ->set('prd.stqfornecedor', $queryBuilder->expr()->literal($newStock))
            ->set('prd.dtaAlteracao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->where('prd.id = ?1')
            ->setParameter(1, $productId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $productId
     * @param $quantity
     * @param $clerk
     * @return bool
     *
     * Increase provider stock
     */
    public function increaseProviderStock($productId, $quantity, $clerk)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPrdprd', 'prd')
            ->set('prd.stqfornecedor', 'prd.stqfornecedor + ' . $quantity)
            ->set('prd.dtaAlteracao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->set('prd.usuarioAlteracao', $queryBuilder->expr()->literal($clerk . ' - por cancelamento de pedido'))
            ->where('prd.id = ?1')
            ->setParameter(1, $productId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param int $kitId
     * @return array
     *
     * Get kit data
     */
    public function getKit($kitId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'prd.disponivel',
                'prd.estoque',
                'prd.stqminimo',
                'prd.stqminAcao',
                'prd.stqfornecedor',
                'kit.quantidade',
                'kit.desconto',
                'prd.pvreal',
                'prd.pvpromocao',
                'prd.promocaoini',
                'prd.promocaofim',
                '1 as mostra',
                'prd.dispDias',
                'prd.prazoFornecedor',
                'prd.fornecedor',
                'kit.obrigatorio'
            )
            ->from('AppBundle:B2cPrdprd', 'prd')
            ->innerJoin('AppBundle:B2cProdutosKits', 'kit', 'WITH', 'kit.idProduto = prd.id')
            ->where('kit.idKit = :kitId')
            ->andWhere('kit.desmarcar = 0 AND prd.disponivel = 1 AND prd.pvreal > 0')
            ->andWhere('prd.estoque > 0 OR prd.stqminAcao IN (1,2) OR kit.obrigatorio = 1')
            ->setParameter('kitId', $kitId)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param CreateProductVO $product
     * @return bool
     *
     * Update kit stock
     */
    public function updateKitStock(CreateProductVO $product)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPrdprd', 'prd')
            ->set('prd.peso', $queryBuilder->expr()->literal(1))
            ->set('prd.estoque', $queryBuilder->expr()->literal($product->getProductStock()))
            ->set('prd.stqfornecedor', $queryBuilder->expr()->literal($product->getProviderStock()))
            ->set('prd.stqminimo', $queryBuilder->expr()->literal($product->getMinStock()))
            ->set('prd.stqminAcao', $queryBuilder->expr()->literal($product->getMinStockAction()))
            ->set('prd.pvreal', $queryBuilder->expr()->literal($product->getRealPrice()))
            ->set('prd.pvpromocao', $queryBuilder->expr()->literal($product->getPromotionPrice()))
            ->set('prd.dispDias', $queryBuilder->expr()->literal($product->getAvailabilityDays()))
            ->set('prd.prazoFornecedor', $queryBuilder->expr()->literal($product->getProviderDeadline()))
            ->set('prd.promocaoini', $queryBuilder->expr()->literal(
                !empty($product->getInitialPromotion()) ? $product->getInitialPromotion()->format('Y-m-d H:i:s') : ''
            ))
            ->set('prd.promocaofim', $queryBuilder->expr()->literal(
                !empty($product->getFinalPromotion()) ? $product->getFinalPromotion()->format('Y-m-d H:i:s') : ''
            ))
            ->set('prd.dtaAlteracao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->where('prd.id = ?1')
            ->setParameter(1, $product->getId())
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param integer $productId
     * @param string $clerk
     * @return bool
     *
     * Update product
     */
    public function updateProduct($productId, $clerk)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPrdprd', 'prd')
            ->set('prd.dtaAlteracao', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->set('prd.usuarioAlteracao', $queryBuilder->expr()->literal($clerk . ' - por cancelamento de pedido'))
            ->where('prd.id = ?1')
            ->setParameter(1, $productId)
            ->getQuery()->execute();

        return true;
    }
}
