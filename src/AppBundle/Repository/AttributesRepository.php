<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AttributesRepository
 * @package AppBundle\Repository
 */
class AttributesRepository extends EntityRepository
{

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get order attributes
     */
    public function getAttributes()
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $attributes = $queryBuilder
            ->select(
                'attr.id as id_attr',
                'attr.name',
                'attrType.name as type_name',
                'attrType.description as type_description',
                'attr.multiple',
                'attr.required',
                'attr.default',
                'attr.hint'
            )
            ->from('AppBundle:Attributes', 'attr')
            ->innerJoin('AppBundle:AttributesEntities', 'attrEnt', 'WITH', 'attrEnt.idAttr = attr.id')
            ->innerJoin('AppBundle:AttributesTypes', 'attrType', 'WITH', 'attrType.id = attr.attrType')
            ->where('attrEnt.entity = :type')
            ->setParameter('type', "order")
            ->getQuery()->getArrayResult();

        return $attributes;
    }
}
