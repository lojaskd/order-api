<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class B2cSelosRepository
 * @package AppBundle\Repository
 */
class B2cSelosRepository extends EntityRepository
{

    /**
     * @param $productId
     * @return array
     */
    public function getSealsProducts($productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('sel.id', 'prds.idPrdprd', 'sel.nome')
            ->from('AppBundle:B2cSelos', 'sel')
            ->join('AppBundle:B2cPrdselos', 'prds', 'WITH', 'prds.idSelo = sel.id')
            ->where('prds.idPrdprd = :product')
            ->setParameter('product', $productId)
            ->getQuery()->getArrayResult();
    }
}
