<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cClicli;
use AppBundle\Entity\Validate\ValidateAddress;
use AppBundle\Entity\Validate\ValidateCustomer;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cClicliRepository
 * @package AppBundle\Repository
 */
class B2cClicliRepository extends EntityRepository
{

    /**
     * @param $customerId
     * @return mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get customer by ID
     */
    public function getCustomerById($customerId)
    {
        $customer = null;

        if ($customerId) {
            $queryBuilder = $this->_em->createQueryBuilder();

            $customer = $queryBuilder
                ->select('cli')
                ->from('AppBundle:B2cClicli', 'cli')
                ->where('cli.idClicli = :customerId')
                ->setParameter('customerId', $customerId)
                ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);
        }

        return $customer;
    }

    /**
     * @param $cpf
     * @param $marketPlaceEmail
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get marketplace customer
     */
    public function getMarketPlaceCustomer($cpf, $marketPlaceEmail)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $customer = $queryBuilder
            ->select('cli')
            ->from('AppBundle:B2cClicli', 'cli')
            ->where('cli.cpfCgc = :cpf')
            ->andWhere('cli.email = :marketPlaceEmail')
            ->setParameter('cpf', $cpf)
            ->setParameter('marketPlaceEmail', $marketPlaceEmail)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);

        return $customer;
    }

    /**
     * @param ValidateCustomer $customer
     * @param ValidateAddress $address
     * @return B2cClicli
     *
     * Create a customer on database
     */
    public function addCustomer(ValidateCustomer $customer, ValidateAddress $address)
    {
        $newCustomer = new B2cClicli();
        $newCustomer->setEmail($customer->getEmail());
        $newCustomer->setCpfCgc($customer->getCpf());
        $newCustomer->setRgIe($customer->getRgIe());
        $newCustomer->setNome($customer->getFirstName());
        $newCustomer->setSobrenome($customer->getLastName());
        $newCustomer->setTipoPessoa(($customer->getPersonType() ? $customer->getPersonType() : 'F'));
        $newCustomer->setSexo($customer->getGenre());
        $newCustomer->setSenha($customer->getPassword() ? $customer->getPassword() : 'lojaskd2016');
        $newCustomer->setSenhaB64($customer->getPassword() ? base64_encode($customer->getPassword()) : base64_encode('lojaskd2016'));
        $newCustomer->setNewsletter($customer->isAcceptsMarketing());
        $newCustomer->setEndereco($address->getAddress());
        $newCustomer->setNumero($address->getNumber());
        $newCustomer->setComplemento($address->getComplement());
        $newCustomer->setBairro($address->getNeighborhood());
        $newCustomer->setEstado($address->getState());
        $newCustomer->setCidade($address->getCity());
        $newCustomer->setPais(1);

        $address->setZip(str_replace(array('-', '.', ' '), '', $address->getZip()));
        $address->setZip(substr($address->getZip(), 0, 5) . '-' . substr($address->getZip(), 5));

        $newCustomer->setCep($address->getZip());
        $newCustomer->setTelefoneA($address->getTelephoneA());
        $newCustomer->setTelefoneB($address->getTelephoneB());
        $newCustomer->setReferencia($address->getReference());
        $newCustomer->setStatus(0);
        $newCustomer->setProcessadows(0);
        $newCustomer->setCadastro(new \DateTime('now'));
        $newCustomer->setPrimeiroAcesso(new \DateTime('now'));
        $newCustomer->setDtStatus(new \DateTime('now'));
        $newCustomer->setDataAfiafi(new \DateTime('now'));
        $newCustomer->setDtaNasc($customer->getBirthday());
        $newCustomer->setTipoCadastro($customer->getTypeRegister());

        $this->_em->persist($newCustomer);
        $this->_em->flush();

        return $newCustomer;
    }

    /**
     * @param $customerId
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get if customer accepts newsletter email
     */
    public function customerAcceptsNewsletter($customerId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $newsletter = $queryBuilder
            ->select('cli.newsletter')
            ->from('AppBundle:B2cClicli', 'cli')
            ->where('cli.idClicli = :customerId')
            ->setParameter('customerId', $customerId)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);

        return (isset($newsletter['newsletter']) ? $newsletter['newsletter'] : true);
    }
}
