<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cRegestRepository
 * @package AppBundle\Repository
 */
class B2cRegestRepository extends EntityRepository
{

    /**
     * @param $city
     * @param $state
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get IBGE code from city and state
     */
    public function getIBGECode($city, $state)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $ibgeCode = $queryBuilder
            ->select('ibge.codIbge')
            ->from('AppBundle:B2cRegest', 'regEst')
            ->leftJoin('AppBundle:CidadesIbge', 'ibge', 'WITH', 'ibge.idRegest = regEst.id')
            ->where('regEst.uf = :state')
            ->andWhere('ibge.cidade = :city')
            ->setParameter('state', $state)
            ->setParameter('city', $city)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return (isset($ibgeCode['codIbge']) ? $ibgeCode['codIbge'] : 0);
    }
}
