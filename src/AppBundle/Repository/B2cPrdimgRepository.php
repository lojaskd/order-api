<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPrdimgRepository
 * @package AppBundle\Repository
 */
class B2cPrdimgRepository extends EntityRepository
{

    /**
     * @param $productId
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get product listing image
     */
    public function getProductListingImage($productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $productListingImage = $queryBuilder
            ->select('img.nome')
            ->from('AppBundle:B2cPrdimg', 'img')
            ->where('img.idPrdprd = :productId')
            ->andWhere('img.tipo = :type')
            ->setParameter('productId', $productId)
            ->setParameter('type', 'lst')
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return (isset($productListingImage['nome']) ? $productListingImage['nome'] : '');
    }
}
