<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AttributesOptionsRepository
 * @package AppBundle\Repository
 */
class AttributesOptionsRepository extends EntityRepository
{

    /**
     * @param integer $attrId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get attribute options
     */
    public function getAttributeOptions($attrId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $options = $queryBuilder
            ->select(
                'opt.label',
                'opt.value'
            )
            ->from('AppBundle:AttributesOptions', 'opt')
            ->where('opt.idAttr = :attrId')
            ->setParameter('attrId', $attrId)
            ->getQuery()->getArrayResult();

        return $options;
    }
}
