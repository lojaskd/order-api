<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPedprdstatusRepository
 * @package AppBundle\Repository
 */
class B2cPedprdstatusRepository extends EntityRepository
{

    /**
     * @param $orderId
     * @param $productId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get product order status
     */
    public function getProductOrderStatus($orderId, $productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
                        ->select('pedprdstatus')
                        ->from('AppBundle:B2cPedprdstatus', 'pedprdstatus')
                        ->where('pedprdstatus.idPedped = :orderId')
                        ->andWhere('pedprdstatus.idPrdprd = :productId')
                        ->setParameter('orderId', $orderId)
                        ->setParameter('productId', $productId)
                        ->setMaxResults(1)
                        ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
