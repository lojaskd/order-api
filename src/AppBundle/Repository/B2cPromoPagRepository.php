<?php

namespace AppBundle\Repository;

use AppBundle\Entity\VO\Create\CreatePaymentVO;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPromoPagRepository
 * @package AppBundle\Repository
 */
class B2cPromoPagRepository extends EntityRepository
{

    /**
     * @param $promoId
     * @param CreatePaymentVO $payment
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Validate payment coupon
     */
    public function validatePaymentCoupon($promoId, CreatePaymentVO $payment)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('promoPag.idPagpag')
            ->from('AppBundle:B2cPromoPag', 'promoPag')
            ->leftJoin('AppBundle:B2cPagfor', 'pagFor', 'WITH', 'promoPag.idPagpag = pagFor.idPagpag')
            ->where('promoPag.idPromo = :promoId')
            ->andWhere('promoPag.idPagpag = :paymentId')
            ->andWhere('(pagFor.numParcelas = :instalment AND pagFor.valMinimo <= :amount) OR (promoPag.idPagfor = 0)')
            ->setParameter('promoId', $promoId)
            ->setParameter('paymentId', $payment->getId())
            ->setParameter('instalment', $payment->getInstalment())
            ->setParameter('amount', ($payment->getInstalment() * $payment->getInstalmentValue()))
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
