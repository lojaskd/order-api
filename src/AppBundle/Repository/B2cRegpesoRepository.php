<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cRegpesoRepository
 * @package AppBundle\Repository
 */
class B2cRegpesoRepository extends EntityRepository
{

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get max weight on store
     */
    public function getMaxWeight()
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $maxWeight = $queryBuilder
            ->select('MAX(wei.fimKg) as max_weight')
            ->from('AppBundle:B2cRegpeso', 'wei')
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return $maxWeight['max_weight'];
    }
}
