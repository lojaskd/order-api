<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\B2cPedpedMarketplace;
use AppBundle\Entity\VO\Create\CreateMarketPlaceVO;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class B2cPedpedMarketplaceRepository
 * @package AppBundle\Repository
 */
class B2cPedpedMarketplaceRepository extends EntityRepository
{

    /**
     * Saves Marketplace Order
     * @param B2cPedped $order
     * @param CreateMarketPlaceVO $marketplace
     * @return bool
     */
    public function insertOrderMarketplace(B2cPedped $order, CreateMarketPlaceVO $marketplace)
    {
        try {
            $newPedpedMarketplace = new B2cPedpedMarketplace();
            $newPedpedMarketplace->setIdPedped($order->getIdPedped());
            $newPedpedMarketplace->setIdMarketplace($marketplace->getId());
            $newPedpedMarketplace->setCodOrder($marketplace->getOrder());
            $newPedpedMarketplace->setIdChannel($marketplace->getChannelId());
            $newPedpedMarketplace->setPedSituacao($order->getPedSituacao());
            $newPedpedMarketplace->setDtaCad(new \DateTime('now'));

            if ($marketplace->getNewDeliveryDate() instanceof \Datetime) {
                $newPedpedMarketplace->setDeliveryDate($marketplace->getNewDeliveryDate());
            }

            // Persist the order log in database
            $this->_em->persist($newPedpedMarketplace);
            $this->_em->flush();
        } catch (\Exception $e) {
            throw new Exception('Error to insert marketplace order: '. $e->getMessage());
        }

        return true;
    }
}
