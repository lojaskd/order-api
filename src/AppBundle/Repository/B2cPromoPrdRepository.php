<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPromoPrdRepository
 * @package AppBundle\Repository
 */
class B2cPromoPrdRepository extends EntityRepository
{

    /**
     * @param $promoId
     * @param $productId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Validate product coupon
     */
    public function validateProductCoupon($promoId, $productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('promoPrd.idProduto')
            ->from('AppBundle:B2cPromoPrd', 'promoPrd')
            ->where('promoPrd.idPromo = :promoId')
            ->andWhere('promoPrd.idProduto = :productId')
            ->setParameter('promoId', $promoId)
            ->setParameter('productId', $productId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
