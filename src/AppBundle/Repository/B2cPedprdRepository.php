<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\B2cPedprd;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderVO;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class B2cPedprdRepository
 * @package AppBundle\Repository
 */
class B2cPedprdRepository extends EntityRepository
{

    /**
     * @param $product array
     * @param $pedprdstatus array
     * @return int
     *
     * Get product order status
     */
    public function getStatus($product, $pedprdstatus)
    {
        $status = 0;

        if (!empty($product['dataCancelamentoSalesforce'])) {
            $status = 10;
        } elseif ($pedprdstatus['dataEntR']) {
            $status = 9;
        } elseif ($pedprdstatus['dataCteR']) {
            $status = 8;
        } elseif ($pedprdstatus['dataEmbR']) {
            $status = 7;
        } elseif ($pedprdstatus['dataNfR']) {
            $status = 6;
        } elseif ($pedprdstatus['dataEntFornR']) {
            $status = 5;
        } elseif ($pedprdstatus['dataEntdFornR']) {
            $status = 4;
        } elseif ($pedprdstatus['dataFatFornR']) {
            $status = 3;
        } elseif ($pedprdstatus['dataLibR']) {
            $status = 2;
        } elseif ($pedprdstatus['dataAprR']) {
            $status = 1;
        }

        return $status;
    }

    /**
     * @param $pedprdstatus
     * @return float|int|null
     *
     * Get product production time
     */
    public function getProduction($pedprdstatus)
    {
        if ($pedprdstatus['dataEntFornR']) {
            return 0;
        } elseif ($pedprdstatus['dataEntdForn'] && $pedprdstatus['dataLibR']) {
            return ($pedprdstatus['dataEntdForn']->getTimestamp() - $pedprdstatus['dataLibR']->getTimestamp()) / 1000;
        }

        return null;
    }

    /**
     * @param $order
     * @param $product
     * @param $pedprdstatus
     * @param $status
     * @param $production
     * @param $polo
     * @param $branchOfficeInfo
     * @param null $shipper
     * @return array
     *
     * Format Product Info
     */
    public function formatProductInfo(
        $order,
        $product,
        $pedprdstatus,
        $status,
        $production,
        $polo,
        $branchOfficeInfo,
        $shipper = null,
        $invoices = array()
    )
    {
        $productInfo = array();

        $productInfo['status_produto'] = strval($status);
        $productInfo['producao'] = $production;
        $productInfo['data_aprovacao'] = ($pedprdstatus['dataAprR'] ? $pedprdstatus['dataAprR']->getTimestamp() : null);
        $productInfo['time_liberacao'] = ($pedprdstatus['dataLibR'] ? $pedprdstatus['dataLibR']->getTimestamp() : null);
        $productInfo['status_msg'] = $pedprdstatus['statusMsg'];
        $productInfo['data_apr'] = ($pedprdstatus['dataApr'] ? $pedprdstatus['dataApr']->format('Y-m-d') : null);
        $productInfo['data_apr_r'] = ($pedprdstatus['dataAprR'] ? $pedprdstatus['dataAprR']->format('Y-m-d') : null);
        $productInfo['data_lib'] = ($pedprdstatus['dataLib'] ? $pedprdstatus['dataLib']->format('Y-m-d') : null);
        $productInfo['data_lib_r'] = ($pedprdstatus['dataLibR'] ? $pedprdstatus['dataLibR']->format('Y-m-d') : null);
        $productInfo['data_fat_forn'] = $pedprdstatus['dataFatForn'] ? $pedprdstatus['dataFatForn']->format('Y-m-d') : null;
        $productInfo['data_fat_forn_r'] = $pedprdstatus['dataFatFornR'] ? $pedprdstatus['dataFatFornR']->format('Y-m-d') : null;
        $productInfo['data_entd_forn'] = $pedprdstatus['dataEntdForn'] ? $pedprdstatus['dataEntdForn']->format('Y-m-d') : null;
        $productInfo['data_entd_forn_r'] = $pedprdstatus['dataEntdFornR'] ? $pedprdstatus['dataEntdFornR']->format('Y-m-d') : null;
        $productInfo['data_ent_forn'] = $pedprdstatus['dataEntForn'] ? $pedprdstatus['dataEntForn']->format('Y-m-d') : null;
        $productInfo['data_ent_forn_r'] = $pedprdstatus['dataEntFornR'] ? $pedprdstatus['dataEntFornR']->format('Y-m-d') : null;
        $productInfo['data_nf'] = ($pedprdstatus['dataNf'] ? $pedprdstatus['dataNf']->format('Y-m-d') : null);
        $productInfo['data_nf_r'] = ($pedprdstatus['dataNfR'] ? $pedprdstatus['dataNfR']->format('Y-m-d') : null);
        $productInfo['data_emb'] = ($pedprdstatus['dataEmb'] ? $pedprdstatus['dataEmb']->format('Y-m-d') : null);
        $productInfo['data_emb_r'] = ($pedprdstatus['dataEmbR'] ? $pedprdstatus['dataEmbR']->format('Y-m-d') : null);
        $productInfo['data_cte'] = ($pedprdstatus['dataCte'] ? $pedprdstatus['dataCte']->format('Y-m-d') : null);
        $productInfo['data_cte_r'] = ($pedprdstatus['dataCteR'] ? $pedprdstatus['dataCteR']->format('Y-m-d') : null);
        $productInfo['data_ent'] = ($pedprdstatus['dataEnt'] ? $pedprdstatus['dataEnt']->format('Y-m-d') : null);
        $productInfo['data_ent_r'] = ($pedprdstatus['dataEntR'] ? $pedprdstatus['dataEntR']->format('Y-m-d') : null);
        $productInfo['id'] = intval($product['id']);
        $productInfo['id_pedped'] = $order['idPedped'];
        $productInfo['id_prdprd'] = $product['idPrdprd'];
        $productInfo['prd_nome'] = $product['prdNome'];
        $productInfo['prd_qtd'] = intval($product['prdQtd']);
        $productInfo['prd_data'] = ($product['prdData'] ? $product['prdData']->format('Y-m-d H:i:s') : '');
        $productInfo['prd_referencia'] = $product['prdReferencia'];
        $productInfo['prd_categoria'] = $product['prdCategoria'];
        $productInfo['prd_fornecedor'] = $product['prdFornecedor'];
        $productInfo['prd_forn_nome'] = $product['prdFornNome'];
        $productInfo['prd_prazo_forn'] = $product['prdPrazoForn'];
        $productInfo['prd_prazo_disp'] = $product['prdPrazoDisp'];
        $productInfo['prd_data_entrega_fornecedor'] = $product['prdDataEntregaFornecedor'] ? $product['prdDataEntregaFornecedor']->format('Y-m-d') : '';
        $productInfo['prd_destaque'] = $product['prdDestaque'];
        $productInfo['prd_peso'] = $product['prdPeso'];
        $productInfo['prd_estilo'] = $product['prdEstilo'];
        $productInfo['prd_presente'] = $product['prdPresente'];
        $productInfo['prd_stq_loja'] = $product['prdStqLoja'];
        $productInfo['prd_stq_fornecedor'] = $product['prdStqFornecedor'];
        $productInfo['prd_embalagem'] = $product['prdEmbalagem'];
        $productInfo['prd_texto_cartao'] = $product['prdTextoCartao'];
        $productInfo['prd_aro'] = $product['prdAro'];
        $productInfo['prd_imglistagem'] = $product['prdImglistagem'];
        $productInfo['prd_promo_desc'] = $product['prdPromoDesc'];
        $productInfo['bpromocao'] = $product['bpromocao'];
        $productInfo['prd_desc_perc'] = $product['prdDescPerc'];
        $productInfo['prd_cubagem'] = $product['prdCubagem'];
        $productInfo['prd_cor'] = $product['prdCor'];
        $productInfo['prd_sexo'] = $product['prdSexo'];
        $productInfo['prd_tamanho'] = $product['prdTamanho'];
        $productInfo['prd_pvreal'] = $product['prdPvreal'];
        $productInfo['prd_pvpromocao'] = $product['prdPvpromocao'];
        $productInfo['prd_vale_compra'] = $product['prdValeCompra'];
        $productInfo['prd_vnd_info'] = $product['prdVndInfo'];
        $productInfo['prd_desc_valor'] = $product['prdDescValor'];
        $productInfo['prd_pvtotal'] = $product['prdPvtotal'];
        $productInfo['prd_promo_fim'] = ($product['prdPromoFim'] ? $product['prdPromoFim']->format('Y-m-d') : null);
        $productInfo['prd_custo'] = $product['prdCusto'];
        $productInfo['prd_vnd_aberto'] = $product['prdVndAberto'];
        $productInfo['prd_vnd_junta'] = $product['prdVndJunta'];
        $productInfo['prd_promo_ini'] = ($product['prdPromoIni'] ? $product['prdPromoIni']->format('Y-m-d') : null);
        $productInfo['sf_opportunitylineitem'] = $product['sfOpportunitylineitem'];
        $productInfo['prd_data_entrega_fornecedor_realizada'] = $product['prdDataEntregaFornecedorRealizada'] ? $product['prdDataEntregaFornecedorRealizada']->format('Y-m-d') : null;
        $productInfo['id_notafiscal'] = $product['idNotafiscal'];
        $productInfo['id_tipo_ocorrencia'] = $product['idTipoOcorrencia'];
        $productInfo['nome_ocorrencia'] = $product['nomeOcorrencia'];
        $productInfo['grupo_ocorrencia'] = $product['grupoOcorrencia'];
        $productInfo['data_ocorrencia'] = $product['dataOcorrencia'] ? $product['dataOcorrencia']->format('Y-m-d') : null;
        $productInfo['item_cancelado'] = $product['itemCancelado'];
        $productInfo['data_cancelamento_salesforce'] = $product['dataCancelamentoSalesforce'] ? $product['dataCancelamentoSalesforce']->format('Y-m-d') : null;
        $productInfo['ordem_compra'] = $product['ordemCompra'];
        $productInfo['data_prev_entrega_forn'] = $product['dataPrevEntregaForn'] ? $product['dataPrevEntregaForn']->format('Y-m-d') : null;
        $productInfo['cluster_rmkt'] = $product['clusterRmkt'];
        $productInfo['data_ordem_compra'] = $product['dataOrdemCompra'];
        $productInfo['id_filial_estoque'] = $product['idFilialEstoque'];
        $productInfo['estoque_acao'] = $this->getEntityManager()->getRepository('AppBundle:B2cViewProdutos')
            ->getMinStockAction($pedprdstatus['idPrdprd']);
        $productInfo['id_filial'] = (int)$branchOfficeInfo['id_filial'];
        $productInfo['id_filial_sap'] = (int)$branchOfficeInfo['id_filial_sap'];
        $productInfo['entrega_imediata'] = (bool)$branchOfficeInfo['entrega_imediata'];
        $productInfo['polo'] = $polo;
        $productInfo['cod_transportador_coleta'] = $product['codigoTransportadorColeta'];

        if ($shipper) {
            $productInfo['nome_transportadora'] = $shipper['nomeTransportadora'];
            $productInfo['hub'] = $shipper['hub'];
        }

        if (!empty($invoices)) {
            $productInfo['invoices'] = $invoices;
        }

        $productInfo['destino'] = $product['destination'];
        $productInfo['tipo_venda_produto'] = $product['tipoVendaProduto'];
        $productInfo['prd_valor_real_pago'] = $product['prdValorRealPago'];
        $productInfo['prd_frete_real_pago'] = $product['prdFreteRealPago'];

        return $productInfo;
    }

    /**
     * @param B2cPedped $order
     * @param CreateProductVO $product
     * @return B2cPedprd
     *
     * Generate product order on b2c_pedprd
     */
    public function generateProductsOrder(B2cPedped $order, CreateProductVO $product)
    {
        try {
            $newProductOrder = new B2cPedprd();
            $newProductOrder->setIdPedped($order);
            $newProductOrder->setIdPrdprd($product->getId());
            $newProductOrder->setPrdNome($product->getName());
            $newProductOrder->setPrdQtd($product->getQuantity());
            $newProductOrder->setPrdData(new \Datetime('now'));
            $newProductOrder->setPrdReferencia($product->getReference());
            $newProductOrder->setPrdCategoria($product->getCategory());
            $newProductOrder->setPrdFornecedor($product->getProvider());
            $newProductOrder->setPrdFornNome($product->getProviderName());

            /* Set order product dates */
            $newProductOrder->setPrdPrazoForn($product->getProviderDeadline());
            $newProductOrder->setPrdPrazoDisp($product->getAvailabilityDeadline());
            $newProductOrder->setPrdDataEntregaFornecedor($product->getProviderDeliveryDate());

            $newProductOrder->setPrdDestaque($product->isFeatured());
            $newProductOrder->setPrdPeso($product->getWeight() * $product->getQuantity());
            $newProductOrder->setPrdEstilo($product->getStyle());
            $newProductOrder->setPrdPresente(0);
            $newProductOrder->setPrdStqLoja($product->getProductStock());
            $newProductOrder->setPrdStqFornecedor($product->getProviderStock());
            $newProductOrder->setPrdStqFornecedorReservado($product->getProviderStockReserved());
            $newProductOrder->setPrdStqMarketplace(null);
            $newProductOrder->setPrdEmbalagem(0);
            $newProductOrder->setPrdTextoCartao('');
            $newProductOrder->setPrdAro('');
            $newProductOrder->setPrdImglistagem($product->getImage());
            $newProductOrder->setPrdPromoDesc('');
            $newProductOrder->setBpromocao($product->isPromotion());
            $newProductOrder->setPrdDescPerc($product->getDiscountPercentage());
            $newProductOrder->setPrdDescValor($product->getDiscountValue());
            $newProductOrder->setPrdCubagem($product->getCubage() * $product->getQuantity());
            $newProductOrder->setPrdCor($product->getColorName());
            $newProductOrder->setPrdSexo($product->getGenre());
            $newProductOrder->setPrdTamanho($product->getSize());
            $newProductOrder->setPrdPvreal($product->getRealPrice());
            $newProductOrder->setPrdPvpromocao($product->getPromotionPrice());
            $newProductOrder->setPrdCusto($product->getCostPrice() ? $product->getCostPrice() : 0);
            $newProductOrder->setPrdValeCompra(0);
            $newProductOrder->setPrdVndInfo('');
            $newProductOrder->setPrdPvtotal($product->getTotalprice());
            $newProductOrder->setPrdPromoIni($product->getInitialPromotion());
            $newProductOrder->setPrdPromoFim($product->getFinalPromotion());
            $newProductOrder->setPrdVndAberto(0);
            $newProductOrder->setPrdVndJunta(0);
            $newProductOrder->setItemCancelado(0);
            $newProductOrder->setIdFilialEstoque(0);
            $newProductOrder->setCodigoTransportadorColeta($product->getCollectionConveyorCode());
            $newProductOrder->setPathXmlOc($product->getPathXmlOc());

            $salesProductType = $this->getSalesProductType($product->getId());
            $newProductOrder->setTipoVendaProduto($salesProductType['respostas']);

        } catch (\Exception $e) {
            throw new Exception('Error to insert product order: ' . $e->getMessage());
        }

        return $newProductOrder;
    }

    /**
     * @param $orderId
     * @param $productId
     * @param $storeStock
     * @param $providerStock
     * @param $branchOfficeId
     * @return bool
     *
     * Set product stock info on b2c_pedprd
     */
    public function setStockInfo($orderId, $productId, $storeStock, $providerStock, $branchOfficeId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedprd', 'pedprd')
            ->set('pedprd.prdStqLoja', $queryBuilder->expr()->literal($storeStock))
            ->set('pedprd.prdStqFornecedor', $queryBuilder->expr()->literal($providerStock))
            ->set('pedprd.idFilialEstoque', $queryBuilder->expr()->literal($branchOfficeId))
            ->where('pedprd.idPedped = ?1')
            ->andWhere('pedprd.idPrdprd = ?2')
            ->setParameter(1, $orderId)
            ->setParameter(2, $productId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $orderId
     * @param $productId
     * @param $marketPlaceStock
     * @return bool
     *
     * Set MarketPlace product stock info on b2c_pedprd
     */
    public function setMarketplaceStockInfo($orderId, $productId, $marketPlaceStock)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedprd', 'pedprd')
            ->set('pedprd.prdStqMarketplace', $queryBuilder->expr()->literal($marketPlaceStock))
            ->where('pedprd.idPedped = ?1')
            ->andWhere('pedprd.idPrdprd = ?2')
            ->setParameter(1, $orderId)
            ->setParameter(2, $productId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $orderId
     * @param $productId
     * @param $providerDays
     * @param $availabilityDays
     * @param $providerDeadline
     * @return bool
     *
     * Set MarketPlace product stock info on b2c_pedprd
     */
    public function setNewDeadline($orderId, $productId, $providerDays, $availabilityDays, $providerDeadline)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedprd', 'pedprd')
            ->set('pedprd.prdPrazoForn', $queryBuilder->expr()->literal($providerDays))
            ->set('pedprd.prdPrazoDisp', $queryBuilder->expr()->literal($availabilityDays))
            ->set('pedprd.prdDataEntregaFornecedor', $queryBuilder->expr()->literal($providerDeadline))
            ->where('pedprd.idPedped = ?1')
            ->andWhere('pedprd.idPrdprd = ?2')
            ->setParameter(1, $orderId)
            ->setParameter(2, $productId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $orderId
     * @return array
     *
     * Get products order stock
     */
    public function getOrderProductsStock($orderId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('ped.idPedped', 'ped.idClicli', 'ped.entNome', 'ped.entSobrenome', 'ped.pedSituacao')
            ->from('AppBundle:B2cPedped', 'ped')
            ->leftJoin('ped.products', 'pedprd')
            ->leftJoin('AppBundle:B2cPrdprd', 'prd', 'WITH', 'prd.id = pedprd.idPrdprd')
            ->leftJoin('AppBundle:B2cViewProdutos', 'viewprd', 'WITH', 'viewprd.id = prd.id')
            ->where('prd.estoque = 0 AND prd.stqfornecedor = 0')
            ->orWhere('viewprd.estoque = 0 AND viewprd.stqfornecedor = 0')
            ->andWhere('ped.idPedped = :orderId')
            ->setParameter('orderId', $orderId)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param $orderId
     * @param $prdId
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get branch office information
     */
    public function getBranchOfficeInformation($orderId, $prdId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('ped.idPedped', 'sapitem.idFilial as id_filial_sap')
            ->distinct()
            ->addSelect('CASE
                    WHEN
                        (prd.prdStqLoja > 0
                            AND prd.prdPrazoForn = 0
                            AND prd.idFilialEstoque > 0)
                    THEN
                        prd.idFilialEstoque
                    ELSE IFNULL(filial.idFilial, 5)
                END AS id_filial')
            ->addSelect('CASE
                    WHEN
                        (prd.prdStqLoja > 0
                            AND prd.prdPrazoForn = 0
                            AND prd.idFilialEstoque > 0)
                    THEN
                        1
                    ELSE 0
                END AS entrega_imediata')
            ->from('AppBundle:B2cPedped', 'ped')
            ->innerJoin('ped.products', 'prd')
            ->leftJoin('AppBundle:CidadesIbge', 'ci', 'WITH', 'ci.codIbge = ped.entCodIbge AND ped.entCodIbge > 0')
            ->leftJoin('AppBundle:Tblsappedidoitem', 'sapitem', 'WITH', 'sapitem.idPedido = ped.idPedped and sapitem.idItem = prd.prdReferencia')
            ->leftJoin(
                'AppBundle:B2cFornecedorRegiao',
                'filial',
                'WITH',
                'filial.idForfor = prd.prdFornecedor AND filial.idRegreg = ci.idRegreg'
            )
            ->where('ped.idPedped = :orderId')
            ->andWhere('prd.idPrdprd = :prdId')
            ->setParameter('orderId', $orderId)
            ->setParameter('prdId', $prdId)
            ->getQuery()->getSingleResult();
    }

    /**
     * @param OrderVO $orderVO array
     * @return array
     *
     * Get max products dates
     */
    public function getProductsDatesByStatus(OrderVO $orderVO)
    {
        //total products of order
        $totalProducts = 0;
        //total products available of order
        $totalProductsAvail = 0;
        //total products cancelled
        $totalProductsCancelled = 0;
        //control array
        $arrDates = array(
            'approvalDate' => array(
                'field' => 'dataAprR',
                'dates' => array(),
            ),
            'releaseDate' => array(
                'field' => 'dataLibR',
                'dates' => array(),
            ),
            'supplierBillingDate' => array(
                'field' => 'dataFatFornR',
                'dates' => array(),
            ),
            'supplierInvoiceDate' => array(
                'field' => 'dataEntdFornR',
                'dates' => array(),
            ),
            'supplierDeliveryDate' => array(
                'field' => 'dataEntFornR',
                'dates' => array(),
            ),
            'invoiceDate' => array(
                'field' => 'dataNfR',
                'dates' => array(),
            ),
            'shippingDate' => array(
                'field' => 'dataEmbR',
                'dates' => array(),
            ),
            'deliveryDate' => array(
                'field' => 'dataEntR',
                'dates' => array(),
            )
        );
        //for each product
        /* @var $product OrderProductVO */
        foreach ($orderVO->getProducts() as $product) {
            //increment total of products
            $totalProducts++;
            //check cancel
            if (!$product->isCancelled()) {
                //increment totalAvail of products
                $totalProductsAvail++;
                //get prd status
                $pedprdstatus = $this->_em->getRepository('AppBundle:B2cPedprdstatus')
                    ->getProductOrderStatus($orderVO->getOrderId(), $product->getProductId());
                //foreach control array
                foreach ($arrDates as $key => $type) {
                    //check date
                    if (isset($pedprdstatus[$type['field']]) && $pedprdstatus[$type['field']] instanceof \DateTime) {
                        //insert date
                        $arrDates[$key]['dates'][] = $pedprdstatus[$type['field']];
                    }
                }
            } else {
                $totalProductsCancelled++;
            }
        }

        return array($totalProducts, $totalProductsAvail, $totalProductsCancelled, $arrDates);
    }

    /**
     * @param $orderId
     * @param $productId
     * @return bool
     *
     * Cancel item by order
     */
    public function cancelOrderProduct($orderId, $productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPedprd', 'pedprd')
            ->set('pedprd.itemCancelado', 1)
            ->set('pedprd.dataCancelamento', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->where('pedprd.idPedped = ?1')
            ->andWhere('pedprd.idPrdprd = ?2')
            ->andWhere('pedprd.itemCancelado = 0')
            ->setParameter(1, $orderId)
            ->setParameter(2, $productId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param $orderId
     * @param $productId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get order by orderId and productId
     */
    public function getByOrderAndProduct($orderId, $productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('pedprd', 'pedped')
            ->from('AppBundle:B2cPedprd', 'pedprd')
            ->innerJoin('pedprd.idPedped', 'pedped')
            ->where('pedprd.idPedped = :id_pedido')
            ->andWhere('pedprd.idPrdprd = :id_produto')
            ->setParameter('id_pedido', $orderId)
            ->setParameter('id_produto', $productId)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * Set a destination for an specific product.
     *
     * @param $orderId
     * @param $productId
     * @param $destination
     * @return bool
     */
    public function setDestination($orderId, $productId, $destination)
    {
        $queryBuilder = $this->_em->createQueryBuilder();
        $queryBuilder->update('AppBundle:B2cPedprd', 'pedprd')
            ->set('pedprd.destination', $queryBuilder->expr()->literal($destination))
            ->where('pedprd.idPedped = :orderId')
            ->andWhere('pedprd.idPrdprd = :productId')
            ->setParameter('orderId', $orderId)
            ->setParameter('productId', $productId)
            ->getQuery()
            ->execute();
        return true;
    }

    /**
     * @param $productId
     * @return mixed
     *
     * Search the type of product sale
     */
    public function getSalesProductType($productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        if (!$productId) {
            return false;
        }

        return $queryBuilder
            ->select('atrv.respostas')
            ->from('AppBundle:B2cAtributo', 'atr')
            ->innerJoin('AppBundle:B2cAtributoValor', 'atrv' , 'WITH' , 'atrv.idAtributo = atr.idAtributo')
            ->where(' atr.atributoGrupo = 36  AND atr.nomeAtributo = \'MarketPlace LKD\' ')

            ->andWhere('atrv.b2cPrdprd = :productId')
            ->setParameter('productId', $productId)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param $orderId
     * @param $productId
     * @return bool
     *
     * Updates cancellation date
     */
    public function updateCancellationDate($orderId, $productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();
        $queryBuilder->update('AppBundle:B2cPedprd', 'pedprd')
            ->set('pedprd.dataCancelamento', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->where('pedprd.idPedped = :orderId')
            ->andWhere('pedprd.idPrdprd = :productId')
            ->andWhere('pedprd.itemCancelado = 1')
            ->setParameter('orderId', $orderId)
            ->setParameter('productId', $productId)
            ->getQuery()->execute();

        return true;
    }

    /**
     * @param integer $orderId
     * @param string $productReference
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOrderProductFromReference($orderId, $productReference)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('pedprd', 'status')
            ->from('AppBundle:B2cPedprd', 'pedprd')
            ->leftJoin('AppBundle:B2cPedprdstatus', 'status', 'WITH', 'pedprd.idPedped = status.idPedped and pedprd.idPrdprd = status.idPrdprd')
            ->where('pedprd.idPedped = ?1')
            ->andWhere('pedprd.prdReferencia = ?2')
            ->setParameter(1, $orderId)
            ->setParameter(2, $productReference)
            ->getQuery()->getResult();
    }

    /**
     * Update invoice ID on b2c_pedprd
     *
     * @param $orderId
     * @param $productReference
     * @param $invoiceId
     * @return bool
     */
    public function updateProductInvoice($orderId, $productReference, $invoiceId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();
        $queryBuilder->update('AppBundle:B2cPedprd', 'pedprd')
            ->set('pedprd.idNotafiscal', $queryBuilder->expr()->literal($invoiceId))
            ->where('pedprd.idPedped = :orderId')
            ->andWhere('pedprd.prdReferencia = :productReference')
            ->setParameter('orderId', $orderId)
            ->setParameter('productReference', $productReference)
            ->getQuery()->execute();

        return true;
    }
}
