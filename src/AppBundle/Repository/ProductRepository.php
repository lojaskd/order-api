<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Validate\ValidateProduct;

/**
 * Class ProductRepository
 * @package AppBundle\Repository
 */
class ProductRepository extends EntityRepository
{

    /**
     * @param array $products
     * @return array
     *
     * Get products request info
     */
    public function getProductRequestInfo($products)
    {
        $productsReturn = array();

        foreach ($products as $product) {
            /* @var $product ValidateProduct */
            $productInfo = array();
            $productInfo['id'] = $product->getId();
            $productInfo['kitId'] = $product->getKitId();
            $productInfo['quantity'] = $product->getQuantity();
            $productInfo['price'] = $product->getPrice();

            $productsReturn[] = $productInfo;
        }

        return $productsReturn;
    }
}
