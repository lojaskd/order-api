<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cPedlog;
use AppBundle\Entity\VO\Create\CreateOrderLogVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class B2cPedlogRepository
 * @package AppBundle\Repository
 */
class B2cPedlogRepository extends EntityRepository
{

    /**
     * @param UpdateOrderStatusVO $updateOrderStatusVO
     * @return bool
     *
     * Insert update status order log on database
     */
    public function insertUpdateStatusLog(UpdateOrderStatusVO $updateOrderStatusVO)
    {
        try {
            $newOrderLog = new B2cPedlog();

            $newOrderLog->setIdPedped($updateOrderStatusVO->getOrderId());
            $newOrderLog->setTitulo('Pedido atualizado por: ' . $updateOrderStatusVO->getClerk());

            $msg = '';

            if ($updateOrderStatusVO->getStatus()) {
                $status = $this->_em->getRepository('AppBundle:B2cPedsituacao')->findOneBy(array(
                    'id' => $updateOrderStatusVO->getStatus()
                ));

                if ($status) {
                    $msg .='Status do Pedido: ' . $status->getName();
                }
            }

            if ($updateOrderStatusVO->getInternalStatus()) {
                $internalStatus = $this->_em->getRepository('AppBundle:B2cStaint')->findOneBy(array(
                    'id' => $updateOrderStatusVO->getInternalStatus()
                ));

                if ($internalStatus) {
                    $msg .=PHP_EOL . 'Status interno: ' . $internalStatus->getNome();
                }
            }

            $newOrderLog->setTexto($msg);
            $newOrderLog->setData(new \DateTime('now'));
            $newOrderLog->setMostrarAreavip(0);
            $newOrderLog->setStatusAtual($updateOrderStatusVO->getStatus());
            $newOrderLog->setStatusInternosAtual($updateOrderStatusVO->getInternalStatus());

            // Persist the order log in database
            $this->_em->persist($newOrderLog);
            $this->_em->flush();

            return true;
        } catch (\Exception $e) {
            throw new Exception('Error to insert update order status log: ' . $e->getMessage());
        }
    }

    /**
     * @param OrderVO $orderVO
     * @param $orderDeliveryDate
     * @param $productsDeadlines
     * @return bool
     *
     * Insert deadline order log on database
     */
    public function insertOrderDeadlineLog(OrderVO $orderVO, $orderDeliveryDate, $productsDeadlines)
    {
        try {
            $newOrderLog = new B2cPedlog();

            $newOrderLog->setIdPedped($orderVO->getOrderId());
            $newOrderLog->setTitulo('Atualiza&ccedil;&atilde;o de prazo');

            $msg = '';

            if ($orderVO->getPredictDeliveryDate() instanceof \DateTime) {
                $msg = "Data prevista de entrega anterior: " . $orderVO->getPredictDeliveryDate()->format('d/m/Y') . "<br>";
            }

            if ($orderDeliveryDate instanceof \DateTime) {
                $msg.= "Data prevista de entrega atualizada: " . $orderDeliveryDate->format('d/m/Y') . "<br><br>";
            }
            
            foreach ($productsDeadlines as $productDeadlines) {
                foreach ($productDeadlines as $deadLineType => $productDeadline) {
                    $msg.= $deadLineType . ": " . $productDeadline . '<br>';
                }
                $msg.= "---------------------------------------------<br>";
            }

            $newOrderLog->setTexto($msg);
            $newOrderLog->setData(new \DateTime('now'));
            $newOrderLog->setMostrarAreavip(0);
            $newOrderLog->setStatusAtual($orderVO->getStatus()->getId());
            $newOrderLog->setStatusInternosAtual($orderVO->getInternalStatus()->getId());

            // Persist the order log in database
            $this->_em->persist($newOrderLog);
            $this->_em->flush();

            return true;
        } catch (\Exception $e) {
            throw new Exception('Error to insert update order status log: ' . $e->getMessage());
        }
    }

    /**
     * @param CreateOrderLogVO $orderLogVO
     * @return bool
     *
     * Insert order log on database
     */
    public function insertOrderLog(CreateOrderLogVO $orderLogVO)
    {
        try {
            $newOrderLog = new B2cPedlog();

            $newOrderLog->setIdPedped($orderLogVO->getOrderId());
            $newOrderLog->setData($orderLogVO->getCreatedAt());
            $newOrderLog->setTitulo($orderLogVO->getTitle());
            $newOrderLog->setTexto($orderLogVO->getText());
            $newOrderLog->setMostrarAreavip($orderLogVO->isShow());
            $newOrderLog->setStatusAtual($orderLogVO->getStatus());
            $newOrderLog->setStatusInternosAtual($orderLogVO->getInternalStatus());

            // Persist the order log in database
            $this->_em->persist($newOrderLog);
            $this->_em->flush();

            return true;
        } catch (\Exception $e) {
            throw new Exception('Error to insert order log: ' . $e->getMessage());
        }
    }
}
