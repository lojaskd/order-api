<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPrdfreteRepository
 * @package AppBundle\Repository
 */
class B2cPrdfreteRepository extends EntityRepository
{

    /**
     * @param $regionId
     * @param $productsId
     * @return array
     * Get shipping product value
     */
    public function getShippingProduct($regionId, $productsId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $shippingValue = $queryBuilder
            ->select('frete.valor as price, frete.idPrdprd as productId')
            ->from('AppBundle:B2cPrdfrete', 'frete')
            ->where($queryBuilder->expr()->in('frete.idPrdprd', implode(', ', $productsId)))
            ->andWhere('frete.idRegreg = :regionId')
            ->andWhere('frete.valor > 0 ')
            ->setParameter('regionId', $regionId)
            ->getQuery()->getArrayResult();

        return $shippingValue;
    }
}
