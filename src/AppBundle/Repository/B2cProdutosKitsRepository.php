<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cProdutosKitsRepository
 * @package AppBundle\Repository
 */
class B2cProdutosKitsRepository extends EntityRepository
{

    /**
     * @param $productId
     * @param $kitId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get product kit
     */
    public function getProductKit($productId, $kitId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('prdKit.idProduto', 'prdKit.quantidade', 'prdKit.desconto')
            ->from('AppBundle:B2cProdutosKits', 'prdKit')
            ->where('prdKit.idProduto = :productId')
            ->andWhere('prdKit.idKit = :kitId')
            ->andWhere('prdKit.obrigatorio = 1')
            ->setParameter('productId', $productId)
            ->setParameter('kitId', $kitId)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param $productId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get product kit, based in relation with B2cViewProdutos
     */
    public function getRelatedKitByProductId($productId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select("prdKit")
            ->from('AppBundle:B2cProdutosKits', 'prdKit')
            ->leftJoin('AppBundle:B2cViewProdutos', "viewProd", "WITH", "viewProd.id = prdKit.idKit")
            ->where('prdKit.idProduto = :productId')
            ->andWhere('viewProd.id is not null')
            ->setParameter('productId', $productId)
            ->getQuery()->getResult();
    }

}
