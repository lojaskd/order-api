<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cPedprdEstorno;
use AppBundle\Entity\VO\Update\UpdateProductsStatusVO;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class B2cPedprdEstornoRepository
 * @package AppBundle\Repository
 */
class B2cPedprdEstornoRepository extends EntityRepository
{

    /**
     * Saves Reversal Details
     *
     * @param UpdateProductsStatusVO $updateProductStatusVO
     * @param integer $productId
     * @param float $productAmount
     * @param float $shippingAmount
     *
     * @return B2cPedprdEstorno
     * @throws Exception if some error occurs on trying to insert the reversal information
     *
     */
    public function insertOrderProductReversal(
        UpdateProductsStatusVO $updateProductStatusVO,
        $productId,
        $productAmount = null,
        $shippingAmount = null
    ) {
        try {
            $newPedprdEstorno = new B2cPedprdEstorno();
            $newPedprdEstorno->setIdPedped($updateProductStatusVO->getOrderId());
            $newPedprdEstorno->setIdPrdprd($productId);
            $newPedprdEstorno->setTicket($updateProductStatusVO->getTicket());
            $newPedprdEstorno->setDtaCad(new \DateTime('now'));
            $newPedprdEstorno->setValorProduto($productAmount);
            $newPedprdEstorno->setValorFrete($shippingAmount);
            $newPedprdEstorno->setEstornoLiberado(1);
            $newPedprdEstorno->setOcrCode($updateProductStatusVO->getOcrCode());
            $newPedprdEstorno->setCancellationIndicator($updateProductStatusVO->getCancellationIndicator());
            $newPedprdEstorno->setTicketType($updateProductStatusVO->getTicketType());
            $newPedprdEstorno->setHub($updateProductStatusVO->getHub());
            $newPedprdEstorno->setShipper($updateProductStatusVO->getShipper());
            $newPedprdEstorno->setInvoice($updateProductStatusVO->getInvoice());
            $newPedprdEstorno->setProductCauser($updateProductStatusVO->getProductCauser());
            $newPedprdEstorno->setIdForest($updateProductStatusVO->getRefundForm());

            // Persist the order in database
            $this->_em->persist($newPedprdEstorno);
            $this->_em->flush();
        } catch (\Exception $e) {
            throw new Exception('Error to insert order reversal: ' . $e->getMessage());
        }

        return $newPedprdEstorno;
    }
}
