<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPromoRegRepository
 * @package AppBundle\Repository
 */
class B2cPromoRegRepository extends EntityRepository
{

    /**
     * @param $promoId
     * @param $regionId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Validate delivery coupon
     */
    public function validateDeliveryCoupon($promoId, $regionId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('promoReg.idRegiao')
            ->from('AppBundle:B2cPromoReg', 'promoReg')
            ->where('promoReg.idPromo = :promoId')
            ->andWhere('promoReg.idRegiao = :regionId')
            ->setParameter('promoId', $promoId)
            ->setParameter('regionId', $regionId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
