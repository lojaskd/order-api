<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Class B2cPromoRepository
 * @package AppBundle\Repository
 */
class B2cPromoRepository extends EntityRepository
{

    /**
     * @param $region
     * @param $customerId
     * @param $productsId
     * @param $paymentsId
     * @param $providersId
     * @param $subtotal
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get active shipping promotions
     */
    public function getActiveShippingPromotions($region, $customerId, $productsId, $paymentsId, $providersId, $subtotal)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('AppBundle\Entity\B2cPromo', 'promo');
        $rsm->addFieldResult('promo', 'id_promo', 'idPromo');
        $rsm->addFieldResult('promo', 'frete_gratis', 'freteGratis');
        $rsm->addFieldResult('promo', 'frete_fixo', 'freteFixo');
        $rsm->addFieldResult('promo', 'frete_fixo_valor', 'freteFixoValor');
        $rsm->addFieldResult('promo', 'usar_categoria', 'usarCategoria');

        $dql =  "SELECT
                    promo.id_promo,
                    promo.nome,
                    promo.frete_gratis,
                    promo.frete_fixo,
                    promo.frete_fixo_valor,
                    promo.usar_categoria,
                    IF(promo.aplicar_entrega = 1, (SELECT count(reg.id_regiao) FROM b2c_promo_reg reg where reg.id_regiao = {$region} AND reg.id_promo = promo.id_promo), 1) as delivery,
                    IF(promo.aplicar_cliente = 1, (SELECT count(cli.id_cliente) FROM b2c_promo_cli cli where cli.id_cliente = {$customerId} AND cli.id_promo = promo.id_promo), 1) as customer,
                    IF(promo.usar_produtos = 1, (SELECT count(prd.id_produto) FROM b2c_promo_prd prd where prd.id_produto IN ({$productsId}) AND prd.id_promo = promo.id_promo), :countProducts) as product,
                    IF(promo.aplicar_pagamento = 1, (SELECT count(pag.id_pagpag) FROM b2c_promo_pag pag where pag.id_pagpag IN ({$paymentsId}) AND pag.id_promo = promo.id_promo), :countPayments) as payment,
                    IF(promo.usar_fornecedor = 1, (SELECT count(forn.id_fornecedor) FROM b2c_promo_for forn where forn.id_fornecedor IN ({$providersId}) AND forn.id_promo = promo.id_promo), :countProviders) as provider
                FROM
                    b2c_promo AS promo
                WHERE
                    promo.ativo = 1
                    AND NOW() BETWEEN
                        CAST(CONCAT(promo.dta_validade_inicio, ' ', promo.hra_validade_inicio) AS DATETIME)
                        AND CAST(CONCAT(promo.dta_validade_fim, ' ', promo.hra_validade_fim) AS DATETIME)
                    AND promo.vale_compra = 0
                    AND promo.promocao_brinde = 0
                    AND (promo.frete_gratis = 1 OR promo.frete_fixo = 1)
                    AND promo.valor_minimo <= :subtotal
                GROUP BY promo.id_promo
                HAVING (
                    delivery > 0
                    AND customer > 0
                    AND product = :countProducts
                    AND payment = :countPayments
                    AND provider = :countProviders
                    )
                ORDER BY promo.prioridade DESC";

        $promotions = $this->_em->createNativeQuery($dql, $rsm)
            ->setParameter('subtotal', $subtotal)
            ->setParameter('countProducts', count(array_unique(explode(', ', $productsId))))
            ->setParameter('countPayments', count(array_unique(explode(', ', $paymentsId))))
            ->setParameter('countProviders', count(array_unique(explode(', ', $providersId))))
            ->getArrayResult();

        return $promotions;
    }

    /**
     * @param $discountCode
     * @param $source
     * @param $medium
     * @param $content
     * @param $regionId
     * @param $customerId
     * @param $productsId
     * @param $paymentsId
     * @param $categoriesId
     * @param $providersId
     * @param $subtotal
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Validate coupon from code
     */
    public function getValidCouponFromCode($discountCode, $source, $medium, $content, $regionId, $customerId, $productsId, $paymentsId, $categoriesId, $providersId, $subtotal)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('AppBundle\Entity\B2cPromo', 'promo');
        $rsm->addFieldResult('promo', 'id_promo', 'idPromo');
        $rsm->addFieldResult('promo', 'frete_gratis', 'freteGratis');
        $rsm->addFieldResult('promo', 'frete_fixo', 'freteFixo');
        $rsm->addFieldResult('promo', 'frete_fixo_valor', 'freteFixoValor');
        $rsm->addFieldResult('promo', 'desconto_perc', 'descontoPerc');
        $rsm->addFieldResult('promo', 'desconto_valor', 'descontoValor');
        $rsm->addFieldResult('promo', 'promo_inclui_frete', 'promoIncluiFrete');
        $rsm->addFieldResult('promo', 'aplicar_produtos', 'aplicarProdutos');
        $rsm->addFieldResult('promo', 'aplicar_pagamento', 'aplicarPagamento');
        $rsm->addFieldResult('promo', 'aplicar_entrega', 'aplicarEntrega');
        $rsm->addFieldResult('promo', 'usar_categoria', 'usarCategoria');
        $rsm->addFieldResult('promo', 'usar_fornecedor', 'usarFornecedor');

        $dql =  "SELECT
                    promo.id_promo,
                    promo.frete_gratis,
                    promo.frete_fixo,
                    promo.frete_fixo_valor,
                    promo.desconto_perc,
                    promo.desconto_valor,
                    promo.promo_inclui_frete,
                    promo.aplicar_produtos,
                    promo.aplicar_pagamento,
                    promo.aplicar_entrega,
                    promo.usar_categoria,
                    promo.usar_fornecedor,
                    IF(promo.aplicar_entrega = 1, (SELECT COUNT(reg.id_regiao) FROM b2c_promo_reg reg where reg.id_regiao = {$regionId} AND reg.id_promo = promo.id_promo), 1) as delivery,
                    IF(promo.aplicar_cliente = 1, (SELECT COUNT(cli.id_cliente) FROM b2c_promo_cli cli where cli.id_cliente = {$customerId} AND cli.id_promo = promo.id_promo), 1) as customer,
                    IF(promo.aplicar_produtos = 1, (SELECT COUNT(prd.id_produto) FROM b2c_promo_prd prd where prd.id_produto IN ({$productsId}) AND prd.id_promo = promo.id_promo), :countProducts) as product,
                    IF(promo.aplicar_pagamento = 1, (SELECT COUNT(pag.id_pagpag) FROM b2c_promo_pag pag where pag.id_pagpag IN ({$paymentsId}) AND pag.id_promo = promo.id_promo), :countPayments) as payment,
                    IF(promo.usar_categoria = 1, (SELECT COUNT(cat.id_categoria) FROM b2c_promo_cat cat where cat.id_categoria IN ({$categoriesId}) AND cat.id_promo = promo.id_promo), :countCategories) as category,
                    IF(promo.usar_fornecedor = 1, (SELECT COUNT(forn.id_fornecedor) FROM b2c_promo_for forn where forn.id_fornecedor IN ({$providersId}) AND forn.id_promo = promo.id_promo), :countProviders) as provider
                FROM
                    b2c_promo AS promo
                    INNER JOIN b2c_promo_vale vale ON vale.id_promo = promo.id_promo
                    LEFT JOIN b2c_promo_utm utm ON utm.id_promo = promo.id_promo
                WHERE
                    promo.ativo = 1
                    AND NOW() BETWEEN
                        CAST(CONCAT(promo.dta_validade_inicio, ' ', promo.hra_validade_inicio) AS DATETIME)
                        AND CAST(CONCAT(promo.dta_validade_fim, ' ', promo.hra_validade_fim) AS DATETIME)
                    AND promo.vale_compra = 1
                    AND promo.valor_minimo <= :subtotal
                    AND vale.codigo = '{$discountCode}'
                    AND vale.situacao IN ('N', 'E')
                    AND (
                            (utm.id_promo_utm IS NULL)
                                OR
                            (
                                utm.id_promo_utm IS NOT NULL
                                AND (utm.source = '{$source}' OR utm.source IS NULL)
                                AND (utm.medium = '{$medium}' OR utm.medium IS NULL)
                                AND (utm.content = '{$content}' OR utm.content IS NULL)
                            )
                        )
                GROUP BY promo.id_promo
                HAVING (
                    delivery > 0
                    AND customer > 0
                    AND product > 0
                    AND payment > 0
                    AND category > 0
                    AND provider > 0
                    )
                ORDER BY promo.prioridade DESC
                LIMIT 1";

        $coupon = $this->_em->createNativeQuery($dql, $rsm)
            ->setParameter('subtotal', $subtotal)
            ->setParameter('countProducts', count(explode(', ', $productsId)))
            ->setParameter('countPayments', count(explode(', ', $paymentsId)))
            ->setParameter('countCategories', count(explode(', ', $categoriesId)))
            ->setParameter('countProviders', count(explode(', ', $providersId)))
            ->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return $coupon;
    }

    /**
     * @param $idPayment
     * @param $idPaymentForm
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get active discount promotion on store
     */
    public function getActiveDiscountPromotion($idPayment, $idPaymentForm)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('AppBundle\Entity\B2cPromo', 'promo');
        $rsm->addFieldResult('promo', 'id_promo', 'idPromo');
        $rsm->addFieldResult('promo', 'desconto_perc', 'descontoPerc');
        $rsm->addFieldResult('promo', 'desconto_valor', 'descontoValor');

        $dql = "SELECT
                        promo.id_promo,
                        promo.desconto_perc,
                        promo.desconto_valor
                    FROM
                        b2c_promo promo
                            INNER JOIN
                        b2c_promo_pag pag ON promo.id_promo = pag.id_promo
                    WHERE
                        pag.id_pagfor = :idPaymentForm
                        AND pag.id_pagpag = :idPayment
                        AND promo.aplicar_pagamento = 1
                        AND promo.ativo = 1
                        AND promo.vale_compra = 0
                        AND NOW() BETWEEN CAST(CONCAT(promo.dta_validade_inicio, ' ', promo.hra_validade_inicio) AS DATETIME)
                        AND CAST(CONCAT(promo.dta_validade_fim, ' ', promo.hra_validade_fim) AS DATETIME)
                    ORDER BY promo.prioridade DESC
                    LIMIT 1";

        return $this->_em->createNativeQuery($dql, $rsm)
            ->setParameter('idPaymentForm', $idPaymentForm)
            ->setParameter('idPayment', $idPayment)
            ->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param $idProduct
     * @param $idPayment
     * @param $idPaymentForm
     * @return mixed
     *
     * Get active discount promotion by product on store
     */
    public function getActiveDiscountPromotionProduct($idProduct, $idPayment, $idPaymentForm)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('AppBundle\Entity\B2cPromo', 'promo');
        $rsm->addFieldResult('promo', 'id_promo', 'idPromo');
        $rsm->addFieldResult('promo', 'desconto_perc', 'descontoPerc');
        $rsm->addFieldResult('promo', 'desconto_valor', 'descontoValor');

        $dql = "SELECT
                        promo.id_promo,
                        promo.desconto_perc,
                        promo.desconto_valor
                    FROM
                        b2c_promo promo
                            INNER JOIN
                        b2c_promo_pag pag ON promo.id_promo = pag.id_promo
                            LEFT JOIN
                        b2c_promo_prd prd ON promo.id_promo = prd.id_promo AND prd.id_produto = :idProduct
                    WHERE
                        pag.id_pagfor = :idPaymentForm
                        AND pag.id_pagpag = :idPayment
                        AND promo.aplicar_pagamento = 1
                        AND promo.ativo = 1
                        AND promo.vale_compra = 0
                        AND ((promo.usar_todaloja = 1) OR (promo.usar_todaloja = 0 AND prd.id_promo is not null ))
                        AND NOW() BETWEEN CAST(CONCAT(promo.dta_validade_inicio, ' ', promo.hra_validade_inicio) AS DATETIME)
                        AND CAST(CONCAT(promo.dta_validade_fim, ' ', promo.hra_validade_fim) AS DATETIME)
                    ORDER BY promo.prioridade DESC
                    LIMIT 1";

        return $this->_em->createNativeQuery($dql, $rsm)
            ->setParameter('idProduct', $idProduct)
            ->setParameter('idPaymentForm', $idPaymentForm)
            ->setParameter('idPayment', $idPayment)
            ->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
