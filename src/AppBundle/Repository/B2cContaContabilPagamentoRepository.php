<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cContaContabilPagamentoRepository
 * @package AppBundle\Repository
 */
class B2cContaContabilPagamentoRepository extends EntityRepository
{

    /**
     * @param $pagamentoId
     * @return array
     *
     * save details marketplace and order B2c_conta_contabil
     */
    public function getAccounting($pagamentoId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $accounting = $queryBuilder
            ->select('cc')
            ->from('AppBundle:B2cContaContabilPagamento', 'ccp')
            ->join('AppBundle:B2cContaContabil', 'cc', 'WITH', 'cc.id = ccp.idConta')
            ->where('ccp.id = :pagamentoId')
            ->setParameter('pagamentoId', $pagamentoId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return $accounting;
    }

    /**
     * @param $channelId
     * @return array
     *
     * save details marketplace and order b2c_pedped_marketplace
     */
    public function getAccountingMarketPlace($channelId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $accounting = $queryBuilder
            ->select('cc')
            ->from('AppBundle:B2cMarketplaceChannel', 'mpch')
            ->join('AppBundle:B2cMarketplace', 'mp', 'WITH', 'mp.id = mpch.marketplaceId')
            ->join('AppBundle:B2cContaContabil', 'cc', 'WITH', 'cc.id = mp.idConta')
            ->where('mpch.id = :channelId')
            ->setParameter('channelId', $channelId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return $accounting;
    }
}
