<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Validate\ValidatePayment;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class PaymentRepository
 * @package AppBundle\Repository
 */
class PaymentRepository extends EntityRepository
{

    /**
     * @param $payments
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get payment info
     */
    public function getPaymentInfo($payments)
    {
        $paymentsreturn = array();

        /* @var $payment ValidatePayment */
        foreach ($payments as $payment) {
            if ($payment->getId() > 0) {
                $pagFor = $this->_em->createQueryBuilder()
                    ->select(
                        'pagFor.idPagfor',
                        'pagFor.indexador',
                        'pagFor.numParcelas',
                        'pagPag.nome',
                        'pagPag.descConfirmacao',
                        'pagPag.prazo',
                        '(CASE WHEN (pagFor.numParcelas = 0 OR pagFor.numParcelas = 1 OR pagFor.numParcelas IS NULL) THEN \' à vista\' ELSE CONCAT(\' em \', pagFor.numParcelas, \'x\') END) as instalmentText',
                        '(CASE WHEN (pagFor.indexador = 0 OR pagFor.indexador IS NULL) THEN \' sem juros\' ELSE \' com juros\' END) as interestText'
                    )
                    ->from('AppBundle:B2cPagpag', 'pagPag')
                    ->leftJoin('AppBundle:B2cPagfor', 'pagFor', 'WITH', 'pagPag.idPagpag = pagFor.idPagpag AND pagFor.numParcelas = :instalment AND pagFor.valMinimo <= :amount')
                    ->where('pagPag.idPagpag = :paymentId')
                    ->setParameter('paymentId', $payment->getId())
                    ->setParameter('instalment', $payment->getInstalment())
                    ->setParameter('amount', ($payment->getInstalment() * $payment->getInstalmentValue()))
                    ->setMaxResults(1)
                    ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

                $deadlineDays = ($pagFor['prazo'] > 0 && $pagFor['prazo'] != '' ? $pagFor['prazo'] : 3);

                $paymentInfo = array();
                $paymentInfo['id'] = $payment->getId();
                $paymentInfo['idPaymentForm'] = (isset($pagFor['idPagfor']) ? $pagFor['idPagfor'] : 0);
                $paymentInfo['indexer'] = (isset($pagFor['indexador']) ? $pagFor['indexador'] : 0);
                $paymentInfo['instalment'] = $payment->getInstalment();
                $paymentInfo['instalmentValue'] = $payment->getInstalmentValue();
                $paymentInfo['name'] = $pagFor['nome'];
                $paymentInfo['description'] = $pagFor['descConfirmacao'];
                $paymentInfo['deadline'] = \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime(date('Y-m-d') . ' +' . $deadlineDays . ' weekdays')));
                $paymentInfo['paymentFormDesc'] = $pagFor['instalmentText'] . ' de R$ ' . number_format($payment->getInstalmentValue(), 2, ',', '.') . $pagFor['interestText'];

                $paymentsreturn[] = $paymentInfo;
            }
        }

        return $paymentsreturn;
    }
}
