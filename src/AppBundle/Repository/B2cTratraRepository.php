<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\VO\Create\CreateRegionVO;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cTratraRepository
 * @package AppBundle\Repository
 */
class B2cTratraRepository extends EntityRepository
{

    /**
     * @param CreateRegionVO $region
     * @param $categories
     * @return array
     *
     * Get shipping data from categories and region
     */
    public function getShippingFromCategoriesAndRegion(CreateRegionVO $region, $categories, $providerId = null)
    {
        $queryCategories = '';
        foreach ($categories as $categoryId) {
            $queryCategories .= "tra.categoria LIKE '%" . $categoryId . "%' AND ";
        }
        $queryCategories = substr($queryCategories, 0, -5);

        $pattern = array('0', '1');
        if ($region->isChooseCustomer()) {
            $pattern = array('1');
        }
        $queryProviders = 'tra.idForfor IS NULL';
        if (!empty($providerId)) {
            $queryProviders = 'tra.idForfor = ' . $providerId;
        }

        $queryBuilder = $this->_em->createQueryBuilder();
        return $queryBuilder
            ->select(
                'IDENTITY(tra.idTratra) as idTratra',
                'IDENTITY(tra.idRegreg) as idRegreg',
                'tra.padrao',
                'tra.tipoFrete',
                'tra.nomePublico',
                'tra.prazoFreteFixo',
                'tra.valorFreteFixo',
                'tra.prazoFreteGratuito',
                'tra.prazoTransportadora',
                'tra.categoria',
                'tra.seguro',
                'tra.tipoServico',
                'tra.idForfor'
            )
            ->from('AppBundle:B2cTratra', 'tra')
            ->where('tra.idRegreg = :region')
            ->andWhere($queryCategories)
            ->andWhere($queryProviders)
            ->andWhere($queryBuilder->expr()->in('tra.padrao', $pattern))
            ->setParameter('region', $region->getId())
            ->orderBy('tra.padrao', 'DESC')
            ->addOrderBy('tra.nomePublico')
            ->getQuery()->getArrayResult();
    }

    /**
     * @param CreateRegionVO $region
     * @param $transId
     * @param $weight
     * @param $maxWeight
     * @param $cubage
     * @param $maxCubage
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Calculate shipping value
     */
    public function calculateFreightShipping(CreateRegionVO $region, $transId, $weight, $maxWeight, $cubage, $maxCubage)
    {
        $queryBuilderAux = $this->_em->createQueryBuilder();

        $queryBuilderAux
            ->select('regp.id')
            ->from('AppBundle:B2cRegpeso', 'regp')
            ->where("({$weight} - regp.iniKg) >= 0")
            ->andWhere("(regp.fimKg - {$weight}) >= 0")
            ->orWhere("{$weight} > {$maxWeight} AND regp.fimKg >= {$maxWeight}");

        $shipping = $this->_em->createQueryBuilder()
            ->select('regTransVal.valor')
            ->from('AppBundle:B2cRegtransVal', 'regTransVal')
            ->join('regTransVal.idTrans', 'tra')
            ->where('tra.idRegreg = :region')
            ->andWhere('regTransVal.idTrans = :transId')
            ->andWhere($this->_em->createQueryBuilder()->expr()->in('regTransVal.idKg', $queryBuilderAux->getDQL()))
            ->setParameter('region', $region->getId())
            ->setParameter('transId', intval($transId))
            ->orderBy('regTransVal.valor', 'DESC')
            ->getQuery()->getArrayResult();

        $shippingValue = $shipping[0]['valor'];

        if (($weight == 0 || $cubage > 0) && (($weight > 0 && $cubage > 0) || ($weight == 0 && $cubage == 0))) {
            $queryBuilderAux = $this->_em->createQueryBuilder();

            $queryBuilderAux
                ->select('regmec.id')
                ->from('AppBundle:B2cRegmec', 'regmec')
                ->where("({$cubage} - regmec.iniMc) >= 0")
                ->andWhere("(regmec.fimMc - {$cubage}) >= 0")
                ->orWhere("{$cubage} > {$maxCubage} AND regmec.fimMc >= {$maxCubage}");

            $shipping = $this->_em->createQueryBuilder()
                ->select('regTransVal.valor')
                ->from('AppBundle:B2cRegtransVal', 'regTransVal')
                ->join('regTransVal.idTrans', 'tra')
                ->where('tra.idRegreg = :region')
                ->andWhere('regTransVal.idTrans = :transId')
                ->andWhere($this->_em->createQueryBuilder()->expr()->in('regTransVal.idMc', $queryBuilderAux->getDQL()))
                ->setParameter('region', $region->getId())
                ->setParameter('transId', $transId)
                ->orderBy('regTransVal.valor', 'DESC')
                ->getQuery()->getArrayResult();

            $shippingValue += $shipping[0]['valor'];
        }

        return $shippingValue;
    }

    /**
     * @param B2cPedped $order
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get shipper from region by existing order data
     */
    public function getShipperRegion(B2cPedped $order)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'IDENTITY(tra.idTratra) as shipperId',
                'reg.id as regionId',
                'tra.prazoFreteFixo',
                'tra.prazoFreteGratuito',
                'tra.prazoTransportadora'
            )
            ->from('AppBundle:B2cTratra', 'tra')
            ->leftJoin('AppBundle:B2cRegreg', 'reg', 'WITH', 'reg.id = tra.idRegreg')
            ->where('reg.regiao = :region')
            ->andWhere('tra.nomePublico = :publicName')
            ->setParameter('region', $order->getPedEntregaRegiao())
            ->setParameter('publicName', $order->getPedTransportadora())
            ->orderBy('tra.padrao', 'DESC')
            ->addOrderBy('tra.nomePublico')
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
