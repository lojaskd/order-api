<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cRegmecRepository
 * @package AppBundle\Repository
 */
class B2cRegmecRepository extends EntityRepository
{

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get max cubage on store
     */
    public function getMaxCubage()
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $maxCubage = $queryBuilder
            ->select('MAX(cub.fimMc) as max_cubage')
            ->from('AppBundle:B2cRegmec', 'cub')
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return $maxCubage['max_cubage'];
    }
}
