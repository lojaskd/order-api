<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPrdprdMarketplaceRepository
 * @package AppBundle\Repository
 */
class B2cPrdprdMarketplaceRepository extends EntityRepository
{

    /**
     * @param $productId
     * @param $marketplaceId
     * @return int
     *
     * Get marketplace product stock
     */
    public function getProductStock($productId, $marketplaceId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $marketplaceProductStock = $queryBuilder
            ->select('prd.estoque')
            ->from('AppBundle:B2cPrdprdMarketplace', 'prd')
            ->where('prd.idPrdprd = :productId')
            ->andWhere('prd.idMarketplace = :marketplaceId')
            ->setParameter('productId', $productId)
            ->setParameter('marketplaceId', $marketplaceId)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return (isset($marketplaceProductStock['estoque']) ? $marketplaceProductStock['estoque'] : 0);
    }

    /**
     * @param int $productId
     * @param int $quantity
     * @param int $marketplaceId
     * @return boolean
     *
     * Update marketplace product stock
     */
    public function updateProductStock($productId, $quantity, $marketplaceId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->update('AppBundle:B2cPrdprdMarketplace', 'mktplace')
            ->set('mktplace.estoque', 'mktplace.estoque - '. $quantity)
            ->set('mktplace.dtaAlt', $queryBuilder->expr()->literal(date('Y-m-d H:i:s')))
            ->where('mktplace.idMarketplace = ?1')
            ->andWhere('mktplace.idPrdprd = ?2')
            ->setParameter(1, $marketplaceId)
            ->setParameter(2, $productId)
            ->getQuery()->execute();

        return true;
    }
}
