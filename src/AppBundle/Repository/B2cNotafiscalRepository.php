<?php

namespace AppBundle\Repository;

use AppBundle\Entity\B2cNotafiscal;
use AppBundle\Entity\B2cNotafiscalItem;
use AppBundle\Entity\VO\Create\CreateInvoiceItemVO;
use AppBundle\Entity\VO\Create\CreateInvoiceVO;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class B2cNotafiscalRepository
 * @package AppBundle\Repository
 */
class B2cNotafiscalRepository extends EntityRepository
{

    /**
     * @param string $term
     * @return mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get NF order from search term
     */
    public function searchNFFromTerm($term)
    {
        $orders = array();

        if ($term) {
            $queryBuilder = $this->_em->createQueryBuilder();

            $orders = $queryBuilder
                            ->select('ped.idPedped', 'ped.idClicli', 'ped.entNome', 'ped.entSobrenome', 'pedsit.name as situacao')
                            ->from('AppBundle:B2cNotafiscal', 'nf')
                            ->innerJoin('AppBundle:B2cPedped', 'ped', 'WITH', 'ped.idPedped = nf.idPedped')
                            ->innerJoin('AppBundle:B2cPedsituacao', 'pedsit', 'WITH', 'ped.pedSituacao = pedsit.id')
                            ->where('nf.idNotafiscal = :term')
                            ->setParameter('term', $term)
                            ->setMaxResults(10)
                            ->getQuery()->getArrayResult();
        }

        return $orders;
    }

    /**
     * @param CreateInvoiceVO $createInvoiceVO
     * @return B2cNotaFiscal
     *
     * Create new invoice
     */
    public function createInvoice(CreateInvoiceVO $createInvoiceVO)
    {
        $this->_em->getConnection()->beginTransaction();

        try {
            $invoice = new B2cNotafiscal();
            $invoice->setIdPedped($createInvoiceVO->getOrderId());
            $invoice->setIdNotafiscal($createInvoiceVO->getInvoiceId());
            $invoice->setIdFilial($createInvoiceVO->getBranchId());
            $invoice->setDataCriacao($createInvoiceVO->getCreatedDate());
            $invoice->setDataDevolucao($createInvoiceVO->getReturnedDate());
            $invoice->setDataEmbarque($createInvoiceVO->getShippingDate());
            $invoice->setDataEntregaProvavel($createInvoiceVO->getPredictDeliveryDate());
            $invoice->setDataEntregaRealizada($createInvoiceVO->getDeliveryDate());
            $invoice->setDevolvido($createInvoiceVO->isReturned());
            $invoice->setSerieNotafiscal($createInvoiceVO->getSerie());
            $invoice->setChave($createInvoiceVO->getKey());

            //save
            $this->_em->persist($invoice);

            /* @var $createInvoiceItemVO CreateInvoiceItemVO */
            foreach ($createInvoiceVO->getItens() as $createInvoiceItemVO) {
                $invoiceItem = new B2cNotafiscalItem;
                $invoiceItem->setIdPedped($createInvoiceVO->getOrderId());
                $invoiceItem->setIdNotafiscal($createInvoiceVO->getInvoiceId());
                $invoiceItem->setIdFilial($createInvoiceVO->getBranchId());
                $invoiceItem->setDataDevolucao($createInvoiceItemVO->getReturnedDate());
                $invoiceItem->setDevolvido($createInvoiceItemVO->isReturned());
                $invoiceItem->setNomeTransportadora($createInvoiceVO->getShipperName());
                $invoiceItem->setDocumentoTransportadora($createInvoiceVO->getShipperDoc());
                $invoiceItem->setHub($createInvoiceVO->getHub());
                $invoiceItem->setPrdQtde($createInvoiceItemVO->getQty());
                $invoiceItem->setPrdReferencia($createInvoiceItemVO->getProductReference());
                //save
                $this->_em->persist($invoiceItem);
            }

            $this->_em->flush();
            // Try and commit the transaction
            $this->_em->getConnection()->commit();

            return $invoice;
        } catch (Exception $e) {
            // Rollback the failed transaction attempt
            $this->_em->getConnection()->rollback();
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Gets all product invoices
     *
     * @param $orderId
     * @param $productReference
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getInvoices($orderId, $productReference)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select(
                'nf.idNotafiscal',
                'nf.dataCriacao',
                'nfi.idFilial',
                'nfi.idPedped',
                'nfi.prdReferencia',
                'nfi.prdQtde',
                'nfi.dataCadastro',
                'nfi.dataDevolucao',
                'nfi.devolvido',
                'nfi.nomeTransportadora',
                'nfi.documentoTransportadora',
                'nfi.hub'
            )
            ->from('AppBundle:B2cNotafiscal', 'nf')
            ->innerJoin('AppBundle:B2cNotafiscalItem', 'nfi', 'WITH', 'nf.idPedped = nfi.idPedped AND nf.idNotafiscal = nfi.idNotafiscal')
            ->where('nf.idPedped = :orderId')
            ->andWhere('nfi.prdReferencia = :productReference')
            ->setParameter('orderId', $orderId)
            ->setParameter('productReference', $productReference)
            ->orderBy('nf.dataCriacao', 'DESC')
            ->getQuery()->getArrayResult();
    }
}
