<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cPromoForRepository
 * @package AppBundle\Repository
 */
class B2cPromoForRepository extends EntityRepository
{

    /**
     * @param $promoId
     * @param $providerId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Validate provider coupon
     */
    public function validateProviderCoupon($promoId, $providerId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        return $queryBuilder
            ->select('promoFor.idFornecedor')
            ->from('AppBundle:B2cPromoFor', 'promoFor')
            ->where('promoFor.idPromo = :promoId')
            ->andWhere('promoFor.idFornecedor = :providerId')
            ->setParameter('promoId', $promoId)
            ->setParameter('providerId', $providerId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
