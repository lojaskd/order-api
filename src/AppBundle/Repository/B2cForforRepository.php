<?php

namespace AppBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 * Class B2cForforRepository
 * @package AppBundle\Repository
 */
class B2cForforRepository extends EntityRepository
{

    public function getPolo($providerId)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $poloInfo = $queryBuilder
            ->select("CASE WHEN for.estado = 'RS' THEN 1 ELSE (CASE WHEN for.estado = 'PR' THEN 2 ELSE 3 END) END AS polo")
            ->from('AppBundle:B2cForfor', 'for')
            ->where('for.id = :providerId')
            ->setParameter('providerId', $providerId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);

        return $poloInfo['polo'] ? $poloInfo['polo'] : 0;
    }
}
