<?php

namespace AppBundle\Command;

use AppBundle\Entity\B2cNotafiscal;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ProcessDeliveryCommand
 * @package AppBundle\Command
 */
class ProcessDeliveryCommand extends ContainerAwareCommand
{

    /**
     * Configure command method
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:process-delivery')

            // the short description shown while running "php bin/console list"
            ->setDescription('Process Cancelled Delivered Orders.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to process cancelled delivered orders...")
        ;
    }

    /**
     * Execute command method
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '========================',
            'Process Delivered Orders',
            '========================',
            '',
        ]);

        $this->getContainer()->get('queue_service')->getMsgs('order.delivery', '', 1, array($this, 'processDelivery'));

        $output->writeln(['Fim do processamento! :)', '']);
    }

    /**
     * Method to process delivered message
     *
     * @param AMQPMessage $message
     * @throws \Exception
     */
    public function processDelivery($message)
    {
        // Total retry
        $totalRetry = 0;
        $props = $message->get_properties();
        $output = new ConsoleOutput();

        // Get total retry to headers
        if (isset($props['application_headers'])) {
            $headers = $message->get('application_headers')->getNativeData();
            if (isset($headers['x-death'])) {
                array_walk_recursive(
                    $headers['x-death'],
                    function ($vHeader, $kHeader, $uHeader) {
                        if ($kHeader === $uHeader[0]) {
                            $uHeader[1] = $vHeader;
                        }
                    },
                    ['count', &$totalRetry]
                );
            }
        }

        $output->writeln(['Mensagem: ' . $message->body]);

        $data = json_decode($message->body, true);
        $output->writeln(['Processing invoice ' . $data['invoice_id'] . ' - Retry: ' . $totalRetry]);

        try {
            if (isset($data['invoice_id']) && isset($data['order_id'])) {
                if (!isset($data['task'])) {
                    throw new \Exception('Task code was not sent.');
                }

                if (!isset($data['ticket']) || (isset($data['ticket']) && empty($data['ticket']))) {
                    throw new \Exception('No Zendesk ticket was informed.');
                }

                $ticket = $this->getContainer()->get('occurrence_service')->getTicket($data['ticket']);
                if (!isset($ticket['code']) || (isset($ticket['code']) && $ticket['code'] != 200) || empty($ticket['data'])) {
                    throw new \Exception('Error getting ticket: ' . serialize($ticket));
                }

                $taskKey = $data['task'];
                $originalTask = $this->getContainer()->get('occurrence_service')->getTask($taskKey);
                if (!isset($originalTask['data']['fields'])) {
                    throw new \Exception('Error getting original task : ' . serialize($originalTask));
                }

                preg_match_all('/(\d+)( )?-?( )?(\w+)?( )?-?( )?(\w+)?( )?(\d+)?(\/)?(\d+)?/', $originalTask['data']['fields']['customfield_11204'], $products);
                if (empty($products)) {
                    $output->writeln('Product regex match error on product: ' . $originalTask['data']['fields']['customfield_11204']);
                    throw new \Exception('Product regex match error on product: ' . $originalTask['data']['fields']['customfield_11204']);
                }
                $products = $products[0];

                $transitions = array();
                if (isset($originalTask['data']['transitions']) && !empty($originalTask['data']['transitions'])) {
                    foreach ($originalTask['data']['transitions'] as $transition) {
                        $transitions[$transition['name']] = $transition['id'];
                    }
                }

                /* @var $invoiceStatus B2cNotafiscal */
                $invoiceStatus = $this->getContainer()->get('invoice_service')->getInvoice($data['invoice_id'], $data['order_id']);
                $productInfo = $this->getContainer()->get('product_service')->getProductOrderInfo($data['order_id'], trim($products[0]));

                $messageDiscarded = false;
                if (isset($invoiceStatus)) {
                    /*
                     ***********************************
                     * Checks if invoice was delivered *
                     * *********************************
                     */
                    if (!$invoiceStatus->getDataEntregaRealizada() && empty($productInfo['data_ent_r'])) {
                        $output->writeln([
                            'Product doesn\'t have delivery date, discarding message...',
                            '',
                            ''
                        ]);
                        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                        return;
                    }

                    $order = $this->getContainer()->get('order_service')->getOrderInfo($data['order_id']);

                    /*
                     ****************************************
                     * Assistance order is always discarded *
                     * **************************************
                     */
                    if ($order['assistencia']) {
                        $output->writeln(['Assistance order, transitioning to discard...']);
                        if (!array_key_exists('descartar', $transitions)) {
                            throw new \Exception('Its not possible to transition Jira task to Descarte, transition not allowed to the current status.');
                        }

                        $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($taskKey, $transitions['descartar']);

                        if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                            throw new \Exception('Error transitioning task to descartar: ' . serialize($transitionAction));
                        }

                        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                        return;
                    }

                    $reverseTotalCost = $totalProductCost = 0;
                    /*
                     ********************************************************************
                     * For each product calculates the freight cost and gets cost price *
                     * ******************************************************************
                     */
                    $taskLog = $productLog = array();
                    foreach ($products as $product) {
                        $productReference = $this->getProductReference($order, $product);
                        $productLog[$productReference] = array();
                        $outgoingInvoice = $this->getOutgoingInvoice($data['order_id'], $productReference);

                        // Get shipping cost value on shippers panel
                        $reverseProductValue = $this->getShippingValues($data['order_id'], $outgoingInvoice, $productReference, $taskKey);

                        if ($reverseProductValue === false || $reverseProductValue == 0) {
                            $output->writeln(['Transitioning to descarte_coleta because no shipping data was found...']);
                            if (!array_key_exists('verificar_custos', $transitions)) {
                                $output->writeln(['Its not possible to transition Jira task to descarte_coleta, transition not allowed to the current status. Discarding message...', '']);

                                $comment = $this->getContainer()->get('occurrence_service')->commentTask($taskKey, 'Ocorreu um erro ao mover a task para descarte_coleta - Jira não se encontra no status que permite a transição.');
                                if (!isset($comment['code']) || (isset($comment['code']) && $comment['code'] != 200)) {
                                    throw new \Exception('Error commenting on task: ' . serialize($comment));
                                }

                                $messageDiscarded = true;
                            }

                            if (!$messageDiscarded) {
                                $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($taskKey, $transitions['verificar_custos']);
                                if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                                    throw new \Exception('Error transitioning task to descarte_coleta: ' . serialize($transitionAction));
                                }

                                $updateTask = $this->getContainer()->get('occurrence_service')->updateTask($taskKey, array('fail_reverse_cost' => 'frete_nao_cadastrado'));
                                if (!isset($updateTask['code']) || (isset($updateTask['code']) && $updateTask['code'] != 200)) {
                                    throw new \Exception('Error updating erro_calculo_descarte/coleta field on Jira: ' . serialize($updateTask));
                                }
                            }

                            $this->getContainer()->get('occurrence_service')->commentTicket(
                                $data['ticket'],
                                'Faltou cadastro de custo de frete para verificar se é descarte ou coleta, é necessário aguardar o retorno da Operação para prosseguir com a tratativa.',
                                false
                            );

                            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                            return;
                        }

                        $outgoingInvoice['volume'] = ($outgoingInvoice['volume'] == 0 ? 1 : $outgoingInvoice['volume']);
                        $reverseTotalCost += ($reverseProductValue + (4.8 * $outgoingInvoice['volume']));

                        // Get product data
                        $productInfo = $this->getProduct($product);
                        $totalProductCost += $productInfo['custo'];

                        $productLog[$productReference]['volume'] = $outgoingInvoice['volume'];
                        $productLog[$productReference]['cost_price'] = $productInfo['custo'];
                        $productLog[$productReference]['reverse_value'] = $reverseProductValue;
                    }

                    $taskLog['products'] = $productLog;
                    $taskLog['reverse_value_total'] = $reverseTotalCost;
                    $reverseTotalCost += 11.89;

                    if (!isset($ticket['data']['custom_fields']['cf81049868']['value']) || empty($ticket['data']['custom_fields']['cf81049868']['value'])) {
                        $this->getContainer()->get('occurrence_service')->commentTicket(
                            $data['ticket'],
                            'Necessário inserir a informação do estado do produto para que seja calculado o valor do descarte.',
                            false
                        );

                        throw new \Exception('No product status was informed.');
                    }

                    $costPercentage = 1;
                    switch ($ticket['data']['custom_fields']['cf81049868']['value']) {
                        case 'bom___b2c':
                            // If product state is good, 50 until 80% of cost price
                            $costPercentage = 0.5;
                            break;
                        case 'razoavel___b2b':
                            // If product state is reasonable, 40 until 60% of cost price
                            $costPercentage = 0.4;
                            break;
                        case 'ruim____des_montado':
                            // If product state is bad, 30 until 40% of cost price
                            $costPercentage = 0.3;
                            break;
                    }

                    $taskLog['products_state'] = $ticket['data']['custom_fields']['cf81049868']['value'];
                    $taskLog['products_state_percentage'] = $costPercentage;

                    $reverseTotalRevenue = $totalProductCost * $costPercentage;
                    $taskLog['total_products_cost_price'] = $totalProductCost;
                    $taskLog['total_revenue'] = $reverseTotalRevenue;
                    $taskLog['total_cost'] = $reverseTotalCost;

                    $collect = true;
                    if ($reverseTotalCost > $reverseTotalRevenue) {
                        $collect = false;
                    }

                    $taskLog['reverse_difference'] = $reverseTotalRevenue - $reverseTotalCost;
                    $taskLog['collect'] = $collect;

                    $this->logDiscardValues($data['task'], $data['ticket'], $taskLog);

                    /*
                     **********************************************************
                     * If cost is higher than revenue, discard the product(s) *
                     * ********************************************************
                     */
                    if (!$collect) {
                        $output->writeln(['Transitioning to discard...']);
                        if (!array_key_exists('descartar', $transitions)) {
                            $output->writeln(['Its not possible to transition Jira task to Descarte, transition not allowed to the current status. Discarding message...', '']);

                            $comment = $this->getContainer()->get('occurrence_service')->commentTask($taskKey, 'Ocorreu um erro ao mover a task para descarte - Jira não se encontra no status que permite a transição.');
                            if (!isset($comment['code']) || (isset($comment['code']) && $comment['code'] != 200)) {
                                throw new \Exception('Error commenting on task: ' . serialize($comment));
                            }

                            $messageDiscarded = true;
                        }

                        if (!$messageDiscarded) {
                            $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($taskKey, $transitions['descartar']);

                            if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                                throw new \Exception('Error transitioning task to descartar: ' . serialize($transitionAction));
                            }
                        }
                    }

                    /*
                     **********************************************************
                     * If revenue is higher than cost, collect the product(s) *
                     * ********************************************************
                     */
                    if ($collect) {
                        $output->writeln(['Transitioning to collect...']);
                        if (!array_key_exists('coletar_reversa', $transitions)) {
                            $output->writeln(['Its not possible to transition Jira task to Coleta Reversa, transition not allowed to the current status. Discarding message...', '']);

                            $comment = $this->getContainer()->get('occurrence_service')->commentTask($taskKey, 'Ocorreu um erro ao mover a task para Coleta Reversa - Jira não se encontra no status que permite a transição.');
                            if (!isset($comment['code']) || (isset($comment['code']) && $comment['code'] != 200)) {
                                throw new \Exception('Error commenting on task: ' . serialize($comment));
                            }

                            $messageDiscarded = true;
                        }

                        if (!$messageDiscarded) {
                            $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($taskKey, $transitions['coletar_reversa']);

                            if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                                throw new \Exception('Error transitioning task to coletar_reversa: ' . serialize($transitionAction));
                            }
                        }
                    }

                    $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                    return;
                }
            }
        } catch (\Exception $exception) {
            $output->writeln(['Fail (' . $totalRetry . ') to process delivered invoice ' . $data['invoice_id'] . ': ' . $exception->getMessage(), '']);

            $this->getContainer()->get('queue_service')->publishOrderMessage(
                $message,
                ($totalRetry < 5 ? 'order.delivery.retry' : 'order.delivery.fail'),
                ''
            );

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);

            throw $exception;
        }
    }

    /**
     * Gets product reference
     *
     * @param array $order
     * @param integer $productId
     * @return mixed
     * @throws \Exception
     */
    private function getProductReference($order, $productId)
    {
        if (isset($order['produtos']) && !empty($order['produtos'])) {
            foreach ($order['produtos'] as $product) {
                if (intval($product['id_prdprd']) === intval($productId)) {
                    return $product['prd_referencia'];
                }
            }
        }

        throw new \Exception('Error searching for product reference.');
    }

    /**
     * Gets outgoing invoice
     *
     * @param integer $orderId
     * @param string $productReference
     * @return mixed
     * @throws \Exception
     */
    private function getOutgoingInvoice($orderId, $productReference)
    {
        try {
            $outgoingInvoice = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference) . '/invoice'
            );

            if (!isset($outgoingInvoice['code']) || (isset($outgoingInvoice['code']) && $outgoingInvoice['code'] != 200)) {
                throw new \Exception('Error getting outgoing invoice: ' . serialize($outgoingInvoice));
            }

            if (empty($outgoingInvoice['data'])) {
                throw new \Exception('Outgoing invoice not found.');
            }

            return $outgoingInvoice['data'][0];
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Get shipping values from freight API
     *
     * @param integer $orderId
     * @param array $invoice
     * @param string $productReference
     * @param string $taskKey
     * @return mixed
     * @throws \Exception
     */
    private function getShippingValues($orderId, $invoice, $productReference, $taskKey)
    {
        try {
            $shipping = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('freight_api_url') . '/orders/' . $orderId . '/invoices/' . $invoice['serial'] . '/products/' . base64_encode($productReference) . '/shipper/value'
            );

            if (!isset($shipping['code']) || (isset($shipping['code']) && $shipping['code'] != 200)) {
                throw new \Exception('Error getting shipper: ' . serialize($shipping));
            }

            if (empty($shipping['data']) || (isset($shipping['data']['shippers']) && empty($shipping['data']['shippers'])) || !isset($shipping['data']['shippers'])) {
                return $this->getCte($orderId, $invoice, $productReference, $taskKey);
            }

            $totalProductShipping = floatval($shipping['data']['shippers'][0]['values']['total_value_with_taxes']);
            $reversePercentage = floatval($shipping['data']['shippers'][0]['shipper_branch_term']['reverse_percentage']);
            $reverseTax = floatval($shipping['data']['shippers'][0]['shipper_branch_term']['reverse_tax']);

            return ($totalProductShipping * $reversePercentage / 100) + $reverseTax;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Gets CTE on SAP API
     *
     * @param integer $orderId
     * @param array $invoice
     * @param string $productReference
     * @param string $taskKey
     * @return float|int|bool
     * @throws \Exception
     */
    private function getCte($orderId, $invoice, $productReference, $taskKey)
    {
        try {
            $this->notifyNoShipperFound($orderId, $invoice, $productReference);

            $cte = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/invoices/' . $invoice['serial'] . '/products/' . base64_encode($productReference) . '/ctes'
            );

            if (!isset($cte['code']) || (isset($cte['code']) && $cte['code'] != 200)) {
                throw new \Exception('Error getting CTE from SAP: ' . serialize($cte));
            }

            if (!empty($cte['data'])) {
                $totalValue = 0;
                foreach ($cte['data'] as $product) {
                    $totalValue += floatval($product['cte_value']);
                }

                return (($cte['data'][0]['product_weight'] * $cte['data'][0]['cte_value']) / $cte['data'][0]['invoice_weight']);
            }

            $comment = $this->getContainer()->get('occurrence_service')->commentTask($taskKey, '[ERRO] Ocorreu um erro ao calcular o descarte - Transportadora/Acordo comercial não cadastrados e nenhuma CTE encontrada.');
            if (!isset($comment['code']) || (isset($comment['code']) && $comment['code'] != 200)) {
                throw new \Exception('Error commenting on task: ' . serialize($comment));
            }

            return false;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Notifies if shipper was not found on shippers panel register
     *
     * @param integer $orderId
     * @param array $invoice
     * @param string $productReference
     * @throws \Exception
     */
    private function notifyNoShipperFound($orderId, $invoice, $productReference)
    {
        $this->getContainer()->get('email_service')->send(
            'Painel de Transportadoras - Novo cadastro',
            'ti@lojaskd.com.br',
            'transporte@lojaskd.com.br',
            'Transporte LKD',
            'email/shipper_not_found.html.twig',
            array('invoice' => $invoice, 'order'=> $orderId, 'product' => $productReference)
        );
    }

    /**
     * Get product data
     *
     * @param int $productId
     * @return array
     * @throws \Exception
     */
    private function getProduct($productId)
    {
        try {
            if (!$product = $this->getContainer()->get('product_api_service')->getProductInfo($productId)) {
                throw new \Exception('Error getting product data.');
            }

            return $product;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Logs discard/collect values on Jira task
     *
     * @param $taskKey
     * @param $ticket
     * @param $log
     * @throws \Exception
     */
    private function logDiscardValues($taskKey, $ticket, $log)
    {
        try {
            $comment = "----------------------\nProdutos:\n\n";

            foreach ($log['products'] as $productId => $product) {
                $comment .= $productId . ":\n";
                $comment .= 'Volume = ' . $product['volume'] . "\n";
                $comment .= 'Custo = ' . $product['cost_price'] . "\n";
                $comment .= 'Valor da Reversa = ' . $product['reverse_value'] . "\n";
                $comment .= "Taxa por volume = 4.8\n\n";
            }

            $comment .= "----------------------\n";

            $comment .= "Cálculo da Receita: \n";
            $comment .= 'Total do custo dos produtos = ' . $log['total_products_cost_price'] . "\n";
            $comment .= 'Estado dos produtos = ' . $log['products_state'] . "\n";
            $comment .= 'Percentagem sobre o estado dos produtos = ' . $log['products_state_percentage'] . "\n";
            $comment .= 'Total de receita = ' . $log['total_revenue'] . "\n\n";

            $comment .= "----------------------\n";
            $comment .= "Cálculo do Custo: \n";
            $comment .= "Taxa produto/hora = 11.89\n";
            $comment .= 'Valor total da reversa = ' . $log['reverse_value_total'] . "\n";
            $comment .= 'Total de custo = ' . $log['total_cost'] . "\n\n";

            $comment .= "----------------------\n\n";

            $comment .= 'Coletar = ' . ($log['collect'] ? 'sim' : 'não') . "\n";

            $this->getContainer()->get('occurrence_service')->commentTask($taskKey, $comment);
            $this->getContainer()->get('occurrence_service')->updateTask($taskKey, array('discard_collect_value' => $log['reverse_value_total'], 'reverse_difference' => $log['reverse_difference']));

            str_replace('\n', '
            
            ', $comment);

            $this->getContainer()->get('occurrence_service')->commentTicket($ticket, 'Cálculo de descarte/coleta:
            
            ' . $comment, false);
        } catch (\Exception $exception) {
            throw new \Exception('Error logging the discard/collection calculation: ' . $exception->getMessage());
        }
    }
}
