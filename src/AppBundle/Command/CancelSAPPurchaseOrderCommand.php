<?php

namespace AppBundle\Command;

use AppBundle\Entity\B2cPedprdstatus;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CancelSAPPurchaseOrderCommand
 * @package AppBundle\Command
 */
class CancelSAPPurchaseOrderCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:cancel-sap-purchase-order')

            // the short description shown while running "php bin/console list"
            ->setDescription('Cancels SAP purchase orders.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to cancel SAP purchase orders...")
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '===============================',
            'SAP Purchase Order Cancellation',
            '===============================',
            '',
        ]);

        $this->getContainer()->get('queue_service')->getMsgs('purchase.order', '', 1, array($this, 'cancelSapPurchaseOrder'));

        $output->writeln(['Fim do cancelamento da ordem de compra no SAP!', '']);
    }

    /**
     * @param AMQPMessage $message
     * @throws \Exception
     */
    public function cancelSapPurchaseOrder($message)
    {
        // Total retry
        $totalRetry = 0;
        $props = $message->get_properties();
        $output = new ConsoleOutput();

//      Get total retry to headers
        if (isset($props['application_headers'])) {
            $headers = $message->get('application_headers')->getNativeData();
            if (isset($headers['x-death'])) {
                array_walk_recursive(
                    $headers['x-death'],
                    function ($vHeader, $kHeader, $uHeader) {
                        if ($kHeader === $uHeader[0]) {
                            $uHeader[1] = $vHeader;
                        }
                    },
                    ['count', &$totalRetry]
                );
            }
        }

        $output->writeln(['Mensagem: ' . $message->body]);

        $data = json_decode($message->body, true);
        $output->writeln(['Processing: ' . $data['order_id'] . ' - Retry: ' . $totalRetry]);

        try {
            if (!isset($data['order_id'])) {
                $output->writeln(['Numero de pedido inválido.']);
                $output->writeln(['#############################################', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            if (!isset($data['task']) || empty($data['task'])) {
                $output->writeln(['Número da task não informada.']);
                $output->writeln(['#############################################', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            $purchaseOrder = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $data['order_id'] . '/purchaseOrders/' . $data['purchase_order'] . '/products'
            );

            if (!isset($purchaseOrder['code']) || (isset($purchaseOrder['code']) && $purchaseOrder['code'] != 200)) {
                if (isset($purchaseOrder['message'])) {
                    throw new \Exception('Erro ao buscar produtos da OC / pedido na API-SAP: ' . $purchaseOrder['message']);
                }

                throw new \Exception('Erro ao buscar produtos da OC / pedido na API-SAP: ' . serialize($purchaseOrder));
            }

            $products = explode(',', $data['products']);
            $purchaseOrderProducts = $industryReference = array();
            $purchaseOrderStatus = $industry = $pnCode = $branchName = null;

            foreach ($products as $product) {
                $output->writeln(['Buscando OC do produto ' . $product]);
                $purchaseOrderProduct = $this->getContainer()->get('curl_service')->execute(
                    'GET',
                    $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $data['order_id'] . '/products/' . base64_encode(trim($product)) . '/purchase'
                );

                if (!isset($purchaseOrderProduct['code']) || (isset($purchaseOrderProduct['code']) && $purchaseOrderProduct['code'] != 200)) {
                    if (isset($purchaseOrderProduct['message'])) {
                        throw new \Exception('Erro ao buscar OC para o item na API-SAP: ' . $purchaseOrderProduct['message']);
                    }

                    throw new \Exception('Erro ao buscar OC para o item no SAP: ' . serialize($purchaseOrderProduct));
                }

                if (empty($purchaseOrderStatus)) {
                    $purchaseOrderStatus = $purchaseOrderProduct['data'][0]['docstatus'];
                }
                if (empty($industry)) {
                    $industry = $purchaseOrderProduct['data'][0]['cardname'];
                }
                if (empty($pnCode) && !empty($purchaseOrderProduct['data'][0]['cardcode'])) {
                    $pnCode = $purchaseOrderProduct['data'][0]['cardcode'];
                }
                if (empty($branchName)) {
                    $branchName = $purchaseOrderProduct['data'][0]['bplname'];
                }
                $industryReference[] = $purchaseOrderProduct['data'][0]['suppcatnum'];

                if (!empty($purchaseOrderProduct['data']) &&
                    (!empty($purchaseOrderProduct['data'][0]["potrgnum"]) || !empty($purchaseOrderProduct['data'][0]["uNumoc"]))
                    && ($purchaseOrderProduct['data'][0]["potrgnum"] == intval($data['purchase_order']) || $purchaseOrderProduct['data'][0]["uNumoc"] == intval($data['purchase_order']))) {
                    $purchaseOrderProducts[] = $product;
                    continue;
                }
            }

            if (empty($purchaseOrderProducts)) {
                $output->writeln(['Nenhum produto informado está contigo na OC ' . $data['purchase_order'] . ', destamos descartando a mensagem...', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            $normalizedIndustryReference = $this->normalizeIndustryReference(implode(',', $industryReference));
            $searchTask = $this->getContainer()->get('occurrence_service')->searchTask('jql=(project=CANCELOC and ordem_de_compra=' . $data['purchase_order'] . ' and referencia_industria ~ "' . $normalizedIndustryReference . '")');
            if (!isset($searchTask['data']['issues'])) {
                throw new \Exception('Error searching if the task was already created: ' . serialize($searchTask));
            }

            $cancellationJiraTask = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('occurrence_api_url') . '/workflow/issues/' . $data['task']
            );

            if (!isset($cancellationJiraTask['data'])) {
                throw new \Exception('Error get cancellation task: ' . serialize($cancellationJiraTask));
            }

            if($cancellationJiraTask['data']['fields']['issuetype']['name'] == "cancelamento_ocr_finalizadora"){
                $output->writeln(['CANCELA do tipo ocorrência finalizadora, não abrimos canceloc e descartamos a mensagem', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            if (!empty($searchTask['data']['issues'])) {
                $createdTaskKey = $searchTask['data']['issues'][0]['key'];
                $output->writeln(['Task ' . $createdTaskKey . ' já existente!']);
            }

            if (empty($searchTask['data']['issues']))  {
                $createdTask = $this->getContainer()->get('occurrence_service')->createTask(array(
                    'project' => 'CANCELOC',
                    'type' => ($purchaseOrderStatus == 'C' ? 'oc_fechada' : 'cancelamento_oc'),
                    'reporter' => 'ti',
                    'assignee' => 'supply chain',
                    'summary' => 'Cancelamento OC ' . $data['purchase_order'],
                    'order_id' => $data['order_id'],
                    'industry' => $industry,
                    'purchase_order' => $data['purchase_order'],
                    'pn_code' => $pnCode,
                    'branch_name' => $branchName,
                    'product_reference' => implode(',', $purchaseOrderProducts),
                    'industry_reference' => implode(',', $industryReference),
                    'description' => 'Favor verificar cancelamento da ordem de compra ' . $data['purchase_order'] . ' / Indústria = ' . $industry . ' / PN = ' . $pnCode,
                    'total_or_partial' => (count($purchaseOrder['data']) == count($purchaseOrderProducts) ? 'oc_total' : 'oc_parcial')
                ));

                if (!isset($createdTask['code']) || (isset($createdTask['code']) && $createdTask['code'] != 200)) {
                    throw new \Exception('Erro ao criar CANCELOC task: ' . serialize($createdTask));
                }

                $createdTaskKey = $createdTask['data']['key'];
                $output->writeln(['Task: ' . $createdTaskKey . ' criada com sucesso!']);
            }

            $linkedTasks = $this->getContainer()->get('occurrence_service')->linkTasks(array('name' => 'Blocks', 'task_in' => $createdTaskKey, 'task_out' => $data['task']));
            if (!isset($linkedTasks['code']) || (isset($linkedTasks['code']) && $linkedTasks['code'] != 200)) {
                throw new \Exception('Error linking child task with its parent.');
            }

            $output->writeln(['Link para ' . $createdTaskKey . ' inserido na task: ' . $data['task']]);

            // Call to cancel purchase order
            foreach ($purchaseOrderProducts as $purchaseOrderProduct) {
                $output->writeln(['Enviando OC ' . $data['purchase_order'] . ' e item ' . $purchaseOrderProduct . ' para cancelar no SAP...']);
                $this->cancelPurchaseOrderProduct($data['order_id'], $purchaseOrderProduct, $data['purchase_order'], $output);
                $output->writeln(['OC e produto enviados com sucesso! :)']);
            }

            $output->writeln(['Encerrado processamento da OC ' . $data['purchase_order'] . ' :) :) :)']);
            $output->writeln(['#############################################', '', '']);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);

        } catch (\Exception $e) {
            $output->writeln(['Falha (' . $totalRetry . ') no processamento da OC ' . $data['purchase_order'] . ': ' . $e->getMessage(), '']);

            $this->getContainer()->get('queue_service')->publishOrderMessage(
                $message,
                ($totalRetry < 5 ? 'purchase.order.retry' : 'purchase.order.fail'),
                ''
            );

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            throw $e;
        }
    }


    /**
     * Normalizes the industry reference field with two backslashes on special characters to process correctly on Jira search method
     *
     * @param string $industryReference
     * @return bool|string
     */
    private function normalizeIndustryReference($industryReference)
    {
        $newIndustryReference = '';
        $industryReferenceLength = strlen($industryReference);

        if ($industryReferenceLength > 0) {
            for ($i = 0; $i < $industryReferenceLength; $i++) {
                $newIndustryReference .= $industryReference[$i];
                if (in_array($industryReference[$i], array('[', ']', '(', ')', '{', '}', '+', '-', '&', '|', '!', '^', '~', '*', '?', '\\', ':'))) {
                    $newIndustryReference = substr($newIndustryReference, 0, -1);
                    $newIndustryReference .= '\\\\' . $industryReference[$i];
                }
            }
        }

        return $newIndustryReference;
    }

    /**
     * @param $orderId
     * @param $productReference
     * @param $purchaseOrder
     * @param OutputInterface $output
     * @throws \Exception
     */
    private function cancelPurchaseOrderProduct($orderId, $productReference, $purchaseOrder, $output)
    {
        try {
            $cancelItem = $this->getContainer()->get('curl_service')->execute(
                'PUT',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference) . '/purchase/' . $purchaseOrder . '/cancel'
            );

            if (!isset($cancelItem['code']) || (isset($cancelItem['code']) && $cancelItem['code'] != 200)) {
                if (isset($cancelItem['message'])) {
                    $output->writeln(['Erro ao cancelar o item da OC no SAP: ' . $cancelItem['message']]);
                    throw new \Exception('Erro ao cancelar o item da OC no SAP: ' . $cancelItem['message']);
                }

                $output->writeln(['Erro ao cancelar o item no SAP.']);
                throw new \Exception('Erro ao cancelar o item da OC no SAP.');
            }
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
