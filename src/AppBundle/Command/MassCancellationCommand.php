<?php

namespace AppBundle\Command;

use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MassCancellationCommand
 * @package AppBundle\Command
 */
class MassCancellationCommand extends ContainerAwareCommand
{
    /**
     * Configure command method
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:mass-cancellation')

            // the short description shown while running "php bin/console list"
            ->setDescription('Open cancellation tasks on Jira.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to open cancellation tasks on Jira...")
        ;
    }

    /**
     * Execute command method
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '===================',
            ' Mass Cancellation ',
            '===================',
            '',
        ]);

        $this->getContainer()->get('queue_service')->getMsgs('mass.cancellation', '', 1, array($this, 'openJira'));
        $output->writeln(['Fim da abertura do Jira :)', '']);
    }

    /**
     * Cancel order method
     *
     * @param AMQPMessage $message
     * @throws \Exception
     */
    public function openJira($message)
    {
        // Total retry
        $totalRetry = 0;
        $props = $message->get_properties();
        $output = new ConsoleOutput();

        if (isset($props['application_headers'])) {
            $headers = $message->get('application_headers')->getNativeData();
            if (isset($headers['x-death'])) {
                array_walk_recursive(
                    $headers['x-death'],
                    function ($vHeader, $kHeader, $uHeader) {
                        if ($kHeader === $uHeader[0]) {
                            $uHeader[1] = $vHeader;
                        }
                    },
                    ['count', &$totalRetry]
                );
            }
        }

        $output->writeln(['Message: ' . $message->body]);

        $data = json_decode($message->body, true);
        $output->writeln(['Processing: ' . $data['order_id'] . ' - Retry: ' . $totalRetry]);

        try {
            $createdTask = $this->getContainer()->get('occurrence_service')->createTask($data);

            if (!isset($createdTask['code']) || (isset($createdTask['code']) && $createdTask['code'] != 200)) {
                throw new \Exception('Error creating cancellation task: ' . serialize($createdTask));
            }

            $output->writeln(['Task: ' . $createdTask['data']['key'] . ' created successfully!']);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        } catch (\Exception $exception) {
            $output->writeln(['Fail (' . $totalRetry . ') on opening Jira task process ' . $data['order_id'] . ': ' . $exception->getMessage(), '']);

            $this->getContainer()->get('queue_service')->publishMessage(
                $message,
                ($totalRetry < 5 ? 'mass.cancellation.retry' : 'mass.cancellation.fail'),
                'mass_cancellation'
            );

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        }
    }
}
