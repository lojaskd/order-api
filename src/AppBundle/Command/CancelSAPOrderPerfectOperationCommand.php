<?php

namespace AppBundle\Command;

use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CancelSAPOrderPerfectOperationCommand
 * @package AppBundle\Command
 */
class CancelSAPOrderPerfectOperationCommand extends ContainerAwareCommand
{

    /**
     * Method to configure the command
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:cancel-sap-order-perfect-operation')

            // the short description shown while running "php bin/console list"
            ->setDescription('Cancels SAP orders - Perfect Operation.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to cancel SAP orders with new process...")
        ;
    }

    /**
     * Method to execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '==========================================',
            'SAP Order Cancellation - Perfect Operation',
            '==========================================',
            '',
        ]);

        $this->getContainer()->get('queue_service')->getMsgs('sap.order.cancellation', '', 1, array($this, 'cancelSapOrder'));

        $output->writeln(['Fim do cancelamento do pedido no SAP!', '']);
    }

    /**
     * @param AMQPMessage $message
     * @throws \Exception
     */
    public function cancelSapOrder($message)
    {
        // Total retry
        $totalRetry = 0;
        $props = $message->get_properties();
        $output = new ConsoleOutput();

        // Get total retry to headers
        if (isset($props['application_headers'])) {
            $headers = $message->get('application_headers')->getNativeData();
            if (isset($headers['x-death'])) {
                array_walk_recursive(
                    $headers['x-death'],
                    function ($vHeader, $kHeader, $uHeader) {
                        if ($kHeader === $uHeader[0]) {
                            $uHeader[1] = $vHeader;
                        }
                    },
                    ['count', &$totalRetry]
                );
            }
        }

        $output->writeln(['Mensagem: ' . $message->body]);
        $data = json_decode($message->body, true);
        $output->writeln(['Processing: ' . $data['order_id'] . ' - Retry: ' . $totalRetry]);

        try {
            $order = $this->getContainer()->get('order_service')->getOrderInfo($data['order_id']);

            /*
             ***************************************************************************************
             * Verificamos se os dados da mensagem são suficientes para executarmos o cancelamento *
             ***************************************************************************************
             */
            if (!$this->checkValidMessage($data, $order, $output)) {
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            $invoiceProducts = $purchaseOrdersProducts = $branches = array();
            $countItems = 0;

            /*
             **************************************
             * Buscamos a filial do pedido no SAP *
             **************************************
             */
            $branchId = $this->getSapOrderBranch($data['order_id'], $data['ticket'], $output, $totalRetry);
            if ($branchId === false) {
                throw new \Exception('Pedido não integrado no SAP.');
            }

            foreach ($data['products'] as $product) {
                /*
                 ***********************************************************************************************
                 * Verificamos se o produto está aprovado e se temos sua referência para poder cancelar no SAP *
                 ***********************************************************************************************
                 */
                if (!$productInfo = $this->checkValidProduct($data['order_id'], $product, $data['ticket'], $output)) {
                    continue;
                }

                /*
                 *******************************************************************************************************
                 * Buscamos se o produto possui uma nota fiscal atrelada a ele, se houver não efetuamos o cancelamento *
                 *******************************************************************************************************
                 */
                $invoiceKey = $this->checkProductInvoice($data['order_id'], $productInfo['prd_referencia'], $data['ticket'], $output);
                if (!empty($invoiceKey)) {
                    if (!isset($invoiceProducts[$invoiceKey])) {
                        $invoiceProducts[$invoiceKey] = array();
                    }
                    $invoiceProducts[$invoiceKey][] = $productInfo['prd_referencia'];
                    continue;
                }

                $output->writeln(['Buscando produto no SAP para ver se já se encontra fechado...']);

                /*
                 ****************************************
                 * Search for order product info on SAP *
                 ****************************************
                 */
                $sapProduct = $this->getSapProduct($data['order_id'], $productInfo['prd_referencia'], $output);

                /*
                 **************************************************
                 * Format product purchase order to send to queue *
                 **************************************************
                 */
                if ((isset($sapProduct['potrgnum']) && !empty($sapProduct['potrgnum'])) || (isset($sapProduct['uNumoc']) && !empty($sapProduct['uNumoc']))) {
                    $productPurchaseOrder = isset($sapProduct['potrgnum']) && !empty($sapProduct['potrgnum']) ? $sapProduct['potrgnum'] : $sapProduct['uNumoc'];
                    $output->writeln(['Ordem de compra do produto ' . $productInfo['prd_referencia'] . ': ' . $productPurchaseOrder]);
                    if (!isset($purchaseOrdersProducts[$productPurchaseOrder])) {
                        $purchaseOrdersProducts[$productPurchaseOrder] = array();
                    }
                    $purchaseOrdersProducts[$productPurchaseOrder][] = $productInfo['prd_referencia'];
                }
                if ($sapProduct['treetype'] == 'S') {
                    $items = $this->getSapProductItems($data['order_id'], $productInfo['prd_referencia'], $output);
                    foreach ($items as $item) {
                        if ((isset($item['potrgnum']) && !empty($item['potrgnum'])) || (isset($item['uNumoc']) && !empty($item['uNumoc']))) {
                            $itemPurchaseOrder = (isset($item['potrgnum']) && !empty($item['potrgnum'])) ? $item['potrgnum'] : $item['uNumoc'];
                            if (!isset($purchaseOrdersProducts[$itemPurchaseOrder])) {
                                $purchaseOrdersProducts[$itemPurchaseOrder] = array();
                            }
                            $purchaseOrdersProducts[$itemPurchaseOrder][] = $item['itemcode'];
                        }
                    }
                }

                /*
                 ************************************************
                 * Checks if the product is already cancelled:  *
                 * YES: Go to the next product                  *
                 * NO: Cancel It on SAP                         *
                 ************************************************
                 */
                if ($this->checkProductStatus($sapProduct, $data['ticket']) === 1) {
                    $output->writeln(['Product ' . $productInfo['prd_referencia'] . ' already cancelled on SAP.']);
                    $branches[] = $sapProduct['bplid'];
                    $countItems++;
                    continue;
                }

                /*
                 *************************************************
                 * Fazemos a chamada para cancelar o item no SAP *
                 *************************************************
                 */
                $output->writeln(['Iniciando cancelamento do item.']);
                $this->cancelItem($data['order_id'], $productInfo['prd_referencia'], $data['ticket'], $output);

                $output->writeln(['Item cancelado com sucesso :)']);
                $countItems++;
                $branches[] = $sapProduct['bplid'];

                $output->writeln(['Item concluído :)']);
            }

            $branches = array_unique($branches);

            /*
             *********************************************************************
             * Send the message to shipment queue to do workflow process actions *
             *********************************************************************
             */
            if (!empty($invoiceProducts)) {
                $hasParent = false;
                if (count($invoiceProducts) > 1) {
                    $hasParent = true;
                }

                foreach ($invoiceProducts as $invoiceId => $products) {
                    $this->sendShipment($data, $invoiceId, $products, $output, $hasParent);
                }
            }

            /*
             *************************************************************************
             * Send the message to purchase order queue to cancel the purchase order *
             *************************************************************************
             */
            if (!empty($purchaseOrdersProducts)) {
                foreach ($purchaseOrdersProducts as $purchaseOrder => $products) {
                    $this->sendPurchaseOrder($data, $purchaseOrder, $products, $output);
                }
            }

            /*
             *******************************************************************************************************************
             * Se nenhum produto foi cancelado, apenas descartamos a mensagem, não tentamos cancelar/fechar o pedido da filial *
             *******************************************************************************************************************
             */
            if (empty($branches)) {
                $output->writeln(['Nenhum pedido de filial será cancelado/fechado no SAP, encerrando processamento :) :) :)', '', '']);
            }

            if (!empty($branches)) {

                /*
                 ************************************************************
                 * Fazemos as atualizações necessárias no ticket do Zendesk *
                 ************************************************************
                 */
                if (count($data['products']) == $countItems) {
                    $this->updateReadyToReverse($data['task'], $output);
                }

                $output->writeln(['Atualizado reversa no jira :) :) :)']);
            }

            $output->writeln(['#############################################', '', '']);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            return;
        } catch (\Exception $e) {
            $output->writeln(['Falha (' . $totalRetry . ') no cancelamento de pedido ' . $data['order_id'] . ': ' . $e->getMessage(), '']);

            $this->getContainer()->get('queue_service')->publishOrderMessage(
                $message,
                ($totalRetry < 5 ? 'sap.order.cancellation.retry' : 'sap.order.cancellation.fail'),
                ''
            );

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            throw $e;
        }
    }

    /**
     * @param $orderId
     * @param $ticketId
     * @param OutputInterface $output
     * @param integer $retryCount
     * @return bool
     * @throws \Exception
     */
    private function getSapOrderBranch($orderId, $ticketId, $output, $retryCount)
    {
        try {
            $branchOrder = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId
            );

            if (!isset($branchOrder['code']) || (isset($branchOrder['code']) && $branchOrder['code'] != 200)) {
                if (isset($branchOrder['message'])) {
                    $output->writeln(['Erro ao buscar filial do pedido na API-SAP: ' . $branchOrder['message']]);
                    throw new \Exception('Erro ao buscar filial do pedido na API-SAP: ' . $branchOrder['message']);
                }

                $output->writeln(['Erro ao buscar filial do pedido na API-SAP.']);
                throw new \Exception('Erro ao buscar filial do pedido na API-SAP.');
            }

            if (empty($branchOrder['data'])) {
                $output->writeln(['Pedido não encontrado no SAP...']);

                if ($retryCount < 2) {
                    $this->getContainer()->get('occurrence_service')->commentTicket(
                        $ticketId,
                        'Cancelamento Automático - Pedido ' . $orderId . ' não integrado no SAP.',
                        false
                    );
                }
                return false;
            }

            return intval($branchOrder['data']['bplid']);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $data
     * @param $order
     * @param OutputInterface $output
     * @return bool
     */
    private function checkValidMessage($data, $order, $output)
    {
        if (!isset($data['order_id'])) {
            $output->writeln(['Numero de pedido inválido.']);
            $output->writeln(['#############################################', '', '']);
            return false;
        }

        if (empty($order)) {
            $output->writeln(['Pedido não encontrado, descartando mensagem...']);
            $output->writeln(['#############################################', '', '']);
            return false;
        }

        if (empty($order['realizada_aprovacao'])) {
            $output->writeln(['Pedido não foi aprovado (não integrado no SAP), descartando mensagem...', '', '']);
            return false;
        }

        return true;
    }

    /**
     * @param $orderId
     * @param $product
     * @param $ticketId
     * @param OutputInterface $output
     * @return array|bool
     * @throws \Exception
     */
    private function checkValidProduct($orderId, $product, $ticketId, $output)
    {
        try {
            $output->writeln(['Iniciando cancelamento do pedido ' . $orderId . ' e item ' . $product]);
            $productInfo = $this->getContainer()->get('product_service')->getProductOrderInfo($orderId, $product);

            if (empty($productInfo['data_apr_r'])) {
                $output->writeln(['Produto não foi aprovado (não integrado no SAP), prosseguindo com os demais produtos...']);
                return false;
            }

            /*
             *******************************************************************************************************************
             * Se a referência do produto no pedido for vazia, procuramos o produto na API-Produtos para buscar sua referência *
             *******************************************************************************************************************
             */
            if (empty($productInfo['prd_referencia'])) {
                $productDB = $this->getContainer()->get('product_api_service')->getProductInfo($product);
                if (isset($productDB['referencia'])) {
                    $productInfo['prd_referencia'] = $productDB['referencia'];
                }
            }

            /*
             *******************************************************************************
             * Se a API-Produtos não retornar a referência, avisamos no Zendesk o problema *
             *******************************************************************************
             */
            if (empty($productInfo['prd_referencia'])) {
                $this->getContainer()->get('occurrence_service')->commentTicket(
                    $ticketId,
                    'Ocorreu um erro ao cancelar pedido no SAP, a referência do produto não foi encontrada no pedido e no seu cadastro.',
                    false
                );

                return false;
            }

            return $productInfo;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $orderId
     * @param $productReference
     * @param $ticketId
     * @param OutputInterface $output
     * @return bool
     * @throws \Exception
     */
    private function checkProductInvoice($orderId, $productReference, $ticketId, $output)
    {
        try {
            $orderProductInvoice = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference) . '/invoice'
            );

            $output->writeln(['Encode da referência do produto: ' . base64_encode($productReference)]);

            if (!isset($orderProductInvoice['code']) || (isset($orderProductInvoice['code']) && $orderProductInvoice['code'] != 200)) {
                if (isset($orderProductInvoice['message'])) {
                    $output->writeln(['Erro ao buscar nota fiscal para o item na API-SAP: ' . $orderProductInvoice['message']]);
                    throw new \Exception('Erro ao buscar nota fiscal para o item na API-SAP: ' . $orderProductInvoice['message']);
                }

                $output->writeln(['Erro ao buscar nota fiscal para o item no SAP.']);
                throw new \Exception('Erro ao buscar nota fiscal para o item no SAP: ' . serialize($orderProductInvoice));
            }

            if (!empty($orderProductInvoice['data'])) {
                $output->writeln(
                    [
                        'Produto possui nota fiscal de saída, nenhum cancelamento será realizado para ele.',
                        'Setando mensagem de não liberação do cancelamento no SAP por possuir NF de saída - Ticket ' . $ticketId
                    ]
                );

                $this->getContainer()->get('occurrence_service')->commentTicket(
                    $ticketId,
                    'Cancelamento Automático - Produto ' . $productReference . ' possui nota fiscal de saída, por isso não será cancelado no SAP automaticamente.',
                    false
                );

                return $orderProductInvoice['data'][0]['serial'];
            }

            return false;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $product
     * @param $ticketId
     * @return int
     * @throws \Exception
     */
    private function checkProductStatus($product, $ticketId)
    {
        try {
            if (isset($product['linestatus'])) {
                if ($product['linestatus'] == 'C') {
                    $this->getContainer()->get('occurrence_service')->commentTicket(
                        $ticketId,
                        'Cancelamento Automático - Produto ' . $product['itemcode'] . ' já se encontra fechado.',
                        false
                    );

                    return 1;
                }
            }

            return 0;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $orderId
     * @param $productReference
     * @param OutputInterface $output
     * @return array
     * @throws \Exception
     */
    private function getSapProduct($orderId, $productReference, $output)
    {
        $orderProduct = $this->getContainer()->get('curl_service')->execute(
            'GET',
            $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference)
        );

        if (!isset($orderProduct['code']) || (isset($orderProduct['code']) && $orderProduct['code'] != 200)) {
            if (isset($orderProduct['message'])) {
                $output->writeln(['Erro ao buscar produto do pedido na API-SAP: ' . $orderProduct['message']]);
                throw new \Exception('Erro ao buscar produto do pedido na API-SAP: ' . $orderProduct['message']);
            }

            $output->writeln(['Erro ao buscar produto do pedido na API-SAP.']);
            throw new \Exception('Erro ao buscar produto do pedido na API-SAP.');
        }

        return isset($orderProduct['data'][0]) ? $orderProduct['data'][0] : array();
    }

    /**
     * Gets kit sap items from order ID and product Reference
     *
     * @param $orderId
     * @param $productReference
     * @param OutputInterface $output
     * @return array
     * @throws \Exception
     */
    private function getSapProductItems($orderId, $productReference, $output)
    {
        $orderProduct = $this->getContainer()->get('curl_service')->execute(
            'GET',
            $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference) . '/items'
        );

        if (!isset($orderProduct['code']) || (isset($orderProduct['code']) && $orderProduct['code'] != 200)) {
            if (isset($orderProduct['message'])) {
                $output->writeln(['Erro ao buscar items do kit sap na API-SAP: ' . $orderProduct['message']]);
                throw new \Exception('Erro ao buscar items do kit sap na API-SAP: ' . $orderProduct['message']);
            }

            $output->writeln(['Erro ao buscar items do kit sap na API-SAP.']);
            throw new \Exception('Erro ao buscar items do kit sap na API-SAP.');
        }

        return isset($orderProduct['data']) ? $orderProduct['data'] : array();
    }

    /**
     * @param $orderId
     * @param $productReference
     * @param $ticketId
     * @param OutputInterface $output
     * @return bool
     * @throws \Exception
     */
    private function cancelItem($orderId, $productReference, $ticketId, $output)
    {
        try {
            $cancelItem = $this->getContainer()->get('curl_service')->execute(
                'PUT',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference) . '/cancel'
            );

            if (!isset($cancelItem['code']) || (isset($cancelItem['code']) && $cancelItem['code'] != 200)) {
                if (isset($cancelItem['message'])) {
                    $output->writeln(['Erro ao cancelar o item: ' . $cancelItem['message']]);
                    throw new \Exception('Erro ao cancelar o item: ' . $cancelItem['message']);
                }

                $output->writeln(['Erro ao cancelar o item no SAP.']);
                throw new \Exception('Erro ao cancelar o item no SAP.');
            }

            $this->getContainer()->get('occurrence_service')->commentTicket(
                $ticketId,
                'Cancelamento Automático - Produto ' . $productReference . ' foi adicionado ao processo de cancelamento no SAP.',
                false
            );

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Sends a massage to purchase order queue
     *
     * @param array $data
     * @param array $products
     * @param string $purchaseOrder
     * @param OutputInterface $output
     * @throws \Exception
     */
    private function sendPurchaseOrder($data, $purchaseOrder, $products, $output)
    {
        try {
            $output->writeln(['Enviando mensagem para a fila de cancelamento de OC...']);

            // Publish message on Purchase Order queue to cancel It
            $this->getContainer()->get('queue_service')->publishPurchaseCancellationMsg(
                array(
                    'order_id' => $data['order_id'],
                    'products' => implode(',', $products),
                    'purchase_order' => $purchaseOrder,
                    'ticket' => $data['ticket'],
                    'task' => $data['task']
                )
            );
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Transitions Jira task to ready to reverse status
     *
     * @param $taskKey
     * @param OutputInterface $output
     * @throws \Exception
     */
    private function updateReadyToReverse($taskKey, $output)
    {
        try {
            $task = $this->getContainer()->get('occurrence_service')->getTask($taskKey);
            if (!isset($task['data']['fields'])) {
                throw new \Exception('Error getting task on Jira: ' . serialize($task));
            }

            $transitions = array();
            if (isset($task['data']['transitions']) && !empty($task['data']['transitions'])) {
                foreach ($task['data']['transitions'] as $transition) {
                    $transitions[$transition['name']] = $transition['id'];
                }
            }

            $output->writeln(['Transicionando Jira para cancelado_sap...']);
            if (array_key_exists('cancelado_sap', $transitions)) {
                $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($taskKey, $transitions['cancelado_sap']);

                if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                    throw new \Exception('Error transitioning task to cancelado_sap: ' . serialize($transitionAction));
                }
            }

            $output->writeln(['Jira transicionado com sucesso.']);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $data
     * @param $invoiceId
     * @param $products
     * @param OutputInterface $output
     * @param $hasParent
     */
    private function sendShipment($data, $invoiceId, $products, $output, $hasParent)
    {
        $output->writeln(['Enviando mensagem para a fila de embarque...']);
        // Publish message on Shipment Order queue
        $this->getContainer()->get('queue_service')->publishOrderShipmentMsg(
            array(
                'order_id' => $data['order_id'],
                'invoice_id' => $invoiceId,
                'products' => $products,
                'ticket' => $data['ticket'],
                'task' => $data['task'],
                'parent_task' => $hasParent,
            )
        );
    }
}
