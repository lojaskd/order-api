<?php

namespace AppBundle\Command;

use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CancelSAPOrderCommand
 * @package AppBundle\Command
 */
class CancelSAPOrderCommand extends ContainerAwareCommand
{

    /**
     * @var array
     */
    private $ticketStatus = array(
        'new' => 'novo',
        'open' => 'aberto',
        'closed' => 'fechado',
        'pending' => 'pendente',
        'hold' => 'em espera',
        'solved' => 'resolvido'
    );


    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:cancel-sap-order')

            // the short description shown while running "php bin/console list"
            ->setDescription('Cancels SAP orders.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to cancel SAP orders...")
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '======================',
            'SAP Order Cancellation',
            '======================',
            '',
        ]);

        $this->getContainer()->get('queue_service')->getMsgs('sap.order.item.cancellation', 'SapOrderItemCancellation', 1, array($this, 'cancelSapOrder'));

        $output->writeln(['Fim do cancelamento do pedido no SAP!', '']);
    }

    /**
     * @param AMQPMessage $message
     * @throws \Exception
     */
    public function cancelSapOrder($message)
    {
        // Total retry
        $totalRetry = 0;
        $props = $message->get_properties();
        $output = new ConsoleOutput();

        // Get total retry to headers
        if (isset($props['application_headers'])) {
            $headers = $message->get('application_headers')->getNativeData();
            if (isset($headers['x-death'])) {
                array_walk_recursive(
                    $headers['x-death'],
                    function ($vHeader, $kHeader, $uHeader) {
                        if ($kHeader === $uHeader[0]) {
                            $uHeader[1] = $vHeader;
                        }
                    },
                    ['count', &$totalRetry]
                );
            }
        }

        $output->writeln(['Mensagem: ' . $message->body]);

        $data = json_decode($message->body, true);
        $output->writeln(['Processing: ' . $data['order_id'] . ' - Retry: ' . $totalRetry]);

        try {
            if (!isset($data['order_id'])) {
                $output->writeln(['Numero de pedido inválido.']);
                $output->writeln(['#############################################', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            if (!isset($data['cancelled']) || empty($data['cancelled'])) {
                $output->writeln(['Mensagem não está setada para cancelamento (cancelled = false).']);
                $output->writeln(['#############################################', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            if (isset($data['clerk']) && trim($data['clerk']) == "TI - Cancelamento Automatico") {
                $output->writeln(['Cancelamento automático, descartando mensagem...']);
                $output->writeln(['#############################################', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            $order = $this->getContainer()->get('order_service')->getOrderInfo($data['order_id']);

            if (empty($order)) {
                $output->writeln(['Pedido não encontrado, descartando mensagem...']);
                $output->writeln(['#############################################', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            if (empty($order['realizada_aprovacao'])) {
                $output->writeln(['Pedido não foi aprovado (não integrado no SAP), descartando mensagem...', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            preg_match("/\#\d{7,12}/", $data['ticket'], $matchesTicket);
            $ticketId = intval($data['ticket']);
            if ($matchesTicket) {
                $ticketId = intval(str_replace("#", "", $data['ticket']));
            }

            $countItems = 0;
            $productsWithError = array();

            /**************************************
             * Buscamos a filial do pedido no SAP *
             **************************************/
            $branchOrder = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $data['order_id']
            );

            if (!isset($branchOrder['code']) || (isset($branchOrder['code']) && $branchOrder['code'] != 200)) {
                if (isset($branchOrder['message'])) {
                    $output->writeln(['Erro ao buscar filial do pedido na API-SAP: ' . $branchOrder['message']]);
                    throw new \Exception('Erro ao buscar filial do pedido na API-SAP: ' . $branchOrder['message']);
                }

                $output->writeln(['Erro ao buscar filial do pedido na API-SAP.']);
                throw new \Exception('Erro ao buscar filial do pedido na API-SAP.');
            }

            if (empty($branchOrder['data'])) {
                $output->writeln(['Pedido não encontrado no SAP.']);

                if ($totalRetry < 2) {
                    $this->addTicketComment(
                        $ticketId,
                        'Cancelamento Automático - Pedido ' . $data['order_id'] . ' não integrado no SAP.',
                        false,
                        $output
                    );
                }

                throw new \Exception('Pedido não encontrado no SAP.');
            }

            $branchId = $branchOrder['data']['bplid'];

            foreach ($data['products'] as $product) {
                /*********************************************************************
                 * Verificamos se o produto está aprovado para poder cancelar no SAP *
                 *********************************************************************/
                $output->writeln(['Iniciando cancelamento do pedido ' . $data['order_id'] . ' e item ' . $product['id']]);
                $productInfo = $this->getContainer()->get('product_service')->getProductOrderInfo($data['order_id'], $product['id']);

                if (empty($productInfo['data_apr_r'])) {
                    $productsWithError[] = $product['prd_referencia'];
                    $output->writeln(['Produto não foi aprovado (não integrado no SAP), prosseguindo com os demais produtos...']);
                    continue;
                }

                /*******************************************************************************************************************
                 * Se a referência do produto no pedido for vazia, procuramos o produto na API-Produtos para buscar sua referência *
                 *******************************************************************************************************************/
                if (empty($productInfo['prd_referencia'])) {
                    $product = $this->getContainer()->get('product_api_service')->getProductInfo($product['id']);
                    if (isset($product['referencia'])) {
                        $productInfo['prd_referencia'] = $product['referencia'];
                    }
                }

                /*******************************************************************************
                 * Se a API-Produtos não retornar a referência, avisamos no Zendesk o problema *
                 *******************************************************************************/
                if (empty($productInfo['prd_referencia'])) {
                    $this->addTicketComment(
                        $ticketId,
                        'Ocorreu um erro ao cancelar o pedido no SAP, a referência do produto não foi encontrada no pedido e no seu cadastro.',
                        false,
                        $output
                    );

                    continue;
                }

                /*******************************************************************************************************
                 * Buscamos se o produto possui uma nota fiscal atrelada a ele, se houver não efetuamos o cancelamento *
                 *******************************************************************************************************/
                $orderProductInvoice = $this->getContainer()->get('curl_service')->execute(
                    'GET',
                    $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $data['order_id'] . '/products/' . base64_encode($productInfo['prd_referencia']) . '/invoice'
                );

                $output->writeln(['Encode da referência do produto: ' . base64_encode($productInfo['prd_referencia'])]);

                if (!isset($orderProductInvoice['code']) || (isset($orderProductInvoice['code']) && $orderProductInvoice['code'] != 200)) {
                    $productsWithError[] = $product['prd_referencia'];

                    if (isset($orderProductInvoice['message'])) {
                        $output->writeln(['Erro ao buscar nota fiscal para o item na API-SAP: ' . $orderProductInvoice['message']]);
                        throw new \Exception('Erro ao buscar nota fiscal para o item na API-SAP: ' . $orderProductInvoice['message']);
                    }

                    $output->writeln(['Erro ao buscar nota fiscal para o item no SAP.']);
                    throw new \Exception('Erro ao buscar nota fiscal para o item no SAP: ' . serialize($orderProductInvoice));
                }

                if (!empty($orderProductInvoice['data'])) {
                    $productsWithError[] = $product['prd_referencia'];

                    $output->writeln(
                        [
                            'Produto possui nota fiscal de saída, nenhum cancelamento será realizado para ele.',
                            'Setando mensagem de não liberação do cancelamento no SAP por possuir NF de saída - Ticket ' . $ticketId
                        ]
                    );

                    $this->addTicketComment(
                        $ticketId,
                        'Cancelamento Automático - Produto ' . $productInfo['prd_referencia'] . ' possui nota fiscal de saída, por isso não será cancelado no SAP automaticamente. Favor realizar o cancelamento manual.',
                        false,
                        $output
                    );

                    continue;
                }

                $output->writeln(['Produto não possui nota fiscal de saída ativa, buscando produto para ver se já se encontra fechado...']);

                /****************************************************
                 * Buscamos informações do produto do pedido no SAP *
                 ****************************************************/
                $orderProduct = $this->getContainer()->get('curl_service')->execute(
                    'GET',
                    $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $data['order_id'] . '/products/' . base64_encode($productInfo['prd_referencia'])
                );

                if (!isset($orderProduct['code']) || (isset($orderProduct['code']) && $orderProduct['code'] != 200)) {
                    $productsWithError[] = $product['prd_referencia'];

                    if (isset($orderProduct['message'])) {
                        $output->writeln(['Erro ao buscar produto do pedido na API-SAP: ' . $orderProduct['message']]);
                        throw new \Exception('Erro ao buscar produto do pedido na API-SAP: ' . $orderProduct['message']);
                    }

                    $output->writeln(['Erro ao buscar produto do pedido na API-SAP.']);
                    throw new \Exception('Erro ao buscar produto do pedido na API-SAP.');
                }

                if (empty($orderProduct['data'])) {
                    $productsWithError[] = $product['prd_referencia'];
                    $output->writeln(['Produto não encontrado no SAP, encerrando cancelamento do item.']);

                    $this->addTicketComment(
                        $ticketId,
                        'Cancelamento Automático - Produto ' . $productInfo['prd_referencia'] . ' não foi encontrado no SAP, favor verificar manualmente.',
                        false,
                        $output
                    );

                    continue;
                }

                if (isset($orderProduct['data'][0]['linestatus'])) {
                    if ($orderProduct['data'][0]['linestatus'] == 'C') {
                        $this->addTicketComment(
                            $ticketId,
                            'Cancelamento Automático - Produto ' . $productInfo['prd_referencia'] . ' já se encontra fechado.',
                            false,
                            $output
                        );

                        $countItems++;
                        continue;
                    }
                }

                $output->writeln(['Iniciando cancelamento do item...']);

                /*************************************************
                 * Fazemos a chamada para cancelar o item no SAP *
                 *************************************************/
                $cancelItem = $this->getContainer()->get('curl_service')->execute(
                    'PUT',
                    $this->getContainer()->getParameter('erp_integration_url') . '/orders/' . $data['order_id'] . '/branches/' . $productInfo['id_filial_sap'] .  '/products/' . base64_encode($productInfo['prd_referencia']) . '/status/cancel'
                );

                if (!isset($cancelItem['code']) || (isset($cancelItem['code']) && $cancelItem['code'] != 200)) {
                    $productsWithError[] = $product['prd_referencia'];
                    if (isset($cancelItem['message'])) {
                        $output->writeln(['Erro ao cancelar o item: ' . $cancelItem['message']]);
                        throw new \Exception('Erro ao cancelar o item: ' . $cancelItem['message']);
                    }

                    $output->writeln(['Erro ao cancelar o item no SAP.']);
                    throw new \Exception('Erro ao cancelar o item no SAP.');
                }

                $this->addTicketComment(
                    $ticketId,
                    'Cancelamento Automático - Produto ' . $productInfo['prd_referencia'] . ' foi cancelado com sucesso no SAP.',
                    false,
                    $output
                );

                $output->writeln(['Item cancelado com sucesso :)']);
                $countItems++;
            }

            /*******************************************************************************************************************
             * Se nenhum produto foi cancelado, apenas descartamos a mensagem, não tentamos cancelar/fechar o pedido da filial *
             *******************************************************************************************************************/
            if ($countItems == 0) {
                $output->writeln(['Nenhum pedido de filial será cancelado/fechado no SAP, encerrando processamento :) :) :)', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            /*******************************************************************************************************
             * Buscamos se há algum item que não está como fechado no SAP para o pedido e filial em questão.       *
             * Se houver, não cancelamos/fechamos o pedido.                                                        *
             *******************************************************************************************************/
            $output->writeln(['Buscando na API-SAP se é possível cancelar/fechar o pedido ' . $data['order_id'] . ' para a filial ' . $branchId . '...']);
            $canCancelOrderBranch = true;
            $canCancelOrder = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $data['order_id'] . '/branches/' . $branchId . '/cancel'
            );

            if (!isset($canCancelOrder['code']) || (isset($canCancelOrder['code']) && $canCancelOrder['code'] != 200)) {
                if (isset($canCancelOrder['message'])) {
                    $output->writeln(['Erro ao pesquisar se é possível cancelar o pedido no SAP: ' . $canCancelOrder['message']]);
                    throw new \Exception('Erro ao buscar pedido para a filial na API-SAP: ' . $canCancelOrder['message']);
                }

                $output->writeln(['Erro ao pesquisar se é possível cancelar o pedido no SAP.']);
                throw new \Exception('Erro ao buscar pedido para a filial na API-SAP.');
            }

            if (!isset($canCancelOrder['data']) || (isset($canCancelOrder['data']) && !empty($canCancelOrder['data']))) {
                $output->writeln(['Não é possível cancelar e/ou fechar o pedido no SAP pois há items ainda não fechados, encerrando o processamento...']);
                $canCancelOrderBranch = false;
            }

            if ($canCancelOrderBranch === true) {
                $output->writeln(['Pedido pode ser cancelado/fechado no SAP, iniciando cancelamento do pedido ...']);

                /********************************************************************
                 * Fazemos a chamada para cancelar/fechar o pedido da filial no SAP *
                 ********************************************************************/
                $cancelOrderBranch = $this->getContainer()->get('curl_service')->execute(
                    'PUT',
                    $this->getContainer()->getParameter('erp_integration_url') . '/orders/' . $data['order_id'] . '/branches/' . $branchId . '/status/cancel'
                );

                if (!isset($cancelOrderBranch['code']) || (isset($cancelOrderBranch['code']) && $cancelOrderBranch['code'] != 200)) {
                    if (isset($cancelOrderBranch['message'])) {
                        $output->writeln(['Erro ao cancelar o pedido da filial: ' . $cancelOrderBranch['message']]);
                        throw new \Exception('Erro ao cancelar o pedido da filial: ' . $cancelOrderBranch['message']);
                    }

                    $output->writeln(['Erro ao cancelar o pedido da filial.']);
                    throw new \Exception('Erro ao cancelar o pedido da filial.');
                }

                $output->writeln(['Pedido da filial cancelado com sucesso :)']);
            }

            /************************************************************
             * Fazemos as atualizações necessárias no ticket do Zendesk *
             ************************************************************/
            if (count($data['products']) == $countItems) {
                $occurrence = $this->getContainer()->get('curl_service')->execute(
                    'GET',
                    $this->getContainer()->getParameter('occurrence_api_url') . '/occurrences/' . $ticketId
                );

                $ticketStatus = 'aberto';
                if (isset($occurrence['data']['status'])) {
                    $ticketStatus = isset($this->ticketStatus[$occurrence['data']['status']]) ? $this->ticketStatus[$occurrence['data']['status']] : 'aberto';
                }

                $occurrenceUpdate = array(
                    'occurrences' => array(
                        array(
                            'ticket_id' => $ticketId,
                            'status' => $ticketStatus,
                            'reversal_approved' => true
                        )
                    )
                );

                $output->writeln(['Atualizando ticket ' . $ticketId . ' no Zendesk...']);
                $updateOccurrence = $this->getContainer()->get('curl_service')->execute(
                    'PUT',
                    $this->getContainer()->getParameter('occurrence_api_url') . '/occurrences',
                    json_encode($occurrenceUpdate)
                );

                if ((isset($updateOccurrence[0]) && $updateOccurrence[0] === true)) {
                    $output->writeln(['Ocorrência atualizada com sucesso no Zendesk.']);
                } else {
                    $output->writeln(['Ocorreu um erro ao atualizar a ocorrência no Zendesk.']);
                }
            }

            $output->writeln(['Pedido cancelado com sucesso no SAP :) :) :)']);
            $output->writeln(['#############################################', '', '']);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        } catch (\Exception $e) {
            $output->writeln(['Falha (' . $totalRetry . ') no cancelamento de pedido ' . $data['order_id'] . ': ' . $e->getMessage(), '']);

            if ($totalRetry < 5) {
                $this->getContainer()->get('queue_service')->publishMessage(
                    $message,
                    $this->getContainer()->getParameter('sap_retry_exchange'),
                    $this->getContainer()->getParameter('sap_retry_route')
                );
            } else {
                // Message error
                $this->getContainer()->get('queue_service')->publishMessage(
                    $message,
                    $this->getContainer()->getParameter('sap_retry_exchange'),
                    $this->getContainer()->getParameter('sap_fail_route')
                );
            }

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);

            throw $e;
        }
    }

    /**
     * @param $ticketId
     * @param $comment
     * @param $type
     * @param OutputInterface $output
     * @return bool
     * @throws \Exception
     */
    private function addTicketComment($ticketId, $comment, $type, $output)
    {
        try {
            $comment = array(
                'user_id' => '8616913087',
                'public' => $type,
                'body' => $comment
            );
            $createComment = $this->getContainer()->get('curl_service')->execute(
                'PUT',
                $this->getContainer()->getParameter('occurrence_api_url') . '/occurrences/' . $ticketId . '/comment',
                json_encode($comment)
            );

            if ((isset($createComment['code']) && $createComment['code'] == 200)) {
                $output->writeln(['Ocorrência atualizada com sucesso no Zendesk.']);
            } else {
                $output->writeln(['Ocorreu um erro ao atualizar a ocorrência no Zendesk.']);
                throw new \Exception('Não foi possível atualizar o ticket no Zendesk, enviando para retry...');
            }
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
