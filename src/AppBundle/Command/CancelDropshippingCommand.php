<?php

namespace AppBundle\Command;

use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CancelDropshippingCommand
 * @package AppBundle\Command
 */
class CancelDropshippingCommand extends ContainerAwareCommand
{

    /**
     * Method to configure the command
     */
    protected function configure()
    {
        $this
            ->setName('app:cancel-dropshipping')
            ->setDescription('Cancel Dropshipping.')
            ->setHelp("This command allows you to cancel dropshipping orders on SAP...")
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '================================================',
            ' Dropshipping Cancellation - ' . date('Y-m-d H:i:s'),
            '================================================',
            '',
        ]);

        /*
         ***********************************************************************************************************
         * Message pattern                                                                                          *
         * {"order_id": 1234567, "invoice_id": 123456, "product": 12345, "ticket": 1234567, "task": "CANCELA-123"]  *
         ************************************************************************************************************
         */
        $this->getContainer()->get('queue_service')->getMsgs('dropshipping.cancellation', 'dropshipping', 1, array($this, 'cancel'));
    }

    /**
     * @param AMQPMessage $message
     * @throws \Exception
     */
    public function cancel(AMQPMessage $message)
    {
        $totalRetry = 0;
        $props = $message->get_properties();
        $output = new ConsoleOutput();

        // Get total retry to headers
        if (isset($props['application_headers'])) {
            $headers = $message->get('application_headers')->getNativeData();
            if (isset($headers['x-death'])) {
                array_walk_recursive(
                    $headers['x-death'],
                    function ($vHeader, $kHeader, $uHeader) {
                        if ($kHeader === $uHeader[0]) {
                            $uHeader[1] = $vHeader;
                        }
                    },
                    ['count', &$totalRetry]
                );
            }
        }

        $output->writeln(['[' . date('Y-m-d H:i:s') . '] Mensagem: ' . print_r($message->body)]);
        $data = json_decode($message->body, true);
        $output->writeln(['[' . date('Y-m-d H:i:s') . '] Processing: ' . $data['order_id'] . ' - Retry: ' . $totalRetry]);

        try {
            /*
             *******************************************************************
             * Check if the message data is enough to process the cancellation *
             *******************************************************************
             */
            $order = $this->getContainer()->get('order_service')->getOrderInfo($data['order_id']);
            if (!$this->checkValidMessage($data, $order, $output)) {
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            /*
             ******************************
             * Get cancellation task info *
             ******************************
             */
            $output->writeln(['Getting cancellation task info...']);
            $task = $this->getContainer()->get('occurrence_service')->getTask($data['task']);
            $transitions = array();
            if (isset($task['data']['transitions']) && !empty($task['data']['transitions'])) {
                foreach ($task['data']['transitions'] as $transition) {
                    $transitions[$transition['name']] = $transition['id'];
                }
            }

            /*
             ****************************************
             * Get order product info from database *
             ****************************************
             */
            $output->writeln(['Getting order product info...']);
            if (!$productInfo = $this->getContainer()->get('product_service')->getProductOrderInfo($data['order_id'], $data['product'])) {
                $output->writeln(['Não foram encontradas informações do produto do pedido no banco de dados.']);
                throw new \Exception('Não foram encontradas informações do produto do pedido no banco de dados.');
            }

            $lastInvoice = [];
            $outgoingInvoiceId = 0;
            if (!empty($productInfo['invoices'])) {
                $lastInvoice = $productInfo['invoices'][0];

                if (count($productInfo['invoices']) > 1) {
                    foreach ($productInfo['invoices'] as $invoice) {
                        if ($invoice['dataCriacao'] > $lastInvoice['dataCriacao']) {
                            $lastInvoice = $invoice;
                        }
                    }
                }

                $outgoingInvoiceId = $lastInvoice['idNotafiscal'];
            }

            /*
             ****************************************
             * Search for order product info on SAP *
             ****************************************
             */
            $output->writeln(['Getting order product info on SAP...']);
            $sapProduct = $this->getSapProduct($data['order_id'], $productInfo['prd_referencia'], $output);
            $sapProductItems = $industryReferences = [];
            $sapProductItems[] = $sapProduct['itemcode'];
            $industryReferences[] = $sapProduct['suppcatnum'];
            if ($sapProduct['treetype'] == 'S') {
                $items = $this->getSapProductItems($data['order_id'], $productInfo['prd_referencia'], $output);
                foreach ($items as $item) {
                    $sapProductItems[] = $item['itemcode'];
                    $industryReferences[] = $item['suppcatnum'];
                }
            }
            $purchaseOrder = !empty($productInfo['ordem_compra']) ? $productInfo['ordem_compra'] : (!empty($sapProduct['potrgnum']) ? $sapProduct['potrgnum'] : (!empty($sapProduct['uNumoc']) ? $sapProduct['uNumoc'] : ''));

            /*
             ***********************************************************
             * Search on SAP if the order product has outgoing invoice *
             ***********************************************************
             */
            $output->writeln(['Checking if the order product has outgoing invoice on SAP...']);
            $sapProductInvoice = $this->checkProductInvoice($data['order_id'], $productInfo['prd_referencia'], $data['ticket'], $output);
            if (!empty($sapProductInvoice)) {
                $outgoingInvoiceId = $sapProductInvoice;
            }

            /*
             *************************************************************
             * Search if there is an follow up open to the order product *
             *************************************************************
             */
            $output->writeln(['Searching for opened follow up...']);
            $follows = $this->getFollow($data['order_id'], $productInfo['prd_referencia'], $output);
            if (empty($follows)) {
                $output->writeln(['No opened follows to the order product, cancelling SAP order and transitioning to Done...']);

                if (empty($lastInvoice)) {
                    $output->writeln(['Cancelando produto no SAP...']);
                    $this->cancelProduct($data['order_id'], $purchaseOrder, $productInfo['prd_referencia'], $sapProductItems, $data['ticket'], $output);
                }

                if (array_key_exists('oc_sem_pagamento', $transitions)) {
                    $output->writeln(['Transitioning task to Done...']);
                    $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($data['task'], $transitions['oc_sem_pagamento']);

                    if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                        throw new \Exception('Error transitioning task to Done: ' . serialize($transitionAction));
                    }
                } else {
                    $output->writeln(['It was not possible to transition the cancellation task to Done, transition is not allowed to the current status.']);

                    $comment = $this->getContainer()->get('occurrence_service')->commentTask($data['task'], 'Ocorreu um erro ao mover a task para Done - Jira não se encontra no status que permite a transição.');
                    if (!isset($comment['code']) || (isset($comment['code']) && $comment['code'] != 200)) {
                        throw new \Exception('Error commenting on task: ' . serialize($comment));
                    }
                }
            }

            /*
             *******************************************************
             * Treatment to final occurrence -> Transition to Done *
             *******************************************************
             */
            if ($task['data']['fields']['issuetype']['name'] == 'cancelamento_ocr_finalizadora') {
                $output->writeln(['Cancelamento com ocorrência finalizadora do pedido/produto, apenas transicionamos para Done...']);

                if (empty($lastInvoice)) {
                    $output->writeln(['Cancelando produto no SAP...']);
                    $this->cancelProduct($data['order_id'], $purchaseOrder, $productInfo['prd_referencia'], $sapProductItems, $data['ticket'], $output);
                }

                if (array_key_exists('ocorrencia_finalizadora', $transitions)) {
                    $output->writeln(['Transicionando task de cancelamento para ocorrencia_finalizadora...']);
                    $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($data['task'], $transitions['ocorrencia_finalizadora']);

                    if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                        throw new \Exception('Error transitioning task to ocorrencia_finalizadora: ' . serialize($transitionAction));
                    }
                } else {
                    $output->writeln(['Não foi possível transicionar a task de cancelamento para Done, transição não permitida para o status atual.']);

                    $comment = $this->getContainer()->get('occurrence_service')->commentTask($data['task'], 'Ocorreu um erro ao mover a task para Done - Jira não se encontra no status que permite a transição.');
                    if (!isset($comment['code']) || (isset($comment['code']) && $comment['code'] != 200)) {
                        throw new \Exception('Error commenting on task: ' . serialize($comment));
                    }
                }

                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            /*
             *********************************************
             * Search if the order was integrated on SAP *
             *********************************************
             */
            $branchId = $this->getSapOrderBranch($data['order_id'], $data['ticket'], $output, $totalRetry);
            if ($branchId === false) {
                throw new \Exception('Pedido não integrado no SAP.');
            }

            /*
             ***************************************************************
             * Filling order product data to update Jira Cancellation task *
             ***************************************************************
             */
            $updateJiraData = [];
            if (!empty($outgoingInvoiceId)) {
                $updateJiraData['outgoing_invoice'] = $outgoingInvoiceId;
            }
            if (!empty($purchaseOrder)) {
                $updateJiraData['purchase_order'] = $purchaseOrder;

                $purchaseOrderProduct = $this->getContainer()->get('curl_service')->execute(
                    'GET',
                    $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $data['order_id'] . '/products/' . base64_encode(trim($sapProductItems[0])) . '/purchase'
                );

                if (!isset($purchaseOrderProduct['code']) || (isset($purchaseOrderProduct['code']) && $purchaseOrderProduct['code'] != 200)) {
                    if (isset($purchaseOrderProduct['message'])) {
                        throw new \Exception('Error getting purchase order from item on SAP API: ' . $purchaseOrderProduct['message']);
                    }

                    throw new \Exception('Error getting purchase order from item on SAP API: ' . serialize($purchaseOrderProduct));
                }

                if (!empty($purchaseOrderProduct['data'][0]['docstatus']) && $purchaseOrderProduct['data'][0]['docstatus'] == 'C') {
                    $updateJiraData['type'] = 'oc_fechada';
                }
                if (!empty($purchaseOrderProduct['data'][0]['cardname'])) {
                    $updateJiraData['industry'] = $purchaseOrderProduct['data'][0]['cardname'];
                }
                if (!empty($purchaseOrderProduct['data'][0]['cardcode'])) {
                    $updateJiraData['pn_code'] = $purchaseOrderProduct['data'][0]['cardcode'];
                }
                if (!empty($purchaseOrderProduct['data'][0]['bplname'])) {
                    $updateJiraData['branch_name'] = $purchaseOrderProduct['data'][0]['bplname'];
                }
            }

            $updateJiraData['product_reference'] = implode(',', $sapProductItems);
            $updateJiraData['industry_reference'] = implode(',', $industryReferences);

            if (!empty($updateJiraData)) {
                $output->writeln(['Atualizando task de cancelamento com informações de produto/nota fiscal...']);
                $this->getContainer()->get('occurrence_service')->updateTask($data['task'], $updateJiraData);
                if (!isset($updateTask['code']) || (isset($updateTask['code']) && $updateTask['code'] != 200)) {
                    throw new \Exception('Error updating invoice ID on Jira task : ' . serialize($updateTask));
                }
            }

            if (empty($outgoingInvoiceId)) {
                /*
                 ***********************************************
                 * Check if the product is already cancelled:  *
                 * YES: Go to the next product                 *
                 * NO: Cancel It on SAP                        *
                 ***********************************************
                 */
                if ($this->checkProductStatus($sapProduct, $data['ticket']) === 0) {
                    $output->writeln(['Cancelando pedido ' . $data['order_id'] . ' e produto ' . $productInfo['prd_referencia'] . ' no SAP.']);
                    $this->cancelProduct($data['order_id'], $purchaseOrder, $productInfo['prd_referencia'], $sapProductItems, $data['ticket'], $output);
                }
                $output->writeln(['Produto ' . $productInfo['prd_referencia'] . ' cancelado no SAP.']);

                if (empty($productInfo['data_fat_forn_r'])) {
                    $output->writeln(['Transicionando Jira para cancelar_industria...']);
                    if (array_key_exists('cancelar_industria', $transitions)) {
                        $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($data['task'], $transitions['cancelar_industria']);

                        if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                            throw new \Exception('Error transitioning task to cancelar_industria: ' . serialize($transitionAction));
                        }
                    }
                } else {
                    $output->writeln(['Transicionando Jira para reaproveitar_oc...']);
                    if (array_key_exists('reaproveitar_oc', $transitions)) {
                        $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($data['task'], $transitions['reaproveitar_oc']);

                        if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                            throw new \Exception('Error transitioning task to reaproveitar_oc: ' . serialize($transitionAction));
                        }
                    }
                }
            } else {
                $output->writeln(['Buscando follow para atualizar campo de transportadora na task de cancelamento...']);
                $follow = $this->getContainer()->get('follow_service')->getFollowFromTypeOrderInvoice('dropshipping_shipment', $data['order_id'], $outgoingInvoiceId);

                if (empty($task['data']['fields']['customfield_10302']) && !empty($follow['shipper'])) {
                    $updateTask = $this->getContainer()->get('occurrence_service')->updateTask($data['task'], array('shipper' => $follow['shipper']));

                    if (!isset($updateTask['code']) || (isset($updateTask['code']) && $updateTask['code'] != 200)) {
                        throw new \Exception('Error updating shipper on Jira task : ' . serialize($updateTask));
                    }
                } elseif (empty($task['data']['fields']['customfield_10302']) && empty($follow['shipper'])) {
                    $this->getContainer()->get('occurrence_service')->commentTask($data['task'], 'Erro ao popular transportadora, follow não encontrado.');
                    throw new \Exception('Error updating shipper on Jira task, follow not found.');
                }

                if (empty($productInfo['data_emb_r'])) {
                    $output->writeln(['Transicionando Jira para barrar_embarque...']);
                    if (array_key_exists('barrar_embarque', $transitions)) {
                        $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($data['task'], $transitions['barrar_embarque']);

                        if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                            throw new \Exception('Error transitioning task to barrar_embarque: ' . serialize($transitionAction));
                        }
                    }
                } elseif (empty($productInfo['data_ent_r'])) {
                    $output->writeln(['Transicionando Jira para retorno_transito...']);
                    if (array_key_exists('retorno_transito', $transitions)) {
                        $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($data['task'], $transitions['retorno_transito']);

                        if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                            throw new \Exception('Error transitioning task to retorno_transito: ' . serialize($transitionAction));
                        }
                    }
                } else {
                    $output->writeln(['Transicionando Jira para descarte_coleta...']);
                    if (array_key_exists('descarte_coleta', $transitions)) {
                        $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($data['task'], $transitions['descarte_coleta']);

                        if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                            throw new \Exception('Error transitioning task to descarte_coleta: ' . serialize($transitionAction));
                        }
                    }
                }
            }

            $output->writeln(['[' . date('Y-m-d H:i:s') . '] Fim do cancelamento do produto no SAP']);
            $output->writeln(['---------------------------------------------', '']);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            return;
        } catch (\Exception $exception) {
            $output->writeln(['Falha (' . $totalRetry . ') no cancelamento do pedido ' . $data['order_id'] . ' e produto ' . $data['product'] . ': ' . $exception->getMessage(), '']);

            $this->getContainer()->get('queue_service')->publishOrderMessage(
                $message,
                ($totalRetry < 5 ? 'sap.cancellation.retry' : 'sap.cancellation.fail'),
                'dropshipping'
            );

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            throw $exception;
        }
    }

    /**
     * @param $orderId
     * @param $ticketId
     * @param OutputInterface $output
     * @param integer $retryCount
     * @return bool
     * @throws \Exception
     */
    private function getSapOrderBranch($orderId, $ticketId, $output, $retryCount)
    {
        try {
            $branchOrder = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId
            );

            if (!isset($branchOrder['code']) || (isset($branchOrder['code']) && $branchOrder['code'] != 200)) {
                if (isset($branchOrder['message'])) {
                    $output->writeln(['Erro ao buscar filial do pedido na API-SAP: ' . $branchOrder['message']]);
                    throw new \Exception('Erro ao buscar filial do pedido na API-SAP: ' . $branchOrder['message']);
                }

                $output->writeln(['Erro ao buscar filial do pedido na API-SAP.']);
                throw new \Exception('Erro ao buscar filial do pedido na API-SAP.');
            }

            if (empty($branchOrder['data'])) {
                $output->writeln(['Pedido não encontrado no SAP...']);

                if ($retryCount < 2) {
                    $this->getContainer()->get('occurrence_service')->commentTicket(
                        $ticketId,
                        'Cancelamento Automático - Pedido ' . $orderId . ' não integrado no SAP.',
                        false
                    );
                }
                return false;
            }

            return intval($branchOrder['data']['bplid']);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $data
     * @param $order
     * @param OutputInterface $output
     * @return bool
     */
    private function checkValidMessage($data, $order, $output)
    {
        if (!isset($data['order_id'])) {
            $output->writeln(['Numero de pedido inválido.']);
            $output->writeln(['---------------------------------------------', '', '']);
            return false;
        }

        if (empty($order)) {
            $output->writeln(['Pedido não encontrado, descartando mensagem...']);
            $output->writeln(['---------------------------------------------', '', '']);
            return false;
        }

        if (empty($order['realizada_aprovacao'])) {
            $output->writeln(['Pedido não foi aprovado (não integrado no SAP), descartando mensagem...', '', '']);
            return false;
        }

        return true;
    }

    /**
     * @param $product
     * @param $ticketId
     * @return int
     * @throws \Exception
     */
    private function checkProductStatus($product, $ticketId)
    {
        try {
            if (isset($product['linestatus'])) {
                if ($product['linestatus'] == 'C') {
                    $this->getContainer()->get('occurrence_service')->commentTicket(
                        $ticketId,
                        'Cancelamento Automático - Produto ' . $product['itemcode'] . ' já se encontra fechado.',
                        false
                    );

                    return 1;
                }
            }

            return 0;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $orderId
     * @param $productReference
     * @param OutputInterface $output
     * @return array
     * @throws \Exception
     */
    private function getSapProduct($orderId, $productReference, $output)
    {
        $orderProduct = $this->getContainer()->get('curl_service')->execute(
            'GET',
            $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference)
        );

        if (!isset($orderProduct['code']) || (isset($orderProduct['code']) && $orderProduct['code'] != 200)) {
            if (isset($orderProduct['message'])) {
                $output->writeln(['Erro ao buscar produto do pedido na API-SAP: ' . $orderProduct['message']]);
                throw new \Exception('Erro ao buscar produto do pedido na API-SAP: ' . $orderProduct['message']);
            }

            $output->writeln(['Erro ao buscar produto do pedido na API-SAP.']);
            throw new \Exception('Erro ao buscar produto do pedido na API-SAP.');
        }

        return isset($orderProduct['data'][0]) ? $orderProduct['data'][0] : array();
    }

    /**
     * Gets kit sap items from order ID and product Reference
     *
     * @param $orderId
     * @param $productReference
     * @param OutputInterface $output
     * @return array
     * @throws \Exception
     */
    private function getSapProductItems($orderId, $productReference, $output)
    {
        $orderProduct = $this->getContainer()->get('curl_service')->execute(
            'GET',
            $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference) . '/items'
        );

        if (!isset($orderProduct['code']) || (isset($orderProduct['code']) && $orderProduct['code'] != 200)) {
            if (isset($orderProduct['message'])) {
                $output->writeln(['Erro ao buscar items do kit sap na API-SAP: ' . $orderProduct['message']]);
                throw new \Exception('Erro ao buscar items do kit sap na API-SAP: ' . $orderProduct['message']);
            }

            $output->writeln(['Erro ao buscar items do kit sap na API-SAP.']);
            throw new \Exception('Erro ao buscar items do kit sap na API-SAP.');
        }

        return isset($orderProduct['data']) ? $orderProduct['data'] : array();
    }

    /**
     * @param int $orderId
     * @param int $purchaseOrder
     * @param string $productReference
     * @param array $productItems
     * @param int $ticketId
     * @param OutputInterface $output
     * @return bool
     * @throws \Exception
     */
    private function cancelProduct($orderId, $purchaseOrder, $productReference, $productItems, $ticketId, $output)
    {
        try {
            $cancelItem = $this->getContainer()->get('curl_service')->execute(
                'PUT',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference) . '/cancel'
            );

            if (!isset($cancelItem['code']) || (isset($cancelItem['code']) && $cancelItem['code'] != 200)) {
                if (isset($cancelItem['message'])) {
                    $output->writeln(['Erro ao cancelar o item: ' . $cancelItem['message']]);
                    throw new \Exception('Erro ao cancelar o item: ' . $cancelItem['message']);
                }

                $output->writeln(['Erro ao cancelar o item no SAP.']);
                throw new \Exception('Erro ao cancelar o item no SAP.');
            }

            $this->getContainer()->get('occurrence_service')->commentTicket(
                $ticketId,
                'Cancelamento Automático - Produto ' . $productReference . ' foi adicionado ao processo de cancelamento no SAP.',
                false
            );

            if (!empty($purchaseOrder)) {
                $output->writeln(['Ordem de compra existente, iniciando o cancelamento dela...']);
                $cancelPurchaseOrderItem = $this->getContainer()->get('curl_service')->execute(
                    'PUT',
                    $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference) . '/purchase/' . $purchaseOrder . '/cancel'
                );

                if (!isset($cancelPurchaseOrderItem['code']) || (isset($cancelPurchaseOrderItem['code']) && $cancelPurchaseOrderItem['code'] != 200)) {
                    if (isset($cancelPurchaseOrderItem['message'])) {
                        $output->writeln(['Erro ao cancelar o item da OC no SAP: ' . $cancelPurchaseOrderItem['message']]);
                        throw new \Exception('Erro ao cancelar o item da OC no SAP: ' . $cancelPurchaseOrderItem['message']);
                    }

                    $output->writeln(['Erro ao cancelar o item no SAP.']);
                    throw new \Exception('Erro ao cancelar o item da OC no SAP.');
                }

                if (!empty($productItems)) {
                    $output->writeln(['Cancelando itens de kit SAP na OC...']);
                    foreach ($productItems as $productItem) {
                        $cancelPurchaseOrderItem = $this->getContainer()->get('curl_service')->execute(
                            'PUT',
                            $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productItem) . '/purchase/' . $purchaseOrder . '/cancel'
                        );

                        if (!isset($cancelPurchaseOrderItem['code']) || (isset($cancelPurchaseOrderItem['code']) && $cancelPurchaseOrderItem['code'] != 200)) {
                            if (isset($cancelPurchaseOrderItem['message'])) {
                                $output->writeln(['Erro ao cancelar item do kit SAP: ' . $cancelPurchaseOrderItem['message']]);
                                throw new \Exception('Erro ao cancelar item do kit SAP: ' . $cancelPurchaseOrderItem['message']);
                            }

                            $output->writeln(['Erro ao cancelar o item do kit SAP.']);
                            throw new \Exception('Erro ao cancelar o item do kit SAP.');
                        }
                    }
                    $output->writeln(['Itens do kit SAP da OC cancelados com sucesso.']);
                }

            }

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $orderId
     * @param $productReference
     * @param $ticketId
     * @param OutputInterface $output
     * @return bool
     * @throws \Exception
     */
    private function checkProductInvoice($orderId, $productReference, $ticketId, $output)
    {
        try {
            $orderProductInvoice = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('sap_api_url') . '/orders/' . $orderId . '/products/' . base64_encode($productReference) . '/invoice'
            );

            $output->writeln(['Encode da referência do produto: ' . base64_encode($productReference)]);

            if (!isset($orderProductInvoice['code']) || (isset($orderProductInvoice['code']) && $orderProductInvoice['code'] != 200)) {
                if (isset($orderProductInvoice['message'])) {
                    $output->writeln(['Erro ao buscar nota fiscal para o item na API-SAP: ' . $orderProductInvoice['message']]);
                    throw new \Exception('Erro ao buscar nota fiscal para o item na API-SAP: ' . $orderProductInvoice['message']);
                }

                $output->writeln(['Erro ao buscar nota fiscal para o item no SAP.']);
                throw new \Exception('Erro ao buscar nota fiscal para o item no SAP: ' . serialize($orderProductInvoice));
            }

            if (!empty($orderProductInvoice['data'])) {
                $output->writeln(
                    [
                        'Produto possui nota fiscal de saída, nenhum cancelamento será realizado para ele.',
                        'Setando mensagem de não liberação do cancelamento no Zendesk por possuir NF de saída no SAP - Ticket ' . $ticketId
                    ]
                );

                $this->getContainer()->get('occurrence_service')->commentTicket(
                    $ticketId,
                    'Cancelamento Automático - Produto ' . $productReference . ' possui nota fiscal de saída, por isso não será cancelado no SAP automaticamente.',
                    false
                );

                return $orderProductInvoice['data'][0]['serial'];
            }

            return false;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param int $orderId
     * @param string $productReference
     * @param OutputInterface $output
     * @return bool
     * @throws \Exception
     */
    private function getFollow($orderId, $productReference, $output)
    {
        try {
            $follows = $this->getContainer()->get('curl_service')->execute(
                'GET',
                $this->getContainer()->getParameter('follow_api_url') . '/follows?query=(order_id = ' . $orderId . ' and product_reference = \'' . $productReference . '\')'
            );

            if (!isset($follows['code']) || (isset($follows['code']) && $follows['code'] != 200)) {
                if (isset($follows['message'])) {
                    $output->writeln(['Error searching for follows on Follow API: ' . $follows['message']]);
                    throw new \Exception('Error searching for follows on Follow API: ' . $follows['message']);
                }

                $output->writeln(['Error searching for follows on Follow API.']);
                throw new \Exception('Error searching for follows on Follow API: ' . serialize($follows));
            }

            if (!empty($follows['data']['follows'])) {
                return true;
            }

            return false;


        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
