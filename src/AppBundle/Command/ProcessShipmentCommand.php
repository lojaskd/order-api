<?php

namespace AppBundle\Command;

use AppBundle\Entity\B2cNotafiscal;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ProcessShipmentCommand
 * @package AppBundle\Command
 */
class ProcessShipmentCommand extends ContainerAwareCommand
{

    /**
     * @var array
     */
    private $newTask;

    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:process-shipment')

            // the short description shown while running "php bin/console list"
            ->setDescription('Process Cancelled Orders Shipment.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to process cancelled orders shipment...")
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '=======================',
            'Process Orders Shipment',
            '=======================',
            '',
        ]);

        $this->getContainer()->get('queue_service')->getMsgs('order.shipment', '', 1, array($this, 'processShipment'));

        $output->writeln(['Fim do processamento! :)', '']);
    }

    /**
     * @param AMQPMessage $message
     * @throws \Exception
     */
    public function processShipment($message)
    {
        // Total retry
        $totalRetry = 0;
        $props = $message->get_properties();
        $output = new ConsoleOutput();

        // Get total retry to headers
        if (isset($props['application_headers'])) {
            $headers = $message->get('application_headers')->getNativeData();
            if (isset($headers['x-death'])) {
                array_walk_recursive(
                    $headers['x-death'],
                    function ($vHeader, $kHeader, $uHeader) {
                        if ($kHeader === $uHeader[0]) {
                            $uHeader[1] = $vHeader;
                        }
                    },
                    ['count', &$totalRetry]
                );
            }
        }

        $output->writeln(['Mensagem: ' . $message->body]);
        $data = json_decode($message->body, true);
        $output->writeln(['Processing invoice ' . $data['invoice_id'] . ' - Retry: ' . $totalRetry]);

        try {
            if (isset($data['invoice_id']) && isset($data['order_id'])) {
                if (!isset($data['task'])) {
                    throw new \Exception('Task code was not sent.');
                }

                $originalTask = $this->getContainer()->get('occurrence_service')->getTask($data['task']);
                if (!isset($originalTask['data']['fields'])) {
                    throw new \Exception('Error getting original task : ' . serialize($originalTask));
                }

                /*
                 ****************************************************************************************************************
                 * On SAP cancellation process we pass the products because they came from different NF in the same parent task *
                 * **************************************************************************************************************
                 */
                $products = array();
                if (isset($data['products']) && !empty($data['products'])) {
                    $products = $data['products'];
                }

                /*
                 *************************************************************
                 * If the products were not passed, we get them on Jira task *
                 * ***********************************************************
                 */
                if (empty($products)) {
                    $products = preg_replace("/,+/", ",", preg_replace('/[^0-9\,]/', ',', $originalTask['data']['fields']['customfield_11409']));
                    if (empty($products)) {
                        $output->writeln('Product regex match error on product: ' . $originalTask['data']['fields']['customfield_11409']);
                        throw new \Exception('Product regex match error on product: ' . $originalTask['data']['fields']['customfield_11409']);
                    }
                    $products = explode(',', $products);
                }

                asort($products);

                $transitions = array();
                if (isset($originalTask['data']['transitions']) && !empty($originalTask['data']['transitions'])) {
                    foreach ($originalTask['data']['transitions'] as $transition) {
                        $transitions[$transition['name']] = $transition['id'];
                    }
                }

                $taskKey = $originalTask['data']['key'];

                if ($data['parent_task']) {
                    $filterTask = $this->getContainer()->get('occurrence_service')->searchTask('project=CANCELA&invoice=' . $data['invoice_id'] . '&order=' . $data['order_id']);

                    if (!isset($filterTask['data']['issues'])) {
                        throw new \Exception('Error searching if the child task was already created: ' . serialize($filterTask));
                    }

                    $duplicateTask = true;
                    if (!empty($filterTask['data']['issues'])) {
                        $taskProducts = explode(',', preg_replace("/,+/", ",", preg_replace('/[^0-9\,]/', ',', $filterTask['data']['issues'][0]['fields']['customfield_11409'])));
                        asort($taskProducts);
                        if ($taskProducts == $products) {
                            $taskKey = $filterTask['data']['issues'][0]['key'];
                            $duplicateTask = false;
                        }
                    }

                    if ($duplicateTask) {
                        $newTask = $this->duplicateTask($originalTask["data"], $products, $data['invoice_id']);
                        $taskKey = $newTask['data']['key'];
                    }

                    $linkedTasks = $this->getContainer()->get('occurrence_service')->linkTasks(array('name' => 'Blocks', 'task_in' => $taskKey, 'task_out' => $originalTask['data']['key']));
                    if (!isset($linkedTasks['code']) || (isset($linkedTasks['code']) && $linkedTasks['code'] != 200)) {
                        throw new \Exception('Error linking child task with its parent.');
                    }

                    if (isset($originalTask['data']['transitions']) && !empty($originalTask['data']['transitions'])) {
                        if (array_key_exists('aguardar_tasks', $transitions)) {
                            $transitionAction =  $this->getContainer()->get('occurrence_service')->transitionTask($originalTask['data']['key'], $transitions['aguardar_tasks']);
                            if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                                throw new \Exception('Error transitioning parent task : ' . serialize($transitionAction));
                            }
                        }
                    }
                } else {
                    if (empty($originalTask['data']['fields']['customfield_11101'])) {
                        $updateTask = $this->getContainer()->get('occurrence_service')->updateTask($taskKey, array('outgoing_invoice' => $data['invoice_id']));
                        if (!isset($updateTask['code']) || (isset($updateTask['code']) && $updateTask['code'] != 200)) {
                            throw new \Exception('Error updating invoice ID on Jira task : ' . serialize($updateTask));
                        }
                    }
                }

                if ($originalTask['data']['fields']['issuetype']['name'] == 'cancelamento_ocr_finalizadora') {
                    $output->writeln(['Cancelamento ativo do pedido/produto, apenas transicionamos para pronto_estornar...']);
                    if (array_key_exists('cancelado_sap', $transitions)) {
                        $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($taskKey, $transitions['cancelado_sap']);

                        if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                            throw new \Exception('Error transitioning task to cancelado_sap: ' . serialize($transitionAction));
                        }
                    } else {
                        $output->writeln('Its not possible to transition Jira task to pronto_estornar, transition not allowed to the current status.');

                        $comment = $this->getContainer()->get('occurrence_service')->commentTask($taskKey, 'Ocorreu um erro ao mover a task para pronto_estornar - Jira não se encontra no status que permite a transição.');
                        if (!isset($comment['code']) || (isset($comment['code']) && $comment['code'] != 200)) {
                            throw new \Exception('Error commenting on task: ' . serialize($comment));
                        }
                    }

                    $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                    return;
                }

                /* @var $invoiceStatus B2cNotafiscal */
                $invoiceStatus = $this->getContainer()->get('invoice_service')->getInvoice($data['invoice_id'], $data['order_id']);
                $productInfo = $this->getContainer()->get('product_service')->getProductOrderInfo($data['order_id'], trim($products[0]));

                if (isset($invoiceStatus)) {
                    $output->writeln(['Nota fiscal encontrada, processando...']);
                    $follow = $this->getContainer()->get('follow_service')->getFollowFromTypeOrderInvoice('hub', $data['order_id'], $data['invoice_id']);

                    if (empty($originalTask['data']['fields']['customfield_10603']) && !empty($follow['hub'])) {
                        $updateTask = $this->getContainer()->get('occurrence_service')->updateTask($taskKey, array('hub' => $follow['hub']));
                        if (!isset($updateTask['code']) || (isset($updateTask['code']) && $updateTask['code'] != 200)) {
                            throw new \Exception('Error updating hub on Jira task : ' . serialize($updateTask));
                        }
                    }

                    if (empty($originalTask['data']['fields']['customfield_10302']) && !empty($follow['shipper'])) {
                        $updateTask = $this->getContainer()->get('occurrence_service')->updateTask($taskKey, array('shipper' => $follow['shipper']));
                        if (!isset($updateTask['code']) || (isset($updateTask['code']) && $updateTask['code'] != 200)) {
                            throw new \Exception('Error updating shipper on Jira task : ' . serialize($updateTask));
                        }
                    }

                    /*
                     ********************************
                     * Treatment to delivered order *
                     * ******************************
                     */
                    if ($invoiceStatus->getDataEntregaRealizada() || !empty($productInfo['data_ent_r'])) {
                        $output->writeln([
                            'Produto possui entrega realizada, mandando para a fila de entrega e descartando mensagem...',
                            '',
                            ''
                        ]);
                        $this->getContainer()->get('queue_service')->publishDeliveredOrderMsg(array(
                            'order_id' => $data['order_id'],
                            'invoice_id' => $data['invoice_id'],
                            'ticket' => $data['ticket'],
                            'task' => $taskKey
                        ));
                        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                        return;
                    }

                    if (!isset($data['ticket']) || (isset($data['ticket']) && empty($data['ticket']))) {
                        throw new \Exception('No Zendesk ticket was informed.');
                    }

                    /*
                     **********************************
                     * Treatment to not shipped order *
                     * ********************************
                     */
                    if (!$invoiceStatus->getDataEmbarque() && empty($productInfo['data_emb_r'])) {
                        $output->writeln(['Produto não possui data de embarque, transicionando o Jira para barrar_embarque e ticket do Zendesk para barrar...']);
                        if (array_key_exists('barrar_embarque', $transitions)) {
                            $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($taskKey, $transitions['barrar_embarque']);

                            if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                                throw new \Exception('Error transitioning task to barrar_embarque: ' . serialize($transitionAction));
                            }
                        }

                        $ticketData = array(
                            'ticket_id' => $data['ticket'],
                            'status' => 'em espera',
                            'group' => 30828707,
                            'treatment' => 'barrar'
                        );

                        $updatedTicket = $this->getContainer()->get('occurrence_service')->updateTicket($data['ticket'], $ticketData);
                        if (!isset($updatedTicket['code']) || (isset($updatedTicket['code']) && $updatedTicket['code'] != 200)) {
                            throw new \Exception('Error updating Zendesk ticket to barrar.');
                        }
                    }

                    /*
                     ******************************
                     * Treatment to shipped order *
                     * ****************************
                     */
                    if ($invoiceStatus->getDataEmbarque() || !empty($productInfo['data_emb_r'])) {
                        $output->writeln(['Produto possui data de embarque, transicionando o Jira...']);
                        $ticket = $this->getContainer()->get('occurrence_service')->getTicket($data['ticket']);

                        if (!isset($ticket['code']) || (isset($ticket['code']) && $ticket['code'] != 200)) {
                            throw new \Exception('Error getting Zendesk ticket.');
                        }

                        $output->writeln(['Transicionando Jira para retornar_transito...']);
                        if (array_key_exists('retornar_transito', $transitions)) {
                            $transitionAction = $this->getContainer()->get('occurrence_service')->transitionTask($taskKey, $transitions['retornar_transito']);

                            if (!isset($transitionAction['code']) || (isset($transitionAction['code']) && $transitionAction['code'] != 200)) {
                                throw new \Exception('Error transitioning task to retorno_transito: ' . serialize($transitionAction));
                            }
                        }
                    }
                    $output->writeln(['Encerrando processamento da NF...']);
                } else {
                    $output->writeln(['NF não encontrada no banco de dados, encerrando processamento...']);
                }

                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }
        } catch (\Exception $e) {
            $output->writeln(['Falha (' . $totalRetry . ') no processamento de embarque da NF ' . $data['invoice_id'] . ': ' . $e->getMessage(), '']);

            $this->getContainer()->get('queue_service')->publishOrderMessage(
                $message,
                ($totalRetry < 5 ? 'order.shipment.retry' : 'order.shipment.fail'),
                ''
            );

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);

            throw $e;
        }
    }

    /**
     * @param $originalTask
     * @param $products
     * @param $invoice
     * @return mixed
     * @throws \Exception
     */
    private function duplicateTask($originalTask, $products, $invoice)
    {
        try {
            $this->newTask = array();

            $this->checkFilledField($originalTask['fields']['customfield_10203'], 'order_id');
            $this->checkFilledField(implode(',', $products), 'product_reference');
            $this->checkFilledField($invoice, 'outgoing_invoice');
            $this->checkFilledField($originalTask['fields']['project']['key'], 'project');
            $this->checkFilledField($originalTask['fields']['issuetype'], 'type', 'name');
            $this->checkFilledField($originalTask['fields']['reporter']['key'], 'reporter');
            $this->checkFilledField($originalTask['fields']['assignee'], 'assignee');
            $this->checkFilledField($originalTask['fields']['customfield_10214'], 'talk_to');
            $this->checkFilledField($originalTask['fields']['customfield_10213'], 'internal_demand', 'value');
            $this->checkFilledField($originalTask['fields']['summary'], 'summary');
            $this->checkFilledField($originalTask['fields']['customfield_11205'], 'ocr_code', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_10507'], 'industry_invoice');
            $this->checkFilledField($originalTask['fields']['customfield_10600'], 'industry_devolution_invoice');
            $this->checkFilledField($originalTask['fields']['customfield_11102'], 'outgoing_devolution_invoice');
            $this->checkFilledField($originalTask['fields']['customfield_11203'], 'industry', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_10601'], 'fob', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_10603'], 'hub', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_11218'], 'reverse_hub', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_10302'], 'shipper', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_11004'], 'indemnity_evidence');
            $this->checkFilledField((!empty($originalTask['fields']['description']) ? $originalTask['fields']['description'] : 'Cancelamento PV ' . $originalTask['fields']['customfield_10203']), 'description');
            $this->checkFilledField($originalTask['fields']['customfield_11005'], 'zendesk_group');
            $this->checkFilledField($originalTask['fields']['customfield_11100'], 'customer_contact', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_11103'], 'delivery_date');
            $this->checkFilledField($originalTask['fields']['customfield_11006'], 'ocr_status', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_10212'], 'zendesk_id');
            $this->checkFilledField($originalTask['fields']['customfield_10700'], 'polo', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_11207'], 'cancellation_indicator');
            $this->checkFilledField($originalTask['fields']['customfield_11208'], 'invoice_type', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_11210'], 'refund_form', 'value');
            $this->checkFilledField($originalTask['fields']['customfield_11305'], 'reversal');
            $this->checkFilledField($originalTask['fields']['customfield_10505'], 'purchase_order');

            $createdTask = $this->getContainer()->get('occurrence_service')->createTask($this->newTask);
            if (!isset($createdTask['code']) || (isset($createdTask['code']) && $createdTask['code'] != 200) || !isset($createdTask['data']['key'])) {
                throw new \Exception('Error creating child task: ' . serialize($createdTask));
            }

            return $createdTask;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $value
     * @param $name
     * @param string $option
     * @param null $dateFormat
     */
    private function checkFilledField($value, $name, $option = '', $dateFormat = null)
    {
        if ($option && isset($value[$option])) {
            $value = $value[$option];
        }

        if (!empty($value)) {
            if ($dateFormat) {
                /* @var $value \Datetime */
                $value = $value->format('Y-m-d');
            }

            $this->newTask[$name] = $value;
        }
    }

}
