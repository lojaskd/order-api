<?php

namespace AppBundle\Command;

use AppBundle\Entity\Validate\Update\UpdateValidateProductStatus;
use AppBundle\Entity\VO\Update\UpdateProductsStatusVO;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CancelOrderCommand
 * @package AppBundle\Command
 */
class CancelOrderCommand extends ContainerAwareCommand
{
    /**
     * Configure command method
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:cancel-order')

            // the short description shown while running "php bin/console list"
            ->setDescription('Cancels order on ADM.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to cancel order on ADM...")
        ;
    }

    /**
     * Execute command method
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '====================',
            ' Order Cancellation ',
            '====================',
            '',
        ]);

        $this->getContainer()->get('queue_service')->getMsgs('order.cancellation', '', 1, array($this, 'cancelOrder'));

        $output->writeln(['Fim do cancelamento do pedido :)', '']);
    }

    /**
     * Cancel order method
     *
     * @param AMQPMessage $message
     * @throws \Exception
     */
    public function cancelOrder($message)
    {
        // Total retry
        $totalRetry = 0;
        $props = $message->get_properties();
        $output = new ConsoleOutput();

        if (isset($props['application_headers'])) {
            $headers = $message->get('application_headers')->getNativeData();
            if (isset($headers['x-death'])) {
                array_walk_recursive(
                    $headers['x-death'],
                    function ($vHeader, $kHeader, $uHeader) {
                        if ($kHeader === $uHeader[0]) {
                            $uHeader[1] = $vHeader;
                        }
                    },
                    ['count', &$totalRetry]
                );
            }
        }

        $output->writeln(['Message: ' . $message->body]);

        $data = json_decode($message->body, true);
        $output->writeln(['Processing: ' . $data['order_id'] . ' - Retry: ' . $totalRetry]);

        try {
            $validEntry = $this->checkValidEntry($data);

            if ($validEntry !== true) {
                $output->writeln([$validEntry]);
                $output->writeln(['#############################################', '', '']);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                return;
            }

            $task = $this->getTask($data['task']);
            $ticket = $this->getTicket($data['ticket']);

            foreach ($data['products'] as $product) {
                $output->writeln(['Cancelling product ' . $product . '...']);
                $productInfo = $this->getContainer()->get('product_service')->getProductOrderInfo($data['order_id'], $product);

                if ($productInfo["item_cancelado"] === true || $productInfo["item_cancelado"] === 1) {
                    $this->getContainer()->get('occurrence_service')->commentTicket($ticket['id'], 'Produto ' . $product . ' já se encontra cancelado no ADM.', false);
                    $output->writeln(['Product is already cancelled, inserting reversal log...']);
                    $this->insertReversalLog($data['order_id'], $product, $ticket);
                }
                $this->cancelProduct($data['order_id'], $product, $ticket);

                if ($productInfo["item_cancelado"] === false || $productInfo["item_cancelado"] === 0) {
                    $this->getContainer()->get('occurrence_service')->commentTicket($ticket['id'], 'Produto ' . $product . ' cancelado com sucesso no ADM.', false);
                }
            }

            $output->writeln('Setting Jira task to cancelled on ADM...');
            $updateTask = $this->getContainer()->get('occurrence_service')->updateTask($task['key'], array('cancelled_adm' => 'sim'));
            if (!isset($updateTask['code']) || (isset($updateTask['code']) && $updateTask['code'] != 200)) {
                throw new \Exception('Error updating Jira task : ' . serialize($updateTask));
            }

            $this->getContainer()->get('occurrence_service')->commentTicket($ticket['id'], 'Cancelamento finalizado no ADM com sucesso.', false);

            $output->writeln(['Order cancellation process ended.']);
            $output->writeln(['#############################################', '', '']);

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        } catch (\Exception $e) {
            $output->writeln(['Fail (' . $totalRetry . ') on cancellation process of order ' . $data['order_id'] . ': ' . $e->getMessage(), '']);

            $this->getContainer()->get('queue_service')->publishMessage(
                $message,
                ($totalRetry < 5 ? 'order.cancellation.retry' : 'order.cancellation.fail'),
                ''
            );

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return bool|string
     */
    private function checkValidEntry($data)
    {
        if (!isset($data['order_id'])) {
            return 'Order ID is invalid.';
        }

        if (empty($data['products'])) {
            return 'No products were informed.';
        }

        if (empty($data['ticket'])) {
            return 'No ticket was informed.';
        }

        if (empty($data['task'])) {
            return 'No task was informed.';
        }

        return true;
    }

    /**
     * @param string $taskKey
     * @return mixed
     */
    private function getTask($taskKey)
    {
        $task = $this->getContainer()->get('occurrence_service')->getTask($taskKey);
        return $this->returnTreatment($task);
    }

    /**
     * @param integer $ticketId
     * @return mixed
     */
    private function getTicket($ticketId)
    {
        $ticket = $this->getContainer()->get('occurrence_service')->getTicket($ticketId);
        return $this->returnTreatment($ticket);
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    private function returnTreatment($data)
    {
        if (!isset($data['code']) || (isset($data['code']) && $data['code'] != 200)) {
            throw new \Exception('Return code wasn\'t returned or Its not 200: ' . serialize($data));
        }

        if (empty($data['data'])) {
            throw new \Exception('No data was returned: : ' . serialize($data));
        }

        return $data['data'];
    }

    /**
     * Inserts reversal log to products that were already cancelled
     *
     * @param int $orderId
     * @param int $productId
     * @param array $ticket
     * @throws \Exception
     */
    private function insertReversalLog($orderId, $productId, $ticket)
    {
        try {
            $reversalLogSearch = $this->getContainer()->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:B2cPedprdEstorno')
                ->findBy(array('idPedped' => $orderId, 'idPrdprd' => $productId, 'ticket' => $ticket['id']));

            if (empty($reversalLogSearch)) {
                switch ($ticket['custom_fields']['cf40584247']['value']) {
                    case 'vale_compras':
                        $refundForm = 3;
                        break;
                    case 'estorno_cartão':
                        $refundForm = 1;
                        break;
                    case 'reembolso':
                        $refundForm = 2;
                        break;
                    case 'markeplace':
                        $refundForm = 5;
                        break;
                    case 'assistência':
                        $refundForm = 4;
                        break;
                    case 'chargeback':
                        $refundForm = 7;
                        break;
                    default:
                        $refundForm = 0;
                        break;
                }

                $reversalLog = new UpdateProductsStatusVO();
                $reversalLog->setCancelled(true);
                $reversalLog->setOrderId($orderId);
                $reversalLog->setClerk('Cancelamento Automático');
                $reversalLog->setTicket($ticket['id']);
                $reversalLog->setOcrCode($ticket['custom_fields']['cf78300127']['value']);
                $reversalLog->setCancellationIndicator($ticket['custom_fields']['cf40428088']['value']);
                $reversalLog->setTicketType($ticket['type']);
                $reversalLog->setHub($ticket['custom_fields']['cf39541428']['value']);
                $reversalLog->setShipper($ticket['custom_fields']['cf39481467']['value']);
                $reversalLog->setInvoice($ticket['custom_fields']['cf37270327']['value']);
                $reversalLog->setProductCauser($productId);
                $reversalLog->setRefundForm($refundForm);

                /* Calculates the price to return on refund */
                $order = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository('AppBundle:B2cPedped')->findOneBy(array('idPedped' => $orderId));
                $product = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository('AppBundle:B2cPedprd')->findOneBy(array('idPedped' => $orderId, 'idPrdprd' => $productId));

                $totalDiscount = $order->getPedValorDesconto();
                if (strpos($order->getPedCupom(), 'SAP') === false) {
                    $totalDiscount += ($order->getPedValorValecompra());
                }

                $percentDiscount = round(($totalDiscount / floatval($order->getPedValorSubtotal())) * 100, 2);
                $productAmount = $product->getPrdPvtotal() - (round($product->getPrdPvtotal() * ($percentDiscount / 100), 2));
                $shippingAmount = round(($product->getPrdPvtotal() / $order->getPedValorSubtotal()) * $order->getPedValorFrete(), 2);

                /* Saves the reversal data on refund table */
                $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository('AppBundle:B2cPedprdEstorno')->insertOrderProductReversal(
                    $reversalLog,
                    $productId,
                    $productAmount,
                    $shippingAmount
                );

                $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository('AppBundle:B2cPedprd')->updateCancellationDate($orderId, $productId);
            }
        } catch (\Exception $exception) {
            throw new \Exception('Error on insertReversalLog:' . $exception->getMessage());
        }
    }

    /**
     * @param integer $orderId
     * @param integer $productId
     * @param array $ticket
     * @throws \Exception
     */
    private function cancelProduct($orderId, $productId, $ticket)
    {
        try {
            switch ($ticket['custom_fields']['cf40584247']['value']) {
                case 'vale_compras':
                    $refundForm = 3;
                    break;
                case 'estorno_cartão':
                    $refundForm = 1;
                    break;
                case 'reembolso':
                    $refundForm = 2;
                    break;
                case 'markeplace':
                    $refundForm = 5;
                    break;
                case 'assistência':
                    $refundForm = 4;
                    break;
                case 'chargeback':
                    $refundForm = 7;
                    break;
                default:
                    $refundForm = 0;
                    break;
            }

            $cancelProduct = new UpdateValidateProductStatus();
            $cancelProduct->setCancelled(true);
            $cancelProduct->setOrderId($orderId);
            $cancelProduct->setClerk('Cancelamento Automático');
            $cancelProduct->setTicket($ticket['id']);
            $cancelProduct->setOcrCode($ticket['custom_fields']['cf78300127']['value']);
            $cancelProduct->setCancellationIndicator($ticket['custom_fields']['cf40428088']['value']);
            $cancelProduct->setTicketType($ticket['type']);
            $cancelProduct->setHub($ticket['custom_fields']['cf39541428']['value']);
            $cancelProduct->setShipper($ticket['custom_fields']['cf39481467']['value']);
            $cancelProduct->setInvoice($ticket['custom_fields']['cf37270327']['value']);
            $cancelProduct->setProductCauser($productId);
            $cancelProduct->setRefundForm($refundForm);

            $orderVO = $this->getContainer()->get('order_service')->getCommonOrderInfo($orderId);
            $this->getContainer()->get('product_service')->updateProductStatus($orderVO, $productId, $cancelProduct);
        } catch (\Exception $exception) {
            throw new \Exception('Error on product cancellation:' . $exception->getMessage());
        }

    }
}
