<?php

namespace AppBundle\Monolog;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class WebProcessor
 * @package AppBundle\Monolog
 */
class WebProcessor
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * WebProcessor constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * Process log record to add more info
     *
     * @param array $record
     * @return array
     */
    public function processRecord(array $record)
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request) {
            $record['extra']['host'] = $request->getHost();
            $record['extra']['url'] = $request->getRequestUri();
            $record['extra']['query_string'] = $request->getQueryString();
            $record['extra']['scheme'] = $request->getScheme();
            $record['extra']['method'] = $request->getMethod();
            $record['extra']['content'] = $request->getContent();
        }

        return $record;
    }
}