<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class B2cAtributoGrupo
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="b2c_atributo_grupo")
 * @ORM\Entity
 */
class B2cAtributoGrupo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_atributo_grupo", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAtributoGrupo;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=30, nullable=true)
     */
    private $nome;

    public function setIdAtributoGrupo($idAtributoGrupo)
    {
        $this->idAtributoGrupo = $idAtributoGrupo;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAtributoGrupo()
    {
        return $this->idAtributoGrupo;
    }

    /**
     * @param $nome
     * @return $this
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }
}
