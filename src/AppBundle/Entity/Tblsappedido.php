<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tblsappedido
 *
 * @ORM\Table(name="tblSAPPedido")
 * @ORM\Entity
 */
class Tblsappedido
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedido", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPedido = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cliente", type="integer", nullable=true)
     */
    private $idCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_cliente", type="string", length=100, nullable=true)
     */
    private $nomeCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="cpf_cliente", type="string", length=14, nullable=true)
     */
    private $cpfCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="cnpj_cliente", type="string", length=18, nullable=true)
     */
    private $cnpjCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="rg_cliente", type="string", length=20, nullable=true)
     */
    private $rgCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="inscEst_cliente", type="string", length=20, nullable=true)
     */
    private $inscestCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="ddd_cliente", type="string", length=2, nullable=true)
     */
    private $dddCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="fone_cliente", type="string", length=10, nullable=true)
     */
    private $foneCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="email_cliente", type="string", length=100, nullable=true)
     */
    private $emailCliente;

    /**
     * @var string
     *
     * @ORM\Column(name="nomeEndereco", type="string", length=50, nullable=true)
     */
    private $nomeendereco;

    /**
     * @var string
     *
     * @ORM\Column(name="logradouro", type="string", length=20, nullable=true)
     */
    private $logradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="nomeRua", type="string", length=100, nullable=true)
     */
    private $nomerua;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=10, nullable=true)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="complemento", type="string", length=100, nullable=true)
     */
    private $complemento;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro", type="string", length=100, nullable=true)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=true)
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=100, nullable=true)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=2, nullable=true)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="entregaNomeEndereco", type="string", length=255, nullable=false)
     */
    private $entreganomeendereco;

    /**
     * @var string
     *
     * @ORM\Column(name="entregaLogradouro", type="string", length=255, nullable=true)
     */
    private $entregalogradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="entregaNomeRua", type="string", length=255, nullable=false)
     */
    private $entreganomerua;

    /**
     * @var string
     *
     * @ORM\Column(name="entregaNumero", type="string", length=10, nullable=true)
     */
    private $entreganumero;

    /**
     * @var string
     *
     * @ORM\Column(name="entregaComplemento", type="string", length=100, nullable=true)
     */
    private $entregacomplemento;

    /**
     * @var string
     *
     * @ORM\Column(name="entregaBairro", type="string", length=100, nullable=true)
     */
    private $entregabairro;

    /**
     * @var string
     *
     * @ORM\Column(name="entregaCep", type="string", length=10, nullable=true)
     */
    private $entregacep;

    /**
     * @var string
     *
     * @ORM\Column(name="entregaCidade", type="string", length=100, nullable=false)
     */
    private $entregacidade;

    /**
     * @var string
     *
     * @ORM\Column(name="entregaUf", type="string", length=2, nullable=false)
     */
    private $entregauf;

    /**
     * @var integer
     *
     * @ORM\Column(name="entregaCodIbge", type="integer", nullable=true)
     */
    private $entregacodibge;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_pedido", type="datetime", nullable=true)
     */
    private $dtPedido;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entrega", type="datetime", nullable=true)
     */
    private $dtEntrega;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entrega_fornecedor", type="datetime", nullable=true)
     */
    private $dtEntregaFornecedor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_remetido", type="datetime", nullable=true)
     */
    private $dtRemetido;

    /**
     * @var string
     *
     * @ORM\Column(name="observacao", type="string", length=250, nullable=true)
     */
    private $observacao;

    /**
     * @var string
     *
     * @ORM\Column(name="cond_pagamento", type="string", length=100, nullable=true)
     */
    private $condPagamento;

    /**
     * @var string
     *
     * @ORM\Column(name="cond_pagamento_2", type="string", length=100, nullable=true)
     */
    private $condPagamento2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_transportadora", type="integer", nullable=true)
     */
    private $idTransportadora;

    /**
     * @var integer
     *
     * @ORM\Column(name="tp_frete", type="integer", nullable=true)
     */
    private $tpFrete;

    /**
     * @var integer
     *
     * @ORM\Column(name="qt_embalagem", type="integer", nullable=true)
     */
    private $qtEmbalagem;

    /**
     * @var float
     *
     * @ORM\Column(name="vl_frete", type="float", precision=7, scale=2, nullable=true)
     */
    private $vlFrete;

    /**
     * @var string
     *
     * @ORM\Column(name="forma_pgto", type="string", length=20, nullable=false)
     */
    private $formaPgto;

    /**
     * @var string
     *
     * @ORM\Column(name="forma_pgto_2", type="string", length=20, nullable=true)
     */
    private $formaPgto2;

    /**
     * @var integer
     *
     * @ORM\Column(name="qt_parcelas", type="integer", nullable=false)
     */
    private $qtParcelas;

    /**
     * @var integer
     *
     * @ORM\Column(name="qt_parcelas_2", type="integer", nullable=true)
     */
    private $qtParcelas2;

    /**
     * @var float
     *
     * @ORM\Column(name="vl_parcelas", type="float", precision=9, scale=2, nullable=false)
     */
    private $vlParcelas;

    /**
     * @var float
     *
     * @ORM\Column(name="vl_parcelas_2", type="float", precision=9, scale=2, nullable=true)
     */
    private $vlParcelas2;

    /**
     * @var float
     *
     * @ORM\Column(name="vl_total", type="float", precision=9, scale=2, nullable=false)
     */
    private $vlTotal;

    /**
     * @var float
     *
     * @ORM\Column(name="per_desconto", type="float", precision=5, scale=2, nullable=true)
     */
    private $perDesconto;

    /**
     * @var string
     *
     * @ORM\Column(name="num_comprovante", type="string", length=20, nullable=true)
     */
    private $numComprovante;

    /**
     * @var string
     *
     * @ORM\Column(name="num_comprovante_2", type="string", length=20, nullable=true)
     */
    private $numComprovante2;

    /**
     * @var integer
     *
     * @ORM\Column(name="importou", type="integer", nullable=true)
     */
    private $importou;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_importacao", type="datetime", nullable=true)
     */
    private $dtImportacao;

    /**
     * @var string
     *
     * @ORM\Column(name="num_autorizacao", type="string", length=20, nullable=true)
     */
    private $numAutorizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="num_autorizacao_2", type="string", length=20, nullable=true)
     */
    private $numAutorizacao2;

    /**
     * @var string
     *
     * @ORM\Column(name="operadora", type="string", length=10, nullable=true)
     */
    private $operadora;

    /**
     * @var string
     *
     * @ORM\Column(name="operadora_2", type="string", length=10, nullable=true)
     */
    private $operadora2;

    /**
     * @var string
     *
     * @ORM\Column(name="cod_cupom", type="string", length=255, nullable=true)
     */
    private $codCupom;

    /**
     * @var float
     *
     * @ORM\Column(name="vl_subtotal", type="float", precision=9, scale=2, nullable=false)
     */
    private $vlSubtotal = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="conta_contabil_1", type="string", length=50, nullable=true)
     */
    private $contaContabil1;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_contabil_2", type="string", length=50, nullable=true)
     */
    private $contaContabil2;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = 'incluido';

    /**
     * @var integer
     *
     * @ORM\Column(name="cupom_credito", type="integer", nullable=false)
     */
    private $cupomCredito = 'b\'0\'';

    /**
     * @var float
     *
     * @ORM\Column(name="vl_cupom", type="float", precision=9, scale=2, nullable=true)
     */
    private $vlCupom;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_contabil_nome_1", type="string", length=30, nullable=true)
     */
    private $contaContabilNome1;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_contabil_nome_2", type="string", length=30, nullable=true)
     */
    private $contaContabilNome2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_captura_1", type="datetime", nullable=true)
     */
    private $dataCaptura1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_captura_2", type="datetime", nullable=true)
     */
    private $dataCaptura2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_pedido_venda", type="datetime", nullable=true)
     */
    private $dtPedidoVenda;


    /**
     * Get idPedido
     *
     * @return integer
     */
    public function getIdPedido()
    {
        return $this->idPedido;
    }

    /**
     * Set idCliente
     *
     * @param integer $idCliente
     *
     * @return Tblsappedido
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get idCliente
     *
     * @return integer
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set nomeCliente
     *
     * @param string $nomeCliente
     *
     * @return Tblsappedido
     */
    public function setNomeCliente($nomeCliente)
    {
        $this->nomeCliente = $nomeCliente;

        return $this;
    }

    /**
     * Get nomeCliente
     *
     * @return string
     */
    public function getNomeCliente()
    {
        return $this->nomeCliente;
    }

    /**
     * Set cpfCliente
     *
     * @param string $cpfCliente
     *
     * @return Tblsappedido
     */
    public function setCpfCliente($cpfCliente)
    {
        $this->cpfCliente = $cpfCliente;

        return $this;
    }

    /**
     * Get cpfCliente
     *
     * @return string
     */
    public function getCpfCliente()
    {
        return $this->cpfCliente;
    }

    /**
     * Set cnpjCliente
     *
     * @param string $cnpjCliente
     *
     * @return Tblsappedido
     */
    public function setCnpjCliente($cnpjCliente)
    {
        $this->cnpjCliente = $cnpjCliente;

        return $this;
    }

    /**
     * Get cnpjCliente
     *
     * @return string
     */
    public function getCnpjCliente()
    {
        return $this->cnpjCliente;
    }

    /**
     * Set rgCliente
     *
     * @param string $rgCliente
     *
     * @return Tblsappedido
     */
    public function setRgCliente($rgCliente)
    {
        $this->rgCliente = $rgCliente;

        return $this;
    }

    /**
     * Get rgCliente
     *
     * @return string
     */
    public function getRgCliente()
    {
        return $this->rgCliente;
    }

    /**
     * Set inscestCliente
     *
     * @param string $inscestCliente
     *
     * @return Tblsappedido
     */
    public function setInscestCliente($inscestCliente)
    {
        $this->inscestCliente = $inscestCliente;

        return $this;
    }

    /**
     * Get inscestCliente
     *
     * @return string
     */
    public function getInscestCliente()
    {
        return $this->inscestCliente;
    }

    /**
     * Set dddCliente
     *
     * @param string $dddCliente
     *
     * @return Tblsappedido
     */
    public function setDddCliente($dddCliente)
    {
        $this->dddCliente = $dddCliente;

        return $this;
    }

    /**
     * Get dddCliente
     *
     * @return string
     */
    public function getDddCliente()
    {
        return $this->dddCliente;
    }

    /**
     * Set foneCliente
     *
     * @param string $foneCliente
     *
     * @return Tblsappedido
     */
    public function setFoneCliente($foneCliente)
    {
        $this->foneCliente = $foneCliente;

        return $this;
    }

    /**
     * Get foneCliente
     *
     * @return string
     */
    public function getFoneCliente()
    {
        return $this->foneCliente;
    }

    /**
     * Set emailCliente
     *
     * @param string $emailCliente
     *
     * @return Tblsappedido
     */
    public function setEmailCliente($emailCliente)
    {
        $this->emailCliente = $emailCliente;

        return $this;
    }

    /**
     * Get emailCliente
     *
     * @return string
     */
    public function getEmailCliente()
    {
        return $this->emailCliente;
    }

    /**
     * Set nomeendereco
     *
     * @param string $nomeendereco
     *
     * @return Tblsappedido
     */
    public function setNomeendereco($nomeendereco)
    {
        $this->nomeendereco = $nomeendereco;

        return $this;
    }

    /**
     * Get nomeendereco
     *
     * @return string
     */
    public function getNomeendereco()
    {
        return $this->nomeendereco;
    }

    /**
     * Set logradouro
     *
     * @param string $logradouro
     *
     * @return Tblsappedido
     */
    public function setLogradouro($logradouro)
    {
        $this->logradouro = $logradouro;

        return $this;
    }

    /**
     * Get logradouro
     *
     * @return string
     */
    public function getLogradouro()
    {
        return $this->logradouro;
    }

    /**
     * Set nomerua
     *
     * @param string $nomerua
     *
     * @return Tblsappedido
     */
    public function setNomerua($nomerua)
    {
        $this->nomerua = $nomerua;

        return $this;
    }

    /**
     * Get nomerua
     *
     * @return string
     */
    public function getNomerua()
    {
        return $this->nomerua;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Tblsappedido
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set complemento
     *
     * @param string $complemento
     *
     * @return Tblsappedido
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;

        return $this;
    }

    /**
     * Get complemento
     *
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * Set bairro
     *
     * @param string $bairro
     *
     * @return Tblsappedido
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get bairro
     *
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * Set cep
     *
     * @param string $cep
     *
     * @return Tblsappedido
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     *
     * @return Tblsappedido
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Tblsappedido
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set entreganomeendereco
     *
     * @param string $entreganomeendereco
     *
     * @return Tblsappedido
     */
    public function setEntreganomeendereco($entreganomeendereco)
    {
        $this->entreganomeendereco = $entreganomeendereco;

        return $this;
    }

    /**
     * Get entreganomeendereco
     *
     * @return string
     */
    public function getEntreganomeendereco()
    {
        return $this->entreganomeendereco;
    }

    /**
     * Set entregalogradouro
     *
     * @param string $entregalogradouro
     *
     * @return Tblsappedido
     */
    public function setEntregalogradouro($entregalogradouro)
    {
        $this->entregalogradouro = $entregalogradouro;

        return $this;
    }

    /**
     * Get entregalogradouro
     *
     * @return string
     */
    public function getEntregalogradouro()
    {
        return $this->entregalogradouro;
    }

    /**
     * Set entreganomerua
     *
     * @param string $entreganomerua
     *
     * @return Tblsappedido
     */
    public function setEntreganomerua($entreganomerua)
    {
        $this->entreganomerua = $entreganomerua;

        return $this;
    }

    /**
     * Get entreganomerua
     *
     * @return string
     */
    public function getEntreganomerua()
    {
        return $this->entreganomerua;
    }

    /**
     * Set entreganumero
     *
     * @param string $entreganumero
     *
     * @return Tblsappedido
     */
    public function setEntreganumero($entreganumero)
    {
        $this->entreganumero = $entreganumero;

        return $this;
    }

    /**
     * Get entreganumero
     *
     * @return string
     */
    public function getEntreganumero()
    {
        return $this->entreganumero;
    }

    /**
     * Set entregacomplemento
     *
     * @param string $entregacomplemento
     *
     * @return Tblsappedido
     */
    public function setEntregacomplemento($entregacomplemento)
    {
        $this->entregacomplemento = $entregacomplemento;

        return $this;
    }

    /**
     * Get entregacomplemento
     *
     * @return string
     */
    public function getEntregacomplemento()
    {
        return $this->entregacomplemento;
    }

    /**
     * Set entregabairro
     *
     * @param string $entregabairro
     *
     * @return Tblsappedido
     */
    public function setEntregabairro($entregabairro)
    {
        $this->entregabairro = $entregabairro;

        return $this;
    }

    /**
     * Get entregabairro
     *
     * @return string
     */
    public function getEntregabairro()
    {
        return $this->entregabairro;
    }

    /**
     * Set entregacep
     *
     * @param string $entregacep
     *
     * @return Tblsappedido
     */
    public function setEntregacep($entregacep)
    {
        $this->entregacep = $entregacep;

        return $this;
    }

    /**
     * Get entregacep
     *
     * @return string
     */
    public function getEntregacep()
    {
        return $this->entregacep;
    }

    /**
     * Set entregacidade
     *
     * @param string $entregacidade
     *
     * @return Tblsappedido
     */
    public function setEntregacidade($entregacidade)
    {
        $this->entregacidade = $entregacidade;

        return $this;
    }

    /**
     * Get entregacidade
     *
     * @return string
     */
    public function getEntregacidade()
    {
        return $this->entregacidade;
    }

    /**
     * Set entregauf
     *
     * @param string $entregauf
     *
     * @return Tblsappedido
     */
    public function setEntregauf($entregauf)
    {
        $this->entregauf = $entregauf;

        return $this;
    }

    /**
     * Get entregauf
     *
     * @return string
     */
    public function getEntregauf()
    {
        return $this->entregauf;
    }

    /**
     * Set entregacodibge
     *
     * @param integer $entregacodibge
     *
     * @return Tblsappedido
     */
    public function setEntregacodibge($entregacodibge)
    {
        $this->entregacodibge = $entregacodibge;

        return $this;
    }

    /**
     * Get entregacodibge
     *
     * @return integer
     */
    public function getEntregacodibge()
    {
        return $this->entregacodibge;
    }

    /**
     * Set dtPedido
     *
     * @param \DateTime $dtPedido
     *
     * @return Tblsappedido
     */
    public function setDtPedido($dtPedido)
    {
        $this->dtPedido = $dtPedido;

        return $this;
    }

    /**
     * Get dtPedido
     *
     * @return \DateTime
     */
    public function getDtPedido()
    {
        return $this->dtPedido;
    }

    /**
     * Set dtEntrega
     *
     * @param \DateTime $dtEntrega
     *
     * @return Tblsappedido
     */
    public function setDtEntrega($dtEntrega)
    {
        $this->dtEntrega = $dtEntrega;

        return $this;
    }

    /**
     * Get dtEntrega
     *
     * @return \DateTime
     */
    public function getDtEntrega()
    {
        return $this->dtEntrega;
    }

    /**
     * Set dtEntregaFornecedor
     *
     * @param \DateTime $dtEntregaFornecedor
     *
     * @return Tblsappedido
     */
    public function setDtEntregaFornecedor($dtEntregaFornecedor)
    {
        $this->dtEntregaFornecedor = $dtEntregaFornecedor;

        return $this;
    }

    /**
     * Get dtEntregaFornecedor
     *
     * @return \DateTime
     */
    public function getDtEntregaFornecedor()
    {
        return $this->dtEntregaFornecedor;
    }

    /**
     * Set dtRemetido
     *
     * @param \DateTime $dtRemetido
     *
     * @return Tblsappedido
     */
    public function setDtRemetido($dtRemetido)
    {
        $this->dtRemetido = $dtRemetido;

        return $this;
    }

    /**
     * Get dtRemetido
     *
     * @return \DateTime
     */
    public function getDtRemetido()
    {
        return $this->dtRemetido;
    }

    /**
     * Set observacao
     *
     * @param string $observacao
     *
     * @return Tblsappedido
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get observacao
     *
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * Set condPagamento
     *
     * @param string $condPagamento
     *
     * @return Tblsappedido
     */
    public function setCondPagamento($condPagamento)
    {
        $this->condPagamento = $condPagamento;

        return $this;
    }

    /**
     * Get condPagamento
     *
     * @return string
     */
    public function getCondPagamento()
    {
        return $this->condPagamento;
    }

    /**
     * Set condPagamento2
     *
     * @param string $condPagamento2
     *
     * @return Tblsappedido
     */
    public function setCondPagamento2($condPagamento2)
    {
        $this->condPagamento2 = $condPagamento2;

        return $this;
    }

    /**
     * Get condPagamento2
     *
     * @return string
     */
    public function getCondPagamento2()
    {
        return $this->condPagamento2;
    }

    /**
     * Set idTransportadora
     *
     * @param integer $idTransportadora
     *
     * @return Tblsappedido
     */
    public function setIdTransportadora($idTransportadora)
    {
        $this->idTransportadora = $idTransportadora;

        return $this;
    }

    /**
     * Get idTransportadora
     *
     * @return integer
     */
    public function getIdTransportadora()
    {
        return $this->idTransportadora;
    }

    /**
     * Set tpFrete
     *
     * @param integer $tpFrete
     *
     * @return Tblsappedido
     */
    public function setTpFrete($tpFrete)
    {
        $this->tpFrete = $tpFrete;

        return $this;
    }

    /**
     * Get tpFrete
     *
     * @return integer
     */
    public function getTpFrete()
    {
        return $this->tpFrete;
    }

    /**
     * Set qtEmbalagem
     *
     * @param integer $qtEmbalagem
     *
     * @return Tblsappedido
     */
    public function setQtEmbalagem($qtEmbalagem)
    {
        $this->qtEmbalagem = $qtEmbalagem;

        return $this;
    }

    /**
     * Get qtEmbalagem
     *
     * @return integer
     */
    public function getQtEmbalagem()
    {
        return $this->qtEmbalagem;
    }

    /**
     * Set vlFrete
     *
     * @param float $vlFrete
     *
     * @return Tblsappedido
     */
    public function setVlFrete($vlFrete)
    {
        $this->vlFrete = $vlFrete;

        return $this;
    }

    /**
     * Get vlFrete
     *
     * @return float
     */
    public function getVlFrete()
    {
        return $this->vlFrete;
    }

    /**
     * Set formaPgto
     *
     * @param string $formaPgto
     *
     * @return Tblsappedido
     */
    public function setFormaPgto($formaPgto)
    {
        $this->formaPgto = $formaPgto;

        return $this;
    }

    /**
     * Get formaPgto
     *
     * @return string
     */
    public function getFormaPgto()
    {
        return $this->formaPgto;
    }

    /**
     * Set formaPgto2
     *
     * @param string $formaPgto2
     *
     * @return Tblsappedido
     */
    public function setFormaPgto2($formaPgto2)
    {
        $this->formaPgto2 = $formaPgto2;

        return $this;
    }

    /**
     * Get formaPgto2
     *
     * @return string
     */
    public function getFormaPgto2()
    {
        return $this->formaPgto2;
    }

    /**
     * Set qtParcelas
     *
     * @param integer $qtParcelas
     *
     * @return Tblsappedido
     */
    public function setQtParcelas($qtParcelas)
    {
        $this->qtParcelas = $qtParcelas;

        return $this;
    }

    /**
     * Get qtParcelas
     *
     * @return integer
     */
    public function getQtParcelas()
    {
        return $this->qtParcelas;
    }

    /**
     * Set qtParcelas2
     *
     * @param integer $qtParcelas2
     *
     * @return Tblsappedido
     */
    public function setQtParcelas2($qtParcelas2)
    {
        $this->qtParcelas2 = $qtParcelas2;

        return $this;
    }

    /**
     * Get qtParcelas2
     *
     * @return integer
     */
    public function getQtParcelas2()
    {
        return $this->qtParcelas2;
    }

    /**
     * Set vlParcelas
     *
     * @param float $vlParcelas
     *
     * @return Tblsappedido
     */
    public function setVlParcelas($vlParcelas)
    {
        $this->vlParcelas = $vlParcelas;

        return $this;
    }

    /**
     * Get vlParcelas
     *
     * @return float
     */
    public function getVlParcelas()
    {
        return $this->vlParcelas;
    }

    /**
     * Set vlParcelas2
     *
     * @param float $vlParcelas2
     *
     * @return Tblsappedido
     */
    public function setVlParcelas2($vlParcelas2)
    {
        $this->vlParcelas2 = $vlParcelas2;

        return $this;
    }

    /**
     * Get vlParcelas2
     *
     * @return float
     */
    public function getVlParcelas2()
    {
        return $this->vlParcelas2;
    }

    /**
     * Set vlTotal
     *
     * @param float $vlTotal
     *
     * @return Tblsappedido
     */
    public function setVlTotal($vlTotal)
    {
        $this->vlTotal = $vlTotal;

        return $this;
    }

    /**
     * Get vlTotal
     *
     * @return float
     */
    public function getVlTotal()
    {
        return $this->vlTotal;
    }

    /**
     * Set perDesconto
     *
     * @param float $perDesconto
     *
     * @return Tblsappedido
     */
    public function setPerDesconto($perDesconto)
    {
        $this->perDesconto = $perDesconto;

        return $this;
    }

    /**
     * Get perDesconto
     *
     * @return float
     */
    public function getPerDesconto()
    {
        return $this->perDesconto;
    }

    /**
     * Set numComprovante
     *
     * @param string $numComprovante
     *
     * @return Tblsappedido
     */
    public function setNumComprovante($numComprovante)
    {
        $this->numComprovante = $numComprovante;

        return $this;
    }

    /**
     * Get numComprovante
     *
     * @return string
     */
    public function getNumComprovante()
    {
        return $this->numComprovante;
    }

    /**
     * Set numComprovante2
     *
     * @param string $numComprovante2
     *
     * @return Tblsappedido
     */
    public function setNumComprovante2($numComprovante2)
    {
        $this->numComprovante2 = $numComprovante2;

        return $this;
    }

    /**
     * Get numComprovante2
     *
     * @return string
     */
    public function getNumComprovante2()
    {
        return $this->numComprovante2;
    }

    /**
     * Set importou
     *
     * @param integer $importou
     *
     * @return Tblsappedido
     */
    public function setImportou($importou)
    {
        $this->importou = $importou;

        return $this;
    }

    /**
     * Get importou
     *
     * @return integer
     */
    public function getImportou()
    {
        return $this->importou;
    }

    /**
     * Set dtImportacao
     *
     * @param \DateTime $dtImportacao
     *
     * @return Tblsappedido
     */
    public function setDtImportacao($dtImportacao)
    {
        $this->dtImportacao = $dtImportacao;

        return $this;
    }

    /**
     * Get dtImportacao
     *
     * @return \DateTime
     */
    public function getDtImportacao()
    {
        return $this->dtImportacao;
    }

    /**
     * Set numAutorizacao
     *
     * @param string $numAutorizacao
     *
     * @return Tblsappedido
     */
    public function setNumAutorizacao($numAutorizacao)
    {
        $this->numAutorizacao = $numAutorizacao;

        return $this;
    }

    /**
     * Get numAutorizacao
     *
     * @return string
     */
    public function getNumAutorizacao()
    {
        return $this->numAutorizacao;
    }

    /**
     * Set numAutorizacao2
     *
     * @param string $numAutorizacao2
     *
     * @return Tblsappedido
     */
    public function setNumAutorizacao2($numAutorizacao2)
    {
        $this->numAutorizacao2 = $numAutorizacao2;

        return $this;
    }

    /**
     * Get numAutorizacao2
     *
     * @return string
     */
    public function getNumAutorizacao2()
    {
        return $this->numAutorizacao2;
    }

    /**
     * Set operadora
     *
     * @param string $operadora
     *
     * @return Tblsappedido
     */
    public function setOperadora($operadora)
    {
        $this->operadora = $operadora;

        return $this;
    }

    /**
     * Get operadora
     *
     * @return string
     */
    public function getOperadora()
    {
        return $this->operadora;
    }

    /**
     * Set operadora2
     *
     * @param string $operadora2
     *
     * @return Tblsappedido
     */
    public function setOperadora2($operadora2)
    {
        $this->operadora2 = $operadora2;

        return $this;
    }

    /**
     * Get operadora2
     *
     * @return string
     */
    public function getOperadora2()
    {
        return $this->operadora2;
    }

    /**
     * Set codCupom
     *
     * @param string $codCupom
     *
     * @return Tblsappedido
     */
    public function setCodCupom($codCupom)
    {
        $this->codCupom = $codCupom;

        return $this;
    }

    /**
     * Get codCupom
     *
     * @return string
     */
    public function getCodCupom()
    {
        return $this->codCupom;
    }

    /**
     * Set vlSubtotal
     *
     * @param float $vlSubtotal
     *
     * @return Tblsappedido
     */
    public function setVlSubtotal($vlSubtotal)
    {
        $this->vlSubtotal = $vlSubtotal;

        return $this;
    }

    /**
     * Get vlSubtotal
     *
     * @return float
     */
    public function getVlSubtotal()
    {
        return $this->vlSubtotal;
    }

    /**
     * Set contaContabil1
     *
     * @param string $contaContabil1
     *
     * @return Tblsappedido
     */
    public function setContaContabil1($contaContabil1)
    {
        $this->contaContabil1 = $contaContabil1;

        return $this;
    }

    /**
     * Get contaContabil1
     *
     * @return string
     */
    public function getContaContabil1()
    {
        return $this->contaContabil1;
    }

    /**
     * Set contaContabil2
     *
     * @param string $contaContabil2
     *
     * @return Tblsappedido
     */
    public function setContaContabil2($contaContabil2)
    {
        $this->contaContabil2 = $contaContabil2;

        return $this;
    }

    /**
     * Get contaContabil2
     *
     * @return string
     */
    public function getContaContabil2()
    {
        return $this->contaContabil2;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Tblsappedido
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set cupomCredito
     *
     * @param integer $cupomCredito
     *
     * @return Tblsappedido
     */
    public function setCupomCredito($cupomCredito)
    {
        $this->cupomCredito = $cupomCredito;

        return $this;
    }

    /**
     * Get cupomCredito
     *
     * @return integer
     */
    public function getCupomCredito()
    {
        return $this->cupomCredito;
    }

    /**
     * Set vlCupom
     *
     * @param float $vlCupom
     *
     * @return Tblsappedido
     */
    public function setVlCupom($vlCupom)
    {
        $this->vlCupom = $vlCupom;

        return $this;
    }

    /**
     * Get vlCupom
     *
     * @return float
     */
    public function getVlCupom()
    {
        return $this->vlCupom;
    }

    /**
     * Set contaContabilNome1
     *
     * @param string $contaContabilNome1
     *
     * @return Tblsappedido
     */
    public function setContaContabilNome1($contaContabilNome1)
    {
        $this->contaContabilNome1 = $contaContabilNome1;

        return $this;
    }

    /**
     * Get contaContabilNome1
     *
     * @return string
     */
    public function getContaContabilNome1()
    {
        return $this->contaContabilNome1;
    }

    /**
     * Set contaContabilNome2
     *
     * @param string $contaContabilNome2
     *
     * @return Tblsappedido
     */
    public function setContaContabilNome2($contaContabilNome2)
    {
        $this->contaContabilNome2 = $contaContabilNome2;

        return $this;
    }

    /**
     * Get contaContabilNome2
     *
     * @return string
     */
    public function getContaContabilNome2()
    {
        return $this->contaContabilNome2;
    }

    /**
     * Set dataCaptura1
     *
     * @param \DateTime $dataCaptura1
     *
     * @return Tblsappedido
     */
    public function setDataCaptura1($dataCaptura1)
    {
        $this->dataCaptura1 = $dataCaptura1;

        return $this;
    }

    /**
     * Get dataCaptura1
     *
     * @return \DateTime
     */
    public function getDataCaptura1()
    {
        return $this->dataCaptura1;
    }

    /**
     * Set dataCaptura2
     *
     * @param \DateTime $dataCaptura2
     *
     * @return Tblsappedido
     */
    public function setDataCaptura2($dataCaptura2)
    {
        $this->dataCaptura2 = $dataCaptura2;

        return $this;
    }

    /**
     * Get dataCaptura2
     *
     * @return \DateTime
     */
    public function getDataCaptura2()
    {
        return $this->dataCaptura2;
    }

    /**
     * Set dtPedidoVenda
     *
     * @param \DateTime $dtPedidoVenda
     *
     * @return Tblsappedido
     */
    public function setDtPedidoVenda($dtPedidoVenda)
    {
        $this->dtPedidoVenda = $dtPedidoVenda;

        return $this;
    }

    /**
     * Get dtPedidoVenda
     *
     * @return \DateTime
     */
    public function getDtPedidoVenda()
    {
        return $this->dtPedidoVenda;
    }
}
