<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * B2cRegreg
 *
 * @ORM\Table(name="b2c_regreg", indexes={@ORM\Index(name="internacional", columns={"internacional"})})
 * @ORM\Entity
 */
class B2cRegreg
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="regiao", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $regiao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="internacional", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $internacional;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cliente_escolhe", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $clienteEscolhe;

    /**
     * @var string
     *
     * @ORM\Column(name="paises", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $paises;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\B2cRegcep", mappedBy="idReg")
     */
    private $regCep;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->regCep = new ArrayCollection();
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param $regCep
     * @return $this
     */
    public function setRegCep($regCep)
    {
        $this->regCep = $regCep;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set regiao
     *
     * @param string $regiao
     *
     * @return B2cRegreg
     */
    public function setRegiao($regiao)
    {
        $this->regiao = $regiao;

        return $this;
    }

    /**
     * Get regiao
     *
     * @return string
     */
    public function getRegiao()
    {
        return $this->regiao;
    }

    /**
     * Set internacional
     *
     * @param boolean $internacional
     *
     * @return B2cRegreg
     */
    public function setInternacional($internacional)
    {
        $this->internacional = $internacional;

        return $this;
    }

    /**
     * Get internacional
     *
     * @return boolean
     */
    public function getInternacional()
    {
        return $this->internacional;
    }

    /**
     * Set clienteEscolhe
     *
     * @param boolean $clienteEscolhe
     *
     * @return B2cRegreg
     */
    public function setClienteEscolhe($clienteEscolhe)
    {
        $this->clienteEscolhe = $clienteEscolhe;

        return $this;
    }

    /**
     * Get clienteEscolhe
     *
     * @return boolean
     */
    public function getClienteEscolhe()
    {
        return $this->clienteEscolhe;
    }

    /**
     * Set paises
     *
     * @param string $paises
     *
     * @return B2cRegreg
     */
    public function setPaises($paises)
    {
        $this->paises = $paises;

        return $this;
    }

    /**
     * Get paises
     *
     * @return string
     */
    public function getPaises()
    {
        return $this->paises;
    }
}
