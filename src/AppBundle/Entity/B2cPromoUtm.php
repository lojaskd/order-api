<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoUtm
 *
 * @ORM\Table(name="b2c_promo_utm")
 * @ORM\Entity
 */
class B2cPromoUtm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo_utm", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPromoUtm;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idPromo;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $content;

    /**
     * @param $idPromoUtm
     * @return $this
     */
    public function setIdPromoUtm($idPromoUtm)
    {
        $this->idPromoUtm = $idPromoUtm;
        return $this;
    }

    /**
     * Get idPromoUtm
     *
     * @return integer
     */
    public function getIdPromoUtm()
    {
        return $this->idPromoUtm;
    }

    /**
     * Set idPromo
     *
     * @param integer $idPromo
     *
     * @return B2cPromoUtm
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;

        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return B2cPromoUtm
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return B2cPromoUtm
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}
