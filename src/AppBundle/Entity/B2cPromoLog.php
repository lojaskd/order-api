<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoLog
 *
 * @ORM\Table(name="b2c_promo_log")
 * @ORM\Entity
 */
class B2cPromoLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo_log", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPromoLog;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idPromo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cfgusu", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idCfgusu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ocorrido", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ocorrido;

    /**
     * @var string
     *
     * @ORM\Column(name="post", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $post;

    /**
     * @var string
     *
     * @ORM\Column(name="get", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $get;

    /**
     * @var string
     *
     * @ORM\Column(name="cookies", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $cookies;

    /**
     * @var string
     *
     * @ORM\Column(name="session", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $session;

    /**
     * @param $idPromoLog
     * @return $this
     */
    public function setIdPromoLog($idPromoLog)
    {
        $this->idPromoLog = $idPromoLog;
        return $this;
    }

    /**
     * Get idPromoLog
     *
     * @return integer
     */
    public function getIdPromoLog()
    {
        return $this->idPromoLog;
    }

    /**
     * Set idPromo
     *
     * @param integer $idPromo
     *
     * @return B2cPromoLog
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;

        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set idCfgusu
     *
     * @param integer $idCfgusu
     *
     * @return B2cPromoLog
     */
    public function setIdCfgusu($idCfgusu)
    {
        $this->idCfgusu = $idCfgusu;

        return $this;
    }

    /**
     * Get idCfgusu
     *
     * @return integer
     */
    public function getIdCfgusu()
    {
        return $this->idCfgusu;
    }

    /**
     * Set ocorrido
     *
     * @param \DateTime $ocorrido
     *
     * @return B2cPromoLog
     */
    public function setOcorrido($ocorrido)
    {
        $this->ocorrido = $ocorrido;

        return $this;
    }

    /**
     * Get ocorrido
     *
     * @return \DateTime
     */
    public function getOcorrido()
    {
        return $this->ocorrido;
    }

    /**
     * Set post
     *
     * @param string $post
     *
     * @return B2cPromoLog
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return string
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set get
     *
     * @param string $get
     *
     * @return B2cPromoLog
     */
    public function setGet($get)
    {
        $this->get = $get;

        return $this;
    }

    /**
     * Get get
     *
     * @return string
     */
    public function getGet()
    {
        return $this->get;
    }

    /**
     * Set cookies
     *
     * @param string $cookies
     *
     * @return B2cPromoLog
     */
    public function setCookies($cookies)
    {
        $this->cookies = $cookies;

        return $this;
    }

    /**
     * Get cookies
     *
     * @return string
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * Set session
     *
     * @param string $session
     *
     * @return B2cPromoLog
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return string
     */
    public function getSession()
    {
        return $this->session;
    }
}
