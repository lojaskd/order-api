<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tblsappedidoitem
 *
 * @ORM\Table(name="tblSAPPedidoItem", indexes={@ORM\Index(name="IDX_3F413E81E2DBA323", columns={"id_pedido"})})
 * @ORM\Entity
 */
class Tblsappedidoitem
{
    /**
     * @var string
     *
     * @ORM\Column(name="id_item", type="string", length=20, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idItem = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="qt_vendida", type="float", precision=15, scale=2, nullable=true)
     */
    private $qtVendida;

    /**
     * @var float
     *
     * @ORM\Column(name="vl_unitario", type="float", precision=7, scale=2, nullable=true)
     */
    private $vlUnitario;

    /**
     * @var float
     *
     * @ORM\Column(name="per_desconto", type="float", precision=5, scale=2, nullable=true)
     */
    private $perDesconto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_disponivel", type="date", nullable=false)
     */
    private $dtDisponivel = '0000-00-00';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_filial", type="integer", nullable=false)
     */
    private $idFilial = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = 'incluido';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_supply", type="date", nullable=true)
     */
    private $dtSupply;

    /**
     * @var integer
     *
     * @ORM\Column(name="entrega_imediata", type="integer", nullable=true)
     */
    private $entregaImediata;

    /**
     * @var string
     *
     * @ORM\Column(name="destino", type="string", length=45, nullable=true)
     */
    private $destino;

    /**
     * @var Tblsappedido
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Tblsappedido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pedido", referencedColumnName="id_pedido")
     * })
     */
    private $idPedido;


    /**
     * Set idItem
     *
     * @param string $idItem
     *
     * @return Tblsappedidoitem
     */
    public function setIdItem($idItem)
    {
        $this->idItem = $idItem;

        return $this;
    }

    /**
     * Get idItem
     *
     * @return string
     */
    public function getIdItem()
    {
        return $this->idItem;
    }

    /**
     * Set qtVendida
     *
     * @param float $qtVendida
     *
     * @return Tblsappedidoitem
     */
    public function setQtVendida($qtVendida)
    {
        $this->qtVendida = $qtVendida;

        return $this;
    }

    /**
     * Get qtVendida
     *
     * @return float
     */
    public function getQtVendida()
    {
        return $this->qtVendida;
    }

    /**
     * Set vlUnitario
     *
     * @param float $vlUnitario
     *
     * @return Tblsappedidoitem
     */
    public function setVlUnitario($vlUnitario)
    {
        $this->vlUnitario = $vlUnitario;

        return $this;
    }

    /**
     * Get vlUnitario
     *
     * @return float
     */
    public function getVlUnitario()
    {
        return $this->vlUnitario;
    }

    /**
     * Set perDesconto
     *
     * @param float $perDesconto
     *
     * @return Tblsappedidoitem
     */
    public function setPerDesconto($perDesconto)
    {
        $this->perDesconto = $perDesconto;

        return $this;
    }

    /**
     * Get perDesconto
     *
     * @return float
     */
    public function getPerDesconto()
    {
        return $this->perDesconto;
    }

    /**
     * Set dtDisponivel
     *
     * @param \DateTime $dtDisponivel
     *
     * @return Tblsappedidoitem
     */
    public function setDtDisponivel($dtDisponivel)
    {
        $this->dtDisponivel = $dtDisponivel;

        return $this;
    }

    /**
     * Get dtDisponivel
     *
     * @return \DateTime
     */
    public function getDtDisponivel()
    {
        return $this->dtDisponivel;
    }

    /**
     * Set idFilial
     *
     * @param integer $idFilial
     *
     * @return Tblsappedidoitem
     */
    public function setIdFilial($idFilial)
    {
        $this->idFilial = $idFilial;

        return $this;
    }

    /**
     * Get idFilial
     *
     * @return integer
     */
    public function getIdFilial()
    {
        return $this->idFilial;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Tblsappedidoitem
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dtSupply
     *
     * @param \DateTime $dtSupply
     *
     * @return Tblsappedidoitem
     */
    public function setDtSupply($dtSupply)
    {
        $this->dtSupply = $dtSupply;

        return $this;
    }

    /**
     * Get dtSupply
     *
     * @return \DateTime
     */
    public function getDtSupply()
    {
        return $this->dtSupply;
    }

    /**
     * Set entregaImediata
     *
     * @param integer $entregaImediata
     *
     * @return Tblsappedidoitem
     */
    public function setEntregaImediata($entregaImediata)
    {
        $this->entregaImediata = $entregaImediata;

        return $this;
    }

    /**
     * Get entregaImediata
     *
     * @return integer
     */
    public function getEntregaImediata()
    {
        return $this->entregaImediata;
    }

    /**
     * Set destino
     *
     * @param string $destino
     *
     * @return Tblsappedidoitem
     */
    public function setDestino($destino)
    {
        $this->destino = $destino;

        return $this;
    }

    /**
     * Get destino
     *
     * @return string
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * Set idPedido
     *
     * @param Tblsappedido $idPedido
     *
     * @return Tblsappedidoitem
     */
    public function setIdPedido(Tblsappedido $idPedido)
    {
        $this->idPedido = $idPedido;

        return $this;
    }

    /**
     * Get idPedido
     *
     * @return Tblsappedido
     */
    public function getIdPedido()
    {
        return $this->idPedido;
    }
}
