<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cProdutosKits
 *
 * @ORM\Table(name="b2c_produtos_kits", uniqueConstraints={@ORM\UniqueConstraint(name="produto_kit_unico", columns={"id_kit", "id_produto"})}, indexes={@ORM\Index(name="id_produto", columns={"id_produto"}), @ORM\Index(name="id_kit", columns={"id_kit"}), @ORM\Index(name="obrigatorio", columns={"obrigatorio"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cProdutosKitsRepository")
 */
class B2cProdutosKits
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id_kit", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idKit;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id_produto", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idProduto;

    /**
     * @var float
     *
     * @ORM\Column(name="preco_produto", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $precoProduto;

    /**
     * @var string
     *
     * @ORM\Column(name="quantidade", type="decimal", precision=11, scale=2, nullable=false, unique=false)
     */
    private $quantidade;

    /**
     * @var boolean
     *
     * @ORM\Column(name="desconto", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $desconto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="obrigatorio", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $obrigatorio;

    /**
     * @var boolean
     *
     * @ORM\Column(name="desmarcar", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $desmarcar;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ordem", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ordem;

    /**
     * @var boolean
     *
     * @ORM\Column(name="automatico", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $automatico;



    /**
     * Set idKit
     *
     * @param integer $idKit
     *
     * @return B2cProdutosKits
     */
    public function setIdKit($idKit)
    {
        $this->idKit = $idKit;

        return $this;
    }

    /**
     * Get idKit
     *
     * @return integer
     */
    public function getIdKit()
    {
        return $this->idKit;
    }

    /**
     * Set idProduto
     *
     * @param integer $idProduto
     *
     * @return B2cProdutosKits
     */
    public function setIdProduto($idProduto)
    {
        $this->idProduto = $idProduto;

        return $this;
    }

    /**
     * Get idProduto
     *
     * @return integer
     */
    public function getIdProduto()
    {
        return $this->idProduto;
    }

    /**
     * Set precoProduto
     *
     * @param float $precoProduto
     *
     * @return B2cProdutosKits
     */
    public function setPrecoProduto($precoProduto)
    {
        $this->precoProduto = $precoProduto;

        return $this;
    }

    /**
     * Get precoProduto
     *
     * @return float
     */
    public function getPrecoProduto()
    {
        return $this->precoProduto;
    }

    /**
     * Set quantidade
     *
     * @param string $quantidade
     *
     * @return B2cProdutosKits
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;

        return $this;
    }

    /**
     * Get quantidade
     *
     * @return string
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * Set desconto
     *
     * @param boolean $desconto
     *
     * @return B2cProdutosKits
     */
    public function setDesconto($desconto)
    {
        $this->desconto = $desconto;

        return $this;
    }

    /**
     * Get desconto
     *
     * @return boolean
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * Set obrigatorio
     *
     * @param boolean $obrigatorio
     *
     * @return B2cProdutosKits
     */
    public function setObrigatorio($obrigatorio)
    {
        $this->obrigatorio = $obrigatorio;

        return $this;
    }

    /**
     * Get obrigatorio
     *
     * @return boolean
     */
    public function getObrigatorio()
    {
        return $this->obrigatorio;
    }

    /**
     * Set desmarcar
     *
     * @param boolean $desmarcar
     *
     * @return B2cProdutosKits
     */
    public function setDesmarcar($desmarcar)
    {
        $this->desmarcar = $desmarcar;

        return $this;
    }

    /**
     * Get desmarcar
     *
     * @return boolean
     */
    public function getDesmarcar()
    {
        return $this->desmarcar;
    }

    /**
     * Set ordem
     *
     * @param boolean $ordem
     *
     * @return B2cProdutosKits
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * Get ordem
     *
     * @return boolean
     */
    public function getOrdem()
    {
        return $this->ordem;
    }

    /**
     * Set automatico
     *
     * @param boolean $automatico
     *
     * @return B2cProdutosKits
     */
    public function setAutomatico($automatico)
    {
        $this->automatico = $automatico;

        return $this;
    }

    /**
     * Get automatico
     *
     * @return boolean
     */
    public function getAutomatico()
    {
        return $this->automatico;
    }
}
