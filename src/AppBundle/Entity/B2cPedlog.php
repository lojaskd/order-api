<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPedlog
 *
 * @ORM\Table(name="b2c_pedlog", indexes={@ORM\Index(name="id_pedped", columns={"id_pedped"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPedlogRepository")
 */
class B2cPedlog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedlog", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPedlog;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idPedped;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=200, precision=0, scale=0, nullable=true, unique=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $texto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $data;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mostrar_areavip", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $mostrarAreavip;

    /**
     * @var integer
     *
     * @ORM\Column(name="status_atual", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $statusAtual;

    /**
     * @var integer
     *
     * @ORM\Column(name="status_internos_atual", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $statusInternosAtual;

    /**
     * @param $idPedlog
     * @return $this
     */
    public function setIdPedlog($idPedlog)
    {
        $this->idPedlog = $idPedlog;
        return $this;
    }

    /**
     * Get idPedlog
     *
     * @return integer
     */
    public function getIdPedlog()
    {
        return $this->idPedlog;
    }

    /**
     * Set idPedped
     *
     * @param integer $idPedped
     *
     * @return B2cPedlog
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;

        return $this;
    }

    /**
     * Get idPedped
     *
     * @return integer
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return B2cPedlog
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set texto
     *
     * @param string $texto
     *
     * @return B2cPedlog
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set data
     *
     * @param \DateTime $date
     *
     * @return B2cPedlog
     */
    public function setData($date)
    {
        $this->data = $date;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set mostrarAreavip
     *
     * @param boolean $mostrarAreavip
     *
     * @return B2cPedlog
     */
    public function setMostrarAreavip($mostrarAreavip)
    {
        $this->mostrarAreavip = $mostrarAreavip;

        return $this;
    }

    /**
     * Get mostrarAreavip
     *
     * @return boolean
     */
    public function getMostrarAreavip()
    {
        return $this->mostrarAreavip;
    }

    /**
     * Set statusAtual
     *
     * @param integer $statusAtual
     *
     * @return B2cPedlog
     */
    public function setStatusAtual($statusAtual)
    {
        $this->statusAtual = $statusAtual;

        return $this;
    }

    /**
     * Get statusAtual
     *
     * @return integer
     */
    public function getStatusAtual()
    {
        return $this->statusAtual;
    }

    /**
     * Set statusInternosAtual
     *
     * @param integer $statusInternosAtual
     *
     * @return B2cPedlog
     */
    public function setStatusInternosAtual($statusInternosAtual)
    {
        $this->statusInternosAtual = $statusInternosAtual;

        return $this;
    }

    /**
     * Get statusInternosAtual
     *
     * @return integer
     */
    public function getStatusInternosAtual()
    {
        return $this->statusInternosAtual;
    }
}
