<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class AttributesEntities
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="attributes_entities")
 * @ORM\Entity()
 */
class AttributesEntities extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $entity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_attr", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idAttr;

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     * @return $this
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAttr()
    {
        return $this->idAttr;
    }

    /**
     * @param int $idAttr
     * @return $this
     */
    public function setIdAttr($idAttr)
    {
        $this->idAttr = $idAttr;
        return $this;
    }
}
