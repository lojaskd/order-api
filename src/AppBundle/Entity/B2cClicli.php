<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cClicli
 *
 * @ORM\Table(name="b2c_clicli", uniqueConstraints={@ORM\UniqueConstraint(name="cpf", columns={"cpf_cgc", "email"})}, indexes={@ORM\Index(name="email", columns={"email"}), @ORM\Index(name="senha", columns={"senha"}), @ORM\Index(name="idx_data", columns={"cadastro", "primeiro_acesso"}), @ORM\Index(name="nome", columns={"nome"}), @ORM\Index(name="sobrenome", columns={"sobrenome"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cClicliRepository")
 */
class B2cClicli
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_clicli", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idClicli;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_pessoa", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tipoPessoa;

    /**
     * @var string
     *
     * @ORM\Column(name="cpf_cgc", type="string", length=20, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cpfCgc;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="sobrenome", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $sobrenome;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="senha", type="string", length=20, precision=0, scale=0, nullable=true, unique=false)
     */
    private $senha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_nasc", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtaNasc;

    /**
     * @var string
     *
     * @ORM\Column(name="rg_ie", type="string", length=12, precision=0, scale=0, nullable=true, unique=false)
     */
    private $rgIe;

    /**
     * @var string
     *
     * @ORM\Column(name="contato", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $contato;

    /**
     * @var string
     *
     * @ORM\Column(name="sexo", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $sexo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="newsletter", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $newsletter;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco", type="string", length=200, precision=0, scale=0, nullable=true, unique=false)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="complemento", type="string", length=200, precision=0, scale=0, nullable=true, unique=false)
     */
    private $complemento;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro", type="string", length=200, precision=0, scale=0, nullable=true, unique=false)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=2, precision=0, scale=0, nullable=true, unique=false)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="provincia", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $provincia;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cidade;

    /**
     * @var integer
     *
     * @ORM\Column(name="pais", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pais;

    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone_a", type="string", length=16, precision=0, scale=0, nullable=true, unique=false)
     */
    private $telefoneA;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone_b", type="string", length=16, precision=0, scale=0, nullable=true, unique=false)
     */
    private $telefoneB;

    /**
     * @var string
     *
     * @ORM\Column(name="referencia", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $referencia;

    /**
     * @var string
     *
     * @ORM\Column(name="senha_b64", type="string", length=20, precision=0, scale=0, nullable=true, unique=false)
     */
    private $senhaB64;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cadastro", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $cadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="primeiro_acesso", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $primeiroAcesso;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_status", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $descStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_status", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_afiafi", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dataAfiafi;

    /**
     * @var integer
     *
     * @ORM\Column(name="afiliado", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $afiliado;

    /**
     * @var string
     *
     * @ORM\Column(name="primeira_origem", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $primeiraOrigem;

    /**
     * @var string
     *
     * @ORM\Column(name="cadastro_origem", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cadastroOrigem;

    /**
     * @var string
     *
     * @ORM\Column(name="cliente_origem", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $clienteOrigem;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cliente_origem", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataClienteOrigem;

    /**
     * @var string
     *
     * @ORM\Column(name="primeira_origem_afiliado", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $primeiraOrigemAfiliado;

    /**
     * @var boolean
     *
     * @ORM\Column(name="processadoWS", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $processadows;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_atualizacao", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtaAtualizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="sf_account", type="string", length=18, precision=0, scale=0, nullable=true, unique=false)
     */
    private $sfAccount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sf_data_sync", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $sfDataSync;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_cadastro", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $tipoCadastro;

    /**
     * @param $idClicli
     * @return $this
     */
    public function setIdClicli($idClicli)
    {
        $this->idClicli = $idClicli;
        return $this;
    }

    /**
     * Get idClicli
     *
     * @return integer
     */
    public function getIdClicli()
    {
        return $this->idClicli;
    }

    /**
     * Set tipoPessoa
     *
     * @param string $tipoPessoa
     *
     * @return B2cClicli
     */
    public function setTipoPessoa($tipoPessoa)
    {
        $this->tipoPessoa = $tipoPessoa;

        return $this;
    }

    /**
     * Get tipoPessoa
     *
     * @return string
     */
    public function getTipoPessoa()
    {
        return $this->tipoPessoa;
    }

    /**
     * Set cpfCgc
     *
     * @param string $cpfCgc
     *
     * @return B2cClicli
     */
    public function setCpfCgc($cpfCgc)
    {
        $this->cpfCgc = $cpfCgc;

        return $this;
    }

    /**
     * Get cpfCgc
     *
     * @return string
     */
    public function getCpfCgc()
    {
        return $this->cpfCgc;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cClicli
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set sobrenome
     *
     * @param string $sobrenome
     *
     * @return B2cClicli
     */
    public function setSobrenome($sobrenome)
    {
        $this->sobrenome = $sobrenome;

        return $this;
    }

    /**
     * Get sobrenome
     *
     * @return string
     */
    public function getSobrenome()
    {
        return $this->sobrenome;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return B2cClicli
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set senha
     *
     * @param string $senha
     *
     * @return B2cClicli
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;

        return $this;
    }

    /**
     * Get senha
     *
     * @return string
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * Set dtaNasc
     *
     * @param \DateTime $dtaNasc
     *
     * @return B2cClicli
     */
    public function setDtaNasc($dtaNasc)
    {
        $this->dtaNasc = $dtaNasc;

        return $this;
    }

    /**
     * Get dtaNasc
     *
     * @return \DateTime
     */
    public function getDtaNasc()
    {
        return $this->dtaNasc;
    }

    /**
     * Set rgIe
     *
     * @param string $rgIe
     *
     * @return B2cClicli
     */
    public function setRgIe($rgIe)
    {
        $this->rgIe = $rgIe;

        return $this;
    }

    /**
     * Get rgIe
     *
     * @return string
     */
    public function getRgIe()
    {
        return $this->rgIe;
    }

    /**
     * Set contato
     *
     * @param string $contato
     *
     * @return B2cClicli
     */
    public function setContato($contato)
    {
        $this->contato = $contato;

        return $this;
    }

    /**
     * Get contato
     *
     * @return string
     */
    public function getContato()
    {
        return $this->contato;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     *
     * @return B2cClicli
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return string
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set newsletter
     *
     * @param boolean $newsletter
     *
     * @return B2cClicli
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return boolean
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * Set endereco
     *
     * @param string $endereco
     *
     * @return B2cClicli
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;

        return $this;
    }

    /**
     * Get endereco
     *
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return B2cClicli
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set complemento
     *
     * @param string $complemento
     *
     * @return B2cClicli
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;

        return $this;
    }

    /**
     * Get complemento
     *
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * Set bairro
     *
     * @param string $bairro
     *
     * @return B2cClicli
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get bairro
     *
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return B2cClicli
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set provincia
     *
     * @param string $provincia
     *
     * @return B2cClicli
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return string
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     *
     * @return B2cClicli
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set pais
     *
     * @param integer $pais
     *
     * @return B2cClicli
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return integer
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set cep
     *
     * @param string $cep
     *
     * @return B2cClicli
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set telefoneA
     *
     * @param string $telefoneA
     *
     * @return B2cClicli
     */
    public function setTelefoneA($telefoneA)
    {
        $this->telefoneA = $telefoneA;

        return $this;
    }

    /**
     * Get telefoneA
     *
     * @return string
     */
    public function getTelefoneA()
    {
        return $this->telefoneA;
    }

    /**
     * Set telefoneB
     *
     * @param string $telefoneB
     *
     * @return B2cClicli
     */
    public function setTelefoneB($telefoneB)
    {
        $this->telefoneB = $telefoneB;

        return $this;
    }

    /**
     * Get telefoneB
     *
     * @return string
     */
    public function getTelefoneB()
    {
        return $this->telefoneB;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     *
     * @return B2cClicli
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set senhaB64
     *
     * @param string $senhaB64
     *
     * @return B2cClicli
     */
    public function setSenhaB64($senhaB64)
    {
        $this->senhaB64 = $senhaB64;

        return $this;
    }

    /**
     * Get senhaB64
     *
     * @return string
     */
    public function getSenhaB64()
    {
        return $this->senhaB64;
    }

    /**
     * Set cadastro
     *
     * @param \DateTime $cadastro
     *
     * @return B2cClicli
     */
    public function setCadastro($cadastro)
    {
        $this->cadastro = $cadastro;

        return $this;
    }

    /**
     * Get cadastro
     *
     * @return \DateTime
     */
    public function getCadastro()
    {
        return $this->cadastro;
    }

    /**
     * Set primeiroAcesso
     *
     * @param \DateTime $primeiroAcesso
     *
     * @return B2cClicli
     */
    public function setPrimeiroAcesso($primeiroAcesso)
    {
        $this->primeiroAcesso = $primeiroAcesso;

        return $this;
    }

    /**
     * Get primeiroAcesso
     *
     * @return \DateTime
     */
    public function getPrimeiroAcesso()
    {
        return $this->primeiroAcesso;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return B2cClicli
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set descStatus
     *
     * @param string $descStatus
     *
     * @return B2cClicli
     */
    public function setDescStatus($descStatus)
    {
        $this->descStatus = $descStatus;

        return $this;
    }

    /**
     * Get descStatus
     *
     * @return string
     */
    public function getDescStatus()
    {
        return $this->descStatus;
    }

    /**
     * Set dtStatus
     *
     * @param \DateTime $dtStatus
     *
     * @return B2cClicli
     */
    public function setDtStatus($dtStatus)
    {
        $this->dtStatus = $dtStatus;

        return $this;
    }

    /**
     * Get dtStatus
     *
     * @return \DateTime
     */
    public function getDtStatus()
    {
        return $this->dtStatus;
    }

    /**
     * Set dataAfiafi
     *
     * @param \DateTime $dataAfiafi
     *
     * @return B2cClicli
     */
    public function setDataAfiafi($dataAfiafi)
    {
        $this->dataAfiafi = $dataAfiafi;

        return $this;
    }

    /**
     * Get dataAfiafi
     *
     * @return \DateTime
     */
    public function getDataAfiafi()
    {
        return $this->dataAfiafi;
    }

    /**
     * Set afiliado
     *
     * @param integer $afiliado
     *
     * @return B2cClicli
     */
    public function setAfiliado($afiliado)
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    /**
     * Get afiliado
     *
     * @return integer
     */
    public function getAfiliado()
    {
        return $this->afiliado;
    }

    /**
     * Set primeiraOrigem
     *
     * @param string $primeiraOrigem
     *
     * @return B2cClicli
     */
    public function setPrimeiraOrigem($primeiraOrigem)
    {
        $this->primeiraOrigem = $primeiraOrigem;

        return $this;
    }

    /**
     * Get primeiraOrigem
     *
     * @return string
     */
    public function getPrimeiraOrigem()
    {
        return $this->primeiraOrigem;
    }

    /**
     * Set cadastroOrigem
     *
     * @param string $cadastroOrigem
     *
     * @return B2cClicli
     */
    public function setCadastroOrigem($cadastroOrigem)
    {
        $this->cadastroOrigem = $cadastroOrigem;

        return $this;
    }

    /**
     * Get cadastroOrigem
     *
     * @return string
     */
    public function getCadastroOrigem()
    {
        return $this->cadastroOrigem;
    }

    /**
     * Set clienteOrigem
     *
     * @param string $clienteOrigem
     *
     * @return B2cClicli
     */
    public function setClienteOrigem($clienteOrigem)
    {
        $this->clienteOrigem = $clienteOrigem;

        return $this;
    }

    /**
     * Get clienteOrigem
     *
     * @return string
     */
    public function getClienteOrigem()
    {
        return $this->clienteOrigem;
    }

    /**
     * Set dataClienteOrigem
     *
     * @param \DateTime $dataClienteOrigem
     *
     * @return B2cClicli
     */
    public function setDataClienteOrigem($dataClienteOrigem)
    {
        $this->dataClienteOrigem = $dataClienteOrigem;

        return $this;
    }

    /**
     * Get dataClienteOrigem
     *
     * @return \DateTime
     */
    public function getDataClienteOrigem()
    {
        return $this->dataClienteOrigem;
    }

    /**
     * Set primeiraOrigemAfiliado
     *
     * @param string $primeiraOrigemAfiliado
     *
     * @return B2cClicli
     */
    public function setPrimeiraOrigemAfiliado($primeiraOrigemAfiliado)
    {
        $this->primeiraOrigemAfiliado = $primeiraOrigemAfiliado;

        return $this;
    }

    /**
     * Get primeiraOrigemAfiliado
     *
     * @return string
     */
    public function getPrimeiraOrigemAfiliado()
    {
        return $this->primeiraOrigemAfiliado;
    }

    /**
     * Set processadows
     *
     * @param boolean $processadows
     *
     * @return B2cClicli
     */
    public function setProcessadows($processadows)
    {
        $this->processadows = $processadows;

        return $this;
    }

    /**
     * Get processadows
     *
     * @return boolean
     */
    public function getProcessadows()
    {
        return $this->processadows;
    }

    /**
     * Set dtaAtualizacao
     *
     * @param \DateTime $dtaAtualizacao
     *
     * @return B2cClicli
     */
    public function setDtaAtualizacao($dtaAtualizacao)
    {
        $this->dtaAtualizacao = $dtaAtualizacao;

        return $this;
    }

    /**
     * Get dtaAtualizacao
     *
     * @return \DateTime
     */
    public function getDtaAtualizacao()
    {
        return $this->dtaAtualizacao;
    }

    /**
     * Set sfAccount
     *
     * @param string $sfAccount
     *
     * @return B2cClicli
     */
    public function setSfAccount($sfAccount)
    {
        $this->sfAccount = $sfAccount;

        return $this;
    }

    /**
     * Get sfAccount
     *
     * @return string
     */
    public function getSfAccount()
    {
        return $this->sfAccount;
    }

    /**
     * Set sfDataSync
     *
     * @param \DateTime $sfDataSync
     *
     * @return B2cClicli
     */
    public function setSfDataSync($sfDataSync)
    {
        $this->sfDataSync = $sfDataSync;

        return $this;
    }

    /**
     * Get sfDataSync
     *
     * @return \DateTime
     */
    public function getSfDataSync()
    {
        return $this->sfDataSync;
    }

    /**
     * @return string
     */
    public function getTipoCadastro()
    {
        return $this->tipoCadastro;
    }

    /**
     * @param string $tipoCadastro
     */
    public function setTipoCadastro($tipoCadastro)
    {
        $this->tipoCadastro = $tipoCadastro;
    }
}
