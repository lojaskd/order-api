<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cLogCartao
 *
 * @ORM\Table(name="b2c_log_cartao", indexes={@ORM\Index(name="chave_id_pedido", columns={"id_pedido"}), @ORM\Index(name="chave_id_pagamento_cartao", columns={"id_pagamento_cartao"}), @ORM\Index(name="dta_inc_log_cartao", columns={"dta_inc_log_cartao"}), @ORM\Index(name="tid_log_cartao", columns={"tid_log_cartao"}), @ORM\Index(name="codigo_log_cartao", columns={"codigo_log_cartao"})})
 * @ORM\Entity
 */
class B2cLogCartao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_log_cartao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLogCartao;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedido", type="integer", nullable=false)
     */
    private $idPedido;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagamento_cartao", type="integer", nullable=false)
     */
    private $idPagamentoCartao;

    /**
     * @var integer
     *
     * @ORM\Column(name="codigo_log_cartao", type="integer", nullable=false)
     */
    private $codigoLogCartao;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_log_cartao", type="string", length=255, nullable=false)
     */
    private $descLogCartao;

    /**
     * @var string
     *
     * @ORM\Column(name="tid_log_cartao", type="string", length=30, nullable=true)
     */
    private $tidLogCartao;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_operadora_transmissao", type="string", length=20, nullable=true)
     */
    private $nomeOperadoraTransmissao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_inc_log_cartao", type="datetime", nullable=false)
     */
    private $dtaIncLogCartao = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="gateway_transmissao", type="string", length=30, nullable=true)
     */
    private $gatewayTransmissao;

    /**
     * @var string
     *
     * @ORM\Column(name="titular_cartao", type="string", length=255, nullable=true)
     */
    private $titularCartao;

    /**
     * @var string
     *
     * @ORM\Column(name="validade_cartao", type="string", length=8, nullable=true)
     */
    private $validadeCartao;

    /**
     * @var string
     *
     * @ORM\Column(name="bin_cartao", type="string", length=6, nullable=true)
     */
    private $binCartao;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_final_cartao", type="string", length=4, nullable=true)
     */
    private $numeroFinalCartao;

    /**
     * @param $idLogCartao
     * @return $this
     */
    public function setIdLogCartao($idLogCartao)
    {
        $this->idLogCartao = $idLogCartao;
        return $this;
    }

    /**
     * Get idLogCartao
     *
     * @return integer
     */
    public function getIdLogCartao()
    {
        return $this->idLogCartao;
    }

    /**
     * Set idPedido
     *
     * @param integer $idPedido
     *
     * @return B2cLogCartao
     */
    public function setIdPedido($idPedido)
    {
        $this->idPedido = $idPedido;

        return $this;
    }

    /**
     * Get idPedido
     *
     * @return integer
     */
    public function getIdPedido()
    {
        return $this->idPedido;
    }

    /**
     * Set idPagamentoCartao
     *
     * @param integer $idPagamentoCartao
     *
     * @return B2cLogCartao
     */
    public function setIdPagamentoCartao($idPagamentoCartao)
    {
        $this->idPagamentoCartao = $idPagamentoCartao;

        return $this;
    }

    /**
     * Get idPagamentoCartao
     *
     * @return integer
     */
    public function getIdPagamentoCartao()
    {
        return $this->idPagamentoCartao;
    }

    /**
     * Set codigoLogCartao
     *
     * @param integer $codigoLogCartao
     *
     * @return B2cLogCartao
     */
    public function setCodigoLogCartao($codigoLogCartao)
    {
        $this->codigoLogCartao = $codigoLogCartao;

        return $this;
    }

    /**
     * Get codigoLogCartao
     *
     * @return integer
     */
    public function getCodigoLogCartao()
    {
        return $this->codigoLogCartao;
    }

    /**
     * Set descLogCartao
     *
     * @param string $descLogCartao
     *
     * @return B2cLogCartao
     */
    public function setDescLogCartao($descLogCartao)
    {
        $this->descLogCartao = $descLogCartao;

        return $this;
    }

    /**
     * Get descLogCartao
     *
     * @return string
     */
    public function getDescLogCartao()
    {
        return $this->descLogCartao;
    }

    /**
     * Set tidLogCartao
     *
     * @param string $tidLogCartao
     *
     * @return B2cLogCartao
     */
    public function setTidLogCartao($tidLogCartao)
    {
        $this->tidLogCartao = $tidLogCartao;

        return $this;
    }

    /**
     * Get tidLogCartao
     *
     * @return string
     */
    public function getTidLogCartao()
    {
        return $this->tidLogCartao;
    }

    /**
     * Set nomeOperadoraTransmissao
     *
     * @param string $nomeOperadoraTransmissao
     *
     * @return B2cLogCartao
     */
    public function setNomeOperadoraTransmissao($nomeOperadoraTransmissao)
    {
        $this->nomeOperadoraTransmissao = $nomeOperadoraTransmissao;

        return $this;
    }

    /**
     * Get nomeOperadoraTransmissao
     *
     * @return string
     */
    public function getNomeOperadoraTransmissao()
    {
        return $this->nomeOperadoraTransmissao;
    }

    /**
     * Set dtaIncLogCartao
     *
     * @param \DateTime $dtaIncLogCartao
     *
     * @return B2cLogCartao
     */
    public function setDtaIncLogCartao($dtaIncLogCartao)
    {
        $this->dtaIncLogCartao = $dtaIncLogCartao;

        return $this;
    }

    /**
     * Get dtaIncLogCartao
     *
     * @return \DateTime
     */
    public function getDtaIncLogCartao()
    {
        return $this->dtaIncLogCartao;
    }

    /**
     * Set gatewayTransmissao
     *
     * @param string $gatewayTransmissao
     *
     * @return B2cLogCartao
     */
    public function setGatewayTransmissao($gatewayTransmissao)
    {
        $this->gatewayTransmissao = $gatewayTransmissao;

        return $this;
    }

    /**
     * Get gatewayTransmissao
     *
     * @return string
     */
    public function getGatewayTransmissao()
    {
        return $this->gatewayTransmissao;
    }

    /**
     * Set titularCartao
     *
     * @param string $titularCartao
     *
     * @return B2cLogCartao
     */
    public function setTitularCartao($titularCartao)
    {
        $this->titularCartao = $titularCartao;

        return $this;
    }

    /**
     * Get titularCartao
     *
     * @return string
     */
    public function getTitularCartao()
    {
        return $this->titularCartao;
    }

    /**
     * Set validadeCartao
     *
     * @param string $validadeCartao
     *
     * @return B2cLogCartao
     */
    public function setValidadeCartao($validadeCartao)
    {
        $this->validadeCartao = $validadeCartao;

        return $this;
    }

    /**
     * Get validadeCartao
     *
     * @return string
     */
    public function getValidadeCartao()
    {
        return $this->validadeCartao;
    }

    /**
     * Set binCartao
     *
     * @param string $binCartao
     *
     * @return B2cLogCartao
     */
    public function setBinCartao($binCartao)
    {
        $this->binCartao = $binCartao;

        return $this;
    }

    /**
     * Get binCartao
     *
     * @return string
     */
    public function getBinCartao()
    {
        return $this->binCartao;
    }

    /**
     * Set numeroFinalCartao
     *
     * @param string $numeroFinalCartao
     *
     * @return B2cLogCartao
     */
    public function setNumeroFinalCartao($numeroFinalCartao)
    {
        $this->numeroFinalCartao = $numeroFinalCartao;

        return $this;
    }

    /**
     * Get numeroFinalCartao
     *
     * @return string
     */
    public function getNumeroFinalCartao()
    {
        return $this->numeroFinalCartao;
    }
}
