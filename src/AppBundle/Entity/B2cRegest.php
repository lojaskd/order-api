<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cRegest
 *
 * @ORM\Table(name="b2c_regest", indexes={@ORM\Index(name="name", columns={"nome"}), @ORM\Index(name="code", columns={"uf"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cRegestRepository")
 */
class B2cRegest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=72, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="uf", type="string", length=2, precision=0, scale=0, nullable=false, unique=false)
     */
    private $uf;

    /**
     * @var string
     *
     * @ORM\Column(name="cep_inicio", type="string", length=5, precision=0, scale=0, nullable=false, unique=false)
     */
    private $cepInicio;

    /**
     * @var string
     *
     * @ORM\Column(name="cep_fim", type="string", length=5, precision=0, scale=0, nullable=false, unique=false)
     */
    private $cepFim;

    /**
     * @var string
     *
     * @ORM\Column(name="cod_mun_capital", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $codMunCapital;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cRegest
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set uf
     *
     * @param string $uf
     *
     * @return B2cRegest
     */
    public function setUf($uf)
    {
        $this->uf = $uf;

        return $this;
    }

    /**
     * Get uf
     *
     * @return string
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * Set cepInicio
     *
     * @param string $cepInicio
     *
     * @return B2cRegest
     */
    public function setCepInicio($cepInicio)
    {
        $this->cepInicio = $cepInicio;

        return $this;
    }

    /**
     * Get cepInicio
     *
     * @return string
     */
    public function getCepInicio()
    {
        return $this->cepInicio;
    }

    /**
     * Set cepFim
     *
     * @param string $cepFim
     *
     * @return B2cRegest
     */
    public function setCepFim($cepFim)
    {
        $this->cepFim = $cepFim;

        return $this;
    }

    /**
     * Get cepFim
     *
     * @return string
     */
    public function getCepFim()
    {
        return $this->cepFim;
    }

    /**
     * Set codMunCapital
     *
     * @param string $codMunCapital
     *
     * @return B2cRegest
     */
    public function setCodMunCapital($codMunCapital)
    {
        $this->codMunCapital = $codMunCapital;

        return $this;
    }

    /**
     * Get codMunCapital
     *
     * @return string
     */
    public function getCodMunCapital()
    {
        return $this->codMunCapital;
    }
}
