<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPedped
 *
 * @ORM\Table(name="b2c_pedped", indexes={@ORM\Index(name="email", columns={"cob_email"}), @ORM\Index(name="id_clicli",
 *     columns={"id_clicli"}), @ORM\Index(name="ip", columns={"ip"}), @ORM\Index(name="ped_origem",
 *     columns={"ped_origem"}), @ORM\Index(name="ped_pend_canc", columns={"ped_pend_canc"}),
 *     @ORM\Index(name="ped_situacao", columns={"ped_situacao"}), @ORM\Index(name="ped_staint",
 *     columns={"ped_staint"}), @ORM\Index(name="ped_dta_compra", columns={"ped_dta_compra", "ped_pend_canc",
 *     "ped_situacao"}), @ORM\Index(name="id_pagfor", columns={"id_pagfor_1"}), @ORM\Index(name="id_pagpag",
 *     columns={"id_pagpag_1"}), @ORM\Index(name="exportado", columns={"exportado"}),
 *     @ORM\Index(name="ped_dta_atualizacao", columns={"ped_dta_atualizacao"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPedpedRepository")
 */
class B2cPedped
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPedped;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ped_dta_compra", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedDtaCompra;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ped_dta_atualizacao", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedDtaAtualizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="atendente", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $atendente;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_situacao", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedSituacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_staint", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $pedStaint;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_pend_canc", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedPendCanc;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_clicli", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idClicli;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobNome;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_sobrenome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobSobrenome;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_email", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_cpf_cgc", type="string", length=30, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobCpfCgc;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_rg_ie", type="string", length=30, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobRgIe;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_tipo_pessoa", type="string", length=1, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobTipoPessoa;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_sexo", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $cobSexo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cob_dta_nasc", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $cobDtaNasc;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_contato", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobContato;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_endereco", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobEndereco;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_end_numero", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobEndNumero;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_end_comp", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobEndComp;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_end_bairro", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobEndBairro;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_end_cidade", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobEndCidade;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_end_uf", type="string", length=2, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobEndUf;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_end_cep", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobEndCep;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_end_provincia", type="string", length=255, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $cobEndProvincia;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_end_pais", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobEndPais;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_tel_a", type="string", length=30, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobTelA;

    /**
     * @var string
     *
     * @ORM\Column(name="cob_tel_b", type="string", length=30, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cobTelB;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entNome;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_sobrenome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entSobrenome;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_identificacao", type="string", length=255, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $entIdentificacao;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_endereco", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entEndereco;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_end_numero", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entEndNumero;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_end_comp", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entEndComp;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_end_bairro", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entEndBairro;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_end_cidade", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entEndCidade;

    /**
     * @var integer
     *
     * @ORM\Column(name="ent_cod_ibge", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $entCodIbge;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_end_uf", type="string", length=2, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entEndUf;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_end_cep", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entEndCep;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_end_ref", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entEndRef;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_end_provincia", type="string", length=255, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $entEndProvincia;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_end_pais", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entEndPais;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_tel_a", type="string", length=30, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entTelA;

    /**
     * @var string
     *
     * @ORM\Column(name="ent_tel_b", type="string", length=30, precision=0, scale=0, nullable=true, unique=false)
     */
    private $entTelB;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_observacao", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedObservacao;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_tipo_frete", type="string", length=20, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedTipoFrete;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_transportadora", type="string", length=255, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $pedTransportadora;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_prazo_entrega", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedPrazoEntrega;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_prazo_transporte", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $pedPrazoTransporte;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ped_data_entrega", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $pedDataEntrega;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_entrega_regiao", type="string", length=255, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $pedEntregaRegiao;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_cod_envio", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $pedCodEnvio;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_montagem", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $pedMontagem;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_montagem", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $pedValorMontagem;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_subtotal", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pedValorSubtotal;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_embalagem", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pedValorEmbalagem;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_custofrete", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $pedValorCustofrete;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_frete", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pedValorFrete;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_desconto", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pedValorDesconto;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_desconto_loja", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pedValorDescontoLoja;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_valecompra", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pedValorValecompra;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_total", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pedValorTotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagpag_1", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idPagpag1;

    /**
     * @var string
     *
     * @ORM\Column(name="id_pagfor_1", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $idPagfor1;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_total_1", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $pedValorTotal1;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_real_1", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $pedValorReal1;

    /**
     * @var integer
     *
     * @ORM\Column(name="pag_num_parcelas_1", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pagNumParcelas1;

    /**
     * @var float
     *
     * @ORM\Column(name="pag_val_entrada_1", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pagValEntrada1;

    /**
     * @var float
     *
     * @ORM\Column(name="pag_val_parcela_1", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pagValParcela1;

    /**
     * @var float
     *
     * @ORM\Column(name="pag_indexador_1", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $pagIndexador1;

    /**
     * @var integer
     *
     * @ORM\Column(name="boleto_parcelado_1", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $boletoParcelado1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="boleto_vencimento_1", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $boletoVencimento1;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_tipo_pagamento_1", type="string", length=255, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $pedTipoPagamento1;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_forma_pagamento_1", type="string", length=255, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $pedFormaPagamento1;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_forma_pagdesc_1", type="text", length=65535, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $pedFormaPagdesc1;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_desconto_loja_1", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $pedValorDescontoLoja1;

    /**
     * @var integer
     *
     * @ORM\Column(name="contador_alteracao_desconto_loja_1", type="integer", precision=0, scale=0, nullable=false,
     *     unique=false)
     */
    private $contadorAlteracaoDescontoLoja1;

    /**
     * @var integer
     *
     * @ORM\Column(name="pagamento_confirmado_1", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pagamentoConfirmado1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagpag_2", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idPagpag2;

    /**
     * @var string
     *
     * @ORM\Column(name="id_pagfor_2", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $idPagfor2;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_total_2", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $pedValorTotal2;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_real_2", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $pedValorReal2;

    /**
     * @var integer
     *
     * @ORM\Column(name="pag_num_parcelas_2", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pagNumParcelas2;

    /**
     * @var float
     *
     * @ORM\Column(name="pag_val_entrada_2", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pagValEntrada2;

    /**
     * @var float
     *
     * @ORM\Column(name="pag_val_parcela_2", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pagValParcela2;

    /**
     * @var float
     *
     * @ORM\Column(name="pag_indexador_2", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $pagIndexador2;

    /**
     * @var integer
     *
     * @ORM\Column(name="boleto_parcelado_2", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $boletoParcelado2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="boleto_vencimento_2", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $boletoVencimento2;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_tipo_pagamento_2", type="string", length=255, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $pedTipoPagamento2;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_forma_pagamento_2", type="string", length=255, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $pedFormaPagamento2;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_forma_pagdesc_2", type="text", length=65535, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $pedFormaPagdesc2;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_desconto_loja_2", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $pedValorDescontoLoja2;

    /**
     * @var integer
     *
     * @ORM\Column(name="contador_alteracao_desconto_loja_2", type="integer", precision=0, scale=0, nullable=false,
     *     unique=false)
     */
    private $contadorAlteracaoDescontoLoja2;

    /**
     * @var integer
     *
     * @ORM\Column(name="pagamento_confirmado_2", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pagamentoConfirmado2;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_origem", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedOrigem;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_primeira_origem", type="string", length=255, precision=0, scale=0, nullable=false,
     *     unique=false)
     */
    private $pedPrimeiraOrigem;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_primeira_origem_data", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $pedPrimeiraOrigemData;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_referer", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedReferer;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_cupom", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedCupom;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_promocao", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedPromocao;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="cs_usuario", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $csUsuario;

    /**
     * @var integer
     *
     * @ORM\Column(name="cs_status", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $csStatus;

    /**
     * @var float
     *
     * @ORM\Column(name="cs_score", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $csScore;

    /**
     * @var string
     *
     * @ORM\Column(name="cs_diagnostico", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $csDiagnostico;

    /**
     * @var string
     *
     * @ORM\Column(name="cs_solicitacao", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $csSolicitacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="SeqPed", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $seqped;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="exportado", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $exportado;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_transportadora_integrado", type="integer", precision=0, scale=0, nullable=false,
     *     unique=false)
     */
    private $pedTransportadoraIntegrado;

    /**
     * @var float
     *
     * @ORM\Column(name="ped_valor_comissao", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pedValorComissao;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_data_afiliado", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedDataAfiliado;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_afiliado", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedAfiliado;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem_geracao_boleto", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ordemGeracaoBoleto;

    /**
     * @var integer
     *
     * @ORM\Column(name="analytics_negativado", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $analyticsNegativado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ped_entrega_data", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedEntregaData;

    /**
     * @var string
     *
     * @ORM\Column(name="ped_faturado", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedFaturado;

    /**
     * @var string
     *
     * @ORM\Column(name="pag_tel_1", type="string", length=30, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pagTel1;

    /**
     * @var string
     *
     * @ORM\Column(name="pag_tel_2", type="string", length=30, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pagTel2;

    /**
     * @var string
     *
     * @ORM\Column(name="pag_titular_1", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pagTitular1;

    /**
     * @var string
     *
     * @ORM\Column(name="pag_titular_2", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pagTitular2;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_contabil_1", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $contaContabil1;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_contabil_2", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $contaContabil2;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_contabil_nome_1", type="string", length=30, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $contaContabilNome1;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_contabil_nome_2", type="string", length=30, precision=0, scale=0, nullable=true,
     *     unique=false)
     */
    private $contaContabilNome2;

    /**
     * @var string
     *
     * @ORM\Column(name="sf_opportunity", type="string", length=18, precision=0, scale=0, nullable=true, unique=false)
     */
    private $sfOpportunity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sf_data_sync", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $sfDataSync;

    /**
     * @var integer
     *
     * @ORM\Column(name="sf_custo_financeiro_sync", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $sfCustoFinanceiroSync;

    /**
     * @var integer
     *
     * @ORM\Column(name="assistencia", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $assistencia;

    /**
     * @var integer
     *
     * @ORM\Column(name="recompra", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $recompra;

    /**
     * @var integer
     *
     * @ORM\Column(name="troca", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $troca;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ped_nova_data_entrega", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedNovaDataEntrega;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_marketplace", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idMarketplace;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_pedido", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tipoPedido;

    /**
     * @var \Doctrine\Common\Collections\Collection $products
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\B2cPedprd", mappedBy="idPedped", cascade={"all"})
     */
    private $products;

    /**
     * @var \Doctrine\Common\Collections\Collection $status
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\B2cPedstatus", mappedBy="idPedped", cascade={"all"})
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $device;

    /**
     * @var string
     *
     * @ORM\Column(name="regra_frete", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $ruleFreight;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->status = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idPedped
     *
     * @return integer
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * Set idPedped
     *
     * @param integer $idPedped
     *
     * @return B2cPedped
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;

        return $this;
    }

    /**
     * Set pedDtaCompra
     *
     * @param \DateTime $pedDtaCompra
     *
     * @return B2cPedped
     */
    public function setPedDtaCompra($pedDtaCompra)
    {
        $this->pedDtaCompra = $pedDtaCompra;

        return $this;
    }

    /**
     * Get pedDtaCompra
     *
     * @return \DateTime
     */
    public function getPedDtaCompra()
    {
        return $this->pedDtaCompra;
    }

    /**
     * Set pedDtaAtualizacao
     *
     * @param \DateTime $pedDtaAtualizacao
     *
     * @return B2cPedped
     */
    public function setPedDtaAtualizacao($pedDtaAtualizacao)
    {
        $this->pedDtaAtualizacao = $pedDtaAtualizacao;

        return $this;
    }

    /**
     * Get pedDtaAtualizacao
     *
     * @return \DateTime
     */
    public function getPedDtaAtualizacao()
    {
        return $this->pedDtaAtualizacao;
    }

    /**
     * Set atendente
     *
     * @param string $atendente
     *
     * @return B2cPedped
     */
    public function setAtendente($atendente)
    {
        $this->atendente = $atendente;

        return $this;
    }

    /**
     * Get atendente
     *
     * @return string
     */
    public function getAtendente()
    {
        return $this->atendente;
    }

    /**
     * Set pedSituacao
     *
     * @param integer $pedSituacao
     *
     * @return B2cPedped
     */
    public function setPedSituacao($pedSituacao)
    {
        $this->pedSituacao = $pedSituacao;

        return $this;
    }

    /**
     * Get pedSituacao
     *
     * @return integer
     */
    public function getPedSituacao()
    {
        return $this->pedSituacao;
    }

    /**
     * Set pedStaint
     *
     * @param integer $pedStaint
     *
     * @return B2cPedped
     */
    public function setPedStaint($pedStaint)
    {
        $this->pedStaint = $pedStaint;

        return $this;
    }

    /**
     * Get pedStaint
     *
     * @return integer
     */
    public function getPedStaint()
    {
        return $this->pedStaint;
    }

    /**
     * Set pedPendCanc
     *
     * @param integer $pedPendCanc
     *
     * @return B2cPedped
     */
    public function setPedPendCanc($pedPendCanc)
    {
        $this->pedPendCanc = $pedPendCanc;

        return $this;
    }

    /**
     * Get pedPendCanc
     *
     * @return integer
     */
    public function getPedPendCanc()
    {
        return $this->pedPendCanc;
    }

    /**
     * Set idClicli
     *
     * @param integer $idClicli
     *
     * @return B2cPedped
     */
    public function setIdClicli($idClicli)
    {
        $this->idClicli = $idClicli;

        return $this;
    }

    /**
     * Get idClicli
     *
     * @return integer
     */
    public function getIdClicli()
    {
        return $this->idClicli;
    }

    /**
     * Set cobNome
     *
     * @param string $cobNome
     *
     * @return B2cPedped
     */
    public function setCobNome($cobNome)
    {
        $this->cobNome = $cobNome;

        return $this;
    }

    /**
     * Get cobNome
     *
     * @return string
     */
    public function getCobNome()
    {
        return $this->cobNome;
    }

    /**
     * Set cobSobrenome
     *
     * @param string $cobSobrenome
     *
     * @return B2cPedped
     */
    public function setCobSobrenome($cobSobrenome)
    {
        $this->cobSobrenome = $cobSobrenome;

        return $this;
    }

    /**
     * Get cobSobrenome
     *
     * @return string
     */
    public function getCobSobrenome()
    {
        return $this->cobSobrenome;
    }

    /**
     * Set cobEmail
     *
     * @param string $cobEmail
     *
     * @return B2cPedped
     */
    public function setCobEmail($cobEmail)
    {
        $this->cobEmail = $cobEmail;

        return $this;
    }

    /**
     * Get cobEmail
     *
     * @return string
     */
    public function getCobEmail()
    {
        return $this->cobEmail;
    }

    /**
     * Set cobCpfCgc
     *
     * @param string $cobCpfCgc
     *
     * @return B2cPedped
     */
    public function setCobCpfCgc($cobCpfCgc)
    {
        $this->cobCpfCgc = $cobCpfCgc;

        return $this;
    }

    /**
     * Get cobCpfCgc
     *
     * @return string
     */
    public function getCobCpfCgc()
    {
        return $this->cobCpfCgc;
    }

    /**
     * Set cobRgIe
     *
     * @param string $cobRgIe
     *
     * @return B2cPedped
     */
    public function setCobRgIe($cobRgIe)
    {
        $this->cobRgIe = $cobRgIe;

        return $this;
    }

    /**
     * Get cobRgIe
     *
     * @return string
     */
    public function getCobRgIe()
    {
        return $this->cobRgIe;
    }

    /**
     * Set cobTipoPessoa
     *
     * @param string $cobTipoPessoa
     *
     * @return B2cPedped
     */
    public function setCobTipoPessoa($cobTipoPessoa)
    {
        $this->cobTipoPessoa = $cobTipoPessoa;

        return $this;
    }

    /**
     * Get cobTipoPessoa
     *
     * @return string
     */
    public function getCobTipoPessoa()
    {
        return $this->cobTipoPessoa;
    }

    /**
     * Set cobSexo
     *
     * @param string $cobSexo
     *
     * @return B2cPedped
     */
    public function setCobSexo($cobSexo)
    {
        $this->cobSexo = $cobSexo;

        return $this;
    }

    /**
     * Get cobSexo
     *
     * @return string
     */
    public function getCobSexo()
    {
        return $this->cobSexo;
    }

    /**
     * Set cobDtaNasc
     *
     * @param \DateTime $cobDtaNasc
     *
     * @return B2cPedped
     */
    public function setCobDtaNasc($cobDtaNasc)
    {
        $this->cobDtaNasc = $cobDtaNasc;

        return $this;
    }

    /**
     * Get cobDtaNasc
     *
     * @return \DateTime
     */
    public function getCobDtaNasc()
    {
        return $this->cobDtaNasc;
    }

    /**
     * Set cobContato
     *
     * @param string $cobContato
     *
     * @return B2cPedped
     */
    public function setCobContato($cobContato)
    {
        $this->cobContato = $cobContato;

        return $this;
    }

    /**
     * Get cobContato
     *
     * @return string
     */
    public function getCobContato()
    {
        return $this->cobContato;
    }

    /**
     * Set cobEndereco
     *
     * @param string $cobEndereco
     *
     * @return B2cPedped
     */
    public function setCobEndereco($cobEndereco)
    {
        $this->cobEndereco = $cobEndereco;

        return $this;
    }

    /**
     * Get cobEndereco
     *
     * @return string
     */
    public function getCobEndereco()
    {
        return $this->cobEndereco;
    }

    /**
     * Set cobEndNumero
     *
     * @param string $cobEndNumero
     *
     * @return B2cPedped
     */
    public function setCobEndNumero($cobEndNumero)
    {
        $this->cobEndNumero = $cobEndNumero;

        return $this;
    }

    /**
     * Get cobEndNumero
     *
     * @return string
     */
    public function getCobEndNumero()
    {
        return $this->cobEndNumero;
    }

    /**
     * Set cobEndComp
     *
     * @param string $cobEndComp
     *
     * @return B2cPedped
     */
    public function setCobEndComp($cobEndComp)
    {
        $this->cobEndComp = $cobEndComp;

        return $this;
    }

    /**
     * Get cobEndComp
     *
     * @return string
     */
    public function getCobEndComp()
    {
        return $this->cobEndComp;
    }

    /**
     * Set cobEndBairro
     *
     * @param string $cobEndBairro
     *
     * @return B2cPedped
     */
    public function setCobEndBairro($cobEndBairro)
    {
        $this->cobEndBairro = $cobEndBairro;

        return $this;
    }

    /**
     * Get cobEndBairro
     *
     * @return string
     */
    public function getCobEndBairro()
    {
        return $this->cobEndBairro;
    }

    /**
     * Set cobEndCidade
     *
     * @param string $cobEndCidade
     *
     * @return B2cPedped
     */
    public function setCobEndCidade($cobEndCidade)
    {
        $this->cobEndCidade = $cobEndCidade;

        return $this;
    }

    /**
     * Get cobEndCidade
     *
     * @return string
     */
    public function getCobEndCidade()
    {
        return $this->cobEndCidade;
    }

    /**
     * Set cobEndUf
     *
     * @param string $cobEndUf
     *
     * @return B2cPedped
     */
    public function setCobEndUf($cobEndUf)
    {
        $this->cobEndUf = $cobEndUf;

        return $this;
    }

    /**
     * Get cobEndUf
     *
     * @return string
     */
    public function getCobEndUf()
    {
        return $this->cobEndUf;
    }

    /**
     * Set cobEndCep
     *
     * @param string $cobEndCep
     *
     * @return B2cPedped
     */
    public function setCobEndCep($cobEndCep)
    {
        $this->cobEndCep = $cobEndCep;

        return $this;
    }

    /**
     * Get cobEndCep
     *
     * @return string
     */
    public function getCobEndCep()
    {
        return $this->cobEndCep;
    }

    /**
     * Set cobEndProvincia
     *
     * @param string $cobEndProvincia
     *
     * @return B2cPedped
     */
    public function setCobEndProvincia($cobEndProvincia)
    {
        $this->cobEndProvincia = $cobEndProvincia;

        return $this;
    }

    /**
     * Get cobEndProvincia
     *
     * @return string
     */
    public function getCobEndProvincia()
    {
        return $this->cobEndProvincia;
    }

    /**
     * Set cobEndPais
     *
     * @param string $cobEndPais
     *
     * @return B2cPedped
     */
    public function setCobEndPais($cobEndPais)
    {
        $this->cobEndPais = $cobEndPais;

        return $this;
    }

    /**
     * Get cobEndPais
     *
     * @return string
     */
    public function getCobEndPais()
    {
        return $this->cobEndPais;
    }

    /**
     * Set cobTelA
     *
     * @param string $cobTelA
     *
     * @return B2cPedped
     */
    public function setCobTelA($cobTelA)
    {
        $this->cobTelA = $cobTelA;

        return $this;
    }

    /**
     * Get cobTelA
     *
     * @return string
     */
    public function getCobTelA()
    {
        return $this->cobTelA;
    }

    /**
     * Set cobTelB
     *
     * @param string $cobTelB
     *
     * @return B2cPedped
     */
    public function setCobTelB($cobTelB)
    {
        $this->cobTelB = $cobTelB;

        return $this;
    }

    /**
     * Get cobTelB
     *
     * @return string
     */
    public function getCobTelB()
    {
        return $this->cobTelB;
    }

    /**
     * Set entNome
     *
     * @param string $entNome
     *
     * @return B2cPedped
     */
    public function setEntNome($entNome)
    {
        $this->entNome = $entNome;

        return $this;
    }

    /**
     * Get entNome
     *
     * @return string
     */
    public function getEntNome()
    {
        return $this->entNome;
    }

    /**
     * Set entSobrenome
     *
     * @param string $entSobrenome
     *
     * @return B2cPedped
     */
    public function setEntSobrenome($entSobrenome)
    {
        $this->entSobrenome = $entSobrenome;

        return $this;
    }

    /**
     * Get entSobrenome
     *
     * @return string
     */
    public function getEntSobrenome()
    {
        return $this->entSobrenome;
    }

    /**
     * Set entIdentificacao
     *
     * @param string $entIdentificacao
     *
     * @return B2cPedped
     */
    public function setEntIdentificacao($entIdentificacao)
    {
        $this->entIdentificacao = $entIdentificacao;

        return $this;
    }

    /**
     * Get entIdentificacao
     *
     * @return string
     */
    public function getEntIdentificacao()
    {
        return $this->entIdentificacao;
    }

    /**
     * Set entEndereco
     *
     * @param string $entEndereco
     *
     * @return B2cPedped
     */
    public function setEntEndereco($entEndereco)
    {
        $this->entEndereco = $entEndereco;

        return $this;
    }

    /**
     * Get entEndereco
     *
     * @return string
     */
    public function getEntEndereco()
    {
        return $this->entEndereco;
    }

    /**
     * Set entEndNumero
     *
     * @param string $entEndNumero
     *
     * @return B2cPedped
     */
    public function setEntEndNumero($entEndNumero)
    {
        $this->entEndNumero = $entEndNumero;

        return $this;
    }

    /**
     * Get entEndNumero
     *
     * @return string
     */
    public function getEntEndNumero()
    {
        return $this->entEndNumero;
    }

    /**
     * Set entEndComp
     *
     * @param string $entEndComp
     *
     * @return B2cPedped
     */
    public function setEntEndComp($entEndComp)
    {
        $this->entEndComp = $entEndComp;

        return $this;
    }

    /**
     * Get entEndComp
     *
     * @return string
     */
    public function getEntEndComp()
    {
        return $this->entEndComp;
    }

    /**
     * Set entEndBairro
     *
     * @param string $entEndBairro
     *
     * @return B2cPedped
     */
    public function setEntEndBairro($entEndBairro)
    {
        $this->entEndBairro = $entEndBairro;

        return $this;
    }

    /**
     * Get entEndBairro
     *
     * @return string
     */
    public function getEntEndBairro()
    {
        return $this->entEndBairro;
    }

    /**
     * Set entEndCidade
     *
     * @param string $entEndCidade
     *
     * @return B2cPedped
     */
    public function setEntEndCidade($entEndCidade)
    {
        $this->entEndCidade = $entEndCidade;

        return $this;
    }

    /**
     * Get entEndCidade
     *
     * @return string
     */
    public function getEntEndCidade()
    {
        return $this->entEndCidade;
    }

    /**
     * Set entCodIbge
     *
     * @param integer $entCodIbge
     *
     * @return B2cPedped
     */
    public function setEntCodIbge($entCodIbge)
    {
        $this->entCodIbge = $entCodIbge;

        return $this;
    }

    /**
     * Get entCodIbge
     *
     * @return integer
     */
    public function getEntCodIbge()
    {
        return $this->entCodIbge;
    }

    /**
     * Set entEndUf
     *
     * @param string $entEndUf
     *
     * @return B2cPedped
     */
    public function setEntEndUf($entEndUf)
    {
        $this->entEndUf = $entEndUf;

        return $this;
    }

    /**
     * Get entEndUf
     *
     * @return string
     */
    public function getEntEndUf()
    {
        return $this->entEndUf;
    }

    /**
     * Set entEndCep
     *
     * @param string $entEndCep
     *
     * @return B2cPedped
     */
    public function setEntEndCep($entEndCep)
    {
        $this->entEndCep = $entEndCep;

        return $this;
    }

    /**
     * Get entEndCep
     *
     * @return string
     */
    public function getEntEndCep()
    {
        return $this->entEndCep;
    }

    /**
     * Set entEndRef
     *
     * @param string $entEndRef
     *
     * @return B2cPedped
     */
    public function setEntEndRef($entEndRef)
    {
        $this->entEndRef = $entEndRef;

        return $this;
    }

    /**
     * Get entEndRef
     *
     * @return string
     */
    public function getEntEndRef()
    {
        return $this->entEndRef;
    }

    /**
     * Set entEndProvincia
     *
     * @param string $entEndProvincia
     *
     * @return B2cPedped
     */
    public function setEntEndProvincia($entEndProvincia)
    {
        $this->entEndProvincia = $entEndProvincia;

        return $this;
    }

    /**
     * Get entEndProvincia
     *
     * @return string
     */
    public function getEntEndProvincia()
    {
        return $this->entEndProvincia;
    }

    /**
     * Set entEndPais
     *
     * @param string $entEndPais
     *
     * @return B2cPedped
     */
    public function setEntEndPais($entEndPais)
    {
        $this->entEndPais = $entEndPais;

        return $this;
    }

    /**
     * Get entEndPais
     *
     * @return string
     */
    public function getEntEndPais()
    {
        return $this->entEndPais;
    }

    /**
     * Set entTelA
     *
     * @param string $entTelA
     *
     * @return B2cPedped
     */
    public function setEntTelA($entTelA)
    {
        $this->entTelA = $entTelA;

        return $this;
    }

    /**
     * Get entTelA
     *
     * @return string
     */
    public function getEntTelA()
    {
        return $this->entTelA;
    }

    /**
     * Set entTelB
     *
     * @param string $entTelB
     *
     * @return B2cPedped
     */
    public function setEntTelB($entTelB)
    {
        $this->entTelB = $entTelB;

        return $this;
    }

    /**
     * Get entTelB
     *
     * @return string
     */
    public function getEntTelB()
    {
        return $this->entTelB;
    }

    /**
     * Set pedObservacao
     *
     * @param string $pedObservacao
     *
     * @return B2cPedped
     */
    public function setPedObservacao($pedObservacao)
    {
        $this->pedObservacao = $pedObservacao;

        return $this;
    }

    /**
     * Get pedObservacao
     *
     * @return string
     */
    public function getPedObservacao()
    {
        return $this->pedObservacao;
    }

    /**
     * Set pedTipoFrete
     *
     * @param string $pedTipoFrete
     *
     * @return B2cPedped
     */
    public function setPedTipoFrete($pedTipoFrete)
    {
        $this->pedTipoFrete = $pedTipoFrete;

        return $this;
    }

    /**
     * Get pedTipoFrete
     *
     * @return string
     */
    public function getPedTipoFrete()
    {
        return $this->pedTipoFrete;
    }

    /**
     * Set pedTransportadora
     *
     * @param string $pedTransportadora
     *
     * @return B2cPedped
     */
    public function setPedTransportadora($pedTransportadora)
    {
        $this->pedTransportadora = $pedTransportadora;

        return $this;
    }

    /**
     * Get pedTransportadora
     *
     * @return string
     */
    public function getPedTransportadora()
    {
        return $this->pedTransportadora;
    }

    /**
     * Set pedPrazoEntrega
     *
     * @param integer $pedPrazoEntrega
     *
     * @return B2cPedped
     */
    public function setPedPrazoEntrega($pedPrazoEntrega)
    {
        $this->pedPrazoEntrega = $pedPrazoEntrega;

        return $this;
    }

    /**
     * Get pedPrazoEntrega
     *
     * @return integer
     */
    public function getPedPrazoEntrega()
    {
        return $this->pedPrazoEntrega;
    }

    /**
     * Set pedPrazoTransporte
     *
     * @param integer $pedPrazoTransporte
     *
     * @return B2cPedped
     */
    public function setPedPrazoTransporte($pedPrazoTransporte)
    {
        $this->pedPrazoTransporte = $pedPrazoTransporte;

        return $this;
    }

    /**
     * Get pedPrazoTransporte
     *
     * @return integer
     */
    public function getPedPrazoTransporte()
    {
        return $this->pedPrazoTransporte;
    }

    /**
     * Set pedDataEntrega
     *
     * @param \DateTime $pedDataEntrega
     *
     * @return B2cPedped
     */
    public function setPedDataEntrega($pedDataEntrega)
    {
        $this->pedDataEntrega = $pedDataEntrega;

        return $this;
    }

    /**
     * Get pedDataEntrega
     *
     * @return \DateTime
     */
    public function getPedDataEntrega()
    {
        return $this->pedDataEntrega;
    }

    /**
     * Set pedEntregaRegiao
     *
     * @param string $pedEntregaRegiao
     *
     * @return B2cPedped
     */
    public function setPedEntregaRegiao($pedEntregaRegiao)
    {
        $this->pedEntregaRegiao = $pedEntregaRegiao;

        return $this;
    }

    /**
     * Get pedEntregaRegiao
     *
     * @return string
     */
    public function getPedEntregaRegiao()
    {
        return $this->pedEntregaRegiao;
    }

    /**
     * Set pedCodEnvio
     *
     * @param string $pedCodEnvio
     *
     * @return B2cPedped
     */
    public function setPedCodEnvio($pedCodEnvio)
    {
        $this->pedCodEnvio = $pedCodEnvio;

        return $this;
    }

    /**
     * Get pedCodEnvio
     *
     * @return string
     */
    public function getPedCodEnvio()
    {
        return $this->pedCodEnvio;
    }

    /**
     * Set pedMontagem
     *
     * @param integer $pedMontagem
     *
     * @return B2cPedped
     */
    public function setPedMontagem($pedMontagem)
    {
        $this->pedMontagem = $pedMontagem;

        return $this;
    }

    /**
     * Get pedMontagem
     *
     * @return integer
     */
    public function getPedMontagem()
    {
        return $this->pedMontagem;
    }

    /**
     * Set pedValorMontagem
     *
     * @param float $pedValorMontagem
     *
     * @return B2cPedped
     */
    public function setPedValorMontagem($pedValorMontagem)
    {
        $this->pedValorMontagem = $pedValorMontagem;

        return $this;
    }

    /**
     * Get pedValorMontagem
     *
     * @return float
     */
    public function getPedValorMontagem()
    {
        return $this->pedValorMontagem;
    }

    /**
     * Set pedValorSubtotal
     *
     * @param float $pedValorSubtotal
     *
     * @return B2cPedped
     */
    public function setPedValorSubtotal($pedValorSubtotal)
    {
        $this->pedValorSubtotal = $pedValorSubtotal;

        return $this;
    }

    /**
     * Get pedValorSubtotal
     *
     * @return float
     */
    public function getPedValorSubtotal()
    {
        return $this->pedValorSubtotal;
    }

    /**
     * Set pedValorEmbalagem
     *
     * @param float $pedValorEmbalagem
     *
     * @return B2cPedped
     */
    public function setPedValorEmbalagem($pedValorEmbalagem)
    {
        $this->pedValorEmbalagem = $pedValorEmbalagem;

        return $this;
    }

    /**
     * Get pedValorEmbalagem
     *
     * @return float
     */
    public function getPedValorEmbalagem()
    {
        return $this->pedValorEmbalagem;
    }

    /**
     * Set pedValorCustofrete
     *
     * @param float $pedValorCustofrete
     *
     * @return B2cPedped
     */
    public function setPedValorCustofrete($pedValorCustofrete)
    {
        $this->pedValorCustofrete = $pedValorCustofrete;

        return $this;
    }

    /**
     * Get pedValorCustofrete
     *
     * @return float
     */
    public function getPedValorCustofrete()
    {
        return $this->pedValorCustofrete;
    }

    /**
     * Set pedValorFrete
     *
     * @param float $pedValorFrete
     *
     * @return B2cPedped
     */
    public function setPedValorFrete($pedValorFrete)
    {
        $this->pedValorFrete = $pedValorFrete;

        return $this;
    }

    /**
     * Get pedValorFrete
     *
     * @return float
     */
    public function getPedValorFrete()
    {
        return $this->pedValorFrete;
    }

    /**
     * Set pedValorDesconto
     *
     * @param float $pedValorDesconto
     *
     * @return B2cPedped
     */
    public function setPedValorDesconto($pedValorDesconto)
    {
        $this->pedValorDesconto = $pedValorDesconto;

        return $this;
    }

    /**
     * Get pedValorDesconto
     *
     * @return float
     */
    public function getPedValorDesconto()
    {
        return $this->pedValorDesconto;
    }

    /**
     * Set pedValorDescontoLoja
     *
     * @param float $pedValorDescontoLoja
     *
     * @return B2cPedped
     */
    public function setPedValorDescontoLoja($pedValorDescontoLoja)
    {
        $this->pedValorDescontoLoja = $pedValorDescontoLoja;

        return $this;
    }

    /**
     * Get pedValorDescontoLoja
     *
     * @return float
     */
    public function getPedValorDescontoLoja()
    {
        return $this->pedValorDescontoLoja;
    }

    /**
     * Set pedValorValecompra
     *
     * @param float $pedValorValecompra
     *
     * @return B2cPedped
     */
    public function setPedValorValecompra($pedValorValecompra)
    {
        $this->pedValorValecompra = $pedValorValecompra;

        return $this;
    }

    /**
     * Get pedValorValecompra
     *
     * @return float
     */
    public function getPedValorValecompra()
    {
        return $this->pedValorValecompra;
    }

    /**
     * Set pedValorTotal
     *
     * @param float $pedValorTotal
     *
     * @return B2cPedped
     */
    public function setPedValorTotal($pedValorTotal)
    {
        $this->pedValorTotal = $pedValorTotal;

        return $this;
    }

    /**
     * Get pedValorTotal
     *
     * @return float
     */
    public function getPedValorTotal()
    {
        return $this->pedValorTotal;
    }

    /**
     * Set idPagpag1
     *
     * @param integer $idPagpag1
     *
     * @return B2cPedped
     */
    public function setIdPagpag1($idPagpag1)
    {
        $this->idPagpag1 = $idPagpag1;

        return $this;
    }

    /**
     * Get idPagpag1
     *
     * @return integer
     */
    public function getIdPagpag1()
    {
        return $this->idPagpag1;
    }

    /**
     * Set idPagfor1
     *
     * @param string $idPagfor1
     *
     * @return B2cPedped
     */
    public function setIdPagfor1($idPagfor1)
    {
        $this->idPagfor1 = $idPagfor1;

        return $this;
    }

    /**
     * Get idPagfor1
     *
     * @return string
     */
    public function getIdPagfor1()
    {
        return $this->idPagfor1;
    }

    /**
     * Set pedValorTotal1
     *
     * @param float $pedValorTotal1
     *
     * @return B2cPedped
     */
    public function setPedValorTotal1($pedValorTotal1)
    {
        $this->pedValorTotal1 = $pedValorTotal1;

        return $this;
    }

    /**
     * Get pedValorTotal1
     *
     * @return float
     */
    public function getPedValorTotal1()
    {
        return $this->pedValorTotal1;
    }

    /**
     * Set pedValorReal1
     *
     * @param float $pedValorReal1
     *
     * @return B2cPedped
     */
    public function setPedValorReal1($pedValorReal1)
    {
        $this->pedValorReal1 = $pedValorReal1;

        return $this;
    }

    /**
     * Get pedValorReal1
     *
     * @return float
     */
    public function getPedValorReal1()
    {
        return $this->pedValorReal1;
    }

    /**
     * Set pagNumParcelas1
     *
     * @param integer $pagNumParcelas1
     *
     * @return B2cPedped
     */
    public function setPagNumParcelas1($pagNumParcelas1)
    {
        $this->pagNumParcelas1 = $pagNumParcelas1;

        return $this;
    }

    /**
     * Get pagNumParcelas1
     *
     * @return integer
     */
    public function getPagNumParcelas1()
    {
        return $this->pagNumParcelas1;
    }

    /**
     * Set pagValEntrada1
     *
     * @param float $pagValEntrada1
     *
     * @return B2cPedped
     */
    public function setPagValEntrada1($pagValEntrada1)
    {
        $this->pagValEntrada1 = $pagValEntrada1;

        return $this;
    }

    /**
     * Get pagValEntrada1
     *
     * @return float
     */
    public function getPagValEntrada1()
    {
        return $this->pagValEntrada1;
    }

    /**
     * Set pagValParcela1
     *
     * @param float $pagValParcela1
     *
     * @return B2cPedped
     */
    public function setPagValParcela1($pagValParcela1)
    {
        $this->pagValParcela1 = $pagValParcela1;

        return $this;
    }

    /**
     * Get pagValParcela1
     *
     * @return float
     */
    public function getPagValParcela1()
    {
        return $this->pagValParcela1;
    }

    /**
     * Set pagIndexador1
     *
     * @param float $pagIndexador1
     *
     * @return B2cPedped
     */
    public function setPagIndexador1($pagIndexador1)
    {
        $this->pagIndexador1 = $pagIndexador1;

        return $this;
    }

    /**
     * Get pagIndexador1
     *
     * @return float
     */
    public function getPagIndexador1()
    {
        return $this->pagIndexador1;
    }

    /**
     * Set boletoParcelado1
     *
     * @param integer $boletoParcelado1
     *
     * @return B2cPedped
     */
    public function setBoletoParcelado1($boletoParcelado1)
    {
        $this->boletoParcelado1 = $boletoParcelado1;

        return $this;
    }

    /**
     * Get boletoParcelado1
     *
     * @return integer
     */
    public function getBoletoParcelado1()
    {
        return $this->boletoParcelado1;
    }

    /**
     * Set boletoVencimento1
     *
     * @param \DateTime $boletoVencimento1
     *
     * @return B2cPedped
     */
    public function setBoletoVencimento1($boletoVencimento1)
    {
        $this->boletoVencimento1 = $boletoVencimento1;

        return $this;
    }

    /**
     * Get boletoVencimento1
     *
     * @return \DateTime
     */
    public function getBoletoVencimento1()
    {
        return $this->boletoVencimento1;
    }

    /**
     * Set pedTipoPagamento1
     *
     * @param string $pedTipoPagamento1
     *
     * @return B2cPedped
     */
    public function setPedTipoPagamento1($pedTipoPagamento1)
    {
        $this->pedTipoPagamento1 = $pedTipoPagamento1;

        return $this;
    }

    /**
     * Get pedTipoPagamento1
     *
     * @return string
     */
    public function getPedTipoPagamento1()
    {
        return $this->pedTipoPagamento1;
    }

    /**
     * Set pedFormaPagamento1
     *
     * @param string $pedFormaPagamento1
     *
     * @return B2cPedped
     */
    public function setPedFormaPagamento1($pedFormaPagamento1)
    {
        $this->pedFormaPagamento1 = $pedFormaPagamento1;

        return $this;
    }

    /**
     * Get pedFormaPagamento1
     *
     * @return string
     */
    public function getPedFormaPagamento1()
    {
        return $this->pedFormaPagamento1;
    }

    /**
     * Set pedFormaPagdesc1
     *
     * @param string $pedFormaPagdesc1
     *
     * @return B2cPedped
     */
    public function setPedFormaPagdesc1($pedFormaPagdesc1)
    {
        $this->pedFormaPagdesc1 = $pedFormaPagdesc1;

        return $this;
    }

    /**
     * Get pedFormaPagdesc1
     *
     * @return string
     */
    public function getPedFormaPagdesc1()
    {
        return $this->pedFormaPagdesc1;
    }

    /**
     * Set pedValorDescontoLoja1
     *
     * @param float $pedValorDescontoLoja1
     *
     * @return B2cPedped
     */
    public function setPedValorDescontoLoja1($pedValorDescontoLoja1)
    {
        $this->pedValorDescontoLoja1 = $pedValorDescontoLoja1;

        return $this;
    }

    /**
     * Get pedValorDescontoLoja1
     *
     * @return float
     */
    public function getPedValorDescontoLoja1()
    {
        return $this->pedValorDescontoLoja1;
    }

    /**
     * Set contadorAlteracaoDescontoLoja1
     *
     * @param integer $contadorAlteracaoDescontoLoja1
     *
     * @return B2cPedped
     */
    public function setContadorAlteracaoDescontoLoja1($contadorAlteracaoDescontoLoja1)
    {
        $this->contadorAlteracaoDescontoLoja1 = $contadorAlteracaoDescontoLoja1;

        return $this;
    }

    /**
     * Get contadorAlteracaoDescontoLoja1
     *
     * @return integer
     */
    public function getContadorAlteracaoDescontoLoja1()
    {
        return $this->contadorAlteracaoDescontoLoja1;
    }

    /**
     * Set pagamentoConfirmado1
     *
     * @param integer $pagamentoConfirmado1
     *
     * @return B2cPedped
     */
    public function setPagamentoConfirmado1($pagamentoConfirmado1)
    {
        $this->pagamentoConfirmado1 = $pagamentoConfirmado1;

        return $this;
    }

    /**
     * Get pagamentoConfirmado1
     *
     * @return integer
     */
    public function getPagamentoConfirmado1()
    {
        return $this->pagamentoConfirmado1;
    }

    /**
     * Set idPagpag2
     *
     * @param integer $idPagpag2
     *
     * @return B2cPedped
     */
    public function setIdPagpag2($idPagpag2)
    {
        $this->idPagpag2 = $idPagpag2;

        return $this;
    }

    /**
     * Get idPagpag2
     *
     * @return integer
     */
    public function getIdPagpag2()
    {
        return $this->idPagpag2;
    }

    /**
     * Set idPagfor2
     *
     * @param string $idPagfor2
     *
     * @return B2cPedped
     */
    public function setIdPagfor2($idPagfor2)
    {
        $this->idPagfor2 = $idPagfor2;

        return $this;
    }

    /**
     * Get idPagfor2
     *
     * @return string
     */
    public function getIdPagfor2()
    {
        return $this->idPagfor2;
    }

    /**
     * Set pedValorTotal2
     *
     * @param float $pedValorTotal2
     *
     * @return B2cPedped
     */
    public function setPedValorTotal2($pedValorTotal2)
    {
        $this->pedValorTotal2 = $pedValorTotal2;

        return $this;
    }

    /**
     * Get pedValorTotal2
     *
     * @return float
     */
    public function getPedValorTotal2()
    {
        return $this->pedValorTotal2;
    }

    /**
     * Set pedValorReal2
     *
     * @param float $pedValorReal2
     *
     * @return B2cPedped
     */
    public function setPedValorReal2($pedValorReal2)
    {
        $this->pedValorReal2 = $pedValorReal2;

        return $this;
    }

    /**
     * Get pedValorReal2
     *
     * @return float
     */
    public function getPedValorReal2()
    {
        return $this->pedValorReal2;
    }

    /**
     * Set pagNumParcelas2
     *
     * @param integer $pagNumParcelas2
     *
     * @return B2cPedped
     */
    public function setPagNumParcelas2($pagNumParcelas2)
    {
        $this->pagNumParcelas2 = $pagNumParcelas2;

        return $this;
    }

    /**
     * Get pagNumParcelas2
     *
     * @return integer
     */
    public function getPagNumParcelas2()
    {
        return $this->pagNumParcelas2;
    }

    /**
     * Set pagValEntrada2
     *
     * @param float $pagValEntrada2
     *
     * @return B2cPedped
     */
    public function setPagValEntrada2($pagValEntrada2)
    {
        $this->pagValEntrada2 = $pagValEntrada2;

        return $this;
    }

    /**
     * Get pagValEntrada2
     *
     * @return float
     */
    public function getPagValEntrada2()
    {
        return $this->pagValEntrada2;
    }

    /**
     * Set pagValParcela2
     *
     * @param float $pagValParcela2
     *
     * @return B2cPedped
     */
    public function setPagValParcela2($pagValParcela2)
    {
        $this->pagValParcela2 = $pagValParcela2;

        return $this;
    }

    /**
     * Get pagValParcela2
     *
     * @return float
     */
    public function getPagValParcela2()
    {
        return $this->pagValParcela2;
    }

    /**
     * Set pagIndexador2
     *
     * @param float $pagIndexador2
     *
     * @return B2cPedped
     */
    public function setPagIndexador2($pagIndexador2)
    {
        $this->pagIndexador2 = $pagIndexador2;

        return $this;
    }

    /**
     * Get pagIndexador2
     *
     * @return float
     */
    public function getPagIndexador2()
    {
        return $this->pagIndexador2;
    }

    /**
     * Set boletoParcelado2
     *
     * @param integer $boletoParcelado2
     *
     * @return B2cPedped
     */
    public function setBoletoParcelado2($boletoParcelado2)
    {
        $this->boletoParcelado2 = $boletoParcelado2;

        return $this;
    }

    /**
     * Get boletoParcelado2
     *
     * @return integer
     */
    public function getBoletoParcelado2()
    {
        return $this->boletoParcelado2;
    }

    /**
     * Set boletoVencimento2
     *
     * @param \DateTime $boletoVencimento2
     *
     * @return B2cPedped
     */
    public function setBoletoVencimento2($boletoVencimento2)
    {
        $this->boletoVencimento2 = $boletoVencimento2;

        return $this;
    }

    /**
     * Get boletoVencimento2
     *
     * @return \DateTime
     */
    public function getBoletoVencimento2()
    {
        return $this->boletoVencimento2;
    }

    /**
     * Set pedTipoPagamento2
     *
     * @param string $pedTipoPagamento2
     *
     * @return B2cPedped
     */
    public function setPedTipoPagamento2($pedTipoPagamento2)
    {
        $this->pedTipoPagamento2 = $pedTipoPagamento2;

        return $this;
    }

    /**
     * Get pedTipoPagamento2
     *
     * @return string
     */
    public function getPedTipoPagamento2()
    {
        return $this->pedTipoPagamento2;
    }

    /**
     * Set pedFormaPagamento2
     *
     * @param string $pedFormaPagamento2
     *
     * @return B2cPedped
     */
    public function setPedFormaPagamento2($pedFormaPagamento2)
    {
        $this->pedFormaPagamento2 = $pedFormaPagamento2;

        return $this;
    }

    /**
     * Get pedFormaPagamento2
     *
     * @return string
     */
    public function getPedFormaPagamento2()
    {
        return $this->pedFormaPagamento2;
    }

    /**
     * Set pedFormaPagdesc2
     *
     * @param string $pedFormaPagdesc2
     *
     * @return B2cPedped
     */
    public function setPedFormaPagdesc2($pedFormaPagdesc2)
    {
        $this->pedFormaPagdesc2 = $pedFormaPagdesc2;

        return $this;
    }

    /**
     * Get pedFormaPagdesc2
     *
     * @return string
     */
    public function getPedFormaPagdesc2()
    {
        return $this->pedFormaPagdesc2;
    }

    /**
     * Set pedValorDescontoLoja2
     *
     * @param float $pedValorDescontoLoja2
     *
     * @return B2cPedped
     */
    public function setPedValorDescontoLoja2($pedValorDescontoLoja2)
    {
        $this->pedValorDescontoLoja2 = $pedValorDescontoLoja2;

        return $this;
    }

    /**
     * Get pedValorDescontoLoja2
     *
     * @return float
     */
    public function getPedValorDescontoLoja2()
    {
        return $this->pedValorDescontoLoja2;
    }

    /**
     * Set contadorAlteracaoDescontoLoja2
     *
     * @param integer $contadorAlteracaoDescontoLoja2
     *
     * @return B2cPedped
     */
    public function setContadorAlteracaoDescontoLoja2($contadorAlteracaoDescontoLoja2)
    {
        $this->contadorAlteracaoDescontoLoja2 = $contadorAlteracaoDescontoLoja2;

        return $this;
    }

    /**
     * Get contadorAlteracaoDescontoLoja2
     *
     * @return integer
     */
    public function getContadorAlteracaoDescontoLoja2()
    {
        return $this->contadorAlteracaoDescontoLoja2;
    }

    /**
     * Set pagamentoConfirmado2
     *
     * @param integer $pagamentoConfirmado2
     *
     * @return B2cPedped
     */
    public function setPagamentoConfirmado2($pagamentoConfirmado2)
    {
        $this->pagamentoConfirmado2 = $pagamentoConfirmado2;

        return $this;
    }

    /**
     * Get pagamentoConfirmado2
     *
     * @return integer
     */
    public function getPagamentoConfirmado2()
    {
        return $this->pagamentoConfirmado2;
    }

    /**
     * Set pedOrigem
     *
     * @param string $pedOrigem
     *
     * @return B2cPedped
     */
    public function setPedOrigem($pedOrigem)
    {
        $this->pedOrigem = $pedOrigem;

        return $this;
    }

    /**
     * Get pedOrigem
     *
     * @return string
     */
    public function getPedOrigem()
    {
        return $this->pedOrigem;
    }

    /**
     * Set pedPrimeiraOrigem
     *
     * @param string $pedPrimeiraOrigem
     *
     * @return B2cPedped
     */
    public function setPedPrimeiraOrigem($pedPrimeiraOrigem)
    {
        $this->pedPrimeiraOrigem = $pedPrimeiraOrigem;

        return $this;
    }

    /**
     * Get pedPrimeiraOrigem
     *
     * @return string
     */
    public function getPedPrimeiraOrigem()
    {
        return $this->pedPrimeiraOrigem;
    }

    /**
     * Set pedPrimeiraOrigemData
     *
     * @param string $pedPrimeiraOrigemData
     *
     * @return B2cPedped
     */
    public function setPedPrimeiraOrigemData($pedPrimeiraOrigemData)
    {
        $this->pedPrimeiraOrigemData = $pedPrimeiraOrigemData;

        return $this;
    }

    /**
     * Get pedPrimeiraOrigemData
     *
     * @return string
     */
    public function getPedPrimeiraOrigemData()
    {
        return $this->pedPrimeiraOrigemData;
    }

    /**
     * Set pedReferer
     *
     * @param string $pedReferer
     *
     * @return B2cPedped
     */
    public function setPedReferer($pedReferer)
    {
        $this->pedReferer = $pedReferer;

        return $this;
    }

    /**
     * Get pedReferer
     *
     * @return string
     */
    public function getPedReferer()
    {
        return $this->pedReferer;
    }

    /**
     * Set pedCupom
     *
     * @param string $pedCupom
     *
     * @return B2cPedped
     */
    public function setPedCupom($pedCupom)
    {
        $this->pedCupom = $pedCupom;

        return $this;
    }

    /**
     * Get pedCupom
     *
     * @return string
     */
    public function getPedCupom()
    {
        return $this->pedCupom;
    }

    /**
     * Set pedPromocao
     *
     * @param string $pedPromocao
     *
     * @return B2cPedped
     */
    public function setPedPromocao($pedPromocao)
    {
        $this->pedPromocao = $pedPromocao;

        return $this;
    }

    /**
     * Get pedPromocao
     *
     * @return string
     */
    public function getPedPromocao()
    {
        return $this->pedPromocao;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return B2cPedped
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set csUsuario
     *
     * @param string $csUsuario
     *
     * @return B2cPedped
     */
    public function setCsUsuario($csUsuario)
    {
        $this->csUsuario = $csUsuario;

        return $this;
    }

    /**
     * Get csUsuario
     *
     * @return string
     */
    public function getCsUsuario()
    {
        return $this->csUsuario;
    }

    /**
     * Set csStatus
     *
     * @param integer $csStatus
     *
     * @return B2cPedped
     */
    public function setCsStatus($csStatus)
    {
        $this->csStatus = $csStatus;

        return $this;
    }

    /**
     * Get csStatus
     *
     * @return integer
     */
    public function getCsStatus()
    {
        return $this->csStatus;
    }

    /**
     * Set csScore
     *
     * @param float $csScore
     *
     * @return B2cPedped
     */
    public function setCsScore($csScore)
    {
        $this->csScore = $csScore;

        return $this;
    }

    /**
     * Get csScore
     *
     * @return float
     */
    public function getCsScore()
    {
        return $this->csScore;
    }

    /**
     * Set csDiagnostico
     *
     * @param string $csDiagnostico
     *
     * @return B2cPedped
     */
    public function setCsDiagnostico($csDiagnostico)
    {
        $this->csDiagnostico = $csDiagnostico;

        return $this;
    }

    /**
     * Get csDiagnostico
     *
     * @return string
     */
    public function getCsDiagnostico()
    {
        return $this->csDiagnostico;
    }

    /**
     * Set csSolicitacao
     *
     * @param string $csSolicitacao
     *
     * @return B2cPedped
     */
    public function setCsSolicitacao($csSolicitacao)
    {
        $this->csSolicitacao = $csSolicitacao;

        return $this;
    }

    /**
     * Get csSolicitacao
     *
     * @return string
     */
    public function getCsSolicitacao()
    {
        return $this->csSolicitacao;
    }

    /**
     * Set seqped
     *
     * @param integer $seqped
     *
     * @return B2cPedped
     */
    public function setSeqped($seqped)
    {
        $this->seqped = $seqped;

        return $this;
    }

    /**
     * Get seqped
     *
     * @return integer
     */
    public function getSeqped()
    {
        return $this->seqped;
    }

    /**
     * Set exportado
     *
     * @param \DateTime $exportado
     *
     * @return B2cPedped
     */
    public function setExportado($exportado)
    {
        $this->exportado = $exportado;

        return $this;
    }

    /**
     * Get exportado
     *
     * @return \DateTime
     */
    public function getExportado()
    {
        return $this->exportado;
    }

    /**
     * Set pedTransportadoraIntegrado
     *
     * @param integer $pedTransportadoraIntegrado
     *
     * @return B2cPedped
     */
    public function setPedTransportadoraIntegrado($pedTransportadoraIntegrado)
    {
        $this->pedTransportadoraIntegrado = $pedTransportadoraIntegrado;

        return $this;
    }

    /**
     * Get pedTransportadoraIntegrado
     *
     * @return integer
     */
    public function getPedTransportadoraIntegrado()
    {
        return $this->pedTransportadoraIntegrado;
    }

    /**
     * Set pedValorComissao
     *
     * @param float $pedValorComissao
     *
     * @return B2cPedped
     */
    public function setPedValorComissao($pedValorComissao)
    {
        $this->pedValorComissao = $pedValorComissao;

        return $this;
    }

    /**
     * Get pedValorComissao
     *
     * @return float
     */
    public function getPedValorComissao()
    {
        return $this->pedValorComissao;
    }

    /**
     * Set pedDataAfiliado
     *
     * @param string $pedDataAfiliado
     *
     * @return B2cPedped
     */
    public function setPedDataAfiliado($pedDataAfiliado)
    {
        $this->pedDataAfiliado = $pedDataAfiliado;

        return $this;
    }

    /**
     * Get pedDataAfiliado
     *
     * @return string
     */
    public function getPedDataAfiliado()
    {
        return $this->pedDataAfiliado;
    }

    /**
     * Set pedAfiliado
     *
     * @param integer $pedAfiliado
     *
     * @return B2cPedped
     */
    public function setPedAfiliado($pedAfiliado)
    {
        $this->pedAfiliado = $pedAfiliado;

        return $this;
    }

    /**
     * Get pedAfiliado
     *
     * @return integer
     */
    public function getPedAfiliado()
    {
        return $this->pedAfiliado;
    }

    /**
     * Set ordemGeracaoBoleto
     *
     * @param integer $ordemGeracaoBoleto
     *
     * @return B2cPedped
     */
    public function setOrdemGeracaoBoleto($ordemGeracaoBoleto)
    {
        $this->ordemGeracaoBoleto = $ordemGeracaoBoleto;

        return $this;
    }

    /**
     * Get ordemGeracaoBoleto
     *
     * @return integer
     */
    public function getOrdemGeracaoBoleto()
    {
        return $this->ordemGeracaoBoleto;
    }

    /**
     * Set analyticsNegativado
     *
     * @param integer $analyticsNegativado
     *
     * @return B2cPedped
     */
    public function setAnalyticsNegativado($analyticsNegativado)
    {
        $this->analyticsNegativado = $analyticsNegativado;

        return $this;
    }

    /**
     * Get analyticsNegativado
     *
     * @return integer
     */
    public function getAnalyticsNegativado()
    {
        return $this->analyticsNegativado;
    }

    /**
     * Set pedEntregaData
     *
     * @param \DateTime $pedEntregaData
     *
     * @return B2cPedped
     */
    public function setPedEntregaData($pedEntregaData)
    {
        $this->pedEntregaData = $pedEntregaData;

        return $this;
    }

    /**
     * Get pedEntregaData
     *
     * @return \DateTime
     */
    public function getPedEntregaData()
    {
        return $this->pedEntregaData;
    }

    /**
     * Set pedFaturado
     *
     * @param string $pedFaturado
     *
     * @return B2cPedped
     */
    public function setPedFaturado($pedFaturado)
    {
        $this->pedFaturado = $pedFaturado;

        return $this;
    }

    /**
     * Get pedFaturado
     *
     * @return string
     */
    public function getPedFaturado()
    {
        return $this->pedFaturado;
    }

    /**
     * Set pagTel1
     *
     * @param string $pagTel1
     *
     * @return B2cPedped
     */
    public function setPagTel1($pagTel1)
    {
        $this->pagTel1 = $pagTel1;

        return $this;
    }

    /**
     * Get pagTel1
     *
     * @return string
     */
    public function getPagTel1()
    {
        return $this->pagTel1;
    }

    /**
     * Set pagTel2
     *
     * @param string $pagTel2
     *
     * @return B2cPedped
     */
    public function setPagTel2($pagTel2)
    {
        $this->pagTel2 = $pagTel2;

        return $this;
    }

    /**
     * Get pagTel2
     *
     * @return string
     */
    public function getPagTel2()
    {
        return $this->pagTel2;
    }

    /**
     * Set pagTitular1
     *
     * @param string $pagTitular1
     *
     * @return B2cPedped
     */
    public function setPagTitular1($pagTitular1)
    {
        $this->pagTitular1 = $pagTitular1;

        return $this;
    }

    /**
     * Get pagTitular1
     *
     * @return string
     */
    public function getPagTitular1()
    {
        return $this->pagTitular1;
    }

    /**
     * Set pagTitular2
     *
     * @param string $pagTitular2
     *
     * @return B2cPedped
     */
    public function setPagTitular2($pagTitular2)
    {
        $this->pagTitular2 = $pagTitular2;

        return $this;
    }

    /**
     * Get pagTitular2
     *
     * @return string
     */
    public function getPagTitular2()
    {
        return $this->pagTitular2;
    }

    /**
     * Set contaContabil1
     *
     * @param string $contaContabil1
     *
     * @return B2cPedped
     */
    public function setContaContabil1($contaContabil1)
    {
        $this->contaContabil1 = $contaContabil1;

        return $this;
    }

    /**
     * Get contaContabil1
     *
     * @return string
     */
    public function getContaContabil1()
    {
        return $this->contaContabil1;
    }

    /**
     * Set contaContabil2
     *
     * @param string $contaContabil2
     *
     * @return B2cPedped
     */
    public function setContaContabil2($contaContabil2)
    {
        $this->contaContabil2 = $contaContabil2;

        return $this;
    }

    /**
     * Get contaContabil2
     *
     * @return string
     */
    public function getContaContabil2()
    {
        return $this->contaContabil2;
    }

    /**
     * Set contaContabilNome1
     *
     * @param string $contaContabilNome1
     *
     * @return B2cPedped
     */
    public function setContaContabilNome1($contaContabilNome1)
    {
        $this->contaContabilNome1 = $contaContabilNome1;

        return $this;
    }

    /**
     * Get contaContabilNome1
     *
     * @return string
     */
    public function getContaContabilNome1()
    {
        return $this->contaContabilNome1;
    }

    /**
     * Set contaContabilNome2
     *
     * @param string $contaContabilNome2
     *
     * @return B2cPedped
     */
    public function setContaContabilNome2($contaContabilNome2)
    {
        $this->contaContabilNome2 = $contaContabilNome2;

        return $this;
    }

    /**
     * Get contaContabilNome2
     *
     * @return string
     */
    public function getContaContabilNome2()
    {
        return $this->contaContabilNome2;
    }

    /**
     * Set sfOpportunity
     *
     * @param string $sfOpportunity
     *
     * @return B2cPedped
     */
    public function setSfOpportunity($sfOpportunity)
    {
        $this->sfOpportunity = $sfOpportunity;

        return $this;
    }

    /**
     * Get sfOpportunity
     *
     * @return string
     */
    public function getSfOpportunity()
    {
        return $this->sfOpportunity;
    }

    /**
     * Set sfDataSync
     *
     * @param \DateTime $sfDataSync
     *
     * @return B2cPedped
     */
    public function setSfDataSync($sfDataSync)
    {
        $this->sfDataSync = $sfDataSync;

        return $this;
    }

    /**
     * Get sfDataSync
     *
     * @return \DateTime
     */
    public function getSfDataSync()
    {
        return $this->sfDataSync;
    }

    /**
     * Set sfCustoFinanceiroSync
     *
     * @param integer $sfCustoFinanceiroSync
     *
     * @return B2cPedped
     */
    public function setSfCustoFinanceiroSync($sfCustoFinanceiroSync)
    {
        $this->sfCustoFinanceiroSync = $sfCustoFinanceiroSync;

        return $this;
    }

    /**
     * Get sfCustoFinanceiroSync
     *
     * @return integer
     */
    public function getSfCustoFinanceiroSync()
    {
        return $this->sfCustoFinanceiroSync;
    }

    /**
     * Set assistencia
     *
     * @param integer $assistencia
     *
     * @return B2cPedped
     */
    public function setAssistencia($assistencia)
    {
        $this->assistencia = $assistencia;

        return $this;
    }

    /**
     * Get assistencia
     *
     * @return integer
     */
    public function getAssistencia()
    {
        return $this->assistencia;
    }

    /**
     * Set recompra
     *
     * @param integer $recompra
     *
     * @return B2cPedped
     */
    public function setRecompra($recompra)
    {
        $this->recompra = $recompra;

        return $this;
    }

    /**
     * Get recompra
     *
     * @return integer
     */
    public function getRecompra()
    {
        return $this->recompra;
    }

    /**
     * Set troca
     *
     * @param integer $troca
     *
     * @return B2cPedped
     */
    public function setTroca($troca)
    {
        $this->troca = $troca;

        return $this;
    }

    /**
     * Get troca
     *
     * @return integer
     */
    public function getTroca()
    {
        return $this->troca;
    }

    /**
     * Set pedNovaDataEntrega
     *
     * @param \DateTime $pedNovaDataEntrega
     *
     * @return B2cPedped
     */
    public function setPedNovaDataEntrega($pedNovaDataEntrega)
    {
        $this->pedNovaDataEntrega = $pedNovaDataEntrega;

        return $this;
    }

    /**
     * Get pedNovaDataEntrega
     *
     * @return \DateTime
     */
    public function getPedNovaDataEntrega()
    {
        return $this->pedNovaDataEntrega;
    }

    /**
     * @return int
     */
    public function getIdMarketplace()
    {
        return $this->idMarketplace;
    }

    /**
     * @param int $idMarketplace
     */
    public function setIdMarketplace($idMarketplace)
    {
        $this->idMarketplace = $idMarketplace;
    }

    /**
     * @return string
     */
    public function getTipoPedido()
    {
        return $this->tipoPedido;
    }

    /**
     * @param string $tipoPedido
     */
    public function setTipoPedido($tipoPedido)
    {
        $this->tipoPedido = $tipoPedido;
    }

    /**
     * Add product
     *
     * @param \AppBundle\Entity\B2cPedprd $product
     *
     * @return B2cPedped
     */
    public function addProduct(\AppBundle\Entity\B2cPedprd $product)
    {
        $this->products->add($product);

        return $this;
    }

    /**
     * Remove product
     *
     * @param \AppBundle\Entity\B2cPedprd $product
     */
    public function removeProduct(\AppBundle\Entity\B2cPedprd $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add status
     *
     * @param \AppBundle\Entity\B2cPedstatus $status
     *
     * @return B2cPedped
     */
    public function addStatus(\AppBundle\Entity\B2cPedstatus $status)
    {
        $this->status->add($status);

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param string $device
     */
    public function setDevice($device)
    {
        $this->device = $device;
    }

    /**
     * @return string
     */
    public function getRuleFreight()
    {
        return $this->ruleFreight;
    }

    /**
     * @param string $ruleFreight
     */
    public function setRuleFreight($ruleFreight)
    {
        $this->ruleFreight = $ruleFreight;
    }

    /**
     * @param $products
     * @return $this
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }
}
