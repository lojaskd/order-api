<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdprdPrecosAgressivos
 *
 * @ORM\Table(name="b2c_prdprd_precos_agressivos")
 * @ORM\Entity
 */
class B2cPrdprdPrecosAgressivos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPrdprd;

    /**
     * @var string
     *
     * @ORM\Column(name="preco", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $preco;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tipo_desconto", type="boolean", nullable=true)
     */
    private $tipoDesconto;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_desconto", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorDesconto;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_limite_desconto", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $tipoLimiteDesconto;

    /**
     * @var string
     *
     * @ORM\Column(name="limite_desconto", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $limiteDesconto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_cad", type="datetime", nullable=true)
     */
    private $dtaCad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_alt", type="datetime", nullable=true)
     */
    private $dtaAlt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_limite", type="datetime", nullable=true)
     */
    private $dtaLimite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_alt_desconto", type="datetime", nullable=true)
     */
    private $dtaAltDesconton;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_primeiro_desconto", type="datetime", nullable=true)
     */
    private $dtaPrimeiroDesconto;

    /**
     * @var \string
     *
     * @ORM\Column(name="usuario", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $usuario;

    /**
     * @var integer
     *
     * @ORM\Column(name="ativo", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ativo;

    /**
     * @param $idPrdprd
     * @return $this
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;
        return $this;
    }

    /**
     * Get idPrdprd
     *
     * @return integer
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * Set preco
     *
     * @param string $preco
     *
     * @return B2cPrdprdPrecosAgressivos
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;

        return $this;
    }

    /**
     * Get preco
     *
     * @return string
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * Set tipoDesconto
     *
     * @param boolean $tipoDesconto
     *
     * @return B2cPrdprdPrecosAgressivos
     */
    public function setTipoDesconto($tipoDesconto)
    {
        $this->tipoDesconto = $tipoDesconto;

        return $this;
    }

    /**
     * Get tipoDesconto
     *
     * @return boolean
     */
    public function getTipoDesconto()
    {
        return $this->tipoDesconto;
    }

    /**
     * Set valorDesconto
     *
     * @param string $valorDesconto
     *
     * @return B2cPrdprdPrecosAgressivos
     */
    public function setValorDesconto($valorDesconto)
    {
        $this->valorDesconto = $valorDesconto;

        return $this;
    }

    /**
     * Get valorDesconto
     *
     * @return string
     */
    public function getValorDesconto()
    {
        return $this->valorDesconto;
    }

    /**
     * Set tipoLimiteDesconto
     *
     * @param string $tipoLimiteDesconto
     *
     * @return B2cPrdprdPrecosAgressivos
     */
    public function setTipoLimiteDesconto($tipoLimiteDesconto)
    {
        $this->tipoLimiteDesconto = $tipoLimiteDesconto;

        return $this;
    }

    /**
     * Get tipoLimiteDesconto
     *
     * @return string
     */
    public function getTipoLimiteDesconto()
    {
        return $this->tipoLimiteDesconto;
    }

    /**
     * Set limiteDesconto
     *
     * @param string $limiteDesconto
     *
     * @return B2cPrdprdPrecosAgressivos
     */
    public function setLimiteDesconto($limiteDesconto)
    {
        $this->limiteDesconto = $limiteDesconto;

        return $this;
    }

    /**
     * Get limiteDesconto
     *
     * @return string
     */
    public function getLimiteDesconto()
    {
        return $this->limiteDesconto;
    }

    /**
     * Set dtaCad
     *
     * @param \DateTime $dtaCad
     *
     * @return B2cPrdprdPrecosAgressivos
     */
    public function setDtaCad($dtaCad)
    {
        $this->dtaCad = $dtaCad;

        return $this;
    }

    /**
     * Get dtaCad
     *
     * @return \DateTime
     */
    public function getDtaCad()
    {
        return $this->dtaCad;
    }

    /**
     * Set dtaAlt
     *
     * @param \DateTime $dtaAlt
     *
     * @return B2cPrdprdPrecosAgressivos
     */
    public function setDtaAlt($dtaAlt)
    {
        $this->dtaAlt = $dtaAlt;

        return $this;
    }

    /**
     * Get dtaAlt
     *
     * @return \DateTime
     */
    public function getDtaAlt()
    {
        return $this->dtaAlt;
    }

    /**
     * Set dtaLimite
     *
     * @param \DateTime $dtaLimite
     *
     * @return B2cPrdprdPrecosAgressivos
     */
    public function setDtaLimite($dtaLimite)
    {
        $this->dtaLimite = $dtaLimite;

        return $this;
    }

    /**
     * Get dtaLimite
     *
     * @return \DateTime
     */
    public function getDtaLimite()
    {
        return $this->dtaLimite;
    }

    /**
     * @return \DateTime
     */
    public function getDtaAltDesconton()
    {
        return $this->dtaAltDesconton;
    }

    /**
     * @param \DateTime $dtaAltDesconton
     */
    public function setDtaAltDesconton($dtaAltDesconton)
    {
        $this->dtaAltDesconton = $dtaAltDesconton;
    }

    /**
     * @return \DateTime
     */
    public function getDtaPrimeiroDesconto()
    {
        return $this->dtaPrimeiroDesconto;
    }

    /**
     * @param \DateTime $dtaPrimeiroDesconto
     */
    public function setDtaPrimeiroDesconto($dtaPrimeiroDesconto)
    {
        $this->dtaPrimeiroDesconto = $dtaPrimeiroDesconto;
    }

    /**
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param string $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param int $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

}
