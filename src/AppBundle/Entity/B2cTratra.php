<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cTratra
 *
 * @ORM\Table(name="b2c_tratra", indexes={@ORM\Index(name="id_regreg", columns={"id_regreg"}), @ORM\Index(name="padrao", columns={"padrao"}), @ORM\Index(name="idx_tratra", columns={"id_tratra", "nome_interno", "nome_publico"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cTratraRepository")
 */
class B2cTratra
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\B2cRegtransVal")
     * @ORM\JoinColumn(name="id_tratra", referencedColumnName="id_trans", nullable=true)
     */
    private $idTratra;

    /**
     * @var \AppBundle\Entity\B2cRegcep
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\B2cRegcep", inversedBy="transportadoras")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_regreg", referencedColumnName="id_reg", nullable=true)
     * })
     */
    private $idRegreg;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_interno", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nomeInterno;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_publico", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nomePublico;

    /**
     * @var boolean
     *
     * @ORM\Column(name="padrao", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $padrao;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_frete", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $tipoFrete;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_frete_fixo", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $valorFreteFixo;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_transportadora", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prazoTransportadora;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_frete_gratuito", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prazoFreteGratuito;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_frete_fixo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prazoFreteFixo;

    /**
     * @var float
     *
     * @ORM\Column(name="seguro", type="float", precision=3, scale=2, nullable=false, unique=false)
     */
    private $seguro;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $descricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_servico", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $tipoServico;

    /**
     * @var integer
     *
     * @ORM\Column(name="limitador_peso", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $limitadorPeso;

    /**
     * @var string
     *
     * @ORM\Column(name="cep_referencia", type="string", length=15, precision=0, scale=0, nullable=false, unique=false)
     */
    private $cepReferencia;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_filial", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idFilial;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_forfor", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idForfor;


    /**
     * @param $idTratra
     * @return $this
     */
    public function setIdTratra($idTratra)
    {
        $this->idTratra = $idTratra;
        return $this;
    }

    /**
     * Get idTratra
     *
     * @return integer
     */
    public function getIdTratra()
    {
        return $this->idTratra;
    }

    /**
     * Set idRegreg
     *
     * @param integer $idRegreg
     *
     * @return B2cTratra
     */
    public function setIdRegreg($idRegreg)
    {
        $this->idRegreg = $idRegreg;

        return $this;
    }

    /**
     * Get idRegreg
     *
     * @return integer
     */
    public function getIdRegreg()
    {
        return $this->idRegreg;
    }

    /**
     * Set nomeInterno
     *
     * @param string $nomeInterno
     *
     * @return B2cTratra
     */
    public function setNomeInterno($nomeInterno)
    {
        $this->nomeInterno = $nomeInterno;

        return $this;
    }

    /**
     * Get nomeInterno
     *
     * @return string
     */
    public function getNomeInterno()
    {
        return $this->nomeInterno;
    }

    /**
     * Set nomePublico
     *
     * @param string $nomePublico
     *
     * @return B2cTratra
     */
    public function setNomePublico($nomePublico)
    {
        $this->nomePublico = $nomePublico;

        return $this;
    }

    /**
     * Get nomePublico
     *
     * @return string
     */
    public function getNomePublico()
    {
        return $this->nomePublico;
    }

    /**
     * Set padrao
     *
     * @param boolean $padrao
     *
     * @return B2cTratra
     */
    public function setPadrao($padrao)
    {
        $this->padrao = $padrao;

        return $this;
    }

    /**
     * Get padrao
     *
     * @return boolean
     */
    public function getPadrao()
    {
        return $this->padrao;
    }

    /**
     * Set tipoFrete
     *
     * @param integer $tipoFrete
     *
     * @return B2cTratra
     */
    public function setTipoFrete($tipoFrete)
    {
        $this->tipoFrete = $tipoFrete;

        return $this;
    }

    /**
     * Get tipoFrete
     *
     * @return integer
     */
    public function getTipoFrete()
    {
        return $this->tipoFrete;
    }

    /**
     * Set valorFreteFixo
     *
     * @param float $valorFreteFixo
     *
     * @return B2cTratra
     */
    public function setValorFreteFixo($valorFreteFixo)
    {
        $this->valorFreteFixo = $valorFreteFixo;

        return $this;
    }

    /**
     * Get valorFreteFixo
     *
     * @return float
     */
    public function getValorFreteFixo()
    {
        return $this->valorFreteFixo;
    }

    /**
     * Set prazoTransportadora
     *
     * @param integer $prazoTransportadora
     *
     * @return B2cTratra
     */
    public function setPrazoTransportadora($prazoTransportadora)
    {
        $this->prazoTransportadora = $prazoTransportadora;

        return $this;
    }

    /**
     * Get prazoTransportadora
     *
     * @return integer
     */
    public function getPrazoTransportadora()
    {
        return $this->prazoTransportadora;
    }

    /**
     * Set prazoFreteGratuito
     *
     * @param integer $prazoFreteGratuito
     *
     * @return B2cTratra
     */
    public function setPrazoFreteGratuito($prazoFreteGratuito)
    {
        $this->prazoFreteGratuito = $prazoFreteGratuito;

        return $this;
    }

    /**
     * Get prazoFreteGratuito
     *
     * @return integer
     */
    public function getPrazoFreteGratuito()
    {
        return $this->prazoFreteGratuito;
    }

    /**
     * Set prazoFreteFixo
     *
     * @param integer $prazoFreteFixo
     *
     * @return B2cTratra
     */
    public function setPrazoFreteFixo($prazoFreteFixo)
    {
        $this->prazoFreteFixo = $prazoFreteFixo;

        return $this;
    }

    /**
     * Get prazoFreteFixo
     *
     * @return integer
     */
    public function getPrazoFreteFixo()
    {
        return $this->prazoFreteFixo;
    }

    /**
     * Set seguro
     *
     * @param float $seguro
     *
     * @return B2cTratra
     */
    public function setSeguro($seguro)
    {
        $this->seguro = $seguro;

        return $this;
    }

    /**
     * Get seguro
     *
     * @return float
     */
    public function getSeguro()
    {
        return $this->seguro;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     *
     * @return B2cTratra
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return B2cTratra
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set tipoServico
     *
     * @param integer $tipoServico
     *
     * @return B2cTratra
     */
    public function setTipoServico($tipoServico)
    {
        $this->tipoServico = $tipoServico;

        return $this;
    }

    /**
     * Get tipoServico
     *
     * @return integer
     */
    public function getTipoServico()
    {
        return $this->tipoServico;
    }

    /**
     * Set limitadorPeso
     *
     * @param integer $limitadorPeso
     *
     * @return B2cTratra
     */
    public function setLimitadorPeso($limitadorPeso)
    {
        $this->limitadorPeso = $limitadorPeso;

        return $this;
    }

    /**
     * Get limitadorPeso
     *
     * @return integer
     */
    public function getLimitadorPeso()
    {
        return $this->limitadorPeso;
    }

    /**
     * Set cepReferencia
     *
     * @param string $cepReferencia
     *
     * @return B2cTratra
     */
    public function setCepReferencia($cepReferencia)
    {
        $this->cepReferencia = $cepReferencia;

        return $this;
    }

    /**
     * Get cepReferencia
     *
     * @return string
     */
    public function getCepReferencia()
    {
        return $this->cepReferencia;
    }

    /**
     * Set idFilial
     *
     * @param integer $idFilial
     *
     * @return B2cTratra
     */
    public function setIdFilial($idFilial)
    {
        $this->idFilial = $idFilial;

        return $this;
    }

    /**
     * Get idFilial
     *
     * @return integer
     */
    public function getIdFilial()
    {
        return $this->idFilial;
    }

    /**
     * @return int
     */
    public function getIdForfor()
    {
        return $this->idForfor;
    }

    /**
     * @param int $idForfor
     */
    public function setIdForfor($idForfor)
    {
        $this->idForfor = $idForfor;
    }
}
