<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AttributeJson
 * @package AppBundle\Entity
 *
 * @ORM\Entity()
 */
class AttributeJson
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id_attr;

    /**
     * @var array
     *
     * @Assert\NotBlank(message="Value is required.")
     * @Assert\Type(
     *     type="array",
     *     message="The value of type {{ value }} is not a valid type (array)"
     * )
     *
     * @ORM\Column(type="array")
     * @Type("array")
     */
    private $value;


    /**
     * @return int
     */
    public function getIdAttr()
    {
        return $this->id_attr;
    }

    /**
     * @param int $id_attr
     * @return $this
     */
    public function setIdAttr($id_attr)
    {
        $this->id_attr = $id_attr;
        return $this;
    }

    /**
     * @return array
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param array $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
