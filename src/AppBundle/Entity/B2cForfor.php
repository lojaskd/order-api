<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cForfor
 *
 * @ORM\Table(name="b2c_forfor", indexes={@ORM\Index(name="nome", columns={"nome"}), @ORM\Index(name="codigoerp", columns={"codigoerp"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cForforRepository")
 */
class B2cForfor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="marca", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $marca;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prazo;

    /**
     * @var integer
     *
     * @ORM\Column(name="codigoerp", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $codigoerp;

    /**
     * @var string
     *
     * @ORM\Column(name="slug_fornecedor", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $slugFornecedor;

    /**
     * @var string
     *
     * @ORM\Column(name="representante", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $representante;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=2, precision=0, scale=0, nullable=true, unique=false)
     */
    private $estado;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_coleta", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prazoColeta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="reputacao", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $reputacao;

    /**
     * @var string
     *
     * @ORM\Column(name="razao_social", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $razaoSocial;

    /**
     * @var integer
     *
     * @ORM\Column(name="pn_sap", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pnSap;

    /**
     * @var string
     *
     * @ORM\Column(name="cnpj", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cnpj;

    /**
     * @var integer
     *
     * @ORM\Column(name="status_fornecedor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $statusFornecedor;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_pagamento_fornecedor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prazoPagamentoFornecedor;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cForfor
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set marca
     *
     * @param string $marca
     *
     * @return B2cForfor
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return string
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set prazo
     *
     * @param integer $prazo
     *
     * @return B2cForfor
     */
    public function setPrazo($prazo)
    {
        $this->prazo = $prazo;

        return $this;
    }

    /**
     * Get prazo
     *
     * @return integer
     */
    public function getPrazo()
    {
        return $this->prazo;
    }

    /**
     * Set codigoerp
     *
     * @param integer $codigoerp
     *
     * @return B2cForfor
     */
    public function setCodigoerp($codigoerp)
    {
        $this->codigoerp = $codigoerp;

        return $this;
    }

    /**
     * Get codigoerp
     *
     * @return integer
     */
    public function getCodigoerp()
    {
        return $this->codigoerp;
    }

    /**
     * Set slugFornecedor
     *
     * @param string $slugFornecedor
     *
     * @return B2cForfor
     */
    public function setSlugFornecedor($slugFornecedor)
    {
        $this->slugFornecedor = $slugFornecedor;

        return $this;
    }

    /**
     * Get slugFornecedor
     *
     * @return string
     */
    public function getSlugFornecedor()
    {
        return $this->slugFornecedor;
    }

    /**
     * Set representante
     *
     * @param string $representante
     *
     * @return B2cForfor
     */
    public function setRepresentante($representante)
    {
        $this->representante = $representante;

        return $this;
    }

    /**
     * Get representante
     *
     * @return string
     */
    public function getRepresentante()
    {
        return $this->representante;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     *
     * @return B2cForfor
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return B2cForfor
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set prazoColeta
     *
     * @param integer $prazoColeta
     *
     * @return B2cForfor
     */
    public function setPrazoColeta($prazoColeta)
    {
        $this->prazoColeta = $prazoColeta;

        return $this;
    }

    /**
     * Get prazoColeta
     *
     * @return integer
     */
    public function getPrazoColeta()
    {
        return $this->prazoColeta;
    }

    /**
     * Set reputacao
     *
     * @param boolean $reputacao
     *
     * @return B2cForfor
     */
    public function setReputacao($reputacao)
    {
        $this->reputacao = $reputacao;

        return $this;
    }

    /**
     * Get reputacao
     *
     * @return boolean
     */
    public function getReputacao()
    {
        return $this->reputacao;
    }

    /**
     * Set razaoSocial
     *
     * @param string $razaoSocial
     *
     * @return B2cForfor
     */
    public function setRazaoSocial($razaoSocial)
    {
        $this->razaoSocial = $razaoSocial;

        return $this;
    }

    /**
     * Get razaoSocial
     *
     * @return string
     */
    public function getRazaoSocial()
    {
        return $this->razaoSocial;
    }

    /**
     * Set pnSap
     *
     * @param integer $pnSap
     *
     * @return B2cForfor
     */
    public function setPnSap($pnSap)
    {
        $this->pnSap = $pnSap;

        return $this;
    }

    /**
     * Get pnSap
     *
     * @return integer
     */
    public function getPnSap()
    {
        return $this->pnSap;
    }

    /**
     * Set cnpj
     *
     * @param string $cnpj
     *
     * @return B2cForfor
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Get cnpj
     *
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Set statusFornecedor
     *
     * @param integer $statusFornecedor
     *
     * @return B2cForfor
     */
    public function setStatusFornecedor($statusFornecedor)
    {
        $this->statusFornecedor = $statusFornecedor;

        return $this;
    }

    /**
     * Get statusFornecedor
     *
     * @return integer
     */
    public function getStatusFornecedor()
    {
        return $this->statusFornecedor;
    }

    /**
     * Set prazoPagamentoFornecedor
     *
     * @param integer $prazoPagamentoFornecedor
     *
     * @return B2cForfor
     */
    public function setPrazoPagamentoFornecedor($prazoPagamentoFornecedor)
    {
        $this->prazoPagamentoFornecedor = $prazoPagamentoFornecedor;

        return $this;
    }

    /**
     * Get prazoPagamentoFornecedor
     *
     * @return integer
     */
    public function getPrazoPagamentoFornecedor()
    {
        return $this->prazoPagamentoFornecedor;
    }
}
