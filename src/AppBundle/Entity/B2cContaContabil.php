<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cContaContabil
 *
 * @ORM\Table(name="b2c_conta_contabil", indexes={@ORM\Index(name="id", columns={"id"})})
 * @ORM\Entity
 */
class B2cContaContabil
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $codigo;


    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="cardcode", type="string", length=30, precision=0, scale=0, nullable=false, unique=false)
     */
    private $cardcode;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getCardcode()
    {
        return $this->cardcode;
    }

    /**
     * @param string $cardcode
     */
    public function setCardcode($cardcode)
    {
        $this->cardcode = $cardcode;
    }

}
