<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cViewPrecos
 *
 * @ORM\Table(name="b2c_view_precos")
 * @ORM\Entity
 */
class B2cViewPrecos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_kit", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idKit;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_produto", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProduto;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $quantidade;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_real", type="decimal", precision=11, scale=2, nullable=false, unique=false)
     */
    private $precoReal;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_promocao", type="decimal", precision=11, scale=2, nullable=false, unique=false)
     */
    private $precoPromocao;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_avista", type="decimal", precision=11, scale=2, nullable=false, unique=false)
     */
    private $precoAvista;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_parcelado", type="decimal", precision=11, scale=2, nullable=false, unique=false)
     */
    private $precoParcelado;

    /**
     * @var integer
     *
     * @ORM\Column(name="qtde_parcelas", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $qtdeParcelas;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_atualizacao", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dataAtualizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="desconto_avista", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $descontoAvista;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_especial_promocao", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $precoEspecialPromocao;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_especial_avista", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $precoEspecialAvista;

    /**
     * @var integer
     *
     * @ORM\Column(name="preco_especial_kit", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $precoEspecialKit;

    /**
     * @var string
     *
     * @ORM\Column(name="qtde_parcelas_juros", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $qtdeParcelasJuros;

    /**
     * @var float
     *
     * @ORM\Column(name="taxa_parcelas_juros", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $taxaParcelasJuros;



    /**
     * Set idKit
     *
     * @param integer $idKit
     *
     * @return B2cViewPrecos
     */
    public function setIdKit($idKit)
    {
        $this->idKit = $idKit;

        return $this;
    }

    /**
     * Get idKit
     *
     * @return integer
     */
    public function getIdKit()
    {
        return $this->idKit;
    }

    /**
     * Set idProduto
     *
     * @param integer $idProduto
     *
     * @return B2cViewPrecos
     */
    public function setIdProduto($idProduto)
    {
        $this->idProduto = $idProduto;

        return $this;
    }

    /**
     * Get idProduto
     *
     * @return integer
     */
    public function getIdProduto()
    {
        return $this->idProduto;
    }

    /**
     * Set quantidade
     *
     * @param integer $quantidade
     *
     * @return B2cViewPrecos
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;

        return $this;
    }

    /**
     * Get quantidade
     *
     * @return integer
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * Set precoReal
     *
     * @param string $precoReal
     *
     * @return B2cViewPrecos
     */
    public function setPrecoReal($precoReal)
    {
        $this->precoReal = $precoReal;

        return $this;
    }

    /**
     * Get precoReal
     *
     * @return string
     */
    public function getPrecoReal()
    {
        return $this->precoReal;
    }

    /**
     * Set precoPromocao
     *
     * @param string $precoPromocao
     *
     * @return B2cViewPrecos
     */
    public function setPrecoPromocao($precoPromocao)
    {
        $this->precoPromocao = $precoPromocao;

        return $this;
    }

    /**
     * Get precoPromocao
     *
     * @return string
     */
    public function getPrecoPromocao()
    {
        return $this->precoPromocao;
    }

    /**
     * Set precoAvista
     *
     * @param string $precoAvista
     *
     * @return B2cViewPrecos
     */
    public function setPrecoAvista($precoAvista)
    {
        $this->precoAvista = $precoAvista;

        return $this;
    }

    /**
     * Get precoAvista
     *
     * @return string
     */
    public function getPrecoAvista()
    {
        return $this->precoAvista;
    }

    /**
     * Set precoParcelado
     *
     * @param string $precoParcelado
     *
     * @return B2cViewPrecos
     */
    public function setPrecoParcelado($precoParcelado)
    {
        $this->precoParcelado = $precoParcelado;

        return $this;
    }

    /**
     * Get precoParcelado
     *
     * @return string
     */
    public function getPrecoParcelado()
    {
        return $this->precoParcelado;
    }

    /**
     * Set qtdeParcelas
     *
     * @param integer $qtdeParcelas
     *
     * @return B2cViewPrecos
     */
    public function setQtdeParcelas($qtdeParcelas)
    {
        $this->qtdeParcelas = $qtdeParcelas;

        return $this;
    }

    /**
     * Get qtdeParcelas
     *
     * @return integer
     */
    public function getQtdeParcelas()
    {
        return $this->qtdeParcelas;
    }

    /**
     * Set dataAtualizacao
     *
     * @param \DateTime $dataAtualizacao
     *
     * @return B2cViewPrecos
     */
    public function setDataAtualizacao($dataAtualizacao)
    {
        $this->dataAtualizacao = $dataAtualizacao;

        return $this;
    }

    /**
     * Get dataAtualizacao
     *
     * @return \DateTime
     */
    public function getDataAtualizacao()
    {
        return $this->dataAtualizacao;
    }

    /**
     * Set descontoAvista
     *
     * @param string $descontoAvista
     *
     * @return B2cViewPrecos
     */
    public function setDescontoAvista($descontoAvista)
    {
        $this->descontoAvista = $descontoAvista;

        return $this;
    }

    /**
     * Get descontoAvista
     *
     * @return string
     */
    public function getDescontoAvista()
    {
        return $this->descontoAvista;
    }

    /**
     * Set precoEspecialPromocao
     *
     * @param string $precoEspecialPromocao
     *
     * @return B2cViewPrecos
     */
    public function setPrecoEspecialPromocao($precoEspecialPromocao)
    {
        $this->precoEspecialPromocao = $precoEspecialPromocao;

        return $this;
    }

    /**
     * Get precoEspecialPromocao
     *
     * @return string
     */
    public function getPrecoEspecialPromocao()
    {
        return $this->precoEspecialPromocao;
    }

    /**
     * Set precoEspecialAvista
     *
     * @param string $precoEspecialAvista
     *
     * @return B2cViewPrecos
     */
    public function setPrecoEspecialAvista($precoEspecialAvista)
    {
        $this->precoEspecialAvista = $precoEspecialAvista;

        return $this;
    }

    /**
     * Get precoEspecialAvista
     *
     * @return string
     */
    public function getPrecoEspecialAvista()
    {
        return $this->precoEspecialAvista;
    }

    /**
     * Set precoEspecialKit
     *
     * @param integer $precoEspecialKit
     *
     * @return B2cViewPrecos
     */
    public function setPrecoEspecialKit($precoEspecialKit)
    {
        $this->precoEspecialKit = $precoEspecialKit;

        return $this;
    }

    /**
     * Get precoEspecialKit
     *
     * @return integer
     */
    public function getPrecoEspecialKit()
    {
        return $this->precoEspecialKit;
    }

    /**
     * Set qtdeParcelasJuros
     *
     * @param string $qtdeParcelasJuros
     *
     * @return B2cViewPrecos
     */
    public function setQtdeParcelasJuros($qtdeParcelasJuros)
    {
        $this->qtdeParcelasJuros = $qtdeParcelasJuros;

        return $this;
    }

    /**
     * Get qtdeParcelasJuros
     *
     * @return string
     */
    public function getQtdeParcelasJuros()
    {
        return $this->qtdeParcelasJuros;
    }

    /**
     * Set taxaParcelasJuros
     *
     * @param float $taxaParcelasJuros
     *
     * @return B2cViewPrecos
     */
    public function setTaxaParcelasJuros($taxaParcelasJuros)
    {
        $this->taxaParcelasJuros = $taxaParcelasJuros;

        return $this;
    }

    /**
     * Get taxaParcelasJuros
     *
     * @return float
     */
    public function getTaxaParcelasJuros()
    {
        return $this->taxaParcelasJuros;
    }
}
