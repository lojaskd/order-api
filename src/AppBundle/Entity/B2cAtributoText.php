<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class B2cAtributoText
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="b2c_atributo_text", indexes={@ORM\Index(name="b2c_prdprd", columns={"b2c_prdprd"}), @ORM\Index(name="id_atributo", columns={"id_atributo"})})
 * @ORM\Entity
 */
class B2cAtributoText
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_atributo_valore", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAtributoValore;

    /**
     * @var string
     *
     * @ORM\Column(name="respostas", type="text", length=65535, nullable=true)
     */
    private $respostas;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_atributo", type="smallint", nullable=true)
     */
    private $idAtributo;

    /**
     * @var integer
     *
     * @ORM\Column(name="b2c_prdprd", type="integer", nullable=true)
     */
    private $b2cPrdprd;

    /**
     * @param $idAtributoValore
     * @return $this
     */
    public function setIdAtributoValore($idAtributoValore)
    {
        $this->idAtributoValore = $idAtributoValore;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAtributoValore()
    {
        return $this->idAtributoValore;
    }

    /**
     * @param $respostas
     * @return $this
     */
    public function setRespostas($respostas)
    {
        $this->respostas = $respostas;
        return $this;
    }

    /**
     * @return string
     */
    public function getRespostas()
    {
        return $this->respostas;
    }

    /**
     * @param $idAtributo
     * @return $this
     */
    public function setIdAtributo($idAtributo)
    {
        $this->idAtributo = $idAtributo;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAtributo()
    {
        return $this->idAtributo;
    }

    /**
     * @param $b2cPrdprd
     * @return $this
     */
    public function setB2cPrdprd($b2cPrdprd)
    {
        $this->b2cPrdprd = $b2cPrdprd;
        return $this;
    }

    /**
     * @return int
     */
    public function getB2cPrdprd()
    {
        return $this->b2cPrdprd;
    }
}
