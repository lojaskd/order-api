<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoPag
 *
 * @ORM\Table(name="b2c_promo_pag", indexes={@ORM\Index(name="id_promo", columns={"id_promo", "id_pagpag", "id_pagfor"}), @ORM\Index(name="id_pagfor", columns={"id_pagfor"}), @ORM\Index(name="id_pagpag", columns={"id_pagpag"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPromoPagRepository")
 */
class B2cPromoPag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     */
    private $idPromo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagpag", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     */
    private $idPagpag;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagfor", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idPagfor;



    /**
     * Set idPromo
     *
     * @param integer $idPromo
     *
     * @return B2cPromoPag
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;

        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set idPagpag
     *
     * @param integer $idPagpag
     *
     * @return B2cPromoPag
     */
    public function setIdPagpag($idPagpag)
    {
        $this->idPagpag = $idPagpag;

        return $this;
    }

    /**
     * Get idPagpag
     *
     * @return integer
     */
    public function getIdPagpag()
    {
        return $this->idPagpag;
    }

    /**
     * Set idPagfor
     *
     * @param integer $idPagfor
     *
     * @return B2cPromoPag
     */
    public function setIdPagfor($idPagfor)
    {
        $this->idPagfor = $idPagfor;

        return $this;
    }

    /**
     * Get idPagfor
     *
     * @return integer
     */
    public function getIdPagfor()
    {
        return $this->idPagfor;
    }
}
