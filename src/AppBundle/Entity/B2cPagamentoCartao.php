<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPagamentoCartao
 *
 * @ORM\Table(name="b2c_pagamento_cartao", indexes={@ORM\Index(name="chave_id_pedido", columns={"id_pedido"})})
 * @ORM\Entity
 */
class B2cPagamentoCartao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagamento_cartao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPagamentoCartao;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_pagamento", type="integer", nullable=false)
     */
    private $idTipoPagamento;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cliente", type="integer", nullable=false)
     */
    private $idCliente;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedido", type="integer", nullable=false)
     */
    private $idPedido;

    /**
     * @var string
     *
     * @ORM\Column(name="tid", type="string", length=40, nullable=true)
     */
    private $tid;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_autorizacao_pagamento", type="string", length=70, nullable=true)
     */
    private $codigoAutorizacaoPagamento;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_comprovante_venda", type="string", length=70, nullable=true)
     */
    private $numeroComprovanteVenda;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pagamento_capturado", type="boolean", nullable=false)
     */
    private $pagamentoCapturado = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="pago", type="boolean", nullable=false)
     */
    private $pago = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="pago_operadora", type="string", length=15, nullable=true)
     */
    private $pagoOperadora;

    /**
     * @var boolean
     *
     * @ORM\Column(name="numero_pagamento", type="boolean", nullable=false)
     */
    private $numeroPagamento = '1';

    /**
     * @var float
     *
     * @ORM\Column(name="valor_pagamento", type="float", precision=9, scale=2, nullable=false)
     */
    private $valorPagamento;

    /**
     * @var integer
     *
     * @ORM\Column(name="qtd_parcela", type="integer", nullable=false)
     */
    private $qtdParcela = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_pagamento", type="boolean", nullable=false)
     */
    private $statusPagamento = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criado", type="datetime", nullable=false)
     */
    private $dataCriado = 'CURRENT_TIMESTAMP';

    /**
     * @param $idPagamentoCartao
     * @return $this
     */
    public function setIdPagamentoCartao($idPagamentoCartao)
    {
        $this->idPagamentoCartao = $idPagamentoCartao;
        return $this;
    }

    /**
     * Get idPagamentoCartao
     *
     * @return integer
     */
    public function getIdPagamentoCartao()
    {
        return $this->idPagamentoCartao;
    }

    /**
     * Set idTipoPagamento
     *
     * @param integer $idTipoPagamento
     *
     * @return B2cPagamentoCartao
     */
    public function setIdTipoPagamento($idTipoPagamento)
    {
        $this->idTipoPagamento = $idTipoPagamento;

        return $this;
    }

    /**
     * Get idTipoPagamento
     *
     * @return integer
     */
    public function getIdTipoPagamento()
    {
        return $this->idTipoPagamento;
    }

    /**
     * Set idCliente
     *
     * @param integer $idCliente
     *
     * @return B2cPagamentoCartao
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get idCliente
     *
     * @return integer
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set idPedido
     *
     * @param integer $idPedido
     *
     * @return B2cPagamentoCartao
     */
    public function setIdPedido($idPedido)
    {
        $this->idPedido = $idPedido;

        return $this;
    }

    /**
     * Get idPedido
     *
     * @return integer
     */
    public function getIdPedido()
    {
        return $this->idPedido;
    }

    /**
     * Set tid
     *
     * @param string $tid
     *
     * @return B2cPagamentoCartao
     */
    public function setTid($tid)
    {
        $this->tid = $tid;

        return $this;
    }

    /**
     * Get tid
     *
     * @return string
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * Set codigoAutorizacaoPagamento
     *
     * @param string $codigoAutorizacaoPagamento
     *
     * @return B2cPagamentoCartao
     */
    public function setCodigoAutorizacaoPagamento($codigoAutorizacaoPagamento)
    {
        $this->codigoAutorizacaoPagamento = $codigoAutorizacaoPagamento;

        return $this;
    }

    /**
     * Get codigoAutorizacaoPagamento
     *
     * @return string
     */
    public function getCodigoAutorizacaoPagamento()
    {
        return $this->codigoAutorizacaoPagamento;
    }

    /**
     * Set numeroComprovanteVenda
     *
     * @param string $numeroComprovanteVenda
     *
     * @return B2cPagamentoCartao
     */
    public function setNumeroComprovanteVenda($numeroComprovanteVenda)
    {
        $this->numeroComprovanteVenda = $numeroComprovanteVenda;

        return $this;
    }

    /**
     * Get numeroComprovanteVenda
     *
     * @return string
     */
    public function getNumeroComprovanteVenda()
    {
        return $this->numeroComprovanteVenda;
    }

    /**
     * Set pagamentoCapturado
     *
     * @param boolean $pagamentoCapturado
     *
     * @return B2cPagamentoCartao
     */
    public function setPagamentoCapturado($pagamentoCapturado)
    {
        $this->pagamentoCapturado = $pagamentoCapturado;

        return $this;
    }

    /**
     * Get pagamentoCapturado
     *
     * @return boolean
     */
    public function getPagamentoCapturado()
    {
        return $this->pagamentoCapturado;
    }

    /**
     * Set pago
     *
     * @param boolean $pago
     *
     * @return B2cPagamentoCartao
     */
    public function setPago($pago)
    {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return boolean
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * Set pagoOperadora
     *
     * @param string $pagoOperadora
     *
     * @return B2cPagamentoCartao
     */
    public function setPagoOperadora($pagoOperadora)
    {
        $this->pagoOperadora = $pagoOperadora;

        return $this;
    }

    /**
     * Get pagoOperadora
     *
     * @return string
     */
    public function getPagoOperadora()
    {
        return $this->pagoOperadora;
    }

    /**
     * Set numeroPagamento
     *
     * @param boolean $numeroPagamento
     *
     * @return B2cPagamentoCartao
     */
    public function setNumeroPagamento($numeroPagamento)
    {
        $this->numeroPagamento = $numeroPagamento;

        return $this;
    }

    /**
     * Get numeroPagamento
     *
     * @return boolean
     */
    public function getNumeroPagamento()
    {
        return $this->numeroPagamento;
    }

    /**
     * Set valorPagamento
     *
     * @param float $valorPagamento
     *
     * @return B2cPagamentoCartao
     */
    public function setValorPagamento($valorPagamento)
    {
        $this->valorPagamento = $valorPagamento;

        return $this;
    }

    /**
     * Get valorPagamento
     *
     * @return float
     */
    public function getValorPagamento()
    {
        return $this->valorPagamento;
    }

    /**
     * Set qtdParcela
     *
     * @param integer $qtdParcela
     *
     * @return B2cPagamentoCartao
     */
    public function setQtdParcela($qtdParcela)
    {
        $this->qtdParcela = $qtdParcela;

        return $this;
    }

    /**
     * Get qtdParcela
     *
     * @return integer
     */
    public function getQtdParcela()
    {
        return $this->qtdParcela;
    }

    /**
     * Set statusPagamento
     *
     * @param boolean $statusPagamento
     *
     * @return B2cPagamentoCartao
     */
    public function setStatusPagamento($statusPagamento)
    {
        $this->statusPagamento = $statusPagamento;

        return $this;
    }

    /**
     * Get statusPagamento
     *
     * @return boolean
     */
    public function getStatusPagamento()
    {
        return $this->statusPagamento;
    }

    /**
     * Set dataCriado
     *
     * @param \DateTime $dataCriado
     *
     * @return B2cPagamentoCartao
     */
    public function setDataCriado($dataCriado)
    {
        $this->dataCriado = $dataCriado;

        return $this;
    }

    /**
     * Get dataCriado
     *
     * @return \DateTime
     */
    public function getDataCriado()
    {
        return $this->dataCriado;
    }
}
