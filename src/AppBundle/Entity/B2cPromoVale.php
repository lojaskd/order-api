<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoVale
 *
 * @ORM\Table(name="b2c_promo_vale", uniqueConstraints={@ORM\UniqueConstraint(name="codigo", columns={"codigo", "id_promo", "situacao"}), @ORM\UniqueConstraint(name="codigo_UNIQUE", columns={"codigo"})}, indexes={@ORM\Index(name="id_promo", columns={"id_promo"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPromoValeRepository")
 */
class B2cPromoVale
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_vale", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idVale;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idPromo;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="situacao", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $situacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="vale_utilizado", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $valeUtilizado;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_colaborador", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isColaborador;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tipo;

    /**
     * @param $idVale
     * @return $this
     */
    public function setIdVale($idVale)
    {
        $this->idVale = $idVale;
        return $this;
    }

    /**
     * Get idVale
     *
     * @return integer
     */
    public function getIdVale()
    {
        return $this->idVale;
    }

    /**
     * Set idPromo
     *
     * @param integer $idPromo
     *
     * @return B2cPromoVale
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;

        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return B2cPromoVale
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set situacao
     *
     * @param string $situacao
     *
     * @return B2cPromoVale
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;

        return $this;
    }

    /**
     * Get situacao
     *
     * @return string
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * Set valeUtilizado
     *
     * @param integer $valeUtilizado
     *
     * @return B2cPromoVale
     */
    public function setValeUtilizado($valeUtilizado)
    {
        $this->valeUtilizado = $valeUtilizado;

        return $this;
    }

    /**
     * Get valeUtilizado
     *
     * @return integer
     */
    public function getValeUtilizado()
    {
        return $this->valeUtilizado;
    }

    /**
     * Set isColaborador
     *
     * @param boolean $isColaborador
     *
     * @return B2cPromoVale
     */
    public function setIsColaborador($isColaborador)
    {
        $this->isColaborador = $isColaborador;

        return $this;
    }

    /**
     * Get isColaborador
     *
     * @return boolean
     */
    public function getIsColaborador()
    {
        return $this->isColaborador;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     *
     * @return B2cPromoVale
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
