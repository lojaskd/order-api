<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class B2cAtributoTipoValor
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="b2c_atributo_tipo_valor")
 * @ORM\Entity
 */
class B2cAtributoTipoValor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_atributo_tipo_valor", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAtributoTipoValor;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=45, nullable=false)
     */
    private $descricao;

    /**
     * @param $idAtributoTipoValor
     * @return $this
     */
    public function setIdAtributoTipoValor($idAtributoTipoValor)
    {
        $this->idAtributoTipoValor = $idAtributoTipoValor;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAtributoTipoValor()
    {
        return $this->idAtributoTipoValor;
    }

    /**
     * @param $descricao
     * @return $this
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }
}
