<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdlog
 *
 * @ORM\Table(name="b2c_prdlog", indexes={@ORM\Index(name="id_clicli", columns={"id_clicli", "carrinho", "data_log"})})
 * @ORM\Entity
 */
class B2cPrdlog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdlog", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPrdlog;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_clicli", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idClicli;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idPrdprd;

    /**
     * @var integer
     *
     * @ORM\Column(name="matriz", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $matriz;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_nome", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $prdNome;

    /**
     * @var boolean
     *
     * @ORM\Column(name="carrinho", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $carrinho;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_log", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dataLog;

    /**
     * @param $idPrdlog
     * @return $this
     */
    public function setIdPrdlog($idPrdlog)
    {
        $this->idPrdlog = $idPrdlog;
        return $this;
    }

    /**
     * Get idPrdlog
     *
     * @return integer
     */
    public function getIdPrdlog()
    {
        return $this->idPrdlog;
    }

    /**
     * Set idClicli
     *
     * @param integer $idClicli
     *
     * @return B2cPrdlog
     */
    public function setIdClicli($idClicli)
    {
        $this->idClicli = $idClicli;

        return $this;
    }

    /**
     * Get idClicli
     *
     * @return integer
     */
    public function getIdClicli()
    {
        return $this->idClicli;
    }

    /**
     * Set idPrdprd
     *
     * @param integer $idPrdprd
     *
     * @return B2cPrdlog
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;

        return $this;
    }

    /**
     * Get idPrdprd
     *
     * @return integer
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * Set matriz
     *
     * @param integer $matriz
     *
     * @return B2cPrdlog
     */
    public function setMatriz($matriz)
    {
        $this->matriz = $matriz;

        return $this;
    }

    /**
     * Get matriz
     *
     * @return integer
     */
    public function getMatriz()
    {
        return $this->matriz;
    }

    /**
     * Set prdNome
     *
     * @param string $prdNome
     *
     * @return B2cPrdlog
     */
    public function setPrdNome($prdNome)
    {
        $this->prdNome = $prdNome;

        return $this;
    }

    /**
     * Get prdNome
     *
     * @return string
     */
    public function getPrdNome()
    {
        return $this->prdNome;
    }

    /**
     * Set carrinho
     *
     * @param boolean $carrinho
     *
     * @return B2cPrdlog
     */
    public function setCarrinho($carrinho)
    {
        $this->carrinho = $carrinho;

        return $this;
    }

    /**
     * Get carrinho
     *
     * @return boolean
     */
    public function getCarrinho()
    {
        return $this->carrinho;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return B2cPrdlog
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set dataLog
     *
     * @param \DateTime $dataLog
     *
     * @return B2cPrdlog
     */
    public function setDataLog($dataLog)
    {
        $this->dataLog = $dataLog;

        return $this;
    }

    /**
     * Get dataLog
     *
     * @return \DateTime
     */
    public function getDataLog()
    {
        return $this->dataLog;
    }
}
