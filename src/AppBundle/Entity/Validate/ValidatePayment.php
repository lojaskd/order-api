<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidatePayment
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaymentRepository")
 */
class ValidatePayment
{

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="The payment id is required.")
     * @Assert\NotNull(message="The payment id cannot be null.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of product ID {{ value }} is not a valid type (integer)"
     * )
     * @Assert\GreaterThan(
     *     value = 0,
     *     message="ID value should be greater than 0"
     * )
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var float
     *
     * @Assert\NotBlank(message="The payment amount is required.")
     * @Assert\Type(
     *     type="float",
     *     message="The value of payment amount {{ value }} is not a valid type (float)"
     * )
     * @Assert\GreaterThanOrEqual(
     *     value = 0,
     *     message="Payment amount value should be greater or equal than 0"
     * )
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $amount;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="Instalment is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of instalment {{ value }} is not a valid type (integer)"
     * )
     * @Assert\GreaterThan(
     *     value = 0,
     *     message="Instalment should be greater than 0"
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $instalment;

    /**
     * @var float
     *
     * @Assert\NotBlank(message="Instalment value is required.")
     * @Assert\Type(
     *     type="float",
     *     message="The value of instalment {{ value }} is not a valid type (float)"
     * )
     * @Assert\GreaterThanOrEqual(
     *     value = 0,
     *     message="Instalment value should be greater or equal than 0"
     * )
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $instalment_value;

    /**
     * @var \Datetime
     *
     * @Assert\NotBlank(message="Billet deadline value is required.")
     * @Assert\Date()
     *
     * @ORM\Column(type="datetime")
     * @Type("datetime")
     */
    private $billet_deadline;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getInstalment()
    {
        return $this->instalment;
    }

    /**
     * @param int $instalment
     */
    public function setInstalment($instalment)
    {
        $this->instalment = $instalment;
    }

    /**
     * @return float
     */
    public function getInstalmentValue()
    {
        return $this->instalment_value;
    }

    /**
     * @param float $instalment_value
     */
    public function setInstalmentValue($instalment_value)
    {
        $this->instalment_value = $instalment_value;
    }

    /**
     * @return \Datetime
     */
    public function getBilletDeadline()
    {
        return $this->billet_deadline;
    }

    /**
     * @param \Datetime $billet_deadline
     */
    public function setBilletDeadline($billet_deadline)
    {
        $this->billet_deadline = $billet_deadline;
    }

}