<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductDate
 *
 * @ORM\Entity()
 */
class ValidateInvoiceDate
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $invoiceId;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Type is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of type {{ value }} is not a valid type (string)"
     * )
     * @Assert\Choice(choices = {"shippingDate", "deliveryDate", "supplierDeliveryDate"}, message = "Choose a valid record type")
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $type;

    /**
     * @var \Datetime
     * @Assert\NotBlank(message="Date is required.")
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of date {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $date;

    /**
     * @var string
     * @Assert\NotBlank(message="Clerk is required.")
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return \Datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \Datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

}
