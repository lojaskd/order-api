<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateProductDate
 *
 * @ORM\Entity()
 */
class ValidateProductDate
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var array
     *
     * @Assert\Valid
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\Validate\ValidateProductDateProduct>")
     */
    private $products;

    /**
     * @var array
     *
     * @Assert\NotBlank(message="The dateTypes field is required.")
     * @Assert\Valid
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\Validate\ValidateProductDateType>")
     */
    private $dateTypes;

    /**
     * @var string
     * @Assert\NotBlank(message="Clerk is required.")
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Assert\Type(
     *     type="boolean",
     *     message="The value of predict {{ value }} is not a valid type (boolean)"
     * )
     * @Type("boolean")
     */
    private $predict;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return array
     */
    public function getDateTypes()
    {
        return $this->dateTypes;
    }

    /**
     * @param ValidateProductDateType $dateType
     */
    public function addDateType(ValidateProductDateType $dateType)
    {
        $this->dateTypes[] = $dateType;
    }

    /**
     * @param array $dateTypes
     */
    public function setDateTypes($dateTypes)
    {
        $this->dateTypes = $dateTypes;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @param ValidateProductDateProduct $product
     */
    public function addProduct(ValidateProductDateProduct $product)
    {
        $this->products[] = $product;
    }

    /**
     * @return array $products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return boolean
     */
    public function isPredict()
    {
        return $this->predict;
    }

    /**
     * @param boolean $predict
     */
    public function setPredict($predict)
    {
        $this->predict = $predict;
    }

}
