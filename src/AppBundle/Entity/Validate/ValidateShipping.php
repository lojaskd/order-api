<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateShipping
 *
 * @ORM\Entity()
 */
class ValidateShipping
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="The shipping type is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of shipping type {{ value }} is not a valid type (integer)"
     * )
     * @Assert\Choice(choices = {1, 2, 3}, message = "Choose a valid shipping type - 1 (free), 2 (fixed) or 3 (shipping company).")
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $type;

    /**
     * @var float
     *
     * @Assert\NotBlank(message="The shipping price is required.")
     * @Assert\NotNull(message="The shipping price cannot be null.")
     * @Assert\Type(
     *     type="float",
     *     message="The value of shipping price {{ value }} is not a valid type (float)"
     * )
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $price;


    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

}
