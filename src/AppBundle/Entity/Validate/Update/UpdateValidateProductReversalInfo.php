<?php

namespace AppBundle\Entity\Validate\Update;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UpdateValidateProductStatus
 *
 * @ORM\Entity()
 */
class UpdateValidateProductReversalInfo
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var string
     * @Assert\NotBlank(message="Clerk is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of clerk {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var integer
     *
     * @Assert\Type(
     *     type="integer",
     *     message="The value of type reversal {{ value }} is not a valid type (integer)"
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $typeReversal;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of operator {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $operator;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of transaction {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $transaction;

    /**
     * @var array
     *
     * @Assert\Valid
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\Validate\Update\UpdateValidateProductStatusProduct>")
     */
    private $products;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return int
     */
    public function getTypeReversal() {
        return $this->typeReversal;
    }

    /**
     * @param int $typeReversal
     */
    public function setTypeReversal($typeReversal) {
        $this->typeReversal = $typeReversal;
    }

    /**
     * @return string
     */
    public function getOperator() {
        return $this->operator;
    }

    /**
     * @param string $operator
     */
    public function setOperator($operator) {
        $this->operator = $operator;
    }

    /**
     * @return string
     */
    public function getTransaction() {
        return $this->transaction;
    }

    /**
     * @param string $transaction
     */
    public function setTransaction($transaction) {
        $this->transaction = $transaction;
    }

    /**
     * @param UpdateValidateProductStatusProduct $product
     */
    public function addProduct(UpdateValidateProductStatusProduct $product)
    {
        $this->products[] = $product;
    }

    /**
     * @return array $products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

}
