<?php

namespace AppBundle\Entity\Validate\Update;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UpdateNewDeliveryDate
 */
class UpdateNewDeliveryDate
{

    /**
     * @var \Datetime
     *
     * @Assert\NotBlank(message="O campo nova data de entrega não pode ser vazio.")
     * @Assert\Date()
     *
     * @Type("DateTime<'Y-m-d'>")
     */
    private $newDeliveryDate;

    /**
     * @var boolean
     *
     * @Assert\NotBlank(message="O campo de aceite não pode ser vazio.")
     * @Assert\Type(
     *     type="boolean",
     *     message="O valor {{ value }} não é um {{ type }}."
     * )
     *
     * @Type("boolean")
     */
    private $accept;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="O valor {{ value }} não é um {{ type }}."
     * )
     *
     * @Type("string")
     */
    private $user;


    /**
     * @return \Datetime
     */
    public function getNewDeliveryDate()
    {
        return $this->newDeliveryDate;
    }

    /**
     * @param \Datetime $newDeliveryDate
     */
    public function setNewDeliveryDate($newDeliveryDate)
    {
        $this->newDeliveryDate = $newDeliveryDate;
    }

    /**
     * @return boolean
     */
    public function isAccept()
    {
        return $this->accept;
    }

    /**
     * @param boolean $accept
     */
    public function setAccept($accept)
    {
        $this->accept = $accept;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}
