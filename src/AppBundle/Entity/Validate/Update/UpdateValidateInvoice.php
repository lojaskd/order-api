<?php

namespace AppBundle\Entity\Validate\Update;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateInvoice
 *
 * @ORM\Entity()
 */
class UpdateValidateInvoice
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $invoiceId;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of key {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $key;

    /**
     * @var \Datetime
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of dateKey {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $dateKey;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="The value of returned {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $marketplaceIntegration;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of  Path XML {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $pathXml;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of Path PDF {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $pathPdf;

    /**
     * @var \Datetime
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of returnedDate {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $returnedDate;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="The value of returned {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $returned;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;


    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of shipperName {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $shipperName;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of shipperDoc {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $shipperDoc;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of hub {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $hub;


    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return \Datetime
     */
    function getReturnedDate()
    {
        return $this->returnedDate;
    }

    /**
     * @param \Datetime $returnedDate
     */
    function setReturnedDate(\Datetime $returnedDate)
    {
        $this->returnedDate = $returnedDate;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return boolean
     */
    function isReturned()
    {
        return $this->returned;
    }

    /**
     * @param boolean $returned
     */
    function setReturned($returned)
    {
        $this->returned = $returned;
    }

    /**
     * @return \Datetime
     */
    public function getDateKey()
    {
        return $this->dateKey;
    }

    /**
     * @param \Datetime $dateKey
     */
    public function setDateKey($dateKey)
    {
        $this->dateKey = $dateKey;
    }

    /**
     * @return boolean
     */
    public function isMarketplaceIntegration()
    {
        return $this->marketplaceIntegration;
    }

    /**
     * @param boolean $marketplaceIntegration
     */
    public function setMarketplaceIntegration($marketplaceIntegration)
    {
        $this->marketplaceIntegration = $marketplaceIntegration;
    }

    /**
     * @return string
     */
    public function getPathXml()
    {
        return $this->pathXml;
    }

    /**
     * @param string $pathXml
     */
    public function setPathXml($pathXml)
    {
        $this->pathXml = $pathXml;
    }

    /**
     * @return string
     */
    public function getPathPdf()
    {
        return $this->pathPdf;
    }

    /**
     * @param string $pathPdf
     */
    public function setPathPdf($pathPdf)
    {
        $this->pathPdf = $pathPdf;
    }

    /**
     * @return string
     */
    public function getShipperName()
    {
        return $this->shipperName;
    }

    /**
     * @param string $shipperName
     */
    public function setShipperName($shipperName)
    {
        $this->shipperName = $shipperName;
    }

    /**
     * @return string
     */
    public function getShipperDoc()
    {
        return $this->shipperDoc;
    }

    /**
     * @param string $shipperDoc
     */
    public function setShipperDoc($shipperDoc)
    {
        $this->shipperDoc = $shipperDoc;
    }

    /**
     * @return string
     */
    public function getHub()
    {
        return $this->hub;
    }

    /**
     * @param string $hub
     */
    public function setHub($hub)
    {
        $this->hub = $hub;
    }

}
