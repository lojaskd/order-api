<?php

namespace AppBundle\Entity\Validate\Update;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateProviderBillingDate
 */
class ValidateProviderBillingDate
{

    /**
     * @var integer
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     * @Type("integer")
     */
    private $purchaseOrder;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Type is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of type {{ value }} is not a valid type (string)"
     * )
     * @Assert\Choice(choices = {"supplierBillingDate"}, message = "Choose a valid record type")
     * @Type("string")
     */
    private $type;

    /**
     * @var string
     * @Assert\NotBlank(message="Clerk is required.")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var \Datetime
     * @Assert\NotBlank(message="Date is required.")
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of date {{ value }} is not a valid type (date)"
     * )
     *
     * @Type("DateTime<'Y-m-d'>")
     */
    private $date;


    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * @param int $purchaseOrder
     */
    public function setPurchaseOrder($purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return \Datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \Datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
}
