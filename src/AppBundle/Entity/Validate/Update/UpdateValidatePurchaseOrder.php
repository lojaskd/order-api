<?php

namespace AppBundle\Entity\Validate\Update;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UpdateValidatePurchaseOrder
 *
 */
class UpdateValidatePurchaseOrder
{
    /**
     * @var integer
     *
     * @Assert\NotBlank(message="purchase_order is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of purchase Order {{ value }} is not a valid type (integer)"
     * )
     *
     * @Type("integer")
     */
    private $purchaseOrder;

    /**
     * @var array
     * @Assert\NotBlank(message="orders is required.")
     * @Assert\Type(
     *     type="array",
     *     message="The value of  Orders {{ value }} is not a valid type (array)"
     * )
     *
     * @Type("array")
     */
    private $orders;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of  Path XML {{ value }} is not a valid type (string)"
     * )
     *
     * @Type("string")
     */
    private $pathXml;

    /**
     * @return int
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * @param int $purchaseOrder
     */
    public function setPurchaseOrder($purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    /**
     * @return array
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param array $orders
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
    }

    /**
     * @return string
     */
    public function getPathXml()
    {
        return $this->pathXml;
    }

    /**
     * @param string $pathXml
     */
    public function setPathXml($pathXml)
    {
        $this->pathXml = $pathXml;
    }
}
