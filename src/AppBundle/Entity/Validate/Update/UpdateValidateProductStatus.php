<?php

namespace AppBundle\Entity\Validate\Update;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UpdateValidateProductStatus
 *
 * @ORM\Entity()
 */
class UpdateValidateProductStatus
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @Assert\Type(
     *     type="integer",
     *     message="The value of status {{ value }} is not a valid type (integer)"
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $status;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="O valor {{ value }} não é um {{ type }}."
     * )
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $cancelled;

    /**
     * @var string
     * @Assert\NotBlank(message="Clerk is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of clerk {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of ticket {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ticket;

    /**
     * @var array
     *
     * @Assert\Valid
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\Validate\Update\UpdateValidateProductStatusProduct>")
     */
    private $products;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of ocrCode {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ocrCode;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of cancellationIndicator {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $cancellationIndicator;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of ticketType {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ticketType;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of hub {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $hub;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of shipper {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $shipper;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of invoice {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $invoice;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of productCauser {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $productCauser;

    /**
     * @var integer
     *
     * @Assert\Type(
     *     type="integer",
     *     message="The value of refund form {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $refundForm;


    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return boolean
     */
    public function isCancelled()
    {
        return $this->cancelled;
    }

    /**
     * @param boolean $cancelled
     */
    public function setCancelled($cancelled)
    {
        $this->cancelled = $cancelled;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param string $ticket
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * @param UpdateValidateProductStatusProduct $product
     */
    public function addProduct(UpdateValidateProductStatusProduct $product)
    {
        $this->products[] = $product;
    }

    /**
     * @return array $products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return string
     */
    public function getOcrCode()
    {
        return $this->ocrCode;
    }

    /**
     * @param string $ocrCode
     */
    public function setOcrCode($ocrCode)
    {
        $this->ocrCode = $ocrCode;
    }

    /**
     * @return string
     */
    public function getCancellationIndicator()
    {
        return $this->cancellationIndicator;
    }

    /**
     * @param string $cancellationIndicator
     */
    public function setCancellationIndicator($cancellationIndicator)
    {
        $this->cancellationIndicator = $cancellationIndicator;
    }

    /**
     * @return string
     */
    public function getTicketType()
    {
        return $this->ticketType;
    }

    /**
     * @param string $ticketType
     */
    public function setTicketType($ticketType)
    {
        $this->ticketType = $ticketType;
    }

    /**
     * @return string
     */
    public function getHub()
    {
        return $this->hub;
    }

    /**
     * @param string $hub
     */
    public function setHub($hub)
    {
        $this->hub = $hub;
    }

    /**
     * @return string
     */
    public function getShipper()
    {
        return $this->shipper;
    }

    /**
     * @param string $shipper
     */
    public function setShipper($shipper)
    {
        $this->shipper = $shipper;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return string
     */
    public function getProductCauser()
    {
        return $this->productCauser;
    }

    /**
     * @param string $productCauser
     */
    public function setProductCauser($productCauser)
    {
        $this->productCauser = $productCauser;
    }

    /**
     * @return int
     */
    public function getRefundForm()
    {
        return $this->refundForm;
    }

    /**
     * @param int $refundForm
     */
    public function setRefundForm($refundForm)
    {
        $this->refundForm = $refundForm;
    }
}
