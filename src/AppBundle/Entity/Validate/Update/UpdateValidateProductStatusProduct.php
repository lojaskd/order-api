<?php

namespace AppBundle\Entity\Validate\Update;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UpdateValidateProductStatusProduct
 *
 * @ORM\Entity()
 */
class UpdateValidateProductStatusProduct
{

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @ORM\Id
     * @Assert\Type(
     *     type="string",
     *     message="The value of string {{ value }} is not a valid type (string)"
     * )
     * @Type("string")
     */
    private $id;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
