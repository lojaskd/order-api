<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateCustomer
 *
 * @ORM\Entity()
 */
class ValidateCustomer
{

    /**
     * @var integer
     *
     * @Assert\Type(
     *     type="integer",
     *     message="The value of customer ID {{ value }} is not a valid type (integer)"
     * )
     * @Assert\GreaterThanOrEqual(
     *     value = 0
     * )
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The customer email is required.")
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The customer CPF is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(
     *      min = 11,
     *      max = 18,
     *      minMessage = "Your CPF must be at least {{ limit }} characters long",
     *      maxMessage = "Your CPF cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string", nullable=false)
     * @Type("string")
     */
    private $cpf;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The customer first name is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 100,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string", nullable=false)
     * @Type("string")
     */
    private $first_name;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The customer last name is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 100,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string", nullable=false)
     * @Type("string")
     */
    private $last_name;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $accepts_marketing;

    /**
     * @var \Datetime
     *
     * @Assert\Type(
     *     type="datetime",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $birthday;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="string", nullable=false)
     * @Type("string")
     */
    private $type_register;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="string", nullable=false)
     * @Type("string")
     */
    private $rg_ie;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Choice(choices = {"F", "J"}, message = "Choose a valid person_type record type")
     *
     * @ORM\Column(type="string", nullable=false)
     * @Type("string")
     */
    private $person_type;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Choice(choices = {"M", "F"}, message = "Choose a valid genre record type")
     *
     * @ORM\Column(type="string", nullable=false)
     * @Type("string")
     */
    private $genre;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="string", nullable=false)
     * @Type("string")
     */
    private $ip;

    /**
     * @var string
     *
     * @Type("string")
     */
    private $password;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param string $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return boolean
     */
    public function isAcceptsMarketing()
    {
        return $this->accepts_marketing;
    }

    /**
     * @param boolean $accepts_marketing
     */
    public function setAcceptsMarketing($accepts_marketing)
    {
        $this->accepts_marketing = $accepts_marketing;
    }

    /**
     * @return \Datetime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \Datetime $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getTypeRegister()
    {
        return $this->type_register;
    }

    /**
     * @param string $type_register
     */
    public function setTypeRegister($type_register)
    {
        $this->type_register = $type_register;
    }

    /**
     * @return string
     */
    public function getRgIe()
    {
        return $this->rg_ie;
    }

    /**
     * @param string $rgIe
     */
    public function setRgIe($rgIe)
    {
        $this->rg_ie = $rgIe;
    }

    /**
     * @return string
     */
    public function getPersonType()
    {
        return $this->person_type;
    }

    /**
     * @param string $personType
     */
    public function setPersonType($personType)
    {
        $this->person_type = $personType;
    }

    /**
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param string $genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

}
