<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateOrigin
 *
 * @ORM\Entity()
 */
class ValidateOrigin
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of source origin {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $source;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of medium origin {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $medium;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of content origin {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $content;

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getMedium()
    {
        return $this->medium;
    }

    /**
     * @param string $medium
     */
    public function setMedium($medium)
    {
        $this->medium = $medium;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

}
