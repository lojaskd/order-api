<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateProductDateType
 *
 * @ORM\Entity()
 */
class ValidateProductDateType
{

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Type is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of type {{ value }} is not a valid type (string)"
     * )
     * @Assert\Choice(choices = {"approvalDate", "releaseDate", "supplierBillingDate", "supplierInvoiceDate", "supplierDeliveryDate", "shippingDate", "invoiceDate", "deliveryDate"}, message = "Choose a valid record type")
     *
     * @ORM\Column(type="string")
     * @ORM\Id
     * @Type("string")
     */
    private $type;

    /**
     * @var \Datetime
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of date {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $date;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return \Datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \Datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

}
