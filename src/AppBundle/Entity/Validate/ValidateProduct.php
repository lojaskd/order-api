<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateProduct
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 */
class ValidateProduct
{

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="The product id is required.")
     * @Assert\NotNull(message="The product id cannot be null.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of product ID {{ value }} is not a valid type (integer)"
     * )
     * @Assert\GreaterThan(
     *     value = 0
     * )
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="The kit id is required.")
     * @Assert\NotNull(message="The kit id cannot be null.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of kit ID {{ value }} is not a valid type (integer)"
     * )
     * @Assert\GreaterThanOrEqual(
     *     value = 0
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $kitId;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The product name is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 200,
     *      minMessage = "Your product's name must be at least {{ limit }} characters long",
     *      maxMessage = "Your product's name cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $name;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="The product quantity is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of product quantity {{ value }} is not a valid type (integer)"
     * )
     * @Assert\GreaterThanOrEqual(
     *     value = 1
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $quantity;

    /**
     * @var float
     *
     * @Assert\NotBlank(message="The product price is required.")
     * @Assert\Type(
     *     type="float",
     *     message="The value of product price {{ value }} is not a valid type (float)"
     * )
     * @Assert\GreaterThan(
     *     value = 0
     * )
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $price;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getKitId()
    {
        return $this->kitId;
    }

    /**
     * @param int $kitId
     */
    public function setKitId($kitId)
    {
        $this->kitId = $kitId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

}