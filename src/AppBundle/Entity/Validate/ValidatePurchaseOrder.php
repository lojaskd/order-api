<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidatePurchaseOrder
 *
 * @ORM\Entity()
 */
class ValidatePurchaseOrder
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="purchase_order_id is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of purchase_order_id {{ value }} is not a valid type (integer)"
     * )
     * @Assert\GreaterThan(
     *     value = 0
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $purchaseOrderId;

    /**
     * @var array
     *
     * @Assert\Valid
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\Validate\ValidatePurchaseOrderProduct>")
     */
    private $products;

    /**
     * @var \Datetime
     * @Assert\NotBlank(message="Date is required.")
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of date {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $date;

    /**
     * @var string
     * @Assert\NotBlank(message="Clerk is required.")
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getPurchaseOrderId()
    {
        return $this->purchaseOrderId;
    }

    /**
     * @param int $purchaseOrderId
     */
    public function setPurchaseOrderId($purchaseOrderId)
    {
        $this->purchaseOrderId = $purchaseOrderId;
    }

    /**
     * @return \Datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \Datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @param ValidatePurchaseOrderProduct $product
     */
    public function addProduct(ValidatePurchaseOrderProduct $product)
    {
        $this->products[] = $product;
    }

    /**
     * @return array $products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

}
