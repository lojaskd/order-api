<?php

namespace AppBundle\Entity\Validate\Delete;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateProductDate
 *
 */
class ProductDate
{

    /**
     * @var integer
     *
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var array
     *
     * @Assert\Valid
     *
     * @Type("array<AppBundle\Entity\Validate\ValidateProductDateProduct>")
     */
    private $products;

    /**
     * @var array
     *
     * @Assert\NotBlank(message="The dateTypes field is required.")
     * @Assert\Valid
     *
     * @Type("array<AppBundle\Entity\Validate\Delete\ProductDateType>")
     */
    private $dateTypes;

    /**
     * @var string
     * @Assert\NotBlank(message="Clerk is required.")
     * @Type("string")
     */
    private $clerk;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return array
     */
    public function getDateTypes()
    {
        return $this->dateTypes;
    }

    /**
     * @param array $dateTypes
     */
    public function setDateTypes($dateTypes)
    {
        $this->dateTypes = $dateTypes;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }
}