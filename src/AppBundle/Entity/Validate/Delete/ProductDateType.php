<?php

namespace AppBundle\Entity\Validate\Delete;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateProductDateType
 *
 */
class ProductDateType
{

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Type is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of type {{ value }} is not a valid type (string)"
     * )
     * @Assert\Choice(choices = {"approvalDate", "releaseDate", "supplierBillingDate", "supplierInvoiceDate",
     *     "supplierDeliveryDate", "shippingDate", "invoiceDate", "deliveryDate"}, message = "Choose a valid record
     *     type")
     *
     * @Type("string")
     */
    private $type;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
