<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateOrder
 *
 * @ORM\Entity
 */
class ValidateOrder
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var \AppBundle\Entity\Validate\ValidateCustomer
     *
     * @Assert\NotBlank(message="The customer field is required.")
     * @Assert\Valid
     *
     * @ORM\Column(type="\AppBundle\Entity\Validate\ValidateCustomer")
     * @Type("AppBundle\Entity\Validate\ValidateCustomer")
     */
    private $customer;

    /**
     * @var array
     *
     * @Assert\NotBlank(message="The products field is required.")
     * @Assert\Valid
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\Validate\ValidateProduct>")
     */
    private $products;

    /**
     * @var float
     *
     * @Assert\NotBlank(message="The subtotal field is required.")
     * @Assert\Type(
     *     type="float",
     *     message="The value of subtotal {{ value }} is not a valid type (float)"
     * )
     * @Assert\GreaterThan(
     *     value = 0,
     *     message="Subtotal value should be greater than 0"
     * )
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $subtotal;

    /**
     * @var float
     *
     * @Assert\NotBlank(message="The total value field is required.")
     * @Assert\Type(
     *     type="float",
     *     message="The total value {{ value }} is not a valid type (float)"
     * )
     * @Assert\GreaterThanOrEqual(
     *     value = 0,
     *     message="Total value should be greater or equal than 0"
     * )
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $total;

    /**
     * @var float
     *
     * @Assert\NotBlank(message="The discount value field is required.")
     * @Assert\Type(
     *     type="float",
     *     message="The discount value {{ value }} is not a valid type (float)"
     * )
     * @Assert\GreaterThanOrEqual(
     *     value = 0,
     *     message="Discount value should be greater or equal than 0"
     * )
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $discount;

    /**
     * @var \AppBundle\Entity\Validate\ValidateShipping
     *
     * @Assert\NotBlank(message="The shipping field is required.")
     * @Assert\Valid
     *
     * @ORM\Column(type="\AppBundle\Entity\Validate\ValidateShipping")
     * @Type("AppBundle\Entity\Validate\ValidateShipping")
     */
    private $shipping;

    /**
     * @var string
     *
     * @var \AppBundle\Entity\Validate\ValidateCoupon
     *
     * @Assert\Valid
     *
     * @ORM\Column(type="\AppBundle\Entity\Validate\ValidateCoupon")
     * @Type("AppBundle\Entity\Validate\ValidateCoupon")
     */
    private $coupon;

    /**
     * @var \AppBundle\Entity\Validate\ValidateOrigin
     *
     * @Assert\Valid
     *
     * @ORM\Column(type="\AppBundle\Entity\Validate\ValidateOrigin")
     * @Type("AppBundle\Entity\Validate\ValidateOrigin")
     */
    private $origin;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of observation {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $observation;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The order type field is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of order type {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $order_type;

    /**
     * @var array
     *
     * @Assert\NotBlank(message="The payments field is required.")
     * @Assert\Valid
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\Validate\ValidatePayment>")
     */
    private $payments;

    /**
     * @var \AppBundle\Entity\Validate\ValidateAddress
     *
     *
     * @ORM\Column(type="\AppBundle\Entity\Validate\ValidateAddress")
     * @Type("AppBundle\Entity\Validate\ValidateAddress")
     */
    private $billingAddress;

    /**
     * @var \AppBundle\Entity\Validate\ValidateAddress
     *
     *
     * @ORM\Column(type="\AppBundle\Entity\Validate\ValidateAddress")
     * @Type("AppBundle\Entity\Validate\ValidateAddress")
     */
    private $deliveryAddress;

    /**
     * @var \AppBundle\Entity\Validate\ValidateMarketPlace
     *
     *
     * @ORM\Column(type="\AppBundle\Entity\Validate\ValidateMarketPlace")
     * @Type("AppBundle\Entity\Validate\ValidateMarketPlace")
     */
    private $marketplace;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of device {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $device;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="The value of installmentSapCoupon {{ value }} is not a valid type (boolean)"
     * )
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $installmentSapCoupon;

    /*
     * Constructor
     */
    public function __construct()
    {
        $this->customer = new ValidateCustomer();
        $this->products = new ValidateProduct();
        $this->shipping = new ValidateShipping();
        $this->billingAddress = new ValidateAddress();
        $this->deliveryAddress = new ValidateAddress();
        $this->payments = array();
        $this->marketplace = array();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return ValidateCustomer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param ValidateCustomer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @param ValidateProduct $product
     */
    public function addProducts($product)
    {
        $this->products[] = $product;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param float $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return ValidateShipping
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param ValidateShipping $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return ValidateCoupon
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param ValidateCoupon $coupon
     */
    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @return ValidateOrigin
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param ValidateOrigin $origin
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return string
     */
    public function getOrderType()
    {
        return $this->order_type;
    }

    /**
     * @param string $order_type
     */
    public function setOrderType($order_type)
    {
        $this->order_type = $order_type;
    }

    /**
     * @return array
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param array $payments
     */
    public function setPayments($payments)
    {
        $this->payments = $payments;
    }

    /**
     * @param ValidatePayment $payment
     */
    public function addPayments($payment)
    {
        $this->payments[] = $payment;
    }

    /**
     * @return ValidateAddress
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param ValidateAddress $billingAddress
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return ValidateAddress
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @param ValidateAddress $deliveryAddress
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * @return ValidateMarketplace
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * @param ValidateMarketplace $marketplace
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;
    }

    /**
     * @return string
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param string $device
     */
    public function setDevice($device)
    {
        $this->device = $device;
    }

    /**
     * @return boolean
     */
    public function getInstallmentSapCoupon()
    {
        return $this->installmentSapCoupon;
    }

    /**
     * @param boolean $installmentSapCoupon
     */
    public function setInstallmentSapCoupon($installmentSapCoupon)
    {
        $this->installmentSapCoupon = $installmentSapCoupon;
    }
}
