<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateOrderStatus
 *
 * @ORM\Entity()
 */
class ValidateOrderStatus
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="Status is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of status {{ value }} is not a valid type (integer)"
     * )
     * @Assert\GreaterThan(
     *     value = 0
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $status;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="Internal status  is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of Internal status {{ value }} is not a valid type (integer)"
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $internal_status;

    /**
     * @var string
     * @Assert\NotBlank(message="Clerk is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of clerk {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var integer
     *
     * @Assert\Type(
     *     type="integer",
     *     message="The value of type reversal {{ value }} is not a valid type (integer)"
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $typeReversal;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of ticket {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ticket;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of operator {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $operator;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of transaction {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $transaction;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of ocrCode {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ocrCode;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of cancellationIndicator {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $cancellationIndicator;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of ticketType {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ticketType;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of hub {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $hub;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of shipper {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $shipper;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of invoice {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $invoice;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of productCauser {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $productCauser;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getInternalStatus()
    {
        return $this->internal_status;
    }

    /**
     * @param int $internal_status
     */
    public function setInternalStatus($internal_status)
    {
        $this->internal_status = $internal_status;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return int
     */
    public function getTypeReversal()
    {
        return $this->typeReversal;
    }

    /**
     * @param int $typeReversal
     */
    public function setTypeReversal($typeReversal)
    {
        $this->typeReversal = $typeReversal;
    }

    /**
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param string $ticket
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return string
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param string $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return string
     */
    public function getOcrCode()
    {
        return $this->ocrCode;
    }

    /**
     * @param string $ocrCode
     */
    public function setOcrCode($ocrCode)
    {
        $this->ocrCode = $ocrCode;
    }

    /**
     * @return string
     */
    public function getCancellationIndicator()
    {
        return $this->cancellationIndicator;
    }

    /**
     * @param string $cancellationIndicator
     */
    public function setCancellationIndicator($cancellationIndicator)
    {
        $this->cancellationIndicator = $cancellationIndicator;
    }

    /**
     * @return string
     */
    public function getTicketType()
    {
        return $this->ticketType;
    }

    /**
     * @param string $ticketType
     */
    public function setTicketType($ticketType)
    {
        $this->ticketType = $ticketType;
    }

    /**
     * @return string
     */
    public function getHub()
    {
        return $this->hub;
    }

    /**
     * @param string $hub
     */
    public function setHub($hub)
    {
        $this->hub = $hub;
    }

    /**
     * @return string
     */
    public function getShipper()
    {
        return $this->shipper;
    }

    /**
     * @param string $shipper
     */
    public function setShipper($shipper)
    {
        $this->shipper = $shipper;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return string
     */
    public function getProductCauser()
    {
        return $this->productCauser;
    }

    /**
     * @param string $productCauser
     */
    public function setProductCauser($productCauser)
    {
        $this->productCauser = $productCauser;
    }
}
