<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateCoupon
 *
 * @ORM\Entity()
 */
class ValidateCoupon
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of coupon code {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $code;

    /**
     * @var float
     *
     * @Assert\Type(
     *     type="float",
     *     message="The value amount of the coupon {{ value }} is not a valid type (float)"
     * )
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $value;


    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

}
