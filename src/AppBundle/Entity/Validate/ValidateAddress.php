<?php

namespace AppBundle\Entity\Validate;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateAddress
 *
 * @ORM\Entity()
 */
class ValidateAddress
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $identification;
    
    /**
     * @var string
     *
     * @Assert\NotBlank(message="The address' first name is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "Your address' first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your address' first name cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $first_name;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The address' last name is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 80,
     *      minMessage = "Your address' last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your address' last name cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $last_name;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The address is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 200,
     *      minMessage = "Your address must be at least {{ limit }} characters long",
     *      maxMessage = "Your address cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $address;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="The address number is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of address number {{ value }} is not a valid type (integer)"
     * )
     * @Assert\GreaterThanOrEqual(
     *     value = 0
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $number;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $complement;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The neighborhood is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of neighborhood {{ value }} is not a valid type (string)"
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 200,
     *      minMessage = "Your neighborhood must be at least {{ limit }} characters long",
     *      maxMessage = "Your neighborhood cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $neighborhood;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The city is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of city {{ value }} is not a valid type (string)"
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 200,
     *      minMessage = "Your city must be at least {{ limit }} characters long",
     *      maxMessage = "Your city cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The state is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of state {{ value }} is not a valid type (string)"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 2,
     *      exactMessage = "Your state should have 2 characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $state;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The zip code is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of zip code {{ value }} is not a valid type (string)"
     * )
     * @Assert\Length(
     *      min = 8,
     *      max = 9,
     *      minMessage = "Your zip code must be at least {{ limit }} characters long",
     *      maxMessage = "Your zip code cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $zip;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The country name is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of country name {{ value }} is not a valid type (string)"
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "Your country name must be at least {{ limit }} characters long",
     *      maxMessage = "Your country name cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $country;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The telephone A is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of telephone A {{ value }} is not a valid type (string)"
     * )
     * @Assert\Length(
     *      min = 10,
     *      max = 15,
     *      minMessage = "Your telephone A must be at least {{ limit }} characters long",
     *      maxMessage = "Your telephone A cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $telephone_a;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The telephone B is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of telephone B {{ value }} is not a valid type (string)"
     * )
     * @Assert\Length(
     *      min = 10,
     *      max = 15,
     *      minMessage = "Your telephone B must be at least {{ limit }} characters long",
     *      maxMessage = "Your telephone B cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $telephone_b;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $reference;

    /**
     * @return string
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * @param string $identification
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * @param string $complement
     */
    public function setComplement($complement)
    {
        $this->complement = $complement;
    }

    /**
     * @return string
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param string $neighborhood
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getTelephoneA()
    {
        return $this->telephone_a;
    }

    /**
     * @param string $telephone_a
     */
    public function setTelephoneA($telephone_a)
    {
        $this->telephone_a = $telephone_a;
    }

    /**
     * @return string
     */
    public function getTelephoneB()
    {
        return $this->telephone_b;
    }

    /**
     * @param string $telephone_b
     */
    public function setTelephoneB($telephone_b)
    {
        $this->telephone_b = $telephone_b;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

}