<?php

namespace AppBundle\Entity\Validate\Create;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrderMessage
 */
class OrderMessage
{

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="Order ID is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of order Id {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="invoice ID is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of invoice Id {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("integer")
     */
    private $invoiceId;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Ticket is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of ticket {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("string")
     */
    private $ticket;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Clerk is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of clerk {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("string")
     */
    private $clerk;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Task key is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of task key {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("string")
     */
    private $task;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="The value of parent task {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("boolean")
     */
    private $parentTask;


    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param string $ticket
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return string
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param string $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return bool
     */
    public function isParentTask()
    {
        return $this->parentTask;
    }

    /**
     * @param bool $parentTask
     */
    public function setParentTask($parentTask)
    {
        $this->parentTask = $parentTask;
    }
}
