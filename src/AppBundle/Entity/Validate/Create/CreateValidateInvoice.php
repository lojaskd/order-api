<?php

namespace AppBundle\Entity\Validate\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateInvoice
 *
 * @ORM\Entity()
 */
class CreateValidateInvoice
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="invoiceId is required.")
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $invoiceId;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="branchId is required.")
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $branchId;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of serie {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $serie;

    /**
     * @var \Datetime
     * @Assert\NotBlank(message="createdDate is required.")
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of createdDate {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $createdDate;

    /**
     * @var \Datetime
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of shippingDate {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $shippingDate;

    /**
     * @var \Datetime
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of predictDeliveryDate {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $predictDeliveryDate;

    /**
     * @var \Datetime
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of returnedDate {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $returnedDate;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="The value of returned {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $returned;

    /**
     * @var \Datetime
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of deliveryDate {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $deliveryDate;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of key {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $key;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="The value of returned {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $marketplaceIntegration;

    /**
     * @var string
     * @Assert\NotBlank(message="Clerk is required.")
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of shipperName {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $shipperName;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of shipperDoc {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $shipperDoc;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of hub {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $hub;

    /**
     * @var array
     *
     * @Assert\NotBlank(message="The itens field is required.")
     * @Assert\Valid
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\Validate\Create\CreateValidateInvoiceItem>")
     */
    private $itens;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return int
     */
    public function getBranchId()
    {
        return $this->branchId;
    }

    /**
     * @param int $branchId
     */
    public function setBranchId($branchId)
    {
        $this->branchId = $branchId;
    }

    /**
     * @return string
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param string $serie
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \Datetime $createdDate
     */
    public function setCreatedDate(\Datetime $createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \Datetime
     */
    public function getShippingDate()
    {
        return $this->shippingDate;
    }

    /**
     * @param \Datetime $shippingDate
     */
    public function setShippingDate(\Datetime $shippingDate)
    {
        $this->shippingDate = $shippingDate;
    }

    /**
     * @return \Datetime
     */
    public function getPredictDeliveryDate()
    {
        return $this->predictDeliveryDate;
    }

    /**
     * @param \Datetime $predictDeliveryDate
     */
    public function setPredictDeliveryDate(\Datetime $predictDeliveryDate)
    {
        $this->predictDeliveryDate = $predictDeliveryDate;
    }

    /**
     * @return \Datetime
     */
    public function getReturnedDate()
    {
        return $this->returnedDate;
    }

    /**
     * @param \Datetime $returnedDate
     */
    public function setReturnedDate(\Datetime $returnedDate)
    {
        $this->returnedDate = $returnedDate;
    }

    /**
     * @return boolean
     */
    public function isReturned()
    {
        return $this->returned;
    }

    /**
     * @param boolean $returned
     */
    public function setReturned($returned)
    {
        $this->returned = $returned;
    }

    /**
     * @return \Datetime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param \Datetime $deliveryDate
     */
    public function setDeliveryDate(\Datetime $deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return boolean
     */
    public function isMarketplaceIntegration()
    {
        return $this->marketplaceIntegration;
    }

    /**
     * @param boolean $marketplaceIntegration
     */
    public function setMarketplaceIntegration($marketplaceIntegration)
    {
        $this->marketplaceIntegration = $marketplaceIntegration;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return string
     */
    public function getShipperName()
    {
        return $this->shipperName;
    }

    /**
     * @param string $shipperName
     */
    public function setShipperName($shipperName)
    {
        $this->shipperName = $shipperName;
    }

    /**
     * @return string
     */
    public function getShipperDoc()
    {
        return $this->shipperDoc;
    }

    /**
     * @param string $shipperDoc
     */
    public function setShipperDoc($shipperDoc)
    {
        $this->shipperDoc = $shipperDoc;
    }

    /**
     * @return string
     */
    public function getHub()
    {
        return $this->hub;
    }

    /**
     * @param string $hub
     */
    public function setHub($hub)
    {
        $this->hub = $hub;
    }

    /**
     * @return array
     */
    public function getItens()
    {
        return $this->itens;
    }

    /**
     * @param array $itens
     */
    public function setItens($itens)
    {
        $this->itens = $itens;
    }

    /**
     * @param ValidateInvoiceItem $item
     */
    public function addItem(CreateValidateInvoiceItem $item)
    {
        $this->itens[] = $item;
    }
}
