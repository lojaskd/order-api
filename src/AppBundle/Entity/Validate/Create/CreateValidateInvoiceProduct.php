<?php

namespace AppBundle\Entity\Validate\Create;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CreateValidateInvoiceProduct
 */
class CreateValidateInvoiceProduct
{

    /**
     * @var integer
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="invoiceId is required.")
     * @Type("integer")
     */
    private $invoiceId;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="branchId is required.")
     * @Type("integer")
     */
    private $branchId;

    /**
     * @var \Datetime
     * @Assert\NotBlank(message="createdDate is required.")
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of createdDate {{ value }} is not a valid type (date)"
     * )
     * @Type("DateTime<'Y-m-d'>")
     */
    private $createdDate;

    /**
     * @var \Datetime
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of returnedDate {{ value }} is not a valid type (date)"
     * )
     * @Type("DateTime<'Y-m-d'>")
     */
    private $returnedDate;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="The value of returned {{ value }} is not a valid {{ type }}."
     * )
     * @Type("boolean")
     */
    private $returned;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of shipperName {{ value }} is not a valid type (string)"
     * )
     * @Type("string")
     */
    private $shipperName;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of shipperDoc {{ value }} is not a valid type (string)"
     * )
     * @Type("string")
     */
    private $shipperDoc;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of hub {{ value }} is not a valid type (string)"
     * )
     * @Type("string")
     */
    private $hub;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="productReference is required.")
     * @Type("string")
     */
    private $productReference;

    /**
     * @var integer
     * @Assert\NotBlank(message="qty is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of qty {{ value }} is not a valid type (string)"
     * )
     *
     * @Type("integer")
     */
    private $qty;


    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return int
     */
    public function getBranchId()
    {
        return $this->branchId;
    }

    /**
     * @param int $branchId
     */
    public function setBranchId($branchId)
    {
        $this->branchId = $branchId;
    }

    /**
     * @return \Datetime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \Datetime $createdDate
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \Datetime
     */
    public function getReturnedDate()
    {
        return $this->returnedDate;
    }

    /**
     * @param \Datetime $returnedDate
     */
    public function setReturnedDate($returnedDate)
    {
        $this->returnedDate = $returnedDate;
    }

    /**
     * @return bool
     */
    public function isReturned()
    {
        return $this->returned;
    }

    /**
     * @param bool $returned
     */
    public function setReturned($returned)
    {
        $this->returned = $returned;
    }

    /**
     * @return string
     */
    public function getShipperName()
    {
        return $this->shipperName;
    }

    /**
     * @param string $shipperName
     */
    public function setShipperName($shipperName)
    {
        $this->shipperName = $shipperName;
    }

    /**
     * @return string
     */
    public function getShipperDoc()
    {
        return $this->shipperDoc;
    }

    /**
     * @param string $shipperDoc
     */
    public function setShipperDoc($shipperDoc)
    {
        $this->shipperDoc = $shipperDoc;
    }

    /**
     * @return string
     */
    public function getHub()
    {
        return $this->hub;
    }

    /**
     * @param string $hub
     */
    public function setHub($hub)
    {
        $this->hub = $hub;
    }

    /**
     * @return string
     */
    public function getProductReference()
    {
        return $this->productReference;
    }

    /**
     * @param string $productReference
     */
    public function setProductReference($productReference)
    {
        $this->productReference = $productReference;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
    }
}
