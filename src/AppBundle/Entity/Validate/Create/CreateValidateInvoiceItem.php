<?php

namespace AppBundle\Entity\Validate\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidateInvoiceItem
 *
 * @ORM\Entity()
 */
class CreateValidateInvoiceItem
{

    /**
     * @var string
     *
     * @Assert\NotBlank(message="productReference is required.")
     *
     * @ORM\Column(type="string")
     * @ORM\Id
     * @Type("string")
     */
    private $productReference;

    /**
     * @var \Datetime
     * @Assert\Type(
     *     type="datetime",
     *     message="The value of returnedDate {{ value }} is not a valid type (date)"
     * )
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $returnedDate;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="The value of returned {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $returned;

    /**
     * @var integer
     * @Assert\NotBlank(message="qty is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of qty {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $qty;

    /**
     * @return string
     */
    function getProductReference()
    {
        return $this->productReference;
    }

    /**
     * @param string $productReference
     */
    function setProductReference($productReference)
    {
        $this->productReference = $productReference;
    }

    /**
     * @return \Datetime
     */
    function getReturnedDate()
    {
        return $this->returnedDate;
    }

    /**
     * @param \Datetime $returnedDate
     */
    function setReturnedDate(\Datetime $returnedDate)
    {
        $this->returnedDate = $returnedDate;
    }

    /**
     * @return boolean
     */
    function isReturned()
    {
        return $this->returned;
    }

    /**
     * @param boolean $returned
     */
    function setReturned($returned)
    {
        $this->returned = $returned;
    }

    /**
     * @return int
     */
    function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     */
    function setQty($qty)
    {
        $this->qty = $qty;
    }

}
