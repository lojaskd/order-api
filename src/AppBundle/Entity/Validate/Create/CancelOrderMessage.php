<?php

namespace AppBundle\Entity\Validate\Create;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CancelOrderMessage
 */
class CancelOrderMessage
{

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="Order ID is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of order Id {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var array
     *
     * @Assert\NotBlank(message="Products are required.")
     * @Assert\Type(
     *     type="array",
     *     message="The value of products {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("array")
     */
    private $products;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="Ticket is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of ticket {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("integer")
     */
    private $ticket;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Task key is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of task key {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("string")
     */
    private $task;

    /**
     * @var string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The value of clerk {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("string")
     */
    private $clerk;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Type is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of type {{ value }} is not a valid {{ type }}."
     * )
     *
     * @Type("string")
     */
    private $type;


    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param string $ticket
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * @return string
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param string $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
