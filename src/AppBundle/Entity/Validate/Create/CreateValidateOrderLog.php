<?php

namespace AppBundle\Entity\Validate\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CreateValidateOrderLog
 */
class CreateValidateOrderLog
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Title is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of title {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $title;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Text is required.")
     * @Assert\Type(
     *     type="string",
     *     message="The value of text {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $text;

    /**
     * @var boolean
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="The value of show {{ value }} is not a valid {{ type }}."
     * )
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $show;

    /**
     * @var integer
     * @Assert\NotBlank(message="Status is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of status {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $status;

    /**
     * @var integer
     * @Assert\NotBlank(message="Internal status is required.")
     * @Assert\Type(
     *     type="integer",
     *     message="The value of internal status {{ value }} is not a valid type (string)"
     * )
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $internalStatus;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return boolean
     */
    public function isShow()
    {
        return $this->show;
    }

    /**
     * @param boolean $show
     */
    public function setShow($show)
    {
        $this->show = $show;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getInternalStatus()
    {
        return $this->internalStatus;
    }

    /**
     * @param int $internalStatus
     */
    public function setInternalStatus($internalStatus)
    {
        $this->internalStatus = $internalStatus;
    }

}
