<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdpag
 *
 * @ORM\Table(name="b2c_prdpag", indexes={@ORM\Index(name="id_prdprd", columns={"id_prdprd"}), @ORM\Index(name="id_pagpag", columns={"id_pagpag"}), @ORM\Index(name="id_pagfor", columns={"id_pagfor"}), @ORM\Index(name="padrao", columns={"padrao"})})
 * @ORM\Entity
 */
class B2cPrdpag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPrdprd;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagpag", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPagpag;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagfor", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPagfor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="padrao", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $padrao;



    /**
     * Set idPrdprd
     *
     * @param integer $idPrdprd
     *
     * @return B2cPrdpag
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;

        return $this;
    }

    /**
     * Get idPrdprd
     *
     * @return integer
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * Set idPagpag
     *
     * @param integer $idPagpag
     *
     * @return B2cPrdpag
     */
    public function setIdPagpag($idPagpag)
    {
        $this->idPagpag = $idPagpag;

        return $this;
    }

    /**
     * Get idPagpag
     *
     * @return integer
     */
    public function getIdPagpag()
    {
        return $this->idPagpag;
    }

    /**
     * Set idPagfor
     *
     * @param integer $idPagfor
     *
     * @return B2cPrdpag
     */
    public function setIdPagfor($idPagfor)
    {
        $this->idPagfor = $idPagfor;

        return $this;
    }

    /**
     * Get idPagfor
     *
     * @return integer
     */
    public function getIdPagfor()
    {
        return $this->idPagfor;
    }

    /**
     * Set padrao
     *
     * @param boolean $padrao
     *
     * @return B2cPrdpag
     */
    public function setPadrao($padrao)
    {
        $this->padrao = $padrao;

        return $this;
    }

    /**
     * Get padrao
     *
     * @return boolean
     */
    public function getPadrao()
    {
        return $this->padrao;
    }
}
