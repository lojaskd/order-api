<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoPrd
 *
 * @ORM\Table(name="b2c_promo_prd", indexes={@ORM\Index(name="aplicar", columns={"aplicar", "id_promo", "id_produto"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPromoPrdRepository")
 */
class B2cPromoPrd
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPromo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_produto", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProduto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aplicar", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $aplicar;



    /**
     * Set idPromo
     *
     * @param integer $idPromo
     *
     * @return B2cPromoPrd
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;

        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set idProduto
     *
     * @param integer $idProduto
     *
     * @return B2cPromoPrd
     */
    public function setIdProduto($idProduto)
    {
        $this->idProduto = $idProduto;

        return $this;
    }

    /**
     * Get idProduto
     *
     * @return integer
     */
    public function getIdProduto()
    {
        return $this->idProduto;
    }

    /**
     * Set aplicar
     *
     * @param boolean $aplicar
     *
     * @return B2cPromoPrd
     */
    public function setAplicar($aplicar)
    {
        $this->aplicar = $aplicar;

        return $this;
    }

    /**
     * Get aplicar
     *
     * @return boolean
     */
    public function getAplicar()
    {
        return $this->aplicar;
    }
}
