<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cNotafiscal
 *
 * @ORM\Table(name="b2c_notafiscal")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cNotafiscalRepository")
 */
class B2cNotafiscal
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_filial", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idFilial;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_notafiscal", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idNotafiscal;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPedped;

    /**
     * @var string
     *
     * @ORM\Column(name="serie_notafiscal", type="string", length=3, precision=0, scale=0, nullable=true, unique=false)
     */
    private $serieNotafiscal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_embarque", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEmbarque;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_entrega_provavel", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEntregaProvavel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_entrega_realizada", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEntregaRealizada;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_devolucao", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataDevolucao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="devolvido", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $devolvido;

    /**
     * @var string
     *
     * @ORM\Column(name="chave", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $chave;

    /**
     * @var integer
     *
     * @ORM\Column(name="integracao_marketplace", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $integracaoMarketplace;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_chave", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataChave;

    /**
     * @var string
     *
     * @ORM\Column(name="path_xml", type="string", length=300, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pathXml;

    /**
     * @var string
     *
     * @ORM\Column(name="path_pdf", type="string", length=300, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pathPdf;

    /**
     * @return int
     */
    public function getIdFilial()
    {
        return $this->idFilial;
    }

    /**
     * @param int $idFilial
     */
    public function setIdFilial($idFilial)
    {
        $this->idFilial = $idFilial;
    }

    /**
     * @return int
     */
    public function getIdNotafiscal()
    {
        return $this->idNotafiscal;
    }

    /**
     * @param int $idNotafiscal
     */
    public function setIdNotafiscal($idNotafiscal)
    {
        $this->idNotafiscal = $idNotafiscal;
    }

    /**
     * @return int
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * @param int $idPedped
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;
    }

    /**
     * @return string
     */
    public function getSerieNotafiscal()
    {
        return $this->serieNotafiscal;
    }

    /**
     * @param string $serieNotafiscal
     */
    public function setSerieNotafiscal($serieNotafiscal)
    {
        $this->serieNotafiscal = $serieNotafiscal;
    }

    /**
     * @return \DateTime
     */
    public function getDataEmbarque()
    {
        return $this->dataEmbarque;
    }

    /**
     * @param \DateTime $dataEmbarque
     */
    public function setDataEmbarque($dataEmbarque)
    {
        $this->dataEmbarque = $dataEmbarque;
    }

    /**
     * @return \DateTime
     */
    public function getDataEntregaProvavel()
    {
        return $this->dataEntregaProvavel;
    }

    /**
     * @param \DateTime $dataEntregaProvavel
     */
    public function setDataEntregaProvavel($dataEntregaProvavel)
    {
        $this->dataEntregaProvavel = $dataEntregaProvavel;
    }

    /**
     * @return \DateTime
     */
    public function getDataEntregaRealizada()
    {
        return $this->dataEntregaRealizada;
    }

    /**
     * @param \DateTime $dataEntregaRealizada
     */
    public function setDataEntregaRealizada($dataEntregaRealizada)
    {
        $this->dataEntregaRealizada = $dataEntregaRealizada;
    }

    /**
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * @param \DateTime $dataCriacao
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;
    }

    /**
     * @return \DateTime
     */
    public function getDataDevolucao()
    {
        return $this->dataDevolucao;
    }

    /**
     * @param \DateTime $dataDevolucao
     */
    public function setDataDevolucao($dataDevolucao)
    {
        $this->dataDevolucao = $dataDevolucao;
    }

    /**
     * @return boolean
     */
    public function isDevolvido()
    {
        return $this->devolvido;
    }

    /**
     * @param boolean $devolvido
     */
    public function setDevolvido($devolvido)
    {
        $this->devolvido = $devolvido;
    }

    /**
     * @return string
     */
    public function getChave()
    {
        return $this->chave;
    }

    /**
     * @param string $chave
     */
    public function setChave($chave)
    {
        $this->chave = $chave;
    }

    /**
     * @return int
     */
    public function getIntegracaoMarketplace()
    {
        return $this->integracaoMarketplace;
    }

    /**
     * @param int $integracaoMarketplace
     */
    public function setIntegracaoMarketplace($integracaoMarketplace)
    {
        $this->integracaoMarketplace = $integracaoMarketplace;
    }

    /**
     * @return \DateTime
     */
    public function getDataChave()
    {
        return $this->dataChave;
    }

    /**
     * @param \DateTime $dataChave
     */
    public function setDataChave($dataChave)
    {
        $this->dataChave = $dataChave;
    }

    /**
     * @return string
     */
    public function getPathXml()
    {
        return $this->pathXml;
    }

    /**
     * @param string $pathXml
     */
    public function setPathXml($pathXml)
    {
        $this->pathXml = $pathXml;
    }

    /**
     * @return string
     */
    public function getPathPdf()
    {
        return $this->pathPdf;
    }

    /**
     * @param string $pathPdf
     */
    public function setPathPdf($pathPdf)
    {
        $this->pathPdf = $pathPdf;
    }

}
