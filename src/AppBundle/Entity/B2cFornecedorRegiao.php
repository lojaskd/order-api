<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cFornecedorRegiao
 *
 * @ORM\Table(name="b2c_fornecedor_regiao", indexes={
 *     @ORM\Index(name="id_forfor", columns={"id_forfor"}),
 *     @ORM\Index(name="id_regreg", columns={"id_regreg"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cFornecedorRegiaoRepository")
 */
class B2cFornecedorRegiao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_forfor", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     */
    private $idForfor;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_regreg", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     */
    private $idRegreg;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_filial", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idFilial;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_coleta", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prazoColeta;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_recebimento", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prazoRecebimento;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_separacao", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prazoSeparacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_romaneio", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prazoRomaneio;

    /**
     * @var integer
     *
     * @ORM\Column(name="prioridade", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prioridade;

    /**
     * @param integer $codigoTransportadorColeta
     *
     * @ORM\Column(name="cod_transportador_coleta", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $codigoTransportadorColeta;

    /**
     * @param string $destination
     *
     * @ORM\Column(name="destino", type="string", nullable=false, unique=false)
     */
    private $destination;

    /**
     * @return int
     */
    public function getIdForfor()
    {
        return $this->idForfor;
    }

    /**
     * @param $idForfor
     * @return $this
     */
    public function setIdForfor($idForfor)
    {
        $this->idForfor = $idForfor;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdRegreg()
    {
        return $this->idRegreg;
    }

    /**
     * @param $idRegreg
     * @return $this
     */
    public function setIdRegreg($idRegreg)
    {
        $this->idRegreg = $idRegreg;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdFilial()
    {
        return $this->idFilial;
    }

    /**
     * @param $idFilial
     * @return $this
     */
    public function setIdFilial($idFilial)
    {
        $this->idFilial = $idFilial;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrazoColeta()
    {
        return $this->prazoColeta;
    }

    /**
     * @param $prazoColeta
     * @return $this
     */
    public function setPrazoColeta($prazoColeta)
    {
        $this->prazoColeta = $prazoColeta;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrazoRecebimento()
    {
        return $this->prazoRecebimento;
    }

    /**
     * @param $prazoRecebimento
     * @return $this
     */
    public function setPrazoRecebimento($prazoRecebimento)
    {
        $this->prazoRecebimento = $prazoRecebimento;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrazoSeparacao()
    {
        return $this->prazoSeparacao;
    }

    /**
     * @param $prazoSeparacao
     * @return $this
     */
    public function setPrazoSeparacao($prazoSeparacao)
    {
        $this->prazoSeparacao = $prazoSeparacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrazoRomaneio()
    {
        return $this->prazoRomaneio;
    }

    /**
     * @param $prazoRomaneio
     * @return $this
     */
    public function setPrazoRomaneio($prazoRomaneio)
    {
        $this->prazoRomaneio = $prazoRomaneio;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrioridade()
    {
        return $this->prioridade;
    }

    /**
     * @param $prioridade
     * @return $this
     */
    public function setPrioridade($prioridade)
    {
        $this->prioridade = $prioridade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodigoTransportadorColeta()
    {
        return $this->codigoTransportadorColeta;
    }

    /**
     * @param $codigoTransportadorColeta
     * @return $this
     */
    public function setCodigoTransportadorColeta($codigoTransportadorColeta)
    {
        $this->codigoTransportadorColeta = $codigoTransportadorColeta;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param $destination
     * @return $this
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
        return $this;
    }
}
