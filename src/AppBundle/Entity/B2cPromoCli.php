<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoCli
 *
 * @ORM\Table(name="b2c_promo_cli")
 * @ORM\Entity
 */
class B2cPromoCli
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPromo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cliente", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCliente;



    /**
     * Set idPromo
     *
     * @param integer $idPromo
     *
     * @return B2cPromoCli
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;

        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set idCliente
     *
     * @param integer $idCliente
     *
     * @return B2cPromoCli
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get idCliente
     *
     * @return integer
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }
}
