<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cContaContabil
 *
 * @ORM\Table(name="b2c_marketplace", indexes={@ORM\Index(name="id", columns={"id"})})
 * @ORM\Entity
 */
class B2cMarketplace
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_marketplace", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nome;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_conta", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idConta;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return int
     */
    public function getIdConta()
    {
        return $this->idConta;
    }

    /**
     * @param int $idConta
     */
    public function setIdConta($idConta)
    {
        $this->idConta = $idConta;
    }

}
