<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class B2cAtributo
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="b2c_atributo", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug"})}, indexes={@ORM\Index(name="buscavel", columns={"buscavel", "tipo_campo"}), @ORM\Index(name="idx_buscavel_tipo", columns={"buscavel", "tipo_campo"})})
 * @ORM\Entity
 */
class B2cAtributo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_atributo", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAtributo;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_atributo", type="string", length=45, nullable=false)
     */
    private $nomeAtributo;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_campo", type="string", length=30, nullable=false)
     */
    private $tipoCampo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="buscavel", type="boolean", nullable=true)
     */
    private $buscavel;

    /**
     * @var integer
     *
     * @ORM\Column(name="atributo_grupo", type="smallint", nullable=false)
     */
    private $atributoGrupo;

    /**
     * @var integer
     *
     * @ORM\Column(name="filtravel", type="integer", nullable=true)
     */
    private $filtravel = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="expedicao", type="integer", nullable=false)
     */
    private $expedicao = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_atributo_tipo_valor", type="smallint", nullable=true)
     */
    private $idAtributoTipoValor;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_atributo_otimizado", type="string", length=45, nullable=true)
     */
    private $nomeAtributoOtimizado;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem", type="integer", nullable=false)
     */
    private $ordem = '9999';

    /**
     * @var boolean
     *
     * @ORM\Column(name="visivel", type="boolean", nullable=false)
     */
    private $visivel = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem_adm", type="integer", nullable=false)
     */
    private $ordemAdm = '9999';

    /**
     * @param $idAtributo
     * @return $this
     */
    public function setIdAtributo($idAtributo)
    {
        $this->idAtributo = $idAtributo;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAtributo()
    {
        return $this->idAtributo;
    }

    /**
     * @param $nomeAtributo
     * @return $this
     */
    public function setNomeAtributo($nomeAtributo)
    {
        $this->nomeAtributo = $nomeAtributo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomeAtributo()
    {
        return $this->nomeAtributo;
    }

    /**
     * @param $tipoCampo
     * @return $this
     */
    public function setTipoCampo($tipoCampo)
    {
        $this->tipoCampo = $tipoCampo;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoCampo()
    {
        return $this->tipoCampo;
    }

    /**
     * @param $buscavel
     * @return $this
     */
    public function setBuscavel($buscavel)
    {
        $this->buscavel = $buscavel;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBuscavel()
    {
        return $this->buscavel;
    }

    /**
     * @param $atributoGrupo
     * @return $this
     */
    public function setAtributoGrupo($atributoGrupo)
    {
        $this->atributoGrupo = $atributoGrupo;
        return $this;
    }

    /**
     * @return int
     */
    public function getAtributoGrupo()
    {
        return $this->atributoGrupo;
    }

    /**
     * @param $filtravel
     * @return $this
     */
    public function setFiltravel($filtravel)
    {
        $this->filtravel = $filtravel;
        return $this;
    }

    /**
     * @return int
     */
    public function getFiltravel()
    {
        return $this->filtravel;
    }

    /**
     * @param $expedicao
     * @return $this
     */
    public function setExpedicao($expedicao)
    {
        $this->expedicao = $expedicao;
        return $this;
    }

    /**
     * @return int
     */
    public function getExpedicao()
    {
        return $this->expedicao;
    }

    /**
     * @param $slug
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param $idAtributoTipoValor
     * @return $this
     */
    public function setIdAtributoTipoValor($idAtributoTipoValor)
    {
        $this->idAtributoTipoValor = $idAtributoTipoValor;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAtributoTipoValor()
    {
        return $this->idAtributoTipoValor;
    }

    /**
     * @param $nomeAtributoOtimizado
     * @return $this
     */
    public function setNomeAtributoOtimizado($nomeAtributoOtimizado)
    {
        $this->nomeAtributoOtimizado = $nomeAtributoOtimizado;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomeAtributoOtimizado()
    {
        return $this->nomeAtributoOtimizado;
    }

    /**
     * @param $ordem
     * @return $this
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrdem()
    {
        return $this->ordem;
    }

    /**
     * @param $visivel
     * @return $this
     */
    public function setVisivel($visivel)
    {
        $this->visivel = $visivel;
        return $this;
    }

    /**
     * @return bool
     */
    public function getVisivel()
    {
        return $this->visivel;
    }

    /**
     * @param $ordemAdm
     * @return $this
     */
    public function setOrdemAdm($ordemAdm)
    {
        $this->ordemAdm = $ordemAdm;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrdemAdm()
    {
        return $this->ordemAdm;
    }
}
