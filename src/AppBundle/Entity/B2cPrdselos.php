<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdselos
 *
 * @ORM\Table(name="b2c_prdselos")
 * @ORM\Entity
 */
class B2cPrdselos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPrdprd;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_selo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idSelo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ativo;

    /**
     * @return int
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * @param int $idPrdprd
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;
    }

    /**
     * @return int
     */
    public function getIdSelo()
    {
        return $this->idSelo;
    }

    /**
     * @param int $idSelo
     */
    public function setIdSelo($idSelo)
    {
        $this->idSelo = $idSelo;
    }

    /**
     * @return boolean
     */
    public function isAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param boolean $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

}
