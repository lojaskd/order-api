<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Attributes
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="attributes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttributesRepository")
 */
class Attributes extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="attr_type", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $attrType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="multiple", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $multiple;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $required;

    /**
     * @var string
     *
     * @ORM\Column(name="default", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $default;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hint", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $hint;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getAttrType()
    {
        return $this->attrType;
    }

    /**
     * @param int $attrType
     * @return $this
     */
    public function setAttrType($attrType)
    {
        $this->attrType = $attrType;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isMultiple()
    {
        return $this->multiple;
    }

    /**
     * @param boolean $multiple
     * @return $this
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @param boolean $required
     * @return $this
     */
    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param string $default
     * @return $this
     */
    public function setDefault($default)
    {
        $this->default = $default;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isHint()
    {
        return $this->hint;
    }

    /**
     * @param boolean $hint
     * @return $this
     */
    public function setHint($hint)
    {
        $this->hint = $hint;
        return $this;
    }
}
