<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdimg
 *
 * @ORM\Table(name="b2c_prdimg", indexes={@ORM\Index(name="id_prdprd", columns={"id_prdprd"}), @ORM\Index(name="tipo", columns={"tipo"}), @ORM\Index(name="id_imgdesc", columns={"id_imgdesc"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPrdimgRepository")
 */
class B2cPrdimg
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdimg", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPrdimg;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idPrdprd;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $tipo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="thumb", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $thumb;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ordem;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_imgdesc", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idImgdesc;

    /**
     * @param $idPrdimg
     * @return $this
     */
    public function setIdPrdimg($idPrdimg)
    {
        $this->idPrdimg = $idPrdimg;
        return $this;
    }

    /**
     * Get idPrdimg
     *
     * @return integer
     */
    public function getIdPrdimg()
    {
        return $this->idPrdimg;
    }

    /**
     * Set idPrdprd
     *
     * @param integer $idPrdprd
     *
     * @return B2cPrdimg
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;

        return $this;
    }

    /**
     * Get idPrdprd
     *
     * @return integer
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cPrdimg
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return B2cPrdimg
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set thumb
     *
     * @param boolean $thumb
     *
     * @return B2cPrdimg
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;

        return $this;
    }

    /**
     * Get thumb
     *
     * @return boolean
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * Set ordem
     *
     * @param integer $ordem
     *
     * @return B2cPrdimg
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * Get ordem
     *
     * @return integer
     */
    public function getOrdem()
    {
        return $this->ordem;
    }

    /**
     * Set idImgdesc
     *
     * @param integer $idImgdesc
     *
     * @return B2cPrdimg
     */
    public function setIdImgdesc($idImgdesc)
    {
        $this->idImgdesc = $idImgdesc;

        return $this;
    }

    /**
     * Get idImgdesc
     *
     * @return integer
     */
    public function getIdImgdesc()
    {
        return $this->idImgdesc;
    }
}
