<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * B2cRegcep
 *
 * @ORM\Table(name="b2c_regcep", indexes={@ORM\Index(name="cep", columns={"ini_cep", "fim_cep"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cRegcepRepository")
 */
class B2cRegcep
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cep", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCep;

    /**
     * @var \AppBundle\Entity\B2cRegreg
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\B2cRegreg", inversedBy="regCep")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_reg", referencedColumnName="id", nullable=true)
     * })
     */
    private $idReg;

    /**
     * @var string
     *
     * @ORM\Column(name="ini_cep", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $iniCep;

    /**
     * @var string
     *
     * @ORM\Column(name="fim_cep", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $fimCep;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\B2cTratra", mappedBy="idRegreg")
     */
    private $transportadoras;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transportadoras = new ArrayCollection();
    }

    /**
     * @param $idCep
     * @return $this
     */
    public function setIdCep($idCep)
    {
        $this->idCep = $idCep;
        return $this;
    }

    /**
     * Get idCep
     *
     * @return integer
     */
    public function getIdCep()
    {
        return $this->idCep;
    }

    /**
     * Set idReg
     *
     * @param integer $idReg
     *
     * @return B2cRegcep
     */
    public function setIdReg($idReg)
    {
        $this->idReg = $idReg;

        return $this;
    }

    /**
     * Get idReg
     *
     * @return integer
     */
    public function getIdReg()
    {
        return $this->idReg;
    }

    /**
     * Set iniCep
     *
     * @param string $iniCep
     *
     * @return B2cRegcep
     */
    public function setIniCep($iniCep)
    {
        $this->iniCep = $iniCep;

        return $this;
    }

    /**
     * Get iniCep
     *
     * @return string
     */
    public function getIniCep()
    {
        return $this->iniCep;
    }

    /**
     * Set fimCep
     *
     * @param string $fimCep
     *
     * @return B2cRegcep
     */
    public function setFimCep($fimCep)
    {
        $this->fimCep = $fimCep;

        return $this;
    }

    /**
     * Get fimCep
     *
     * @return string
     */
    public function getFimCep()
    {
        return $this->fimCep;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransportadoras()
    {
        return $this->transportadoras;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $transportadoras
     */
    public function setTransportadoras($transportadoras)
    {
        $this->transportadoras = $transportadoras;
    }

}
