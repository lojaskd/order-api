<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdprd
 *
 * @ORM\Table(name="b2c_prdprd", indexes={@ORM\Index(name="disponivel", columns={"disponivel"}), @ORM\Index(name="cor", columns={"cor"}), @ORM\Index(name="tamanho", columns={"tamanho"}), @ORM\Index(name="estilo", columns={"estilo"}), @ORM\Index(name="fornecedor", columns={"fornecedor"}), @ORM\Index(name="categoria", columns={"categoria"}), @ORM\Index(name="matrizes", columns={"matriz"}), @ORM\Index(name="cross_sale_disponivel", columns={"cross_sale_disponivel"}), @ORM\Index(name="use_auto_crosssale", columns={"ignorar_auto_crosssale"}), @ORM\Index(name="fila_atualizacao", columns={"fila_atualizacao"}), @ORM\Index(name="fila_inclusao", columns={"fila_inclusao"}), @ORM\Index(name="fila_exclusao", columns={"fila_exclusao"}), @ORM\Index(name="codigoerp", columns={"codigoerp"}), @ORM\Index(name="nomer_unico_idx", columns={"nome_unico"}), @ORM\Index(name="idx_pricebookentry", columns={"sf_pricebookentry"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPrdprdRepository")
 */
class B2cPrdprd
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="matriz", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $matriz;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_inclusao", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtaInclusao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_alteracao", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtaAlteracao;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_alteracao", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $usuarioAlteracao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fila_inclusao", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $filaInclusao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fila_atualizacao", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $filaAtualizacao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fila_exclusao", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $filaExclusao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="n_mostrar", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $nMostrar;

    /**
     * @var string
     *
     * @ORM\Column(name="referencia", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $referencia;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nome;

    /**
     * @var integer
     *
     * @ORM\Column(name="categoria", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $categoria;

    /**
     * @var integer
     *
     * @ORM\Column(name="fornecedor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $fornecedor;

    /**
     * @var string
     *
     * @ORM\Column(name="cross_sale_grupo_1", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $crossSaleGrupo1;

    /**
     * @var string
     *
     * @ORM\Column(name="cross_sale_grupo_2", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $crossSaleGrupo2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ignorar_auto_crosssale", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ignorarAutoCrosssale;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cross_sale_disponivel", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $crossSaleDisponivel;

    /**
     * @var string
     *
     * @ORM\Column(name="chamada", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $chamada;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="pchaves", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pchaves;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $link;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem_lst", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ordemLst;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem_prd", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ordemPrd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disponivel", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $disponivel;

    /**
     * @var integer
     *
     * @ORM\Column(name="disp_dias", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dispDias;

    /**
     * @var boolean
     *
     * @ORM\Column(name="destaque", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $destaque;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prevenda", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prevenda;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lancamento", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $lancamento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtiniciolancamento", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtiniciolancamento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtfimlancamento", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtfimlancamento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtchegada", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtchegada;

    /**
     * @var boolean
     *
     * @ORM\Column(name="presente", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $presente;

    /**
     * @var boolean
     *
     * @ORM\Column(name="maisvendido", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $maisvendido;

    /**
     * @var integer
     *
     * @ORM\Column(name="mais_vend_ordem", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $maisVendOrdem;

    /**
     * @var integer
     *
     * @ORM\Column(name="tamanho", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tamanho;

    /**
     * @var integer
     *
     * @ORM\Column(name="sexo", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $sexo;

    /**
     * @var integer
     *
     * @ORM\Column(name="cor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $cor;

    /**
     * @var integer
     *
     * @ORM\Column(name="estilo", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $estilo;

    /**
     * @var float
     *
     * @ORM\Column(name="peso", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $peso;

    /**
     * @var float
     *
     * @ORM\Column(name="cubagem", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $cubagem;

    /**
     * @var integer
     *
     * @ORM\Column(name="itens", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $itens;

    /**
     * @var integer
     *
     * @ORM\Column(name="volumes", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $volumes;

    /**
     * @var string
     *
     * @ORM\Column(name="estoque", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $estoque;

    /**
     * @var string
     *
     * @ORM\Column(name="stqminimo", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $stqminimo;

    /**
     * @var integer
     *
     * @ORM\Column(name="stqmin_acao", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $stqminAcao;

    /**
     * @var string
     *
     * @ORM\Column(name="stqfornecedor", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $stqfornecedor;

    /**
     * @var float
     *
     * @ORM\Column(name="custo", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $custo;

    /**
     * @var float
     *
     * @ORM\Column(name="icms", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $icms;

    /**
     * @var float
     *
     * @ORM\Column(name="pvreal", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pvreal;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_fornecedor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prazoFornecedor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="promocao", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $promocao;

    /**
     * @var float
     *
     * @ORM\Column(name="pvpromocao", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $pvpromocao;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto_valor", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $descontoValor;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto_perc", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $descontoPerc;

    /**
     * @var string
     *
     * @ORM\Column(name="use_desc", type="string", length=1, precision=0, scale=0, nullable=true, unique=false)
     */
    private $useDesc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="promocaoini", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $promocaoini;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="promocaofim", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $promocaofim;

    /**
     * @var string
     *
     * @ORM\Column(name="link_video", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $linkVideo;

    /**
     * @var string
     *
     * @ORM\Column(name="link_extra", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $linkExtra;

    /**
     * @var string
     *
     * @ORM\Column(name="cor_nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $corNome;

    /**
     * @var integer
     *
     * @ORM\Column(name="vnd_produto_1", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $vndProduto1;

    /**
     * @var float
     *
     * @ORM\Column(name="vnd_desconto_1", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $vndDesconto1;

    /**
     * @var integer
     *
     * @ORM\Column(name="vnd_produto_2", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $vndProduto2;

    /**
     * @var float
     *
     * @ORM\Column(name="vnd_desconto_2", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $vndDesconto2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ignorar_venda_junta", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ignorarVendaJunta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="produto_kit", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $produtoKit;

    /**
     * @var string
     *
     * @ORM\Column(name="lista_prd_kit", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $listaPrdKit;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_kit", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $descKit;

    /**
     * @var string
     *
     * @ORM\Column(name="codigoerp", type="string", length=16, precision=0, scale=0, nullable=true, unique=false)
     */
    private $codigoerp;

    /**
     * @var string
     *
     * @ORM\Column(name="unidade_fracionada", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $unidadeFracionada;

    /**
     * @var float
     *
     * @ORM\Column(name="altura", type="float", precision=9, scale=5, nullable=true, unique=false)
     */
    private $altura;

    /**
     * @var float
     *
     * @ORM\Column(name="largura", type="float", precision=9, scale=5, nullable=true, unique=false)
     */
    private $largura;

    /**
     * @var float
     *
     * @ORM\Column(name="profundidade", type="float", precision=9, scale=5, nullable=true, unique=false)
     */
    private $profundidade;

    /**
     * @var float
     *
     * @ORM\Column(name="diametro", type="float", precision=9, scale=5, nullable=true, unique=false)
     */
    private $diametro;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_unico", type="string", length=150, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nomeUnico;

    /**
     * @var boolean
     *
     * @ORM\Column(name="subsidiar_frete", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $subsidiarFrete;

    /**
     * @var string
     *
     * @ORM\Column(name="sf_product", type="string", length=18, precision=0, scale=0, nullable=true, unique=false)
     */
    private $sfProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="sf_pricebook", type="string", length=18, precision=0, scale=0, nullable=true, unique=false)
     */
    private $sfPricebook;

    /**
     * @var string
     *
     * @ORM\Column(name="sf_pricebookentry", type="string", length=18, precision=0, scale=0, nullable=true, unique=false)
     */
    private $sfPricebookentry;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lista_ambiente", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $listaAmbiente;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="negociacao_ini", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $negociacaoIni;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="negociacao_fim", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $negociacaoFim;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="saindo_de_linha_ini", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $saindoDeLinhaIni;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="saindo_de_linha_fim", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $saindoDeLinhaFim;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_lst", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tipoLst;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip_saindo_de_linha", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $tooltipSaindoDeLinha;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip_negociacao", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $tooltipNegociacao;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip_promocao", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $tooltipPromocao;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip_cor", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $tooltipCor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="relogio_promocao", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $relogioPromocao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="stqfornilimitado", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $stqfornilimitado;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matriz
     *
     * @param integer $matriz
     *
     * @return B2cPrdprd
     */
    public function setMatriz($matriz)
    {
        $this->matriz = $matriz;

        return $this;
    }

    /**
     * Get matriz
     *
     * @return integer
     */
    public function getMatriz()
    {
        return $this->matriz;
    }

    /**
     * Set dtaInclusao
     *
     * @param \DateTime $dtaInclusao
     *
     * @return B2cPrdprd
     */
    public function setDtaInclusao($dtaInclusao)
    {
        $this->dtaInclusao = $dtaInclusao;

        return $this;
    }

    /**
     * Get dtaInclusao
     *
     * @return \DateTime
     */
    public function getDtaInclusao()
    {
        return $this->dtaInclusao;
    }

    /**
     * Set dtaAlteracao
     *
     * @param \DateTime $dtaAlteracao
     *
     * @return B2cPrdprd
     */
    public function setDtaAlteracao($dtaAlteracao)
    {
        $this->dtaAlteracao = $dtaAlteracao;

        return $this;
    }

    /**
     * Get dtaAlteracao
     *
     * @return \DateTime
     */
    public function getDtaAlteracao()
    {
        return $this->dtaAlteracao;
    }

    /**
     * Set usuarioAlteracao
     *
     * @param string $usuarioAlteracao
     *
     * @return B2cPrdprd
     */
    public function setUsuarioAlteracao($usuarioAlteracao)
    {
        $this->usuarioAlteracao = $usuarioAlteracao;

        return $this;
    }

    /**
     * Get usuarioAlteracao
     *
     * @return string
     */
    public function getUsuarioAlteracao()
    {
        return $this->usuarioAlteracao;
    }

    /**
     * Set filaInclusao
     *
     * @param boolean $filaInclusao
     *
     * @return B2cPrdprd
     */
    public function setFilaInclusao($filaInclusao)
    {
        $this->filaInclusao = $filaInclusao;

        return $this;
    }

    /**
     * Get filaInclusao
     *
     * @return boolean
     */
    public function getFilaInclusao()
    {
        return $this->filaInclusao;
    }

    /**
     * Set filaAtualizacao
     *
     * @param boolean $filaAtualizacao
     *
     * @return B2cPrdprd
     */
    public function setFilaAtualizacao($filaAtualizacao)
    {
        $this->filaAtualizacao = $filaAtualizacao;

        return $this;
    }

    /**
     * Get filaAtualizacao
     *
     * @return boolean
     */
    public function getFilaAtualizacao()
    {
        return $this->filaAtualizacao;
    }

    /**
     * Set filaExclusao
     *
     * @param boolean $filaExclusao
     *
     * @return B2cPrdprd
     */
    public function setFilaExclusao($filaExclusao)
    {
        $this->filaExclusao = $filaExclusao;

        return $this;
    }

    /**
     * Get filaExclusao
     *
     * @return boolean
     */
    public function getFilaExclusao()
    {
        return $this->filaExclusao;
    }

    /**
     * Set nMostrar
     *
     * @param boolean $nMostrar
     *
     * @return B2cPrdprd
     */
    public function setNMostrar($nMostrar)
    {
        $this->nMostrar = $nMostrar;

        return $this;
    }

    /**
     * Get nMostrar
     *
     * @return boolean
     */
    public function getNMostrar()
    {
        return $this->nMostrar;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     *
     * @return B2cPrdprd
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cPrdprd
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set categoria
     *
     * @param integer $categoria
     *
     * @return B2cPrdprd
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return integer
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set fornecedor
     *
     * @param integer $fornecedor
     *
     * @return B2cPrdprd
     */
    public function setFornecedor($fornecedor)
    {
        $this->fornecedor = $fornecedor;

        return $this;
    }

    /**
     * Get fornecedor
     *
     * @return integer
     */
    public function getFornecedor()
    {
        return $this->fornecedor;
    }

    /**
     * Set crossSaleGrupo1
     *
     * @param string $crossSaleGrupo1
     *
     * @return B2cPrdprd
     */
    public function setCrossSaleGrupo1($crossSaleGrupo1)
    {
        $this->crossSaleGrupo1 = $crossSaleGrupo1;

        return $this;
    }

    /**
     * Get crossSaleGrupo1
     *
     * @return string
     */
    public function getCrossSaleGrupo1()
    {
        return $this->crossSaleGrupo1;
    }

    /**
     * Set crossSaleGrupo2
     *
     * @param string $crossSaleGrupo2
     *
     * @return B2cPrdprd
     */
    public function setCrossSaleGrupo2($crossSaleGrupo2)
    {
        $this->crossSaleGrupo2 = $crossSaleGrupo2;

        return $this;
    }

    /**
     * Get crossSaleGrupo2
     *
     * @return string
     */
    public function getCrossSaleGrupo2()
    {
        return $this->crossSaleGrupo2;
    }

    /**
     * Set ignorarAutoCrosssale
     *
     * @param boolean $ignorarAutoCrosssale
     *
     * @return B2cPrdprd
     */
    public function setIgnorarAutoCrosssale($ignorarAutoCrosssale)
    {
        $this->ignorarAutoCrosssale = $ignorarAutoCrosssale;

        return $this;
    }

    /**
     * Get ignorarAutoCrosssale
     *
     * @return boolean
     */
    public function getIgnorarAutoCrosssale()
    {
        return $this->ignorarAutoCrosssale;
    }

    /**
     * Set crossSaleDisponivel
     *
     * @param boolean $crossSaleDisponivel
     *
     * @return B2cPrdprd
     */
    public function setCrossSaleDisponivel($crossSaleDisponivel)
    {
        $this->crossSaleDisponivel = $crossSaleDisponivel;

        return $this;
    }

    /**
     * Get crossSaleDisponivel
     *
     * @return boolean
     */
    public function getCrossSaleDisponivel()
    {
        return $this->crossSaleDisponivel;
    }

    /**
     * Set chamada
     *
     * @param string $chamada
     *
     * @return B2cPrdprd
     */
    public function setChamada($chamada)
    {
        $this->chamada = $chamada;

        return $this;
    }

    /**
     * Get chamada
     *
     * @return string
     */
    public function getChamada()
    {
        return $this->chamada;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return B2cPrdprd
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set pchaves
     *
     * @param string $pchaves
     *
     * @return B2cPrdprd
     */
    public function setPchaves($pchaves)
    {
        $this->pchaves = $pchaves;

        return $this;
    }

    /**
     * Get pchaves
     *
     * @return string
     */
    public function getPchaves()
    {
        return $this->pchaves;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return B2cPrdprd
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set ordemLst
     *
     * @param integer $ordemLst
     *
     * @return B2cPrdprd
     */
    public function setOrdemLst($ordemLst)
    {
        $this->ordemLst = $ordemLst;

        return $this;
    }

    /**
     * Get ordemLst
     *
     * @return integer
     */
    public function getOrdemLst()
    {
        return $this->ordemLst;
    }

    /**
     * Set ordemPrd
     *
     * @param integer $ordemPrd
     *
     * @return B2cPrdprd
     */
    public function setOrdemPrd($ordemPrd)
    {
        $this->ordemPrd = $ordemPrd;

        return $this;
    }

    /**
     * Get ordemPrd
     *
     * @return integer
     */
    public function getOrdemPrd()
    {
        return $this->ordemPrd;
    }

    /**
     * Set disponivel
     *
     * @param boolean $disponivel
     *
     * @return B2cPrdprd
     */
    public function setDisponivel($disponivel)
    {
        $this->disponivel = $disponivel;

        return $this;
    }

    /**
     * Get disponivel
     *
     * @return boolean
     */
    public function getDisponivel()
    {
        return $this->disponivel;
    }

    /**
     * Set dispDias
     *
     * @param integer $dispDias
     *
     * @return B2cPrdprd
     */
    public function setDispDias($dispDias)
    {
        $this->dispDias = $dispDias;

        return $this;
    }

    /**
     * Get dispDias
     *
     * @return integer
     */
    public function getDispDias()
    {
        return $this->dispDias;
    }

    /**
     * Set destaque
     *
     * @param boolean $destaque
     *
     * @return B2cPrdprd
     */
    public function setDestaque($destaque)
    {
        $this->destaque = $destaque;

        return $this;
    }

    /**
     * Get destaque
     *
     * @return boolean
     */
    public function getDestaque()
    {
        return $this->destaque;
    }

    /**
     * Set prevenda
     *
     * @param boolean $prevenda
     *
     * @return B2cPrdprd
     */
    public function setPrevenda($prevenda)
    {
        $this->prevenda = $prevenda;

        return $this;
    }

    /**
     * Get prevenda
     *
     * @return boolean
     */
    public function getPrevenda()
    {
        return $this->prevenda;
    }

    /**
     * Set lancamento
     *
     * @param boolean $lancamento
     *
     * @return B2cPrdprd
     */
    public function setLancamento($lancamento)
    {
        $this->lancamento = $lancamento;

        return $this;
    }

    /**
     * Get lancamento
     *
     * @return boolean
     */
    public function getLancamento()
    {
        return $this->lancamento;
    }

    /**
     * Set dtiniciolancamento
     *
     * @param \DateTime $dtiniciolancamento
     *
     * @return B2cPrdprd
     */
    public function setDtiniciolancamento($dtiniciolancamento)
    {
        $this->dtiniciolancamento = $dtiniciolancamento;

        return $this;
    }

    /**
     * Get dtiniciolancamento
     *
     * @return \DateTime
     */
    public function getDtiniciolancamento()
    {
        return $this->dtiniciolancamento;
    }

    /**
     * Set dtfimlancamento
     *
     * @param \DateTime $dtfimlancamento
     *
     * @return B2cPrdprd
     */
    public function setDtfimlancamento($dtfimlancamento)
    {
        $this->dtfimlancamento = $dtfimlancamento;

        return $this;
    }

    /**
     * Get dtfimlancamento
     *
     * @return \DateTime
     */
    public function getDtfimlancamento()
    {
        return $this->dtfimlancamento;
    }

    /**
     * Set dtchegada
     *
     * @param \DateTime $dtchegada
     *
     * @return B2cPrdprd
     */
    public function setDtchegada($dtchegada)
    {
        $this->dtchegada = $dtchegada;

        return $this;
    }

    /**
     * Get dtchegada
     *
     * @return \DateTime
     */
    public function getDtchegada()
    {
        return $this->dtchegada;
    }

    /**
     * Set presente
     *
     * @param boolean $presente
     *
     * @return B2cPrdprd
     */
    public function setPresente($presente)
    {
        $this->presente = $presente;

        return $this;
    }

    /**
     * Get presente
     *
     * @return boolean
     */
    public function getPresente()
    {
        return $this->presente;
    }

    /**
     * Set maisvendido
     *
     * @param boolean $maisvendido
     *
     * @return B2cPrdprd
     */
    public function setMaisvendido($maisvendido)
    {
        $this->maisvendido = $maisvendido;

        return $this;
    }

    /**
     * Get maisvendido
     *
     * @return boolean
     */
    public function getMaisvendido()
    {
        return $this->maisvendido;
    }

    /**
     * Set maisVendOrdem
     *
     * @param integer $maisVendOrdem
     *
     * @return B2cPrdprd
     */
    public function setMaisVendOrdem($maisVendOrdem)
    {
        $this->maisVendOrdem = $maisVendOrdem;

        return $this;
    }

    /**
     * Get maisVendOrdem
     *
     * @return integer
     */
    public function getMaisVendOrdem()
    {
        return $this->maisVendOrdem;
    }

    /**
     * Set tamanho
     *
     * @param integer $tamanho
     *
     * @return B2cPrdprd
     */
    public function setTamanho($tamanho)
    {
        $this->tamanho = $tamanho;

        return $this;
    }

    /**
     * Get tamanho
     *
     * @return integer
     */
    public function getTamanho()
    {
        return $this->tamanho;
    }

    /**
     * Set sexo
     *
     * @param integer $sexo
     *
     * @return B2cPrdprd
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return integer
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set cor
     *
     * @param integer $cor
     *
     * @return B2cPrdprd
     */
    public function setCor($cor)
    {
        $this->cor = $cor;

        return $this;
    }

    /**
     * Get cor
     *
     * @return integer
     */
    public function getCor()
    {
        return $this->cor;
    }

    /**
     * Set estilo
     *
     * @param integer $estilo
     *
     * @return B2cPrdprd
     */
    public function setEstilo($estilo)
    {
        $this->estilo = $estilo;

        return $this;
    }

    /**
     * Get estilo
     *
     * @return integer
     */
    public function getEstilo()
    {
        return $this->estilo;
    }

    /**
     * Set peso
     *
     * @param float $peso
     *
     * @return B2cPrdprd
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return float
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set cubagem
     *
     * @param float $cubagem
     *
     * @return B2cPrdprd
     */
    public function setCubagem($cubagem)
    {
        $this->cubagem = $cubagem;

        return $this;
    }

    /**
     * Get cubagem
     *
     * @return float
     */
    public function getCubagem()
    {
        return $this->cubagem;
    }

    /**
     * Set itens
     *
     * @param integer $itens
     *
     * @return B2cPrdprd
     */
    public function setItens($itens)
    {
        $this->itens = $itens;

        return $this;
    }

    /**
     * Get itens
     *
     * @return integer
     */
    public function getItens()
    {
        return $this->itens;
    }

    /**
     * Set volumes
     *
     * @param integer $volumes
     *
     * @return B2cPrdprd
     */
    public function setVolumes($volumes)
    {
        $this->volumes = $volumes;

        return $this;
    }

    /**
     * Get volumes
     *
     * @return integer
     */
    public function getVolumes()
    {
        return $this->volumes;
    }

    /**
     * Set estoque
     *
     * @param string $estoque
     *
     * @return B2cPrdprd
     */
    public function setEstoque($estoque)
    {
        $this->estoque = $estoque;

        return $this;
    }

    /**
     * Get estoque
     *
     * @return string
     */
    public function getEstoque()
    {
        return $this->estoque;
    }

    /**
     * Set stqminimo
     *
     * @param string $stqminimo
     *
     * @return B2cPrdprd
     */
    public function setStqminimo($stqminimo)
    {
        $this->stqminimo = $stqminimo;

        return $this;
    }

    /**
     * Get stqminimo
     *
     * @return string
     */
    public function getStqminimo()
    {
        return $this->stqminimo;
    }

    /**
     * Set stqminAcao
     *
     * @param integer $stqminAcao
     *
     * @return B2cPrdprd
     */
    public function setStqminAcao($stqminAcao)
    {
        $this->stqminAcao = $stqminAcao;

        return $this;
    }

    /**
     * Get stqminAcao
     *
     * @return integer
     */
    public function getStqminAcao()
    {
        return $this->stqminAcao;
    }

    /**
     * Set stqfornecedor
     *
     * @param string $stqfornecedor
     *
     * @return B2cPrdprd
     */
    public function setStqfornecedor($stqfornecedor)
    {
        $this->stqfornecedor = $stqfornecedor;

        return $this;
    }

    /**
     * Get stqfornecedor
     *
     * @return string
     */
    public function getStqfornecedor()
    {
        return $this->stqfornecedor;
    }

    /**
     * Set custo
     *
     * @param float $custo
     *
     * @return B2cPrdprd
     */
    public function setCusto($custo)
    {
        $this->custo = $custo;

        return $this;
    }

    /**
     * Get custo
     *
     * @return float
     */
    public function getCusto()
    {
        return $this->custo;
    }

    /**
     * Set icms
     *
     * @param float $icms
     *
     * @return B2cPrdprd
     */
    public function setIcms($icms)
    {
        $this->icms = $icms;

        return $this;
    }

    /**
     * Get icms
     *
     * @return float
     */
    public function getIcms()
    {
        return $this->icms;
    }

    /**
     * Set pvreal
     *
     * @param float $pvreal
     *
     * @return B2cPrdprd
     */
    public function setPvreal($pvreal)
    {
        $this->pvreal = $pvreal;

        return $this;
    }

    /**
     * Get pvreal
     *
     * @return float
     */
    public function getPvreal()
    {
        return $this->pvreal;
    }

    /**
     * Set prazoFornecedor
     *
     * @param integer $prazoFornecedor
     *
     * @return B2cPrdprd
     */
    public function setPrazoFornecedor($prazoFornecedor)
    {
        $this->prazoFornecedor = $prazoFornecedor;

        return $this;
    }

    /**
     * Get prazoFornecedor
     *
     * @return integer
     */
    public function getPrazoFornecedor()
    {
        return $this->prazoFornecedor;
    }

    /**
     * Set promocao
     *
     * @param boolean $promocao
     *
     * @return B2cPrdprd
     */
    public function setPromocao($promocao)
    {
        $this->promocao = $promocao;

        return $this;
    }

    /**
     * Get promocao
     *
     * @return boolean
     */
    public function getPromocao()
    {
        return $this->promocao;
    }

    /**
     * Set pvpromocao
     *
     * @param float $pvpromocao
     *
     * @return B2cPrdprd
     */
    public function setPvpromocao($pvpromocao)
    {
        $this->pvpromocao = $pvpromocao;

        return $this;
    }

    /**
     * Get pvpromocao
     *
     * @return float
     */
    public function getPvpromocao()
    {
        return $this->pvpromocao;
    }

    /**
     * Set descontoValor
     *
     * @param float $descontoValor
     *
     * @return B2cPrdprd
     */
    public function setDescontoValor($descontoValor)
    {
        $this->descontoValor = $descontoValor;

        return $this;
    }

    /**
     * Get descontoValor
     *
     * @return float
     */
    public function getDescontoValor()
    {
        return $this->descontoValor;
    }

    /**
     * Set descontoPerc
     *
     * @param float $descontoPerc
     *
     * @return B2cPrdprd
     */
    public function setDescontoPerc($descontoPerc)
    {
        $this->descontoPerc = $descontoPerc;

        return $this;
    }

    /**
     * Get descontoPerc
     *
     * @return float
     */
    public function getDescontoPerc()
    {
        return $this->descontoPerc;
    }

    /**
     * Set useDesc
     *
     * @param string $useDesc
     *
     * @return B2cPrdprd
     */
    public function setUseDesc($useDesc)
    {
        $this->useDesc = $useDesc;

        return $this;
    }

    /**
     * Get useDesc
     *
     * @return string
     */
    public function getUseDesc()
    {
        return $this->useDesc;
    }

    /**
     * Set promocaoini
     *
     * @param \DateTime $promocaoini
     *
     * @return B2cPrdprd
     */
    public function setPromocaoini($promocaoini)
    {
        $this->promocaoini = $promocaoini;

        return $this;
    }

    /**
     * Get promocaoini
     *
     * @return \DateTime
     */
    public function getPromocaoini()
    {
        return $this->promocaoini;
    }

    /**
     * Set promocaofim
     *
     * @param \DateTime $promocaofim
     *
     * @return B2cPrdprd
     */
    public function setPromocaofim($promocaofim)
    {
        $this->promocaofim = $promocaofim;

        return $this;
    }

    /**
     * Get promocaofim
     *
     * @return \DateTime
     */
    public function getPromocaofim()
    {
        return $this->promocaofim;
    }

    /**
     * Set linkVideo
     *
     * @param string $linkVideo
     *
     * @return B2cPrdprd
     */
    public function setLinkVideo($linkVideo)
    {
        $this->linkVideo = $linkVideo;

        return $this;
    }

    /**
     * Get linkVideo
     *
     * @return string
     */
    public function getLinkVideo()
    {
        return $this->linkVideo;
    }

    /**
     * Set linkExtra
     *
     * @param string $linkExtra
     *
     * @return B2cPrdprd
     */
    public function setLinkExtra($linkExtra)
    {
        $this->linkExtra = $linkExtra;

        return $this;
    }

    /**
     * Get linkExtra
     *
     * @return string
     */
    public function getLinkExtra()
    {
        return $this->linkExtra;
    }

    /**
     * Set corNome
     *
     * @param string $corNome
     *
     * @return B2cPrdprd
     */
    public function setCorNome($corNome)
    {
        $this->corNome = $corNome;

        return $this;
    }

    /**
     * Get corNome
     *
     * @return string
     */
    public function getCorNome()
    {
        return $this->corNome;
    }

    /**
     * Set vndProduto1
     *
     * @param integer $vndProduto1
     *
     * @return B2cPrdprd
     */
    public function setVndProduto1($vndProduto1)
    {
        $this->vndProduto1 = $vndProduto1;

        return $this;
    }

    /**
     * Get vndProduto1
     *
     * @return integer
     */
    public function getVndProduto1()
    {
        return $this->vndProduto1;
    }

    /**
     * Set vndDesconto1
     *
     * @param float $vndDesconto1
     *
     * @return B2cPrdprd
     */
    public function setVndDesconto1($vndDesconto1)
    {
        $this->vndDesconto1 = $vndDesconto1;

        return $this;
    }

    /**
     * Get vndDesconto1
     *
     * @return float
     */
    public function getVndDesconto1()
    {
        return $this->vndDesconto1;
    }

    /**
     * Set vndProduto2
     *
     * @param integer $vndProduto2
     *
     * @return B2cPrdprd
     */
    public function setVndProduto2($vndProduto2)
    {
        $this->vndProduto2 = $vndProduto2;

        return $this;
    }

    /**
     * Get vndProduto2
     *
     * @return integer
     */
    public function getVndProduto2()
    {
        return $this->vndProduto2;
    }

    /**
     * Set vndDesconto2
     *
     * @param float $vndDesconto2
     *
     * @return B2cPrdprd
     */
    public function setVndDesconto2($vndDesconto2)
    {
        $this->vndDesconto2 = $vndDesconto2;

        return $this;
    }

    /**
     * Get vndDesconto2
     *
     * @return float
     */
    public function getVndDesconto2()
    {
        return $this->vndDesconto2;
    }

    /**
     * Set ignorarVendaJunta
     *
     * @param boolean $ignorarVendaJunta
     *
     * @return B2cPrdprd
     */
    public function setIgnorarVendaJunta($ignorarVendaJunta)
    {
        $this->ignorarVendaJunta = $ignorarVendaJunta;

        return $this;
    }

    /**
     * Get ignorarVendaJunta
     *
     * @return boolean
     */
    public function getIgnorarVendaJunta()
    {
        return $this->ignorarVendaJunta;
    }

    /**
     * Set produtoKit
     *
     * @param boolean $produtoKit
     *
     * @return B2cPrdprd
     */
    public function setProdutoKit($produtoKit)
    {
        $this->produtoKit = $produtoKit;

        return $this;
    }

    /**
     * Get produtoKit
     *
     * @return boolean
     */
    public function getProdutoKit()
    {
        return $this->produtoKit;
    }

    /**
     * Set listaPrdKit
     *
     * @param string $listaPrdKit
     *
     * @return B2cPrdprd
     */
    public function setListaPrdKit($listaPrdKit)
    {
        $this->listaPrdKit = $listaPrdKit;

        return $this;
    }

    /**
     * Get listaPrdKit
     *
     * @return string
     */
    public function getListaPrdKit()
    {
        return $this->listaPrdKit;
    }

    /**
     * Set descKit
     *
     * @param string $descKit
     *
     * @return B2cPrdprd
     */
    public function setDescKit($descKit)
    {
        $this->descKit = $descKit;

        return $this;
    }

    /**
     * Get descKit
     *
     * @return string
     */
    public function getDescKit()
    {
        return $this->descKit;
    }

    /**
     * Set codigoerp
     *
     * @param string $codigoerp
     *
     * @return B2cPrdprd
     */
    public function setCodigoerp($codigoerp)
    {
        $this->codigoerp = $codigoerp;

        return $this;
    }

    /**
     * Get codigoerp
     *
     * @return string
     */
    public function getCodigoerp()
    {
        return $this->codigoerp;
    }

    /**
     * Set unidadeFracionada
     *
     * @param string $unidadeFracionada
     *
     * @return B2cPrdprd
     */
    public function setUnidadeFracionada($unidadeFracionada)
    {
        $this->unidadeFracionada = $unidadeFracionada;

        return $this;
    }

    /**
     * Get unidadeFracionada
     *
     * @return string
     */
    public function getUnidadeFracionada()
    {
        return $this->unidadeFracionada;
    }

    /**
     * Set altura
     *
     * @param float $altura
     *
     * @return B2cPrdprd
     */
    public function setAltura($altura)
    {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get altura
     *
     * @return float
     */
    public function getAltura()
    {
        return $this->altura;
    }

    /**
     * Set largura
     *
     * @param float $largura
     *
     * @return B2cPrdprd
     */
    public function setLargura($largura)
    {
        $this->largura = $largura;

        return $this;
    }

    /**
     * Get largura
     *
     * @return float
     */
    public function getLargura()
    {
        return $this->largura;
    }

    /**
     * Set profundidade
     *
     * @param float $profundidade
     *
     * @return B2cPrdprd
     */
    public function setProfundidade($profundidade)
    {
        $this->profundidade = $profundidade;

        return $this;
    }

    /**
     * Get profundidade
     *
     * @return float
     */
    public function getProfundidade()
    {
        return $this->profundidade;
    }

    /**
     * Set diametro
     *
     * @param float $diametro
     *
     * @return B2cPrdprd
     */
    public function setDiametro($diametro)
    {
        $this->diametro = $diametro;

        return $this;
    }

    /**
     * Get diametro
     *
     * @return float
     */
    public function getDiametro()
    {
        return $this->diametro;
    }

    /**
     * Set nomeUnico
     *
     * @param string $nomeUnico
     *
     * @return B2cPrdprd
     */
    public function setNomeUnico($nomeUnico)
    {
        $this->nomeUnico = $nomeUnico;

        return $this;
    }

    /**
     * Get nomeUnico
     *
     * @return string
     */
    public function getNomeUnico()
    {
        return $this->nomeUnico;
    }

    /**
     * Set subsidiarFrete
     *
     * @param boolean $subsidiarFrete
     *
     * @return B2cPrdprd
     */
    public function setSubsidiarFrete($subsidiarFrete)
    {
        $this->subsidiarFrete = $subsidiarFrete;

        return $this;
    }

    /**
     * Get subsidiarFrete
     *
     * @return boolean
     */
    public function getSubsidiarFrete()
    {
        return $this->subsidiarFrete;
    }

    /**
     * Set sfProduct
     *
     * @param string $sfProduct
     *
     * @return B2cPrdprd
     */
    public function setSfProduct($sfProduct)
    {
        $this->sfProduct = $sfProduct;

        return $this;
    }

    /**
     * Get sfProduct
     *
     * @return string
     */
    public function getSfProduct()
    {
        return $this->sfProduct;
    }

    /**
     * Set sfPricebook
     *
     * @param string $sfPricebook
     *
     * @return B2cPrdprd
     */
    public function setSfPricebook($sfPricebook)
    {
        $this->sfPricebook = $sfPricebook;

        return $this;
    }

    /**
     * Get sfPricebook
     *
     * @return string
     */
    public function getSfPricebook()
    {
        return $this->sfPricebook;
    }

    /**
     * Set sfPricebookentry
     *
     * @param string $sfPricebookentry
     *
     * @return B2cPrdprd
     */
    public function setSfPricebookentry($sfPricebookentry)
    {
        $this->sfPricebookentry = $sfPricebookentry;

        return $this;
    }

    /**
     * Get sfPricebookentry
     *
     * @return string
     */
    public function getSfPricebookentry()
    {
        return $this->sfPricebookentry;
    }

    /**
     * Set listaAmbiente
     *
     * @param boolean $listaAmbiente
     *
     * @return B2cPrdprd
     */
    public function setListaAmbiente($listaAmbiente)
    {
        $this->listaAmbiente = $listaAmbiente;

        return $this;
    }

    /**
     * Get listaAmbiente
     *
     * @return boolean
     */
    public function getListaAmbiente()
    {
        return $this->listaAmbiente;
    }

    /**
     * Set negociacaoIni
     *
     * @param \DateTime $negociacaoIni
     *
     * @return B2cPrdprd
     */
    public function setNegociacaoIni($negociacaoIni)
    {
        $this->negociacaoIni = $negociacaoIni;

        return $this;
    }

    /**
     * Get negociacaoIni
     *
     * @return \DateTime
     */
    public function getNegociacaoIni()
    {
        return $this->negociacaoIni;
    }

    /**
     * Set negociacaoFim
     *
     * @param \DateTime $negociacaoFim
     *
     * @return B2cPrdprd
     */
    public function setNegociacaoFim($negociacaoFim)
    {
        $this->negociacaoFim = $negociacaoFim;

        return $this;
    }

    /**
     * Get negociacaoFim
     *
     * @return \DateTime
     */
    public function getNegociacaoFim()
    {
        return $this->negociacaoFim;
    }

    /**
     * Set saindoDeLinhaIni
     *
     * @param \DateTime $saindoDeLinhaIni
     *
     * @return B2cPrdprd
     */
    public function setSaindoDeLinhaIni($saindoDeLinhaIni)
    {
        $this->saindoDeLinhaIni = $saindoDeLinhaIni;

        return $this;
    }

    /**
     * Get saindoDeLinhaIni
     *
     * @return \DateTime
     */
    public function getSaindoDeLinhaIni()
    {
        return $this->saindoDeLinhaIni;
    }

    /**
     * Set saindoDeLinhaFim
     *
     * @param \DateTime $saindoDeLinhaFim
     *
     * @return B2cPrdprd
     */
    public function setSaindoDeLinhaFim($saindoDeLinhaFim)
    {
        $this->saindoDeLinhaFim = $saindoDeLinhaFim;

        return $this;
    }

    /**
     * Get saindoDeLinhaFim
     *
     * @return \DateTime
     */
    public function getSaindoDeLinhaFim()
    {
        return $this->saindoDeLinhaFim;
    }

    /**
     * Set tipoLst
     *
     * @param string $tipoLst
     *
     * @return B2cPrdprd
     */
    public function setTipoLst($tipoLst)
    {
        $this->tipoLst = $tipoLst;

        return $this;
    }

    /**
     * Get tipoLst
     *
     * @return string
     */
    public function getTipoLst()
    {
        return $this->tipoLst;
    }

    /**
     * Set tooltipSaindoDeLinha
     *
     * @param string $tooltipSaindoDeLinha
     *
     * @return B2cPrdprd
     */
    public function setTooltipSaindoDeLinha($tooltipSaindoDeLinha)
    {
        $this->tooltipSaindoDeLinha = $tooltipSaindoDeLinha;

        return $this;
    }

    /**
     * Get tooltipSaindoDeLinha
     *
     * @return string
     */
    public function getTooltipSaindoDeLinha()
    {
        return $this->tooltipSaindoDeLinha;
    }

    /**
     * Set tooltipNegociacao
     *
     * @param string $tooltipNegociacao
     *
     * @return B2cPrdprd
     */
    public function setTooltipNegociacao($tooltipNegociacao)
    {
        $this->tooltipNegociacao = $tooltipNegociacao;

        return $this;
    }

    /**
     * Get tooltipNegociacao
     *
     * @return string
     */
    public function getTooltipNegociacao()
    {
        return $this->tooltipNegociacao;
    }

    /**
     * Set tooltipPromocao
     *
     * @param string $tooltipPromocao
     *
     * @return B2cPrdprd
     */
    public function setTooltipPromocao($tooltipPromocao)
    {
        $this->tooltipPromocao = $tooltipPromocao;

        return $this;
    }

    /**
     * Get tooltipPromocao
     *
     * @return string
     */
    public function getTooltipPromocao()
    {
        return $this->tooltipPromocao;
    }

    /**
     * Set tooltipCor
     *
     * @param string $tooltipCor
     *
     * @return B2cPrdprd
     */
    public function setTooltipCor($tooltipCor)
    {
        $this->tooltipCor = $tooltipCor;

        return $this;
    }

    /**
     * Get tooltipCor
     *
     * @return string
     */
    public function getTooltipCor()
    {
        return $this->tooltipCor;
    }

    /**
     * Set relogioPromocao
     *
     * @param boolean $relogioPromocao
     *
     * @return B2cPrdprd
     */
    public function setRelogioPromocao($relogioPromocao)
    {
        $this->relogioPromocao = $relogioPromocao;

        return $this;
    }

    /**
     * Get relogioPromocao
     *
     * @return boolean
     */
    public function getRelogioPromocao()
    {
        return $this->relogioPromocao;
    }

    /**
     * Set stqfornilimitado
     *
     * @param boolean $stqfornilimitado
     *
     * @return B2cPrdprd
     */
    public function setStqfornilimitado($stqfornilimitado)
    {
        $this->stqfornilimitado = $stqfornilimitado;

        return $this;
    }

    /**
     * Get stqfornilimitado
     *
     * @return boolean
     */
    public function getStqfornilimitado()
    {
        return $this->stqfornilimitado;
    }
}
