<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cFormasEstorno
 *
 * @ORM\Table(name="b2c_formas_estorno")
 * @ORM\Entity
 */
class B2cFormasEstorno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_forest", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idForest;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", nullable=false)
     */
    private $ativo;

    /**
     * @param $idForest
     * @return $this
     */
    public function setIdForest($idForest)
    {
        $this->idForest = $idForest;
        return $this;
    }

    /**
     * Get idForest
     *
     * @return integer
     */
    public function getIdForest()
    {
        return $this->idForest;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cFormasEstorno
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return B2cFormasEstorno
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }
}
