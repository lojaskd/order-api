<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cRegmec
 *
 * @ORM\Table(name="b2c_regmec", indexes={@ORM\Index(name="idx_mec", columns={"id", "ini_mc", "fim_mc"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cRegmecRepository")
 */
class B2cRegmec
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_trans", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idTrans;

    /**
     * @var float
     *
     * @ORM\Column(name="ini_mc", type="float", precision=9, scale=5, nullable=false, unique=false)
     */
    private $iniMc;

    /**
     * @var float
     *
     * @ORM\Column(name="fim_mc", type="float", precision=9, scale=5, nullable=false, unique=false)
     */
    private $fimMc;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTrans
     *
     * @param integer $idTrans
     *
     * @return B2cRegmec
     */
    public function setIdTrans($idTrans)
    {
        $this->idTrans = $idTrans;

        return $this;
    }

    /**
     * Get idTrans
     *
     * @return integer
     */
    public function getIdTrans()
    {
        return $this->idTrans;
    }

    /**
     * Set iniMc
     *
     * @param float $iniMc
     *
     * @return B2cRegmec
     */
    public function setIniMc($iniMc)
    {
        $this->iniMc = $iniMc;

        return $this;
    }

    /**
     * Get iniMc
     *
     * @return float
     */
    public function getIniMc()
    {
        return $this->iniMc;
    }

    /**
     * Set fimMc
     *
     * @param float $fimMc
     *
     * @return B2cRegmec
     */
    public function setFimMc($fimMc)
    {
        $this->fimMc = $fimMc;

        return $this;
    }

    /**
     * Get fimMc
     *
     * @return float
     */
    public function getFimMc()
    {
        return $this->fimMc;
    }
}
