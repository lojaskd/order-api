<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * B2cProcedaFileLines
 *
 * @ORM\Table(name="b2c_proceda_file_lines")
 * @ORM\Entity
 */
class B2cProcedaFileLines
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cnpj", type="string", length=45, nullable=true)
     */
    private $cnpj;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_id", type="integer", length=45, nullable=true)
     */
    private $invoiceId;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_serie", type="string", length=45, nullable=true)
     */
    private $invoiceSerie;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=45, nullable=true)
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="obs_code", type="string", length=45, nullable=true)
     */
    private $obsCode;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=254, nullable=true)
     */
    private $text;

    /**
     * @var integer
     *
     * @ORM\Column(name="proceda_file_id", type="integer", nullable=true)
     */
    private $procedaFileId;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", nullable=true)
     */
    private $idPedped;

    /**
     * @var string
     * @Exclude()
     *
     * @ORM\Column(name="log", type="blob", length=65535, nullable=true)
     */
    private $log;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\B2cProcedaFiles")
     * @ORM\JoinColumn(name="proceda_file_id", referencedColumnName="id")
     */
    private $file;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cnpj
     *
     * @param string $cnpj
     *
     * @return B2cProcedaFileLines
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Get cnpj
     *
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Set invoiceId
     *
     * @param integer $invoiceId
     *
     * @return B2cProcedaFileLines
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * Get invoiceId
     *
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * Set invoiceSerie
     *
     * @param string $invoiceSerie
     *
     * @return B2cProcedaFileLines
     */
    public function setInvoiceSerie($invoiceSerie)
    {
        $this->invoiceSerie = $invoiceSerie;

        return $this;
    }

    /**
     * Get invoiceSerie
     *
     * @return string
     */
    public function getInvoiceSerie()
    {
        return $this->invoiceSerie;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return B2cProcedaFileLines
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return B2cProcedaFileLines
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return B2cProcedaFileLines
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return B2cProcedaFileLines
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set obsCode
     *
     * @param string $obsCode
     *
     * @return B2cProcedaFileLines
     */
    public function setObsCode($obsCode)
    {
        $this->obsCode = $obsCode;

        return $this;
    }

    /**
     * Get obsCode
     *
     * @return string
     */
    public function getObsCode()
    {
        return $this->obsCode;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return B2cProcedaFileLines
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set procedaFileId
     *
     * @param integer $procedaFileId
     *
     * @return B2cProcedaFileLines
     */
    public function setProcedaFileId($procedaFileId)
    {
        $this->procedaFileId = $procedaFileId;

        return $this;
    }

    /**
     * Get procedaFileId
     *
     * @return integer
     */
    public function getProcedaFileId()
    {
        return $this->procedaFileId;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return B2cProcedaFileLines
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idPedped
     *
     * @param integer $idPedped
     *
     * @return B2cProcedaFileLines
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;

        return $this;
    }

    /**
     * Get idPedped
     *
     * @return integer
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * Set log
     *
     * @param string $log
     *
     * @return B2cProcedaFileLines
     */
    public function setLog($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * Get log
     *
     * @return string
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

}
