<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromo
 *
 * @ORM\Table(name="b2c_promo", indexes={@ORM\Index(name="ativo", columns={"ativo"}), @ORM\Index(name="dta_validade_inicio", columns={"dta_validade_inicio"}), @ORM\Index(name="dta_validade_fim", columns={"dta_validade_fim"}), @ORM\Index(name="hra_validade_inicio", columns={"hra_validade_inicio"}), @ORM\Index(name="hra_validade_fim", columns={"hra_validade_fim"}), @ORM\Index(name="vale_compra", columns={"vale_compra"}), @ORM\Index(name="prioridade", columns={"prioridade"}), @ORM\Index(name="sorteio", columns={"sorteio"}), @ORM\Index(name="aplicar_cliente", columns={"aplicar_cliente"}), @ORM\Index(name="aplicar_produtos", columns={"aplicar_produtos"}), @ORM\Index(name="aplicar_pagamento", columns={"aplicar_pagamento"}), @ORM\Index(name="aplicar_entrega", columns={"aplicar_entrega"}), @ORM\Index(name="idx_pgt_cartao", columns={"ativo", "vale_compra", "id_promo"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPromoRepository")
 */
class B2cPromo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPromo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ativo;

    /**
     * @var integer
     *
     * @ORM\Column(name="prioridade", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prioridade;

    /**
     * @var boolean
     *
     * @ORM\Column(name="vale_compra", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $valeCompra;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_inclusao", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtaInclusao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_alteracao", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtaAlteracao;

    /**
     * @var string
     *
     * @ORM\Column(name="responsavel", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $responsavel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_validade_inicio", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtaValidadeInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hra_validade_inicio", type="time", precision=0, scale=0, nullable=false, unique=false)
     */
    private $hraValidadeInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_validade_fim", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtaValidadeFim;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hra_validade_fim", type="time", precision=0, scale=0, nullable=false, unique=false)
     */
    private $hraValidadeFim;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao_simples", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $descricaoSimples;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao_completa", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $descricaoCompleta;

    /**
     * @var string
     *
     * @ORM\Column(name="img_listagem", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $imgListagem;

    /**
     * @var string
     *
     * @ORM\Column(name="img_descricao", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $imgDescricao;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto_perc", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $descontoPerc;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto_valor", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $descontoValor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="frete_gratis", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $freteGratis;

    /**
     * @var boolean
     *
     * @ORM\Column(name="frete_fixo", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $freteFixo;

    /**
     * @var float
     *
     * @ORM\Column(name="frete_fixo_valor", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $freteFixoValor;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_minimo", type="float", precision=9, scale=2, nullable=false, unique=false)
     */
    private $valorMinimo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usar_todaloja", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $usarTodaloja;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usar_produtos", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $usarProdutos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usar_situacao", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $usarSituacao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usar_categoria", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $usarCategoria;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usar_fornecedor", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $usarFornecedor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aplicar_cliente", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $aplicarCliente;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aplicar_produtos", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $aplicarProdutos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aplicar_pagamento", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $aplicarPagamento;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aplicar_entrega", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $aplicarEntrega;

    /**
     * @var boolean
     *
     * @ORM\Column(name="promocao_brinde", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $promocaoBrinde;

    /**
     * @var boolean
     *
     * @ORM\Column(name="aplicar_carrinho", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $aplicarCarrinho;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_exato", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $valorExato;

    /**
     * @var string
     *
     * @ORM\Column(name="qtd_minima", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $qtdMinima;

    /**
     * @var string
     *
     * @ORM\Column(name="qtd_exata", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $qtdExata;

    /**
     * @var integer
     *
     * @ORM\Column(name="qtd_brindes", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $qtdBrindes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sorteio", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $sorteio;

    /**
     * @var boolean
     *
     * @ORM\Column(name="promo_inclui_frete", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $promoIncluiFrete;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_vale", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tipoVale;

    /**
     * @param $idPromo
     * @return $this
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;
        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return B2cPromo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set prioridade
     *
     * @param integer $prioridade
     *
     * @return B2cPromo
     */
    public function setPrioridade($prioridade)
    {
        $this->prioridade = $prioridade;

        return $this;
    }

    /**
     * Get prioridade
     *
     * @return integer
     */
    public function getPrioridade()
    {
        return $this->prioridade;
    }

    /**
     * Set valeCompra
     *
     * @param boolean $valeCompra
     *
     * @return B2cPromo
     */
    public function setValeCompra($valeCompra)
    {
        $this->valeCompra = $valeCompra;

        return $this;
    }

    /**
     * Get valeCompra
     *
     * @return boolean
     */
    public function getValeCompra()
    {
        return $this->valeCompra;
    }

    /**
     * Set dtaInclusao
     *
     * @param \DateTime $dtaInclusao
     *
     * @return B2cPromo
     */
    public function setDtaInclusao($dtaInclusao)
    {
        $this->dtaInclusao = $dtaInclusao;

        return $this;
    }

    /**
     * Get dtaInclusao
     *
     * @return \DateTime
     */
    public function getDtaInclusao()
    {
        return $this->dtaInclusao;
    }

    /**
     * Set dtaAlteracao
     *
     * @param \DateTime $dtaAlteracao
     *
     * @return B2cPromo
     */
    public function setDtaAlteracao($dtaAlteracao)
    {
        $this->dtaAlteracao = $dtaAlteracao;

        return $this;
    }

    /**
     * Get dtaAlteracao
     *
     * @return \DateTime
     */
    public function getDtaAlteracao()
    {
        return $this->dtaAlteracao;
    }

    /**
     * Set responsavel
     *
     * @param string $responsavel
     *
     * @return B2cPromo
     */
    public function setResponsavel($responsavel)
    {
        $this->responsavel = $responsavel;

        return $this;
    }

    /**
     * Get responsavel
     *
     * @return string
     */
    public function getResponsavel()
    {
        return $this->responsavel;
    }

    /**
     * Set dtaValidadeInicio
     *
     * @param \DateTime $dtaValidadeInicio
     *
     * @return B2cPromo
     */
    public function setDtaValidadeInicio($dtaValidadeInicio)
    {
        $this->dtaValidadeInicio = $dtaValidadeInicio;

        return $this;
    }

    /**
     * Get dtaValidadeInicio
     *
     * @return \DateTime
     */
    public function getDtaValidadeInicio()
    {
        return $this->dtaValidadeInicio;
    }

    /**
     * Set hraValidadeInicio
     *
     * @param \DateTime $hraValidadeInicio
     *
     * @return B2cPromo
     */
    public function setHraValidadeInicio($hraValidadeInicio)
    {
        $this->hraValidadeInicio = $hraValidadeInicio;

        return $this;
    }

    /**
     * Get hraValidadeInicio
     *
     * @return \DateTime
     */
    public function getHraValidadeInicio()
    {
        return $this->hraValidadeInicio;
    }

    /**
     * Set dtaValidadeFim
     *
     * @param \DateTime $dtaValidadeFim
     *
     * @return B2cPromo
     */
    public function setDtaValidadeFim($dtaValidadeFim)
    {
        $this->dtaValidadeFim = $dtaValidadeFim;

        return $this;
    }

    /**
     * Get dtaValidadeFim
     *
     * @return \DateTime
     */
    public function getDtaValidadeFim()
    {
        return $this->dtaValidadeFim;
    }

    /**
     * Set hraValidadeFim
     *
     * @param \DateTime $hraValidadeFim
     *
     * @return B2cPromo
     */
    public function setHraValidadeFim($hraValidadeFim)
    {
        $this->hraValidadeFim = $hraValidadeFim;

        return $this;
    }

    /**
     * Get hraValidadeFim
     *
     * @return \DateTime
     */
    public function getHraValidadeFim()
    {
        return $this->hraValidadeFim;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cPromo
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descricaoSimples
     *
     * @param string $descricaoSimples
     *
     * @return B2cPromo
     */
    public function setDescricaoSimples($descricaoSimples)
    {
        $this->descricaoSimples = $descricaoSimples;

        return $this;
    }

    /**
     * Get descricaoSimples
     *
     * @return string
     */
    public function getDescricaoSimples()
    {
        return $this->descricaoSimples;
    }

    /**
     * Set descricaoCompleta
     *
     * @param string $descricaoCompleta
     *
     * @return B2cPromo
     */
    public function setDescricaoCompleta($descricaoCompleta)
    {
        $this->descricaoCompleta = $descricaoCompleta;

        return $this;
    }

    /**
     * Get descricaoCompleta
     *
     * @return string
     */
    public function getDescricaoCompleta()
    {
        return $this->descricaoCompleta;
    }

    /**
     * Set imgListagem
     *
     * @param string $imgListagem
     *
     * @return B2cPromo
     */
    public function setImgListagem($imgListagem)
    {
        $this->imgListagem = $imgListagem;

        return $this;
    }

    /**
     * Get imgListagem
     *
     * @return string
     */
    public function getImgListagem()
    {
        return $this->imgListagem;
    }

    /**
     * Set imgDescricao
     *
     * @param string $imgDescricao
     *
     * @return B2cPromo
     */
    public function setImgDescricao($imgDescricao)
    {
        $this->imgDescricao = $imgDescricao;

        return $this;
    }

    /**
     * Get imgDescricao
     *
     * @return string
     */
    public function getImgDescricao()
    {
        return $this->imgDescricao;
    }

    /**
     * Set descontoPerc
     *
     * @param float $descontoPerc
     *
     * @return B2cPromo
     */
    public function setDescontoPerc($descontoPerc)
    {
        $this->descontoPerc = $descontoPerc;

        return $this;
    }

    /**
     * Get descontoPerc
     *
     * @return float
     */
    public function getDescontoPerc()
    {
        return $this->descontoPerc;
    }

    /**
     * Set descontoValor
     *
     * @param float $descontoValor
     *
     * @return B2cPromo
     */
    public function setDescontoValor($descontoValor)
    {
        $this->descontoValor = $descontoValor;

        return $this;
    }

    /**
     * Get descontoValor
     *
     * @return float
     */
    public function getDescontoValor()
    {
        return $this->descontoValor;
    }

    /**
     * Set freteGratis
     *
     * @param boolean $freteGratis
     *
     * @return B2cPromo
     */
    public function setFreteGratis($freteGratis)
    {
        $this->freteGratis = $freteGratis;

        return $this;
    }

    /**
     * Get freteGratis
     *
     * @return boolean
     */
    public function getFreteGratis()
    {
        return $this->freteGratis;
    }

    /**
     * Set freteFixo
     *
     * @param boolean $freteFixo
     *
     * @return B2cPromo
     */
    public function setFreteFixo($freteFixo)
    {
        $this->freteFixo = $freteFixo;

        return $this;
    }

    /**
     * Get freteFixo
     *
     * @return boolean
     */
    public function getFreteFixo()
    {
        return $this->freteFixo;
    }

    /**
     * Set freteFixoValor
     *
     * @param float $freteFixoValor
     *
     * @return B2cPromo
     */
    public function setFreteFixoValor($freteFixoValor)
    {
        $this->freteFixoValor = $freteFixoValor;

        return $this;
    }

    /**
     * Get freteFixoValor
     *
     * @return float
     */
    public function getFreteFixoValor()
    {
        return $this->freteFixoValor;
    }

    /**
     * Set valorMinimo
     *
     * @param float $valorMinimo
     *
     * @return B2cPromo
     */
    public function setValorMinimo($valorMinimo)
    {
        $this->valorMinimo = $valorMinimo;

        return $this;
    }

    /**
     * Get valorMinimo
     *
     * @return float
     */
    public function getValorMinimo()
    {
        return $this->valorMinimo;
    }

    /**
     * Set usarTodaloja
     *
     * @param boolean $usarTodaloja
     *
     * @return B2cPromo
     */
    public function setUsarTodaloja($usarTodaloja)
    {
        $this->usarTodaloja = $usarTodaloja;

        return $this;
    }

    /**
     * Get usarTodaloja
     *
     * @return boolean
     */
    public function getUsarTodaloja()
    {
        return $this->usarTodaloja;
    }

    /**
     * Set usarProdutos
     *
     * @param boolean $usarProdutos
     *
     * @return B2cPromo
     */
    public function setUsarProdutos($usarProdutos)
    {
        $this->usarProdutos = $usarProdutos;

        return $this;
    }

    /**
     * Get usarProdutos
     *
     * @return boolean
     */
    public function getUsarProdutos()
    {
        return $this->usarProdutos;
    }

    /**
     * Set usarSituacao
     *
     * @param boolean $usarSituacao
     *
     * @return B2cPromo
     */
    public function setUsarSituacao($usarSituacao)
    {
        $this->usarSituacao = $usarSituacao;

        return $this;
    }

    /**
     * Get usarSituacao
     *
     * @return boolean
     */
    public function getUsarSituacao()
    {
        return $this->usarSituacao;
    }

    /**
     * Set usarCategoria
     *
     * @param boolean $usarCategoria
     *
     * @return B2cPromo
     */
    public function setUsarCategoria($usarCategoria)
    {
        $this->usarCategoria = $usarCategoria;

        return $this;
    }

    /**
     * Get usarCategoria
     *
     * @return boolean
     */
    public function getUsarCategoria()
    {
        return $this->usarCategoria;
    }

    /**
     * Set usarFornecedor
     *
     * @param boolean $usarFornecedor
     *
     * @return B2cPromo
     */
    public function setUsarFornecedor($usarFornecedor)
    {
        $this->usarFornecedor = $usarFornecedor;

        return $this;
    }

    /**
     * Get usarFornecedor
     *
     * @return boolean
     */
    public function getUsarFornecedor()
    {
        return $this->usarFornecedor;
    }

    /**
     * Set aplicarCliente
     *
     * @param boolean $aplicarCliente
     *
     * @return B2cPromo
     */
    public function setAplicarCliente($aplicarCliente)
    {
        $this->aplicarCliente = $aplicarCliente;

        return $this;
    }

    /**
     * Get aplicarCliente
     *
     * @return boolean
     */
    public function getAplicarCliente()
    {
        return $this->aplicarCliente;
    }

    /**
     * Set aplicarProdutos
     *
     * @param boolean $aplicarProdutos
     *
     * @return B2cPromo
     */
    public function setAplicarProdutos($aplicarProdutos)
    {
        $this->aplicarProdutos = $aplicarProdutos;

        return $this;
    }

    /**
     * Get aplicarProdutos
     *
     * @return boolean
     */
    public function getAplicarProdutos()
    {
        return $this->aplicarProdutos;
    }

    /**
     * Set aplicarPagamento
     *
     * @param boolean $aplicarPagamento
     *
     * @return B2cPromo
     */
    public function setAplicarPagamento($aplicarPagamento)
    {
        $this->aplicarPagamento = $aplicarPagamento;

        return $this;
    }

    /**
     * Get aplicarPagamento
     *
     * @return boolean
     */
    public function getAplicarPagamento()
    {
        return $this->aplicarPagamento;
    }

    /**
     * Set aplicarEntrega
     *
     * @param boolean $aplicarEntrega
     *
     * @return B2cPromo
     */
    public function setAplicarEntrega($aplicarEntrega)
    {
        $this->aplicarEntrega = $aplicarEntrega;

        return $this;
    }

    /**
     * Get aplicarEntrega
     *
     * @return boolean
     */
    public function getAplicarEntrega()
    {
        return $this->aplicarEntrega;
    }

    /**
     * Set promocaoBrinde
     *
     * @param boolean $promocaoBrinde
     *
     * @return B2cPromo
     */
    public function setPromocaoBrinde($promocaoBrinde)
    {
        $this->promocaoBrinde = $promocaoBrinde;

        return $this;
    }

    /**
     * Get promocaoBrinde
     *
     * @return boolean
     */
    public function getPromocaoBrinde()
    {
        return $this->promocaoBrinde;
    }

    /**
     * Set aplicarCarrinho
     *
     * @param boolean $aplicarCarrinho
     *
     * @return B2cPromo
     */
    public function setAplicarCarrinho($aplicarCarrinho)
    {
        $this->aplicarCarrinho = $aplicarCarrinho;

        return $this;
    }

    /**
     * Get aplicarCarrinho
     *
     * @return boolean
     */
    public function getAplicarCarrinho()
    {
        return $this->aplicarCarrinho;
    }

    /**
     * Set valorExato
     *
     * @param float $valorExato
     *
     * @return B2cPromo
     */
    public function setValorExato($valorExato)
    {
        $this->valorExato = $valorExato;

        return $this;
    }

    /**
     * Get valorExato
     *
     * @return float
     */
    public function getValorExato()
    {
        return $this->valorExato;
    }

    /**
     * Set qtdMinima
     *
     * @param string $qtdMinima
     *
     * @return B2cPromo
     */
    public function setQtdMinima($qtdMinima)
    {
        $this->qtdMinima = $qtdMinima;

        return $this;
    }

    /**
     * Get qtdMinima
     *
     * @return string
     */
    public function getQtdMinima()
    {
        return $this->qtdMinima;
    }

    /**
     * Set qtdExata
     *
     * @param string $qtdExata
     *
     * @return B2cPromo
     */
    public function setQtdExata($qtdExata)
    {
        $this->qtdExata = $qtdExata;

        return $this;
    }

    /**
     * Get qtdExata
     *
     * @return string
     */
    public function getQtdExata()
    {
        return $this->qtdExata;
    }

    /**
     * Set qtdBrindes
     *
     * @param integer $qtdBrindes
     *
     * @return B2cPromo
     */
    public function setQtdBrindes($qtdBrindes)
    {
        $this->qtdBrindes = $qtdBrindes;

        return $this;
    }

    /**
     * Get qtdBrindes
     *
     * @return integer
     */
    public function getQtdBrindes()
    {
        return $this->qtdBrindes;
    }

    /**
     * Set sorteio
     *
     * @param boolean $sorteio
     *
     * @return B2cPromo
     */
    public function setSorteio($sorteio)
    {
        $this->sorteio = $sorteio;

        return $this;
    }

    /**
     * Get sorteio
     *
     * @return boolean
     */
    public function getSorteio()
    {
        return $this->sorteio;
    }

    /**
     * Set promoIncluiFrete
     *
     * @param boolean $promoIncluiFrete
     *
     * @return B2cPromo
     */
    public function setPromoIncluiFrete($promoIncluiFrete)
    {
        $this->promoIncluiFrete = $promoIncluiFrete;

        return $this;
    }

    /**
     * Get promoIncluiFrete
     *
     * @return boolean
     */
    public function getPromoIncluiFrete()
    {
        return $this->promoIncluiFrete;
    }

    /**
     * Set tipoVale
     *
     * @param integer $tipoVale
     *
     * @return B2cPromo
     */
    public function setTipoVale($tipoVale)
    {
        $this->tipoVale = $tipoVale;

        return $this;
    }

    /**
     * Get tipoVale
     *
     * @return integer
     */
    public function getTipoVale()
    {
        return $this->tipoVale;
    }
}
