<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdfrete
 *
 * @ORM\Table(name="b2c_prdfrete")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPrdfreteRepository")
 */
class B2cPrdfrete
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPrdprd;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_regreg", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idRegreg;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $valor;


    /**
     * Set idPrdprd
     *
     * @param integer $idPrdprd
     *
     * @return B2cPrdfrete
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;

        return $this;
    }

    /**
     * Get idPrdprd
     *
     * @return integer
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * Set idRegreg
     *
     * @param integer $idRegreg
     *
     * @return B2cPrdfrete
     */
    public function setIdRegreg($idRegreg)
    {
        $this->idRegreg = $idRegreg;

        return $this;
    }

    /**
     * Get idRegreg
     *
     * @return integer
     */
    public function getIdRegreg()
    {
        return $this->idRegreg;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return B2cPrdfrete
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }
}
