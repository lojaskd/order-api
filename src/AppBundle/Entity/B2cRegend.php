<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cRegend
 *
 * @ORM\Table(name="b2c_regend", indexes={@ORM\Index(name="idx_regiao", columns={"id_regiao"}), @ORM\Index(name="idx_estado", columns={"id_regest"}), @ORM\Index(name="idx_cidade", columns={"cidade"}), @ORM\Index(name="idx_cep", columns={"cep"}), @ORM\Index(name="idx_cidade_estado", columns={"id_regest", "cidade"}), @ORM\Index(name="idx_faixa_cep", columns={"ini_cep", "fim_cep"}), @ORM\Index(name="idx_municipio", columns={"cidade_municipio", "uf"})})
 * @ORM\Entity
 */
class B2cRegend
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_regiao", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idRegiao;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_regest", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idRegest;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="uf", type="string", length=2, precision=0, scale=0, nullable=false, unique=false)
     */
    private $uf;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro_inicio", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $bairroInicio;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro_fim", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $bairroFim;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco", type="string", length=500, precision=0, scale=0, nullable=true, unique=false)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="complemento", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $complemento;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_ibge", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $codIbge;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_ibge_municipio", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $codIbgeMunicipio;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade_municipio", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cidadeMunicipio;

    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=20, precision=0, scale=0, nullable=true, unique=false)
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="ini_cep", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $iniCep;

    /**
     * @var string
     *
     * @ORM\Column(name="fim_cep", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $fimCep;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idRegiao
     *
     * @param integer $idRegiao
     *
     * @return B2cRegend
     */
    public function setIdRegiao($idRegiao)
    {
        $this->idRegiao = $idRegiao;

        return $this;
    }

    /**
     * Get idRegiao
     *
     * @return integer
     */
    public function getIdRegiao()
    {
        return $this->idRegiao;
    }

    /**
     * Set idRegest
     *
     * @param integer $idRegest
     *
     * @return B2cRegend
     */
    public function setIdRegest($idRegest)
    {
        $this->idRegest = $idRegest;

        return $this;
    }

    /**
     * Get idRegest
     *
     * @return integer
     */
    public function getIdRegest()
    {
        return $this->idRegest;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return B2cRegend
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set uf
     *
     * @param string $uf
     *
     * @return B2cRegend
     */
    public function setUf($uf)
    {
        $this->uf = $uf;

        return $this;
    }

    /**
     * Get uf
     *
     * @return string
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     *
     * @return B2cRegend
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set bairroInicio
     *
     * @param string $bairroInicio
     *
     * @return B2cRegend
     */
    public function setBairroInicio($bairroInicio)
    {
        $this->bairroInicio = $bairroInicio;

        return $this;
    }

    /**
     * Get bairroInicio
     *
     * @return string
     */
    public function getBairroInicio()
    {
        return $this->bairroInicio;
    }

    /**
     * Set bairroFim
     *
     * @param string $bairroFim
     *
     * @return B2cRegend
     */
    public function setBairroFim($bairroFim)
    {
        $this->bairroFim = $bairroFim;

        return $this;
    }

    /**
     * Get bairroFim
     *
     * @return string
     */
    public function getBairroFim()
    {
        return $this->bairroFim;
    }

    /**
     * Set endereco
     *
     * @param string $endereco
     *
     * @return B2cRegend
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;

        return $this;
    }

    /**
     * Get endereco
     *
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * Set complemento
     *
     * @param string $complemento
     *
     * @return B2cRegend
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;

        return $this;
    }

    /**
     * Get complemento
     *
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * Set codIbge
     *
     * @param integer $codIbge
     *
     * @return B2cRegend
     */
    public function setCodIbge($codIbge)
    {
        $this->codIbge = $codIbge;

        return $this;
    }

    /**
     * Get codIbge
     *
     * @return integer
     */
    public function getCodIbge()
    {
        return $this->codIbge;
    }

    /**
     * Set codIbgeMunicipio
     *
     * @param integer $codIbgeMunicipio
     *
     * @return B2cRegend
     */
    public function setCodIbgeMunicipio($codIbgeMunicipio)
    {
        $this->codIbgeMunicipio = $codIbgeMunicipio;

        return $this;
    }

    /**
     * Get codIbgeMunicipio
     *
     * @return integer
     */
    public function getCodIbgeMunicipio()
    {
        return $this->codIbgeMunicipio;
    }

    /**
     * Set cidadeMunicipio
     *
     * @param string $cidadeMunicipio
     *
     * @return B2cRegend
     */
    public function setCidadeMunicipio($cidadeMunicipio)
    {
        $this->cidadeMunicipio = $cidadeMunicipio;

        return $this;
    }

    /**
     * Get cidadeMunicipio
     *
     * @return string
     */
    public function getCidadeMunicipio()
    {
        return $this->cidadeMunicipio;
    }

    /**
     * Set cep
     *
     * @param string $cep
     *
     * @return B2cRegend
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set iniCep
     *
     * @param string $iniCep
     *
     * @return B2cRegend
     */
    public function setIniCep($iniCep)
    {
        $this->iniCep = $iniCep;

        return $this;
    }

    /**
     * Get iniCep
     *
     * @return string
     */
    public function getIniCep()
    {
        return $this->iniCep;
    }

    /**
     * Set fimCep
     *
     * @param string $fimCep
     *
     * @return B2cRegend
     */
    public function setFimCep($fimCep)
    {
        $this->fimCep = $fimCep;

        return $this;
    }

    /**
     * Get fimCep
     *
     * @return string
     */
    public function getFimCep()
    {
        return $this->fimCep;
    }
}
