<?php

namespace AppBundle\Entity\VO\Update;

use JMS\Serializer\Annotation\Type;

/**
 * UpdatePurchaseOrderVO
 */
class UpdatePurchaseOrderVO
{
    /**
     * @var integer
     *
     * @Type("integer")
     */
    private $purchaseOrder;

    /**
     * @var string
     *
     * @Type("string")
     */
    private $pathXml;

    /**
     * @var array
     *
     * @Type("array")
     */
    private $orders;

    /**
     * @var \Datetime
     *
     * @Type("DateTime<'Y-m-d'>")
     */
    private $date;

    /**
     * @return int
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * @param int $purchaseOrder
     */
    public function setPurchaseOrder($purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    /**
     * @return int
     */
    public function getPathXml()
    {
        return $this->pathXml;
    }

    /**
     * @param int $pathXml
     */
    public function setPathXml($pathXml)
    {
        $this->pathXml = $pathXml;
    }

    /**
     * @return array
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param array $orders
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
    }

    /**
     * @return \Datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \Datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
}
