<?php

namespace AppBundle\Entity\VO\Update;

use JMS\Serializer\Annotation\Type;
use Doctrine\ORM\Mapping as ORM;

/**
 * UpdateProductsDateTypeVO
 *
 * @ORM\Entity()
 */
class UpdateProductsDateTypeVO
{

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @ORM\Id
     * @Type("string")
     */
    private $type;

    /**
     * @var \Datetime
     * 
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $date;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return \Datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \Datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

}
