<?php

namespace AppBundle\Entity\VO\Update;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * UpdateProductsStatusVO
 *
 * @ORM\Entity()
 */
class UpdateProductsStatusVO
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $status;

    /**
     * @var boolean
     *
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $cancelled;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ticket;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\VO\Update\UpdateProductsStatusProductVO>")
     */
    private $products;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ocrCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $cancellationIndicator;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ticketType;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $hub;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $shipper;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $invoice;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $productCauser;

    /**
     * @var integer
     *
     * @Type("integer")
     */
    private $refundForm;


    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return boolean
     */
    public function isCancelled()
    {
        return $this->cancelled;
    }

    /**
     * @param boolean $cancelled
     */
    public function setCancelled($cancelled)
    {
        $this->cancelled = $cancelled;
    }

    /**
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param string $ticket
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * @param UpdateProductsStatusProductVO $product
     */
    public function addProduct(UpdateProductsStatusProductVO $product)
    {
        $this->products[] = $product;
    }

    /**
     * @return array $products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return string
     */
    public function getOcrCode()
    {
        return $this->ocrCode;
    }

    /**
     * @param string $ocrCode
     */
    public function setOcrCode($ocrCode)
    {
        $this->ocrCode = $ocrCode;
    }

    /**
     * @return string
     */
    public function getCancellationIndicator()
    {
        return $this->cancellationIndicator;
    }

    /**
     * @param string $cancellationIndicator
     */
    public function setCancellationIndicator($cancellationIndicator)
    {
        $this->cancellationIndicator = $cancellationIndicator;
    }

    /**
     * @return string
     */
    public function getTicketType()
    {
        return $this->ticketType;
    }

    /**
     * @param string $ticketType
     */
    public function setTicketType($ticketType)
    {
        $this->ticketType = $ticketType;
    }

    /**
     * @return string
     */
    public function getHub()
    {
        return $this->hub;
    }

    /**
     * @param string $hub
     */
    public function setHub($hub)
    {
        $this->hub = $hub;
    }

    /**
     * @return string
     */
    public function getShipper()
    {
        return $this->shipper;
    }

    /**
     * @param string $shipper
     */
    public function setShipper($shipper)
    {
        $this->shipper = $shipper;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return string
     */
    public function getProductCauser()
    {
        return $this->productCauser;
    }

    /**
     * @param string $productCauser
     */
    public function setProductCauser($productCauser)
    {
        $this->productCauser = $productCauser;
    }

    /**
     * Get cancelled
     *
     * @return boolean
     */
    public function getCancelled()
    {
        return $this->cancelled;
    }

    /**
     * @return int
     */
    public function getRefundForm()
    {
        return $this->refundForm;
    }

    /**
     * @param int $refundForm
     */
    public function setRefundForm($refundForm)
    {
        $this->refundForm = $refundForm;
    }
}
