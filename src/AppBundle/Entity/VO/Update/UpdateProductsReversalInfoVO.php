<?php

namespace AppBundle\Entity\VO\Update;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * UpdateProductsStatusVO
 *
 * @ORM\Entity()
 */
class UpdateProductsReversalInfoVO
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $typeReversal;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $operator;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $transaction;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\VO\Update\UpdateProductsStatusProductVO>")
     */
    private $products;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return int
     */
    public function getTypeReversal()
    {
        return $this->typeReversal;
    }

    /**
     * @param int $typeReversal
     */
    public function setTypeReversal($typeReversal)
    {
        $this->typeReversal = $typeReversal;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return string
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param string $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @param UpdateProductsStatusProductVO $product
     */
    public function addProduct(UpdateProductsStatusProductVO $product)
    {
        $this->products[] = $product;
    }

    /**
     * @return array $products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }
}
