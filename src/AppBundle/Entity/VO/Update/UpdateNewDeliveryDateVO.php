<?php

namespace AppBundle\Entity\VO\Update;

use JMS\Serializer\Annotation\Type;

/**
 * UpdateNewDeliveryDateVO
 */
class UpdateNewDeliveryDateVO
{

    /**
     * @var integer
     *
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var \Datetime
     *
     * @Type("datetime")
     */
    private $newDeliveryDate;

    /**
     * @var boolean
     *
     * @Type("boolean")
     */
    private $accept;

    /**
     * @var string
     *
     * @Type("string")
     */
    private $user;


    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return \Datetime
     */
    public function getNewDeliveryDate()
    {
        return $this->newDeliveryDate;
    }

    /**
     * @param \Datetime $newDeliveryDate
     */
    public function setNewDeliveryDate($newDeliveryDate)
    {
        $this->newDeliveryDate = $newDeliveryDate;
    }

    /**
     * @return boolean
     */
    public function isAccept()
    {
        return $this->accept;
    }

    /**
     * @param boolean $accept
     */
    public function setAccept($accept)
    {
        $this->accept = $accept;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}
