<?php

namespace AppBundle\Entity\VO\Update;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * UpdateProviderBillingDateVO
 */
class UpdateProviderBillingDateVO
{

    /**
     * @var integer
     *
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @Type("integer")
     */
    private $purchaseOrder;

    /**
     * @var string
     *
     * @Type("string")
     */
    private $type;

    /**
     * @var \Datetime
     *
     * @Type("DateTime<'Y-m-d'>")
     */
    private $date;

    /**
     * @var string
     *
     * @Type("string")
     */
    private $clerk;


    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * @param int $purchaseOrder
     */
    public function setPurchaseOrder($purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return \Datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \Datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }
}
