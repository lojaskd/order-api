<?php

namespace AppBundle\Entity\VO\Update;

use JMS\Serializer\Annotation\Type;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UpdateProductsDateVO
 *
 * @ORM\Entity()
 */
class UpdateProductsDateVO
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var array
     *
     * @Assert\NotBlank(message="The products field is required.")
     * @Assert\Valid
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\VO\Update\UpdateProductsDateProductVO>")
     */
    private $products;

    /**
     * @var array
     *
     * @Assert\NotBlank(message="The dateType field is required.")
     * @Assert\Valid
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\VO\Update\UpdateProductsDateTypeVO>")
     */
    private $dateTypes;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $predict;

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @param UpdateProductsDateProductVO $product
     */
    public function addProduct($product)
    {
        $this->products[] = $product;
    }

    /**
     * @return array
     */
    public function getDateTypes()
    {
        return $this->dateTypes;
    }

    /**
     * @param array $dateType
     */
    public function setDateType($dateType)
    {
        $this->dateTypes = $dateType;
    }

    /**
     * @param UpdateProductsDateTypeVO $dateType
     */
    public function addDateType($dateType)
    {
        $this->dateTypes[] = $dateType;
    }

    /**
     * @return boolean
     */
    public function isPredict()
    {
        return $this->predict;
    }

    /**
     * @param boolean $predict
     */
    public function setPredict($predict)
    {
        $this->predict = $predict;
    }


    /**
     * Set dateTypes
     *
     * @param array $dateTypes
     *
     * @return UpdateProductsDateVO
     */
    public function setDateTypes($dateTypes)
    {
        $this->dateTypes = $dateTypes;

        return $this;
    }

    /**
     * Get predict
     *
     * @return boolean
     */
    public function getPredict()
    {
        return $this->predict;
    }
}
