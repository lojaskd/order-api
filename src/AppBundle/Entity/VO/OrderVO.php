<?php

namespace AppBundle\Entity\VO;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * OrderVO
 *
 * @ORM\Entity()
 */
class OrderVO
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var \AppBundle\Entity\VO\OrderCustomerVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\OrderCustomerVO")
     * @Type("\AppBundle\Entity\VO\OrderCustomerVO")
     */
    private $customer;

    /**
     * @var \AppBundle\Entity\VO\OrderAddressVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\OrderAddressVO")
     * @Type("\AppBundle\Entity\VO\OrderAddressVO")
     */
    private $deliveryAddress;

    /**
     * @var \AppBundle\Entity\VO\OrderAddressVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\OrderAddressVO")
     * @Type("\AppBundle\Entity\VO\OrderAddressVO")
     */
    private $billingAddress;

    /**
     * @var \AppBundle\Entity\VO\OrderStatusVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\OrderStatusVO")
     * @Type("\AppBundle\Entity\VO\OrderStatusVO")
     */
    private $status;

    /**
     * @var \AppBundle\Entity\VO\OrderStatusVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\OrderStatusVO")
     * @Type("\AppBundle\Entity\VO\OrderStatusVO")
     */
    private $internalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $marketplaceId;

    /**
     * @var \AppBundle\Entity\VO\OrderProductVO
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\VO\OrderProductVO>")
     */
    private $products;

    /**
     * @var \AppBundle\Entity\VO\OrderPaymentVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\OrderPaymentVO")
     * @Type("\AppBundle\Entity\VO\OrderPaymentVO")
     */
    private $firstPayment;

    /**
     * @var \AppBundle\Entity\VO\OrderPaymentVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\OrderPaymentVO")
     * @Type("\AppBundle\Entity\VO\OrderPaymentVO")
     */
    private $secondPayment;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("datetime")
     */
    private $predictDeliveryDate;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("datetime")
     */
    private $subtotal;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $total;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $discount;

    /**
     * @var String
     *
     * @ORM\Column(type="String")
     * @Type("String")
     */
    private $coupon;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $couponAmount;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $shippingAmount;

    /**
     * @var string
     */
    private $regionName;

    /**
     * @var \AppBundle\Entity\VO\Create\CreateShippingVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreateShippingVO")
     * @Type("AppBundle\Entity\VO\Create\CreateShippingVO")
     */
    private $shipping;

    /**
     * @var \AppBundle\Entity\VO\Create\CreateRegionVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreateRegionVO")
     * @Type("AppBundle\Entity\VO\Create\CreateRegionVO")
     */
    private $region;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return OrderCustomerVO
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param OrderCustomerVO $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @param \AppBundle\Entity\VO\OrderProductVO $product
     * @param integer $key
     */
    public function addProduct($product, $key = -1)
    {
        if ($key >= 0) {
            $this->products[$key] = $product;
        } else {
            $this->products[] = $product;
        }
    }

    /**
     * @return array
     */
    public function getProductsId()
    {
        $productsId = array();

        /* @var $product OrderProductVO */
        foreach ($this->products as $product) {
            $productsId[] = $product->getId();
        }

        return $productsId;
    }

    /**
     * @return array
     */
    public function getProductsQuantity()
    {
        $productsQtd = array();

        /* @var $product OrderProductVO */
        foreach ($this->products as $product) {
            $productsQtd[] = $product->getQuantity();
        }

        return $productsQtd;
    }

    /**
     * @return float
     */
    public function getTotalCubage()
    {
        $cubage = 0;

        /* @var $product OrderProductVO */
        foreach ($this->products as $product) {
            $cubage += $product->getCubage();
        }

        return $cubage;
    }

    /**
     * @return float
     */
    public function getTotalWeight()
    {
        $weight = 0;

        /* @var $product OrderProductVO */
        foreach ($this->products as $product) {
            $weight += $product->getWeight();
        }

        return $weight;
    }

    /**
     * @return integer
     */
    public function getTotalQuantity()
    {
        $quantity = 0;

        /* @var $product OrderProductVO */
        foreach ($this->products as $product) {
            $quantity += $product->getQuantity();
        }

        return $quantity;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        $categories = array();

        /* @var $product OrderProductVO */
        foreach ($this->products as $product) {
            $categories[] = $product->getCategory();
        }

        return $categories;
    }

    /**
     * @return array
     */
    public function getProviders()
    {
        $providers = array();

        /* @var $product OrderProductVO */
        foreach ($this->products as $product) {
            $providers[] = $product->getProvider();
        }

        return $providers;
    }

    /**
     * @return array
     */
    public function getPaymentsId()
    {
        $paymentsId = array();

        $paymentsId[] = $this->getFirstPayment()->getId();

        if ($this->getSecondPayment()->getId()) {
            $paymentsId[] = $this->getSecondPayment()->getId();
        }

        return $paymentsId;
    }

    /**
     * @return OrderAddressVO
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @param OrderAddressVO $deliveryAddress
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * @return OrderAddressVO
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param OrderAddressVO $billingAddress
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return OrderStatusVO
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param OrderStatusVO $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return OrderStatusVO
     */
    public function getInternalStatus()
    {
        return $this->internalStatus;
    }

    /**
     * @param OrderStatusVO $internalStatus
     */
    public function setInternalStatus($internalStatus)
    {
        $this->internalStatus = $internalStatus;
    }

    /**
     * @return OrderStatusVO
     */
    public function getMarketplaceId()
    {
        return $this->marketplaceId;
    }

    /**
     * @param int $marketplaceId
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;
    }

    /**
     * @return OrderPaymentVO
     */
    public function getFirstPayment()
    {
        return $this->firstPayment;
    }

    /**
     * @param OrderPaymentVO $firstPayment
     */
    public function setFirstPayment($firstPayment)
    {
        $this->firstPayment = $firstPayment;
    }

    /**
     * @return OrderPaymentVO
     */
    public function getSecondPayment()
    {
        return $this->secondPayment;
    }

    /**
     * @param OrderPaymentVO $secondPayment
     */
    public function setSecondPayment($secondPayment)
    {
        $this->secondPayment = $secondPayment;
    }

    /**
     * @return \Datetime
     */
    public function getPredictDeliveryDate()
    {
        return $this->predictDeliveryDate;
    }

    /**
     * @param \Datetime $predictDeliveryDate
     */
    public function setPredictDeliveryDate($predictDeliveryDate)
    {
        $this->predictDeliveryDate = $predictDeliveryDate;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param float $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return String
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param String $coupon
     */
    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @return float
     */
    public function getCouponAmount()
    {
        return $this->couponAmount;
    }

    /**
     * @param float $couponAmount
     */
    public function setCouponAmount($couponAmount)
    {
        $this->couponAmount = $couponAmount;
    }

    /**
     * @return float
     */
    public function getShippingAmount()
    {
        return $this->shippingAmount;
    }

    /**
     * @param float $shippingAmount
     */
    public function setShippingAmount($shippingAmount)
    {
        $this->shippingAmount = $shippingAmount;
    }

    /**
     * @return string
     */
    public function getRegionName()
    {
        return $this->regionName;
    }

    /**
     * @param $regionName
     * @return $this
     */
    public function setRegionName($regionName)
    {
        $this->regionName = $regionName;
        return $this;
    }

    /**
     * @return Create\CreateShippingVO
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param Create\CreateShippingVO $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return Create\CreateRegionVO
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }
}
