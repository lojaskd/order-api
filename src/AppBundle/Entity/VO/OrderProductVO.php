<?php

namespace AppBundle\Entity\VO;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * OrderVO
 *
 * @ORM\Entity()
 */
class OrderProductVO extends ProductVO
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $productId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @ORM\Id
     * @Type("string")
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $supplierStock;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $supplierReservedStock;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $productStock;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $cancelled;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $branchStockId;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $immediateDelivery;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $reservedStockDelivery;

    /**
     * @var string
     */
    private $destination;

    /**
     * @var string
     */
    private $supplierId;

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getSupplierStock()
    {
        return $this->supplierStock;
    }

    /**
     * @param int $supplierStock
     */
    public function setSupplierStock($supplierStock)
    {
        $this->supplierStock = $supplierStock;
        return $this;
    }

    /**
     * @return int
     */
    public function getSupplierReservedStock()
    {
        return $this->supplierReservedStock;
    }

    /**
     * @param int $supplierReservedStock
     */
    public function setSupplierReservedStock($supplierReservedStock)
    {
        $this->supplierReservedStock = $supplierReservedStock;
        return $this;
    }

    /**
     * @return int
     */
    public function getProductStock()
    {
        return $this->productStock;
    }

    /**
     * @param int $productStock
     */
    public function setProductStock($productStock)
    {
        $this->productStock = $productStock;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCancelled()
    {
        return $this->cancelled;
    }

    /**
     * @param bool $cancelled
     */
    public function setCancelled($cancelled)
    {
        $this->cancelled = $cancelled;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return int
     */
    public function getBranchStockId()
    {
        return $this->branchStockId;
    }

    /**
     * @param int $branchStockId
     */
    public function setBranchStockId($branchStockId)
    {
        $this->branchStockId = $branchStockId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isImmediateDelivery()
    {
        return $this->immediateDelivery;
    }

    /**
     * @param bool $immediateDelivery
     */
    public function setImmediateDelivery($immediateDelivery)
    {
        $this->immediateDelivery = $immediateDelivery;
        return $this;
    }

    /**
     * @return bool
     */
    public function isReservedStockDelivery()
    {
        return $this->reservedStockDelivery;
    }

    /**
     * @param bool $reservedStockDelivery
     */
    public function setReservedStockDelivery($reservedStockDelivery)
    {
        $this->reservedStockDelivery = $reservedStockDelivery;
        return $this;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param string $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     * @return string
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param string $supplierId
     */
    public function setSupplierId($supplierId)
    {
        $this->supplierId = $supplierId;
        return $this;
    }

    /**
     * Get cancelled
     *
     * @return boolean
     */
    public function getCancelled()
    {
        return $this->cancelled;
    }

    /**
     * Get immediateDelivery
     *
     * @return boolean
     */
    public function getImmediateDelivery()
    {
        return $this->immediateDelivery;
    }

    /**
     * Get reservedStockDelivery
     *
     * @return boolean
     */
    public function getReservedStockDelivery()
    {
        return $this->reservedStockDelivery;
    }
}
