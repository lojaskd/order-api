<?php
namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;


/**
 * CreatePromotionPaymentVO
 *
 * @ORM\Entity()
 */
class CreatePromotionPaymentVO
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $idPromo;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $discountValue;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $discountPerc;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $discountProdPrice;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $discountProduct;

    /**
     * @return int
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * @param int $idPromo
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;
    }

    /**
     * @return float
     */
    public function getDiscountValue()
    {
        return $this->discountValue;
    }

    /**
     * @param float $discountValue
     */
    public function setDiscountValue($discountValue)
    {
        $this->discountValue = $discountValue;
    }

    /**
     * @return float
     */
    public function getDiscountPerc()
    {
        return $this->discountPerc;
    }

    /**
     * @param float $discountPerc
     */
    public function setDiscountPerc($discountPerc)
    {
        $this->discountPerc = $discountPerc;
    }

    /**
     * @return float
     */
    public function getDiscountProdPrice()
    {
        return $this->discountProdPrice;
    }

    /**
     * @param float $discountProdPrice
     */
    public function setDiscountProdPrice($discountProdPrice)
    {
        $this->discountProdPrice = $discountProdPrice;
    }

    /**
     * @return float
     */
    public function getDiscountProduct()
    {
        return $this->discountProduct;
    }

    /**
     * @param float $discountProduct
     */
    public function setDiscountProduct($discountProduct)
    {
        $this->discountProduct = $discountProduct;
    }
}
