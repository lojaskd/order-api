<?php
namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use AppBundle\Entity\VO\ProductVO;

/**
 * CreateProductVO
 *
 * @ORM\Entity()
 */
class CreateProductVO extends ProductVO
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $kitId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $reference;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $featured;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $style;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $colorName;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $genre;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $providerDeadline;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $providerStock;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $providerStockReserved;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $productProviderDeadline;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $minStock;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $minStockAction;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $action;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $show;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $productStock;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $productDeadline;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $out;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $available;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $availability;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $availabilityDays;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $providerDeadlineDate;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $providerDeliveryDate;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $deliveryDays;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $deliveryDate;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $availabilityDeadline;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $promotion;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $initialPromotion;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $finalPromotion;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $discountValue;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $discountPercentage;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $realPrice;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $promotionPrice;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $productPrice;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $totalprice;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $costPrice;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $branchOfficeId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $collectionConveyorCode;

    /**
     * @var \AppBundle\Entity\VO\Create\CreatePromotionPaymentVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreatePromotionPaymentVO")
     * @Type("AppBundle\Entity\VO\Create\CreatePromotionPaymentVO")
     */
    private $promotionPayment;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $pathXmlOc;

    /**
     * @return int
     */
    public function getKitId()
    {
        return $this->kitId;
    }

    /**
     * @param int $kitId
     */
    public function setKitId($kitId)
    {
        $this->kitId = $kitId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return boolean
     */
    public function isFeatured()
    {
        return $this->featured;
    }

    /**
     * @param boolean $featured
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;
    }

    /**
     * @return int
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @param int $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @return string
     */
    public function getColorName()
    {
        return $this->colorName;
    }

    /**
     * @param string $colorName
     */
    public function setColorName($colorName)
    {
        $this->colorName = $colorName;
    }

    /**
     * @return int
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param int $genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
    }

    /**
     * @return int
     */
    public function getProviderDeadline()
    {
        return $this->providerDeadline;
    }

    /**
     * @param int $providerDeadline
     */
    public function setProviderDeadline($providerDeadline)
    {
        $this->providerDeadline = $providerDeadline;
    }

    /**
     * @return int
     */
    public function getProviderStock()
    {
        return $this->providerStock;
    }

    /**
     * @param int $providerStock
     */
    public function setProviderStock($providerStock)
    {
        $this->providerStock = $providerStock;
    }

    /**
     * @return int
     */
    public function getProviderStockReserved()
    {
        return $this->providerStockReserved;
    }

    /**
     * @param int $providerStockReserved
     */
    public function setProviderStockReserved($providerStockReserved)
    {
        $this->providerStockReserved = $providerStockReserved;
    }

    /**
     * @return int
     */
    public function getProductProviderDeadline()
    {
        return $this->productProviderDeadline;
    }

    /**
     * @param int $productProviderDeadline
     */
    public function setProductProviderDeadline($productProviderDeadline)
    {
        $this->productProviderDeadline = $productProviderDeadline;
    }

    /**
     * @return int
     */
    public function getMinStock()
    {
        return $this->minStock;
    }

    /**
     * @param int $minStock
     */
    public function setMinStock($minStock)
    {
        $this->minStock = $minStock;
    }

    /**
     * @return int
     */
    public function getMinStockAction()
    {
        return $this->minStockAction;
    }

    /**
     * @param int $minStockAction
     */
    public function setMinStockAction($minStockAction)
    {
        $this->minStockAction = $minStockAction;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return boolean
     */
    public function isShow()
    {
        return $this->show;
    }

    /**
     * @param boolean $show
     */
    public function setShow($show)
    {
        $this->show = $show;
    }

    /**
     * @return int
     */
    public function getProductStock()
    {
        return $this->productStock;
    }

    /**
     * @param int $productStock
     */
    public function setProductStock($productStock)
    {
        $this->productStock = $productStock;
    }

    /**
     * @return int
     */
    public function getProductDeadline()
    {
        return $this->productDeadline;
    }

    /**
     * @param int $productDeadline
     */
    public function setProductDeadline($productDeadline)
    {
        $this->productDeadline = $productDeadline;
    }

    /**
     * @return boolean
     */
    public function isOut()
    {
        return $this->out;
    }

    /**
     * @param boolean $out
     */
    public function setOut($out)
    {
        $this->out = $out;
    }

    /**
     * @return boolean
     */
    public function isAvailable()
    {
        return $this->available;
    }

    /**
     * @param boolean $available
     */
    public function setAvailable($available)
    {
        $this->available = $available;
    }

    /**
     * @return string
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * @param string $availability
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
    }

    /**
     * @return int
     */
    public function getAvailabilityDays()
    {
        return $this->availabilityDays;
    }

    /**
     * @param int $availabilityDays
     */
    public function setAvailabilityDays($availabilityDays)
    {
        $this->availabilityDays = $availabilityDays;
    }

    /**
     * @return \Datetime
     */
    public function getProviderDeadlineDate()
    {
        return $this->providerDeadlineDate;
    }

    /**
     * @param \Datetime $providerDeadlineDate
     */
    public function setProviderDeadlineDate($providerDeadlineDate)
    {
        $this->providerDeadlineDate = $providerDeadlineDate;
    }

    /**
     * @return \Datetime
     */
    public function getProviderDeliveryDate()
    {
        return $this->providerDeliveryDate;
    }

    /**
     * @param \Datetime $providerDeliveryDate
     */
    public function setProviderDeliveryDate($providerDeliveryDate)
    {
        $this->providerDeliveryDate = $providerDeliveryDate;
    }

    /**
     * @return int
     */
    public function getDeliveryDays()
    {
        return $this->deliveryDays;
    }

    /**
     * @param int $deliveryDays
     */
    public function setDeliveryDays($deliveryDays)
    {
        $this->deliveryDays = $deliveryDays;
    }

    /**
     * @return \Datetime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param \Datetime $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return int
     */
    public function getAvailabilityDeadline()
    {
        return $this->availabilityDeadline;
    }

    /**
     * @param int $availabilityDeadline
     */
    public function setAvailabilityDeadline($availabilityDeadline)
    {
        $this->availabilityDeadline = $availabilityDeadline;
    }

    /**
     * @return boolean
     */
    public function isPromotion()
    {
        return $this->promotion;
    }

    /**
     * @param boolean $promotion
     */
    public function setPromotion($promotion)
    {
        $this->promotion = $promotion;
    }

    /**
     * @return \Datetime
     */
    public function getInitialPromotion()
    {
        return $this->initialPromotion;
    }

    /**
     * @param \Datetime $initialPromotion
     */
    public function setInitialPromotion($initialPromotion)
    {
        $this->initialPromotion = $initialPromotion;
    }

    /**
     * @return \Datetime
     */
    public function getFinalPromotion()
    {
        return $this->finalPromotion;
    }

    /**
     * @param \Datetime $finalPromotion
     */
    public function setFinalPromotion($finalPromotion)
    {
        $this->finalPromotion = $finalPromotion;
    }

    /**
     * @return float
     */
    public function getDiscountValue()
    {
        return $this->discountValue;
    }

    /**
     * @param float $discountValue
     */
    public function setDiscountValue($discountValue)
    {
        $this->discountValue = $discountValue;
    }

    /**
     * @return float
     */
    public function getDiscountPercentage()
    {
        return $this->discountPercentage;
    }

    /**
     * @param float $discountPercentage
     */
    public function setDiscountPercentage($discountPercentage)
    {
        $this->discountPercentage = $discountPercentage;
    }

    /**
     * @return float
     */
    public function getRealPrice()
    {
        return $this->realPrice;
    }

    /**
     * @param float $realPrice
     */
    public function setRealPrice($realPrice)
    {
        $this->realPrice = $realPrice;
    }

    /**
     * @return float
     */
    public function getPromotionPrice()
    {
        return $this->promotionPrice;
    }

    /**
     * @param float $promotionPrice
     */
    public function setPromotionPrice($promotionPrice)
    {
        $this->promotionPrice = $promotionPrice;
    }

    /**
     * @return float
     */
    public function getProductPrice()
    {
        return $this->productPrice;
    }

    /**
     * @param float $productPrice
     */
    public function setProductPrice($productPrice)
    {
        $this->productPrice = $productPrice;
    }

    /**
     * @return float
     */
    public function getTotalprice()
    {
        return $this->totalprice;
    }

    /**
     * @param float $totalprice
     */
    public function setTotalprice($totalprice)
    {
        $this->totalprice = $totalprice;
    }

    /**
     * @return float
     */
    public function getCostPrice()
    {
        return $this->costPrice;
    }

    /**
     * @param float $costPrice
     */
    public function setCostPrice($costPrice)
    {
        $this->costPrice = $costPrice;
    }

    /**
     * @return int
     */
    public function getBranchOfficeId()
    {
        return $this->branchOfficeId;
    }

    /**
     * @param int $branchOfficeId
     */
    public function setBranchOfficeId($branchOfficeId)
    {
        $this->branchOfficeId = $branchOfficeId;
    }

    /**
     * @return integer
     */
    public function getCollectionConveyorCode()
    {
        return $this->collectionConveyorCode;
    }

    /**
     * @param integer $collectionConveyorCode
     */
    public function setCollectionConveyorCode($collectionConveyorCode)
    {
        $this->collectionConveyorCode = $collectionConveyorCode;
    }


    /**
     * Get featured
     *
     * @return boolean
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Get show
     *
     * @return boolean
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * Get out
     *
     * @return boolean
     */
    public function getOut()
    {
        return $this->out;
    }

    /**
     * Get available
     *
     * @return boolean
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * Get promotion
     *
     * @return boolean
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * @return CreatePromotionPaymentVO
     */
    public function getPromotionPayment()
    {
        return $this->promotionPayment;
    }

    /**
     * @param CreatePromotionPaymentVO $promotionPayment
     */
    public function setPromotionPayment($promotionPayment)
    {
        $this->promotionPayment = $promotionPayment;
    }

    /**
     * @return string
     */
    public function getPathXmlOc()
    {
        return $this->pathXmlOc;
    }

    /**
     * @param string $pathXmlOc
     */
    public function setPathXmlOc($pathXmlOc)
    {
        $this->pathXmlOc = $pathXmlOc;
    }
}
