<?php
namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * CreateCustomerVO
 *
 * @ORM\Entity()
 */
class CreateCustomerVO
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $cpf;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $rg;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $personType;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $genre;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     *
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ip;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param string $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return string
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param string $rg
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    }

    /**
     * @return string
     */
    public function getPersonType()
    {
        return $this->personType;
    }

    /**
     * @param string $personType
     */
    public function setPersonType($personType)
    {
        $this->personType = $personType;
    }

    /**
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param string $genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
    }

    /**
     * @return \Datetime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \Datetime $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param string $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

}
