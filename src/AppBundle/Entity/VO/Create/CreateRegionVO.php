<?php
namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * CreateRegionVO
 *
 * @ORM\Entity()
 */
class CreateRegionVO
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $dist;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $chooseCustomer;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $ibgeCode;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getDist()
    {
        return $this->dist;
    }

    /**
     * @param int $dist
     */
    public function setDist($dist)
    {
        $this->dist = $dist;
    }

    /**
     * @return boolean
     */
    public function isChooseCustomer()
    {
        return $this->chooseCustomer;
    }

    /**
     * @param boolean $chooseCustomer
     */
    public function setChooseCustomer($chooseCustomer)
    {
        $this->chooseCustomer = $chooseCustomer;
    }

    /**
     * @return int
     */
    public function getIbgeCode()
    {
        return $this->ibgeCode;
    }

    /**
     * @param int $ibgeCode
     */
    public function setIbgeCode($ibgeCode)
    {
        $this->ibgeCode = $ibgeCode;
    }


    /**
     * Get chooseCustomer
     *
     * @return boolean
     */
    public function getChooseCustomer()
    {
        return $this->chooseCustomer;
    }
}
