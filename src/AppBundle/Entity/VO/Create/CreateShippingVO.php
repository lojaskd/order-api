<?php
namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * CreateShippingVO
 *
 * @ORM\Entity()
 */
class CreateShippingVO
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $pattern;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $shippingType;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $fixedDeadline;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $fixedValue;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $freeDeadline;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $shippingCompanyDeadline;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $safety;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $safetyPrice;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $costPrice;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $deadline;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $mounting;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $mountingValue;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $ruleFreight;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     * @Type("array")
     */
    private $freight;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\VO\Create\CreateShippingProductVO>")
     */
    private $products;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\VO\Create\CreateShippingProviderVO>")
     */
    private $providers;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function isPattern()
    {
        return $this->pattern;
    }

    /**
     * @param boolean $pattern
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
    }

    /**
     * @return int
     */
    public function getShippingType()
    {
        return $this->shippingType;
    }

    /**
     * @param int $shippingType
     */
    public function setShippingType($shippingType)
    {
        $this->shippingType = $shippingType;
    }

    /**
     * @return int
     */
    public function getFixedDeadline()
    {
        return $this->fixedDeadline;
    }

    /**
     * @param int $fixedDeadline
     */
    public function setFixedDeadline($fixedDeadline)
    {
        $this->fixedDeadline = $fixedDeadline;
    }

    /**
     * @return float
     */
    public function getFixedValue()
    {
        return $this->fixedValue;
    }

    /**
     * @param float $fixedValue
     */
    public function setFixedValue($fixedValue)
    {
        $this->fixedValue = $fixedValue;
    }

    /**
     * @return int
     */
    public function getFreeDeadline()
    {
        return $this->freeDeadline;
    }

    /**
     * @param int $freeDeadline
     */
    public function setFreeDeadline($freeDeadline)
    {
        $this->freeDeadline = $freeDeadline;
    }

    /**
     * @return int
     */
    public function getShippingCompanyDeadline()
    {
        return $this->shippingCompanyDeadline;
    }

    /**
     * @param int $shippingCompanyDeadline
     */
    public function setShippingCompanyDeadline($shippingCompanyDeadline)
    {
        $this->shippingCompanyDeadline = $shippingCompanyDeadline;
    }

    /**
     * @return boolean
     */
    public function isSafety()
    {
        return $this->safety;
    }

    /**
     * @param boolean $safety
     */
    public function setSafety($safety)
    {
        $this->safety = $safety;
    }

    /**
     * @return float
     */
    public function getSafetyPrice()
    {
        return $this->safetyPrice;
    }

    /**
     * @param float $safetyPrice
     */
    public function setSafetyPrice($safetyPrice)
    {
        $this->safetyPrice = $safetyPrice;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getCostPrice()
    {
        return $this->costPrice;
    }

    /**
     * @param float $costPrice
     */
    public function setCostPrice($costPrice)
    {
        $this->costPrice = $costPrice;
    }

    /**
     * @return int
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param int $deadline
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
    }

    /**
     * @return boolean
     */
    public function isMounting()
    {
        return $this->mounting;
    }

    /**
     * @param boolean $mounting
     */
    public function setMounting($mounting)
    {
        $this->mounting = $mounting;
    }

    /**
     * @return float
     */
    public function getMountingValue()
    {
        return $this->mountingValue;
    }

    /**
     * @param float $mountingValue
     */
    public function setMountingValue($mountingValue)
    {
        $this->mountingValue = $mountingValue;
    }

    /**
     * @return string
     */
    public function getRuleFreight()
    {
        return $this->ruleFreight;
    }

    /**
     * @param string $ruleFreight
     */
    public function setRuleFreight($ruleFreight)
    {
        $this->ruleFreight = $ruleFreight;
    }

    /**
     * @return array
     */
    public function getFreight()
    {
        return $this->freight;
    }

    /**
     * @param array $freight
     */
    public function setFreight($freight)
    {
        $this->freight = $freight;
    }

    /**
     * @param $freight
     */
    public function addFreight($freight)
    {
        $this->freight[] = $freight;
    }

    /**
     * Get pattern
     *
     * @return boolean
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * Get safety
     *
     * @return boolean
     */
    public function getSafety()
    {
        return $this->safety;
    }

    /**
     * Get mounting
     *
     * @return boolean
     */
    public function getMounting()
    {
        return $this->mounting;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @param CreateShippingProductVO $products
     */
    public function addProducts($products)
    {
        $this->products[] = $products;
    }

    /**
     * @return array
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * @param array $providers
     */
    public function setProviders($providers)
    {
        $this->providers = $providers;
    }

    /**
     * @param $providerId
     * @param CreateShippingProviderVO $providers
     */
    public function addProviders($providerId, $providers)
    {
        $this->providers[$providerId] = $providers;
    }
}
