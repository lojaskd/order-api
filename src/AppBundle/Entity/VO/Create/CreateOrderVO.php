<?php
namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * CreateOrderVO
 *
 * @ORM\Entity()
 */
class CreateOrderVO
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var \AppBundle\Entity\VO\Create\CreateCustomerVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreateCustomerVO")
     * @Type("AppBundle\Entity\VO\Create\CreateCustomerVO")
     */
    private $customer;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $total;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $discount;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $observation;

    /**
     * @var \AppBundle\Entity\VO\Create\CreateRegionVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreateRegionVO")
     * @Type("AppBundle\Entity\VO\Create\CreateRegionVO")
     */
    private $region;

    /**
     * @var \AppBundle\Entity\VO\Create\CreateAddressVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreateAddressVO")
     * @Type("AppBundle\Entity\VO\Create\CreateAddressVO")
     */
    private $deliveryAddress;

    /**
     * @var \AppBundle\Entity\VO\Create\CreateAddressVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreateAddressVO")
     * @Type("AppBundle\Entity\VO\Create\CreateAddressVO")
     */
    private $billingAddress;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\VO\Create\CreateProductVO>")
     */
    private $products;

    /**
     * @var \AppBundle\Entity\VO\Create\CreatePaymentVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreatePaymentVO")
     * @Type("AppBundle\Entity\VO\Create\CreatePaymentVO")
     */
    private $firstPayment;

    /**
     * @var \AppBundle\Entity\VO\Create\CreatePaymentVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreatePaymentVO")
     * @Type("AppBundle\Entity\VO\Create\CreatePaymentVO")
     */
    private $secondPayment;

    /**
     * @var \AppBundle\Entity\VO\Create\CreateOriginVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreateOriginVO")
     * @Type("AppBundle\Entity\VO\Create\CreateOriginVO")
     */
    private $origin;

    /**
     * @var \AppBundle\Entity\VO\Create\CreateShippingVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreateShippingVO")
     * @Type("AppBundle\Entity\VO\Create\CreateShippingVO")
     */
    private $shipping;

    /**
     * @var \AppBundle\Entity\VO\Create\CreateCouponVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreateCouponVO")
     * @Type("AppBundle\Entity\VO\Create\CreateCouponVO")
     */
    private $coupon;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $assistance;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $exchange;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $buyback;

    /**
     * @var \AppBundle\Entity\VO\Create\CreateMarketPlaceVO
     *
     * @ORM\Column(type="\AppBundle\Entity\VO\Create\CreateMarketPlaceVO")
     * @Type("AppBundle\Entity\VO\Create\CreateMarketPlaceVO")
     */
    private $marketplace;
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $device;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $statusInternal;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $promotionPayment;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return CreateCustomerVO
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param CreateCustomerVO $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return CreateRegionVO
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param CreateRegionVO $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return CreateAddressVO
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @param CreateAddressVO $deliveryAddresses
     */
    public function setDeliveryAddress($deliveryAddresses)
    {
        $this->deliveryAddress = $deliveryAddresses;
    }

    /**
     * @return CreateAddressVO
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param CreateAddressVO $billingAddress
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @param CreateProductVO $product
     */
    public function addProduct($product)
    {
        $this->products[] = $product;
    }

    /**
     * @return CreatePaymentVO
     */
    public function getFirstPayment()
    {
        return $this->firstPayment;
    }

    /**
     * @param CreatePaymentVO $firstPayment
     */
    public function setFirstPayment($firstPayment)
    {
        $this->firstPayment = $firstPayment;
    }

    /**
     * @return CreatePaymentVO
     */
    public function getSecondPayment()
    {
        return $this->secondPayment;
    }

    /**
     * @param CreatePaymentVO $secondPayment
     */
    public function setSecondPayment($secondPayment)
    {
        $this->secondPayment = $secondPayment;
    }

    /**
     * @return CreateOriginVO
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param CreateOriginVO $origin
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
    }

    /**
     * @return CreateShippingVO
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param CreateShippingVO $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return CreateCouponVO
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param CreateCouponVO $coupon
     */
    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @return boolean
     */
    public function isAssistance()
    {
        return $this->assistance;
    }

    /**
     * @param boolean $assistance
     */
    public function setAssistance($assistance)
    {
        $this->assistance = $assistance;
    }

    /**
     * @return boolean
     */
    public function isExchange()
    {
        return $this->exchange;
    }

    /**
     * @param boolean $exchange
     */
    public function setExchange($exchange)
    {
        $this->exchange = $exchange;
    }

    /**
     * @return boolean
     */
    public function isBuyback()
    {
        return $this->buyback;
    }

    /**
     * @param boolean $buyback
     */
    public function setBuyback($buyback)
    {
        $this->buyback = $buyback;
    }

    /**
     * @return CreateMarketPlaceVO
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * @param CreateMarketPlaceVO $marketplace
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;
    }

    /**
     * @return array
     */
    public function getProductsId()
    {
        $productsId = array();

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $productsId[] = $product->getId();
        }

        return $productsId;
    }

    /**
     * @return array
     */
    public function getProductsQuantity()
    {
        $productsQtd = array();

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $productsQtd[] = $product->getQuantity();
        }

        return $productsQtd;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        $subtotal = 0;

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $subtotal += $product->getTotalPrice();
        }

        return $subtotal;
    }

    /**
     * @return float
     */
    public function getTotalCubage()
    {
        $cubage = 0;

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $cubage += $product->getCubage();
        }

        return $cubage;
    }

    /**
     * @return float
     */
    public function getTotalWeight()
    {
        $weight = 0;

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $weight += $product->getWeight();
        }

        return $weight;
    }

    /**
     * @return integer
     */
    public function getTotalQuantity()
    {
        $quantity = 0;

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $quantity += $product->getQuantity();
        }

        return $quantity;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        $categories = array();

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $categories[] = $product->getCategory();
        }

        return $categories;
    }

    /**
     * @return array
     */
    public function getProviders()
    {
        $providers = array();

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $providers[] = $product->getProvider();
        }

        return $providers;
    }

    /**
     * @return array
     */
    public function getPaymentsId()
    {
        $paymentsId = array();

        $paymentsId[] = $this->getFirstPayment()->getId();

        if ($this->getSecondPayment()) {
            $paymentsId[] = $this->getSecondPayment()->getId();
        }

        return $paymentsId;
    }

    /**
     * @return string
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param string $device
     */
    public function setDevice($device)
    {
        $this->device = $device;
    }

    /**
     * @return int
     */
    public function getStatusInternal()
    {
        return $this->statusInternal;
    }

    /**
     * @param int $statusInternal
     */
    public function setStatusInternal($statusInternal)
    {
        $this->statusInternal = $statusInternal;
    }



    /* ###########################################
     * (START) Get order dates to create the order
     * ########################################### */

    /**
     * @return integer
     */
    public function getMaxDeliveryDeadline()
    {
        $deliveryDeadline = array();

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $deliveryDeadline[] = $product->getAvailabilityDeadline();
        }

        return max($deliveryDeadline);
    }

    /**
     * @return integer
     */
    public function getMaxAvailabilityDays()
    {
        $availabilityDays = array();

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $availabilityDays[] = $product->getAvailabilityDays();
        }

        return max($availabilityDays);
    }

    /**
     * @return \Datetime
     */
    public function getMaxDeliveryDate()
    {
        $deliveryDate = array();

        /* @var $product CreateProductVO */
        foreach ($this->products as $product) {
            $deliveryDate[] = $product->getDeliveryDate();
        }

        return max($deliveryDate);
    }

    /**
     * @return integer
     */
    public function getMaxShippingDeadline()
    {
        $shippingDeadline = 0;
        if ($this->getMaxAvailabilityDays() < $this->getMaxDeliveryDeadline()) {
            $shippingDeadline = ($this->getMaxDeliveryDeadline() - $this->getMaxAvailabilityDays());
        }

        return $shippingDeadline;
    }

    /* #########################################
     * (END) Get order dates to create the order
     * ######################################### */
    

    /**
     * Get assistance
     *
     * @return boolean
     */
    public function getAssistance()
    {
        return $this->assistance;
    }

    /**
     * Get exchange
     *
     * @return boolean
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * Get buyback
     *
     * @return boolean
     */
    public function getBuyback()
    {
        return $this->buyback;
    }

    /**
     * @return bool
     */
    public function isPromotionPayment()
    {
        return $this->promotionPayment;
    }

    /**
     * @param bool $promotionPayment
     */
    public function setPromotionPayment($promotionPayment)
    {
        $this->promotionPayment = $promotionPayment;
    }
}
