<?php

namespace AppBundle\Entity\VO\Create;

use JMS\Serializer\Annotation\Type;
use Doctrine\ORM\Mapping as ORM;

/**
 * CreatePurchaseOrderProductVO
 *
 * @ORM\Entity()
 */
class CreatePurchaseOrderProductVO
{

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @ORM\Id
     * @Type("string")
     */
    private $id;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}
