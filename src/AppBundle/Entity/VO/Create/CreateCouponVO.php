<?php
namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;


/**
 * CreateCouponVO
 *
 * @ORM\Entity()
 */
class CreateCouponVO
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $idPromo;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $promotionName;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $freeShipping;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $fixedShipping;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $fixedValue;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $discountValue;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     * @Type("boolean")
     */
    private $shippingIncluded;


    /**
     * @return int
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * @param int $idPromo
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getPromotionName()
    {
        return $this->promotionName;
    }

    /**
     * @param string $promotionName
     */
    public function setPromotionName($promotionName)
    {
        $this->promotionName = $promotionName;
    }

    /**
     * @return boolean
     */
    public function isFreeShipping()
    {
        return $this->freeShipping;
    }

    /**
     * @param boolean $freeShipping
     */
    public function setFreeShipping($freeShipping)
    {
        $this->freeShipping = $freeShipping;
    }

    /**
     * @return boolean
     */
    public function isFixedShipping()
    {
        return $this->fixedShipping;
    }

    /**
     * @param boolean $fixedShipping
     */
    public function setFixedShipping($fixedShipping)
    {
        $this->fixedShipping = $fixedShipping;
    }

    /**
     * @return float
     */
    public function getFixedValue()
    {
        return $this->fixedValue;
    }

    /**
     * @param float $fixedValue
     */
    public function setFixedValue($fixedValue)
    {
        $this->fixedValue = $fixedValue;
    }

    /**
     * @return float
     */
    public function getDiscountValue()
    {
        return $this->discountValue;
    }

    /**
     * @param float $discountValue
     */
    public function setDiscountValue($discountValue)
    {
        $this->discountValue = $discountValue;
    }

    /**
     * @return boolean
     */
    public function isShippingIncluded()
    {
        return $this->shippingIncluded;
    }

    /**
     * @param boolean $shippingIncluded
     */
    public function setShippingIncluded($shippingIncluded)
    {
        $this->shippingIncluded = $shippingIncluded;
    }


    /**
     * Get freeShipping
     *
     * @return boolean
     */
    public function getFreeShipping()
    {
        return $this->freeShipping;
    }

    /**
     * Get fixedShipping
     *
     * @return boolean
     */
    public function getFixedShipping()
    {
        return $this->fixedShipping;
    }

    /**
     * Get shippingIncluded
     *
     * @return boolean
     */
    public function getShippingIncluded()
    {
        return $this->shippingIncluded;
    }
}
