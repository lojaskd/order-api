<?php
namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * CreatePaymentVO
 *
 * @ORM\Entity()
 */
class CreatePaymentVO
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $paymentFormId;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $indexer;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $instalment;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $instalmentValue;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $description;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("datetime")
     */
    private $deadline;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $paymentFormDesc;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getPaymentFormId()
    {
        return $this->paymentFormId;
    }

    /**
     * @param int $paymentFormId
     */
    public function setPaymentFormId($paymentFormId)
    {
        $this->paymentFormId = $paymentFormId;
    }

    /**
     * @return float
     */
    public function getIndexer()
    {
        return $this->indexer;
    }

    /**
     * @param float $indexer
     */
    public function setIndexer($indexer)
    {
        $this->indexer = $indexer;
    }

    /**
     * @return int
     */
    public function getInstalment()
    {
        return $this->instalment;
    }

    /**
     * @param int $instalment
     */
    public function setInstalment($instalment)
    {
        $this->instalment = $instalment;
    }

    /**
     * @return float
     */
    public function getInstalmentValue()
    {
        return $this->instalmentValue;
    }

    /**
     * @param float $instalmentValue
     */
    public function setInstalmentValue($instalmentValue)
    {
        $this->instalmentValue = $instalmentValue;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \Datetime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param \Datetime $deadline
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
    }

    /**
     * @return string
     */
    public function getPaymentFormDesc()
    {
        return $this->paymentFormDesc;
    }

    /**
     * @param string $paymentFormDesc
     */
    public function setPaymentFormDesc($paymentFormDesc)
    {
        $this->paymentFormDesc = $paymentFormDesc;
    }

    /**
     * @return float
     */
    public function getPaymentTotalAmount()
    {
        return floatval($this->instalment * $this->instalmentValue);
    }

}
