<?php

namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * CreateInvoiceItemVO
 *
 * @ORM\Entity()
 */
class CreateInvoiceItemVO
{

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @ORM\Id
     * @Type("string")
     */
    private $productReference;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $returnedDate;

    /**
     * @var boolean
     *
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $returned;


    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $qty;

    /**
     * @return string
     */
    function getProductReference()
    {
        return $this->productReference;
    }

    /**
     * @param string productReference
     */
    function setProductReference($productReference)
    {
        $this->productReference = $productReference;
    }

    /**
     * @return \Datetime
     */
    function getReturnedDate()
    {
        return $this->returnedDate;
    }

    /**
     * @param \Datetime $returnedDate
     */
    function setReturnedDate(\Datetime $returnedDate)
    {
        $this->returnedDate = $returnedDate;
    }

    /**
     * @return boolean
     */
    function isReturned()
    {
        return $this->returned;
    }

    /**
     * @param boolean $returned
     */
    function setReturned($returned)
    {
        $this->returned = $returned;
    }

    /**
     * @return int
     */
    function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     */
    function setQty($qty)
    {
        $this->qty = $qty;
    }


    /**
     * Get returned
     *
     * @return boolean
     */
    public function getReturned()
    {
        return $this->returned;
    }
}
