<?php

namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * CreateInvoiceVO
 *
 * @ORM\Entity()
 */
class CreateInvoiceVO
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $invoiceId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $branchId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $serie;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $createdDate;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $shippingDate;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $predictDeliveryDate;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $returnedDate;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $returned;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("DateTime<'Y-m-d'>")
     */
    private $deliveryDate;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $key;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Type("boolean")
     */
    private $marketplaceIntegration;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @var string
     *
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $shipperName;

    /**
     * @var string
     *
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $shipperDoc;

    /**
     * @var string
     *
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $hub;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\Create\CreateInvoiceItemVO>")
     */
    private $itens;

    /**
     * @return int
     */
    function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     */
    function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return int
     */
    function getBranchId()
    {
        return $this->branchId;
    }

    /**
     * @param int $branchId
     */
    function setBranchId($branchId)
    {
        $this->branchId = $branchId;
    }

    /**
     * @return string
     */
    function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param string $serie
     */
    function setSerie($serie)
    {
        $this->serie = $serie;
    }

    /**
     * @return \Datetime
     */
    function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \Datetime $createdDate
     */
    function setCreatedDate(\Datetime $createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \Datetime
     */
    function getShippingDate()
    {
        return $this->shippingDate;
    }

    /**
     * @param \Datetime $shippingDate
     */
    function setShippingDate(\Datetime $shippingDate)
    {
        $this->shippingDate = $shippingDate;
    }

    /**
     * @return \Datetime
     */
    function getPredictDeliveryDate()
    {
        return $this->predictDeliveryDate;
    }

    /**
     * @param \Datetime $predictDeliveryDate
     */
    function setPredictDeliveryDate(\Datetime $predictDeliveryDate)
    {
        $this->predictDeliveryDate = $predictDeliveryDate;
    }

    /**
     * @return \Datetime
     */
    function getReturnedDate()
    {
        return $this->returnedDate;
    }

    /**
     * @param \Datetime $returnedDate
     */
    function setReturnedDate(\Datetime $returnedDate)
    {
        $this->returnedDate = $returnedDate;
    }

    /**
     * @return boolean
     */
    function isReturned()
    {
        return $this->returned;
    }

    /**
     * @param boolean $returned
     */
    function setReturned($returned)
    {
        $this->returned = $returned;
    }

    /**
     * @return \Datetime
     */
    function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param \Datetime $deliveryDate
     */
    function setDeliveryDate(\Datetime $deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return string
     */
    function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return boolean
     */
    function isMarketplaceIntegration()
    {
        return $this->marketplaceIntegration;
    }

    /**
     * @param boolean $marketplaceIntegration
     */
    function setMarketplaceIntegration($marketplaceIntegration)
    {
        $this->marketplaceIntegration = $marketplaceIntegration;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

    /**
     * @return array
     */
    public function getItens()
    {
        return $this->itens;
    }

    /**
     * @param array $itens
     */
    public function setItens(Array $itens)
    {
        $this->itens = $itens;
    }

    /**
     * @param CreateInvoiceItemVO $item
     */
    public function addItem(CreateInvoiceItemVO $item)
    {
        $this->itens[] = $item;
    }

    /**
     * @return string
     */
    public function getShipperName()
    {
        return $this->shipperName;
    }

    /**
     * @param string $shipperName
     */
    public function setShipperName($shipperName)
    {
        $this->shipperName = $shipperName;
    }

    /**
     * @return string
     */
    public function getShipperDoc()
    {
        return $this->shipperDoc;
    }

    /**
     * @param string $shipperDoc
     */
    public function setShipperDoc($shipperDoc)
    {
        $this->shipperDoc = $shipperDoc;
    }

    /**
     * @return string
     */
    public function getHub()
    {
        return $this->hub;
    }

    /**
     * @param string $hub
     */
    public function setHub($hub)
    {
        $this->hub = $hub;
    }



    /**
     * Get returned
     *
     * @return boolean
     */
    public function getReturned()
    {
        return $this->returned;
    }

    /**
     * Get marketplaceIntegration
     *
     * @return boolean
     */
    public function getMarketplaceIntegration()
    {
        return $this->marketplaceIntegration;
    }
}
