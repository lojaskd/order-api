<?php

namespace AppBundle\Entity\VO\Create;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * CreatePurchaseOrderVO
 *
 * @ORM\Entity()
 */
class CreatePurchaseOrderVO
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $purchaseOrderId;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("datetime")
     */
    private $date;

    /**
     * @var array
     * 
     * @ORM\Column(type="array")
     * @Type("array<AppBundle\Entity\VO\Create\CreatePurchaseOrderProductVO>")
     */
    private $products;

    /**
     * @var string
     * 
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $clerk;

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getPurchaseOrderId()
    {
        return $this->purchaseOrderId;
    }

    /**
     * @param int $purchaseOrderId
     */
    public function setPurchaseOrderId($purchaseOrderId)
    {
        $this->purchaseOrderId = $purchaseOrderId;
    }

    /**
     * @return \Datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \Datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @param \AppBundle\Entity\VO\Create\CreatePurchaseOrderProductVO $product
     */
    public function addProduct($product)
    {
        $this->products[] = $product;
    }

    /**
     * @return string
     */
    public function getClerk()
    {
        return $this->clerk;
    }

    /**
     * @param string $clerk
     */
    public function setClerk($clerk)
    {
        $this->clerk = $clerk;
    }

}
