<?php
namespace AppBundle\Entity\VO;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * OrderPaymentVO
 *
 * @ORM\Entity()
 */
class OrderPaymentVO
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $idPaymentForm;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $indexer;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $instalment;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     * @Type("float")
     */
    private $instalmentValue;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $description;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("datetime")
     */
    private $deadline;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $paymentFormDesc;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $billingAccount;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Type("string")
     */
    private $billingAccountName;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdPaymentForm()
    {
        return $this->idPaymentForm;
    }

    /**
     * @param int $idPaymentForm
     */
    public function setIdPaymentForm($idPaymentForm)
    {
        $this->idPaymentForm = $idPaymentForm;
    }

    /**
     * @return float
     */
    public function getIndexer()
    {
        return $this->indexer;
    }

    /**
     * @param float $indexer
     */
    public function setIndexer($indexer)
    {
        $this->indexer = $indexer;
    }

    /**
     * @return int
     */
    public function getInstalment()
    {
        return $this->instalment;
    }

    /**
     * @param int $instalment
     */
    public function setInstalment($instalment)
    {
        $this->instalment = $instalment;
    }

    /**
     * @return float
     */
    public function getInstalmentValue()
    {
        return $this->instalmentValue;
    }

    /**
     * @param float $instalmentValue
     */
    public function setInstalmentValue($instalmentValue)
    {
        $this->instalmentValue = $instalmentValue;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \Datetime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param \Datetime $deadline
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
    }

    /**
     * @return string
     */
    public function getPaymentFormDesc()
    {
        return $this->paymentFormDesc;
    }

    /**
     * @param string $paymentFormDesc
     */
    public function setPaymentFormDesc($paymentFormDesc)
    {
        $this->paymentFormDesc = $paymentFormDesc;
    }

    /**
     * @return float
     */
    public function getPaymentTotalAmount()
    {
        return floatval($this->instalment * $this->instalmentValue);
    }

    /**
     * @return string
     */
    public function getBillingAccount()
    {
        return $this->billingAccount;
    }

    /**
     * @param string $billingAccount
     */
    public function setBillingAccount($billingAccount)
    {
        $this->billingAccount = $billingAccount;
    }

    /**
     * @return string
     */
    public function getBillingAccountName()
    {
        return $this->billingAccountName;
    }

    /**
     * @param string $billingAccountName
     */
    public function setBillingAccountName($billingAccountName)
    {
        $this->billingAccountName = $billingAccountName;
    }

}
