<?php
namespace AppBundle\Entity\VO;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
/**
 * OrderStatusProductVO
 *
 * @ORM\Entity()
 */
class OrderStatusProductVO
{

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $productId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @Type("integer")
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $providerDays;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $availabilityDays;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     * @Type("datetime")
     */
    private $providerDeadline;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $providerStock;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $storeStock;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Type("integer")
     */
    private $branchStockId;


    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getProviderDays()
    {
        return $this->providerDays;
    }

    /**
     * @param int $providerDays
     */
    public function setProviderDays($providerDays)
    {
        $this->providerDays = $providerDays;
    }

    /**
     * @return int
     */
    public function getAvailabilityDays()
    {
        return $this->availabilityDays;
    }

    /**
     * @param int $availabilityDays
     */
    public function setAvailabilityDays($availabilityDays)
    {
        $this->availabilityDays = $availabilityDays;
    }

    /**
     * @return \Datetime
     */
    public function getProviderDeadline()
    {
        return $this->providerDeadline;
    }

    /**
     * @param \Datetime $providerDeadline
     */
    public function setProviderDeadline($providerDeadline)
    {
        $this->providerDeadline = $providerDeadline;
    }

    /**
     * @return int
     */
    public function getProviderStock()
    {
        return $this->providerStock;
    }

    /**
     * @param int $providerStock
     */
    public function setProviderStock($providerStock)
    {
        $this->providerStock = $providerStock;
    }

    /**
     * @return int
     */
    public function getStoreStock()
    {
        return $this->storeStock;
    }

    /**
     * @param int $storeStock
     */
    public function setStoreStock($storeStock)
    {
        $this->storeStock = $storeStock;
    }

    /**
     * @return int
     */
    public function getBranchStockId()
    {
        return $this->branchStockId;
    }

    /**
     * @param int $branchStockId
     */
    public function setBranchStockId($branchStockId)
    {
        $this->branchStockId = $branchStockId;
    }

}
