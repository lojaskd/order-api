<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPagfor
 *
 * @ORM\Table(name="b2c_pagfor", indexes={@ORM\Index(name="padrao", columns={"padrao"}), @ORM\Index(name="nome", columns={"nome"}), @ORM\Index(name="id_pagpag", columns={"id_pagpag"}), @ORM\Index(name="pai", columns={"pai"}), @ORM\Index(name="idx_pai_padrao", columns={"pai", "padrao", "num_parcelas"}), @ORM\Index(name="idx_pai_pag", columns={"pai", "num_parcelas", "id_pagpag"})})
 * @ORM\Entity
 */
class B2cPagfor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagfor", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPagfor;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagpag", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idPagpag;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nome;

    /**
     * @var float
     *
     * @ORM\Column(name="val_minimo", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $valMinimo;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_parcelas", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $numParcelas;

    /**
     * @var float
     *
     * @ORM\Column(name="indexador", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $indexador;

    /**
     * @var boolean
     *
     * @ORM\Column(name="padrao", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $padrao;

    /**
     * @var integer
     *
     * @ORM\Column(name="pai", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pai;

    /**
     * @param $idPagfor
     * @return $this
     */
    public function setIdPagfor($idPagfor)
    {
        $this->idPagfor = $idPagfor;
        return $this;
    }

    /**
     * Get idPagfor
     *
     * @return integer
     */
    public function getIdPagfor()
    {
        return $this->idPagfor;
    }

    /**
     * Set idPagpag
     *
     * @param integer $idPagpag
     *
     * @return B2cPagfor
     */
    public function setIdPagpag($idPagpag)
    {
        $this->idPagpag = $idPagpag;

        return $this;
    }

    /**
     * Get idPagpag
     *
     * @return integer
     */
    public function getIdPagpag()
    {
        return $this->idPagpag;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cPagfor
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set valMinimo
     *
     * @param float $valMinimo
     *
     * @return B2cPagfor
     */
    public function setValMinimo($valMinimo)
    {
        $this->valMinimo = $valMinimo;

        return $this;
    }

    /**
     * Get valMinimo
     *
     * @return float
     */
    public function getValMinimo()
    {
        return $this->valMinimo;
    }

    /**
     * Set numParcelas
     *
     * @param integer $numParcelas
     *
     * @return B2cPagfor
     */
    public function setNumParcelas($numParcelas)
    {
        $this->numParcelas = $numParcelas;

        return $this;
    }

    /**
     * Get numParcelas
     *
     * @return integer
     */
    public function getNumParcelas()
    {
        return $this->numParcelas;
    }

    /**
     * Set indexador
     *
     * @param float $indexador
     *
     * @return B2cPagfor
     */
    public function setIndexador($indexador)
    {
        $this->indexador = $indexador;

        return $this;
    }

    /**
     * Get indexador
     *
     * @return float
     */
    public function getIndexador()
    {
        return $this->indexador;
    }

    /**
     * Set padrao
     *
     * @param boolean $padrao
     *
     * @return B2cPagfor
     */
    public function setPadrao($padrao)
    {
        $this->padrao = $padrao;

        return $this;
    }

    /**
     * Get padrao
     *
     * @return boolean
     */
    public function getPadrao()
    {
        return $this->padrao;
    }

    /**
     * Set pai
     *
     * @param integer $pai
     *
     * @return B2cPagfor
     */
    public function setPai($pai)
    {
        $this->pai = $pai;

        return $this;
    }

    /**
     * Get pai
     *
     * @return integer
     */
    public function getPai()
    {
        return $this->pai;
    }
}
