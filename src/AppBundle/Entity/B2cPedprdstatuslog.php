<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPedprdstatuslog
 *
 * @ORM\Table(name="b2c_pedprdstatuslog")
 * @ORM\Entity
 */
class B2cPedprdstatuslog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=3, nullable=true)
     */
    private $tipo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", nullable=true)
     */
    private $idPedped;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", nullable=true)
     */
    private $idPrdprd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="valor_antigo", type="date", nullable=true)
     */
    private $valorAntigo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="valor_novo", type="date", nullable=true)
     */
    private $valorNovo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_tentativa", type="datetime", nullable=true)
     */
    private $dataTentativa;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return B2cPedprdstatuslog
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set idPedped
     *
     * @param integer $idPedped
     *
     * @return B2cPedprdstatuslog
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;

        return $this;
    }

    /**
     * Get idPedped
     *
     * @return integer
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * Set idPrdprd
     *
     * @param integer $idPrdprd
     *
     * @return B2cPedprdstatuslog
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;

        return $this;
    }

    /**
     * Get idPrdprd
     *
     * @return integer
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * Set valorAntigo
     *
     * @param \DateTime $valorAntigo
     *
     * @return B2cPedprdstatuslog
     */
    public function setValorAntigo($valorAntigo)
    {
        $this->valorAntigo = $valorAntigo;

        return $this;
    }

    /**
     * Get valorAntigo
     *
     * @return \DateTime
     */
    public function getValorAntigo()
    {
        return $this->valorAntigo;
    }

    /**
     * Set valorNovo
     *
     * @param \DateTime $valorNovo
     *
     * @return B2cPedprdstatuslog
     */
    public function setValorNovo($valorNovo)
    {
        $this->valorNovo = $valorNovo;

        return $this;
    }

    /**
     * Get valorNovo
     *
     * @return \DateTime
     */
    public function getValorNovo()
    {
        return $this->valorNovo;
    }

    /**
     * Set dataTentativa
     *
     * @param \DateTime $dataTentativa
     *
     * @return B2cPedprdstatuslog
     */
    public function setDataTentativa($dataTentativa)
    {
        $this->dataTentativa = $dataTentativa;

        return $this;
    }

    /**
     * Get dataTentativa
     *
     * @return \DateTime
     */
    public function getDataTentativa()
    {
        return $this->dataTentativa;
    }
}
