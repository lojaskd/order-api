<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPagpag
 *
 * @ORM\Table(name="b2c_pagpag", indexes={@ORM\Index(name="padrao", columns={"padrao"}), @ORM\Index(name="nome", columns={"nome"}), @ORM\Index(name="composto", columns={"composto"}), @ORM\Index(name="ativo", columns={"ativo"})})
 * @ORM\Entity
 */
class B2cPagpag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagpag", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPagpag;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ativo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="padrao", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $padrao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prioridade", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prioridade;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_fechamento", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $descFechamento;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_confirmacao", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $descConfirmacao;

    /**
     * @var string
     *
     * @ORM\Column(name="bandeira", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $bandeira;

    /**
     * @var string
     *
     * @ORM\Column(name="operadora", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $operadora;

    /**
     * @var string
     *
     * @ORM\Column(name="capturar", type="string", length=20, precision=0, scale=0, nullable=true, unique=false)
     */
    private $capturar;

    /**
     * @var string
     *
     * @ORM\Column(name="autorizacao", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $autorizacao;

    /**
     * @var float
     *
     * @ORM\Column(name="val_maximo", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $valMaximo;

    /**
     * @var float
     *
     * @ORM\Column(name="val_minimo", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $valMinimo;

    /**
     * @var float
     *
     * @ORM\Column(name="par_minimo", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $parMinimo;

    /**
     * @var float
     *
     * @ORM\Column(name="val_entrada", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $valEntrada;

    /**
     * @var float
     *
     * @ORM\Column(name="prazo", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $prazo;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_loja", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nomeLoja;

    /**
     * @var float
     *
     * @ORM\Column(name="juros_mes", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $jurosMes;

    /**
     * @var float
     *
     * @ORM\Column(name="juros_ano", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $jurosAno;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_tac", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $valorTac;

    /**
     * @var float
     *
     * @ORM\Column(name="iof", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $iof;

    /**
     * @var integer
     *
     * @ORM\Column(name="carencia", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $carencia;

    /**
     * @var integer
     *
     * @ORM\Column(name="parcelas_sem_juros", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $parcelasSemJuros;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $desconto;

    /**
     * @var integer
     *
     * @ORM\Column(name="conta_1_ordem", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta1Ordem;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_1_nome", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta1Nome;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_1_cedente", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta1Cedente;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_1_agencia", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta1Agencia;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_1_numero", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta1Numero;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_1_codigo", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta1Codigo;

    /**
     * @var float
     *
     * @ORM\Column(name="conta_1_maximo", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $conta1Maximo;

    /**
     * @var integer
     *
     * @ORM\Column(name="conta_2_ordem", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta2Ordem;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_2_nome", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta2Nome;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_2_cedente", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta2Cedente;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_2_agencia", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta2Agencia;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_2_numero", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta2Numero;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_2_codigo", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta2Codigo;

    /**
     * @var float
     *
     * @ORM\Column(name="conta_2_maximo", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $conta2Maximo;

    /**
     * @var integer
     *
     * @ORM\Column(name="conta_3_ordem", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta3Ordem;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_3_nome", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta3Nome;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_3_cedente", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta3Cedente;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_3_agencia", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta3Agencia;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_3_numero", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta3Numero;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_3_codigo", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta3Codigo;

    /**
     * @var float
     *
     * @ORM\Column(name="conta_3_maximo", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $conta3Maximo;

    /**
     * @var integer
     *
     * @ORM\Column(name="conta_4_ordem", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta4Ordem;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_4_nome", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta4Nome;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_4_cedente", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta4Cedente;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_4_agencia", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta4Agencia;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_4_numero", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $conta4Numero;

    /**
     * @var string
     *
     * @ORM\Column(name="conta_4_codigo", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $conta4Codigo;

    /**
     * @var float
     *
     * @ORM\Column(name="conta_4_maximo", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $conta4Maximo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="composto", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $composto;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagboleto", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idPagboleto;

    /**
     * @var string
     *
     * @ORM\Column(name="id_paypal", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $idPaypal;

    /**
     * @var string
     *
     * @ORM\Column(name="senha_paypal", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $senhaPaypal;

    /**
     * @var string
     *
     * @ORM\Column(name="assinatura_paypal", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $assinaturaPaypal;

    /**
     * @param $idPagpag
     * @return $this
     */
    public function setIdPagpag($idPagpag)
    {
        $this->idPagpag = $idPagpag;
        return $this;
    }

    /**
     * Get idPagpag
     *
     * @return integer
     */
    public function getIdPagpag()
    {
        return $this->idPagpag;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return B2cPagpag
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set padrao
     *
     * @param boolean $padrao
     *
     * @return B2cPagpag
     */
    public function setPadrao($padrao)
    {
        $this->padrao = $padrao;

        return $this;
    }

    /**
     * Get padrao
     *
     * @return boolean
     */
    public function getPadrao()
    {
        return $this->padrao;
    }

    /**
     * Set prioridade
     *
     * @param boolean $prioridade
     *
     * @return B2cPagpag
     */
    public function setPrioridade($prioridade)
    {
        $this->prioridade = $prioridade;

        return $this;
    }

    /**
     * Get prioridade
     *
     * @return boolean
     */
    public function getPrioridade()
    {
        return $this->prioridade;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cPagpag
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descFechamento
     *
     * @param string $descFechamento
     *
     * @return B2cPagpag
     */
    public function setDescFechamento($descFechamento)
    {
        $this->descFechamento = $descFechamento;

        return $this;
    }

    /**
     * Get descFechamento
     *
     * @return string
     */
    public function getDescFechamento()
    {
        return $this->descFechamento;
    }

    /**
     * Set descConfirmacao
     *
     * @param string $descConfirmacao
     *
     * @return B2cPagpag
     */
    public function setDescConfirmacao($descConfirmacao)
    {
        $this->descConfirmacao = $descConfirmacao;

        return $this;
    }

    /**
     * Get descConfirmacao
     *
     * @return string
     */
    public function getDescConfirmacao()
    {
        return $this->descConfirmacao;
    }

    /**
     * Set bandeira
     *
     * @param string $bandeira
     *
     * @return B2cPagpag
     */
    public function setBandeira($bandeira)
    {
        $this->bandeira = $bandeira;

        return $this;
    }

    /**
     * Get bandeira
     *
     * @return string
     */
    public function getBandeira()
    {
        return $this->bandeira;
    }

    /**
     * Set operadora
     *
     * @param string $operadora
     *
     * @return B2cPagpag
     */
    public function setOperadora($operadora)
    {
        $this->operadora = $operadora;

        return $this;
    }

    /**
     * Get operadora
     *
     * @return string
     */
    public function getOperadora()
    {
        return $this->operadora;
    }

    /**
     * Set capturar
     *
     * @param string $capturar
     *
     * @return B2cPagpag
     */
    public function setCapturar($capturar)
    {
        $this->capturar = $capturar;

        return $this;
    }

    /**
     * Get capturar
     *
     * @return string
     */
    public function getCapturar()
    {
        return $this->capturar;
    }

    /**
     * Set autorizacao
     *
     * @param string $autorizacao
     *
     * @return B2cPagpag
     */
    public function setAutorizacao($autorizacao)
    {
        $this->autorizacao = $autorizacao;

        return $this;
    }

    /**
     * Get autorizacao
     *
     * @return string
     */
    public function getAutorizacao()
    {
        return $this->autorizacao;
    }

    /**
     * Set valMaximo
     *
     * @param float $valMaximo
     *
     * @return B2cPagpag
     */
    public function setValMaximo($valMaximo)
    {
        $this->valMaximo = $valMaximo;

        return $this;
    }

    /**
     * Get valMaximo
     *
     * @return float
     */
    public function getValMaximo()
    {
        return $this->valMaximo;
    }

    /**
     * Set valMinimo
     *
     * @param float $valMinimo
     *
     * @return B2cPagpag
     */
    public function setValMinimo($valMinimo)
    {
        $this->valMinimo = $valMinimo;

        return $this;
    }

    /**
     * Get valMinimo
     *
     * @return float
     */
    public function getValMinimo()
    {
        return $this->valMinimo;
    }

    /**
     * Set parMinimo
     *
     * @param float $parMinimo
     *
     * @return B2cPagpag
     */
    public function setParMinimo($parMinimo)
    {
        $this->parMinimo = $parMinimo;

        return $this;
    }

    /**
     * Get parMinimo
     *
     * @return float
     */
    public function getParMinimo()
    {
        return $this->parMinimo;
    }

    /**
     * Set valEntrada
     *
     * @param float $valEntrada
     *
     * @return B2cPagpag
     */
    public function setValEntrada($valEntrada)
    {
        $this->valEntrada = $valEntrada;

        return $this;
    }

    /**
     * Get valEntrada
     *
     * @return float
     */
    public function getValEntrada()
    {
        return $this->valEntrada;
    }

    /**
     * Set prazo
     *
     * @param float $prazo
     *
     * @return B2cPagpag
     */
    public function setPrazo($prazo)
    {
        $this->prazo = $prazo;

        return $this;
    }

    /**
     * Get prazo
     *
     * @return float
     */
    public function getPrazo()
    {
        return $this->prazo;
    }

    /**
     * Set nomeLoja
     *
     * @param string $nomeLoja
     *
     * @return B2cPagpag
     */
    public function setNomeLoja($nomeLoja)
    {
        $this->nomeLoja = $nomeLoja;

        return $this;
    }

    /**
     * Get nomeLoja
     *
     * @return string
     */
    public function getNomeLoja()
    {
        return $this->nomeLoja;
    }

    /**
     * Set jurosMes
     *
     * @param float $jurosMes
     *
     * @return B2cPagpag
     */
    public function setJurosMes($jurosMes)
    {
        $this->jurosMes = $jurosMes;

        return $this;
    }

    /**
     * Get jurosMes
     *
     * @return float
     */
    public function getJurosMes()
    {
        return $this->jurosMes;
    }

    /**
     * Set jurosAno
     *
     * @param float $jurosAno
     *
     * @return B2cPagpag
     */
    public function setJurosAno($jurosAno)
    {
        $this->jurosAno = $jurosAno;

        return $this;
    }

    /**
     * Get jurosAno
     *
     * @return float
     */
    public function getJurosAno()
    {
        return $this->jurosAno;
    }

    /**
     * Set valorTac
     *
     * @param float $valorTac
     *
     * @return B2cPagpag
     */
    public function setValorTac($valorTac)
    {
        $this->valorTac = $valorTac;

        return $this;
    }

    /**
     * Get valorTac
     *
     * @return float
     */
    public function getValorTac()
    {
        return $this->valorTac;
    }

    /**
     * Set iof
     *
     * @param float $iof
     *
     * @return B2cPagpag
     */
    public function setIof($iof)
    {
        $this->iof = $iof;

        return $this;
    }

    /**
     * Get iof
     *
     * @return float
     */
    public function getIof()
    {
        return $this->iof;
    }

    /**
     * Set carencia
     *
     * @param integer $carencia
     *
     * @return B2cPagpag
     */
    public function setCarencia($carencia)
    {
        $this->carencia = $carencia;

        return $this;
    }

    /**
     * Get carencia
     *
     * @return integer
     */
    public function getCarencia()
    {
        return $this->carencia;
    }

    /**
     * Set parcelasSemJuros
     *
     * @param integer $parcelasSemJuros
     *
     * @return B2cPagpag
     */
    public function setParcelasSemJuros($parcelasSemJuros)
    {
        $this->parcelasSemJuros = $parcelasSemJuros;

        return $this;
    }

    /**
     * Get parcelasSemJuros
     *
     * @return integer
     */
    public function getParcelasSemJuros()
    {
        return $this->parcelasSemJuros;
    }

    /**
     * Set desconto
     *
     * @param float $desconto
     *
     * @return B2cPagpag
     */
    public function setDesconto($desconto)
    {
        $this->desconto = $desconto;

        return $this;
    }

    /**
     * Get desconto
     *
     * @return float
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * Set conta1Ordem
     *
     * @param integer $conta1Ordem
     *
     * @return B2cPagpag
     */
    public function setConta1Ordem($conta1Ordem)
    {
        $this->conta1Ordem = $conta1Ordem;

        return $this;
    }

    /**
     * Get conta1Ordem
     *
     * @return integer
     */
    public function getConta1Ordem()
    {
        return $this->conta1Ordem;
    }

    /**
     * Set conta1Nome
     *
     * @param string $conta1Nome
     *
     * @return B2cPagpag
     */
    public function setConta1Nome($conta1Nome)
    {
        $this->conta1Nome = $conta1Nome;

        return $this;
    }

    /**
     * Get conta1Nome
     *
     * @return string
     */
    public function getConta1Nome()
    {
        return $this->conta1Nome;
    }

    /**
     * Set conta1Cedente
     *
     * @param string $conta1Cedente
     *
     * @return B2cPagpag
     */
    public function setConta1Cedente($conta1Cedente)
    {
        $this->conta1Cedente = $conta1Cedente;

        return $this;
    }

    /**
     * Get conta1Cedente
     *
     * @return string
     */
    public function getConta1Cedente()
    {
        return $this->conta1Cedente;
    }

    /**
     * Set conta1Agencia
     *
     * @param string $conta1Agencia
     *
     * @return B2cPagpag
     */
    public function setConta1Agencia($conta1Agencia)
    {
        $this->conta1Agencia = $conta1Agencia;

        return $this;
    }

    /**
     * Get conta1Agencia
     *
     * @return string
     */
    public function getConta1Agencia()
    {
        return $this->conta1Agencia;
    }

    /**
     * Set conta1Numero
     *
     * @param string $conta1Numero
     *
     * @return B2cPagpag
     */
    public function setConta1Numero($conta1Numero)
    {
        $this->conta1Numero = $conta1Numero;

        return $this;
    }

    /**
     * Get conta1Numero
     *
     * @return string
     */
    public function getConta1Numero()
    {
        return $this->conta1Numero;
    }

    /**
     * Set conta1Codigo
     *
     * @param string $conta1Codigo
     *
     * @return B2cPagpag
     */
    public function setConta1Codigo($conta1Codigo)
    {
        $this->conta1Codigo = $conta1Codigo;

        return $this;
    }

    /**
     * Get conta1Codigo
     *
     * @return string
     */
    public function getConta1Codigo()
    {
        return $this->conta1Codigo;
    }

    /**
     * Set conta1Maximo
     *
     * @param float $conta1Maximo
     *
     * @return B2cPagpag
     */
    public function setConta1Maximo($conta1Maximo)
    {
        $this->conta1Maximo = $conta1Maximo;

        return $this;
    }

    /**
     * Get conta1Maximo
     *
     * @return float
     */
    public function getConta1Maximo()
    {
        return $this->conta1Maximo;
    }

    /**
     * Set conta2Ordem
     *
     * @param integer $conta2Ordem
     *
     * @return B2cPagpag
     */
    public function setConta2Ordem($conta2Ordem)
    {
        $this->conta2Ordem = $conta2Ordem;

        return $this;
    }

    /**
     * Get conta2Ordem
     *
     * @return integer
     */
    public function getConta2Ordem()
    {
        return $this->conta2Ordem;
    }

    /**
     * Set conta2Nome
     *
     * @param string $conta2Nome
     *
     * @return B2cPagpag
     */
    public function setConta2Nome($conta2Nome)
    {
        $this->conta2Nome = $conta2Nome;

        return $this;
    }

    /**
     * Get conta2Nome
     *
     * @return string
     */
    public function getConta2Nome()
    {
        return $this->conta2Nome;
    }

    /**
     * Set conta2Cedente
     *
     * @param string $conta2Cedente
     *
     * @return B2cPagpag
     */
    public function setConta2Cedente($conta2Cedente)
    {
        $this->conta2Cedente = $conta2Cedente;

        return $this;
    }

    /**
     * Get conta2Cedente
     *
     * @return string
     */
    public function getConta2Cedente()
    {
        return $this->conta2Cedente;
    }

    /**
     * Set conta2Agencia
     *
     * @param string $conta2Agencia
     *
     * @return B2cPagpag
     */
    public function setConta2Agencia($conta2Agencia)
    {
        $this->conta2Agencia = $conta2Agencia;

        return $this;
    }

    /**
     * Get conta2Agencia
     *
     * @return string
     */
    public function getConta2Agencia()
    {
        return $this->conta2Agencia;
    }

    /**
     * Set conta2Numero
     *
     * @param string $conta2Numero
     *
     * @return B2cPagpag
     */
    public function setConta2Numero($conta2Numero)
    {
        $this->conta2Numero = $conta2Numero;

        return $this;
    }

    /**
     * Get conta2Numero
     *
     * @return string
     */
    public function getConta2Numero()
    {
        return $this->conta2Numero;
    }

    /**
     * Set conta2Codigo
     *
     * @param string $conta2Codigo
     *
     * @return B2cPagpag
     */
    public function setConta2Codigo($conta2Codigo)
    {
        $this->conta2Codigo = $conta2Codigo;

        return $this;
    }

    /**
     * Get conta2Codigo
     *
     * @return string
     */
    public function getConta2Codigo()
    {
        return $this->conta2Codigo;
    }

    /**
     * Set conta2Maximo
     *
     * @param float $conta2Maximo
     *
     * @return B2cPagpag
     */
    public function setConta2Maximo($conta2Maximo)
    {
        $this->conta2Maximo = $conta2Maximo;

        return $this;
    }

    /**
     * Get conta2Maximo
     *
     * @return float
     */
    public function getConta2Maximo()
    {
        return $this->conta2Maximo;
    }

    /**
     * Set conta3Ordem
     *
     * @param integer $conta3Ordem
     *
     * @return B2cPagpag
     */
    public function setConta3Ordem($conta3Ordem)
    {
        $this->conta3Ordem = $conta3Ordem;

        return $this;
    }

    /**
     * Get conta3Ordem
     *
     * @return integer
     */
    public function getConta3Ordem()
    {
        return $this->conta3Ordem;
    }

    /**
     * Set conta3Nome
     *
     * @param string $conta3Nome
     *
     * @return B2cPagpag
     */
    public function setConta3Nome($conta3Nome)
    {
        $this->conta3Nome = $conta3Nome;

        return $this;
    }

    /**
     * Get conta3Nome
     *
     * @return string
     */
    public function getConta3Nome()
    {
        return $this->conta3Nome;
    }

    /**
     * Set conta3Cedente
     *
     * @param string $conta3Cedente
     *
     * @return B2cPagpag
     */
    public function setConta3Cedente($conta3Cedente)
    {
        $this->conta3Cedente = $conta3Cedente;

        return $this;
    }

    /**
     * Get conta3Cedente
     *
     * @return string
     */
    public function getConta3Cedente()
    {
        return $this->conta3Cedente;
    }

    /**
     * Set conta3Agencia
     *
     * @param string $conta3Agencia
     *
     * @return B2cPagpag
     */
    public function setConta3Agencia($conta3Agencia)
    {
        $this->conta3Agencia = $conta3Agencia;

        return $this;
    }

    /**
     * Get conta3Agencia
     *
     * @return string
     */
    public function getConta3Agencia()
    {
        return $this->conta3Agencia;
    }

    /**
     * Set conta3Numero
     *
     * @param string $conta3Numero
     *
     * @return B2cPagpag
     */
    public function setConta3Numero($conta3Numero)
    {
        $this->conta3Numero = $conta3Numero;

        return $this;
    }

    /**
     * Get conta3Numero
     *
     * @return string
     */
    public function getConta3Numero()
    {
        return $this->conta3Numero;
    }

    /**
     * Set conta3Codigo
     *
     * @param string $conta3Codigo
     *
     * @return B2cPagpag
     */
    public function setConta3Codigo($conta3Codigo)
    {
        $this->conta3Codigo = $conta3Codigo;

        return $this;
    }

    /**
     * Get conta3Codigo
     *
     * @return string
     */
    public function getConta3Codigo()
    {
        return $this->conta3Codigo;
    }

    /**
     * Set conta3Maximo
     *
     * @param float $conta3Maximo
     *
     * @return B2cPagpag
     */
    public function setConta3Maximo($conta3Maximo)
    {
        $this->conta3Maximo = $conta3Maximo;

        return $this;
    }

    /**
     * Get conta3Maximo
     *
     * @return float
     */
    public function getConta3Maximo()
    {
        return $this->conta3Maximo;
    }

    /**
     * Set conta4Ordem
     *
     * @param integer $conta4Ordem
     *
     * @return B2cPagpag
     */
    public function setConta4Ordem($conta4Ordem)
    {
        $this->conta4Ordem = $conta4Ordem;

        return $this;
    }

    /**
     * Get conta4Ordem
     *
     * @return integer
     */
    public function getConta4Ordem()
    {
        return $this->conta4Ordem;
    }

    /**
     * Set conta4Nome
     *
     * @param string $conta4Nome
     *
     * @return B2cPagpag
     */
    public function setConta4Nome($conta4Nome)
    {
        $this->conta4Nome = $conta4Nome;

        return $this;
    }

    /**
     * Get conta4Nome
     *
     * @return string
     */
    public function getConta4Nome()
    {
        return $this->conta4Nome;
    }

    /**
     * Set conta4Cedente
     *
     * @param string $conta4Cedente
     *
     * @return B2cPagpag
     */
    public function setConta4Cedente($conta4Cedente)
    {
        $this->conta4Cedente = $conta4Cedente;

        return $this;
    }

    /**
     * Get conta4Cedente
     *
     * @return string
     */
    public function getConta4Cedente()
    {
        return $this->conta4Cedente;
    }

    /**
     * Set conta4Agencia
     *
     * @param string $conta4Agencia
     *
     * @return B2cPagpag
     */
    public function setConta4Agencia($conta4Agencia)
    {
        $this->conta4Agencia = $conta4Agencia;

        return $this;
    }

    /**
     * Get conta4Agencia
     *
     * @return string
     */
    public function getConta4Agencia()
    {
        return $this->conta4Agencia;
    }

    /**
     * Set conta4Numero
     *
     * @param string $conta4Numero
     *
     * @return B2cPagpag
     */
    public function setConta4Numero($conta4Numero)
    {
        $this->conta4Numero = $conta4Numero;

        return $this;
    }

    /**
     * Get conta4Numero
     *
     * @return string
     */
    public function getConta4Numero()
    {
        return $this->conta4Numero;
    }

    /**
     * Set conta4Codigo
     *
     * @param string $conta4Codigo
     *
     * @return B2cPagpag
     */
    public function setConta4Codigo($conta4Codigo)
    {
        $this->conta4Codigo = $conta4Codigo;

        return $this;
    }

    /**
     * Get conta4Codigo
     *
     * @return string
     */
    public function getConta4Codigo()
    {
        return $this->conta4Codigo;
    }

    /**
     * Set conta4Maximo
     *
     * @param float $conta4Maximo
     *
     * @return B2cPagpag
     */
    public function setConta4Maximo($conta4Maximo)
    {
        $this->conta4Maximo = $conta4Maximo;

        return $this;
    }

    /**
     * Get conta4Maximo
     *
     * @return float
     */
    public function getConta4Maximo()
    {
        return $this->conta4Maximo;
    }

    /**
     * Set composto
     *
     * @param boolean $composto
     *
     * @return B2cPagpag
     */
    public function setComposto($composto)
    {
        $this->composto = $composto;

        return $this;
    }

    /**
     * Get composto
     *
     * @return boolean
     */
    public function getComposto()
    {
        return $this->composto;
    }

    /**
     * Set idPagboleto
     *
     * @param integer $idPagboleto
     *
     * @return B2cPagpag
     */
    public function setIdPagboleto($idPagboleto)
    {
        $this->idPagboleto = $idPagboleto;

        return $this;
    }

    /**
     * Get idPagboleto
     *
     * @return integer
     */
    public function getIdPagboleto()
    {
        return $this->idPagboleto;
    }

    /**
     * Set idPaypal
     *
     * @param string $idPaypal
     *
     * @return B2cPagpag
     */
    public function setIdPaypal($idPaypal)
    {
        $this->idPaypal = $idPaypal;

        return $this;
    }

    /**
     * Get idPaypal
     *
     * @return string
     */
    public function getIdPaypal()
    {
        return $this->idPaypal;
    }

    /**
     * Set senhaPaypal
     *
     * @param string $senhaPaypal
     *
     * @return B2cPagpag
     */
    public function setSenhaPaypal($senhaPaypal)
    {
        $this->senhaPaypal = $senhaPaypal;

        return $this;
    }

    /**
     * Get senhaPaypal
     *
     * @return string
     */
    public function getSenhaPaypal()
    {
        return $this->senhaPaypal;
    }

    /**
     * Set assinaturaPaypal
     *
     * @param string $assinaturaPaypal
     *
     * @return B2cPagpag
     */
    public function setAssinaturaPaypal($assinaturaPaypal)
    {
        $this->assinaturaPaypal = $assinaturaPaypal;

        return $this;
    }

    /**
     * Get assinaturaPaypal
     *
     * @return string
     */
    public function getAssinaturaPaypal()
    {
        return $this->assinaturaPaypal;
    }
}
