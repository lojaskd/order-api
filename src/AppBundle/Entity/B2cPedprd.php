<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPedprd
 *
 * @ORM\Table(name="b2c_pedprd", uniqueConstraints={@ORM\UniqueConstraint(name="pedprdstatus_uniq", columns={"id_pedped", "id_prdprd"})}, indexes={@ORM\Index(name="prd_vnd_aberto", columns={"prd_vnd_aberto"}), @ORM\Index(name="prd_vnd_junta", columns={"prd_vnd_junta"}), @ORM\Index(name="id_prdprd", columns={"id_prdprd"}), @ORM\Index(name="prd_imglistagem", columns={"prd_imglistagem"}), @ORM\Index(name="id_pedped", columns={"id_pedped"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPedprdRepository")
 */
class B2cPedprd
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idPrdprd;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdNome;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_qtd", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $prdQtd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="prd_data", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdData;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_referencia", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdReferencia;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_categoria", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdCategoria;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_fornecedor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdFornecedor;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_forn_nome", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdFornNome;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_prazo_forn", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdPrazoForn;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_prazo_disp", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdPrazoDisp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="prd_data_entrega_fornecedor", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdDataEntregaFornecedor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prd_destaque", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdDestaque;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_peso", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdPeso;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_estilo", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdEstilo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prd_presente", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdPresente;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_stq_loja", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdStqLoja;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_stq_fornecedor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdStqFornecedor;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_stq_fornecedor_reservado", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdStqFornecedorReservado;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_stq_marketplace", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdStqMarketplace;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_embalagem", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdEmbalagem;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_texto_cartao", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdTextoCartao;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_aro", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdAro;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_imglistagem", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdImglistagem;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_promo_desc", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdPromoDesc;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bpromocao", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $bpromocao;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_desc_perc", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdDescPerc;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_cubagem", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdCubagem;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_cor", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdCor;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_sexo", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdSexo;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_tamanho", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdTamanho;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_pvreal", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdPvreal;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_pvpromocao", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdPvpromocao;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_vale_compra", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdValeCompra;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_vnd_info", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdVndInfo;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_desc_valor", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdDescValor;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_pvtotal", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdPvtotal;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_valor_real_pago", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdValorRealPago;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_frete_real_pago", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdFreteRealPago;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="prd_promo_fim", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdPromoFim;

    /**
     * @var float
     *
     * @ORM\Column(name="prd_custo", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $prdCusto;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_vnd_aberto", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdVndAberto;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_vnd_junta", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdVndJunta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="prd_promo_ini", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdPromoIni;

    /**
     * @var string
     *
     * @ORM\Column(name="sf_opportunitylineitem", type="string", length=18, precision=0, scale=0, nullable=true, unique=false)
     */
    private $sfOpportunitylineitem;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="prd_data_entrega_fornecedor_realizada", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdDataEntregaFornecedorRealizada;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_notafiscal", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idNotafiscal;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_ocorrencia", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idTipoOcorrencia;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_ocorrencia", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nomeOcorrencia;

    /**
     * @var string
     *
     * @ORM\Column(name="grupo_ocorrencia", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $grupoOcorrencia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ocorrencia", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataOcorrencia;

    /**
     * @var boolean
     *
     * @ORM\Column(name="item_cancelado", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $itemCancelado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cancelamento_salesforce", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataCancelamentoSalesforce;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem_compra", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ordemCompra;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_prev_entrega_forn", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataPrevEntregaForn;

    /**
     * @var string
     *
     * @ORM\Column(name="cluster_rmkt", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $clusterRmkt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ordem_compra", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataOrdemCompra;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_filial_estoque", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idFilialEstoque;

    /**
     * @var \AppBundle\Entity\B2cPedped
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\B2cPedped", inversedBy="produtos")
     * @ORM\JoinColumn(name="id_pedped", referencedColumnName="id_pedped")
     */
    private $idPedped;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cancelamento", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataCancelamento;

    /**
     * @param integer $codigoTransportadorColeta
     *
     * @ORM\Column(name="cod_transportador_coleta", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $codigoTransportadorColeta;


    /**
     * @param string $destination
     *
     * @ORM\Column(name="destino", type="string", nullable=true, unique=false)
     */
    private $destination;

    /**
    * @param string $tipoVendaProduto
    *
    * @ORM\Column(name="tipo_venda_produto", type="string", nullable=true, unique=false)
    */
    private $tipoVendaProduto;

    /**
     * @param string $pathXmlOc
     *
     * @ORM\Column(name="path_xml_oc", type="string", nullable=true, unique=false)
     */
    private $pathXmlOc;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPrdprd
     *
     * @param integer $idPrdprd
     *
     * @return B2cPedprd
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;

        return $this;
    }

    /**
     * Get idPrdprd
     *
     * @return integer
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * Set prdNome
     *
     * @param string $prdNome
     *
     * @return B2cPedprd
     */
    public function setPrdNome($prdNome)
    {
        $this->prdNome = $prdNome;

        return $this;
    }

    /**
     * Get prdNome
     *
     * @return string
     */
    public function getPrdNome()
    {
        return $this->prdNome;
    }

    /**
     * Set prdQtd
     *
     * @param string $prdQtd
     *
     * @return B2cPedprd
     */
    public function setPrdQtd($prdQtd)
    {
        $this->prdQtd = $prdQtd;

        return $this;
    }

    /**
     * Get prdQtd
     *
     * @return string
     */
    public function getPrdQtd()
    {
        return $this->prdQtd;
    }

    /**
     * Set prdData
     *
     * @param \DateTime $prdData
     *
     * @return B2cPedprd
     */
    public function setPrdData($prdData)
    {
        $this->prdData = $prdData;

        return $this;
    }

    /**
     * Get prdData
     *
     * @return \DateTime
     */
    public function getPrdData()
    {
        return $this->prdData;
    }

    /**
     * Set prdReferencia
     *
     * @param string $prdReferencia
     *
     * @return B2cPedprd
     */
    public function setPrdReferencia($prdReferencia)
    {
        $this->prdReferencia = $prdReferencia;

        return $this;
    }

    /**
     * Get prdReferencia
     *
     * @return string
     */
    public function getPrdReferencia()
    {
        return $this->prdReferencia;
    }

    /**
     * Set prdCategoria
     *
     * @param integer $prdCategoria
     *
     * @return B2cPedprd
     */
    public function setPrdCategoria($prdCategoria)
    {
        $this->prdCategoria = $prdCategoria;

        return $this;
    }

    /**
     * Get prdCategoria
     *
     * @return integer
     */
    public function getPrdCategoria()
    {
        return $this->prdCategoria;
    }

    /**
     * Set prdFornecedor
     *
     * @param integer $prdFornecedor
     *
     * @return B2cPedprd
     */
    public function setPrdFornecedor($prdFornecedor)
    {
        $this->prdFornecedor = $prdFornecedor;

        return $this;
    }

    /**
     * Get prdFornecedor
     *
     * @return integer
     */
    public function getPrdFornecedor()
    {
        return $this->prdFornecedor;
    }

    /**
     * Set prdFornNome
     *
     * @param string $prdFornNome
     *
     * @return B2cPedprd
     */
    public function setPrdFornNome($prdFornNome)
    {
        $this->prdFornNome = $prdFornNome;

        return $this;
    }

    /**
     * Get prdFornNome
     *
     * @return string
     */
    public function getPrdFornNome()
    {
        return $this->prdFornNome;
    }

    /**
     * Set prdPrazoForn
     *
     * @param integer $prdPrazoForn
     *
     * @return B2cPedprd
     */
    public function setPrdPrazoForn($prdPrazoForn)
    {
        $this->prdPrazoForn = $prdPrazoForn;

        return $this;
    }

    /**
     * Get prdPrazoForn
     *
     * @return integer
     */
    public function getPrdPrazoForn()
    {
        return $this->prdPrazoForn;
    }

    /**
     * Set prdPrazoDisp
     *
     * @param integer $prdPrazoDisp
     *
     * @return B2cPedprd
     */
    public function setPrdPrazoDisp($prdPrazoDisp)
    {
        $this->prdPrazoDisp = $prdPrazoDisp;

        return $this;
    }

    /**
     * Get prdPrazoDisp
     *
     * @return integer
     */
    public function getPrdPrazoDisp()
    {
        return $this->prdPrazoDisp;
    }

    /**
     * Set prdDataEntregaFornecedor
     *
     * @param \DateTime $prdDataEntregaFornecedor
     *
     * @return B2cPedprd
     */
    public function setPrdDataEntregaFornecedor($prdDataEntregaFornecedor)
    {
        $this->prdDataEntregaFornecedor = $prdDataEntregaFornecedor;

        return $this;
    }

    /**
     * Get prdDataEntregaFornecedor
     *
     * @return \DateTime
     */
    public function getPrdDataEntregaFornecedor()
    {
        return $this->prdDataEntregaFornecedor;
    }

    /**
     * Set prdDestaque
     *
     * @param boolean $prdDestaque
     *
     * @return B2cPedprd
     */
    public function setPrdDestaque($prdDestaque)
    {
        $this->prdDestaque = $prdDestaque;

        return $this;
    }

    /**
     * Get prdDestaque
     *
     * @return boolean
     */
    public function getPrdDestaque()
    {
        return $this->prdDestaque;
    }

    /**
     * Set prdPeso
     *
     * @param float $prdPeso
     *
     * @return B2cPedprd
     */
    public function setPrdPeso($prdPeso)
    {
        $this->prdPeso = $prdPeso;

        return $this;
    }

    /**
     * Get prdPeso
     *
     * @return float
     */
    public function getPrdPeso()
    {
        return $this->prdPeso;
    }

    /**
     * Set prdEstilo
     *
     * @param string $prdEstilo
     *
     * @return B2cPedprd
     */
    public function setPrdEstilo($prdEstilo)
    {
        $this->prdEstilo = $prdEstilo;

        return $this;
    }

    /**
     * Get prdEstilo
     *
     * @return string
     */
    public function getPrdEstilo()
    {
        return $this->prdEstilo;
    }

    /**
     * Set prdPresente
     *
     * @param boolean $prdPresente
     *
     * @return B2cPedprd
     */
    public function setPrdPresente($prdPresente)
    {
        $this->prdPresente = $prdPresente;

        return $this;
    }

    /**
     * Get prdPresente
     *
     * @return boolean
     */
    public function getPrdPresente()
    {
        return $this->prdPresente;
    }

    /**
     * Set prdStqLoja
     *
     * @param integer $prdStqLoja
     *
     * @return B2cPedprd
     */
    public function setPrdStqLoja($prdStqLoja)
    {
        $this->prdStqLoja = $prdStqLoja;

        return $this;
    }

    /**
     * Get prdStqLoja
     *
     * @return integer
     */
    public function getPrdStqLoja()
    {
        return $this->prdStqLoja;
    }

    /**
     * Set prdStqFornecedor
     *
     * @param integer $prdStqFornecedor
     *
     * @return B2cPedprd
     */
    public function setPrdStqFornecedor($prdStqFornecedor)
    {
        $this->prdStqFornecedor = $prdStqFornecedor;

        return $this;
    }

    /**
     * Get prdStqFornecedor
     *
     * @return integer
     */
    public function getPrdStqFornecedor()
    {
        return $this->prdStqFornecedor;
    }

    /**
     * Set prdStqFornecedorReservado
     *
     * @param integer $prdStqFornecedorReservado
     *
     * @return B2cPedprd
     */
    public function setPrdStqFornecedorReservado($prdStqFornecedorReservado)
    {
        $this->prdStqFornecedorReservado = $prdStqFornecedorReservado;

        return $this;
    }

    /**
     * Get prdStqFornecedor
     *
     * @return integer
     */
    public function getPrdStqFornecedorReservado()
    {
        return $this->prdStqFornecedorReservado;
    }

    /**
     * @return int
     */
    public function getPrdStqMarketplace()
    {
        return $this->prdStqMarketplace;
    }

    /**
     * @param int $prdStqMarketplace
     */
    public function setPrdStqMarketplace($prdStqMarketplace)
    {
        $this->prdStqMarketplace = $prdStqMarketplace;
    }

    /**
     * Set prdEmbalagem
     *
     * @param integer $prdEmbalagem
     *
     * @return B2cPedprd
     */
    public function setPrdEmbalagem($prdEmbalagem)
    {
        $this->prdEmbalagem = $prdEmbalagem;

        return $this;
    }

    /**
     * Get prdEmbalagem
     *
     * @return integer
     */
    public function getPrdEmbalagem()
    {
        return $this->prdEmbalagem;
    }

    /**
     * Set prdTextoCartao
     *
     * @param string $prdTextoCartao
     *
     * @return B2cPedprd
     */
    public function setPrdTextoCartao($prdTextoCartao)
    {
        $this->prdTextoCartao = $prdTextoCartao;

        return $this;
    }

    /**
     * Get prdTextoCartao
     *
     * @return string
     */
    public function getPrdTextoCartao()
    {
        return $this->prdTextoCartao;
    }

    /**
     * Set prdAro
     *
     * @param string $prdAro
     *
     * @return B2cPedprd
     */
    public function setPrdAro($prdAro)
    {
        $this->prdAro = $prdAro;

        return $this;
    }

    /**
     * Get prdAro
     *
     * @return string
     */
    public function getPrdAro()
    {
        return $this->prdAro;
    }

    /**
     * Set prdImglistagem
     *
     * @param string $prdImglistagem
     *
     * @return B2cPedprd
     */
    public function setPrdImglistagem($prdImglistagem)
    {
        $this->prdImglistagem = $prdImglistagem;

        return $this;
    }

    /**
     * Get prdImglistagem
     *
     * @return string
     */
    public function getPrdImglistagem()
    {
        return $this->prdImglistagem;
    }

    /**
     * Set prdPromoDesc
     *
     * @param string $prdPromoDesc
     *
     * @return B2cPedprd
     */
    public function setPrdPromoDesc($prdPromoDesc)
    {
        $this->prdPromoDesc = $prdPromoDesc;

        return $this;
    }

    /**
     * Get prdPromoDesc
     *
     * @return string
     */
    public function getPrdPromoDesc()
    {
        return $this->prdPromoDesc;
    }

    /**
     * Set bpromocao
     *
     * @param boolean $bpromocao
     *
     * @return B2cPedprd
     */
    public function setBpromocao($bpromocao)
    {
        $this->bpromocao = $bpromocao;

        return $this;
    }

    /**
     * Get bpromocao
     *
     * @return boolean
     */
    public function getBpromocao()
    {
        return $this->bpromocao;
    }

    /**
     * Set prdDescPerc
     *
     * @param float $prdDescPerc
     *
     * @return B2cPedprd
     */
    public function setPrdDescPerc($prdDescPerc)
    {
        $this->prdDescPerc = $prdDescPerc;

        return $this;
    }

    /**
     * Get prdDescPerc
     *
     * @return float
     */
    public function getPrdDescPerc()
    {
        return $this->prdDescPerc;
    }

    /**
     * Set prdCubagem
     *
     * @param float $prdCubagem
     *
     * @return B2cPedprd
     */
    public function setPrdCubagem($prdCubagem)
    {
        $this->prdCubagem = $prdCubagem;

        return $this;
    }

    /**
     * Get prdCubagem
     *
     * @return float
     */
    public function getPrdCubagem()
    {
        return $this->prdCubagem;
    }

    /**
     * Set prdCor
     *
     * @param string $prdCor
     *
     * @return B2cPedprd
     */
    public function setPrdCor($prdCor)
    {
        $this->prdCor = $prdCor;

        return $this;
    }

    /**
     * Get prdCor
     *
     * @return string
     */
    public function getPrdCor()
    {
        return $this->prdCor;
    }

    /**
     * Set prdSexo
     *
     * @param string $prdSexo
     *
     * @return B2cPedprd
     */
    public function setPrdSexo($prdSexo)
    {
        $this->prdSexo = $prdSexo;

        return $this;
    }

    /**
     * Get prdSexo
     *
     * @return string
     */
    public function getPrdSexo()
    {
        return $this->prdSexo;
    }

    /**
     * Set prdTamanho
     *
     * @param string $prdTamanho
     *
     * @return B2cPedprd
     */
    public function setPrdTamanho($prdTamanho)
    {
        $this->prdTamanho = $prdTamanho;

        return $this;
    }

    /**
     * Get prdTamanho
     *
     * @return string
     */
    public function getPrdTamanho()
    {
        return $this->prdTamanho;
    }

    /**
     * Set prdPvreal
     *
     * @param float $prdPvreal
     *
     * @return B2cPedprd
     */
    public function setPrdPvreal($prdPvreal)
    {
        $this->prdPvreal = $prdPvreal;

        return $this;
    }

    /**
     * Get prdPvreal
     *
     * @return float
     */
    public function getPrdPvreal()
    {
        return $this->prdPvreal;
    }

    /**
     * Set prdPvpromocao
     *
     * @param float $prdPvpromocao
     *
     * @return B2cPedprd
     */
    public function setPrdPvpromocao($prdPvpromocao)
    {
        $this->prdPvpromocao = $prdPvpromocao;

        return $this;
    }

    /**
     * Get prdPvpromocao
     *
     * @return float
     */
    public function getPrdPvpromocao()
    {
        return $this->prdPvpromocao;
    }

    /**
     * Set prdValeCompra
     *
     * @param float $prdValeCompra
     *
     * @return B2cPedprd
     */
    public function setPrdValeCompra($prdValeCompra)
    {
        $this->prdValeCompra = $prdValeCompra;

        return $this;
    }

    /**
     * Get prdValeCompra
     *
     * @return float
     */
    public function getPrdValeCompra()
    {
        return $this->prdValeCompra;
    }

    /**
     * Set prdVndInfo
     *
     * @param string $prdVndInfo
     *
     * @return B2cPedprd
     */
    public function setPrdVndInfo($prdVndInfo)
    {
        $this->prdVndInfo = $prdVndInfo;

        return $this;
    }

    /**
     * Get prdVndInfo
     *
     * @return string
     */
    public function getPrdVndInfo()
    {
        return $this->prdVndInfo;
    }

    /**
     * Set prdDescValor
     *
     * @param float $prdDescValor
     *
     * @return B2cPedprd
     */
    public function setPrdDescValor($prdDescValor)
    {
        $this->prdDescValor = $prdDescValor;

        return $this;
    }

    /**
     * Get prdDescValor
     *
     * @return float
     */
    public function getPrdDescValor()
    {
        return $this->prdDescValor;
    }

    /**
     * Set prdPvtotal
     *
     * @param float $prdPvtotal
     *
     * @return B2cPedprd
     */
    public function setPrdPvtotal($prdPvtotal)
    {
        $this->prdPvtotal = $prdPvtotal;

        return $this;
    }

    /**
     * Get prdPvtotal
     *
     * @return float
     */
    public function getPrdPvtotal()
    {
        return $this->prdPvtotal;
    }

    /**
     * @return float
     */
    public function getPrdValorRealPago()
    {
        return $this->prdValorRealPago;
    }

    /**
     * @param float $prdValorRealPago
     */
    public function setPrdValorRealPago($prdValorRealPago)
    {
        $this->prdValorRealPago = $prdValorRealPago;
    }

    /**
     * @return float
     */
    public function getPrdFreteRealPago()
    {
        return $this->prdFreteRealPago;
    }

    /**
     * @param float $prdFreteRealPago
     */
    public function setPrdFreteRealPago($prdFreteRealPago)
    {
        $this->prdFreteRealPago = $prdFreteRealPago;
    }

    /**
     * Set prdPromoFim
     *
     * @param \DateTime $prdPromoFim
     *
     * @return B2cPedprd
     */
    public function setPrdPromoFim($prdPromoFim)
    {
        $this->prdPromoFim = $prdPromoFim;

        return $this;
    }

    /**
     * Get prdPromoFim
     *
     * @return \DateTime
     */
    public function getPrdPromoFim()
    {
        return $this->prdPromoFim;
    }

    /**
     * Set prdCusto
     *
     * @param float $prdCusto
     *
     * @return B2cPedprd
     */
    public function setPrdCusto($prdCusto)
    {
        $this->prdCusto = $prdCusto;

        return $this;
    }

    /**
     * Get prdCusto
     *
     * @return float
     */
    public function getPrdCusto()
    {
        return $this->prdCusto;
    }

    /**
     * Set prdVndAberto
     *
     * @param integer $prdVndAberto
     *
     * @return B2cPedprd
     */
    public function setPrdVndAberto($prdVndAberto)
    {
        $this->prdVndAberto = $prdVndAberto;

        return $this;
    }

    /**
     * Get prdVndAberto
     *
     * @return integer
     */
    public function getPrdVndAberto()
    {
        return $this->prdVndAberto;
    }

    /**
     * Set prdVndJunta
     *
     * @param integer $prdVndJunta
     *
     * @return B2cPedprd
     */
    public function setPrdVndJunta($prdVndJunta)
    {
        $this->prdVndJunta = $prdVndJunta;

        return $this;
    }

    /**
     * Get prdVndJunta
     *
     * @return integer
     */
    public function getPrdVndJunta()
    {
        return $this->prdVndJunta;
    }

    /**
     * Set prdPromoIni
     *
     * @param \DateTime $prdPromoIni
     *
     * @return B2cPedprd
     */
    public function setPrdPromoIni($prdPromoIni)
    {
        $this->prdPromoIni = $prdPromoIni;

        return $this;
    }

    /**
     * Get prdPromoIni
     *
     * @return \DateTime
     */
    public function getPrdPromoIni()
    {
        return $this->prdPromoIni;
    }

    /**
     * Set sfOpportunitylineitem
     *
     * @param string $sfOpportunitylineitem
     *
     * @return B2cPedprd
     */
    public function setSfOpportunitylineitem($sfOpportunitylineitem)
    {
        $this->sfOpportunitylineitem = $sfOpportunitylineitem;

        return $this;
    }

    /**
     * Get sfOpportunitylineitem
     *
     * @return string
     */
    public function getSfOpportunitylineitem()
    {
        return $this->sfOpportunitylineitem;
    }

    /**
     * Set prdDataEntregaFornecedorRealizada
     *
     * @param \DateTime $prdDataEntregaFornecedorRealizada
     *
     * @return B2cPedprd
     */
    public function setPrdDataEntregaFornecedorRealizada($prdDataEntregaFornecedorRealizada)
    {
        $this->prdDataEntregaFornecedorRealizada = $prdDataEntregaFornecedorRealizada;

        return $this;
    }

    /**
     * Get prdDataEntregaFornecedorRealizada
     *
     * @return \DateTime
     */
    public function getPrdDataEntregaFornecedorRealizada()
    {
        return $this->prdDataEntregaFornecedorRealizada;
    }

    /**
     * Set idNotafiscal
     *
     * @param integer $idNotafiscal
     *
     * @return B2cPedprd
     */
    public function setIdNotafiscal($idNotafiscal)
    {
        $this->idNotafiscal = $idNotafiscal;

        return $this;
    }

    /**
     * Get idNotafiscal
     *
     * @return integer
     */
    public function getIdNotafiscal()
    {
        return $this->idNotafiscal;
    }

    /**
     * Set idTipoOcorrencia
     *
     * @param integer $idTipoOcorrencia
     *
     * @return B2cPedprd
     */
    public function setIdTipoOcorrencia($idTipoOcorrencia)
    {
        $this->idTipoOcorrencia = $idTipoOcorrencia;

        return $this;
    }

    /**
     * Get idTipoOcorrencia
     *
     * @return integer
     */
    public function getIdTipoOcorrencia()
    {
        return $this->idTipoOcorrencia;
    }

    /**
     * Set nomeOcorrencia
     *
     * @param string $nomeOcorrencia
     *
     * @return B2cPedprd
     */
    public function setNomeOcorrencia($nomeOcorrencia)
    {
        $this->nomeOcorrencia = $nomeOcorrencia;

        return $this;
    }

    /**
     * Get nomeOcorrencia
     *
     * @return string
     */
    public function getNomeOcorrencia()
    {
        return $this->nomeOcorrencia;
    }

    /**
     * Set grupoOcorrencia
     *
     * @param string $grupoOcorrencia
     *
     * @return B2cPedprd
     */
    public function setGrupoOcorrencia($grupoOcorrencia)
    {
        $this->grupoOcorrencia = $grupoOcorrencia;

        return $this;
    }

    /**
     * Get grupoOcorrencia
     *
     * @return string
     */
    public function getGrupoOcorrencia()
    {
        return $this->grupoOcorrencia;
    }

    /**
     * Set dataOcorrencia
     *
     * @param \DateTime $dataOcorrencia
     *
     * @return B2cPedprd
     */
    public function setDataOcorrencia($dataOcorrencia)
    {
        $this->dataOcorrencia = $dataOcorrencia;

        return $this;
    }

    /**
     * Get dataOcorrencia
     *
     * @return \DateTime
     */
    public function getDataOcorrencia()
    {
        return $this->dataOcorrencia;
    }

    /**
     * Set itemCancelado
     *
     * @param boolean $itemCancelado
     *
     * @return B2cPedprd
     */
    public function setItemCancelado($itemCancelado)
    {
        $this->itemCancelado = $itemCancelado;

        return $this;
    }

    /**
     * Get itemCancelado
     *
     * @return boolean
     */
    public function getItemCancelado()
    {
        return $this->itemCancelado;
    }

    /**
     * Set dataCancelamentoSalesforce
     *
     * @param \DateTime $dataCancelamentoSalesforce
     *
     * @return B2cPedprd
     */
    public function setDataCancelamentoSalesforce($dataCancelamentoSalesforce)
    {
        $this->dataCancelamentoSalesforce = $dataCancelamentoSalesforce;

        return $this;
    }

    /**
     * Get dataCancelamentoSalesforce
     *
     * @return \DateTime
     */
    public function getDataCancelamentoSalesforce()
    {
        return $this->dataCancelamentoSalesforce;
    }

    /**
     * Set ordemCompra
     *
     * @param integer $ordemCompra
     *
     * @return B2cPedprd
     */
    public function setOrdemCompra($ordemCompra)
    {
        $this->ordemCompra = $ordemCompra;

        return $this;
    }

    /**
     * Get ordemCompra
     *
     * @return integer
     */
    public function getOrdemCompra()
    {
        return $this->ordemCompra;
    }

    /**
     * Set dataPrevEntregaForn
     *
     * @param \DateTime $dataPrevEntregaForn
     *
     * @return B2cPedprd
     */
    public function setDataPrevEntregaForn($dataPrevEntregaForn)
    {
        $this->dataPrevEntregaForn = $dataPrevEntregaForn;

        return $this;
    }

    /**
     * Get dataPrevEntregaForn
     *
     * @return \DateTime
     */
    public function getDataPrevEntregaForn()
    {
        return $this->dataPrevEntregaForn;
    }

    /**
     * Set clusterRmkt
     *
     * @param string $clusterRmkt
     *
     * @return B2cPedprd
     */
    public function setClusterRmkt($clusterRmkt)
    {
        $this->clusterRmkt = $clusterRmkt;

        return $this;
    }

    /**
     * Get clusterRmkt
     *
     * @return string
     */
    public function getClusterRmkt()
    {
        return $this->clusterRmkt;
    }

    /**
     * Set dataOrdemCompra
     *
     * @param \DateTime $dataOrdemCompra
     *
     * @return B2cPedprd
     */
    public function setDataOrdemCompra($dataOrdemCompra)
    {
        $this->dataOrdemCompra = $dataOrdemCompra;

        return $this;
    }

    /**
     * Get dataOrdemCompra
     *
     * @return \DateTime
     */
    public function getDataOrdemCompra()
    {
        return $this->dataOrdemCompra;
    }

    /**
     * Set idFilialEstoque
     *
     * @param integer $idFilialEstoque
     *
     * @return B2cPedprd
     */
    public function setIdFilialEstoque($idFilialEstoque)
    {
        $this->idFilialEstoque = $idFilialEstoque;

        return $this;
    }

    /**
     * Get idFilialEstoque
     *
     * @return integer
     */
    public function getIdFilialEstoque()
    {
        return $this->idFilialEstoque;
    }

    /**
     * Set idPedped
     *
     * @param \AppBundle\Entity\B2cPedped $idPedped
     *
     * @return B2cPedprd
     */
    public function setIdPedped(\AppBundle\Entity\B2cPedped $idPedped = null)
    {
        $this->idPedped = $idPedped;

        return $this;
    }

    /**
     * Get idPedped
     *
     * @return \AppBundle\Entity\B2cPedped
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * @return \DateTime
     */
    public function getDataCancelamento()
    {
        return $this->dataCancelamento;
    }

    /**
     * @param \DateTime $dataCancelamento
     */
    public function setDataCancelamento($dataCancelamento)
    {
        $this->dataCancelamento = $dataCancelamento;
    }

    /**
     * @return integer
     */
    public function getCodigoTransportadorColeta()
    {
        return $this->codigoTransportadorColeta;
    }

    /**
     * @param integer $codigoTransportadorColeta
     */
    public function setCodigoTransportadorColeta($codigoTransportadorColeta)
    {
        $this->codigoTransportadorColeta = $codigoTransportadorColeta;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param $destination
     * @return $this
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoVendaProduto()
    {
        return $this->tipoVendaProduto;
    }

    /**
     * @param string $tipoVendaProduto
     */
    public function setTipoVendaProduto($tipoVendaProduto)
    {
        $this->tipoVendaProduto = $tipoVendaProduto;
    }

    /**
     * @return mixed
     */
    public function getPathXmlOc()
    {
        return $this->pathXmlOc;
    }

    /**
     * @param mixed $pathXmlOc
     */
    public function setPathXmlOc($pathXmlOc)
    {
        $this->pathXmlOc = $pathXmlOc;
    }
}
