<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPedstatus
 *
 * @ORM\Table(name="b2c_pedstatus", indexes={@ORM\Index(name="idx_dta_situacao", columns={"ped_dta_situacao"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPedstatusRepository")
 */
class B2cPedstatus
{
    const CREATED = 1;
    const UNDER_APPROVAL = 2;
    const APPROVED = 3;
    const RELEASED = 4;
    const SHIPPED = 5;
    const DELIVERED = 6;
    const CANCELLED = 7;
    const SHIPPED_CORREIOS = 9;
    const DELIVERED_CORREIOS = 10;
    const STATUS_DATE_CONFIG = array(
        'approvalDate' => array(
            'all' => array(
                'status' => self::APPROVED,
                'internal' => 0
            )
        ),
        'releaseDate' => array(
            'all' => array(
                'status' => self::RELEASED,
                'internal' => 0
            )
        ),
        'supplierBillingDate' => array(
            'all' => array(
                'status' => self::RELEASED,
                'internal' => 159
            )
        ),
        'supplierInvoiceDate' => array(
            'all' => array(
                'status' => self::RELEASED,
                'internal' => 86
            )
        ),
        'supplierDeliveryDate' => array(
            'all' => array(
                'status' => self::RELEASED,
                'internal' => 158
            )
        ),
        'invoiceDate' => array(
            'all' => array(
                'status' => self::RELEASED,
                'internal' => 72
            )
        ),
        'shippingDate' => array(
            'all' => array(
                'status' => self::SHIPPED,
                'internal' => 0
            ),
            'partial' => array(
                'status' => self::SHIPPED,
                'internal' => 77
            ),
        ),
        'deliveryDate' => array(
            'all' => array(
                'status' => self::DELIVERED,
                'internal' => 0
            ),
            'all_with_cancel' => array(
                'status' => self::DELIVERED,
                'internal' => 227
            ),
            'partial' => array(
                'status' => self::DELIVERED,
                'internal' => 78
            ),
        )
    );

    /**
     * @var \AppBundle\Entity\B2cPedped
     *
     * @ORM\Id
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\B2cPedped", inversedBy="status")
     * @ORM\JoinColumn(name="id_pedped", referencedColumnName="id_pedped")
     */
    private $idPedped;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_situacao", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $pedSituacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_staint", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $pedStaint;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ped_dta_situacao", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedDtaSituacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ped_dta_situacao_prev", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pedDtaSituacaoPrev;


    /**
     * @return B2cPedped
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * @param B2cPedped $idPedped
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;
    }

    /**
     * Set pedSituacao
     *
     * @param boolean $pedSituacao
     *
     * @return B2cPedstatus
     */
    public function setPedSituacao($pedSituacao)
    {
        $this->pedSituacao = $pedSituacao;

        return $this;
    }

    /**
     * Get pedSituacao
     *
     * @return boolean
     */
    public function getPedSituacao()
    {
        return $this->pedSituacao;
    }

    /**
     * Set pedStaint
     *
     * @param integer $pedStaint
     *
     * @return B2cPedstatus
     */
    public function setPedStaint($pedStaint)
    {
        $this->pedStaint = $pedStaint;

        return $this;
    }

    /**
     * Get pedStaint
     *
     * @return integer
     */
    public function getPedStaint()
    {
        return $this->pedStaint;
    }

    /**
     * Set pedDtaSituacao
     *
     * @param \DateTime $pedDtaSituacao
     *
     * @return B2cPedstatus
     */
    public function setPedDtaSituacao($pedDtaSituacao)
    {
        $this->pedDtaSituacao = $pedDtaSituacao;

        return $this;
    }

    /**
     * Get pedDtaSituacao
     *
     * @return \DateTime
     */
    public function getPedDtaSituacao()
    {
        return $this->pedDtaSituacao;
    }

    /**
     * Set pedDtaSituacaoPrev
     *
     * @param \DateTime $pedDtaSituacaoPrev
     *
     * @return B2cPedstatus
     */
    public function setPedDtaSituacaoPrev($pedDtaSituacaoPrev)
    {
        $this->pedDtaSituacaoPrev = $pedDtaSituacaoPrev;

        return $this;
    }

    /**
     * Get pedDtaSituacaoPrev
     *
     * @return \DateTime
     */
    public function getPedDtaSituacaoPrev()
    {
        return $this->pedDtaSituacaoPrev;
    }
}
