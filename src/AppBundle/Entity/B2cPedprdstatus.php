<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPedprdstatus
 *
 * @ORM\Table(name="b2c_pedprdstatus", uniqueConstraints={@ORM\UniqueConstraint(name="pedprd_uniq", columns={"id_pedped", "id_prdprd"})}, indexes={@ORM\Index(name="idx_referencia", columns={"id_pedped", "prd_referencia"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPedprdstatusRepository")
 */
class B2cPedprdstatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPedped;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPrdprd;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_referencia", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $prdReferencia;

    /**
     * @var string
     *
     * @ORM\Column(name="status_msg", type="string", length=200, precision=0, scale=0, nullable=true, unique=false)
     */
    private $statusMsg;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="status_data", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $statusData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_apr", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataApr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_apr_r", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataAprR;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_lib", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataLib;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_lib_r", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataLibR;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_fat_forn", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataFatForn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_fat_forn_r", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataFatFornR;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_entd_forn", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEntdForn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_entd_forn_r", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEntdFornR;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ent_forn", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEntForn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ent_forn_r", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEntFornR;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_nf", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataNf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_nf_r", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataNfR;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_emb", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEmb;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_emb_r", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEmbR;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cte", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataCte;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cte_r", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataCteR;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ent", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEnt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_ent_r", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataEntR;

    /**
     * @var boolean
     *
     * @ORM\Column(name="data_emb_r_notificado", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dataEmbRNotificado;

    /**
     * @var boolean
     *
     * @ORM\Column(name="data_ent_r_notificado", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dataEntRNotificado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_prev_entrega_transp", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataPrevEntregaTransp;


    /**
     * Set idPedped
     *
     * @param integer $idPedped
     *
     * @return B2cPedprdstatus
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;

        return $this;
    }

    /**
     * Get idPedped
     *
     * @return integer
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * Set idPrdprd
     *
     * @param integer $idPrdprd
     *
     * @return B2cPedprdstatus
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;

        return $this;
    }

    /**
     * Get idPrdprd
     *
     * @return integer
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * Set prdReferencia
     *
     * @param string $prdReferencia
     *
     * @return B2cPedprdstatus
     */
    public function setPrdReferencia($prdReferencia)
    {
        $this->prdReferencia = $prdReferencia;

        return $this;
    }

    /**
     * Get prdReferencia
     *
     * @return string
     */
    public function getPrdReferencia()
    {
        return $this->prdReferencia;
    }

    /**
     * Set statusMsg
     *
     * @param string $statusMsg
     *
     * @return B2cPedprdstatus
     */
    public function setStatusMsg($statusMsg)
    {
        $this->statusMsg = $statusMsg;

        return $this;
    }

    /**
     * Get statusMsg
     *
     * @return string
     */
    public function getStatusMsg()
    {
        return $this->statusMsg;
    }

    /**
     * Set statusData
     *
     * @param \DateTime $statusData
     *
     * @return B2cPedprdstatus
     */
    public function setStatusData($statusData)
    {
        $this->statusData = $statusData;

        return $this;
    }

    /**
     * Get statusData
     *
     * @return \DateTime
     */
    public function getStatusData()
    {
        return $this->statusData;
    }

    /**
     * Set dataApr
     *
     * @param \DateTime $dataApr
     *
     * @return B2cPedprdstatus
     */
    public function setDataApr($dataApr)
    {
        $this->dataApr = $dataApr;

        return $this;
    }

    /**
     * Get dataApr
     *
     * @return \DateTime
     */
    public function getDataApr()
    {
        return $this->dataApr;
    }

    /**
     * Set dataAprR
     *
     * @param \DateTime $dataAprR
     *
     * @return B2cPedprdstatus
     */
    public function setDataAprR($dataAprR)
    {
        $this->dataAprR = $dataAprR;

        return $this;
    }

    /**
     * Get dataAprR
     *
     * @return \DateTime
     */
    public function getDataAprR()
    {
        return $this->dataAprR;
    }

    /**
     * Set dataLib
     *
     * @param \DateTime $dataLib
     *
     * @return B2cPedprdstatus
     */
    public function setDataLib($dataLib)
    {
        $this->dataLib = $dataLib;

        return $this;
    }

    /**
     * Get dataLib
     *
     * @return \DateTime
     */
    public function getDataLib()
    {
        return $this->dataLib;
    }

    /**
     * Set dataLibR
     *
     * @param \DateTime $dataLibR
     *
     * @return B2cPedprdstatus
     */
    public function setDataLibR($dataLibR)
    {
        $this->dataLibR = $dataLibR;

        return $this;
    }

    /**
     * Get dataLibR
     *
     * @return \DateTime
     */
    public function getDataLibR()
    {
        return $this->dataLibR;
    }

    /**
     * Set dataFatForn
     *
     * @param \DateTime $dataFatForn
     *
     * @return B2cPedprdstatus
     */
    public function setDataFatForn($dataFatForn)
    {
        $this->dataFatForn = $dataFatForn;

        return $this;
    }

    /**
     * Get dataFatForn
     *
     * @return \DateTime
     */
    public function getDataFatForn()
    {
        return $this->dataFatForn;
    }

    /**
     * Set dataFatFornR
     *
     * @param \DateTime $dataFatFornR
     *
     * @return B2cPedprdstatus
     */
    public function setDataFatFornR($dataFatFornR)
    {
        $this->dataFatFornR = $dataFatFornR;

        return $this;
    }

    /**
     * Get dataFatFornR
     *
     * @return \DateTime
     */
    public function getDataFatFornR()
    {
        return $this->dataFatFornR;
    }

    /**
     * Set dataEntdForn
     *
     * @param \DateTime $dataEntdForn
     *
     * @return B2cPedprdstatus
     */
    public function setDataEntdForn($dataEntdForn)
    {
        $this->dataEntdForn = $dataEntdForn;

        return $this;
    }

    /**
     * Get dataEntdForn
     *
     * @return \DateTime
     */
    public function getDataEntdForn()
    {
        return $this->dataEntdForn;
    }

    /**
     * Set dataEntdFornR
     *
     * @param \DateTime $dataEntdFornR
     *
     * @return B2cPedprdstatus
     */
    public function setDataEntdFornR($dataEntdFornR)
    {
        $this->dataEntdFornR = $dataEntdFornR;

        return $this;
    }

    /**
     * Get dataEntdFornR
     *
     * @return \DateTime
     */
    public function getDataEntdFornR()
    {
        return $this->dataEntdFornR;
    }

    /**
     * Set dataEntForn
     *
     * @param \DateTime $dataEntForn
     *
     * @return B2cPedprdstatus
     */
    public function setDataEntForn($dataEntForn)
    {
        $this->dataEntForn = $dataEntForn;

        return $this;
    }

    /**
     * Get dataEntForn
     *
     * @return \DateTime
     */
    public function getDataEntForn()
    {
        return $this->dataEntForn;
    }

    /**
     * Set dataEntFornR
     *
     * @param \DateTime $dataEntFornR
     *
     * @return B2cPedprdstatus
     */
    public function setDataEntFornR($dataEntFornR)
    {
        $this->dataEntFornR = $dataEntFornR;

        return $this;
    }

    /**
     * Get dataEntFornR
     *
     * @return \DateTime
     */
    public function getDataEntFornR()
    {
        return $this->dataEntFornR;
    }

    /**
     * Set dataNf
     *
     * @param \DateTime $dataNf
     *
     * @return B2cPedprdstatus
     */
    public function setDataNf($dataNf)
    {
        $this->dataNf = $dataNf;

        return $this;
    }

    /**
     * Get dataNf
     *
     * @return \DateTime
     */
    public function getDataNf()
    {
        return $this->dataNf;
    }

    /**
     * Set dataNfR
     *
     * @param \DateTime $dataNfR
     *
     * @return B2cPedprdstatus
     */
    public function setDataNfR($dataNfR)
    {
        $this->dataNfR = $dataNfR;

        return $this;
    }

    /**
     * Get dataNfR
     *
     * @return \DateTime
     */
    public function getDataNfR()
    {
        return $this->dataNfR;
    }

    /**
     * Set dataEmb
     *
     * @param \DateTime $dataEmb
     *
     * @return B2cPedprdstatus
     */
    public function setDataEmb($dataEmb)
    {
        $this->dataEmb = $dataEmb;

        return $this;
    }

    /**
     * Get dataEmb
     *
     * @return \DateTime
     */
    public function getDataEmb()
    {
        return $this->dataEmb;
    }

    /**
     * Set dataEmbR
     *
     * @param \DateTime $dataEmbR
     *
     * @return B2cPedprdstatus
     */
    public function setDataEmbR($dataEmbR)
    {
        $this->dataEmbR = $dataEmbR;

        return $this;
    }

    /**
     * Get dataEmbR
     *
     * @return \DateTime
     */
    public function getDataEmbR()
    {
        return $this->dataEmbR;
    }

    /**
     * Set dataCte
     *
     * @param \DateTime $dataCte
     *
     * @return B2cPedprdstatus
     */
    public function setDataCte($dataCte)
    {
        $this->dataCte = $dataCte;

        return $this;
    }

    /**
     * Get dataCte
     *
     * @return \DateTime
     */
    public function getDataCte()
    {
        return $this->dataCte;
    }

    /**
     * Set dataCteR
     *
     * @param \DateTime $dataCteR
     *
     * @return B2cPedprdstatus
     */
    public function setDataCteR($dataCteR)
    {
        $this->dataCteR = $dataCteR;

        return $this;
    }

    /**
     * Get dataCteR
     *
     * @return \DateTime
     */
    public function getDataCteR()
    {
        return $this->dataCteR;
    }

    /**
     * Set dataEnt
     *
     * @param \DateTime $dataEnt
     *
     * @return B2cPedprdstatus
     */
    public function setDataEnt($dataEnt)
    {
        $this->dataEnt = $dataEnt;

        return $this;
    }

    /**
     * Get dataEnt
     *
     * @return \DateTime
     */
    public function getDataEnt()
    {
        return $this->dataEnt;
    }

    /**
     * Set dataEntR
     *
     * @param \DateTime $dataEntR
     *
     * @return B2cPedprdstatus
     */
    public function setDataEntR($dataEntR)
    {
        $this->dataEntR = $dataEntR;

        return $this;
    }

    /**
     * Get dataEntR
     *
     * @return \DateTime
     */
    public function getDataEntR()
    {
        return $this->dataEntR;
    }

    /**
     * Set dataEmbRNotificado
     *
     * @param boolean $dataEmbRNotificado
     *
     * @return B2cPedprdstatus
     */
    public function setDataEmbRNotificado($dataEmbRNotificado)
    {
        $this->dataEmbRNotificado = $dataEmbRNotificado;

        return $this;
    }

    /**
     * Get dataEmbRNotificado
     *
     * @return boolean
     */
    public function getDataEmbRNotificado()
    {
        return $this->dataEmbRNotificado;
    }

    /**
     * Set dataEntRNotificado
     *
     * @param boolean $dataEntRNotificado
     *
     * @return B2cPedprdstatus
     */
    public function setDataEntRNotificado($dataEntRNotificado)
    {
        $this->dataEntRNotificado = $dataEntRNotificado;

        return $this;
    }

    /**
     * Get dataEntRNotificado
     *
     * @return boolean
     */
    public function getDataEntRNotificado()
    {
        return $this->dataEntRNotificado;
    }

    /**
     * Set dataPrevEntregaTransp
     *
     * @param \DateTime $dataPrevEntregaTransp
     *
     * @return B2cPedprdstatus
     */
    public function setDataPrevEntregaTransp($dataPrevEntregaTransp)
    {
        $this->dataPrevEntregaTransp = $dataPrevEntregaTransp;

        return $this;
    }

    /**
     * Get dataPrevEntregaTransp
     *
     * @return \DateTime
     */
    public function getDataPrevEntregaTransp()
    {
        return $this->dataPrevEntregaTransp;
    }

}
