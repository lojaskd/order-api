<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cCliend
 *
 * @ORM\Table(name="b2c_cliend", uniqueConstraints={@ORM\UniqueConstraint(name="id_usu", columns={"id_clicli", "identificacao"})})
 * @ORM\Entity
 */
class B2cCliend
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cliend", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCliend;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_clicli", type="integer", nullable=true)
     */
    private $idClicli = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="identificacao", type="string", length=255, nullable=true)
     */
    private $identificacao;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=200, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="sobrenome", type="string", length=200, nullable=true)
     */
    private $sobrenome;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco", type="string", length=200, nullable=true)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=10, nullable=true)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="complemento", type="string", length=200, nullable=true)
     */
    private $complemento;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro", type="string", length=200, nullable=true)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=200, nullable=true)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=2, nullable=true)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="provincia", type="string", length=255, nullable=true)
     */
    private $provincia;

    /**
     * @var integer
     *
     * @ORM\Column(name="pais", type="integer", nullable=true)
     */
    private $pais = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=true)
     */
    private $cep = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="telefone_a", type="string", length=16, nullable=true)
     */
    private $telefoneA;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone_b", type="string", length=16, nullable=true)
     */
    private $telefoneB;

    /**
     * @var string
     *
     * @ORM\Column(name="referencia", type="string", length=255, nullable=true)
     */
    private $referencia;

    /**
     * @param $idCliend
     * @return $this
     */
    public function setIdCliend($idCliend)
    {
        $this->idCliend = $idCliend;
        return $this;
    }

    /**
     * Get idCliend
     *
     * @return integer
     */
    public function getIdCliend()
    {
        return $this->idCliend;
    }

    /**
     * Set idClicli
     *
     * @param integer $idClicli
     *
     * @return B2cCliend
     */
    public function setIdClicli($idClicli)
    {
        $this->idClicli = $idClicli;

        return $this;
    }

    /**
     * Get idClicli
     *
     * @return integer
     */
    public function getIdClicli()
    {
        return $this->idClicli;
    }

    /**
     * Set identificacao
     *
     * @param string $identificacao
     *
     * @return B2cCliend
     */
    public function setIdentificacao($identificacao)
    {
        $this->identificacao = $identificacao;

        return $this;
    }

    /**
     * Get identificacao
     *
     * @return string
     */
    public function getIdentificacao()
    {
        return $this->identificacao;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cCliend
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set sobrenome
     *
     * @param string $sobrenome
     *
     * @return B2cCliend
     */
    public function setSobrenome($sobrenome)
    {
        $this->sobrenome = $sobrenome;

        return $this;
    }

    /**
     * Get sobrenome
     *
     * @return string
     */
    public function getSobrenome()
    {
        return $this->sobrenome;
    }

    /**
     * Set endereco
     *
     * @param string $endereco
     *
     * @return B2cCliend
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;

        return $this;
    }

    /**
     * Get endereco
     *
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return B2cCliend
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set complemento
     *
     * @param string $complemento
     *
     * @return B2cCliend
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;

        return $this;
    }

    /**
     * Get complemento
     *
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * Set bairro
     *
     * @param string $bairro
     *
     * @return B2cCliend
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get bairro
     *
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     *
     * @return B2cCliend
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return B2cCliend
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set provincia
     *
     * @param string $provincia
     *
     * @return B2cCliend
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return string
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set pais
     *
     * @param integer $pais
     *
     * @return B2cCliend
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return integer
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set cep
     *
     * @param string $cep
     *
     * @return B2cCliend
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set telefoneA
     *
     * @param string $telefoneA
     *
     * @return B2cCliend
     */
    public function setTelefoneA($telefoneA)
    {
        $this->telefoneA = $telefoneA;

        return $this;
    }

    /**
     * Get telefoneA
     *
     * @return string
     */
    public function getTelefoneA()
    {
        return $this->telefoneA;
    }

    /**
     * Set telefoneB
     *
     * @param string $telefoneB
     *
     * @return B2cCliend
     */
    public function setTelefoneB($telefoneB)
    {
        $this->telefoneB = $telefoneB;

        return $this;
    }

    /**
     * Get telefoneB
     *
     * @return string
     */
    public function getTelefoneB()
    {
        return $this->telefoneB;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     *
     * @return B2cCliend
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string
     */
    public function getReferencia()
    {
        return $this->referencia;
    }
}
