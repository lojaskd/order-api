<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPedprdEstorno
 *
 * @ORM\Table(name="b2c_pedprd_estorno")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPedprdEstornoRepository")
 */
class B2cPedprdEstorno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_estorno", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idPedped;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idPrdprd;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_forest", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idForest;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket", type="string", nullable=false, unique=false)
     */
    private $ticket;

    /**
     * @var string
     *
     * @ORM\Column(name="num_transacao", type="string", nullable=false, unique=false)
     */
    private $numTransacao;

    /**
     * @var string
     *
     * @ORM\Column(name="operadora", type="string", nullable=false, unique=false)
     */
    private $operadora;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_sugerido", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $valorSugerido;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cad", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtaCad;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_produto", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $valorProduto;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_frete", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $valorFrete;

    /**
     * @var integer
     *
     * @ORM\Column(name="estorno_liberado", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $estornoLiberado;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_ocr", type="string", nullable=true, unique=false)
     */
    private $ocrCode;

    /**
     * @var string
     *
     * @ORM\Column(name="indicador_cancelamento", type="string", nullable=true, unique=false)
     */
    private $cancellationIndicator;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket_tipo", type="string", nullable=true, unique=false)
     */
    private $ticketType;

    /**
     * @var string
     *
     * @ORM\Column(name="hub", type="string", nullable=true, unique=false)
     */
    private $hub;

    /**
     * @var string
     *
     * @ORM\Column(name="transportador", type="string", nullable=true, unique=false)
     */
    private $shipper;

    /**
     * @var string
     *
     * @ORM\Column(name="nota_fiscal", type="string", nullable=false, unique=false)
     */
    private $invoice;

    /**
     * @var string
     *
     * @ORM\Column(name="id_prdprd_causador", type="string", nullable=false, unique=false)
     */
    private $productCauser;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * @param int $idPedped
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;
    }

    /**
     * @return int
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * @param int $idPrdprd
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;
    }

    /**
     * @return int
     */
    public function getIdForest()
    {
        return $this->idForest;
    }

    /**
     * @param int $idForest
     */
    public function setIdForest($idForest)
    {
        $this->idForest = $idForest;
    }

    /**
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param string $ticket
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * @return string
     */
    public function getNumTransacao()
    {
        return $this->numTransacao;
    }

    /**
     * @param string $numTransacao
     */
    public function setNumTransacao($numTransacao)
    {
        $this->numTransacao = $numTransacao;
    }

    /**
     * @return string
     */
    public function getOperadora()
    {
        return $this->operadora;
    }

    /**
     * @param string $operadora
     */
    public function setOperadora($operadora)
    {
        $this->operadora = $operadora;
    }

    /**
     * @return float
     */
    public function getValorSugerido()
    {
        return $this->valorSugerido;
    }

    /**
     * @param float $valorSugerido
     */
    public function setValorSugerido($valorSugerido)
    {
        $this->valorSugerido = $valorSugerido;
    }

    /**
     * @return \DateTime
     */
    public function getDtaCad()
    {
        return $this->dtaCad;
    }

    /**
     * @param \DateTime $dtaCad
     */
    public function setDtaCad($dtaCad)
    {
        $this->dtaCad = $dtaCad;
    }

    /**
     * @return float
     */
    public function getValorProduto()
    {
        return $this->valorProduto;
    }

    /**
     * @param float $valorProduto
     */
    public function setValorProduto($valorProduto)
    {
        $this->valorProduto = $valorProduto;
    }

    /**
     * @return float
     */
    public function getValorFrete()
    {
        return $this->valorFrete;
    }

    /**
     * @param float $valorFrete
     */
    public function setValorFrete($valorFrete)
    {
        $this->valorFrete = $valorFrete;
    }

    /**
     * @return integer
     */
    public function getEstornoLiberado()
    {
        return $this->estornoLiberado;
    }

    /**
     * @param integer $estornoLiberado
     */
    public function setEstornoLiberado($estornoLiberado)
    {
        $this->estornoLiberado = $estornoLiberado;
    }

    /**
     * @return string
     */
    public function getOcrCode()
    {
        return $this->ocrCode;
    }

    /**
     * @param string $ocrCode
     */
    public function setOcrCode($ocrCode)
    {
        $this->ocrCode = $ocrCode;
    }

    /**
     * @return string
     */
    public function getCancellationIndicator()
    {
        return $this->cancellationIndicator;
    }

    /**
     * @param string $cancellationIndicator
     */
    public function setCancellationIndicator($cancellationIndicator)
    {
        $this->cancellationIndicator = $cancellationIndicator;
    }

    /**
     * @return string
     */
    public function getTicketType()
    {
        return $this->ticketType;
    }

    /**
     * @param string $ticketType
     */
    public function setTicketType($ticketType)
    {
        $this->ticketType = $ticketType;
    }

    /**
     * @return string
     */
    public function getHub()
    {
        return $this->hub;
    }

    /**
     * @param string $hub
     */
    public function setHub($hub)
    {
        $this->hub = $hub;
    }

    /**
     * @return string
     */
    public function getShipper()
    {
        return $this->shipper;
    }

    /**
     * @param string $shipper
     */
    public function setShipper($shipper)
    {
        $this->shipper = $shipper;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return string
     */
    public function getProductCauser()
    {
        return $this->productCauser;
    }

    /**
     * @param string $productCauser
     */
    public function setProductCauser($productCauser)
    {
        $this->productCauser = $productCauser;
    }
}
