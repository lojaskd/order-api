<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdestoque
 *
 * @ORM\Table(name="b2c_prdestoque")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPrdestoqueRepository")
 */
class B2cPrdestoque
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\B2cViewProdutos")
     * @ORM\JoinColumn(name="id_prdprd", referencedColumnName="id")
     */
    private $idPrdprd;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_filial", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idFilial;

    /**
     * @var integer
     *
     * @ORM\Column(name="qtd", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $qtd;



    /**
     * Set idPrdprd
     *
     * @param integer $idPrdprd
     *
     * @return B2cPrdestoque
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;

        return $this;
    }

    /**
     * Get idPrdprd
     *
     * @return integer
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * Set idFilial
     *
     * @param integer $idFilial
     *
     * @return B2cPrdestoque
     */
    public function setIdFilial($idFilial)
    {
        $this->idFilial = $idFilial;

        return $this;
    }

    /**
     * Get idFilial
     *
     * @return integer
     */
    public function getIdFilial()
    {
        return $this->idFilial;
    }

    /**
     * Set qtd
     *
     * @param integer $qtd
     *
     * @return B2cPrdestoque
     */
    public function setQtd($qtd)
    {
        $this->qtd = $qtd;

        return $this;
    }

    /**
     * Get qtd
     *
     * @return integer
     */
    public function getQtd()
    {
        return $this->qtd;
    }
}
