<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class B2cAtributoLista
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="b2c_atributo_lista", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug"})}, indexes={@ORM\Index(name="descricao", columns={"descricao"}), @ORM\Index(name="id_atributo", columns={"id_atributo"}), @ORM\Index(name="idx_atributo_descricao", columns={"id_atributo", "descricao"})})
 * @ORM\Entity
 */
class B2cAtributoLista
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_lista_atributo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idListaAtributo;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=255, nullable=false)
     */
    private $descricao;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_atributo", type="smallint", nullable=false)
     */
    private $idAtributo;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @param $idListaAtributo
     * @return $this
     */
    public function setIdListaAtributo($idListaAtributo)
    {
        $this->idListaAtributo = $idListaAtributo;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdListaAtributo()
    {
        return $this->idListaAtributo;
    }

    /**
     * @param $descricao
     * @return $this
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param $idAtributo
     * @return $this
     */
    public function setIdAtributo($idAtributo)
    {
        $this->idAtributo = $idAtributo;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAtributo()
    {
        return $this->idAtributo;
    }

    /**
     * @param $slug
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
