<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoSit
 *
 * @ORM\Table(name="b2c_promo_sit", indexes={@ORM\Index(name="destaque", columns={"destaque"}), @ORM\Index(name="lancamento", columns={"lancamento"}), @ORM\Index(name="maisvendido", columns={"maisvendido"}), @ORM\Index(name="prevenda", columns={"prevenda"}), @ORM\Index(name="presentes", columns={"presentes"})})
 * @ORM\Entity
 */
class B2cPromoSit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPromo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="destaque", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $destaque;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lancamento", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $lancamento;

    /**
     * @var boolean
     *
     * @ORM\Column(name="maisvendido", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $maisvendido;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prevenda", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prevenda;

    /**
     * @var boolean
     *
     * @ORM\Column(name="presentes", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $presentes;

    /**
     * @param $idPromo
     * @return $this
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;
        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set destaque
     *
     * @param boolean $destaque
     *
     * @return B2cPromoSit
     */
    public function setDestaque($destaque)
    {
        $this->destaque = $destaque;

        return $this;
    }

    /**
     * Get destaque
     *
     * @return boolean
     */
    public function getDestaque()
    {
        return $this->destaque;
    }

    /**
     * Set lancamento
     *
     * @param boolean $lancamento
     *
     * @return B2cPromoSit
     */
    public function setLancamento($lancamento)
    {
        $this->lancamento = $lancamento;

        return $this;
    }

    /**
     * Get lancamento
     *
     * @return boolean
     */
    public function getLancamento()
    {
        return $this->lancamento;
    }

    /**
     * Set maisvendido
     *
     * @param boolean $maisvendido
     *
     * @return B2cPromoSit
     */
    public function setMaisvendido($maisvendido)
    {
        $this->maisvendido = $maisvendido;

        return $this;
    }

    /**
     * Get maisvendido
     *
     * @return boolean
     */
    public function getMaisvendido()
    {
        return $this->maisvendido;
    }

    /**
     * Set prevenda
     *
     * @param boolean $prevenda
     *
     * @return B2cPromoSit
     */
    public function setPrevenda($prevenda)
    {
        $this->prevenda = $prevenda;

        return $this;
    }

    /**
     * Get prevenda
     *
     * @return boolean
     */
    public function getPrevenda()
    {
        return $this->prevenda;
    }

    /**
     * Set presentes
     *
     * @param boolean $presentes
     *
     * @return B2cPromoSit
     */
    public function setPresentes($presentes)
    {
        $this->presentes = $presentes;

        return $this;
    }

    /**
     * Get presentes
     *
     * @return boolean
     */
    public function getPresentes()
    {
        return $this->presentes;
    }
}
