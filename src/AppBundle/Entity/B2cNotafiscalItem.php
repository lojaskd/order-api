<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cNotafiscalItem
 *
 * @ORM\Table(name="b2c_notafiscal_item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cNotafiscalItemRepository")
 */
class B2cNotafiscalItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_filial", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idFilial;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_notafiscal", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idNotafiscal;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPedped;

    /**
     * @var string
     *
     * @ORM\Column(name="prd_referencia", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $prdReferencia;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_qtde", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $prdQtde;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_cadastro", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_devolucao", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dataDevolucao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="devolvido", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $devolvido;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_transportadora", type="string", length=150, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nomeTransportadora;

    /**
     * @var string
     *
     * @ORM\Column(name="documento_transportadora", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $documentoTransportadora;

    /**
     * @var string
     *
     * @ORM\Column(name="hub", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $hub;



    /**
     * Set idFilial
     *
     * @param integer $idFilial
     *
     * @return B2cNotafiscalItem
     */
    public function setIdFilial($idFilial)
    {
        $this->idFilial = $idFilial;

        return $this;
    }

    /**
     * Get idFilial
     *
     * @return integer
     */
    public function getIdFilial()
    {
        return $this->idFilial;
    }

    /**
     * Set idNotafiscal
     *
     * @param integer $idNotafiscal
     *
     * @return B2cNotafiscalItem
     */
    public function setIdNotafiscal($idNotafiscal)
    {
        $this->idNotafiscal = $idNotafiscal;

        return $this;
    }

    /**
     * Get idNotafiscal
     *
     * @return integer
     */
    public function getIdNotafiscal()
    {
        return $this->idNotafiscal;
    }

    /**
     * Set idPedped
     *
     * @param integer $idPedped
     *
     * @return B2cNotafiscalItem
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;

        return $this;
    }

    /**
     * Get idPedped
     *
     * @return integer
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * Set prdReferencia
     *
     * @param string $prdReferencia
     *
     * @return B2cNotafiscalItem
     */
    public function setPrdReferencia($prdReferencia)
    {
        $this->prdReferencia = $prdReferencia;

        return $this;
    }

    /**
     * Get prdReferencia
     *
     * @return string
     */
    public function getPrdReferencia()
    {
        return $this->prdReferencia;
    }

    /**
     * Set prdQtde
     *
     * @param integer $prdQtde
     *
     * @return B2cNotafiscalItem
     */
    public function setPrdQtde($prdQtde)
    {
        $this->prdQtde = $prdQtde;

        return $this;
    }

    /**
     * Get prdQtde
     *
     * @return integer
     */
    public function getPrdQtde()
    {
        return $this->prdQtde;
    }

    /**
     * Set dataCadastro
     *
     * @param \DateTime $dataCadastro
     *
     * @return B2cNotafiscalItem
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;

        return $this;
    }

    /**
     * Get dataCadastro
     *
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * Set dataDevolucao
     *
     * @param \DateTime $dataDevolucao
     *
     * @return B2cNotafiscalItem
     */
    public function setDataDevolucao($dataDevolucao)
    {
        $this->dataDevolucao = $dataDevolucao;

        return $this;
    }

    /**
     * Get dataDevolucao
     *
     * @return \DateTime
     */
    public function getDataDevolucao()
    {
        return $this->dataDevolucao;
    }

    /**
     * Set devolvido
     *
     * @param boolean $devolvido
     *
     * @return B2cNotafiscalItem
     */
    public function setDevolvido($devolvido)
    {
        $this->devolvido = $devolvido;

        return $this;
    }

    /**
     * Get devolvido
     *
     * @return boolean
     */
    public function getDevolvido()
    {
        return $this->devolvido;
    }

    /**
     * Set nomeTransportadora
     *
     * @param string $nomeTransportadora
     *
     * @return B2cNotafiscalItem
     */
    public function setNomeTransportadora($nomeTransportadora)
    {
        $this->nomeTransportadora = $nomeTransportadora;

        return $this;
    }

    /**
     * Get nomeTransportadora
     *
     * @return string
     */
    public function getNomeTransportadora()
    {
        return $this->nomeTransportadora;
    }

    /**
     * Set documentoTransportadora
     *
     * @param string $documentoTransportadora
     *
     * @return B2cNotafiscalItem
     */
    public function setDocumentoTransportadora($documentoTransportadora)
    {
        $this->documentoTransportadora = $documentoTransportadora;

        return $this;
    }

    /**
     * Get documentoTransportadora
     *
     * @return string
     */
    public function getDocumentoTransportadora()
    {
        return $this->documentoTransportadora;
    }

    /**
     * Set hub
     *
     * @param string $hub
     *
     * @return B2cNotafiscalItem
     */
    public function setHub($hub)
    {
        $this->hub = $hub;

        return $this;
    }

    /**
     * Get hub
     *
     * @return string
     */
    public function getHub()
    {
        return $this->hub;
    }
}