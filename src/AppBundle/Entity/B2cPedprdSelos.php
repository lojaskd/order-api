<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPedprdSelos
 *
 * @ORM\Table(name="b2c_pedprd_selos")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPedprdSelosRepository")
 */
class B2cPedprdSelos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPedped;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPrdprd;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_selo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idSelo;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=45, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nome;

    /**
     * @return int
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * @param int $idPedped
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;
    }

    /**
     * @return int
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * @param int $idPrdprd
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;
    }

    /**
     * @return int
     */
    public function getIdSelo()
    {
        return $this->idSelo;
    }

    /**
     * @param int $idSelo
     */
    public function setIdSelo($idSelo)
    {
        $this->idSelo = $idSelo;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }
}
