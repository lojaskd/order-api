<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdselcat
 *
 * @ORM\Table(name="b2c_prdselcat", indexes={@ORM\Index(name="idx_prdselcat", columns={"id_cat", "id_prd"}), @ORM\Index(name="id_prd_cat", columns={"id_prd", "id_cat"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPrdselcatRepository")
 */
class B2cPrdselcat
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id_prd", type="integer", nullable=false)
     */
    private $idPrd = '0';

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id_cat", type="integer", nullable=false)
     */
    private $idCat = '0';

    /**
     * @return int
     */
    public function getIdPrd()
    {
        return $this->idPrd;
    }

    /**
     * @param int $idPrd
     */
    public function setIdPrd($idPrd)
    {
        $this->idPrd = $idPrd;
    }

    /**
     * @return int
     */
    public function getIdCat()
    {
        return $this->idCat;
    }

    /**
     * @param int $idCat
     */
    public function setIdCat($idCat)
    {
        $this->idCat = $idCat;
    }

}
