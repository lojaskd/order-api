<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cFilial
 *
 * @ORM\Table(name="b2c_filial")
 * @ORM\Entity
 */
class B2cFilial
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_filial", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idFilial;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="cnpj", type="string", length=45, precision=0, scale=0, nullable=false, unique=false)
     */
    private $cnpj;

    /**
     * @var boolean
     *
     * @ORM\Column(name="matriz", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $matriz;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ativo;

    /**
     * @var integer
     *
     * @ORM\Column(name="logistica_prazo_compra", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $logisticaPrazoCompra;

    /**
     * @var integer
     *
     * @ORM\Column(name="logistica_prazo_recebimento", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $logisticaPrazoRecebimento;

    /**
     * @var integer
     *
     * @ORM\Column(name="logistica_prazo_romaneio", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $logisticaPrazoRomaneio;

    /**
     * @var integer
     *
     * @ORM\Column(name="logistica_prazo_separacao", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $logisticaPrazoSeparacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="cep", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $cep;

    /**
     * @var integer
     *
     * @ORM\Column(name="prioridade", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prioridade;

    /**
     * @param $idFilial
     * @return $this
     */
    public function setIdFilial($idFilial)
    {
        $this->idFilial = $idFilial;
        return $this;
    }

    /**
     * Get idFilial
     *
     * @return integer
     */
    public function getIdFilial()
    {
        return $this->idFilial;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cFilial
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set cnpj
     *
     * @param string $cnpj
     *
     * @return B2cFilial
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Get cnpj
     *
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Set matriz
     *
     * @param boolean $matriz
     *
     * @return B2cFilial
     */
    public function setMatriz($matriz)
    {
        $this->matriz = $matriz;

        return $this;
    }

    /**
     * Get matriz
     *
     * @return boolean
     */
    public function getMatriz()
    {
        return $this->matriz;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return B2cFilial
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set logisticaPrazoCompra
     *
     * @param integer $logisticaPrazoCompra
     *
     * @return B2cFilial
     */
    public function setLogisticaPrazoCompra($logisticaPrazoCompra)
    {
        $this->logisticaPrazoCompra = $logisticaPrazoCompra;

        return $this;
    }

    /**
     * Get logisticaPrazoCompra
     *
     * @return integer
     */
    public function getLogisticaPrazoCompra()
    {
        return $this->logisticaPrazoCompra;
    }

    /**
     * Set logisticaPrazoRecebimento
     *
     * @param integer $logisticaPrazoRecebimento
     *
     * @return B2cFilial
     */
    public function setLogisticaPrazoRecebimento($logisticaPrazoRecebimento)
    {
        $this->logisticaPrazoRecebimento = $logisticaPrazoRecebimento;

        return $this;
    }

    /**
     * Get logisticaPrazoRecebimento
     *
     * @return integer
     */
    public function getLogisticaPrazoRecebimento()
    {
        return $this->logisticaPrazoRecebimento;
    }

    /**
     * Set logisticaPrazoRomaneio
     *
     * @param integer $logisticaPrazoRomaneio
     *
     * @return B2cFilial
     */
    public function setLogisticaPrazoRomaneio($logisticaPrazoRomaneio)
    {
        $this->logisticaPrazoRomaneio = $logisticaPrazoRomaneio;

        return $this;
    }

    /**
     * Get logisticaPrazoRomaneio
     *
     * @return integer
     */
    public function getLogisticaPrazoRomaneio()
    {
        return $this->logisticaPrazoRomaneio;
    }

    /**
     * Set logisticaPrazoSeparacao
     *
     * @param integer $logisticaPrazoSeparacao
     *
     * @return B2cFilial
     */
    public function setLogisticaPrazoSeparacao($logisticaPrazoSeparacao)
    {
        $this->logisticaPrazoSeparacao = $logisticaPrazoSeparacao;

        return $this;
    }

    /**
     * Get logisticaPrazoSeparacao
     *
     * @return integer
     */
    public function getLogisticaPrazoSeparacao()
    {
        return $this->logisticaPrazoSeparacao;
    }

    /**
     * Set cep
     *
     * @param integer $cep
     *
     * @return B2cFilial
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return integer
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set prioridade
     *
     * @param integer $prioridade
     *
     * @return B2cFilial
     */
    public function setPrioridade($prioridade)
    {
        $this->prioridade = $prioridade;

        return $this;
    }

    /**
     * Get prioridade
     *
     * @return integer
     */
    public function getPrioridade()
    {
        return $this->prioridade;
    }
}
