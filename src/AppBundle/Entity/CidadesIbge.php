<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CidadesIbge
 *
 * @ORM\Table(name="cidades_ibge", indexes={@ORM\Index(name="id_regest", columns={"id_regest"})})
 * @ORM\Entity
 */
class CidadesIbge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $cidade;

    /**
     * @var integer
     *
     * @ORM\Column(name="cep", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $cep;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_regest", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idRegest;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_ibge", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $codIbge;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_regreg", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idRegreg;

    /**
     * @var string
     *
     * @ORM\Column(name="regiao", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $regiao;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return CidadesIbge
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     *
     * @return CidadesIbge
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set cep
     *
     * @param integer $cep
     *
     * @return CidadesIbge
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return integer
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set idRegest
     *
     * @param integer $idRegest
     *
     * @return CidadesIbge
     */
    public function setIdRegest($idRegest)
    {
        $this->idRegest = $idRegest;

        return $this;
    }

    /**
     * Get idRegest
     *
     * @return integer
     */
    public function getIdRegest()
    {
        return $this->idRegest;
    }

    /**
     * Set codIbge
     *
     * @param integer $codIbge
     *
     * @return CidadesIbge
     */
    public function setCodIbge($codIbge)
    {
        $this->codIbge = $codIbge;

        return $this;
    }

    /**
     * Get codIbge
     *
     * @return integer
     */
    public function getCodIbge()
    {
        return $this->codIbge;
    }

    /**
     * Set idRegreg
     *
     * @param integer $idRegreg
     *
     * @return CidadesIbge
     */
    public function setIdRegreg($idRegreg)
    {
        $this->idRegreg = $idRegreg;

        return $this;
    }

    /**
     * Get idRegreg
     *
     * @return integer
     */
    public function getIdRegreg()
    {
        return $this->idRegreg;
    }

    /**
     * Set regiao
     *
     * @param string $regiao
     *
     * @return CidadesIbge
     */
    public function setRegiao($regiao)
    {
        $this->regiao = $regiao;

        return $this;
    }

    /**
     * Get regiao
     *
     * @return string
     */
    public function getRegiao()
    {
        return $this->regiao;
    }
}
