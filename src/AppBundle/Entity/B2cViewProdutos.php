<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cViewProdutos
 *
 * @ORM\Table(name="b2c_view_produtos", indexes={@ORM\Index(name="matriz", columns={"matriz"}), @ORM\Index(name="estoque", columns={"estoque"}), @ORM\Index(name="disponivel", columns={"disponivel"}), @ORM\Index(name="n_mostrar", columns={"n_mostrar"}), @ORM\Index(name="stqmin_acao", columns={"stqmin_acao"}), @ORM\Index(name="produto_kit", columns={"produto_kit"}), @ORM\Index(name="categoria", columns={"categoria"}), @ORM\Index(name="mostra", columns={"mostra"}), @ORM\Index(name="ordem_prd", columns={"ordem_prd"}), @ORM\Index(name="usar_brinde", columns={"usar_brinde"}), @ORM\Index(name="lancamento", columns={"blancamento", "disponivel", "mostra", "n_mostrar"}), @ORM\Index(name="destaque", columns={"destaque", "disponivel", "mostra", "n_mostrar"}), @ORM\Index(name="bpromocao", columns={"bpromocao", "disponivel", "mostra", "n_mostrar"}), @ORM\Index(name="fornecedor", columns={"fornecedor", "disponivel", "mostra", "n_mostrar"}), @ORM\Index(name="padrao", columns={"mostra", "disponivel", "n_mostrar", "cat_visivel", "shopping"}), @ORM\Index(name="slug_tamanho", columns={"slug_tamanho"}), @ORM\Index(name="slug_cor", columns={"slug_cor"}), @ORM\Index(name="slug_estilo", columns={"slug_estilo"}), @ORM\Index(name="slug_fornecedor", columns={"slug_fornecedor"}), @ORM\Index(name="presente", columns={"presente"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cViewProdutosRepository")
 */
class B2cViewProdutos
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="matriz", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $matriz;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_inclusao", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtaInclusao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_alteracao", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtaAlteracao;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_alteracao", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $usuarioAlteracao;

    /**
     * @var integer
     *
     * @ORM\Column(name="produto_kit", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $produtoKit;

    /**
     * @var string
     *
     * @ORM\Column(name="lista_prd_kit", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $listaPrdKit;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_kit", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $descKit;

    /**
     * @var string
     *
     * @ORM\Column(name="link_produto", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $linkProduto;

    /**
     * @var string
     *
     * @ORM\Column(name="link_produto_secao", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $linkProdutoSecao;

    /**
     * @var string
     *
     * @ORM\Column(name="link_produto_query", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $linkProdutoQuery;

    /**
     * @var string
     *
     * @ORM\Column(name="referencia", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $referencia;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nome;

    /**
     * @var integer
     *
     * @ORM\Column(name="fornecedor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $fornecedor;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_fornecedor", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nomeFornecedor;

    /**
     * @var string
     *
     * @ORM\Column(name="slug_fornecedor", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $slugFornecedor;

    /**
     * @var string
     *
     * @ORM\Column(name="marca_fornecedor", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $marcaFornecedor;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo_fornecedor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prazoFornecedor;

    /**
     * @var integer
     *
     * @ORM\Column(name="prd_prazo_fornecedor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prdPrazoFornecedor;

    /**
     * @var string
     *
     * @ORM\Column(name="disponibilidade", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $disponibilidade;

    /**
     * @var boolean
     *
     * @ORM\Column(name="esgotado", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $esgotado;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mostra", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $mostra;

    /**
     * @var integer
     *
     * @ORM\Column(name="prazo", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prazo;

    /**
     * @var string
     *
     * @ORM\Column(name="acao", type="string", length=3, precision=0, scale=0, nullable=true, unique=false)
     */
    private $acao;

    /**
     * @var integer
     *
     * @ORM\Column(name="categoria", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_categoria", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nomeCategoria;

    /**
     * @var string
     *
     * @ORM\Column(name="link_categoria", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $linkCategoria;

    /**
     * @var integer
     *
     * @ORM\Column(name="shopping", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $shopping;

    /**
     * @var integer
     *
     * @ORM\Column(name="cat_visivel", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $catVisivel;

    /**
     * @var integer
     *
     * @ORM\Column(name="categoria_pai", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $categoriaPai;

    /**
     * @var string
     *
     * @ORM\Column(name="chamada", type="text", precision=0, scale=0, nullable=true, unique=false)
     */
    private $chamada;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", precision=0, scale=0, nullable=true, unique=false)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="pchaves", type="text", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pchaves;

    /**
     * @var integer
     *
     * @ORM\Column(name="n_mostrar", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $nMostrar;

    /**
     * @var string
     *
     * @ORM\Column(name="cross_sale_grupo_1", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $crossSaleGrupo1;

    /**
     * @var string
     *
     * @ORM\Column(name="cross_sale_grupo_2", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $crossSaleGrupo2;

    /**
     * @var integer
     *
     * @ORM\Column(name="tamanho", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tamanho;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_tamanho", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nomeTamanho;

    /**
     * @var string
     *
     * @ORM\Column(name="slug_tamanho", type="string", length=150, precision=0, scale=0, nullable=true, unique=false)
     */
    private $slugTamanho;

    /**
     * @var integer
     *
     * @ORM\Column(name="cor", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $cor;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_cor", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nomeCor;

    /**
     * @var string
     *
     * @ORM\Column(name="slug_cor", type="string", length=150, precision=0, scale=0, nullable=true, unique=false)
     */
    private $slugCor;

    /**
     * @var string
     *
     * @ORM\Column(name="cor_nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $corNome;

    /**
     * @var integer
     *
     * @ORM\Column(name="estilo", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $estilo;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_estilo", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nomeEstilo;

    /**
     * @var string
     *
     * @ORM\Column(name="slug_estilo", type="string", length=150, precision=0, scale=0, nullable=true, unique=false)
     */
    private $slugEstilo;

    /**
     * @var integer
     *
     * @ORM\Column(name="sexo", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $sexo;

    /**
     * @var string
     *
     * @ORM\Column(name="link_video", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $linkVideo;

    /**
     * @var string
     *
     * @ORM\Column(name="link_extra", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $linkExtra;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disponivel", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $disponivel;

    /**
     * @var integer
     *
     * @ORM\Column(name="disp_dias", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dispDias;

    /**
     * @var boolean
     *
     * @ORM\Column(name="destaque", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $destaque;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prevenda", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $prevenda;

    /**
     * @var boolean
     *
     * @ORM\Column(name="maisvendido", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $maisvendido;

    /**
     * @var integer
     *
     * @ORM\Column(name="mais_vend_ordem", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $maisVendOrdem;

    /**
     * @var boolean
     *
     * @ORM\Column(name="presente", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $presente;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtchegada", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtchegada;

    /**
     * @var boolean
     *
     * @ORM\Column(name="promocao_produto", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $promocaoProduto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="promocaoini", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $promocaoini;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="promocaofim", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $promocaofim;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bpromocao", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $bpromocao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lancamento", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $lancamento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtiniciolancamento", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtiniciolancamento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtfimlancamento", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtfimlancamento;

    /**
     * @var boolean
     *
     * @ORM\Column(name="blancamento", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $blancamento;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem_listagem", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ordemListagem;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem_lst", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ordemLst;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem_prd", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ordemPrd;

    /**
     * @var string
     *
     * @ORM\Column(name="estoque", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $estoque;

    /**
     * @var string
     *
     * @ORM\Column(name="stqfornecedor", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $stqfornecedor;

    /**
     * @var string
     *
     * @ORM\Column(name="stqforn_reservado", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $stqfornReservado;

    /**
     * @var string
     *
     * @ORM\Column(name="stqminimo", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $stqminimo;

    /**
     * @var integer
     *
     * @ORM\Column(name="stqmin_acao", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $stqminAcao;

    /**
     * @var float
     *
     * @ORM\Column(name="peso", type="float", precision=53, scale=2, nullable=true, unique=false)
     */
    private $peso;

    /**
     * @var float
     *
     * @ORM\Column(name="cubagem", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $cubagem;

    /**
     * @var integer
     *
     * @ORM\Column(name="itens", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $itens;

    /**
     * @var float
     *
     * @ORM\Column(name="custo", type="float", precision=53, scale=2, nullable=true, unique=false)
     */
    private $custo;

    /**
     * @var float
     *
     * @ORM\Column(name="pvreal", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pvreal;

    /**
     * @var float
     *
     * @ORM\Column(name="pvpromocao", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pvpromocao;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto_valor", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $descontoValor;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto_perc", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $descontoPerc;

    /**
     * @var string
     *
     * @ORM\Column(name="use_desc", type="string", length=1, precision=0, scale=0, nullable=true, unique=false)
     */
    private $useDesc;

    /**
     * @var float
     *
     * @ORM\Column(name="pvparcelado", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $pvparcelado;

    /**
     * @var float
     *
     * @ORM\Column(name="preco_produto", type="float", precision=9, scale=2, nullable=true, unique=false)
     */
    private $precoProduto;

    /**
     * @var string
     *
     * @ORM\Column(name="imagens_situacao", type="text", precision=0, scale=0, nullable=true, unique=false)
     */
    private $imagensSituacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="vnd_produto_1", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $vndProduto1;

    /**
     * @var float
     *
     * @ORM\Column(name="vnd_desconto_1", type="float", precision=53, scale=2, nullable=true, unique=false)
     */
    private $vndDesconto1;

    /**
     * @var integer
     *
     * @ORM\Column(name="vnd_produto_2", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $vndProduto2;

    /**
     * @var float
     *
     * @ORM\Column(name="vnd_desconto_2", type="float", precision=53, scale=2, nullable=true, unique=false)
     */
    private $vndDesconto2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usar_brinde", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $usarBrinde;

    /**
     * @var float
     *
     * @ORM\Column(name="media_opinioes", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $mediaOpinioes;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_opinioes", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $totalOpinioes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ignorar_auto_crosssale", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ignorarAutoCrosssale;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cross_sale_disponivel", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $crossSaleDisponivel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ignorar_venda_junta", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ignorarVendaJunta;

    /**
     * @var string
     *
     * @ORM\Column(name="unidade_fracionada", type="decimal", precision=11, scale=2, nullable=true, unique=false)
     */
    private $unidadeFracionada;

    /**
     * @var integer
     *
     * @ORM\Column(name="volumes", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $volumes;

    /**
     * @var float
     *
     * @ORM\Column(name="altura", type="float", precision=9, scale=5, nullable=true, unique=false)
     */
    private $altura;

    /**
     * @var float
     *
     * @ORM\Column(name="largura", type="float", precision=9, scale=5, nullable=true, unique=false)
     */
    private $largura;

    /**
     * @var float
     *
     * @ORM\Column(name="profundidade", type="float", precision=9, scale=5, nullable=true, unique=false)
     */
    private $profundidade;

    /**
     * @var float
     *
     * @ORM\Column(name="diametro", type="float", precision=9, scale=5, nullable=true, unique=false)
     */
    private $diametro;

    /**
     * @var boolean
     *
     * @ORM\Column(name="subsidiar_frete", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $subsidiarFrete;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="negociacao_ini", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $negociacaoIni;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="negociacao_fim", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $negociacaoFim;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="saindo_de_linha_ini", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $saindoDeLinhaIni;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="saindo_de_linha_fim", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $saindoDeLinhaFim;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_lst", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tipoLst;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip_saindo_de_linha", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $tooltipSaindoDeLinha;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip_negociacao", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $tooltipNegociacao;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip_promocao", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $tooltipPromocao;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip_cor", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $tooltipCor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="relogio_promocao", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $relogioPromocao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ultima_verificacao_preco", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ultimaVerificacaoPreco;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matriz
     *
     * @param integer $matriz
     *
     * @return B2cViewProdutos
     */
    public function setMatriz($matriz)
    {
        $this->matriz = $matriz;

        return $this;
    }

    /**
     * Get matriz
     *
     * @return integer
     */
    public function getMatriz()
    {
        return $this->matriz;
    }

    /**
     * Set dtaInclusao
     *
     * @param \DateTime $dtaInclusao
     *
     * @return B2cViewProdutos
     */
    public function setDtaInclusao($dtaInclusao)
    {
        $this->dtaInclusao = $dtaInclusao;

        return $this;
    }

    /**
     * Get dtaInclusao
     *
     * @return \DateTime
     */
    public function getDtaInclusao()
    {
        return $this->dtaInclusao;
    }

    /**
     * Set dtaAlteracao
     *
     * @param \DateTime $dtaAlteracao
     *
     * @return B2cViewProdutos
     */
    public function setDtaAlteracao($dtaAlteracao)
    {
        $this->dtaAlteracao = $dtaAlteracao;

        return $this;
    }

    /**
     * Get dtaAlteracao
     *
     * @return \DateTime
     */
    public function getDtaAlteracao()
    {
        return $this->dtaAlteracao;
    }

    /**
     * Set usuarioAlteracao
     *
     * @param string $usuarioAlteracao
     *
     * @return B2cViewProdutos
     */
    public function setUsuarioAlteracao($usuarioAlteracao)
    {
        $this->usuarioAlteracao = $usuarioAlteracao;

        return $this;
    }

    /**
     * Get usuarioAlteracao
     *
     * @return string
     */
    public function getUsuarioAlteracao()
    {
        return $this->usuarioAlteracao;
    }

    /**
     * Set produtoKit
     *
     * @param integer $produtoKit
     *
     * @return B2cViewProdutos
     */
    public function setProdutoKit($produtoKit)
    {
        $this->produtoKit = $produtoKit;

        return $this;
    }

    /**
     * Get produtoKit
     *
     * @return integer
     */
    public function getProdutoKit()
    {
        return $this->produtoKit;
    }

    /**
     * Set listaPrdKit
     *
     * @param string $listaPrdKit
     *
     * @return B2cViewProdutos
     */
    public function setListaPrdKit($listaPrdKit)
    {
        $this->listaPrdKit = $listaPrdKit;

        return $this;
    }

    /**
     * Get listaPrdKit
     *
     * @return string
     */
    public function getListaPrdKit()
    {
        return $this->listaPrdKit;
    }

    /**
     * Set descKit
     *
     * @param string $descKit
     *
     * @return B2cViewProdutos
     */
    public function setDescKit($descKit)
    {
        $this->descKit = $descKit;

        return $this;
    }

    /**
     * Get descKit
     *
     * @return string
     */
    public function getDescKit()
    {
        return $this->descKit;
    }

    /**
     * Set linkProduto
     *
     * @param string $linkProduto
     *
     * @return B2cViewProdutos
     */
    public function setLinkProduto($linkProduto)
    {
        $this->linkProduto = $linkProduto;

        return $this;
    }

    /**
     * Get linkProduto
     *
     * @return string
     */
    public function getLinkProduto()
    {
        return $this->linkProduto;
    }

    /**
     * Set linkProdutoSecao
     *
     * @param string $linkProdutoSecao
     *
     * @return B2cViewProdutos
     */
    public function setLinkProdutoSecao($linkProdutoSecao)
    {
        $this->linkProdutoSecao = $linkProdutoSecao;

        return $this;
    }

    /**
     * Get linkProdutoSecao
     *
     * @return string
     */
    public function getLinkProdutoSecao()
    {
        return $this->linkProdutoSecao;
    }

    /**
     * Set linkProdutoQuery
     *
     * @param string $linkProdutoQuery
     *
     * @return B2cViewProdutos
     */
    public function setLinkProdutoQuery($linkProdutoQuery)
    {
        $this->linkProdutoQuery = $linkProdutoQuery;

        return $this;
    }

    /**
     * Get linkProdutoQuery
     *
     * @return string
     */
    public function getLinkProdutoQuery()
    {
        return $this->linkProdutoQuery;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     *
     * @return B2cViewProdutos
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cViewProdutos
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set fornecedor
     *
     * @param integer $fornecedor
     *
     * @return B2cViewProdutos
     */
    public function setFornecedor($fornecedor)
    {
        $this->fornecedor = $fornecedor;

        return $this;
    }

    /**
     * Get fornecedor
     *
     * @return integer
     */
    public function getFornecedor()
    {
        return $this->fornecedor;
    }

    /**
     * Set nomeFornecedor
     *
     * @param string $nomeFornecedor
     *
     * @return B2cViewProdutos
     */
    public function setNomeFornecedor($nomeFornecedor)
    {
        $this->nomeFornecedor = $nomeFornecedor;

        return $this;
    }

    /**
     * Get nomeFornecedor
     *
     * @return string
     */
    public function getNomeFornecedor()
    {
        return $this->nomeFornecedor;
    }

    /**
     * Set slugFornecedor
     *
     * @param string $slugFornecedor
     *
     * @return B2cViewProdutos
     */
    public function setSlugFornecedor($slugFornecedor)
    {
        $this->slugFornecedor = $slugFornecedor;

        return $this;
    }

    /**
     * Get slugFornecedor
     *
     * @return string
     */
    public function getSlugFornecedor()
    {
        return $this->slugFornecedor;
    }

    /**
     * Set marcaFornecedor
     *
     * @param string $marcaFornecedor
     *
     * @return B2cViewProdutos
     */
    public function setMarcaFornecedor($marcaFornecedor)
    {
        $this->marcaFornecedor = $marcaFornecedor;

        return $this;
    }

    /**
     * Get marcaFornecedor
     *
     * @return string
     */
    public function getMarcaFornecedor()
    {
        return $this->marcaFornecedor;
    }

    /**
     * Set prazoFornecedor
     *
     * @param integer $prazoFornecedor
     *
     * @return B2cViewProdutos
     */
    public function setPrazoFornecedor($prazoFornecedor)
    {
        $this->prazoFornecedor = $prazoFornecedor;

        return $this;
    }

    /**
     * Get prazoFornecedor
     *
     * @return integer
     */
    public function getPrazoFornecedor()
    {
        return $this->prazoFornecedor;
    }

    /**
     * Set prdPrazoFornecedor
     *
     * @param integer $prdPrazoFornecedor
     *
     * @return B2cViewProdutos
     */
    public function setPrdPrazoFornecedor($prdPrazoFornecedor)
    {
        $this->prdPrazoFornecedor = $prdPrazoFornecedor;

        return $this;
    }

    /**
     * Get prdPrazoFornecedor
     *
     * @return integer
     */
    public function getPrdPrazoFornecedor()
    {
        return $this->prdPrazoFornecedor;
    }

    /**
     * Set disponibilidade
     *
     * @param string $disponibilidade
     *
     * @return B2cViewProdutos
     */
    public function setDisponibilidade($disponibilidade)
    {
        $this->disponibilidade = $disponibilidade;

        return $this;
    }

    /**
     * Get disponibilidade
     *
     * @return string
     */
    public function getDisponibilidade()
    {
        return $this->disponibilidade;
    }

    /**
     * Set esgotado
     *
     * @param boolean $esgotado
     *
     * @return B2cViewProdutos
     */
    public function setEsgotado($esgotado)
    {
        $this->esgotado = $esgotado;

        return $this;
    }

    /**
     * Get esgotado
     *
     * @return boolean
     */
    public function getEsgotado()
    {
        return $this->esgotado;
    }

    /**
     * Set mostra
     *
     * @param boolean $mostra
     *
     * @return B2cViewProdutos
     */
    public function setMostra($mostra)
    {
        $this->mostra = $mostra;

        return $this;
    }

    /**
     * Get mostra
     *
     * @return boolean
     */
    public function getMostra()
    {
        return $this->mostra;
    }

    /**
     * Set prazo
     *
     * @param integer $prazo
     *
     * @return B2cViewProdutos
     */
    public function setPrazo($prazo)
    {
        $this->prazo = $prazo;

        return $this;
    }

    /**
     * Get prazo
     *
     * @return integer
     */
    public function getPrazo()
    {
        return $this->prazo;
    }

    /**
     * Set acao
     *
     * @param string $acao
     *
     * @return B2cViewProdutos
     */
    public function setAcao($acao)
    {
        $this->acao = $acao;

        return $this;
    }

    /**
     * Get acao
     *
     * @return string
     */
    public function getAcao()
    {
        return $this->acao;
    }

    /**
     * Set categoria
     *
     * @param integer $categoria
     *
     * @return B2cViewProdutos
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return integer
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set nomeCategoria
     *
     * @param string $nomeCategoria
     *
     * @return B2cViewProdutos
     */
    public function setNomeCategoria($nomeCategoria)
    {
        $this->nomeCategoria = $nomeCategoria;

        return $this;
    }

    /**
     * Get nomeCategoria
     *
     * @return string
     */
    public function getNomeCategoria()
    {
        return $this->nomeCategoria;
    }

    /**
     * Set linkCategoria
     *
     * @param string $linkCategoria
     *
     * @return B2cViewProdutos
     */
    public function setLinkCategoria($linkCategoria)
    {
        $this->linkCategoria = $linkCategoria;

        return $this;
    }

    /**
     * Get linkCategoria
     *
     * @return string
     */
    public function getLinkCategoria()
    {
        return $this->linkCategoria;
    }

    /**
     * Set shopping
     *
     * @param integer $shopping
     *
     * @return B2cViewProdutos
     */
    public function setShopping($shopping)
    {
        $this->shopping = $shopping;

        return $this;
    }

    /**
     * Get shopping
     *
     * @return integer
     */
    public function getShopping()
    {
        return $this->shopping;
    }

    /**
     * Set catVisivel
     *
     * @param integer $catVisivel
     *
     * @return B2cViewProdutos
     */
    public function setCatVisivel($catVisivel)
    {
        $this->catVisivel = $catVisivel;

        return $this;
    }

    /**
     * Get catVisivel
     *
     * @return integer
     */
    public function getCatVisivel()
    {
        return $this->catVisivel;
    }

    /**
     * Set categoriaPai
     *
     * @param integer $categoriaPai
     *
     * @return B2cViewProdutos
     */
    public function setCategoriaPai($categoriaPai)
    {
        $this->categoriaPai = $categoriaPai;

        return $this;
    }

    /**
     * Get categoriaPai
     *
     * @return integer
     */
    public function getCategoriaPai()
    {
        return $this->categoriaPai;
    }

    /**
     * Set chamada
     *
     * @param string $chamada
     *
     * @return B2cViewProdutos
     */
    public function setChamada($chamada)
    {
        $this->chamada = $chamada;

        return $this;
    }

    /**
     * Get chamada
     *
     * @return string
     */
    public function getChamada()
    {
        return $this->chamada;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return B2cViewProdutos
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set pchaves
     *
     * @param string $pchaves
     *
     * @return B2cViewProdutos
     */
    public function setPchaves($pchaves)
    {
        $this->pchaves = $pchaves;

        return $this;
    }

    /**
     * Get pchaves
     *
     * @return string
     */
    public function getPchaves()
    {
        return $this->pchaves;
    }

    /**
     * Set nMostrar
     *
     * @param integer $nMostrar
     *
     * @return B2cViewProdutos
     */
    public function setNMostrar($nMostrar)
    {
        $this->nMostrar = $nMostrar;

        return $this;
    }

    /**
     * Get nMostrar
     *
     * @return integer
     */
    public function getNMostrar()
    {
        return $this->nMostrar;
    }

    /**
     * Set crossSaleGrupo1
     *
     * @param string $crossSaleGrupo1
     *
     * @return B2cViewProdutos
     */
    public function setCrossSaleGrupo1($crossSaleGrupo1)
    {
        $this->crossSaleGrupo1 = $crossSaleGrupo1;

        return $this;
    }

    /**
     * Get crossSaleGrupo1
     *
     * @return string
     */
    public function getCrossSaleGrupo1()
    {
        return $this->crossSaleGrupo1;
    }

    /**
     * Set crossSaleGrupo2
     *
     * @param string $crossSaleGrupo2
     *
     * @return B2cViewProdutos
     */
    public function setCrossSaleGrupo2($crossSaleGrupo2)
    {
        $this->crossSaleGrupo2 = $crossSaleGrupo2;

        return $this;
    }

    /**
     * Get crossSaleGrupo2
     *
     * @return string
     */
    public function getCrossSaleGrupo2()
    {
        return $this->crossSaleGrupo2;
    }

    /**
     * Set tamanho
     *
     * @param integer $tamanho
     *
     * @return B2cViewProdutos
     */
    public function setTamanho($tamanho)
    {
        $this->tamanho = $tamanho;

        return $this;
    }

    /**
     * Get tamanho
     *
     * @return integer
     */
    public function getTamanho()
    {
        return $this->tamanho;
    }

    /**
     * Set nomeTamanho
     *
     * @param string $nomeTamanho
     *
     * @return B2cViewProdutos
     */
    public function setNomeTamanho($nomeTamanho)
    {
        $this->nomeTamanho = $nomeTamanho;

        return $this;
    }

    /**
     * Get nomeTamanho
     *
     * @return string
     */
    public function getNomeTamanho()
    {
        return $this->nomeTamanho;
    }

    /**
     * Set slugTamanho
     *
     * @param string $slugTamanho
     *
     * @return B2cViewProdutos
     */
    public function setSlugTamanho($slugTamanho)
    {
        $this->slugTamanho = $slugTamanho;

        return $this;
    }

    /**
     * Get slugTamanho
     *
     * @return string
     */
    public function getSlugTamanho()
    {
        return $this->slugTamanho;
    }

    /**
     * Set cor
     *
     * @param integer $cor
     *
     * @return B2cViewProdutos
     */
    public function setCor($cor)
    {
        $this->cor = $cor;

        return $this;
    }

    /**
     * Get cor
     *
     * @return integer
     */
    public function getCor()
    {
        return $this->cor;
    }

    /**
     * Set nomeCor
     *
     * @param string $nomeCor
     *
     * @return B2cViewProdutos
     */
    public function setNomeCor($nomeCor)
    {
        $this->nomeCor = $nomeCor;

        return $this;
    }

    /**
     * Get nomeCor
     *
     * @return string
     */
    public function getNomeCor()
    {
        return $this->nomeCor;
    }

    /**
     * Set slugCor
     *
     * @param string $slugCor
     *
     * @return B2cViewProdutos
     */
    public function setSlugCor($slugCor)
    {
        $this->slugCor = $slugCor;

        return $this;
    }

    /**
     * Get slugCor
     *
     * @return string
     */
    public function getSlugCor()
    {
        return $this->slugCor;
    }

    /**
     * Set corNome
     *
     * @param string $corNome
     *
     * @return B2cViewProdutos
     */
    public function setCorNome($corNome)
    {
        $this->corNome = $corNome;

        return $this;
    }

    /**
     * Get corNome
     *
     * @return string
     */
    public function getCorNome()
    {
        return $this->corNome;
    }

    /**
     * Set estilo
     *
     * @param integer $estilo
     *
     * @return B2cViewProdutos
     */
    public function setEstilo($estilo)
    {
        $this->estilo = $estilo;

        return $this;
    }

    /**
     * Get estilo
     *
     * @return integer
     */
    public function getEstilo()
    {
        return $this->estilo;
    }

    /**
     * Set nomeEstilo
     *
     * @param string $nomeEstilo
     *
     * @return B2cViewProdutos
     */
    public function setNomeEstilo($nomeEstilo)
    {
        $this->nomeEstilo = $nomeEstilo;

        return $this;
    }

    /**
     * Get nomeEstilo
     *
     * @return string
     */
    public function getNomeEstilo()
    {
        return $this->nomeEstilo;
    }

    /**
     * Set slugEstilo
     *
     * @param string $slugEstilo
     *
     * @return B2cViewProdutos
     */
    public function setSlugEstilo($slugEstilo)
    {
        $this->slugEstilo = $slugEstilo;

        return $this;
    }

    /**
     * Get slugEstilo
     *
     * @return string
     */
    public function getSlugEstilo()
    {
        return $this->slugEstilo;
    }

    /**
     * Set sexo
     *
     * @param integer $sexo
     *
     * @return B2cViewProdutos
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return integer
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set linkVideo
     *
     * @param string $linkVideo
     *
     * @return B2cViewProdutos
     */
    public function setLinkVideo($linkVideo)
    {
        $this->linkVideo = $linkVideo;

        return $this;
    }

    /**
     * Get linkVideo
     *
     * @return string
     */
    public function getLinkVideo()
    {
        return $this->linkVideo;
    }

    /**
     * Set linkExtra
     *
     * @param string $linkExtra
     *
     * @return B2cViewProdutos
     */
    public function setLinkExtra($linkExtra)
    {
        $this->linkExtra = $linkExtra;

        return $this;
    }

    /**
     * Get linkExtra
     *
     * @return string
     */
    public function getLinkExtra()
    {
        return $this->linkExtra;
    }

    /**
     * Set disponivel
     *
     * @param boolean $disponivel
     *
     * @return B2cViewProdutos
     */
    public function setDisponivel($disponivel)
    {
        $this->disponivel = $disponivel;

        return $this;
    }

    /**
     * Get disponivel
     *
     * @return boolean
     */
    public function getDisponivel()
    {
        return $this->disponivel;
    }

    /**
     * Set dispDias
     *
     * @param integer $dispDias
     *
     * @return B2cViewProdutos
     */
    public function setDispDias($dispDias)
    {
        $this->dispDias = $dispDias;

        return $this;
    }

    /**
     * Get dispDias
     *
     * @return integer
     */
    public function getDispDias()
    {
        return $this->dispDias;
    }

    /**
     * Set destaque
     *
     * @param boolean $destaque
     *
     * @return B2cViewProdutos
     */
    public function setDestaque($destaque)
    {
        $this->destaque = $destaque;

        return $this;
    }

    /**
     * Get destaque
     *
     * @return boolean
     */
    public function getDestaque()
    {
        return $this->destaque;
    }

    /**
     * Set prevenda
     *
     * @param boolean $prevenda
     *
     * @return B2cViewProdutos
     */
    public function setPrevenda($prevenda)
    {
        $this->prevenda = $prevenda;

        return $this;
    }

    /**
     * Get prevenda
     *
     * @return boolean
     */
    public function getPrevenda()
    {
        return $this->prevenda;
    }

    /**
     * Set maisvendido
     *
     * @param boolean $maisvendido
     *
     * @return B2cViewProdutos
     */
    public function setMaisvendido($maisvendido)
    {
        $this->maisvendido = $maisvendido;

        return $this;
    }

    /**
     * Get maisvendido
     *
     * @return boolean
     */
    public function getMaisvendido()
    {
        return $this->maisvendido;
    }

    /**
     * Set maisVendOrdem
     *
     * @param integer $maisVendOrdem
     *
     * @return B2cViewProdutos
     */
    public function setMaisVendOrdem($maisVendOrdem)
    {
        $this->maisVendOrdem = $maisVendOrdem;

        return $this;
    }

    /**
     * Get maisVendOrdem
     *
     * @return integer
     */
    public function getMaisVendOrdem()
    {
        return $this->maisVendOrdem;
    }

    /**
     * Set presente
     *
     * @param boolean $presente
     *
     * @return B2cViewProdutos
     */
    public function setPresente($presente)
    {
        $this->presente = $presente;

        return $this;
    }

    /**
     * Get presente
     *
     * @return boolean
     */
    public function getPresente()
    {
        return $this->presente;
    }

    /**
     * Set dtchegada
     *
     * @param \DateTime $dtchegada
     *
     * @return B2cViewProdutos
     */
    public function setDtchegada($dtchegada)
    {
        $this->dtchegada = $dtchegada;

        return $this;
    }

    /**
     * Get dtchegada
     *
     * @return \DateTime
     */
    public function getDtchegada()
    {
        return $this->dtchegada;
    }

    /**
     * Set promocaoProduto
     *
     * @param boolean $promocaoProduto
     *
     * @return B2cViewProdutos
     */
    public function setPromocaoProduto($promocaoProduto)
    {
        $this->promocaoProduto = $promocaoProduto;

        return $this;
    }

    /**
     * Get promocaoProduto
     *
     * @return boolean
     */
    public function getPromocaoProduto()
    {
        return $this->promocaoProduto;
    }

    /**
     * Set promocaoini
     *
     * @param \DateTime $promocaoini
     *
     * @return B2cViewProdutos
     */
    public function setPromocaoini($promocaoini)
    {
        $this->promocaoini = $promocaoini;

        return $this;
    }

    /**
     * Get promocaoini
     *
     * @return \DateTime
     */
    public function getPromocaoini()
    {
        return $this->promocaoini;
    }

    /**
     * Set promocaofim
     *
     * @param \DateTime $promocaofim
     *
     * @return B2cViewProdutos
     */
    public function setPromocaofim($promocaofim)
    {
        $this->promocaofim = $promocaofim;

        return $this;
    }

    /**
     * Get promocaofim
     *
     * @return \DateTime
     */
    public function getPromocaofim()
    {
        return $this->promocaofim;
    }

    /**
     * Set bpromocao
     *
     * @param boolean $bpromocao
     *
     * @return B2cViewProdutos
     */
    public function setBpromocao($bpromocao)
    {
        $this->bpromocao = $bpromocao;

        return $this;
    }

    /**
     * Get bpromocao
     *
     * @return boolean
     */
    public function getBpromocao()
    {
        return $this->bpromocao;
    }

    /**
     * Set lancamento
     *
     * @param boolean $lancamento
     *
     * @return B2cViewProdutos
     */
    public function setLancamento($lancamento)
    {
        $this->lancamento = $lancamento;

        return $this;
    }

    /**
     * Get lancamento
     *
     * @return boolean
     */
    public function getLancamento()
    {
        return $this->lancamento;
    }

    /**
     * Set dtiniciolancamento
     *
     * @param \DateTime $dtiniciolancamento
     *
     * @return B2cViewProdutos
     */
    public function setDtiniciolancamento($dtiniciolancamento)
    {
        $this->dtiniciolancamento = $dtiniciolancamento;

        return $this;
    }

    /**
     * Get dtiniciolancamento
     *
     * @return \DateTime
     */
    public function getDtiniciolancamento()
    {
        return $this->dtiniciolancamento;
    }

    /**
     * Set dtfimlancamento
     *
     * @param \DateTime $dtfimlancamento
     *
     * @return B2cViewProdutos
     */
    public function setDtfimlancamento($dtfimlancamento)
    {
        $this->dtfimlancamento = $dtfimlancamento;

        return $this;
    }

    /**
     * Get dtfimlancamento
     *
     * @return \DateTime
     */
    public function getDtfimlancamento()
    {
        return $this->dtfimlancamento;
    }

    /**
     * Set blancamento
     *
     * @param boolean $blancamento
     *
     * @return B2cViewProdutos
     */
    public function setBlancamento($blancamento)
    {
        $this->blancamento = $blancamento;

        return $this;
    }

    /**
     * Get blancamento
     *
     * @return boolean
     */
    public function getBlancamento()
    {
        return $this->blancamento;
    }

    /**
     * Set ordemListagem
     *
     * @param integer $ordemListagem
     *
     * @return B2cViewProdutos
     */
    public function setOrdemListagem($ordemListagem)
    {
        $this->ordemListagem = $ordemListagem;

        return $this;
    }

    /**
     * Get ordemListagem
     *
     * @return integer
     */
    public function getOrdemListagem()
    {
        return $this->ordemListagem;
    }

    /**
     * Set ordemLst
     *
     * @param integer $ordemLst
     *
     * @return B2cViewProdutos
     */
    public function setOrdemLst($ordemLst)
    {
        $this->ordemLst = $ordemLst;

        return $this;
    }

    /**
     * Get ordemLst
     *
     * @return integer
     */
    public function getOrdemLst()
    {
        return $this->ordemLst;
    }

    /**
     * Set ordemPrd
     *
     * @param integer $ordemPrd
     *
     * @return B2cViewProdutos
     */
    public function setOrdemPrd($ordemPrd)
    {
        $this->ordemPrd = $ordemPrd;

        return $this;
    }

    /**
     * Get ordemPrd
     *
     * @return integer
     */
    public function getOrdemPrd()
    {
        return $this->ordemPrd;
    }

    /**
     * Set estoque
     *
     * @param string $estoque
     *
     * @return B2cViewProdutos
     */
    public function setEstoque($estoque)
    {
        $this->estoque = $estoque;

        return $this;
    }

    /**
     * Get estoque
     *
     * @return string
     */
    public function getEstoque()
    {
        return $this->estoque;
    }

    /**
     * Set stqfornecedor
     *
     * @param string $stqfornecedor
     *
     * @return B2cViewProdutos
     */
    public function setStqfornecedor($stqfornecedor)
    {
        $this->stqfornecedor = $stqfornecedor;

        return $this;
    }

    /**
     * Get stqfornecedor
     *
     * @return string
     */
    public function getStqfornecedor()
    {
        return $this->stqfornecedor;
    }

    /**
     * Set stqfornReservado
     *
     * @param string $stqfornReservado
     *
     * @return B2cViewProdutos
     */
    public function setStqfornReservado($stqfornReservado)
    {
        $this->stqfornReservado = $stqfornReservado;

        return $this;
    }

    /**
     * Get stqfornecedor
     *
     * @return string
     */
    public function getStqfornReservado()
    {
        return $this->stqfornReservado;
    }

    /**
     * Set stqminimo
     *
     * @param string $stqminimo
     *
     * @return B2cViewProdutos
     */
    public function setStqminimo($stqminimo)
    {
        $this->stqminimo = $stqminimo;

        return $this;
    }

    /**
     * Get stqminimo
     *
     * @return string
     */
    public function getStqminimo()
    {
        return $this->stqminimo;
    }

    /**
     * Set stqminAcao
     *
     * @param integer $stqminAcao
     *
     * @return B2cViewProdutos
     */
    public function setStqminAcao($stqminAcao)
    {
        $this->stqminAcao = $stqminAcao;

        return $this;
    }

    /**
     * Get stqminAcao
     *
     * @return integer
     */
    public function getStqminAcao()
    {
        return $this->stqminAcao;
    }

    /**
     * Set peso
     *
     * @param float $peso
     *
     * @return B2cViewProdutos
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return float
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set cubagem
     *
     * @param float $cubagem
     *
     * @return B2cViewProdutos
     */
    public function setCubagem($cubagem)
    {
        $this->cubagem = $cubagem;

        return $this;
    }

    /**
     * Get cubagem
     *
     * @return float
     */
    public function getCubagem()
    {
        return $this->cubagem;
    }

    /**
     * Set itens
     *
     * @param integer $itens
     *
     * @return B2cViewProdutos
     */
    public function setItens($itens)
    {
        $this->itens = $itens;

        return $this;
    }

    /**
     * Get itens
     *
     * @return integer
     */
    public function getItens()
    {
        return $this->itens;
    }

    /**
     * Set custo
     *
     * @param float $custo
     *
     * @return B2cViewProdutos
     */
    public function setCusto($custo)
    {
        $this->custo = $custo;

        return $this;
    }

    /**
     * Get custo
     *
     * @return float
     */
    public function getCusto()
    {
        return $this->custo;
    }

    /**
     * Set pvreal
     *
     * @param float $pvreal
     *
     * @return B2cViewProdutos
     */
    public function setPvreal($pvreal)
    {
        $this->pvreal = $pvreal;

        return $this;
    }

    /**
     * Get pvreal
     *
     * @return float
     */
    public function getPvreal()
    {
        return $this->pvreal;
    }

    /**
     * Set pvpromocao
     *
     * @param float $pvpromocao
     *
     * @return B2cViewProdutos
     */
    public function setPvpromocao($pvpromocao)
    {
        $this->pvpromocao = $pvpromocao;

        return $this;
    }

    /**
     * Get pvpromocao
     *
     * @return float
     */
    public function getPvpromocao()
    {
        return $this->pvpromocao;
    }

    /**
     * Set descontoValor
     *
     * @param float $descontoValor
     *
     * @return B2cViewProdutos
     */
    public function setDescontoValor($descontoValor)
    {
        $this->descontoValor = $descontoValor;

        return $this;
    }

    /**
     * Get descontoValor
     *
     * @return float
     */
    public function getDescontoValor()
    {
        return $this->descontoValor;
    }

    /**
     * Set descontoPerc
     *
     * @param float $descontoPerc
     *
     * @return B2cViewProdutos
     */
    public function setDescontoPerc($descontoPerc)
    {
        $this->descontoPerc = $descontoPerc;

        return $this;
    }

    /**
     * Get descontoPerc
     *
     * @return float
     */
    public function getDescontoPerc()
    {
        return $this->descontoPerc;
    }

    /**
     * Set useDesc
     *
     * @param string $useDesc
     *
     * @return B2cViewProdutos
     */
    public function setUseDesc($useDesc)
    {
        $this->useDesc = $useDesc;

        return $this;
    }

    /**
     * Get useDesc
     *
     * @return string
     */
    public function getUseDesc()
    {
        return $this->useDesc;
    }

    /**
     * Set pvparcelado
     *
     * @param float $pvparcelado
     *
     * @return B2cViewProdutos
     */
    public function setPvparcelado($pvparcelado)
    {
        $this->pvparcelado = $pvparcelado;

        return $this;
    }

    /**
     * Get pvparcelado
     *
     * @return float
     */
    public function getPvparcelado()
    {
        return $this->pvparcelado;
    }

    /**
     * Set precoProduto
     *
     * @param float $precoProduto
     *
     * @return B2cViewProdutos
     */
    public function setPrecoProduto($precoProduto)
    {
        $this->precoProduto = $precoProduto;

        return $this;
    }

    /**
     * Get precoProduto
     *
     * @return float
     */
    public function getPrecoProduto()
    {
        return $this->precoProduto;
    }

    /**
     * Set imagensSituacao
     *
     * @param string $imagensSituacao
     *
     * @return B2cViewProdutos
     */
    public function setImagensSituacao($imagensSituacao)
    {
        $this->imagensSituacao = $imagensSituacao;

        return $this;
    }

    /**
     * Get imagensSituacao
     *
     * @return string
     */
    public function getImagensSituacao()
    {
        return $this->imagensSituacao;
    }

    /**
     * Set vndProduto1
     *
     * @param integer $vndProduto1
     *
     * @return B2cViewProdutos
     */
    public function setVndProduto1($vndProduto1)
    {
        $this->vndProduto1 = $vndProduto1;

        return $this;
    }

    /**
     * Get vndProduto1
     *
     * @return integer
     */
    public function getVndProduto1()
    {
        return $this->vndProduto1;
    }

    /**
     * Set vndDesconto1
     *
     * @param float $vndDesconto1
     *
     * @return B2cViewProdutos
     */
    public function setVndDesconto1($vndDesconto1)
    {
        $this->vndDesconto1 = $vndDesconto1;

        return $this;
    }

    /**
     * Get vndDesconto1
     *
     * @return float
     */
    public function getVndDesconto1()
    {
        return $this->vndDesconto1;
    }

    /**
     * Set vndProduto2
     *
     * @param integer $vndProduto2
     *
     * @return B2cViewProdutos
     */
    public function setVndProduto2($vndProduto2)
    {
        $this->vndProduto2 = $vndProduto2;

        return $this;
    }

    /**
     * Get vndProduto2
     *
     * @return integer
     */
    public function getVndProduto2()
    {
        return $this->vndProduto2;
    }

    /**
     * Set vndDesconto2
     *
     * @param float $vndDesconto2
     *
     * @return B2cViewProdutos
     */
    public function setVndDesconto2($vndDesconto2)
    {
        $this->vndDesconto2 = $vndDesconto2;

        return $this;
    }

    /**
     * Get vndDesconto2
     *
     * @return float
     */
    public function getVndDesconto2()
    {
        return $this->vndDesconto2;
    }

    /**
     * Set usarBrinde
     *
     * @param boolean $usarBrinde
     *
     * @return B2cViewProdutos
     */
    public function setUsarBrinde($usarBrinde)
    {
        $this->usarBrinde = $usarBrinde;

        return $this;
    }

    /**
     * Get usarBrinde
     *
     * @return boolean
     */
    public function getUsarBrinde()
    {
        return $this->usarBrinde;
    }

    /**
     * Set mediaOpinioes
     *
     * @param float $mediaOpinioes
     *
     * @return B2cViewProdutos
     */
    public function setMediaOpinioes($mediaOpinioes)
    {
        $this->mediaOpinioes = $mediaOpinioes;

        return $this;
    }

    /**
     * Get mediaOpinioes
     *
     * @return float
     */
    public function getMediaOpinioes()
    {
        return $this->mediaOpinioes;
    }

    /**
     * Set totalOpinioes
     *
     * @param integer $totalOpinioes
     *
     * @return B2cViewProdutos
     */
    public function setTotalOpinioes($totalOpinioes)
    {
        $this->totalOpinioes = $totalOpinioes;

        return $this;
    }

    /**
     * Get totalOpinioes
     *
     * @return integer
     */
    public function getTotalOpinioes()
    {
        return $this->totalOpinioes;
    }

    /**
     * Set ignorarAutoCrosssale
     *
     * @param boolean $ignorarAutoCrosssale
     *
     * @return B2cViewProdutos
     */
    public function setIgnorarAutoCrosssale($ignorarAutoCrosssale)
    {
        $this->ignorarAutoCrosssale = $ignorarAutoCrosssale;

        return $this;
    }

    /**
     * Get ignorarAutoCrosssale
     *
     * @return boolean
     */
    public function getIgnorarAutoCrosssale()
    {
        return $this->ignorarAutoCrosssale;
    }

    /**
     * Set crossSaleDisponivel
     *
     * @param boolean $crossSaleDisponivel
     *
     * @return B2cViewProdutos
     */
    public function setCrossSaleDisponivel($crossSaleDisponivel)
    {
        $this->crossSaleDisponivel = $crossSaleDisponivel;

        return $this;
    }

    /**
     * Get crossSaleDisponivel
     *
     * @return boolean
     */
    public function getCrossSaleDisponivel()
    {
        return $this->crossSaleDisponivel;
    }

    /**
     * Set ignorarVendaJunta
     *
     * @param boolean $ignorarVendaJunta
     *
     * @return B2cViewProdutos
     */
    public function setIgnorarVendaJunta($ignorarVendaJunta)
    {
        $this->ignorarVendaJunta = $ignorarVendaJunta;

        return $this;
    }

    /**
     * Get ignorarVendaJunta
     *
     * @return boolean
     */
    public function getIgnorarVendaJunta()
    {
        return $this->ignorarVendaJunta;
    }

    /**
     * Set unidadeFracionada
     *
     * @param string $unidadeFracionada
     *
     * @return B2cViewProdutos
     */
    public function setUnidadeFracionada($unidadeFracionada)
    {
        $this->unidadeFracionada = $unidadeFracionada;

        return $this;
    }

    /**
     * Get unidadeFracionada
     *
     * @return string
     */
    public function getUnidadeFracionada()
    {
        return $this->unidadeFracionada;
    }

    /**
     * Set volumes
     *
     * @param integer $volumes
     *
     * @return B2cViewProdutos
     */
    public function setVolumes($volumes)
    {
        $this->volumes = $volumes;

        return $this;
    }

    /**
     * Get volumes
     *
     * @return integer
     */
    public function getVolumes()
    {
        return $this->volumes;
    }

    /**
     * Set altura
     *
     * @param float $altura
     *
     * @return B2cViewProdutos
     */
    public function setAltura($altura)
    {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get altura
     *
     * @return float
     */
    public function getAltura()
    {
        return $this->altura;
    }

    /**
     * Set largura
     *
     * @param float $largura
     *
     * @return B2cViewProdutos
     */
    public function setLargura($largura)
    {
        $this->largura = $largura;

        return $this;
    }

    /**
     * Get largura
     *
     * @return float
     */
    public function getLargura()
    {
        return $this->largura;
    }

    /**
     * Set profundidade
     *
     * @param float $profundidade
     *
     * @return B2cViewProdutos
     */
    public function setProfundidade($profundidade)
    {
        $this->profundidade = $profundidade;

        return $this;
    }

    /**
     * Get profundidade
     *
     * @return float
     */
    public function getProfundidade()
    {
        return $this->profundidade;
    }

    /**
     * Set diametro
     *
     * @param float $diametro
     *
     * @return B2cViewProdutos
     */
    public function setDiametro($diametro)
    {
        $this->diametro = $diametro;

        return $this;
    }

    /**
     * Get diametro
     *
     * @return float
     */
    public function getDiametro()
    {
        return $this->diametro;
    }

    /**
     * Set subsidiarFrete
     *
     * @param boolean $subsidiarFrete
     *
     * @return B2cViewProdutos
     */
    public function setSubsidiarFrete($subsidiarFrete)
    {
        $this->subsidiarFrete = $subsidiarFrete;

        return $this;
    }

    /**
     * Get subsidiarFrete
     *
     * @return boolean
     */
    public function getSubsidiarFrete()
    {
        return $this->subsidiarFrete;
    }

    /**
     * Set negociacaoIni
     *
     * @param \DateTime $negociacaoIni
     *
     * @return B2cViewProdutos
     */
    public function setNegociacaoIni($negociacaoIni)
    {
        $this->negociacaoIni = $negociacaoIni;

        return $this;
    }

    /**
     * Get negociacaoIni
     *
     * @return \DateTime
     */
    public function getNegociacaoIni()
    {
        return $this->negociacaoIni;
    }

    /**
     * Set negociacaoFim
     *
     * @param \DateTime $negociacaoFim
     *
     * @return B2cViewProdutos
     */
    public function setNegociacaoFim($negociacaoFim)
    {
        $this->negociacaoFim = $negociacaoFim;

        return $this;
    }

    /**
     * Get negociacaoFim
     *
     * @return \DateTime
     */
    public function getNegociacaoFim()
    {
        return $this->negociacaoFim;
    }

    /**
     * Set saindoDeLinhaIni
     *
     * @param \DateTime $saindoDeLinhaIni
     *
     * @return B2cViewProdutos
     */
    public function setSaindoDeLinhaIni($saindoDeLinhaIni)
    {
        $this->saindoDeLinhaIni = $saindoDeLinhaIni;

        return $this;
    }

    /**
     * Get saindoDeLinhaIni
     *
     * @return \DateTime
     */
    public function getSaindoDeLinhaIni()
    {
        return $this->saindoDeLinhaIni;
    }

    /**
     * Set saindoDeLinhaFim
     *
     * @param \DateTime $saindoDeLinhaFim
     *
     * @return B2cViewProdutos
     */
    public function setSaindoDeLinhaFim($saindoDeLinhaFim)
    {
        $this->saindoDeLinhaFim = $saindoDeLinhaFim;

        return $this;
    }

    /**
     * Get saindoDeLinhaFim
     *
     * @return \DateTime
     */
    public function getSaindoDeLinhaFim()
    {
        return $this->saindoDeLinhaFim;
    }

    /**
     * Set tipoLst
     *
     * @param string $tipoLst
     *
     * @return B2cViewProdutos
     */
    public function setTipoLst($tipoLst)
    {
        $this->tipoLst = $tipoLst;

        return $this;
    }

    /**
     * Get tipoLst
     *
     * @return string
     */
    public function getTipoLst()
    {
        return $this->tipoLst;
    }

    /**
     * Set tooltipSaindoDeLinha
     *
     * @param string $tooltipSaindoDeLinha
     *
     * @return B2cViewProdutos
     */
    public function setTooltipSaindoDeLinha($tooltipSaindoDeLinha)
    {
        $this->tooltipSaindoDeLinha = $tooltipSaindoDeLinha;

        return $this;
    }

    /**
     * Get tooltipSaindoDeLinha
     *
     * @return string
     */
    public function getTooltipSaindoDeLinha()
    {
        return $this->tooltipSaindoDeLinha;
    }

    /**
     * Set tooltipNegociacao
     *
     * @param string $tooltipNegociacao
     *
     * @return B2cViewProdutos
     */
    public function setTooltipNegociacao($tooltipNegociacao)
    {
        $this->tooltipNegociacao = $tooltipNegociacao;

        return $this;
    }

    /**
     * Get tooltipNegociacao
     *
     * @return string
     */
    public function getTooltipNegociacao()
    {
        return $this->tooltipNegociacao;
    }

    /**
     * Set tooltipPromocao
     *
     * @param string $tooltipPromocao
     *
     * @return B2cViewProdutos
     */
    public function setTooltipPromocao($tooltipPromocao)
    {
        $this->tooltipPromocao = $tooltipPromocao;

        return $this;
    }

    /**
     * Get tooltipPromocao
     *
     * @return string
     */
    public function getTooltipPromocao()
    {
        return $this->tooltipPromocao;
    }

    /**
     * Set tooltipCor
     *
     * @param string $tooltipCor
     *
     * @return B2cViewProdutos
     */
    public function setTooltipCor($tooltipCor)
    {
        $this->tooltipCor = $tooltipCor;

        return $this;
    }

    /**
     * Get tooltipCor
     *
     * @return string
     */
    public function getTooltipCor()
    {
        return $this->tooltipCor;
    }

    /**
     * Set relogioPromocao
     *
     * @param boolean $relogioPromocao
     *
     * @return B2cViewProdutos
     */
    public function setRelogioPromocao($relogioPromocao)
    {
        $this->relogioPromocao = $relogioPromocao;

        return $this;
    }

    /**
     * Get relogioPromocao
     *
     * @return boolean
     */
    public function getRelogioPromocao()
    {
        return $this->relogioPromocao;
    }

    /**
     * Set ultimaVerificacaoPreco
     *
     * @param \DateTime $ultimaVerificacaoPreco
     *
     * @return B2cViewProdutos
     */
    public function setUltimaVerificacaoPreco($ultimaVerificacaoPreco)
    {
        $this->ultimaVerificacaoPreco = $ultimaVerificacaoPreco;

        return $this;
    }

    /**
     * Get ultimaVerificacaoPreco
     *
     * @return \DateTime
     */
    public function getUltimaVerificacaoPreco()
    {
        return $this->ultimaVerificacaoPreco;
    }
}
