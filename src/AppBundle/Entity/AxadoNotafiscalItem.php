<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AxadoNotafiscalItem
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="axado_notafiscal_item")
 * @ORM\Entity
 */
class AxadoNotafiscalItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPedped;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_filial", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idFilial;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_notafiscal", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idNotafiscal;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_item", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=true)
     */
    private $quantidade;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=4, nullable=true)
     */
    private $valor;

    /**
     * @var float
     *
     * @ORM\Column(name="comprimento", type="float", precision=10, scale=4, nullable=true)
     */
    private $comprimento;

    /**
     * @var float
     *
     * @ORM\Column(name="largura", type="float", precision=10, scale=4, nullable=true)
     */
    private $largura;

    /**
     * @var float
     *
     * @ORM\Column(name="peso", type="float", precision=10, scale=4, nullable=true)
     */
    private $peso;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data_sincronizado", type="datetime", nullable=true)
     */
    private $dataSincronizado;


    /**
     * @param $idPedped
     * @return $this
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * @param $idFilial
     * @return $this
     */
    public function setIdFilial($idFilial)
    {
        $this->idFilial = $idFilial;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdFilial()
    {
        return $this->idFilial;
    }

    /**
     * @param $idNotafiscal
     * @return $this
     */
    public function setIdNotafiscal($idNotafiscal)
    {
        $this->idNotafiscal = $idNotafiscal;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdNotafiscal()
    {
        return $this->idNotafiscal;
    }

    /**
     * @param $idItem
     * @return $this
     */
    public function setIdItem($idItem)
    {
        $this->idItem = $idItem;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdItem()
    {
        return $this->idItem;
    }

    /**
     * @param $quantidade
     * @return $this
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param $valor
     * @return $this
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param $comprimento
     * @return $this
     */
    public function setComprimento($comprimento)
    {
        $this->comprimento = $comprimento;
        return $this;
    }

    /**
     * @return float
     */
    public function getComprimento()
    {
        return $this->comprimento;
    }

    /**
     * @param $largura
     * @return $this
     */
    public function setLargura($largura)
    {
        $this->largura = $largura;
        return $this;
    }

    /**
     * @return float
     */
    public function getLargura()
    {
        return $this->largura;
    }

    /**
     * @param $peso
     * @return $this
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;
        return $this;
    }

    /**
     * @return float
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * @param DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $dataSincronizado
     * @return $this
     */
    public function setDataSincronizado(DateTime $dataSincronizado)
    {
        $this->dataSincronizado = $dataSincronizado;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataSincronizado()
    {
        return $this->dataSincronizado;
    }
}
