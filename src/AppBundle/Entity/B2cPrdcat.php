<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdcat
 *
 * @ORM\Table(name="b2c_prdcat", indexes={@ORM\Index(name="ordem", columns={"ordem", "nome"}), @ORM\Index(name="pai", columns={"pai"}), @ORM\Index(name="idx_cat", columns={"id", "nome"}), @ORM\Index(name="codigoerp", columns={"codigoerp"}), @ORM\Index(name="slug", columns={"slug"})})
 * @ORM\Entity
 */
class B2cPrdcat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $ordem;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visivel", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $visivel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="shopping", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $shopping;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $link;

    /**
     * @var integer
     *
     * @ORM\Column(name="pai", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $pai;

    /**
     * @var integer
     *
     * @ORM\Column(name="codigoerp", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $codigoerp;

    /**
     * @var string
     *
     * @ORM\Column(name="cadeia_completa", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $cadeiaCompleta;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_completo", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nomeCompleto;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=150, precision=0, scale=0, nullable=true, unique=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="slug_cat", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $slugCat;

    /**
     * @var string
     *
     * @ORM\Column(name="google_category_taxonomy", type="string", length=150, precision=0, scale=0, nullable=true, unique=false)
     */
    private $googleCategoryTaxonomy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prioridade", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $prioridade;

    /**
     * @var boolean
     *
     * @ORM\Column(name="categoria_kit", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $categoriaKit;

    /**
     * @var string
     *
     * @ORM\Column(name="pchaves", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $pchaves;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ordem
     *
     * @param integer $ordem
     *
     * @return B2cPrdcat
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * Get ordem
     *
     * @return integer
     */
    public function getOrdem()
    {
        return $this->ordem;
    }

    /**
     * Set visivel
     *
     * @param boolean $visivel
     *
     * @return B2cPrdcat
     */
    public function setVisivel($visivel)
    {
        $this->visivel = $visivel;

        return $this;
    }

    /**
     * Get visivel
     *
     * @return boolean
     */
    public function getVisivel()
    {
        return $this->visivel;
    }

    /**
     * Set shopping
     *
     * @param boolean $shopping
     *
     * @return B2cPrdcat
     */
    public function setShopping($shopping)
    {
        $this->shopping = $shopping;

        return $this;
    }

    /**
     * Get shopping
     *
     * @return boolean
     */
    public function getShopping()
    {
        return $this->shopping;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return B2cPrdcat
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return B2cPrdcat
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set pai
     *
     * @param integer $pai
     *
     * @return B2cPrdcat
     */
    public function setPai($pai)
    {
        $this->pai = $pai;

        return $this;
    }

    /**
     * Get pai
     *
     * @return integer
     */
    public function getPai()
    {
        return $this->pai;
    }

    /**
     * Set codigoerp
     *
     * @param integer $codigoerp
     *
     * @return B2cPrdcat
     */
    public function setCodigoerp($codigoerp)
    {
        $this->codigoerp = $codigoerp;

        return $this;
    }

    /**
     * Get codigoerp
     *
     * @return integer
     */
    public function getCodigoerp()
    {
        return $this->codigoerp;
    }

    /**
     * Set cadeiaCompleta
     *
     * @param string $cadeiaCompleta
     *
     * @return B2cPrdcat
     */
    public function setCadeiaCompleta($cadeiaCompleta)
    {
        $this->cadeiaCompleta = $cadeiaCompleta;

        return $this;
    }

    /**
     * Get cadeiaCompleta
     *
     * @return string
     */
    public function getCadeiaCompleta()
    {
        return $this->cadeiaCompleta;
    }

    /**
     * Set nomeCompleto
     *
     * @param string $nomeCompleto
     *
     * @return B2cPrdcat
     */
    public function setNomeCompleto($nomeCompleto)
    {
        $this->nomeCompleto = $nomeCompleto;

        return $this;
    }

    /**
     * Get nomeCompleto
     *
     * @return string
     */
    public function getNomeCompleto()
    {
        return $this->nomeCompleto;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return B2cPrdcat
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slugCat
     *
     * @param string $slugCat
     *
     * @return B2cPrdcat
     */
    public function setSlugCat($slugCat)
    {
        $this->slugCat = $slugCat;

        return $this;
    }

    /**
     * Get slugCat
     *
     * @return string
     */
    public function getSlugCat()
    {
        return $this->slugCat;
    }

    /**
     * Set googleCategoryTaxonomy
     *
     * @param string $googleCategoryTaxonomy
     *
     * @return B2cPrdcat
     */
    public function setGoogleCategoryTaxonomy($googleCategoryTaxonomy)
    {
        $this->googleCategoryTaxonomy = $googleCategoryTaxonomy;

        return $this;
    }

    /**
     * Get googleCategoryTaxonomy
     *
     * @return string
     */
    public function getGoogleCategoryTaxonomy()
    {
        return $this->googleCategoryTaxonomy;
    }

    /**
     * Set prioridade
     *
     * @param boolean $prioridade
     *
     * @return B2cPrdcat
     */
    public function setPrioridade($prioridade)
    {
        $this->prioridade = $prioridade;

        return $this;
    }

    /**
     * Get prioridade
     *
     * @return boolean
     */
    public function getPrioridade()
    {
        return $this->prioridade;
    }

    /**
     * Set categoriaKit
     *
     * @param boolean $categoriaKit
     *
     * @return B2cPrdcat
     */
    public function setCategoriaKit($categoriaKit)
    {
        $this->categoriaKit = $categoriaKit;

        return $this;
    }

    /**
     * Get categoriaKit
     *
     * @return boolean
     */
    public function getCategoriaKit()
    {
        return $this->categoriaKit;
    }

    /**
     * Set pchaves
     *
     * @param string $pchaves
     *
     * @return B2cPrdcat
     */
    public function setPchaves($pchaves)
    {
        $this->pchaves = $pchaves;

        return $this;
    }

    /**
     * Get pchaves
     *
     * @return string
     */
    public function getPchaves()
    {
        return $this->pchaves;
    }
}
