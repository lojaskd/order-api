<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPrdprd
 *
 * @ORM\Table(name="b2c_prdprd_marketplace")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPrdprdMarketplaceRepository")
 */
class B2cPrdprdMarketplace
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_marketplace", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMarketplace;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_prdprd", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPrdprd;

    /**
     * @var integer
     *
     * @ORM\Column(name="estoque", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $estoque;

    /**
     * @var float
     *
     * @ORM\Column(name="preco", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $preco;

    /**
     * @var integer
     *
     * @ORM\Column(name="acao", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $acao;

    /**
     * @var float
     *
     * @ORM\Column(name="preco_2", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $preco2;

    /**
     * @var float
     *
     * @ORM\Column(name="preco_3", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $preco3;

    /**
     * @var float
     *
     * @ORM\Column(name="preco_4", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $preco4;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_sync", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtaSync;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_cad", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtaCad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_alt", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtaAlt;


    /**
     * @return int
     */
    public function getIdMarketplace()
    {
        return $this->idMarketplace;
    }

    /**
     * @param int $idMarketplace
     */
    public function setIdMarketplace($idMarketplace)
    {
        $this->idMarketplace = $idMarketplace;
    }

    /**
     * @return int
     */
    public function getIdPrdprd()
    {
        return $this->idPrdprd;
    }

    /**
     * @param int $idPrdprd
     */
    public function setIdPrdprd($idPrdprd)
    {
        $this->idPrdprd = $idPrdprd;
    }

    /**
     * @return int
     */
    public function getEstoque()
    {
        return $this->estoque;
    }

    /**
     * @param int $estoque
     */
    public function setEstoque($estoque)
    {
        $this->estoque = $estoque;
    }

    /**
     * @return float
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param float $preco
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
    }

    /**
     * @return int
     */
    public function getAcao()
    {
        return $this->acao;
    }

    /**
     * @param int $acao
     */
    public function setAcao($acao)
    {
        $this->acao = $acao;
    }

    /**
     * @return float
     */
    public function getPreco2()
    {
        return $this->preco2;
    }

    /**
     * @param float $preco2
     */
    public function setPreco2($preco2)
    {
        $this->preco2 = $preco2;
    }

    /**
     * @return float
     */
    public function getPreco3()
    {
        return $this->preco3;
    }

    /**
     * @param float $preco3
     */
    public function setPreco3($preco3)
    {
        $this->preco3 = $preco3;
    }

    /**
     * @return float
     */
    public function getPreco4()
    {
        return $this->preco4;
    }

    /**
     * @param float $preco4
     */
    public function setPreco4($preco4)
    {
        $this->preco4 = $preco4;
    }

    /**
     * @return \DateTime
     */
    public function getDtaSync()
    {
        return $this->dtaSync;
    }

    /**
     * @param \DateTime $dtaSync
     */
    public function setDtaSync($dtaSync)
    {
        $this->dtaSync = $dtaSync;
    }

    /**
     * @return \DateTime
     */
    public function getDtaCad()
    {
        return $this->dtaCad;
    }

    /**
     * @param \DateTime $dtaCad
     */
    public function setDtaCad($dtaCad)
    {
        $this->dtaCad = $dtaCad;
    }

    /**
     * @return \DateTime
     */
    public function getDtaAlt()
    {
        return $this->dtaAlt;
    }

    /**
     * @param \DateTime $dtaAlt
     */
    public function setDtaAlt($dtaAlt)
    {
        $this->dtaAlt = $dtaAlt;
    }

}
