<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cContaContabilPagamento
 *
 * @ORM\Table(name="b2c_conta_contabil_pagamento", indexes={@ORM\Index(name="id", columns={"id_pagamento"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cContaContabilPagamentoRepository")
 */
class B2cContaContabilPagamento
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagamento", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;


    /**
     * @var integer
     *
     * @ORM\Column(name="id_conta", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idConta;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdConta()
    {
        return $this->idConta;
    }

    /**
     * @param int $idConta
     */
    public function setIdConta($idConta)
    {
        $this->idConta = $idConta;
    }


}
