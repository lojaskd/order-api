<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPedpedMarketplace
 *
 * @ORM\Table(name="b2c_pedped_marketplace", indexes={@ORM\Index(name="id_pedped", columns={"id_pedped"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPedpedMarketplaceRepository")
 */
class B2cPedpedMarketplace
{
   /**
     * @var integer
     *
     * @ORM\Column(name="id_marketplace", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idMarketplace;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPedped;

    /**
     * @var string
     *
     * @ORM\Column(name="cod_order", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $codOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_channel", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $idChannel;

    /**
     * @var integer
     *
     * @ORM\Column(name="ped_situacao", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $pedSituacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_cad", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtaCad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_entrega", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $deliveryDate;


    /**
     * @return int
     */
    public function getIdMarketplace()
    {
        return $this->idMarketplace;
    }

    /**
     * @param int $idMarketplace
     */
    public function setIdMarketplace($idMarketplace)
    {
        $this->idMarketplace = $idMarketplace;
    }

    /**
     * @return int
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * @param int $idPedped
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;
    }

    /**
     * @return string
     */
    public function getCodOrder()
    {
        return $this->codOrder;
    }

    /**
     * @param string $codOrder
     */
    public function setCodOrder($codOrder)
    {
        $this->codOrder = $codOrder;
    }

    /**
     * @return int
     */
    public function getIdChannel()
    {
        return $this->idChannel;
    }

    /**
     * @param int $idChannel
     */
    public function setIdChannel($idChannel)
    {
        $this->idChannel = $idChannel;
    }

    /**
     * @return int
     */
    public function getPedSituacao()
    {
        return $this->pedSituacao;
    }

    /**
     * @param int $pedSituacao
     */
    public function setPedSituacao($pedSituacao)
    {
        $this->pedSituacao = $pedSituacao;
    }

    /**
     * @return \DateTime
     */
    public function getDtaCad()
    {
        return $this->dtaCad;
    }

    /**
     * @param \DateTime $dtaCad
     */
    public function setDtaCad($dtaCad)
    {
        $this->dtaCad = $dtaCad;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

}