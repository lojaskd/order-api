<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class AttributesOptions
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="attributes_options")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttributesOptionsRepository")
 */
class AttributesOptions extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_attr", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idAttr;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=150, precision=0, scale=0, nullable=false, unique=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=200, precision=0, scale=0, nullable=false, unique=false)
     */
    private $value;

    /**
     * @return int
     */
    public function getIdAttr()
    {
        return $this->idAttr;
    }

    /**
     * @param int $idAttr
     * @return $this
     */
    public function setIdAttr($idAttr)
    {
        $this->idAttr = $idAttr;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
