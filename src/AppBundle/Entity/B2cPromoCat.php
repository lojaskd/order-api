<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoCat
 *
 * @ORM\Table(name="b2c_promo_cat", indexes={@ORM\Index(name="id_categoria", columns={"id_categoria", "id_promo"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPromoCatRepository")
 */
class B2cPromoCat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPromo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_categoria", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idCategoria;



    /**
     * Set idPromo
     *
     * @param integer $idPromo
     *
     * @return B2cPromoCat
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;

        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set idCategoria
     *
     * @param integer $idCategoria
     *
     * @return B2cPromoCat
     */
    public function setIdCategoria($idCategoria)
    {
        $this->idCategoria = $idCategoria;

        return $this;
    }

    /**
     * Get idCategoria
     *
     * @return integer
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }
}
