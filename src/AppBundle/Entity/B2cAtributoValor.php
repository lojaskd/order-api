<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cAtributoValor
 *
 * @ORM\Table(name="b2c_atributo_valor", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug"})}, indexes={@ORM\Index(name="b2c_prdprd", columns={"b2c_prdprd"}), @ORM\Index(name="id_atributo", columns={"id_atributo"}), @ORM\Index(name="respostas", columns={"respostas", "id_atributo"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cAtributoValorRepository")
 */
class B2cAtributoValor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_atributo_valore", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAtributoValore;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_atributo", type="smallint", nullable=false)
     */
    private $idAtributo = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="b2c_prdprd", type="integer", nullable=false)
     */
    private $b2cPrdprd = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="respostas", type="string", length=255, nullable=true)
     */
    private $respostas;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @param $idAtributoValore
     * @return $this
     */
    public function setIdAtributoValore($idAtributoValore)
    {
        $this->idAtributoValore = $idAtributoValore;
        return $this;
    }

    /**
     * Get idAtributoValore
     *
     * @return integer
     */
    public function getIdAtributoValore()
    {
        return $this->idAtributoValore;
    }

    /**
     * Set idAtributo
     *
     * @param integer $idAtributo
     *
     * @return B2cAtributoValor
     */
    public function setIdAtributo($idAtributo)
    {
        $this->idAtributo = $idAtributo;

        return $this;
    }

    /**
     * Get idAtributo
     *
     * @return integer
     */
    public function getIdAtributo()
    {
        return $this->idAtributo;
    }

    /**
     * Set b2cPrdprd
     *
     * @param integer $b2cPrdprd
     *
     * @return B2cAtributoValor
     */
    public function setB2cPrdprd($b2cPrdprd)
    {
        $this->b2cPrdprd = $b2cPrdprd;

        return $this;
    }

    /**
     * Get b2cPrdprd
     *
     * @return integer
     */
    public function getB2cPrdprd()
    {
        return $this->b2cPrdprd;
    }

    /**
     * Set respostas
     *
     * @param string $respostas
     *
     * @return B2cAtributoValor
     */
    public function setRespostas($respostas)
    {
        $this->respostas = $respostas;

        return $this;
    }

    /**
     * Get respostas
     *
     * @return string
     */
    public function getRespostas()
    {
        return $this->respostas;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return B2cAtributoValor
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
