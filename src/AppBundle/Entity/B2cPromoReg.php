<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoReg
 *
 * @ORM\Table(name="b2c_promo_reg", indexes={@ORM\Index(name="id_regiao", columns={"id_regiao"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPromoRegRepository")
 */
class B2cPromoReg
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPromo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_regiao", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idRegiao;



    /**
     * Set idPromo
     *
     * @param integer $idPromo
     *
     * @return B2cPromoReg
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;

        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set idRegiao
     *
     * @param integer $idRegiao
     *
     * @return B2cPromoReg
     */
    public function setIdRegiao($idRegiao)
    {
        $this->idRegiao = $idRegiao;

        return $this;
    }

    /**
     * Get idRegiao
     *
     * @return integer
     */
    public function getIdRegiao()
    {
        return $this->idRegiao;
    }
}
