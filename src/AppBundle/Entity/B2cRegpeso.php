<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cRegpeso
 *
 * @ORM\Table(name="b2c_regpeso", indexes={@ORM\Index(name="idx_peso", columns={"id", "ini_kg", "fim_kg"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cRegpesoRepository")
 */
class B2cRegpeso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_trans", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idTrans;

    /**
     * @var float
     *
     * @ORM\Column(name="ini_kg", type="float", precision=10, scale=5, nullable=false, unique=false)
     */
    private $iniKg;

    /**
     * @var float
     *
     * @ORM\Column(name="fim_kg", type="float", precision=10, scale=5, nullable=false, unique=false)
     */
    private $fimKg;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTrans
     *
     * @param integer $idTrans
     *
     * @return B2cRegpeso
     */
    public function setIdTrans($idTrans)
    {
        $this->idTrans = $idTrans;

        return $this;
    }

    /**
     * Get idTrans
     *
     * @return integer
     */
    public function getIdTrans()
    {
        return $this->idTrans;
    }

    /**
     * Set iniKg
     *
     * @param float $iniKg
     *
     * @return B2cRegpeso
     */
    public function setIniKg($iniKg)
    {
        $this->iniKg = $iniKg;

        return $this;
    }

    /**
     * Get iniKg
     *
     * @return float
     */
    public function getIniKg()
    {
        return $this->iniKg;
    }

    /**
     * Set fimKg
     *
     * @param float $fimKg
     *
     * @return B2cRegpeso
     */
    public function setFimKg($fimKg)
    {
        $this->fimKg = $fimKg;

        return $this;
    }

    /**
     * Get fimKg
     *
     * @return float
     */
    public function getFimKg()
    {
        return $this->fimKg;
    }
}
