<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoBri
 *
 * @ORM\Table(name="b2c_promo_bri")
 * @ORM\Entity
 */
class B2cPromoBri
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPromo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_brinde", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idBrinde;



    /**
     * Set idPromo
     *
     * @param integer $idPromo
     *
     * @return B2cPromoBri
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;

        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set idBrinde
     *
     * @param integer $idBrinde
     *
     * @return B2cPromoBri
     */
    public function setIdBrinde($idBrinde)
    {
        $this->idBrinde = $idBrinde;

        return $this;
    }

    /**
     * Get idBrinde
     *
     * @return integer
     */
    public function getIdBrinde()
    {
        return $this->idBrinde;
    }
}
