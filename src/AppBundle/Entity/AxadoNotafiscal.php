<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AxadoNotafiscal
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="axado_notafiscal")
 * @ORM\Entity
 */
class AxadoNotafiscal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedped", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPedped;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_filial", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idFilial;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_notafiscal", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idNotafiscal;

    /**
     * @var integer
     *
     * @ORM\Column(name="serie_notafiscal", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $serieNotafiscal;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_notafiscal", type="float", precision=10, scale=4, nullable=true)
     */
    private $valorNotafiscal;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_icms", type="float", precision=10, scale=4, nullable=true)
     */
    private $valorIcms;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_frete", type="float", precision=10, scale=4, nullable=true)
     */
    private $valorFrete;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data_modificado", type="datetime", nullable=true)
     */
    private $dataModificado;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data_sincronizado", type="datetime", nullable=true)
     */
    private $dataSincronizado;

    /**
     * @var string
     *
     * @ORM\Column(name="token_axado", type="string", length=45, nullable=true)
     */
    private $tokenAxado;


    /**
     * @param $idPedped
     * @return $this
     */
    public function setIdPedped($idPedped)
    {
        $this->idPedped = $idPedped;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdPedped()
    {
        return $this->idPedped;
    }

    /**
     * @param $idFilial
     * @return $this
     */
    public function setIdFilial($idFilial)
    {
        $this->idFilial = $idFilial;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdFilial()
    {
        return $this->idFilial;
    }

    /**
     * @param $idNotafiscal
     * @return $this
     */
    public function setIdNotafiscal($idNotafiscal)
    {
        $this->idNotafiscal = $idNotafiscal;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdNotafiscal()
    {
        return $this->idNotafiscal;
    }

    /**
     * @param $serieNotafiscal
     * @return $this
     */
    public function setSerieNotafiscal($serieNotafiscal)
    {
        $this->serieNotafiscal = $serieNotafiscal;
        return $this;
    }

    /**
     * @return int
     */
    public function getSerieNotafiscal()
    {
        return $this->serieNotafiscal;
    }

    /**
     * @param $valorNotafiscal
     * @return $this
     */
    public function setValorNotafiscal($valorNotafiscal)
    {
        $this->valorNotafiscal = $valorNotafiscal;
        return $this;
    }

    /**
     * @return float
     */
    public function getValorNotafiscal()
    {
        return $this->valorNotafiscal;
    }

    /**
     * @param $valorIcms
     * @return $this
     */
    public function setValorIcms($valorIcms)
    {
        $this->valorIcms = $valorIcms;
        return $this;
    }

    /**
     * @return float
     */
    public function getValorIcms()
    {
        return $this->valorIcms;
    }

    /**
     * @param $valorFrete
     * @return $this
     */
    public function setValorFrete($valorFrete)
    {
        $this->valorFrete = $valorFrete;
        return $this;
    }

    /**
     * @return float
     */
    public function getValorFrete()
    {
        return $this->valorFrete;
    }

    /**
     * @param $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $dataModificado
     * @return $this
     */
    public function setDataModificado(DateTime $dataModificado)
    {
        $this->dataModificado = $dataModificado;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataModificado()
    {
        return $this->dataModificado;
    }

    /**
     * @param DateTime $dataSincronizado
     * @return $this
     */
    public function setDataSincronizado(DateTime $dataSincronizado)
    {
        $this->dataSincronizado = $dataSincronizado;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDataSincronizado()
    {
        return $this->dataSincronizado;
    }

    /**
     * @param $tokenAxado
     * @return $this
     */
    public function setTokenAxado($tokenAxado)
    {
        $this->tokenAxado = $tokenAxado;
        return $this;
    }

    /**
     * @return string
     */
    public function getTokenAxado()
    {
        return $this->tokenAxado;
    }
}
