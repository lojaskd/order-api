<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cRegtransVal
 *
 * @ORM\Table(name="b2c_regtrans_val", indexes={@ORM\Index(name="id_kg", columns={"id_kg"}), @ORM\Index(name="id_mc", columns={"id_mc"}), @ORM\Index(name="id_trans", columns={"id_trans"})})
 * @ORM\Entity
 */
class B2cRegtransVal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_kg", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idKg;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_mc", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $idMc;

    /**
     * @var \AppBundle\Entity\B2cTratra
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\B2cTratra")
     * @ORM\JoinColumn(name="id_trans", referencedColumnName="id_tratra", nullable=true)
     */
    private $idTrans;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=2, precision=0, scale=0, nullable=false, unique=false)
     */
    private $tipo;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $valor;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idKg
     *
     * @param integer $idKg
     *
     * @return B2cRegtransVal
     */
    public function setIdKg($idKg)
    {
        $this->idKg = $idKg;

        return $this;
    }

    /**
     * Get idKg
     *
     * @return integer
     */
    public function getIdKg()
    {
        return $this->idKg;
    }

    /**
     * Set idMc
     *
     * @param integer $idMc
     *
     * @return B2cRegtransVal
     */
    public function setIdMc($idMc)
    {
        $this->idMc = $idMc;

        return $this;
    }

    /**
     * Get idMc
     *
     * @return integer
     */
    public function getIdMc()
    {
        return $this->idMc;
    }

    /**
     * Set idTrans
     *
     * @param integer $idTrans
     *
     * @return B2cRegtransVal
     */
    public function setIdTrans($idTrans)
    {
        $this->idTrans = $idTrans;

        return $this;
    }

    /**
     * Get idTrans
     *
     * @return integer
     */
    public function getIdTrans()
    {
        return $this->idTrans;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return B2cRegtransVal
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return B2cRegtransVal
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }
}
