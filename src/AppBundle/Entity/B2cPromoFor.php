<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cPromoFor
 *
 * @ORM\Table(name="b2c_promo_for", indexes={@ORM\Index(name="id_fornecedor", columns={"id_fornecedor", "id_promo"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\B2cPromoForRepository")
 */
class B2cPromoFor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_promo", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPromo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_fornecedor", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idFornecedor;



    /**
     * Set idPromo
     *
     * @param integer $idPromo
     *
     * @return B2cPromoFor
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;

        return $this;
    }

    /**
     * Get idPromo
     *
     * @return integer
     */
    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * Set idFornecedor
     *
     * @param integer $idFornecedor
     *
     * @return B2cPromoFor
     */
    public function setIdFornecedor($idFornecedor)
    {
        $this->idFornecedor = $idFornecedor;

        return $this;
    }

    /**
     * Get idFornecedor
     *
     * @return integer
     */
    public function getIdFornecedor()
    {
        return $this->idFornecedor;
    }
}
