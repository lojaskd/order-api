<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * B2cMarketplaceChannel
 *
 * @ORM\Table(name="b2c_marketplace_channel", indexes={@ORM\Index(name="id", columns={"id"})})
 * @ORM\Entity
 */
class B2cMarketplaceChannel
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_channel", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_marketplace", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $marketplaceId;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     */
    private $nome;


    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dta_cad", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dtaCad;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketplaceId()
    {
        return $this->marketplaceId;
    }

    /**
     * @param int $marketplaceId
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->marketplaceId = $marketplaceId;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return \DateTime
     */
    public function getDtaCad()
    {
        return $this->dtaCad;
    }

    /**
     * @param \DateTime $dtaCad
     */
    public function setDtaCad($dtaCad)
    {
        $this->dtaCad = $dtaCad;
    }

}
