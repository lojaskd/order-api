<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class OrderReportController
 * @package AppBundle\Controller
 */
class OrderReportController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get order report by type",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/report")
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function getOrdersReportAction(Request $request)
    {
        $startTime = microtime(true);
        $orderReport = array();

        try {
            if ($request->query->get('type', null) && $request->query->get('customer_id', null) && $request->query->get('type') == 'revenue') {
                $orderReport = $this->getDoctrine()->getManager()->getRepository('AppBundle:B2cPedped')->getRevenueReportByCustomer($request->query->get('customer_id'));
            }
        } catch (Exception $e) {
            $this->get('logger')->error('API-PEDIDOS GET REPORT - Error generating report: ' . $e->getMessage());
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }

        $response = new Response(json_encode($orderReport, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

        $this->get('logger')->info('API-PEDIDOS GET REPORT - RESPONSE TIME = ' . (microtime(true) - $startTime));

        return $response;
    }
}
