<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Validate\Create\CancelOrderMessage;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class QueueController
 * @package AppBundle\Controller
 */
class QueueController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Publish order message on type queue.",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="type",
     *          "dataType"="string",
     *          "requirement"="\w+",
     *          "description"="type name"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\POST("/queue/{type}/message")
     * @param string $type
     * @param Request $request
     * @return JsonResponse|Response
     *
     */
    public function postOrderMessageAction($type, Request $request)
    {
        try {
            $this->get('logger')->info('[POST] Order Message - Request Json: ' . serialize($request->getContent()));

            $serializer = SerializerBuilder::create()->build();
            $shipment = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Validate\Create\OrderMessage', 'json');
            $validator = $this->get('validator');
            $errors = $validator->validate($shipment);

            if (count($errors) == 0) {
                switch ($type) {
                    case 'shipment':
                        $this->get('queue_service')->publishOrderShipmentMsg(json_decode($request->getContent(), true));
                        break;
                    case 'delivery':
                        $this->get('queue_service')->publishDeliveredOrderMsg(json_decode($request->getContent(), true));
                        break;
                    default:
                        break;
                }
                $this->get('logger')->info('[POST] Order Message - Message successfully sent.');
                return new JsonResponse(array('code' => 200, 'status' => 'success'), 200);
            }

            $errorsMessage = array();
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            return new JsonResponse(array('code' => 500, 'status' => 'error', 'message' => 'Error on input json', 'errors' => $errorsMessage), 500);
        } catch (\Exception $e) {
            $this->get('logger')->error('Error publishing queue message: ' . $e->getMessage());
            return new JsonResponse(array('code' => 500, 'status' => 'error', 'message' => $e->getMessage()), 500);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Publish message on cancel order queue.",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="type",
     *          "dataType"="string",
     *          "requirement"="\w+",
     *          "description"="type name"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\POST("/queue/orders/cancel/message")
     * @param Request $request
     * @return JsonResponse|Response
     *
     */
    public function postCancelOrderMessageAction(Request $request)
    {
        try {
            $this->get('logger')->info('[POST] Cancel Order Message - Request Json: ' . serialize($request->getContent()));

            $serializer = SerializerBuilder::create()->build();
            /* @var CancelOrderMessage $message*/
            $message = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Validate\Create\CancelOrderMessage', 'json');
            $validator = $this->get('validator');
            $errors = $validator->validate($message);

            if (count($errors) == 0) {
                switch (strtolower($message->getType())) {
                    case 'sap':
                        $this->get('queue_service')->publishCancelSAPOrderMsg(json_decode($request->getContent(), true));
                        break;
                    case 'dropshipping':
                        $this->get('queue_service')->publishCancelDropshippingMsg(json_decode($request->getContent(), true));
                        break;
                    default:
                        $this->get('queue_service')->publishCancelOrderMsg(json_decode($request->getContent(), true));
                        break;
                }
                $this->get('logger')->info('[POST] Cancel Order Message - Message successfully sent.');
                return new JsonResponse(array('code' => 200, 'status' => 'success'), 200);
            }

            $errorsMessage = array();
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            return new JsonResponse(array('code' => 500, 'status' => 'error', 'message' => 'Error on input json', 'errors' => $errorsMessage), 500);
        } catch (\Exception $e) {
            $this->get('logger')->error('Error publishing queue message: ' . $e->getMessage());
            return new JsonResponse(array('code' => 500, 'status' => 'error', 'message' => $e->getMessage()), 500);
        }
    }
}
