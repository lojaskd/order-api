<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Entity\Validate\Update\UpdateNewDeliveryDate;
use AppBundle\Entity\VO\Update\UpdateNewDeliveryDateVO;
use AppBundle\Entity\B2cPedstatus;

/**
 * Class OrderController
 *
 * @package AppBundle\Controller
 */
class OrderController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get an order from id.",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/pedido/{orderId}", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     *
     * @return JsonResponse|Response
     */
    public function getPedidoAction($orderId)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            // Search the order
            $order = $entityManager->getRepository('AppBundle:B2cPedped')->getOrder($orderId);

            if ($order) {
                $productsInfo = array();
                // Loop into order products
                foreach ($order['products'] as $product) {
                    // Get product order status
                    $pedprdstatus = $entityManager->getRepository('AppBundle:B2cPedprdstatus')->getProductOrderStatus(
                        $orderId,
                        $product['idPrdprd']
                    );

                    // Get order product status
                    $status = $entityManager->getRepository('AppBundle:B2cPedprd')->getStatus($product, $pedprdstatus);

                    // Get production date to product
                    $production = $entityManager->getRepository('AppBundle:B2cPedprd')->getProduction($pedprdstatus);

                    // Get all product invoices
                    $invoices = $entityManager->getRepository('AppBundle:B2cNotafiscal')
                        ->getInvoices($orderId, $product['prdReferencia']);

                    // Get Shipper info
                    $shipper = $entityManager->getRepository('AppBundle:B2cNotafiscalItem')
                        ->getShipper($orderId, $product['prdReferencia']);

                    // Get polo
                    $polo = $entityManager->getRepository('AppBundle:B2cForfor')->getPolo($product['prdFornecedor']);

                    // Get Branch Office Information
                    $branchOfficeInfo = $entityManager->getRepository('AppBundle:B2cPedprd')
                        ->getBranchOfficeInformation(
                            $orderId,
                            $product['idPrdprd']
                        );

                    // Format product info to response
                    $pInfo = $entityManager->getRepository('AppBundle:B2cPedprd')->formatProductInfo(
                        $order,
                        $product,
                        $pedprdstatus,
                        $status,
                        $production,
                        $polo,
                        $branchOfficeInfo,
                        $shipper,
                        $invoices
                    );

                    $pInfo['pdf_notafiscal'] = '';
                    if (!empty($shipper['pathPdf'])) {
                        $pInfo['pdf_notafiscal'] = $this->get('aws_service')->getS3AuthenticatedUrl($shipper['pathPdf']);
                    }

                    array_push($productsInfo, $pInfo);
                }

                // Get marketplace info
                $marketPlaceInfo = array();
                if ($order['tipoPedido'] == 'marketplace') {
                    $marketPlaceInfo = $entityManager->getRepository('AppBundle:B2cPedped')
                        ->getMarketPlaceOrder($orderId);
                }

                // Get order status
                list($order['situationName'], $order['internalStatusName']) = $entityManager
                    ->getRepository('AppBundle:B2cPedped')->getSituation($orderId);

                // Format order info to response
                $orderInfo = $entityManager->getRepository('AppBundle:B2cPedped')
                    ->formatOrderInfo($order, $marketPlaceInfo);
                $orderInfo['produtos'] = $productsInfo;

                // Return the order in json format
                $response = new Response(json_encode(
                    $orderInfo,
                    JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
                ));
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

                return $response;
            }
        } catch (\Exception $e) {
            $this->get('logger')->error('Error to search order: '.$e->getMessage());

            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }

        return new JsonResponse(array('code' => 204), 204);
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get order data by ID",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/{orderId}", requirements={"orderId"="\d+"})
     * @param $orderId
     *
     * @return JsonResponse|Response
     */
    public function getOrderAction($orderId)
    {
        $startTime = microtime(true);
        $order = array();

        try {
            if ($orderId) {
                // If the order ID was passed, use it to get order info
                $order = $this->get('order_service')->getOrderInfo($orderId);
            }

            // Return the order in json format
            $response = new Response(
                json_encode($order, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
            );
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

            $this->get('logger')->info('API-PEDIDOS GET ORDER - RESPONSE TIME = '.(microtime(true) - $startTime));

            return $response;
        } catch (Exception $e) {
            $this->get('logger')->error('API-PEDIDOS GET ORDER - Error searching order(s): '.$e->getMessage());

            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get order data from search term or customer ID",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders")
     * @param Request $request
     *
     * @return JsonResponse|Response
     */
    public function getOrdersAction(Request $request)
    {
        $startTime = microtime(true);
        $orders = array();

        try {
            if ($request->query->get('term', null) && $request->query->get('type', null)) {
                // If a term and a type were passed, search orders from it
                $orders = $this->get('search_service')
                    ->searchOrderFromTermAndType(
                        $request->query->get('term', null),
                        $request->query->get('type', null)
                    );
            } elseif ($customerId = $request->query->get('customer', null)) {
                // Get orders by customer ID
                $orders = $this->getDoctrine()->getManager()->getRepository('AppBundle:B2cPedped')
                    ->getOrdersByCustomer($customerId);
            } elseif ($request->query->get('limit', null)) {
                $orders = $this->getDoctrine()->getManager()->getRepository('AppBundle:B2cPedped')
                    ->getOrders($request->query->get('limit', null));
            }
        } catch (Exception $e) {
            $this->get('logger')->error('API-PEDIDOS SEARCH - Error searching orders: '.$e->getMessage());

            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }

        $response = new Response(
            json_encode($orders, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
        );
        $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

        $this->get('logger')->info('API-PEDIDOS SEARCH - RESPONSE TIME = '.(microtime(true) - $startTime));

        return $response;
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new order from the submitted data.",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\ValidateOrder",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Post("/orders")
     * @param Request $request
     *
     * @return JsonResponse|Response
     */
    public function postOrderAction(Request $request)
    {
        $startTime = microtime(true);

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();
        $order = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Validate\ValidateOrder', 'json');
        $errorsMessage = $generateOrderVO['errors'] = array();

        $this->get('logger')->info('API-PEDIDOS CALLED - REQUEST JSON: '.serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($order);

        $orderService = $this->get('order_service');

        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')->info('API-PEDIDOS - Request Json has no errors, creating .');
            // Generate common info to all the order types
            $orderVO = $orderService->getCommonCreateOrderInfo($order);
            // Get the order data (shipping, coupon, value) according to the order type (marketplace, assistance, ...)
            $generateOrderVO = $orderService->generateOrder($orderVO, $order);
        } else {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }
        }

        // Generate an unique array of errors to return on response
        $errorsMessage = array_merge($errorsMessage, $generateOrderVO['errors']);

        // If there're no errors till the process,
        // It will save the order and products order, reduce stock, send customer email, ...
        if (!empty($errorsMessage)) {
            $this->get('logger')->error('API-PEDIDOS - Request JSON has errors: '.serialize($errorsMessage));
            $this->get('logger')->critical('API-PEDIDOS - Error validating JSON input: '.serialize($errorsMessage));

            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }

        // If there're errors, return them on response
        try {
            $this->get('logger')->info('API-PEDIDOS - Calling to save the order according to the type...');
            $newOrder = $orderService->insertOrder($generateOrderVO['orderVO']);
        } catch (Exception $e) {
            $this->get('logger')->error('API-PEDIDOS - Error saving the order: '.$e->getMessage());
            $this->get('logger')->critical('API-PEDIDOS - Error saving the order: '.$e->getMessage());

            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }

        $this->get('logger')->info('API-PEDIDOS - ORDER CREATED SUCCESSFULLY: '.$newOrder);
        // Return the order ID
        $response = new Response(
            json_encode($newOrder, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
        );
        $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

        $this->get('logger')->info('API-PEDIDOS - RESPONSE TIME = '.(microtime(true) - $startTime));

        return $response;
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update Status",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\ValidateOrderStatus",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put("/orders/{orderId}/status", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param Request $request
     *
     * @return JsonResponse|Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function putOrderStatusAction($orderId, Request $request)
    {
        $this->get('logger')->info('API-PEDIDOS STATUS CALLED - REQUEST JSON: '.serialize($request->getContent()));

        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        $orderStatus = $serializer->deserialize(
            $request->getContent(),
            'AppBundle\Entity\Validate\ValidateOrderStatus',
            'json'
        );

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($orderStatus);

        // If there're no errors in request data
        if (count($errors) > 0) {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            $this->get('logger')->error('API-PEDIDOS STATUS - Request Json has errors: '.serialize($errorsMessage));

            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }

        $orderService = $this->get('order_service');
        $this->get('logger')->info('API-PEDIDOS STATUS - Request Json has no errors, updating order status.');

        try {
            // Generate common info to all the order types
            $orderStatus->setOrderId($orderId);
            $updateOrderStatusVO = $orderService->getCommonOrderStatusInfo($orderStatus);
            $orderVO = $orderService->getCommonOrderInfo($orderId);

            $shippingService = $this->get('shipping_service');
            if (empty($orderVO->getShipping()->getName()) &&
                !empty($orderVO->getMarketplaceId()) &&
                $updateOrderStatusVO->getStatus() != B2cPedstatus::CANCELLED
            ) {
                $shippingVO = $shippingService->calculateShipping($orderVO);
                if (!$shippingVO->getId()) {
                    throw new Exception('Pedido sem frete definido.', 428);
                }

                //update shipping
                $this->getDoctrine()->getManager()->getRepository('AppBundle:B2cPedped')->updateOrderShipping($orderVO, $shippingVO);

                $orderVO = $orderService->getCommonOrderInfo($orderId);
            }
            $orderService->updateStatus($updateOrderStatusVO, $orderVO);

            $this->get('logger')->info('API-PEDIDOS STATUS - ORDER STATUS UPDATED SUCCESSFULLY: '.$orderId);

            // Return the order ID
            $response = new Response(
                json_encode($orderId, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
            );
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

            $this->get('logger')->info('API-PEDIDOS STATUS - RESPONSE TIME = '.(microtime(true) - $startTime));

            return $response;
        } catch (Exception $e) {
            $this->get('logger')->error('API-PEDIDOS STATUS - Error saving the order: '.$e->getMessage());
            $code = 400;
            if ($e->getCode()) {
                $code = $e->getCode();
            }

            return new JsonResponse(array('code' => $code, 'errors' => array($e->getMessage())), $code);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Create Purchase Order",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\ValidatePurchaseOrder",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Post("/orders/{orderId}/purchaseOrder", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param Request $request
     *
     * @return JsonResponse|Response
     */
    public function postPurchaseOrderAction($orderId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        $purchaseOrder = $serializer->deserialize(
            $request->getContent(),
            'AppBundle\Entity\Validate\ValidatePurchaseOrder',
            'json'
        );

        $this->get('logger')
            ->info('API-PEDIDOS PURCHASE ORDER CALLED - REQUEST JSON: '.serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($purchaseOrder);

        $orderService = $this->get('order_service');

        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')
                ->info('API-PEDIDOS PURCHASE ORDER - Request Json has no errors, updating order status.');

            try {
                $purchaseOrder->setOrderId($orderId);
                // Generate common info to all the order types
                $createPurchaseOrderVO = $orderService->getCommonPurchaseOrderInfo($purchaseOrder);
                $orderVO = $orderService->getCommonOrderInfo($orderId);
                $orderService->createPurchaseOrder($createPurchaseOrderVO, $orderVO);

                $this->get('logger')
                    ->info('API-PEDIDOS PURCHASE ORDER - PURCHASE ORDER UPDATED SUCCESSFULLY: '.$orderId);

                // Return the order ID
                $response = new Response(
                    json_encode($orderId, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
                );
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

                $this->get('logger')
                    ->info('API-PEDIDOS PURCHASE ORDER - RESPONSE TIME = '.(microtime(true) - $startTime));

                return $response;
            } catch (Exception $e) {
                $this->get('logger')->error('API-PEDIDOS PURCHASE ORDER - Error saving the order: '.$e->getMessage());

                return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
            }
        } else {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            $this->get('logger')
                ->error('API-PEDIDOS PURCHASE ORDER - Request Json has errors: '.serialize($errorsMessage));

            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update new delivery date",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\Update\UpdateNewDeliveryDate",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put("/orders/{orderId}/dates/delivery", requirements={"orderId"="\d+"})
     * @param $orderId
     * @param Request $request
     *
     * @return JsonResponse|Response
     */
    public function putNewDeliveryDateAction($orderId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        $this->get('logger')
            ->info('API-PEDIDOS (SET NEW DELIVERY DATE) CALLED - REQUEST JSON: '.serialize($request->getContent()));

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        /* @var $deliveryDate UpdateNewDeliveryDate */
        $deliveryDate = $serializer->deserialize(
            $request->getContent(),
            'AppBundle\Entity\Validate\Update\UpdateNewDeliveryDate',
            'json'
        );

        $this->get('logger')
            ->info('API-PEDIDOS (SET NEW DELIVERY DATE) CALLED - REQUEST JSON: '.serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($deliveryDate);

        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')
                ->info('API-PEDIDOS (SET NEW DELIVERY DATE) - Request Json has no errors, updating new delivery date.');

            try {
                // Create VO to update the new delivery date
                $newDeliveryDateVO = new UpdateNewDeliveryDateVO();
                $newDeliveryDateVO->setOrderId($orderId);
                $newDeliveryDateVO->setAccept($deliveryDate->isAccept());
                $newDeliveryDateVO->setNewDeliveryDate($deliveryDate->getNewDeliveryDate());
                $newDeliveryDateVO->setUser($deliveryDate->getUser());

                // Update new delivery date on b2c_pedped
                $this->get('order_service')->updateNewDeliveryDate($newDeliveryDateVO);

                $this->get('logger')
                    ->info('API-PEDIDOS (SET NEW DELIVERY DATE) - NEW DELIVERY DATE UPDATED SUCCESSFULLY: '.$orderId);

                // Return the order ID
                $response = new Response(json_encode(
                    $orderId,
                    JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
                ));
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

                $this->get('logger')
                    ->info('API-PEDIDOS (SET NEW DELIVERY DATE) - RESPONSE TIME = '.(microtime(true) - $startTime));

                return $response;
            } catch (Exception $e) {
                $this->get('logger')
                    ->error('API-PEDIDOS (SET NEW DELIVERY DATE) - Error saving the order: '.$e->getMessage());

                return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
            }
        }

        // If there're errors, save them and send into response
        foreach ($errors as $error) {
            $errorsMessage[] = $error->getMessage();
        }

        $this->get('logger')
            ->error('API-PEDIDOS (SET NEW DELIVERY DATE) - Request Json has errors: '.serialize($errorsMessage));

        return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
    }
}
