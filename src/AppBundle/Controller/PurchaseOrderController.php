<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Entity\VO\Update\UpdatePurchaseOrderVO;
use AppBundle\Entity\Validate\Update\UpdateValidatePurchaseOrder;

/**
 * Class PurchaseOrderController
 * @package AppBundle\Controller
 */
class PurchaseOrderController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get product info from purchase order and product reference on base64 encode",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/purchase/{purchaseOrder}/references/{productReference}")
     * @param string $purchaseOrder
     * @param string $productReference
     * @return JsonResponse|Response
     */
    public function getProductFromPurchaseAndReferenceAction($purchaseOrder, $productReference)
    {
        $productReference = base64_decode($productReference);

        try {
            $product = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:B2cPedprd')
                ->findOneBy(array('ordemCompra' => $purchaseOrder, 'prdReferencia' => $productReference));

            $serializer = SerializerBuilder::create()->build();
            // Return the order in json format
            $response = new Response(
                $serializer->serialize(array('code' => 200, 'status' => 'success', 'data' => $product), 'json', SerializationContext::create()->setSerializeNull(true))
            );
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

            return $response;
        } catch (Exception $e) {
            $this->get('logger')->error('[GET] PurchaseOrderController::getProductFromPurchaseAndReferenceAction: ' . $e->getMessage());
            return new JsonResponse(array('code' => 500, 'errors' => array($e->getMessage())), 500);
        }
    }


    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update Order Product From Purchase",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\Update\UpdateValidatePurchaseOrder",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="purchaseOrder",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put("/purchase/{purchaseOrder}", requirements={"purchaseOrder"="\d+"}, defaults={"purchaseOrder" = 0})
     * @param integer $purchaseOrder
     * @param Request $request
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function putOrderProductFromPurchaseAction($purchaseOrder, Request $request)
    {
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        /** @var $validate UpdateValidatePurchaseOrder */
        $validate = $serializer->deserialize(
            $request->getContent(),
            'AppBundle\Entity\Validate\Update\UpdateValidatePurchaseOrder',
            'json'
        );

        //set params
        $validate->setPurchaseOrder((int) $purchaseOrder);

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($validate);

        // If there're no errors in request data
        if (count($errors) > 0) {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }
            $this->get('logger')->error('API-PEDIDOS UPDATE PURCHASE ORDER - Request Json has errors: ' . serialize($errorsMessage));
            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }

        try {
            /** @var $updateProductDateVO UpdatePurchaseOrderVO*/
            $updateProductDateVO = new UpdatePurchaseOrderVO();
            $updateProductDateVO->setPurchaseOrder($validate->getPurchaseOrder());
            $updateProductDateVO->setOrders($validate->getOrders());
            $updateProductDateVO->setPathXml($validate->getPathXml());

            $this->get('purchase_order_service')->updatePurchaseOrder($updateProductDateVO);

            return new JsonResponse(array('code' => 200, 'status' => 'success', 'data' => $purchaseOrder), 200);

        } catch (Exception $e) {
            $this->get('logger')->error('API-PEDIDOS PRODUCTS DATE - Error saving the product date: ' . $e->getMessage());
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }

    }
}
