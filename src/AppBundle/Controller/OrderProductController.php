<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Validate\Update\UpdateValidateProductStatusProduct;
use AppBundle\Entity\Validate\Update\ValidateProviderBillingDate;
use AppBundle\Entity\Validate\ValidateProductDateProduct;
use AppBundle\Service\ProductService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OrderProductController
 * @package AppBundle\Controller
 */
class OrderProductController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get products from order.",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/{orderId}/products", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @return JsonResponse|Response
     *
     */
    public function getOrderProductsAction($orderId)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();

            // Search the order
            $products = $entityManager->getRepository('AppBundle:B2cPedprd')->findBy(
                array(
                    'idPedped' => $orderId
                )
            );
        } catch (\Exception $e) {
            $this->get('logger')->error('Error to search order: ' . $e->getMessage());
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }

        if ($products) {
            $serializer = SerializerBuilder::create()->build();
            // Return the order in json format
            $response = new Response(
                $serializer->serialize($products, 'json', SerializationContext::create()->setSerializeNull(true))
            );
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

            return $response;
        }

        return new JsonResponse(array('code' => 204, 'errors' => array('Not found')), 204);
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get an specific product from order.",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="productId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="product id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get(
     *     "/orders/{orderId}/products/{productId}",
     *     requirements={"orderId"="\d+", "productId"="\d+"},
     *     defaults={"orderId" = 0, "productId = 0"}
     * )
     * @param $orderId
     * @param $productId
     * @return JsonResponse
     */
    public function getOrderProductAction($orderId, $productId)
    {
        try {
            $productInfo = $this->get('product_service')->getProductOrderInfo($orderId, $productId);

            if (empty($productInfo)) {
                return new JsonResponse(array('code' => 204, 'errors' => array('Not found')), 204);
            }

            return new JsonResponse($productInfo, 200);
        } catch (\Exception $e) {
            $this->get('logger')->error('Error to search order: ' . $e->getMessage());
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get an specific product reference info from order.",
     *   input = "integer",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/{orderId}/references/{productReference}")
     * @param integer $orderId
     * @param string $productReference
     * @return JsonResponse|Response
     */
    public function getOrderProductReferenceAction($orderId, $productReference)
    {
        try {
            $productReference = base64_decode($productReference);

            $product = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:B2cPedprd')
                ->getOrderProductFromReference($orderId, $productReference);

            $serializer = SerializerBuilder::create()->build();

            if (!$product) {
                $response = new Response(
                    $serializer->serialize(
                        array('code' => 204, 'message' => 'success'),
                        'json'
                    )
                );
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
                return $response;
            }

            $productInfo = $serializer->toArray($product[0], SerializationContext::create()->setSerializeNull(true));
            $productInfo['status'] = $serializer->toArray($product[1], SerializationContext::create()->setSerializeNull(true));

            // Return the order in json format
            $response = new Response(
                $serializer->serialize(
                    array('code' => 200, 'message' => 'success', 'data' => $productInfo),
                    'json',
                    SerializationContext::create()->setSerializeNull(true)
                )
            );
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
            return $response;
        } catch (\Exception $e) {
            $this->get('logger')->error('Error to search order from product reference: ' . $e->getMessage());
            return new JsonResponse(array('code' => 500, 'message' => 'error', 'errors' => array($e->getMessage())), 500);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update Product Dates",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\ValidateProductDate",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="productId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="product id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     204 = "No content",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put("/orders/{orderId}/products/dates", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param Request $request
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function putOrderProductsDatesAction($orderId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        $productDate = $serializer->deserialize(
            $request->getContent(),
            'AppBundle\Entity\Validate\ValidateProductDate',
            'json'
        );

        //set params
        $productDate->setOrderId($orderId);

        $this->get('logger')->info('API-PEDIDOS PRODUCTS DATE DATES CALLED - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($productDate);

        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')->info('API-PEDIDOS PRODUCTS DATE - Request Json has no errors, updating order product date.');

            /* @var $productService ProductService */
            $productService = $this->get('product_service');

            try {
                $orderVO = $this->get('order_service')->getCommonOrderInfo($orderId);
                // Generate common info to all the order types
                $updateProductDateVO = $productService->getCommonInfoProductDate($productDate, $orderVO);

                //Update date
                $productService->updateDate($updateProductDateVO, $orderVO);

                $this->get('logger')->info('API-PEDIDOS PRODUCTS DATE - ORDER STATUS UPDATED SUCCESSFULLY: ' . $orderId);

                // Return the order ID
                $response = new Response(json_encode(
                    $orderId,
                    JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
                ));
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

                $this->get('logger')->info('API-PEDIDOS PRODUCTS DATE - RESPONSE TIME = ' . (microtime(true) - $startTime));

                return $response;
            } catch (Exception $e) {
                $this->get('logger')->error('API-PEDIDOS PRODUCTS DATE - Error saving the product date: ' . $e->getMessage());
                return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
            }
        } else {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            $this->get('logger')->error('API-PEDIDOS PRODUCTS DATE - Request Json has errors: ' . serialize($errorsMessage));
            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Delete Product Dates",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\ValidateProductDate",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="productId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="product id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     204 = "No content",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Delete("/orders/{orderId}/products/dates", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param Request $request
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function deleteOrderProductsDatesAction($orderId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        $productDate = $serializer->deserialize(
            $request->getContent(),
            'AppBundle\Entity\Validate\Delete\ProductDate',
            'json'
        );

        //set params
        $productDate->setOrderId($orderId);

        $this->get('logger')
            ->info('API-PEDIDOS REMOVE DATES CALLED - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($productDate);

        if (count($errors) > 0) {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }
            $this->get('logger')
                ->error('API-PEDIDOS REMOVE DATES - Request Json has errors: '.serialize($errorsMessage));

            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }

        // If there're no errors in request data
        $this->get('logger')
            ->info('API-PEDIDOS PRODUCTS DATE - Request Json has no errors, updating order product date.');

        /* @var $productService ProductService */
        $productService = $this->get('product_service');

        try {
            $orderVO = $this->get('order_service')->getCommonOrderInfo($orderId);
            // Generate common info to all the order types
            $updateProductDateVO = $productService->getCommonInfoRemoveDate($productDate, $orderVO);

            //Update date
            $productService->deleteDate($updateProductDateVO, $orderVO);

            $this->get('logger')
                ->info('API-PEDIDOS PRODUCTS DATE - ORDER STATUS UPDATED SUCCESSFULLY: ' . $orderId);

            // Return the order ID
            $response = new Response(json_encode(
                $orderId,
                JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            ));
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

            $this->get('logger')
                ->info('API-PEDIDOS PRODUCTS DATE - RESPONSE TIME = ' . (microtime(true) - $startTime));

            return $response;
        } catch (Exception $e) {
            $this->get('logger')
                ->error('API-PEDIDOS PRODUCTS DATE - Error saving the product date: ' . $e->getMessage());
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update Product Dates",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\ValidateProductDate",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="productId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="product id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     204 = "No content",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put("/orders/{orderId}/products/{productId}/dates", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param $productId
     * @param Request $request
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function putOrderProductDatesAction($orderId, $productId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        $validateProductDate = $serializer->deserialize(
            $request->getContent(),
            'AppBundle\Entity\Validate\ValidateProductDate',
            'json'
        );

        //set params
        $validateProductDate->setOrderId($orderId);

        $this->get('logger')->info('API-PEDIDOS PRODUCTS DATE DATES CALLED - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($validateProductDate);

        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')->info('API-PEDIDOS PRODUCTS DATE - Request Json has no errors, updating order product date.');

            /* @var $productService ProductService */
            $productService = $this->get('product_service');

            try {
                $orderVO = $this->get('order_service')->getCommonOrderInfo($validateProductDate->getOrderId());

                $validateProductDateProduct = new ValidateProductDateProduct();
                $validateProductDateProduct->setId($productId);
                $validateProductDate->addProduct($validateProductDateProduct);

                // Generate common info to all the order types
                $udpateProductDateVO = $productService->getCommonInfoProductDate($validateProductDate, $orderVO);

                //Update date
                $productService->updateDate($udpateProductDateVO, $orderVO);

                $this->get('logger')->info('API-PEDIDOS PRODUCTS DATE - ORDER STATUS UPDATED SUCCESSFULLY: ' . $orderId);

                // Return the order ID
                $response = new Response(json_encode(
                    $orderId,
                    JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
                ));
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

                $this->get('logger')->info('API-PEDIDOS PRODUCTS DATE - RESPONSE TIME = ' . (microtime(true) - $startTime));

                return $response;
            } catch (Exception $e) {
                $this->get('logger')->error('API-PEDIDOS PRODUCTS DATE - Error saving the product date: ' . $e->getMessage());
                return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
            }
        } else {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            $this->get('logger')->error('API-PEDIDOS PRODUCTS DATE - Request Json has errors: ' . serialize($errorsMessage));
            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update Product Status",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\Update\UpdateValidateProductStatus",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="productId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="product id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     204 = "No content",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put("/orders/{orderId}/products/{productId}/status", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param $productId
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function putOrderProductStatusAction($orderId, $productId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        $validProductStatus = $serializer->deserialize(
            $request->getContent(),
            'AppBundle\Entity\Validate\Update\UpdateValidateProductStatus',
            'json'
        );

        //set params
        $validProductStatus->setOrderId($orderId);

        $this->get('logger')->info('API-PEDIDOS PRODUCTS STATUS CALLED - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($validProductStatus);

        // If there're no errors in request data
        if (count($errors) > 0) {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            $this->get('logger')->error('API-PEDIDOS PRODUCTS STATUS - Request Json has errors: ' . serialize($errorsMessage));
            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }

        $this->get('logger')->info('API-PEDIDOS PRODUCTS STATUS - Request Json has no errors, updating order product status.');

        try {
            $orderVO = $this->get('order_service')->getCommonOrderInfo($orderId);
            $this->get('product_service')->updateProductStatus($orderVO, $productId, $validProductStatus);

            $this->get('logger')->info('API-PEDIDOS PRODUCTS STATUS - ORDER STATUS UPDATED SUCCESSFULLY: ' . $orderId);

            // Return the order ID
            $response = new Response(
                json_encode($orderId, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
            );
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

            $this->get('logger')->info('API-PEDIDOS PRODUCTS DATE - RESPONSE TIME = ' . (microtime(true) - $startTime));

            return $response;
        } catch (Exception $e) {
            $this->get('logger')->error('API-PEDIDOS PRODUCTS STATUS - Error saving the product status: ' . $e->getMessage());
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update Product Reversal Info",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\Update\UpdateValidateProductStatus",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="productId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="product id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     204 = "No content",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put(
     *     "/orders/{orderId}/products/{productId}/reversal",
     *     requirements={"orderId"="\d+", "productId"="\d+"},
     *     defaults={"orderId" = 0}
     * )
     *
     * @param integer $orderId
     * @param integer $productId
     * @param Request $request
     *
     * @return JsonResponse
     *
     */
    public function putApproveReversalAction($orderId, $productId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        $validateProductReversalInfo = $serializer->deserialize(
            $request->getContent(),
            'AppBundle\Entity\Validate\Update\UpdateValidateProductReversalInfo',
            'json'
        );

        //set params
        $validateProductReversalInfo->setOrderId($orderId);

        $this->get('logger')->info(
            'API-PEDIDOS REVERSAL APPROVE CALLED - REQUEST JSON: ' . serialize($request->getContent())
        );

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($validateProductReversalInfo);

        // If there're no errors in request data
        if (count($errors)) {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            $this->get('logger')->error(
                'API-PEDIDOS REVERSAL APPROVE - Request Json has errors: ' . serialize($errorsMessage)
            );
            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }

        $this->get('logger')->info(
            'API-PEDIDOS REVERSAL APPROVE - Request Json has no errors, updating order product status.'
        );

        $productService = $this->get('product_service');

        try {
            $orderVO = $this->get('order_service')->getCommonOrderInfo($validateProductReversalInfo->getOrderId());

            $validateProductStatusProduct = new UpdateValidateProductStatusProduct();
            $validateProductStatusProduct->setId($productId);
            $validateProductReversalInfo->addProduct($validateProductStatusProduct);

            // Generate common info to all the order types
            $upDateProductStatusVO = $productService->getCommonInfoProductReversal(
                $validateProductReversalInfo,
                $orderVO
            );

            //Update status
            $productService->approveReversal($upDateProductStatusVO, $orderVO);

            $this->get('logger')->info('API-PEDIDOS REVERSAL APPROVE - ORDER STATUS UPDATED SUCCESSFULLY: ' . $orderId);

            // Return the order ID
            return new JsonResponse(array('code' => 200, 'data' => array("order_id" => $orderId)), 200);
        } catch (Exception $e) {
            $this->get('logger')->error(
                'API-PEDIDOS REVERSAL APPROVE - Error saving the product status: ' . $e->getMessage()
            );
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        } finally {
            $this->get('logger')->info('API-PEDIDOS PRODUCTS DATE - RESPONSE TIME = ' . (microtime(true) - $startTime));
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update Provider Billing Date",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\ValidateInvoiceDate",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="purchaseOrder",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="purchase order"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put("/orders/{orderId}/purchase_order/{purchaseOrder}/dates", requirements={"orderId"="\d+"})
     * @param $orderId
     * @param $purchaseOrder
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function putProviderBillingDateAction($orderId, $purchaseOrder, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        /* @var $billingDate ValidateProviderBillingDate */
        $billingDate = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Validate\Update\ValidateProviderBillingDate', 'json');

        $this->get('logger')->info('[PUT] Provider Billing Date - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($billingDate);

        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')->info('[PUT] Provider Billing Date - Request Json has no errors, updating provider billing date.');

            try {
                //set Params
                $billingDate->setOrderId($orderId);
                $billingDate->setPurchaseOrder($purchaseOrder);

                // Generate common info to all the order types
                $updateProductDateVO = $this->get('purchase_order_service')->getPurchaseOrderDateInfo($billingDate);

                $orderVO = $this->get('order_service')->getCommonOrderInfo($billingDate->getOrderId());

                //Update date
                $this->get('purchase_order_service')->updatePurchaseOrderDate($updateProductDateVO, $orderVO);

                $this->get('logger')->info('[PUT] Provider Billing Date - Date of order ' . $orderId . ' and purchase order ' . $purchaseOrder . ' updated successfully.');
                $this->get('logger')->info('[PUT] Provider Billing Date - RESPONSE TIME = ' . (microtime(true) - $startTime));

                return new JsonResponse(array('code' => 200, 'status' => 'success'), 200);
            } catch (Exception $e) {
                $this->get('logger')->error('[PUT] Provider Billing Date - Error saving the date: ' . $e->getMessage());
                return new JsonResponse(array('code' => 400, 'status' => 'error', 'errors' => array($e->getMessage())), 400);
            }
        }

        // If there're errors, save them
        foreach ($errors as $error) {
            $errorsMessage[] = $error->getMessage();
        }

        $this->get('logger')->error('[PUT] Provider Billing Date - Request Json has errors: ' . serialize($errorsMessage));
        return new JsonResponse(array('code' => 400, 'status' => 'error', 'errors' => $errorsMessage), 400);
    }
}
