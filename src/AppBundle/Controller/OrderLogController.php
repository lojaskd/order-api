<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Validate\Create\CreateValidateOrderLog;
use AppBundle\Entity\VO\Create\CreateOrderLogVO;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OrderLogController
 * @package AppBundle\Controller
 */
class OrderLogController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get order logs.",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/{orderId}/logs", requirements={"orderId"="\d+"})
     * @param $orderId
     * @return JsonResponse|Response
     */
    public function getOrderLogAction($orderId)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            // Search the order
            $logs = $entityManager->getRepository('AppBundle:B2cPedlog')->findBy(array('idPedped' => $orderId), array('idPedlog' => 'DESC'));

            if ($logs) {
                $serializer = SerializerBuilder::create()->build();
                // Return the order in json format
                $response = new Response(
                    $serializer->serialize(
                        array('code' => 200, 'status' => 'success', 'data' => $logs),
                        'json',
                        SerializationContext::create()->setSerializeNull(true)
                    )
                );
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
                return $response;
            }
        } catch (\Exception $e) {
            $this->get('logger')->error('Error to search order logs: ' . $e->getMessage());
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }

        return new JsonResponse(array('code' => 204), 204);
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Create logs to order.",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\ValidateOrderLog",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Post("/orders/{orderId}/logs", requirements={"orderId"="\d+"})
     * @param $orderId
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function postOrderLogAction($orderId, Request $request)
    {
        $startTime = microtime(true);

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();
        /* @var $orderLog CreateValidateOrderLog */
        $orderLog = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Validate\Create\CreateValidateOrderLog', 'json');
        $errorsMessage = $generateOrderVO['errors'] = array();

        $this->get('logger')->info('API-PEDIDOS (INSERT LOG) CALLED - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $errors = $this->get('validator')->validate($orderLog);

        // If there're no errors in request data
        if (count($errors) == 0) {
            $orderLogVO = new CreateOrderLogVO();
            $orderLogVO->setOrderId($orderId);
            $orderLogVO->setCreatedAt(new \DateTime('now'));
            $orderLogVO->setTitle($orderLog->getTitle());
            $orderLogVO->setText($orderLog->getText());
            $orderLogVO->setShow($orderLog->isShow());
            $orderLogVO->setStatus($orderLog->getStatus());
            $orderLogVO->setInternalStatus($orderLog->getInternalStatus());

            $this->get('logger')->info('API-PEDIDOS (INSERT LOG) - Request Json has no errors, creating log...');

            // Calls to save log on database
            $this->container->get('order_log_service')->insertOrderLog($orderLogVO);
        } else {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }
        }

        // If there're errors, return them
        if (!empty($errorsMessage)) {
            $this->get('logger')->error('API-PEDIDOS (INSERT LOG) - Request Json has errors: ' . serialize($errorsMessage));
            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }

        $this->get('logger')->info('API-PEDIDOS (INSERT LOG) - ORDER LOG CREATED SUCCESSFULLY');
        // Return the order ID
        $response = new Response(json_encode(true, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

        $this->get('logger')->info('API-PEDIDOS (INSERT LOG) - RESPONSE TIME = ' . (microtime(true) - $startTime));

        return $response;
    }
}
