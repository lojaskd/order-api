<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class OrderAttributeController
 * @package AppBundle\Controller
 */
class OrderAttributeController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get order attributes",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     204 = "No content",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/{orderId}/attributes", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function getOrderAttributesAction($orderId, Request $request)
    {
        $startTime = microtime(true);
        try {
            $entityManager = $this->getDoctrine()->getManager();

            $this->get('logger')->info('API-PEDIDOS ATTRIBUTES SEARCH CALLED - REQUEST JSON: ' . serialize($request->getContent()));

            // Search for order attributes
            $attributes = $entityManager->getRepository('AppBundle:Attributes')->getAttributes();

            if (!empty($attributes)) {
                foreach ($attributes as $key => $attribute) {
                    if ($attribute['type_name'] == 'Choice') {
                        // Search for attributes options
                        $attributesOptions = $entityManager->getRepository('AppBundle:AttributesOptions')->getAttributeOptions($attribute['id_attr']);
                        $attributes[$key]['options'] = $attributesOptions;
                    }

                    $attributesValues = $entityManager->getRepository('AppBundle:OrderAttributes')->getOrderAttributes($orderId, $attribute['id_attr']);
                    $attributes[$key]['value'] = $attributesValues;
                }
            }
        } catch (\Exception $e) {
            $this->get('logger')->error('API-PEDIDOS ATTRIBUTES SEARCH - Error searching for order attributes: ' . $e->getMessage());
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }

        if ($attributes) {
            $serializer = SerializerBuilder::create()->build();
            // Return order attributes on json format
            $response = new Response($serializer->serialize(array('attributes' => $attributes), 'json', SerializationContext::create()->setSerializeNull(true)));
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

            $this->get('logger')->info('API-PEDIDOS ATTRIBUTES SEARCH (FOUND) - RESPONSE TIME = ' . (microtime(true) - $startTime));

            return $response;
        }

        $this->get('logger')->info('API-PEDIDOS ATTRIBUTES SEARCH (NOT FOUND) - RESPONSE TIME = ' . (microtime(true) - $startTime));
        return new JsonResponse(array('code' => 204, 'errors' => array('Not found')), 200);
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update Order Attributes",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put("/orders/{orderId}/attributes", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param Request $request
     * @return JsonResponse
    **/
    public function putOrderAttributesAction($orderId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();
        $attribute = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\AttributesJson', 'json');

        $this->get('logger')->info('API-PEDIDOS ATTRIBUTE UPDATE - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $errors = $this->get('validator')->validate($attribute);
        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')->info('API-PEDIDOS ATTRIBUTE UPDATE - Request Json has no errors, updating order attribute.');

            try {
                if (!empty($attribute->getAttributes())) {
                    foreach ($attribute->getAttributes() as $attr) {
                        // Update order attribute
                        $this->get('attribute_service')->updateAttributeOrder($orderId, $attr);
                    }

                    $this->get('logger')->info('API-PEDIDOS ATTRIBUTE UPDATE (Success) - RESPONSE TIME = ' . (microtime(true) - $startTime));
                    return new JsonResponse(array('code' => 200, 'message' => 'Successfully updated attribute(s)'), 200);
                }

                $this->get('logger')->info('API-PEDIDOS ATTRIBUTE UPDATE (Success) - RESPONSE TIME = ' . (microtime(true) - $startTime));
                return new JsonResponse(array('code' => 200, 'message' => 'No attributes were informed.'), 200);
            } catch (Exception $e) {
                $this->get('logger')->error('API-PEDIDOS ATTRIBUTE UPDATE (Error): ' . $e->getMessage());
                return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
            }
        }

        // If there're errors, save and put them on response
        foreach ($errors as $error) {
            $errorsMessage[] = $error->getMessage();
        }

        $this->get('logger')->error('API-PEDIDOS ATTRIBUTE UPDATE - Request Json has errors: ' . serialize($errorsMessage));
        return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
    }
}
