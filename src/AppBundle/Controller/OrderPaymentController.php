<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class OrderPaymentController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get order payment data by ID",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/{orderId}/payments", requirements={"orderId"="\d+"})
     * @param $orderId
     *
     * @return JsonResponse|Response
     */
    public function indexAction($orderId)
    {
        $startTime = microtime(true);
        try {
            $paymentData = $this->get('order_payment_service')->getOrderPayment($orderId);

            $this->get('logger')->info('[GET] Order Payments - Response time = '.(microtime(true) - $startTime));
            $serializer = SerializerBuilder::create()->build();
            // Return the order in json format
            $response = new Response(
                $serializer->serialize(
                    array('code' => 200, 'status' => 'success', 'data' => $paymentData),
                    'json',
                    SerializationContext::create()->setSerializeNull(true)
                )
            );
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
            return $response;

            //return new JsonResponse(array('code' => 200, 'status' => 'success', 'data' => $paymentData), 200);
        } catch (\Exception $e) {
            $this->get('logger')->error('[GET] Order Payments - Error searching order payment: '.$e->getMessage());
            return new JsonResponse(array('code' => 500, 'errors' => array($e->getMessage())), 500);
        }
    }
}
