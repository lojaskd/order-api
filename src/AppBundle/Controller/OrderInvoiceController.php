<?php

namespace AppBundle\Controller;

use AppBundle\Entity\B2cNotafiscalItem;
use AppBundle\Entity\Validate\Create\CreateValidateInvoiceProduct;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class OrderInvoiceController
 * @package AppBundle\Controller
 */
class OrderInvoiceController extends FOSRestController
{

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get invoices from order.",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     204 = "No content",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/{orderId}/invoices", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @return JsonResponse|Response
     */
    public function getOrderInvoicesAction($orderId)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();

            // Search the order
            $invoices = $entityManager->getRepository('AppBundle:B2cNotafiscal')->findBy(array('idPedped' => $orderId));
        } catch (\Exception $e) {
            $this->get('logger')->error('Error to search order: ' . $e->getMessage());
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }

        if ($invoices) {
            $serializer = SerializerBuilder::create()->build();
            // Return the order in json format
            $response = new Response($serializer->serialize($invoices, 'json', SerializationContext::create()->setSerializeNull(true)));
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

            return $response;
        }

        return new JsonResponse(array('code' => 204, 'errors' => array('Not found')), 204);
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get products from order and invoice ID",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="invoiceId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="invoice id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/{orderId}/invoices/{invoiceId}/products", requirements={"orderId"="\d+", "invoiceId"="\d+"})
     * @param $orderId
     * @param $invoiceId
     *
     * @return JsonResponse|Response
     */
    public function getOrderInvoiceProductsAction($orderId, $invoiceId)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            // Search the order
            $invoices = $entityManager->getRepository('AppBundle:B2cNotafiscalItem')->findBy(
                array(
                    'idPedped' => $orderId,
                    'idNotafiscal' => $invoiceId
                )
            );

            if (!empty($invoices)) {
                $serializer = SerializerBuilder::create()->build();
                // Return the order in json format
                $response = new Response(
                    $serializer->serialize(
                        array('code' => 200, 'status' => 'success', 'data' => $invoices),
                        'json',
                        SerializationContext::create()->setSerializeNull(true)
                    )
                );
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
                return $response;
            }
        } catch (\Exception $exception) {
            $this->get('logger')->error('Error searching order invoice products: ' . $exception->getMessage());
            return new JsonResponse(array('code' => 500, 'errors' => array($exception->getMessage())), 500);
        }

        return new JsonResponse(array('code' => 200, 'status' => 'success', 'data' => []), 200);
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get invoice data",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="invoiceId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="invoice id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/{orderId}/invoices/{invoiceId}", requirements={"orderId"="\d+", "invoiceId"="\d+"})
     * @param $orderId
     * @param $invoiceId
     *
     * @return JsonResponse|Response
     */
    public function getOrderInvoiceAction($orderId, $invoiceId)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            // Search the order
            $invoices = $entityManager->getRepository('AppBundle:B2cNotafiscal')->findBy(
                array(
                    'idPedped' => $orderId,
                    'idNotafiscal' => $invoiceId
                )
            );

            if (!empty($invoices)) {
                $serializer = SerializerBuilder::create()->build();
                // Return the order in json format
                $response = new Response(
                    $serializer->serialize(
                        array('code' => 200, 'status' => 'success', 'data' => $invoices),
                        'json',
                        SerializationContext::create()->setSerializeNull(true)
                    )
                );
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
                return $response;
            }
        } catch (\Exception $exception) {
            $this->get('logger')->error('Error searching order invoice data: ' . $exception->getMessage());
            return new JsonResponse(array('code' => 500, 'errors' => array($exception->getMessage())), 500);
        }

        return new JsonResponse(array('code' => 200, 'status' => 'success', 'data' => []), 200);
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get invoice tracking information.",
     *   input = "integer",
     *   input = "integer",
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="invoiceId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="invoice id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     204 = "No content",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Get("/orders/{orderId}/invoices/{invoiceId}/tracking", requirements={"orderId"="\d+", "invoiceId"="\d+"})
     * @param $orderId
     * @param $invoiceId
     * @return JsonResponse|Response
     */
    public function getOrderInvoiceTrackingAction($orderId, $invoiceId)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();

            // Get invoice tracking
            $invoiceTracking = $entityManager->getRepository('AppBundle:B2cProcedaFileLines')->findBy(
                array(
                    'idPedped' => $orderId,
                    'invoiceId' => $invoiceId
                )
            );
        } catch (\Exception $e) {
            $this->get('logger')->error('Error to search order: ' . $e->getMessage());
            return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
        }

        if ($invoiceTracking) {
            $serializer = SerializerBuilder::create()->build();
            // Return the order in json format
            $response = new Response($serializer->serialize($invoiceTracking, 'json', SerializationContext::create()->setSerializeNull(true)));
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

            return $response;
        }

        return new JsonResponse(array('code' => 204, 'errors' => array('Not found')), 204);
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update Invoice Dates",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\ValidateInvoiceDate",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="invoiceId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="invoice id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put("/orders/{orderId}/invoices/{invoiceId}/dates", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param $invoiceId
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function putOrderInvoiceDatesAction($orderId, $invoiceId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        $invoiceDate = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Validate\ValidateInvoiceDate', 'json');

        $this->get('logger')->info('API-PEDIDOS INVOICE DATE DATES CALLED - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($invoiceDate);


        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')->info('API-PEDIDOS INVOICE DATE - Request Json has no errors, updating order product date.');

            $invoiceService = $this->get('invoice_service');

            try {
                //set Params
                $invoiceDate->setOrderId($orderId);
                $invoiceDate->setInvoiceId($invoiceId);

                // Generate common info to all the order types
                $updateProductDateVO = $invoiceService->getCommonInfoInvoiceDate($invoiceDate);

                $orderVO = $this->get('order_service')->getCommonOrderInfo($invoiceDate->getOrderId());

                //Update date
                $invoiceService->updateDate($updateProductDateVO, $orderVO);

                $this->get('logger')->info('API-PEDIDOS INVOICE DATE - ORDER STATUS UPDATED SUCCESSFULLY: ' . $orderId);

                // Return the order ID
                $response = new Response(json_encode($orderId, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

                $this->get('logger')->info('API-PEDIDOS INVOICE DATE - RESPONSE TIME = ' . (microtime(true) - $startTime));

                return $response;
            } catch (Exception $e) {
                $this->get('logger')->error('API-PEDIDOS INVOICE DATE - Error saving the invoice date: ' . $e->getMessage());
                return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
            }
        }

        // If there're errors, save them
        foreach ($errors as $error) {
            $errorsMessage[] = $error->getMessage();
        }

        $this->get('logger')->error('API-PEDIDOS INVOICE DATE - Request Json has errors: ' . serialize($errorsMessage));
        return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Create Invoice",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\Create\CreateValidateInvoice",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Post("/orders/{orderId}/invoices", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function postInvoiceAction($orderId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        $invoice = $serializer
            ->deserialize($request->getContent(), 'AppBundle\Entity\Validate\Create\CreateValidateInvoice', 'json');

        $this->get('logger')->info('API-PEDIDOS INVOICE CALLED - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($invoice);

        $orderService = $this->get('order_service');
        $invoiceService = $this->get('invoice_service');

        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')->info('API-PEDIDOS INVOICE - Request Json has no errors, updating order status.');

            try {
                $invoice->setOrderId($orderId);
                // Generate common info to all the order types
                $orderVO = $orderService->getCommonOrderInfo($orderId);
                $createInvoiceVO = $invoiceService->getCommonInvoiceInfo($invoice);
                $invoiceService->createInvoice($createInvoiceVO, $orderVO);

                $this->get('logger')->info('API-PEDIDOS INVOICE - INVOICE UPDATED SUCCESSFULLY: ' . $orderId);

                // Return the order ID
                $response = new Response(
                    json_encode($orderId, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
                );
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

                $this->get('logger')->info('API-PEDIDOS INVOICE - RESPONSE TIME = ' . (microtime(true) - $startTime));

                return $response;
            } catch (Exception $e) {
                $this->get('logger')->error('API-PEDIDOS INVOICE - Error saving the order: ' . $e->getMessage());
                return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
            }
        } else {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            $this->get('logger')->error('API-PEDIDOS INVOICE - Request Json has errors: ' . serialize($errorsMessage));
            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Create Invoice Product",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\Create\CreateValidateInvoiceProduct",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Post("/orders/{orderId}/products/invoices")
     * @param $orderId
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function postInvoiceProductAction($orderId, Request $request)
    {
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();
        /* @var $invoice CreateValidateInvoiceProduct */
        $invoice = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Validate\Create\CreateValidateInvoiceProduct', 'json');


        $this->get('logger')->info('[POST] postInvoiceProductAction - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($invoice);

        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')->info('[POST] postInvoiceProductAction - Request Json has no errors, updating order status.');

            try {
                $invoice->setOrderId($orderId);
                $orderInvoice = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('AppBundle:B2cNotafiscal')
                    ->findOneBy(
                        array(
                            'idPedped' => $invoice->getOrderId(),
                            'idNotafiscal' => $invoice->getInvoiceId(),
                            'idFilial' => $invoice->getBranchId()
                        )
                    );


                if (empty($orderInvoice)) {
                    return new JsonResponse(array('code' => 500, 'status' => 'error', 'message' => 'No invoice found using order, invoice ID and branch ID.', 'errors' => ['No invoice found using order, invoice ID and branch ID.'], 500));
                }

                $orderInvoiceProduct = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('AppBundle:B2cNotafiscalItem')
                    ->findOneBy(
                        array(
                            'idPedped' => $invoice->getOrderId(),
                            'idNotafiscal' => $invoice->getInvoiceId(),
                            'idFilial' => $invoice->getBranchId(),
                            'prdReferencia' => $invoice->getProductReference()
                        )
                    );

                if (!empty($orderInvoiceProduct)) {
                    return new JsonResponse(array('code' => 500, 'status' => 'error', 'message' => 'Invoice product already created.', 'errors' => ['Invoice product already created.'], 500));
                }

                $productInvoiceCreated = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('AppBundle:B2cNotafiscalItem')
                    ->createInvoiceProduct($invoice);
                if ($productInvoiceCreated instanceof B2cNotafiscalItem) {
                    $this->get('doctrine.orm.entity_manager')->getRepository('AppBundle:B2cPedprd')->updateProductInvoice($invoice->getOrderId(), $invoice->getProductReference(), $invoice->getInvoiceId());
                    $this->get('doctrine.orm.entity_manager')->getRepository('AppBundle:B2cPedped')->updateOrderUpdateDate($invoice->getOrderId());

                    return new JsonResponse([
                        'code' => 200,
                        'status' => 'success',
                        'data' => $productInvoiceCreated->getIdPedped()
                    ]);
                }
                $this->get('logger')->info('[POST] postInvoiceProductAction - Invoice product successfully created.');

                return new JsonResponse(array('code' => 500, 'status' => 'error', 'message' => 'Error creating invoice product.', 'errors' => ['Error creating invoice product.']), 500);
            } catch (\Exception $exception) {
                $this->get('logger')->error('[POST] postInvoiceProductAction - Error saving the order product invoice: ' . $exception->getMessage());
                return new JsonResponse(array('code' => 500, 'status' => 'error', 'message' => $exception->getMessage(), 'errors' => array($exception->getMessage())), 500);
            }
        } else {
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            $this->get('logger')->error('[POST] postInvoiceProductAction - Request Json has errors: ' . serialize($errorsMessage));
            return new JsonResponse(array('code' => 500, 'status' => 'error', 'errors' => $errorsMessage), 500);
        }
    }

    /**
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update Invoice",
     *   input = "integer",
     *   input = {
     *     "class" = "AppBundle\Entity\Validate\Update\UpdateValidateInvoice",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *     }
     *   },
     *   requirements={
     *      {
     *          "name"="orderId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="order id"
     *      },
     *      {
     *          "name"="invoiceId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="invoice id"
     *      }
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the request has errors"
     *   }
     * )
     *
     * @Rest\Put("/orders/{orderId}/invoices/{invoiceId}", requirements={"orderId"="\d+"}, defaults={"orderId" = 0})
     * @param $orderId
     * @param $invoiceId
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function putOrderInvoiceAction($orderId, $invoiceId, Request $request)
    {
        $startTime = microtime(true);
        $errorsMessage = array();

        // Deserialize the request data
        $serializer = SerializerBuilder::create()->build();

        $invoice = $serializer->deserialize($request->getContent(), 'AppBundle\Entity\Validate\Update\UpdateValidateInvoice', 'json');

        $this->get('logger')->info('API-PEDIDOS INVOICE CALLED - REQUEST JSON: ' . serialize($request->getContent()));

        // Validate the request data
        $validator = $this->get('validator');
        $errors = $validator->validate($invoice);

        // If there're no errors in request data
        if (count($errors) == 0) {
            $this->get('logger')->info('API-PEDIDOS INVOICE - Request Json has no errors, updating order Invoice.');

            $invoiceService = $this->get('invoice_service');

            try {
                //set Params
                $invoice->setOrderId($orderId);
                $invoice->setInvoiceId($invoiceId);

                // Generate common info to all the order types
                $orderVO = $this->get('order_service')->getCommonOrderInfo($orderId);

                // Generate common info to invoice
                $updateProductVO = $invoiceService->getCommonUpdateInvoiceInfo($invoice);

                //Update invoice
                $invoiceService->updateInvoice($updateProductVO, $orderVO);

                $this->get('logger')->info('API-PEDIDOS INVOICE - INVOICE UPDATED SUCCESSFULLY: Order: ' . $orderId . ' - Invoice:' . $invoiceId);

                // Return the order ID
                $response = new Response(json_encode($orderId, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
                $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

                $this->get('logger')->info('API-PEDIDOS INVOICE - RESPONSE TIME = ' . (microtime(true) - $startTime));

                return $response;
            } catch (Exception $e) {
                $this->get('logger')->error('API-PEDIDOS INVOICE - Error saving the invoice: ' . $e->getMessage());
                return new JsonResponse(array('code' => 400, 'errors' => array($e->getMessage())), 400);
            }
        } else {
            // If there're errors, save them
            foreach ($errors as $error) {
                $errorsMessage[] = $error->getMessage();
            }

            $this->get('logger')->error('API-PEDIDOS INVOICE - Request Json has errors: ' . serialize($errorsMessage));
            return new JsonResponse(array('code' => 400, 'errors' => $errorsMessage), 400);
        }
    }
}
