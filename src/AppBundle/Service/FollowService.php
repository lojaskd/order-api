<?php
namespace AppBundle\Service;

use Symfony\Bridge\Monolog\Logger;
use GuzzleHttp\Client as GuzzleClient;

/**
 * Class FollowService
 * @package AppBundle\Service
 */
class FollowService
{

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var string
     */
    private $followApiUrl;


    /**
     * @param Logger $logger
     * @param string $followApiUrl
     */
    public function __construct(Logger $logger, $followApiUrl)
    {
        $this->logger = $logger;
        $this->followApiUrl = $followApiUrl;

        $this->client = new GuzzleClient([
            'base_uri' => $this->followApiUrl,
        ]);
    }

    /**
     * @param string $type
     * @param int $orderId
     * @param int $invoiceId
     * @return array
     * @throws \Exception
     */
    public function getFollowFromTypeOrderInvoice($type, $orderId, $invoiceId)
    {
        $filters = $this->filtersQuery(['query' => ['type' => $type, 'order_id' => $orderId, 'outgoing_invoice_id' => $invoiceId]]);

        $request = $this->client->request('GET', 'follows', [
            'connect_timeout' => 5.00,
            'headers' => ['content-type' => 'application/json'],
            'query' => $filters,
            'debug' => false,
        ]);

        if ($request->getStatusCode() != '200') {
            throw new \Exception('No response', $request->getStatusCode());
        }

        $response = json_decode($request->getBody()->getContents(), true);
        if (!empty($response['code']) && !in_array($response['code'], ['200', '204'])) {
            throw new \Exception($response['message'], $response['code']);
        }

        if (empty($response['data'])) {
            return [];
        }

        return (!empty($response['data']['follows']) ? $response['data']['follows'][0] : []);
    }

    /**
     * @param $parameters
     * @return array
     */
    public function filtersQuery($parameters) {
        if (!empty($parameters['query'])) {
            $filters = array_map(function ($value, $filter) {
                if (is_array($value)) {
                    if (!empty($value['op']) && !empty($value['value'])) {
                        return $filter . " " . $value['op'] . " '" . $value['value']. "'";
                    }
                    $value = "'".implode("','", $value)."'";
                    return "$filter in ($value)";
                }
                return "$filter = '$value'";
            }, $parameters['query'], array_keys($parameters['query']));

            $parameters['query'] = "(". implode(' and ', $filters) . ")";
        }

        return $parameters;
    }
}
