<?php

namespace AppBundle\Service;

use AppBundle\Entity\B2cForfor;
use AppBundle\Entity\B2cPedprd;
use AppBundle\Entity\B2cPedprdEstorno;
use AppBundle\Entity\B2cPedprdstatus;
use AppBundle\Entity\B2cProdutosKits;
use AppBundle\Entity\B2cViewProdutos;
use AppBundle\Entity\Validate\Update\UpdateValidateProductReversalInfo;
use AppBundle\Entity\Validate\Update\UpdateValidateProductStatus;
use AppBundle\Entity\Validate\Update\UpdateValidateProductStatusProduct;
use AppBundle\Entity\Validate\ValidateProductDate;
use AppBundle\Entity\Validate\Delete\ProductDate;
use AppBundle\Entity\Validate\Delete\ProductDateType;
use AppBundle\Entity\Validate\ValidateProductDateProduct;
use AppBundle\Entity\Validate\ValidateProductDateType;
use AppBundle\Entity\VO\Create\CreateOrderLogVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateProductVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateTypeVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateVO;
use AppBundle\Entity\VO\Update\UpdateProductsReversalInfoVO;
use AppBundle\Entity\VO\Update\UpdateProductsStatusProductVO;
use AppBundle\Entity\VO\Update\UpdateProductsStatusVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class ProductService
 * @package AppBundle\Service
 */
class ProductService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var OrderStatusService
     */
    private $orderStatusService;

    /**
     * @var OrderDateService
     */
    private $orderDateService;

    /**
     * @var MarketPlaceService
     */
    private $marketPlaceService;

    /**
     * @var OrderLogService
     */
    private $orderLogService;

    /**
     * @var boolean
     */
    private $marketplaceChannelStock;

    /**
     * @var boolean
     */
    private $aggressivePrice;

    /**
     * @var AwsService
     */
    private $awsService;


    /**
     * ProductService constructor.
     * @param EntityManager $entityManager
     * @param QueueService $queueService
     * @param Logger $logger
     * @param EventService $eventService
     * @param $marketplaceChannelStock
     * @param OrderLogService $orderLogService
     * @param AwsService $awsService
     * @param $aggressivePrice
     */
    public function __construct(
        EntityManager $entityManager,
        QueueService $queueService,
        Logger $logger,
        EventService $eventService,
        $marketplaceChannelStock,
        OrderLogService $orderLogService,
        AwsService $awsService,
        $aggressivePrice
    )
    {
        $this->entityManager = $entityManager;
        $this->queueService = $queueService;
        $this->logger = $logger;
        $this->eventService = $eventService;
        $this->marketplaceChannelStock = $marketplaceChannelStock;
        $this->orderLogService = $orderLogService;
        $this->awsService = $awsService;
        $this->aggressivePrice = $aggressivePrice;
    }

    /**
     * @return OrderProductVO
     *
     * Create OrderProductVO from database data
     */
    public function formatProductInfo()
    {
        return new OrderProductVO();
    }

    /**
     * @param OrderStatusService $orderStatusService
     * @return $this
     */
    public function setOrderStatusService(OrderStatusService $orderStatusService)
    {
        $this->orderStatusService = $orderStatusService;
        return $this;
    }

    /**
     * @param OrderDateService $orderDateService
     * @return $this
     */
    public function setOrderDateService(OrderDateService $orderDateService)
    {
        $this->orderDateService = $orderDateService;
        return $this;
    }

    /**
     * @param MarketPlaceService $marketPlaceService
     * @return $this
     */
    public function setMarketPlaceService(MarketPlaceService $marketPlaceService)
    {
        $this->marketPlaceService = $marketPlaceService;
        return $this;
    }

    /**
     * @param array $products
     * @return bool
     *
     * Check if there is product stock
     */
    public function validateProductStock($products)
    {
        $productRepository = $this->entityManager->getRepository('AppBundle:B2cViewProdutos');

        // For each product order
        /* @var $product CreateProductVO */
        foreach ($products as $product) {
            // Get product stock and deadline
            $stock = $productRepository->getProductStockAndDeadline($product->getId(), $product->getQuantity());
            // Get provider stock
            $providerStock = $productRepository->getProviderStock($product->getId());

            // Get the max stock between product and provider
            $maxStock = (
            intval(
                $stock['stock']) >= intval($providerStock['stock']) ? intval($stock['stock']) : intval($providerStock['stock'])
            );

            if ($maxStock < $product->getQuantity()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $products
     * @param int $marketplaceId
     * @param int $channelId
     * @return bool Check if there is product stock
     * @throws \Exception
     *
     * Check if there is product stock
     */
    public function validateMarketPlaceProductStock($products, $marketplaceId, $channelId)
    {
        $idAttrStock = $this->marketPlaceService->getMarketplaceAttrStock($channelId);

        // For each product order
        /* @var $product CreateProductVO */
        foreach ($products as $product) {
            $stock = null;

            // Key to enable marketplace channel stock attribute
            if ($this->marketplaceChannelStock) {
                // Get product stock and deadline
                if ($idAttrStock > 0) {
                    $stockAttr = $this->entityManager->getRepository('AppBundle:B2cAtributoValor')->findOneBy(array(
                        'b2cPrdprd' => $product->getId(),
                        'idAtributo' => $idAttrStock
                    ));

                    if ($stockAttr && $stockAttr->getRespostas() > 0) {
                        $stock = $stockAttr->getRespostas();
                    }
                }
            }

            if ($stock === null) {
                $stock = $this->entityManager->getRepository('AppBundle:B2cPrdprdMarketplace')->getProductStock(
                    $product->getId(),
                    $marketplaceId
                );
            }

            if ($stock < $product->getQuantity()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $productId
     * @return string
     *
     * Get the product image
     */
    public function getProductImage($productId)
    {
        // Get product image to products listing
        $productImage = $this->entityManager->getRepository('AppBundle:B2cPrdimg')->getProductListingImage($productId);

        switch ($productId) {
            case ($productId < 100):
                $imageDir = '0';
                break;
            case ($productId >= 100 && $productId < 1000):
                $imageDir = substr(strval($productId), 0, 1) . '00';
                break;
            case ($productId >= 1000 && $productId < 10000):
                $imageDir = substr(strval($productId), 0, 2) . '00';
                break;
            case ($productId >= 10000 && $productId < 100000):
                $imageDir = substr(strval($productId), 0, 3) . '00';
                break;
            default:
                $imageDir = substr(strval($productId), 0, 4) . '00';
        }

        if ($productImage != '') {
            $productImage = '/' . $imageDir . '/' . $productId . '/' . $productImage;
        } else {
            $productImage = '';
        }

        return $productImage;
    }


    /**
     * @param CreateProductVO $productVO
     * @param bool $isAssistance
     * @return float|int
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * Get the product total price
     */
    public function getTotalPrice(CreateProductVO $productVO, $isAssistance = false)
    {
        // Immediate Delivery
        $totalPrice = 0;
        if ($this->aggressivePrice && !$isAssistance) {
            if ($productVO->getProductStock() >= $productVO->getQuantity()) {
                $imedDeliveryPrice = $this->entityManager->getRepository('AppBundle:B2cPrdprdPrecosAgressivos')
                    ->findOneBy(array(
                        'idPrdprd' => $productVO->getId(),
                        'ativo' => '1'
                    ));

                if (!empty($imedDeliveryPrice) && $imedDeliveryPrice->getPreco() > 0) {
                    $totalPrice = $imedDeliveryPrice->getPreco() * $productVO->getQuantity();
                }
            }
        }

        // If the product has promotion use the promotion price
        if ($totalPrice == 0) {
            if ($productVO->isPromotion()) {
                $totalPrice = $productVO->getPromotionPrice() * $productVO->getQuantity();
                // If the product has no promotion use the real price
            } else {
                $totalPrice = $productVO->getRealPrice() * $productVO->getQuantity();
            }
        }

        // If the product is part of kit
        if ($productVO->getKitId() > 0) {
            // Get the kit discount info
            $hasProductKit = $this->entityManager->getRepository('AppBundle:B2cProdutosKits')
                ->getProductKit($productVO->getId(), $productVO->getKitId());

            // If the product kit has discount, calculate it
            if (isset($hasProductKit['desconto']) && $hasProductKit['desconto'] > 0
                && ($productVO->getQuantity() >= $hasProductKit['quantidade'])
            ) {
                $totalPrice = round(
                    ($productVO->getRealPrice() - ($productVO->getRealPrice() * floatval($hasProductKit['desconto']) / 100)) * $productVO->getQuantity(),
                    2
                );
            }
        }

        return $totalPrice;
    }

    /**
     * @param $orderId
     * @param $marketplaceId
     * @param CreateOrderVO $crateOrderVO
     * @return bool
     *
     * Reduce the product stock
     */
    public function reduceStock($orderId, $marketplaceId, CreateOrderVO $crateOrderVO)
    {
        $result = false;
        try {
            /* @var $product CreateProductVO */
            foreach ($crateOrderVO->getProducts() as $product) {
                if ($marketplaceId == 0) {
                    // Generate queue reduce stock data
                    $productStockInfo = array(
                        'id_pedped' => $orderId,
                        'produto_id' => $product->getId(),
                        'produto_quantidade' => $product->getQuantity(),
                        'produto_estoque' => $product->getProductStock(),
                        'produto_estoque_fornecedor' => $product->getProviderStock(),
                        'produto_estoque_fornecedor_reservado' => $product->getProviderStockReserved()
                    );

                    // try to send to queue, if it return error do manually
                    if (!$this->queueService->sendReduceStock(serialize($productStockInfo))) {
                        $this->reduceStockManually($orderId, $product);
                    }
                    $result = true;
                } else {
                    $this->reduceMarketPlaceStock(
                        $orderId,
                        $marketplaceId,
                        $crateOrderVO->getMarketplace()->getChannelId(),
                        $product
                    );
                    $result = true;
                }
            }
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $result;
    }

    /**
     * @param $orderId
     * @param CreateProductVO $product
     * @return bool
     *
     * Reduce the product stock manually
     */
    public function reduceStockManually($orderId, CreateProductVO $product)
    {
        $storeStock = $providerStock = $newBranchOfficeStock = 0;
        $branchOfficeId = null;

        // Get product stock and deadline info
        $productStockInfo = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')
            ->getProductStockAndDeadline($product->getId(), $product->getQuantity());

        if ($productStockInfo) {
            $branchOfficeId = $productStockInfo['idFilial'];
            $branchOfficeStock = ($productStockInfo['stock'] > 0 ? $productStockInfo['stock'] : 0);

            // If the branch office stock is greater than the product quantity, use it
            if ($branchOfficeStock >= $product->getQuantity()) {
                $newBranchOfficeStock = $branchOfficeStock - $product->getQuantity();
                $storeStock = $product->getQuantity();
                // Else, use the provider stock
            } else {
                if ($product->getProviderStock() >= $product->getQuantity()) {
                    $product->setProviderStock($product->getProviderStock() - $product->getQuantity());
                    $providerStock = $product->getQuantity();
                }
            }
        }

        // Get provider info
        /* @var $provider B2cForfor */
        $provider = $this->entityManager->getRepository('AppBundle:B2cForfor')->findOneBy(array('id' => $product->getProvider()));

        // If the product provider deadline is smaller than the provider deadline, use the provider deadline
        if ($product->getProductProviderDeadline() < $provider->getPrazo()) {
            $product->setProductProviderDeadline($provider->getPrazo());
        }

        // Get the product availability info
        /* @var $product CreateProductVO */
        $product = $this->getAvailabilityInfo($product);

        // Update the provider stock
        $this->entityManager->getRepository('AppBundle:B2cPrdprd')
            ->updateProviderStock($product->getId(), $product->getProviderStock());

        // If there is an branch office, update it's stock
        if ($branchOfficeId) {
            $this->entityManager->getRepository('AppBundle:B2cPrdestoque')
                ->updateBranchOfficeStock($branchOfficeId, $product->getId(), $newBranchOfficeStock);
        }

        // Get product info from database
        $prdprd = $this->entityManager->getRepository('AppBundle:B2cPrdprd')->findOneBy(array('id' => $product->getId()));
        // Update the product stock
        $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->updateProductStockInfo($product, $prdprd);

        // Update kit's stock that has the product included
        $this->updateKitStock($product->getId());

        // Update the stock info in b2c_pedprd
        $this->entityManager->getRepository('AppBundle:B2cPedPrd')
            ->setStockInfo($orderId, $product->getId(), $storeStock, $providerStock, $branchOfficeId);

        return true;
    }

    /**
     * @param CreateProductVO $product
     * @return CreateProductVO
     *
     * Get product availability info
     */
    public function getAvailabilityInfo(CreateProductVO $product)
    {
        // If the product has no stock
        if ($product->getProductStock() == 0) {
            // Based on min stock action, set the availability info
            switch ($product->getMinStockAction()) {
                case 1:
                    $product->setAvailability('ESGOTADO');
                    $product->setOut(1);
                    $product->setAction('sol');
                    break;
                case 2:
                    if ($product->getProviderStock() <= 0) {
                        $product->setAvailability('ESGOTADO');
                        $product->setOut(1);
                        $product->setAction('sol');
                    } else {
                        $product->setAvailability('IMEDIATA');

                        if ($product->getProviderDeadline() >= $product->getProductProviderDeadline()) {
                            if ($product->getProviderDeadline() == 0) {
                                $product->setAvailability('IMEDIATA');
                            } else {
                                $product->setAvailability('Em ' . $product->getProviderDeadline() . ' dias úteis');
                            }
                        } else {
                            if ($product->getProductProviderDeadline() == 0) {
                                $product->setAvailability('IMEDIATA');
                            } else {
                                $product->setAvailability('Em ' . $product->getProductProviderDeadline() . ' dias úteis');
                            }
                        }
                    }
                    break;
                case 3:
                    $product->setShow(0);
                    break;
                default:
                    break;
            }
            // If the product has stock
        } else {
            // If the product is available
            if ($product->isAvailable() > 0) {
                // Set availability message
                if ($product->getAvailabilityDays() == 0) {
                    if ($product->getQuantity() <= $product->getProductStock()) {
                        $product->setAvailability('IMEDIATA');
                    } else {
                        $deadline = $product->getProviderDeadline() ? $product->getProviderDeadline() : $product->getProductProviderDeadline();
                        $product->setAvailability('Em ' . $deadline . ' dias úteis');
                    }
                } else {
                    if ($product->getQuantity() <= $product->getProductStock()) {
                        $product->setAvailability('Em ' . $product->getAvailabilityDays() . ' dias úteis');
                    } else {
                        $deadline = $product->getProviderDeadline() ? $product->getProviderDeadline() : $product->getProductProviderDeadline();
                        $product->setAvailability('Em ' . $deadline . ' dias úteis');
                    }
                }
            }
        }

        return $product;
    }

    /**
     * @param $productId
     *
     * Update kit stock
     */
    public function updateKitStock($productId)
    {
        /* @var $kit B2cProdutosKits */

        // Get kits with this product
        $kits = $this->entityManager->getRepository('AppBundle:B2cProdutosKits')->getRelatedKitByProductId($productId);

        if (!empty($kits)) {
            foreach ($kits as $kit) {
                // Get the kit info on b2c_prdprd
                $prdprdKits = $this->entityManager->getRepository('AppBundle:B2cPrdprd')->getKit($kit->getIdKit());
                // Get generated kit stock info on VO
                $prdPrdKitVO = $this->manageKitStockInfo($prdprdKits);
                $prdPrdKitVO->setId($kit->getIdKit());
                // Update kit stock on b2c_prdprd
                $this->entityManager->getRepository('AppBundle:B2cPrdprd')->updateKitStock($prdPrdKitVO);

                // Get the kit info on b2c_view_produtos
                $viewPrdKits = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getKit($kit->getIdKit());
                // Get generated kit stock info on VO
                $viewPrdKitVO = $this->manageKitStockInfo($viewPrdKits);
                $viewPrdKitVO->setId($kit->getIdKit());
                // Update kit stock on b2c_view_produtos
                $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->updateKitStock($viewPrdKitVO);
            }
        }
    }

    /**
     * @param $kitsStockInfo
     * @return CreateProductVO
     *
     * Generate kit stock info
     */
    public function manageKitStockInfo($kitsStockInfo)
    {
        $stock = $providerStock = $combinedStock = $minStock = 9999999;
        $minStockActionArray = [0, 0, 0, 0];
        $showCounter = $providerProductDeadline = $providerDeadline = $availabilityDays = 0;
        $kitRealPrice = $kitPromotionPrice = $kitPromotionPriceWithPromotion = $kitMinStockAction = 0;
        $available = 1;
        $kitPromotionApply = true;
        $promotion = false;
        $initialPromotion = $finalPromotion = null;

        $productVO = new CreateProductVO();

        foreach ($kitsStockInfo as $kitStock) {
            $realPrice = $kitStock["pvreal"];
            $minStockAction = $kitStock["stqminAcao"];

            if (date("Y-m-d 00:00:00") >= $kitStock["promocaoini"] && date("Y-m-d 00:00:00") <= $kitStock["promocaofim"] && $kitStock["pvpromocao"] > 0) {
                $promotion = true;
                $initialPromotion = $kitStock["promocaoini"];
                $finalPromotion = $kitStock["promocaofim"];
                $productPrice = $promotionPrice = $kitStock["pvpromocao"];
            } else {
                $productPrice = $promotionPrice = $kitStock["pvreal"];
            }

            if ($stock > $kitStock['estoque'] && ($kitStock['estoque'] > 0 || $kitStock['stqminAcao'] == 2)) {
                $stock = $kitStock['estoque'];
            }

            if ($kitStock['stqminAcao'] == 2) {
                if ($providerStock > $kitStock["stqfornecedor"]) {
                    $providerStock = $kitStock["stqfornecedor"];
                }

                if ($combinedStock > ($kitStock["stqfornecedor"] + $kitStock['estoque'])) {
                    $combinedStock = $providerStock + $stock;
                }
            }

            if ($minStock > $kitStock['stqminimo']) {
                $minStock = $kitStock['stqminimo'];
            }

            if (($stock <= 0 && $minStockAction != 2) || $productPrice <= 0 || $kitStock["disponivel"] == 0 || ($stock <= 0 && $minStockAction == 2 && $providerStock <= 0)) {
                if ($kitStock["obrigatorio"]) {
                    $kitPromotionApply = false;
                }
            } else {
                if ($kitStock['mostra']) {
                    $minStockActionArray[$kitStock["stqminAcao"]]++;
                    $showCounter++;
                }
            }

            if ($providerProductDeadline < $kitStock["prazoFornecedor"]) {
                $providerProductDeadline = $kitStock["prazoFornecedor"];
            }

            if ($availabilityDays < $kitStock["dispDias"]) {
                $availabilityDays = $kitStock["dispDias"];
            }

            if ($kitStock['fornecedor']) {
                $provider = $this->entityManager->getRepository('AppBundle:B2cForfor')->findOneBy(array('id' => $kitStock['fornecedor']));

                if ($providerDeadline < $provider->getPrazo()) {
                    $providerDeadline = $provider->getPrazo();
                }
            }

            $kitRealPrice += $realPrice * $kitStock["quantidade"];
            $kitPromotionPrice += $promotionPrice * $kitStock["quantidade"];
            $kitPromotionPriceWithPromotion += ($productPrice - round($productPrice * ($kitStock["desconto"] / 100), 2)) * $kitStock["quantidade"];
        }

        if ($showCounter < 2) {
            $available = $stock = $providerStock = $combinedStock = $minStock = 0;
            $kitMinStockAction = 3;
        } else {
            $minStockActionArray = array_filter($minStockActionArray);

            if (count($minStockActionArray) == 1) {
                $minStockActionArrayKeys = array_keys($minStockActionArray);
                $kitMinStockAction = $minStockActionArrayKeys[0];
            } else {
                if (array_key_exists(2, $minStockActionArray)) {
                    $kitMinStockAction = 2;
                } else {
                    if (array_key_exists(1, $minStockActionArray)) {
                        $kitMinStockAction = 1;
                    } else {
                        $kitMinStockAction = 3;
                    }
                }
            }
        }

        if ($kitPromotionApply && $kitPromotionPrice > $kitPromotionPriceWithPromotion) {
            $kitPromotionPrice = $kitPromotionPriceWithPromotion;

            if (!$promotion) {
                $promotion = true;
                $initialPromotion = new \DateTime('now');
                $finalPromotion = new \DateTime('now');
                $finalPromotion = $finalPromotion->modify('+1 day');
            }
        }

        if (!$promotion) {
            $kitPromotionPrice = 0;
        }

        if ($stock == 0 && $kitMinStockAction == 2) {
            $providerStock = $combinedStock;
        } elseif ($kitMinStockAction != 2) {
            $providerStock = 0;
        }

        $productVO->setProductStock($stock);
        $productVO->setProviderStock($providerStock);
        $productVO->setMinStock($minStock);
        $productVO->setMinStockAction($kitMinStockAction);
        $productVO->setRealPrice($kitRealPrice);
        $productVO->setPromotionPrice($kitPromotionPrice);
        $productVO->setAvailabilityDays($availabilityDays);
        $productVO->setProviderDeadline($providerProductDeadline);
        $productVO->setProductProviderDeadline($providerDeadline);
        $productVO->setInitialPromotion($initialPromotion);
        $productVO->setFinalPromotion($finalPromotion);
        $productVO->setShow($showCounter >= 2);
        $productVO->setPromotion($promotion);
        $productVO->setProductPrice($promotion ? $kitPromotionPrice : $kitRealPrice);
        $productVO->setAvailable($available);
        $productVO->setProductDeadline($availabilityDays);

        return $this->getAvailabilityInfo($productVO);
    }

    /**
     * @param $orderId
     * @param $marketplaceId
     * @param $channelId
     * @param CreateProductVO $product
     * @return bool
     *
     * Reduce the product stock manually
     */
    public function reduceMarketPlaceStock($orderId, $marketplaceId, $channelId, CreateProductVO $product)
    {
        $marketPlaceStock = null;
        $idAttrStock = $this->marketPlaceService->getMarketplaceAttrStock($channelId);

        // Key to enable reducing stock on specific marketplace channel attribute
        if ($this->marketplaceChannelStock) {
            if ($idAttrStock > 0) {
                // Get product stock info
                $stockAttribute = $this->entityManager->getRepository('AppBundle:B2cAtributoValor')->findOneBy(array(
                    'b2cPrdprd' => $product->getId(),
                    'idAtributo' => $idAttrStock
                ));

                if ($stockAttribute && $stockAttribute->getRespostas() > 0 && $stockAttribute->getRespostas() >= $product->getQuantity()) {
                    // Update the product stock
                    $this->entityManager->getRepository('AppBundle:B2cAtributoValor')->updateMarketplaceStock(
                        $stockAttribute,
                        $product->getQuantity()
                    );

                    $marketPlaceStock = intval($stockAttribute->getRespostas());
                }
            }
        }

        if ($marketPlaceStock === null) {
            // Update the product stock
            $this->entityManager->getRepository('AppBundle:B2cPrdprdMarketplace')->updateProductStock(
                $product->getId(),
                $product->getQuantity(),
                $marketplaceId
            );

            // Get product stock and deadline info
            $marketPlaceStock = $this->entityManager->getRepository('AppBundle:B2cPrdprdMarketplace')->getProductStock(
                $product->getId(),
                $marketplaceId
            );
        }

        // Update the stock info in b2c_pedprd
        $this->entityManager->getRepository('AppBundle:B2cPedPrd')->setMarketplaceStockInfo(
            $orderId,
            $product->getId(),
            $marketPlaceStock
        );

        return true;
    }

    /**
     * @param ValidateProductDate $productDate
     * @param OrderVO $orderVO
     * @return UpdateProductsDateVO
     *
     * Create a UpdateProductsDateVO
     */
    public function getCommonInfoProductDate(ValidateProductDate $productDate, OrderVO $orderVO)
    {
        try {
            $updateProductDateVO = new UpdateProductsDateVO();

            $updateProductDateVO->setOrderId($productDate->getOrderId());
            $updateProductDateVO->setClerk($productDate->getClerk());
            //check condition by products
            if (!empty($productDate->getProducts())) {
                //search for order products
                /* @var $product ValidateProductDateProduct */
                foreach ($productDate->getProducts() as $product) {
                    $found = false;
                    /* @var $orderProduct OrderProductVO */
                    foreach ($orderVO->getProducts() as $orderProduct) {
                        //check product
                        if ($product->getId() == $orderProduct->getProductId() || $product->getId() == $orderProduct->getReference()) {
                            $found = $orderProduct->getProductId();

                            $updateProductsDateProductVO = new UpdateProductsDateProductVO();
                            $updateProductsDateProductVO->setId($found);
                            $updateProductDateVO->addProduct($updateProductsDateProductVO);
                        }
                    }

                    if (!$found) {
                        throw new Exception('Product ' . $product->getId() . ' not found');
                    }
                }
                //add all products
            } else {
                /* @var $product OrderProductVO */
                foreach ($orderVO->getProducts() as $product) {
                    $updateProductsDateProductVO = new UpdateProductsDateProductVO();
                    $updateProductsDateProductVO->setId($product->getProductId());
                    $updateProductDateVO->addProduct($updateProductsDateProductVO);
                }
            }
            /* @var $dateType ValidateProductDateType */
            foreach ($productDate->getDateTypes() as $dateType) {
                $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
                $updateProductsDateTypeVO->setType($dateType->getType());
                $updateProductsDateTypeVO->setDate($dateType->getDate());
                //add UpdateProductsDateTypeVO
                $updateProductDateVO->addDateType($updateProductsDateTypeVO);
            }

            return $updateProductDateVO;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param ProductDate $productDate
     * @param OrderVO $orderVO
     * @return UpdateProductsDateVO
     *
     * Create a UpdateProductsDateVO
     */
    public function getCommonInfoRemoveDate(ProductDate $productDate, OrderVO $orderVO)
    {
        try {
            $updateProductDateVO = new UpdateProductsDateVO();

            $updateProductDateVO->setOrderId($productDate->getOrderId());
            $updateProductDateVO->setClerk($productDate->getClerk());
            //check condition by products
            if (!empty($productDate->getProducts())) {
                //search for order products
                /* @var $product ValidateProductDateProduct */
                foreach ($productDate->getProducts() as $product) {
                    $found = false;
                    /* @var $orderProduct OrderProductVO */
                    foreach ($orderVO->getProducts() as $orderProduct) {
                        //check product
                        if ($product->getId() == $orderProduct->getProductId() || $product->getId() == $orderProduct->getReference()) {
                            $found = $orderProduct->getProductId();

                            $updateProductsDateProductVO = new UpdateProductsDateProductVO();
                            $updateProductsDateProductVO->setId($found);
                            $updateProductDateVO->addProduct($updateProductsDateProductVO);
                        }
                    }

                    if (!$found) {
                        throw new Exception('Product ' . $product->getId() . ' not found');
                    }
                }
                //add all products
            } else {
                /* @var $product OrderProductVO */
                foreach ($orderVO->getProducts() as $product) {
                    $updateProductsDateProductVO = new UpdateProductsDateProductVO();
                    $updateProductsDateProductVO->setId($product->getProductId());
                    $updateProductDateVO->addProduct($updateProductsDateProductVO);
                }
            }
            /* @var $dateType ValidateProductDateType */
            foreach ($productDate->getDateTypes() as $dateType) {
                $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
                $updateProductsDateTypeVO->setType($dateType->getType());
                //add UpdateProductsDateTypeVO
                $updateProductDateVO->addDateType($updateProductsDateTypeVO);
            }

            return $updateProductDateVO;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param UpdateProductsDateVO $updateProductsDateVO
     * @param OrderVO $orderVO
     * @return bool
     * @throws \Exception
     *
     * Update Date
     */
    public function updateDate(UpdateProductsDateVO $updateProductsDateVO, OrderVO $orderVO = null)
    {
        // Start transaction
        $this->entityManager->getConnection()->beginTransaction();

        try {
            if (!empty($updateProductsDateVO->getProducts())) {
                /* @var $product UpdateProductsDateProductVO */
                foreach ($updateProductsDateVO->getProducts() as $product) {
                    // Find prdStatus
                    /* @var $productStatus B2cPedprdstatus */
                    $productStatus = $this->entityManager->getRepository('AppBundle:B2cPedprdstatus')->findOneBy(array(
                        'idPedped' => $updateProductsDateVO->getOrderId(),
                        'idPrdprd' => $product->getId()
                    ));
                    if ($productStatus) {
                        /* @var $dateType UpdateProductsDateTypeVO */
                        foreach ($updateProductsDateVO->getDateTypes() as $dateType) {
                            //check if it is predict date
                            if ($updateProductsDateVO->isPredict()) {
                                // Check type
                                switch ($dateType->getType()) {
                                    case 'approvalDate':
                                        $productStatus->setDataApr($dateType->getDate());
                                        break;
                                    case 'releaseDate':
                                        $productStatus->setDataLib($dateType->getDate());
                                        break;
                                    case 'supplierBillingDate':
                                        $productStatus->setDataFatForn($dateType->getDate());
                                        break;
                                    case 'supplierInvoiceDate':
                                        $productStatus->setDataEntdForn($dateType->getDate());
                                        break;
                                    case 'supplierDeliveryDate':
                                        $productStatus->setDataEntForn($dateType->getDate());
                                        break;
                                    case 'shippingDate':
                                        $productStatus->setDataEmb($dateType->getDate());
                                        break;
                                    case 'invoiceDate':
                                        $productStatus->setDataNf($dateType->getDate());
                                        break;
                                    case 'deliveryDate':
                                        $productStatus->setDataEnt($dateType->getDate());
                                        break;
                                    default:
                                        break;
                                }
                            }
                            if (!$updateProductsDateVO->isPredict()) {
                                // Check type
                                switch ($dateType->getType()) {
                                    case 'approvalDate':
                                        $productStatus->setDataAprR($dateType->getDate());
                                        break;
                                    case 'releaseDate':
                                        $productStatus->setDataLibR($dateType->getDate());
                                        break;
                                    case 'supplierBillingDate':
                                        $productStatus->setDataFatFornR($dateType->getDate());
                                        break;
                                    case 'supplierInvoiceDate':
                                        $productStatus->setDataEntdFornR($dateType->getDate());
                                        break;
                                    case 'supplierDeliveryDate':
                                        $productStatus->setDataEntFornR($dateType->getDate());
                                        break;
                                    case 'invoiceDate':
                                        $productStatus->setDataNfR($dateType->getDate());
                                        $supplierDeliveryDate = $dateType->getDate();
                                        if ($supplierDeliveryDate instanceof \DateTime) {
                                            $supplierDeliveryDate = date_create_from_format('Y-m-d',
                                                $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay($supplierDeliveryDate->format('Y-m-d'),
                                                    1));
                                        }
                                        // calc and set delivery date
                                        $productStatus->setDataEntFornR($supplierDeliveryDate);
                                        //get products
                                        /* @var $orderProduct OrderProductVO */
                                        foreach ($orderVO->getProducts() as $orderProduct) {
                                            //check id
                                            if ($orderProduct->getProductId() == $product->getId()) {
                                                //check immediate delivery
                                                if ($orderProduct->isImmediateDelivery()) {
                                                    $productStatus->setDataLibR($productStatus->getDataLib());
                                                    $productStatus->setDataFatFornR($dateType->getDate());
                                                    $productStatus->setDataEntdFornR($dateType->getDate());
                                                }
                                                break;
                                            }
                                        }
                                        break;
                                    case 'shippingDate':
                                        $productStatus->setDataEmbR($dateType->getDate());
                                        break;
                                    case 'deliveryDate':
                                        if ($dateType->getDate() instanceof \DateTime && !$productStatus->getDataEmbR() instanceof \DateTime) {
                                            throw new Exception('Shipping date is not set for product ' . $product->getId());
                                        }
                                        $productStatus->setDataEntR($dateType->getDate());
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        if (!$updateProductsDateVO->isPredict()) {
                            $productStatus->setStatusMsg(null);
                            $productStatus->setStatusData(null);
                            //set product status
                            if ($productStatus->getDataEntR()) {
                                $productStatus->setStatusMsg('Entrega confirmada');
                                $productStatus->setStatusData($productStatus->getDataEntR());
                            } elseif ($productStatus->getDataCteR()) {
                                $productStatus->setStatusMsg('Embarque confirmado transportadora (CTE emitido)');
                                $productStatus->setStatusData($productStatus->getDataCteR());
                            } elseif ($productStatus->getDataEmbR()) {
                                $productStatus->setStatusMsg('Embarque realizado (Geração Romaneio)');
                                $productStatus->setStatusData($productStatus->getDataEmbR());
                            } elseif ($productStatus->getDataEntFornR()) {
                                $productStatus->setStatusMsg('Recebido fisicamente');
                                $productStatus->setStatusData($productStatus->getDataEntFornR());
                            } elseif ($productStatus->getDataNfR()) {
                                $productStatus->setStatusMsg('NF emitida');
                                $productStatus->setStatusData($productStatus->getDataNfR());
                            } elseif ($productStatus->getDataFatFornR()) {
                                $productStatus->setStatusMsg('Faturado indústria');
                                $productStatus->setStatusData($productStatus->getDataFatFornR());
                            } elseif ($productStatus->getDataEntdFornR()) {
                                $productStatus->setStatusMsg('Recebido digitalmente');
                                $productStatus->setStatusData($productStatus->getDataEntdFornR());
                            } elseif ($productStatus->getDataLibR()) {
                                $productStatus->setStatusMsg('Aguardando faturamento da indústria');
                                $productStatus->setStatusData($productStatus->getDataLibR());
                            }
                        }
                        //save
                        $this->entityManager->persist($productStatus);
                        $this->entityManager->flush();
                    } else {
                        throw new Exception('Product Status' . $product->getId() . ' not found');
                    }
                }
                // Try and commit the transaction
                $this->entityManager->getConnection()->commit();
                //set updated order date
                $this->entityManager->getRepository('AppBundle:B2cPedped')
                    ->updateOrderUpdateDate($updateProductsDateVO->getOrderId());
                //send event
                $this->eventService->send('ProductChangeDate', $updateProductsDateVO);
                // non predict dates
                if (!$updateProductsDateVO->isPredict() && $orderVO instanceof OrderVO) {
                    //updateOrderStatus
                    $this->orderStatusService->autoUpdateDatesAndStatus($orderVO, $updateProductsDateVO->getClerk());
                }
            }
        } catch (Exception $e) {
            if ($this->entityManager->getConnection()->getTransactionNestingLevel()) {
                // Rollback the failed transaction attempt
                $this->entityManager->getConnection()->rollback();
            }
            throw new Exception($e->getMessage());
        }

        return true;
    }

    /**
     * @param UpdateProductsDateVO $updateProductsDateVO
     * @param OrderVO $orderVO
     * @return bool
     * @throws \Exception
     *
     * Delete Date
     */
    public function deleteDate(UpdateProductsDateVO $updateProductsDateVO, OrderVO $orderVO = null)
    {
        // Start transaction
        $this->entityManager->getConnection()->beginTransaction();

        try {
            if (!empty($updateProductsDateVO->getProducts())) {
                /* @var $product UpdateProductsDateProductVO */
                foreach ($updateProductsDateVO->getProducts() as $product) {
                    // Find prdStatus
                    /* @var $productStatus B2cPedprdstatus */
                    $productStatus = $this->entityManager->getRepository('AppBundle:B2cPedprdstatus')->findOneBy(array(
                        'idPedped' => $updateProductsDateVO->getOrderId(),
                        'idPrdprd' => $product->getId()
                    ));
                    if ($productStatus) {
                        /* @var $dateType UpdateProductsDateTypeVO */
                        foreach ($updateProductsDateVO->getDateTypes() as $dateType) {
                            // Check type
                            switch ($dateType->getType()) {
                                case 'approvalDate':
                                    $productStatus->setDataAprR(null);
                                    break;
                                case 'releaseDate':
                                    $productStatus->setDataLibR(null);
                                    break;
                                case 'supplierBillingDate':
                                    $productStatus->setDataFatFornR(null);
                                    break;
                                case 'supplierInvoiceDate':
                                    $productStatus->setDataEntdFornR(null);
                                    break;
                                case 'supplierDeliveryDate':
                                    $productStatus->setDataEntFornR(null);
                                    break;
                                case 'invoiceDate':
                                    $productStatus->setDataNfR(null);
                                    break;
                                case 'shippingDate':
                                    $productStatus->setDataEmbR(null);
                                    break;
                                case 'deliveryDate':
                                    $productStatus->setDataEntR(null);
                                    break;
                                default:
                                    break;
                            }
                        }

                        $productStatus->setStatusMsg(null);
                        $productStatus->setStatusData(null);
                        //set product status
                        if ($productStatus->getDataEntR()) {
                            $productStatus->setStatusMsg('Entrega confirmada');
                            $productStatus->setStatusData($productStatus->getDataEntR());
                        } elseif ($productStatus->getDataCteR()) {
                            $productStatus->setStatusMsg('Embarque confirmado transportadora (CTE emitido)');
                            $productStatus->setStatusData($productStatus->getDataCteR());
                        } elseif ($productStatus->getDataEmbR()) {
                            $productStatus->setStatusMsg('Embarque realizado (Geração Romaneio)');
                            $productStatus->setStatusData($productStatus->getDataEmbR());
                        } elseif ($productStatus->getDataEntFornR()) {
                            $productStatus->setStatusMsg('Recebido fisicamente');
                            $productStatus->setStatusData($productStatus->getDataEntFornR());
                        } elseif ($productStatus->getDataNfR()) {
                            $productStatus->setStatusMsg('NF emitida');
                            $productStatus->setStatusData($productStatus->getDataNfR());
                        } elseif ($productStatus->getDataFatFornR()) {
                            $productStatus->setStatusMsg('Faturado indústria');
                            $productStatus->setStatusData($productStatus->getDataFatFornR());
                        } elseif ($productStatus->getDataEntdFornR()) {
                            $productStatus->setStatusMsg('Recebido digitalmente');
                            $productStatus->setStatusData($productStatus->getDataEntdFornR());
                        } elseif ($productStatus->getDataLibR()) {
                            $productStatus->setStatusMsg('Aguardando faturamento da indústria');
                            $productStatus->setStatusData($productStatus->getDataLibR());
                        }

                        //save
                        $this->entityManager->persist($productStatus);
                        $this->entityManager->flush();
                    } else {
                        throw new Exception('Product Status' . $product->getId() . ' not found');
                    }
                }
                // Try and commit the transaction
                $this->entityManager->getConnection()->commit();
                //set updated order date
                $this->entityManager->getRepository('AppBundle:B2cPedped')
                    ->updateOrderUpdateDate($updateProductsDateVO->getOrderId());
                //send event
                $this->eventService->send('ProductChangeDate', $updateProductsDateVO);
                // non predict dates
                if ($orderVO instanceof OrderVO) {
                    //updateOrderStatus
                    $this->orderStatusService->autoUpdateDatesAndStatus($orderVO, $updateProductsDateVO->getClerk());
                }
            }
        } catch (Exception $e) {
            if ($this->entityManager->getConnection()->getTransactionNestingLevel()) {
                // Rollback the failed transaction attempt
                $this->entityManager->getConnection()->rollback();
            }
            throw new Exception($e->getMessage());
        }

        return true;
    }

    /**
     * @param CreateProductVO $product
     * @param $dateRef
     * @param $zipCode
     * @param $qty
     * @return CreateProductVO Get Product Dates
     * @throws \Exception
     *
     * Get Product Dates
     */
    public function getProductDates(CreateProductVO $product, $dateRef, $zipCode, $qty)
    {
        /* @var $product CreateProductVO */
        $product = $this->getAvailabilityInfo($product);

        // get dates
        $productDates = $this->orderDateService->calculateProductDates($product->getId(), $dateRef, $zipCode, $qty);

        // Check Immediate Delivery
        $immediateDelivery = false;
        if ($product->getAvailability() == 'IMEDIATA') {
            $immediateDelivery = true;
        }
        // Check Immediate Delivery
        if (!$immediateDelivery) {
            //set setAvailabilityDays
            $product->setAvailabilityDays($productDates['availabilityDeadline']);
        }

        $product->setProviderDeadline($productDates['providerDeadLine']);
        $product->setAvailabilityDeadline($productDates['availabilityDeadline']);
        $product->setProviderDeliveryDate($productDates['predictDates']['supplierDeliveryDate']);
        $product->setDeliveryDate($productDates['predictDates']['deliveryDate']);

        return $product;
    }

    /**
     * @param UpdateValidateProductStatus $productStatus
     * @param OrderVO $orderVO
     * @return UpdateProductsStatusVO
     *
     * Create a UpdateProductsDateVO
     */
    public function getCommonInfoProductStatus(UpdateValidateProductStatus $productStatus, OrderVO $orderVO)
    {
        try {
            $updateProductStatusVO = new UpdateProductsStatusVO();

            $updateProductStatusVO->setOrderId($productStatus->getOrderId());
            $updateProductStatusVO->setClerk($productStatus->getClerk());
            $updateProductStatusVO->setCancelled($productStatus->isCancelled());
            $updateProductStatusVO->setTicket($productStatus->getTicket());
            $updateProductStatusVO->setOcrCode($productStatus->getOcrCode());
            $updateProductStatusVO->setCancellationIndicator($productStatus->getCancellationIndicator());
            $updateProductStatusVO->setTicketType($productStatus->getTicketType());
            $updateProductStatusVO->setHub($productStatus->getHub());
            $updateProductStatusVO->setShipper($productStatus->getShipper());
            $updateProductStatusVO->setInvoice($productStatus->getInvoice());
            $updateProductStatusVO->setProductCauser($productStatus->getProductCauser());
            $updateProductStatusVO->setRefundForm($productStatus->getRefundForm());

            //check condition by products
            if (!empty($productStatus->getProducts())) {
                //search for order products
                /* @var $product UpdateValidateProductStatusProduct */
                foreach ($productStatus->getProducts() as $product) {
                    $found = false;
                    /* @var $orderProduct OrderProductVO */
                    foreach ($orderVO->getProducts() as $orderProduct) {
                        //check product
                        if ($product->getId() == $orderProduct->getProductId() || $product->getId() == $orderProduct->getReference()) {
                            $found = $orderProduct->getProductId();

                            $updateProductStatusProductVO = new UpdateProductsStatusProductVO();
                            $updateProductStatusProductVO->setId($orderProduct->getProductId());
                            $updateProductStatusProductVO->setReference($orderProduct->getReference());
                            $updateProductStatusProductVO->setQuantity($orderProduct->getQuantity());
                            $updateProductStatusVO->addProduct($updateProductStatusProductVO);
                        }
                    }

                    if (!$found) {
                        throw new Exception('Product ' . $product->getId() . ' not found');
                    }
                }
                //add all products
            } else {
                /* @var $product OrderProductVO */
                foreach ($orderVO->getProducts() as $product) {
                    $updateProductStatusProductVO = new UpdateProductsStatusProductVO();
                    $updateProductStatusProductVO->setId($product->getProductId());
                    $updateProductStatusProductVO->setReference($product->getReference());
                    $updateProductStatusProductVO->setQuantity($product->getQuantity());
                    $updateProductStatusVO->addProduct($updateProductStatusProductVO);
                }
            }

            return $updateProductStatusVO;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param UpdateValidateProductReversalInfo $productStatus
     * @param OrderVO $orderVO
     * @return UpdateProductsReversalInfoVO
     *
     * Create a UpdateProductsDateVO
     */
    public function getCommonInfoProductReversal(UpdateValidateProductReversalInfo $productStatus, OrderVO $orderVO)
    {
        try {
            $updateProductStatusVO = new UpdateProductsReversalInfoVO();

            $updateProductStatusVO->setOrderId($productStatus->getOrderId());
            $updateProductStatusVO->setClerk($productStatus->getClerk());
            $updateProductStatusVO->setTypeReversal($productStatus->getTypeReversal());
            $updateProductStatusVO->setOperator($productStatus->getOperator());
            $updateProductStatusVO->setTransaction($productStatus->getTransaction());

            //check condition by products
            if (empty($productStatus->getProducts())) {
                /* @var $product OrderProductVO */
                foreach ($orderVO->getProducts() as $product) {
                    $updateProductStatusProductVO = new UpdateProductsStatusProductVO();
                    $updateProductStatusProductVO->setId($product->getProductId());
                    $updateProductStatusProductVO->setReference($product->getReference());
                    $updateProductStatusProductVO->setQuantity($product->getQuantity());
                    $updateProductStatusVO->addProduct($updateProductStatusProductVO);
                }

                return $updateProductStatusVO;
            }

            //search for order products
            /* @var $product UpdateValidateProductStatusProduct */
            foreach ($productStatus->getProducts() as $product) {
                $found = false;
                /* @var $orderProduct OrderProductVO */
                foreach ($orderVO->getProducts() as $orderProduct) {
                    //check product
                    if ($product->getId() == $orderProduct->getProductId() || $product->getId() == $orderProduct->getReference()) {
                        $found = $orderProduct->getProductId();

                        $updateProductStatusProductVO = new UpdateProductsStatusProductVO();
                        $updateProductStatusProductVO->setId($orderProduct->getProductId());
                        $updateProductStatusProductVO->setReference($orderProduct->getReference());
                        $updateProductStatusProductVO->setQuantity($orderProduct->getQuantity());
                        $updateProductStatusVO->addProduct($updateProductStatusProductVO);
                    }
                }

                if (!$found) {
                    throw new Exception('Product ' . $product->getId() . ' not found');
                }
            }
            //add all products

            return $updateProductStatusVO;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Updates order product status
     *
     * @param OrderVO $orderVO
     * @param $productId
     * @param UpdateValidateProductStatus $validProductStatus
     */
    public function updateProductStatus(OrderVO $orderVO, $productId, UpdateValidateProductStatus $validProductStatus)
    {
        try {
            $validateProductStatusProduct = new UpdateValidateProductStatusProduct();
            $validateProductStatusProduct->setId($productId);
            $validProductStatus->addProduct($validateProductStatusProduct);

            // Generate common info to all the order types
            $upProductStatusVO = $this->getCommonInfoProductStatus($validProductStatus, $orderVO);

            //Update status
            $this->updateStatus($upProductStatusVO, $orderVO);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param UpdateProductsStatusVO $updateProductsStatusVO
     * @param OrderVO $orderVO
     *
     * Update Date
     * @return bool
     */
    public function updateStatus(UpdateProductsStatusVO $updateProductsStatusVO, OrderVO $orderVO)
    {
        $this->logger->info('[Update Status] - Pedido ' . $orderVO->getOrderId() . ' entrou no ProductService::updateStatus().');
        try {
            // Start transaction
            $this->entityManager->getConnection()->beginTransaction();

            /* @var $product UpdateProductsStatusProductVO */
            foreach ($updateProductsStatusVO->getProducts() as $product) {
                /* @var $orderProductItem OrderProductVO */
                foreach ($orderVO->getProducts() as $key => $orderProductItem) {
                    //check id
                    if ($product->getId() == $orderProductItem->getProductId()) {
                        //get product
                        /* @var $orderProduct B2cPedprd */
                        $orderProduct = $this->entityManager->getRepository('AppBundle:B2cPedprd')->findOneBy(array(
                            'idPedped' => $orderVO->getOrderId(),
                            'idPrdprd' => $product->getId()
                        ));
                        $this->logger->info('[Update Status] - Pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId() . ' será cancelado.');
                        //change cancelled status
                        if ($orderProduct->getItemCancelado() != $updateProductsStatusVO->isCancelled()) {
                            $this->logger->info('[Update Status] - Pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId() . ' não está cancelado, vamos cancelar...');
                            //set status cancelled
                            $orderProduct->setItemCancelado($updateProductsStatusVO->isCancelled());
                            //check cancelled date
                            $dtaCancelled = null;
                            if ($updateProductsStatusVO->isCancelled() == true) {
                                $this->logger->info('[Update Status] - Atualização do estoque para pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
                                //set dta Cancelled
                                $dtaCancelled = new \DateTime('now');
                                //return supplier stock
                                if ($orderProductItem->getSupplierStock() > 0) {
                                    $this->logger->info('[Update Status] - Atualização do estoque do fornecedor para pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
                                    // Increase provider stock on b2c_prdprd
                                    $this->entityManager->getRepository('AppBundle:B2cPrdprd')->increaseProviderStock(
                                        $orderProductItem->getProductId(),
                                        $orderProductItem->getQuantity(),
                                        $updateProductsStatusVO->getClerk()
                                    );
                                    // Increase provider stock on b2c_view_produtos
                                    $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->increaseProviderStock(
                                        $orderProductItem->getProductId(),
                                        $orderProductItem->getQuantity(),
                                        $updateProductsStatusVO->getClerk()
                                    );
                                }
                                //return branch stock
                                if ($orderProductItem->getProductStock() > 0) {
                                    $this->logger->info('[Update Status] - Atualização do estoque da loja para pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
                                    // Increase store stock on b2c_prdestoque
                                    $this->entityManager->getRepository('AppBundle:B2cPrdestoque')->increaseStock(
                                        $orderProductItem->getProductId(),
                                        $orderProductItem->getQuantity(),
                                        $orderProductItem->getBranchStockId()
                                    );
                                    // Increase store stock on b2c_view_produtos
                                    $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->increaseStoreStock(
                                        $orderProductItem->getProductId(),
                                        $orderProductItem->getQuantity(),
                                        $updateProductsStatusVO->getClerk()
                                    );
                                }

                                $this->logger->info('[Update Status] - Encerrou atualização do estoque para pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
                                // Get product on b2c_view_produtos
                                /* @var $viewProduct B2cViewProdutos */
                                $viewProduct = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->findOneBy(array('id' => $product->getId()));

                                // Create product VO to get availability info
                                $productVO = new CreateProductVO();
                                $productVO->setProductStock(intval($viewProduct->getEstoque()));
                                $productVO->setMinStockAction(intval($viewProduct->getStqminAcao()));
                                $productVO->setProviderStock(intval($viewProduct->getStqfornecedor()));
                                $productVO->setProductProviderDeadline(intval($viewProduct->getPrdPrazoFornecedor()));
                                $productVO->setProviderDeadline(intval($viewProduct->getPrazoFornecedor()));
                                $productVO->setAvailabilityDays(intval($viewProduct->getDispDias()));
                                $productVO->setAvailable($viewProduct->getDisponivel());
                                $productVO->setOut($viewProduct->getEsgotado());
                                $productVO->setShow($viewProduct->getMostra());
                                $productVO->setAction($viewProduct->getAcao());

                                $availabilityInfo = $this->getAvailabilityInfo($productVO);
                                $this->logger->info('[Update Status] - Atualizando disponibilidade para pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
                                // Update product availability info on b2c_view_produtos
                                $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->updateProductAvailabilityInfo(
                                    $viewProduct->getId(),
                                    $availabilityInfo,
                                    $updateProductsStatusVO->getClerk()
                                );
                                $this->logger->info('[Update Status] - Atualizando produto - Pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
                                // Update product alteration date on b2c_prdprd
                                $this->entityManager->getRepository('AppBundle:B2cPrdprd')->updateProduct(
                                    $viewProduct->getId(),
                                    $updateProductsStatusVO->getClerk()
                                );
                                $this->logger->info('[Update Status] - Atualizando kit estoque para pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
                                // Update kit's stock that has the product included
                                $this->updateKitStock($product->getId());

                                // insert order and product into b2c_pedprd_estorno
                                //calc discounbt
                                $totalDiscount = $orderVO->getDiscount();
                                if (strpos($orderVO->getCoupon(), 'SAP') === false) {
                                    $totalDiscount += ($orderVO->getCouponAmount());
                                }

                                $percDiscount = round(($totalDiscount / $orderVO->getSubtotal()) * 100, 2);
                                //calc product amount
                                $productAmount = $orderProduct->getPrdPvtotal() - (round($orderProduct->getPrdPvtotal() * ($percDiscount / 100), 2));
                                //calc shipping amount
                                $shippingAmount = round(($orderProduct->getPrdPvtotal() / $orderVO->getSubtotal()) * $orderVO->getShippingAmount(), 2);

                                $this->logger->info('[Update Status] - Inserindo estorno para pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
                                //save history
                                $this->entityManager->getRepository('AppBundle:B2cPedprdEstorno')->insertOrderProductReversal(
                                    $updateProductsStatusVO,
                                    $orderProductItem->getProductId(),
                                    $productAmount,
                                    $shippingAmount
                                );

                                $orderLogVO = new CreateOrderLogVO();
                                $orderLogVO->setOrderId($orderVO->getOrderId());
                                $orderLogVO->setCreatedAt(new \DateTime('now'));
                                $orderLogVO->setTitle("Cancelamento de item");
                                $orderLogVO->setText("O item " . $orderProductItem->getProductId() . " foi cancelado por " . $updateProductsStatusVO->getClerk());
                                $orderLogVO->setShow(true);
                                $orderLogVO->setStatus($orderVO->getStatus()->getId());
                                $orderLogVO->setInternalStatus($orderVO->getInternalStatus()->getId());
                                // Calls to save log on database
                                $this->logger->info('[Update Status] - Inserindo log para pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
                                $this->orderLogService->insertOrderLog($orderLogVO);
                            } else {
                                $orderLogVO = new CreateOrderLogVO();
                                $orderLogVO->setOrderId($orderVO->getOrderId());
                                $orderLogVO->setCreatedAt(new \DateTime('now'));
                                $orderLogVO->setTitle("Cancelamento de item");
                                $orderLogVO->setText("O item " . $orderProductItem->getProductId() . " foi reativado por " . $updateProductsStatusVO->getClerk());
                                $orderLogVO->setShow(true);
                                $orderLogVO->setStatus($orderVO->getStatus()->getId());
                                $orderLogVO->setInternalStatus($orderVO->getInternalStatus()->getId());
                                // Calls to save log on database
                                $this->logger->info('[Update Status] - Produto não está cancelado, inserindo log para pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
                                $this->orderLogService->insertOrderLog($orderLogVO);
                            }
                            //set cancelled date
                            $orderProduct->setDataCancelamento($dtaCancelled);
                        }

                        //save
                        $this->entityManager->persist($orderProduct);
                        $this->entityManager->flush($orderProduct);

                        //update orderVO
                        $orderProductItem->setCancelled($updateProductsStatusVO->isCancelled());
                        $orderVO->addProduct($orderProductItem, $key);
                    }
                }
            }
            $this->logger->info('[Update Status] - Commit da transação para pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
            // Try and commit the transaction
            $this->entityManager->getConnection()->commit();
            $this->logger->info('[Update Status] - Enviando evento(ProductChangeStatus) para o Rabbit - Pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
            //send event
            $eventReturn = $this->eventService->send('ProductChangeStatus', $updateProductsStatusVO);
            $this->logger->info('[Update Status] - Evento(ProductChangeStatus) enviado para o Rabbit - Pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId() . ' / Retorno = ' . $eventReturn);
            //set updated order date
            $this->entityManager->getRepository('AppBundle:B2cPedped')->updateOrderUpdateDate($orderVO->getOrderId());
            $this->logger->info('[Update Status] - Chamou updateOrderUpdateDate - Pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
            //update status
            $this->orderStatusService->autoUpdateDatesAndStatus($orderVO, $updateProductsStatusVO->getClerk());
            $this->logger->info('[Update Status] - Encerrou autoUpdateDatesAndStatus - Pedido ' . $orderVO->getOrderId() . ', produto ' . $product->getId());
        } catch (Exception $e) {
            $this->logger->error('[Update Status] - Erro no meio do cancelamento, fazendo rollback.');
            if ($this->entityManager->getConnection()->getTransactionNestingLevel()) {
                // Rollback the failed transaction attempt
                $this->entityManager->getConnection()->rollback();
            }
            $this->logger->err($e->getTraceAsString());
            throw new Exception($e->getMessage());
        }

        return true;
    }

    /**
     * Method used to update the reversal (payback) info.
     *
     * @param UpdateProductsReversalInfoVO $updateProductsReversalInfoVO
     * @param OrderVO $orderVO
     *
     * @return bool
     * @throws Exception When the transaction fails.
     */
    public function approveReversal(UpdateProductsReversalInfoVO $updateProductsReversalInfoVO, OrderVO $orderVO)
    {
        // Start transaction
        $this->entityManager->getConnection()->beginTransaction();
        try {
            /* @var $product UpdateProductsStatusProductVO */
            foreach ($updateProductsReversalInfoVO->getProducts() as $product) {
                /* @var $orderProductItem OrderProductVO */
                foreach ($orderVO->getProducts() as $key => $orderProductItem) {
                    //check id
                    if ($product->getId() != $orderProductItem->getProductId()) {
                        continue;
                    }

                    //get reversal info
                    /* @var $reversalInfo B2cPedprdEstorno */
                    $reversalInfo = $this
                        ->entityManager->getRepository('AppBundle:B2cPedprdEstorno')
                        ->findOneBy(
                            array(
                                'idPedped' => $orderVO->getOrderId(),
                                'idPrdprd' => $product->getId()
                            ),
                            array('id' => 'DESC')
                        );

                    if (is_null($reversalInfo)) {
                        throw new Exception('Reversal line not found. Please contact our TI team.');
                    }

                    $reversalInfo->setIdForest($updateProductsReversalInfoVO->getTypeReversal());
                    $reversalInfo->setNumTransacao($updateProductsReversalInfoVO->getTransaction());
                    $reversalInfo->setOperadora($updateProductsReversalInfoVO->getOperator());
                    $reversalInfo->setEstornoLiberado(1);

                    //save
                    $this->entityManager->persist($reversalInfo);
                    $this->entityManager->flush($reversalInfo);
                }
            }
            // Try and commit the transaction
            $this->entityManager->getConnection()->commit();
            //send event
            $this->eventService->send('ProductReversalUpdateInfo', $updateProductsReversalInfoVO);
        } catch (Exception $e) {
            if ($this->entityManager->getConnection()->getTransactionNestingLevel()) {
                // Rollback the failed transaction attempt
                $this->entityManager->getConnection()->rollback();
            }
            $this->logger->err($e->getTraceAsString());
            throw new Exception($e->getMessage());
        }

        return true;
    }

    /**
     * @param $orderId
     * @param $productId
     * @return array
     * @throws \Exception
     */
    public function getProductOrderInfo($orderId, $productId)
    {
        try {
            $productInfo = array();
            $product = $this->entityManager->getRepository('AppBundle:B2cPedprd')->getByOrderAndProduct($orderId, $productId);

            if (empty($product)) {
                return $productInfo;
            }

            // Get product order status
            $pedprdstatus = $this->entityManager->getRepository('AppBundle:B2cPedprdstatus')
                ->getProductOrderStatus($orderId, $product['idPrdprd']);

            // Get order product status
            $status = $this->entityManager->getRepository('AppBundle:B2cPedprd')
                ->getStatus($product, $pedprdstatus);

            // Get production date to product
            $production = $this->entityManager->getRepository('AppBundle:B2cPedprd')
                ->getProduction($pedprdstatus);

            // Get all product invoices
            $invoices = $this->entityManager->getRepository('AppBundle:B2cNotafiscal')
                ->getInvoices($orderId, $product['prdReferencia']);

            // Get Shipper info
            $shipper = $this->entityManager->getRepository('AppBundle:B2cNotafiscalItem')
                ->getShipper($orderId, $product['prdReferencia']);

            // Get polo
            $polo = $this->entityManager->getRepository('AppBundle:B2cForfor')
                ->getPolo($product['prdFornecedor']);

            // Get Branch Office Information
            $branchOfficeInfo = $this->entityManager->getRepository('AppBundle:B2cPedprd')
                ->getBranchOfficeInformation($orderId, $product['idPrdprd']);

            // Format product info to response
            $productInfo = $this->entityManager->getRepository('AppBundle:B2cPedprd')
                ->formatProductInfo(
                    array("idPedped" => $product['idPedped']['idPedped']),
                    $product,
                    $pedprdstatus,
                    $status,
                    $production,
                    $polo,
                    $branchOfficeInfo,
                    $shipper,
                    $invoices
                );

            $productInfo['pdf_notafiscal'] = '';
            if (!empty($shipper['pathPdf'])) {
                $productInfo['pdf_notafiscal'] = $this->awsService->getS3AuthenticatedUrl($shipper['pathPdf']);
            }

            $this->entityManager->getConnection()->close();

            return $productInfo;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $orderId
     * @param $productId
     * @return B2cPedprdstatus|array
     */
    public function getOrderProductDates($orderId, $productId)
    {
        return $this->entityManager->getRepository('AppBundle:B2cPedprdstatus')->findOneBy(array('idPedped' => $orderId, 'idPrdprd' => $productId));
    }
    /**
     * Gets order product data from order id and product reference
     *
     * @param integer $orderId
     * @param string $productReference
     * @return B2cPedprd|null|object
     */
    public function getProductOrderInfoByReference($orderId, $productReference)
    {
        return $this->entityManager->getRepository('AppBundle:B2cPedprd')->findOneBy(array('idPedped' => $orderId, 'prdReferencia' => $productReference));
    }

    /**
     * @param $orderId
     * @param $productId
     * @return array
     * @throws \Exception
     */
    public function getOrderProductInvoice($orderId, $productId)
    {
        $invoices = $this->entityManager->getRepository('AppBundle:B2cNotafiscal')
            ->getInvoices($orderId, $product['prdReferencia']);
    }
}
