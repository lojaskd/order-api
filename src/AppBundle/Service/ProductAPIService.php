<?php

namespace AppBundle\Service;

use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class ProductAPIService
 * @package AppBundle\Service
 */
class ProductAPIService
{

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var string
     */
    private $endpoint;


    /**
     * @param Logger $logger
     * @param string $endpoint
     */
    public function __construct(Logger $logger, $endpoint)
    {
        $this->logger = $logger;
        $this->endpoint = $endpoint;
    }

    /**
     * @param $productId
     * @param $dateRef
     * @param string $zip
     * @param int $qty
     * @param null $immediateDelivery
     * @param null $supplierReservedStock
     * @return bool|mixed
     *
     * Get product dates from products API
     * @throws \Exception
     */
    public function getDeadlines($productId, $dateRef, $zip = '', $qty = 1, $immediateDelivery = null, $supplierReservedStock = null)
    {
        $url = 'products/' . $productId . '/deadlines/' . $dateRef . '?qty=' . $qty . '&zip=' . $zip;

        if ($immediateDelivery !== null) {
            $url .= '&immediate_delivery=' . (!$immediateDelivery ? 0 : 1);
        }

        if ($supplierReservedStock !== null) {
            $url .= '&supplier_reserved_stock=' . (!$supplierReservedStock ? 0 : 1);
        }

        if ($result = $this->call($url, array(), 'GET')) {
            return $result;
        }

        return false;
    }

    /**
     * @param $productId
     * @return bool|mixed
     *
     * Get product dates from products API
     * @throws \Exception
     */
    public function getProductInfo($productId)
    {
        if ($result = $this->call('products/' . $productId, array(), 'GET')) {
            return $result;
        }

        return false;
    }

    /**
     * @param $action
     * @param array $args
     * @param string $method
     * @return mixed
     * @throws \Exception
     */
    protected function call($action, $args = array(), $method = 'GET')
    {
        try {
            $submit_url = $this->endpoint . '/' . $action;
            $payload = json_encode($args);

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $submit_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Return from curl_exec rather than echoing
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // SSL verification problem workaround
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); // Follow redirects
            curl_setopt($curl, CURLOPT_FRESH_CONNECT, true); // Always ensure the connection is fresh
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("content-type: application/json"));
            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POST, 1); // Post fields amount -- always 1
            } elseif ($method == 'PUT') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            } elseif ($method == 'GET') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            }
            curl_setopt($curl, CURLOPT_POSTFIELDS, $payload); // Post data

            $result = curl_exec($curl);

            if (!$result) {
                curl_close($curl);
                throw new \Exception("No response from server", 500);
            }

            $meta = curl_getinfo($curl);
            if ($meta['http_code'] != 200) {
                curl_close($curl);
                throw new \Exception($result, $meta['http_code']);
            } else {
                $result = json_decode($result, true);
            }
            curl_close($curl);
        } catch (Exception $e) {
            $this->logger->error('Error calling API-Produtos: ' . $e->getMessage());
            throw $e;
        }

        return $result;
    }
}
