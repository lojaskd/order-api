<?php
namespace AppBundle\Service;

use Aws\S3\S3Client;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class AwsService
 * @package AppBundle\Service
 */
class AwsService
{

    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var string
     */
    private $awsId;
    /**
     * @var string
     */
    private $awsKey;


    /**
     * @param Logger $logger
     * @param string $awsId
     * @param string $awsKey
     */
    public function __construct(Logger $logger, $awsId, $awsKey)
    {
        $this->logger = $logger;
        $this->awsId = $awsId;
        $this->awsKey = $awsKey;
    }

    /**
     * Gets S3 url from path with authentication
     *
     * @param string $path
     * @return string
     */
    public function getS3AuthenticatedUrl($path)
    {
        $s3Client = new S3Client([
            'version'     => 'latest',
            'region'      => 'us-east-1',
            'credentials' => [
                'key'    => $this->awsId,
                'secret' => $this->awsKey,
            ],
        ]);

        $cmd = $s3Client->getCommand('GetObject', [
            'Bucket' => 'nfe-lkd.lojaskd.com.br',
            'Key'    => $path
        ]);

        $request = $s3Client->createPresignedRequest($cmd, '+60 minutes');

        return (string) $request->getUri();
    }
}
