<?php

namespace AppBundle\Service;

use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\ProductVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\Create\CreateShippingVO;
use AppBundle\Entity\VO\Create\CreateShippingProductVO;
use AppBundle\Entity\VO\Create\CreateShippingProviderVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class ShippingService
 *
 * @package AppBundle\Service
 */
class ShippingService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var bool
     */
    private $extraShipping;

    /**
     * @var string
     */
    private $extraShippingValue;

    /**
     * @var string
     */
    private $itemShipping;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var bool
     */
    private $freightProvider;

    /**
     * ShippingService constructor.
     * @param EntityManager $entityManager
     * @param $extraShipping
     * @param $extraShippingValue
     * @param $itemShipping
     * @param Logger $logger
     * @param $freightProvider
     */
    public function __construct(
        EntityManager $entityManager,
        $extraShipping,
        $extraShippingValue,
        $itemShipping,
        Logger $logger,
        $freightProvider
    ) {
        $this->entityManager = $entityManager;
        $this->extraShipping = $extraShipping;
        $this->extraShippingValue = $extraShippingValue;
        $this->itemShipping = $itemShipping;
        $this->logger = $logger;
        $this->freightProvider = $freightProvider;
    }

    /**
     * @param $order
     * @return CreateShippingVO
     * @throws \Doctrine\ORM\NonUniqueResultException
     * Calculate the shipping price
     */
    public function calculateShipping($order)
    {
        $shippingVO = new CreateShippingVO();

        if (!$order->getRegion()) {
            // TODO verificar caso não houver região e/ou transportadora
            return $shippingVO;
        }

        // Set the shipping data on shipping VO object
        $shippingVO->setRuleFreight('Frete não encontrado.');
        $shippingVO->setMounting(0);
        $shippingVO->setMountingValue(0);
        $shippingVO->setCostPrice(0);
        $shippingVO->setPrice(0);

        $shippingRepository = $this->entityManager->getRepository('AppBundle:B2cTratra');

        // Search cost price shipping
        $shippingEmployees = $shippingRepository->getShippingFromCategoriesAndRegion(
            $order->getRegion(),
            $order->getCategories()
        );

        if (empty($shippingEmployees)) {
            return $shippingVO;
        }

        // Get max cubage and weight that shipping support
        $maxCubage = $this->entityManager->getRepository('AppBundle:B2cRegmec')->getMaxCubage();
        $maxWeight = $this->entityManager->getRepository('AppBundle:B2cRegpeso')->getMaxWeight();
        $weightFreightShop = $order->getTotalWeight();
        $cubageFreightShop = $order->getTotalCubage();

        $shipping = $this->calculateEmployeesShipping(
            $shippingEmployees,
            $order,
            $weightFreightShop,
            $cubageFreightShop,
            $maxWeight,
            $maxCubage
        );

        if (empty($shipping)) {
            // TODO verificar caso não houver transportadora
            return $shippingVO;
        }

        // Set the shipping default data on shipping VO object
        $shippingVO->setId($shipping['idTratra']);
        $shippingVO->setName($shipping['nomePublico']);
        $shippingVO->setPattern($shipping['padrao']);
        $shippingVO->setShippingType($shipping['tipoFrete']);
        $shippingVO->setFixedDeadline($shipping['prazoFreteFixo']);
        $shippingVO->setFixedValue($shipping['valorFreteFixo']);
        $shippingVO->setFreeDeadline($shipping['prazoFreteGratuito']);
        $shippingVO->setShippingCompanyDeadline($shipping['prazoTransportadora']);
        $shippingVO->setSafety($shipping['seguro']);
        $shippingVO->setCostPrice($shipping['costValue']);
        $shippingVO->setSafetyPrice($shipping['safetyValue']);

        // Search for shipping promotion (Fretinho)
        $promotionsShipping = $this->entityManager
            ->getRepository('AppBundle:B2cPromo')->getActiveShippingPromotions(
                $order->getRegion()->getId(),
                $order->getCustomer()->getId(),
                implode(', ', $order->getProductsId()),
                implode(', ', $order->getPaymentsId()),
                implode(', ', $order->getProviders()),
                $order->getSubtotal()
            );

        if (!empty($promotionsShipping)) {
            $shippingPrice = 0;
            foreach ($promotionsShipping as $promotionShipping) {
                $applyPromotion = true;

                if ($promotionShipping['usarCategoria']) {
                    $promotionCategories = $this->entityManager->getRepository('AppBundle:B2cPromoCat')
                        ->getPromotionCategories($promotionShipping['idPromo']);
                    $promotionCategoriesList = explode(', ', array_pop($promotionCategories));

                    /* @var $product CreateProductVO */
                    foreach ($order->getProducts() as $product) {
                        if (is_array($product->getRelatedCategories())) {
                            $productCategories = array_merge(
                                array($product->getCategory()),
                                $product->getRelatedCategories()
                            );
                        } else {
                            $productCategories = array($product->getCategory());
                        }

                        if (empty(array_intersect($productCategories, $promotionCategoriesList))) {
                            $applyPromotion = false;
                            break;
                        }
                    }
                }

                if ($applyPromotion && $promotionShipping['freteFixo'] &&
                    $promotionShipping['freteFixoValor'] > $shippingPrice
                ) {
                    $shippingPrice = $promotionShipping['freteFixoValor'];

                    $shippingVO->setRuleFreight('Fretinho');
                    $shippingVO->addFreight('Fretinho');
                    // If there's a promotion shipping get its values
                    if ($promotionShipping['freteGratis']) {
                        $shippingVO->setShippingType(1);
                        $shippingVO->setPrice(0);
                    } elseif ($promotionShipping['freteFixo']) {
                        $shippingVO->setShippingType(2);
                        $shippingVO->setPrice($promotionShipping['freteFixoValor']);
                    }
                }
            }
        }

        // no promotions
        if (empty($shippingPromo)) {
            // If item shipping is set, calculate it
            if ($this->itemShipping) {
                $shippingVO = $this->calculateItemShipping($shippingVO, $order);
            }

            // Freight by provider by kilos or cubage
            if ($this->freightProvider) {
                // for find freight of the shop use kilos or cubages without promotions and freight by provider
                $weightFreightShop = $cubageFreightShop =  0;

                $shippingVO = $this->addProvidersShipping($shippingVO);
                if (!empty($shippingVO->getProviders())) {
                    $ruleFreightProviders = [];
                    foreach ($shippingVO->getProviders() as $providers) {
                        // Get shipping employees from region and products categories
                        $shippingEmployeesProviders = $shippingRepository->getShippingFromCategoriesAndRegion(
                            $order->getRegion(),
                            $order->getCategories(),
                            $providers->getProviderId()
                        );

                        if (!empty($shippingEmployeesProviders)) {
                            $shipping = $this->calculateEmployeesShipping(
                                $shippingEmployeesProviders,
                                $order,
                                $providers->getWeight(),
                                $providers->getCubage(),
                                $maxWeight,
                                $maxCubage
                            );
                            if (!empty($shipping)) {
                                $ruleFreightProviders[] = $providers->getProviderId();
                                $shippingVO->setPrice($shipping['value'] + $shippingVO->getPrice());
                                $shippingVO->setCostPrice($shipping['costValue'] + $shippingVO->getCostPrice());
                                continue;
                            }
                        }
                        $weightFreightShop += $providers->getWeight();
                        $cubageFreightShop += $providers->getCubage();
                    }

                    if (!empty($ruleFreightProviders)) {
                        $shippingVO->addFreight('Frete por Fornecedor');
                    }
                }
            }
        }

        // get price by new total weigth/cubage
        if ($weightFreightShop > 0 || $cubageFreightShop > 0) {
            $shipping = $this->calculateEmployeesShipping(
                $shippingEmployees,
                $order,
                $weightFreightShop,
                $cubageFreightShop,
                $maxWeight,
                $maxCubage
            );

            if (!empty($shipping)) {
                $shippingVO->addFreight('Frete por Região');
                $shippingVO->setPrice($shipping['value'] + $shippingVO->getPrice());
                $shippingVO->setCostPrice($shipping['costValue'] + $shippingVO->getCostPrice());
            }
        }

        $products = $order->getProducts();

        // Get total shipping from shipping employee and deadlines
        $returnShipping = $this->generateFinalShippingValue($shipping, $products);

        // Set the shipping data on shipping VO object
        $shippingVO->setDeadline($returnShipping['deadline']);
        $shippingVO->setMounting($returnShipping['mounting']);
        $shippingVO->setMountingValue($returnShipping['mountingValue']);

        if (!empty($shippingVO->getFreight())) {
            $shippingVO->setRuleFreight(implode(',', $shippingVO->getFreight()));
        }

        // set cost price
        if ($shippingVO->getCostPrice() < $shippingVO->getPrice()) {
            $shippingVO->setCostPrice($shippingVO->getPrice());
        }
        return $shippingVO;
    }

    /**
     * @param $shipping
     * @param $promotionShipping
     * @param $products
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     * Generate the final shipping price
     */
    private function generateFinalShippingValue($shipping, $products)
    {
        $deadline = 0;

        // Get the max deadline value from products
        /* @var $product CreateProductVO */
        foreach ($products as $product) {
            $productStock = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')
                ->getProductStockAndDeadline($product->getId(), $product->getQuantity());

            if ($productStock['deadline'] > $deadline) {
                $deadline = $productStock['deadline'];
            }
        }

        $shipping['deadline'] = $deadline;

        switch ($shipping['tipoFrete']) {
            case 1:
                $shipping['prazoFreteGratuito'] = $deadline + $shipping['prazoFreteGratuito'];
                break;
            case 2:
                $shipping['prazoFreteFixo'] = $deadline + $shipping['prazoFreteFixo'];
                break;
            case 3:
                $shipping['prazoTransportadora'] = $deadline + $shipping['prazoTransportadora'];
                break;
            default:
                break;
        }

        $shipping['mounting'] = 0;
        $shipping['mountingValue'] = 0;

        return $shipping;
    }

    /**
     * @param $shipping
     * @param $productsQtd
     * @param $extraValue
     *
     * @return mixed
     *
     * Calculate extra shipping value
     */
    public function calculateExtraShipping($shipping, $productsQtd, $extraValue)
    {
        if ($extraValue > 0) {
            $extraShippingValue = $shipping['value'] * $extraValue / 100;

            for ($i = 1; $i < $productsQtd; $i++) {
                $shipping['value'] += $extraShippingValue;
            }
        }

        return $shipping;
    }

    /**
     * @param CreateShippingVO $shippingVO
     * @param $order
     * @return CreateShippingVO
     * Calculate shipping from region and product ID
     */
    public function calculateItemShipping(CreateShippingVO $shippingVO, $order)
    {
        // Get shipping product from database
        $shippingProduct = $this->entityManager->getRepository('AppBundle:B2cPrdfrete')
            ->getShippingProduct($order->getRegion()->getId(), $order->getProductsId());

        $products = [];
        foreach ($order->getProducts() as $product) {
            $shippingProdVO = new CreateShippingProductVO();
            $shippingProdVO->setProductId($product->getId());
            $shippingProdVO->setProviderId($product->getProvider());
            $shippingProdVO->setCubage(0);
            $shippingProdVO->setWeight(0);

            // check freight by sku
            if (!empty($shippingProduct)) {
                $keyShipping = array_search($product->getId(), array_column($shippingProduct, "productId"));
                if ($keyShipping !== false) {
                    $products[] = $product->getId();
                    $shippingProdVO->setPrice($shippingProduct[$keyShipping]['price'] * $product->getQuantity());
                    $shippingVO->setPrice($shippingProdVO->getPrice());
                    $shippingVO->addProducts($shippingProdVO);
                    continue;
                }
            }
            $shippingProdVO->setCubage($product->getCubage() * $product->getQuantity());
            $shippingProdVO->setWeight($product->getWeight() * $product->getQuantity());
            $shippingVO->addProducts($shippingProdVO);
        }

        if (!empty($products)) {
            $shippingVO->addFreight('Frete por Produto/Região');
        }
        return $shippingVO;
    }

    public function addProvidersShipping(CreateShippingVO $shippingVO)
    {
        if (!empty($shippingVO->getProducts())) {
            foreach ($shippingVO->getProducts() as $productShipping) {
                $shippingProvVO = new CreateShippingProviderVO();
                if (empty($productShipping->getPrice()) &&
                    (!empty($productShipping->getCubage()) || !empty($productShipping->getWeight()))) {
                    $shippingProvVO->setProviderId($productShipping->getProviderId());
                    $shippingProvVO->setWeight($shippingProvVO->getWeight() + $productShipping->getWeight());
                    $shippingProvVO->setCubage($shippingProvVO->getCubage() + $productShipping->getCubage());
                    $shippingVO->addProviders($productShipping->getProviderId(), $shippingProvVO);
                }
            }
        }

        return $shippingVO;
    }

    /**
     * @param CreateOrderVO $orderVO
     *
     * @return CreateShippingVO
     *
     * Calculate shipping from coupon code
     */
    public function calculateCouponShipping(CreateOrderVO $orderVO)
    {
        $shipping = $orderVO->getShipping();

        if ($orderVO->getCoupon()->isFreeShipping()) {
            $shipping->setPrice(0);
            $shipping->setShippingType(1);
            $shipping->setRuleFreight('Frete Promocional (Grátis)');
        } elseif ($orderVO->getCoupon()->isFixedShipping()) {
            $shipping->setPrice($orderVO->getCoupon()->getFixedValue());
            $shipping->setShippingType(2);
            $shipping->setFixedValue($orderVO->getCoupon()->getFixedValue());
            $shipping->setRuleFreight('Frete Promocional (Fixo)');
        }

        return $shipping;
    }

    public function calculateEmployeesShipping($shippingEmployees, $order, $weight, $cubage, $maxWeight, $maxCubage)
    {
        $shippingRepository = $this->entityManager->getRepository('AppBundle:B2cTratra');
        $shipping = array();
        foreach ($shippingEmployees as $key => $trans) {
            // Get the shipping value from shipping employee
            $shippingValue = $shippingRepository->calculateFreightShipping(
                $order->getRegion(),
                $trans['idTratra'],
                $weight,
                $maxWeight,
                $cubage,
                $maxCubage
            );

            if ($shippingValue <= 0 && !($trans['valor_frete_fixo'] > 0 && $trans['tipo_frete'] == 2)) {
                continue;
            } else {
                $shipping = $shippingEmployees[$key];
                $shipping['value'] = round($shippingValue +
                    round(($order->getSubtotal() * $trans['seguro']) / 100, 2), 2);
                $shipping['safetyValue'] = $shipping['costValue'] = $shipping['value'];
                break;
            }
        }

        // If extra shipping is set, calculate it
        if ($this->extraShipping && $shipping['tipoFrete'] == 2) {
            $shipping = $this->calculateExtraShipping(
                $shipping,
                $order->getTotalQuantity(),
                $this->extraShippingValue
            );
        }

        return $shipping;
    }
}
