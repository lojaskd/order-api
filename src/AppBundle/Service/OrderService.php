<?php

namespace AppBundle\Service;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\B2cPedprd;
use AppBundle\Entity\B2cPedstatus;
use AppBundle\Entity\Validate\ValidateOrder;
use AppBundle\Entity\Validate\ValidateOrderStatus;
use AppBundle\Entity\Validate\ValidatePurchaseOrder;
use AppBundle\Entity\Validate\ValidatePurchaseOrderProduct;
use AppBundle\Entity\VO\OrderAddressVO;
use AppBundle\Entity\VO\OrderCustomerVO;
use AppBundle\Entity\VO\OrderPaymentVO;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderStatusVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Create\CreateAddressVO;
use AppBundle\Entity\VO\Create\CreateOrderLogVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreateOriginVO;
use AppBundle\Entity\VO\Create\CreatePaymentVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\Create\CreatePurchaseOrderProductVO;
use AppBundle\Entity\VO\Create\CreatePurchaseOrderVO;
use AppBundle\Entity\VO\Create\CreateRegionVO;
use AppBundle\Entity\VO\Update\UpdateNewDeliveryDateVO;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateProductVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateTypeVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateVO;
use AppBundle\Entity\VO\Update\UpdateProductsReversalInfoVO;
use AppBundle\Entity\VO\Update\UpdateProductsStatusProductVO;
use AppBundle\Entity\VO\Update\UpdateProductsStatusVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;
use AppBundle\Entity\VO\Create\CreateShippingVO;
use AppBundle\Entity\VO\Create\CreatePromotionPaymentVO;

/**
 * Class OrderService
 *
 * @package AppBundle\Service
 */
class OrderService
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var LojasKdService
     */
    private $lojaskdService;
    /**
     * @var MarketPlaceService
     */
    private $marketPlaceService;
    /**
     * @var ProductService
     */
    private $productService;
    /**
     * @var EmailService
     */
    private $emailService;
    /**
     * @var QueueService
     */
    private $queueService;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var OrderStatusService
     */
    private $orderStatusService;
    /**
     * @var EventService
     */
    private $eventService;
    /**
     * @var OrderDateService
     */
    private $orderDateService;
    /**
     * @var OrderLogService
     */
    private $orderLogService;
    /**
     * @var
     */
    private $saveSeals;
    /**
     * @var ShippingService
     */
    private $shippingService;
    /**
     * @var
     */
    private $cashDiscount;
    /**
     * @var AwsService
     */
    private $awsService;

    /**
     * OrderService constructor.
     *
     * @param EntityManager $entityManager
     * @param LojasKdService $lojaskdService
     * @param MarketPlaceService $marketPlaceService
     * @param ProductService $productService
     * @param EmailService $emailService
     * @param QueueService $queueService
     * @param Logger $logger
     * @param OrderStatusService $orderStatusService
     * @param EventService $eventService
     * @param OrderDateService $orderDateService
     * @param OrderLogService $orderLogService
     * @param $saveSeals
     * @param ShippingService $shippingService
     * @param $cashDiscount
     * @param AwsService $awsService
     */
    public function __construct(
        EntityManager $entityManager,
        LojasKdService $lojaskdService,
        MarketPlaceService $marketPlaceService,
        ProductService $productService,
        EmailService $emailService,
        QueueService $queueService,
        Logger $logger,
        OrderStatusService $orderStatusService,
        EventService $eventService,
        OrderDateService $orderDateService,
        OrderLogService $orderLogService,
        $saveSeals,
        ShippingService $shippingService,
        $cashDiscount,
        AwsService $awsService
    ) {
        $this->entityManager = $entityManager;
        $this->lojaskdService = $lojaskdService;
        $this->marketPlaceService = $marketPlaceService;
        $this->productService = $productService;
        $this->emailService = $emailService;
        $this->queueService = $queueService;
        $this->logger = $logger;
        $this->orderStatusService = $orderStatusService;
        $this->eventService = $eventService;
        $this->orderDateService = $orderDateService;
        $this->orderLogService = $orderLogService;
        $this->saveSeals = $saveSeals;
        $this->shippingService = $shippingService;
        $this->cashDiscount = $cashDiscount;
        $this->awsService = $awsService;
    }


    /**
     * @param ValidateOrder $order
     * @return CreateOrderVO
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     *
     * Create a VO and save the main order info
     */
    public function getCommonCreateOrderInfo(ValidateOrder $order)
    {
        $orderInfo = array();
        $orderVO = new CreateOrderVO();

        $this->logger->info('API_PEDIDOS - Getting common order info.');

        // Save the order type
        $orderVO->setType($order->getOrderType());

        // Search customer region information
        $regionVO = $this->setRegion($order);
        $orderVO->setRegion($regionVO);

        $deliveryAddressVO = new CreateAddressVO();
        $deliveryAddressVO->setIdentification((
        $order->getDeliveryAddress()->getIdentification() ?
            $order->getDeliveryAddress()->getIdentification() :
            'Meu Endereço'
        ));
        $deliveryAddressVO->setFirstName($order->getDeliveryAddress()->getFirstName());
        $deliveryAddressVO->setLastname($order->getDeliveryAddress()->getLastName());
        $deliveryAddressVO->setAddress($order->getDeliveryAddress()->getAddress());
        $deliveryAddressVO->setNumber($order->getDeliveryAddress()->getNumber());
        $deliveryAddressVO->setComplement($order->getDeliveryAddress()->getComplement());
        $deliveryAddressVO->setNeighborhood($order->getDeliveryAddress()->getNeighborhood());
        $deliveryAddressVO->setCity($order->getDeliveryAddress()->getCity());
        $deliveryAddressVO->setState($order->getDeliveryAddress()->getState());
        $deliveryAddressVO->setZip($order->getDeliveryAddress()->getZip());
        $deliveryAddressVO->setCountry($order->getDeliveryAddress()->getCountry());
        $deliveryAddressVO->setTelephoneA($order->getDeliveryAddress()->getTelephoneA());
        $deliveryAddressVO->setTelephoneB($order->getDeliveryAddress()->getTelephoneB());
        $deliveryAddressVO->setReference($order->getDeliveryAddress()->getReference());

        $billingAddressVO = new CreateAddressVO();
        $billingAddressVO->setIdentification((
        $order->getBillingAddress()->getIdentification() ?
            $order->getBillingAddress()->getIdentification() :
            'Meu Endereço'
        ));
        $billingAddressVO->setFirstName($order->getBillingAddress()->getFirstName());
        $billingAddressVO->setLastname($order->getBillingAddress()->getLastName());
        $billingAddressVO->setAddress($order->getBillingAddress()->getAddress());
        $billingAddressVO->setNumber($order->getBillingAddress()->getNumber());
        $billingAddressVO->setComplement($order->getBillingAddress()->getComplement());
        $billingAddressVO->setNeighborhood($order->getBillingAddress()->getNeighborhood());
        $billingAddressVO->setCity($order->getBillingAddress()->getCity());
        $billingAddressVO->setState($order->getBillingAddress()->getState());
        $billingAddressVO->setZip($order->getBillingAddress()->getZip());
        $billingAddressVO->setCountry($order->getBillingAddress()->getCountry());
        $billingAddressVO->setTelephoneA($order->getBillingAddress()->getTelephoneA());
        $billingAddressVO->setTelephoneB($order->getBillingAddress()->getTelephoneB());

        $orderVO->setDeliveryAddress($deliveryAddressVO);
        $orderVO->setBillingAddress($billingAddressVO);

        $productRepository = $this->entityManager->getRepository('AppBundle:Validate\ValidateProduct');

        // Get products ID array from POST data
        $orderInfo['products'] = $productRepository->getProductRequestInfo($order->getProducts());
        // Get products data and format them
        $productsRepository = $this->entityManager->getRepository('AppBundle:B2cViewProdutos');

        $nextBusinessDay = $productsRepository->getDateAddNextBusinessDay(date('Y-m-d'), 1);

        // Get payment info
        $orderInfo['paymentsInfo'] = $this->entityManager->getRepository('AppBundle:Validate\ValidatePayment')->getPaymentInfo($order->getPayments());

        $firstPayment = new CreatePaymentVO();
        $firstPayment->setId($orderInfo['paymentsInfo'][0]['id']);
        $firstPayment->setPaymentFormId($orderInfo['paymentsInfo'][0]['idPaymentForm']);
        $firstPayment->setIndexer($orderInfo['paymentsInfo'][0]['indexer']);
        $firstPayment->setInstalment($orderInfo['paymentsInfo'][0]['instalment']);
        $firstPayment->setInstalmentValue($orderInfo['paymentsInfo'][0]['instalmentValue']);
        $firstPayment->setName($orderInfo['paymentsInfo'][0]['name']);
        $firstPayment->setDescription($orderInfo['paymentsInfo'][0]['description']);
        $firstPayment->setDeadline($orderInfo['paymentsInfo'][0]['deadline']);
        $firstPayment->setPaymentFormDesc($orderInfo['paymentsInfo'][0]['paymentFormDesc']);

        $orderVO->setFirstPayment($firstPayment);

        if (isset($orderInfo['paymentsInfo'][1])) {
            $secondPayment = new CreatePaymentVO();
            $secondPayment->setId($orderInfo['paymentsInfo'][1]['id']);
            $secondPayment->setPaymentFormId($orderInfo['paymentsInfo'][1]['idPaymentForm']);
            $secondPayment->setIndexer($orderInfo['paymentsInfo'][1]['indexer']);
            $secondPayment->setInstalment($orderInfo['paymentsInfo'][1]['instalment']);
            $secondPayment->setInstalmentValue($orderInfo['paymentsInfo'][1]['instalmentValue']);
            $secondPayment->setName($orderInfo['paymentsInfo'][1]['name']);
            $secondPayment->setDescription($orderInfo['paymentsInfo'][1]['description']);
            $secondPayment->setDeadline($orderInfo['paymentsInfo'][1]['deadline']);
            $secondPayment->setPaymentFormDesc($orderInfo['paymentsInfo'][1]['paymentFormDesc']);

            $orderVO->setSecondPayment($secondPayment);
        }

        $totalPromotionsProd = 0;
        foreach ($orderInfo['products'] as $product) {
            $productVO = new CreateProductVO();
            $productVO->setId($product['id']);
            $productVO->setQuantity($product['quantity']);
            $productVO->setKitId($product['kitId']);

            $productInfo = $productsRepository->getProductInfoFromId($productVO->getId());

            if ($productInfo) {
                $productVO->setName($productInfo['nome']);
                $productVO->setReference($productInfo['referencia']);
                $productVO->setCategory($productInfo['categoria']);
                $productVO->setFeatured($productInfo['destaque']);
                $productVO->setStyle($productInfo['estilo']);
                $productVO->setColorName($productInfo['nomeCor']);
                $productVO->setGenre($productInfo['sexo']);
                $productVO->setSize($productInfo['tamanho']);
                $productVO->setWeight($productInfo['peso']);
                $productVO->setCubage($productInfo['cubagem']);
                $productVO->setProvider($productInfo['fornecedor']);
                $productVO->setProviderName($productInfo['nomeFornecedor']);
                $productVO->setProductProviderDeadline($productInfo['prdPrazoFornecedor']);
                $productVO->setProviderDeadline($productInfo['prazoFornecedor']);
                $productVO->setProviderStock($productInfo['stqfornecedor']);
                $productVO->setProviderStockReserved($productInfo['stqfornReservado']);
                $productVO->setMinStockAction($productInfo['stqminAcao']);
                $productVO->setMinStock($productInfo['stqminimo']);
                $productVO->setAction($productInfo['acao']);
                $productVO->setShow($productInfo['mostra']);
                $productVO->setProductStock($productInfo['estoque']);
                $productVO->setAvailable($productInfo['disponivel']);
                $productVO->setOut($productInfo['esgotado']);
                $productVO->setAvailability($productInfo['disponibilidade']);
                $productVO->setAvailabilityDays($productInfo['dispDias']);
                $productVO->setPromotion($productInfo['bpromocao']);
                $productVO->setInitialPromotion($productInfo['promocaoini']);
                $productVO->setFinalPromotion($productInfo['promocaofim']);
                $productVO->setRealPrice($productInfo['pvreal']);
                $productVO->setCostPrice($productInfo['custo']);
                $productVO->setPromotionPrice($productInfo['pvpromocao']);
                $productVO->setProductPrice($productInfo['precoProduto']);

                $image = '/assistencia.jpg';
                if ($productVO->getCategory() != 472) {
                    $image = $this->productService->getProductImage($productVO->getId());
                }
                $productVO->setImage($image);

                $relatedCategories = $this->entityManager->getRepository('AppBundle:B2cPrdselcat')
                    ->getRelatedCategories($productVO->getId());
                foreach ($relatedCategories as $relatedCategory) {
                    $productVO->addRelatedCategory($relatedCategory['category']);
                }

                if ($orderVO->getType() == 'marketplace') {
                    $productVO->setTotalPrice($product['price'] * $product['quantity']);
                } else {
                    $isAssistance = ($orderVO->getType() == 'recompra' || $orderVO->getType() == 'assistencia' || $orderVO->getType() == 'troca');
                    $productVO->setTotalPrice($this->productService->getTotalPrice($productVO, $isAssistance));
                }

                $productVO->setDiscountValue(
                    ($productVO->getRealPrice() * $productVO->getQuantity()) - $productVO->getTotalprice()
                );

                $productVO->setDiscountPercentage(round(
                    ($productVO->getDiscountValue() * 100) / ($productVO->getRealPrice() * $productVO->getQuantity())
                ));

                $fornRegiao = $this->entityManager->getRepository('AppBundle:B2cFornecedorRegiao')
                    ->findOneBy(
                        array(
                            "idForfor" => $productInfo['fornecedor'],
                            "idRegreg" => $orderVO->getRegion()->getId(),
                        )
                    );

                if (!empty($fornRegiao)) {
                    $productVO->setCollectionConveyorCode($fornRegiao->getCodigoTransportadorColeta());
                }

                $productVO = $this->productService->getProductDates(
                    $productVO,
                    $nextBusinessDay,
                    $deliveryAddressVO->getZip(),
                    $productVO->getQuantity()
                );

                $promotion = $this->entityManager->getRepository('AppBundle:B2cPromo')->getActiveDiscountPromotionProduct(
                        $productVO->getId(),
                        $orderVO->getFirstPayment()->getId(),
                        $orderVO->getFirstPayment()->getPaymentFormId()
                    );

                if (!empty($promotion)) {
                    $promotionPayment = new CreatePromotionPaymentVO();
                    $promotionPayment->setIdPromo($promotion['idPromo']);
                    $promotionPayment->setDiscountValue($promotion['descontoValor']);
                    $promotionPayment->setDiscountPerc($promotion['descontoPerc']);
                    $discountProdPrice = $totalDiscount = 0;
                    if ($promotion['descontoPerc'] > 0) {
                        $totalDiscount = round((($productVO->getTotalPrice() * $promotion['descontoPerc']) / 100), 2);
                    } elseif ($promotion["descontoValor"] > 0) {
                        $totalDiscount = (float) $promotion["descontoValor"];
                    }
                    $discountProdPrice = $productVO->getTotalPrice() - $totalDiscount;
                    $promotionPayment->setDiscountProdPrice($discountProdPrice);
                    $promotionPayment->setDiscountProduct($totalDiscount);
                    $productVO->setPromotionPayment($promotionPayment);
                    $totalPromotionsProd++;
                }
            }
            $orderVO->addProduct($productVO);
        }

        if ($this->cashDiscount && $orderVO->getType() != 'marketplace' && $totalPromotionsProd > 0) {
            $orderVO->setPromotionPayment(true);
        }

        // Get the origin of the traffic
        $orderInfo['origin']['source'] = $orderInfo['origin']['medium'] = $orderInfo['origin']['content'] = '';
        if ($order->getOrigin()) {
            $originVO = new CreateOriginVO();
            $originVO->setSource($order->getOrigin()->getSource());
            $originVO->setMedium($order->getOrigin()->getMedium());
            $originVO->setContent($order->getOrigin()->getContent());

            $orderVO->setOrigin($originVO);
        }

        $orderVO->setObservation($order->getObservation());
        $orderVO->setDevice($order->getDevice());

        $orderVO->setExchange(false);
        $orderVO->setBuyback(false);
        $orderVO->setAssistance(false);

        if ($order->getOrderType() == 'troca') {
            $orderVO->setExchange(true);
        } elseif ($order->getOrderType() == 'recompra') {
            $orderVO->setBuyback(true);
        } elseif ($order->getOrderType() == 'assistencia') {
            $orderVO->setAssistance(true);
        }

        $this->logger->info('API_PEDIDOS - Common order info generated: '.serialize($orderVO));

        return $orderVO;
    }

    public function setRegion($order)
    {
        // Search customer region information
        $orderInfo['region'] = $this->entityManager->getRepository('AppBundle:B2cRegcep')
            ->getRegion($order->getDeliveryAddress()->getZip());

        $regionVO = new CreateRegionVO();
        $regionVO->setId($orderInfo['region']['id']);
        $regionVO->setName($orderInfo['region']['regiao']);
        $regionVO->setDist($orderInfo['region']['dist']);
        $regionVO->setChooseCustomer($orderInfo['region']['clienteEscolhe']);

        // Get the IBGE code from address info
        $orderInfo['ibgeCode'] = $this->entityManager->getRepository('AppBundle:B2cRegest')
            ->getIBGECode($order->getDeliveryAddress()->getCity(), $order->getDeliveryAddress()->getState());
        $regionVO->setIbgeCode($orderInfo['ibgeCode']);

        return $regionVO;
    }

    /**
     * @param Integer $orderId
     *
     * @return OrderVO
     *
     * Create a VO order info
     */
    public function getCommonOrderInfo($orderId)
    {
        $this->logger->info('API_PEDIDOS - Getting common order info.');
        /* @var $order B2cPedped */
        $order = $this->entityManager->getRepository('AppBundle:B2cPedped')->findOneBy(array('idPedped' => $orderId));

        if ($order) {
            $orderVO = new OrderVO();
            /* Order */
            $orderVO->setOrderId($order->getIdPedped());
            $orderVO->setPredictDeliveryDate($order->getPedDataEntrega());
            $orderVO->setSubtotal($order->getPedValorSubtotal());
            $orderVO->setDiscount($order->getPedValorDesconto());
            $orderVO->setTotal($order->getPedValorTotal());
            $orderVO->setCoupon($order->getPedCupom());
            $orderVO->setCouponAmount($order->getPedValorValecompra());
            $orderVO->setShippingAmount($order->getPedValorFrete());
            $orderVO->setMarketplaceId($order->getIdMarketplace());

            /* Status */
            $orderStatusVO = new OrderStatusVO;
            $status = $this->entityManager->getRepository('AppBundle:B2cPedsituacao')
                ->findOneBy(array('id' => $order->getPedSituacao()));
            if ($status) {
                $orderStatusVO->setId($status->getId());
                $orderStatusVO->setName($status->getName());
            }
            $orderVO->setStatus($orderStatusVO);

            /* Internal Status */
            $orderInternalStatusVO = new OrderStatusVO;
            $internalStatus = $this->entityManager->getRepository('AppBundle:B2cStaint')
                ->findOneBy(array('id' => $order->getPedStaint()));

            $orderInternalStatusVO->setId($order->getPedStaint());
            $orderInternalStatusVO->setName('');
            if ($internalStatus) {
                $orderInternalStatusVO->setId($internalStatus->getId());
                $orderInternalStatusVO->setName($internalStatus->getNome());
            }
            $orderVO->setInternalStatus($orderInternalStatusVO);

            /* Customer */
            $orderCustomerVO = new OrderCustomerVO();
            $orderCustomerVO->setId($order->getIdClicli());
            $orderVO->setCustomer($orderCustomerVO);

            /* Delivery Address */
            $deliveryAddress = new OrderAddressVO();
            $deliveryAddress->setFirstName($order->getEntNome());
            $deliveryAddress->setLastname($order->getEntSobrenome());
            $deliveryAddress->setAddress($order->getEntEndereco());
            $deliveryAddress->setNumber($order->getEntEndNumero());
            $deliveryAddress->setComplement($order->getEntEndComp());
            $deliveryAddress->setNeighborhood($order->getEntEndBairro());
            $deliveryAddress->setZip($order->getEntEndCep());
            $deliveryAddress->setCity($order->getEntEndCidade());
            $deliveryAddress->setState($order->getEntEndUf());
            $deliveryAddress->setCountry($order->getEntEndPais());
            $deliveryAddress->setTelephoneA($order->getEntTelA());
            $deliveryAddress->setTelephoneB($order->getEntTelB());
            $orderVO->setDeliveryAddress($deliveryAddress);

            /* Billing Address */
            $billingAddress = new OrderAddressVO();
            $billingAddress->setFirstName($order->getCobNome());
            $billingAddress->setLastname($order->getCobSobrenome());
            $billingAddress->setAddress($order->getCobEndereco());
            $billingAddress->setNumber($order->getCobEndNumero());
            $billingAddress->setComplement($order->getCobEndComp());
            $billingAddress->setNeighborhood($order->getCobEndBairro());
            $billingAddress->setZip($order->getCobEndCep());
            $billingAddress->setCity($order->getCobEndCidade());
            $billingAddress->setState($order->getCobEndUf());
            $billingAddress->setCountry($order->getCobEndPais());
            $billingAddress->setTelephoneA($order->getCobTelA());
            $billingAddress->setTelephoneB($order->getCobTelB());
            $orderVO->setBillingAddress($billingAddress);

            $firstPayment = new OrderPaymentVO();
            $firstPayment->setId($order->getIdPagpag1());
            $firstPayment->setBillingAccount($order->getContaContabil1());
            $firstPayment->setBillingAccountName($order->getContaContabilNome1());
            $orderVO->setFirstPayment($firstPayment);

            $secondPayment = new OrderPaymentVO();
            $secondPayment->setId($order->getIdPagpag2());
            $secondPayment->setBillingAccount($order->getContaContabil2());
            $secondPayment->setBillingAccountName($order->getContaContabilNome2());
            $orderVO->setSecondPayment($secondPayment);

            $orderVO->setRegionName($order->getPedEntregaRegiao());
            $productService = $this->entityManager->getRepository('AppBundle:B2cViewProdutos');
            $productCatService = $this->entityManager->getRepository('AppBundle:B2cPrdselcat');
            /* @var $product B2cPedprd */
            foreach ($order->getProducts() as $product) {
                $productInfo = $productService->findOneBy(array('id' => $product->getIdPrdprd()));

                $orderProductVO = new OrderProductVO();
                $orderProductVO->setId($product->getId());
                $orderProductVO->setProductId($product->getIdPrdprd());
                $orderProductVO->setOrderId($order->getIdPedped());
                $orderProductVO->setReference($product->getPrdReferencia());
                $orderProductVO->setName($product->getPrdNome());
                $orderProductVO->setProductStock($product->getPrdStqLoja());
                $orderProductVO->setSupplierStock($product->getPrdStqFornecedor());
                $orderProductVO->setSupplierReservedStock($product->getPrdStqFornecedorReservado());
                $orderProductVO->setCancelled($product->getItemCancelado());
                $orderProductVO->setBranchStockId($product->getIdFilialEstoque());
                $orderProductVO->setQuantity($product->getPrdQtd());
                $orderProductVO->setCategory($product->getPrdCategoria());

                $orderProductVO->setSize($productInfo->getTamanho());
                $orderProductVO->setWeight($productInfo->getPeso());
                $orderProductVO->setCubage($productInfo->getCubagem());
                $orderProductVO->setProvider($productInfo->getFornecedor());
                $orderProductVO->setProviderName($productInfo->getNomeFornecedor());

                $immediateDelivery = false;
                $reservedStockDelivery = false;
                if ($orderProductVO->getProductStock() > 0 && $orderProductVO->getBranchStockId() > 0) {
                    $immediateDelivery = true;
                } elseif ($product->getPrdStqFornecedorReservado() > 0) {
                    $reservedStockDelivery = true;
                }
                $orderProductVO->setImmediateDelivery($immediateDelivery);
                $orderProductVO->setReservedStockDelivery($reservedStockDelivery);
                $orderProductVO->setDestination($product->getDestination());
                $orderProductVO->setSupplierId($product->getPrdFornecedor());

                $relatedCategories = $productCatService->getRelatedCategories($product->getIdPrdprd());
                if ($relatedCategories) {
                    foreach ($relatedCategories as $relatedCategory) {
                        $orderProductVO->addRelatedCategory($relatedCategory['category']);
                    }
                }
                $orderVO->addProduct($orderProductVO);
            }

            // Set the shipping data on shipping VO object
            $shippingVO = new CreateShippingVO();
            $shippingVO->setShippingType($order->getPedTipoFrete());
            $shippingVO->setName($order->getPedTransportadora());
            $shippingVO->setMounting($order->getPedMontagem());
            $shippingVO->setMountingValue($order->getPedValorMontagem());
            $shippingVO->setPrice($order->getPedValorFrete());
            $shippingVO->setCostPrice($order->getPedValorCustofrete());
            $shippingVO->setRuleFreight($order->getRuleFreight());
            $orderVO->setShipping($shippingVO);

            // Search customer region information
            $regionVO = $this->setRegion($orderVO);
            $orderVO->setRegion($regionVO);

            $this->logger->info('API_PEDIDOS - Common order info generated: '.serialize($orderVO));

            return $orderVO;
        } else {
            throw new Exception('Order not found');
        }
    }

    /**
     * @param CreateOrderVO $orderVO
     * @param ValidateOrder $order
     *
     * @return CreateOrderVO|array
     *
     * Generate order according to the order type (marketplace, assistance, ...)
     */
    public function generateOrder(CreateOrderVO $orderVO, ValidateOrder $order)
    {
        if ($orderVO->getType() == 'marketplace') {
            $this->logger->info('API_PEDIDOS - MarketPlace order type.');
            $orderVO = $this->marketPlaceService->getMarketPlaceData($orderVO, $order);
        } else {
            $this->logger->info('API_PEDIDOS - LojasKD order type.');
            $orderVO = $this->lojaskdService->getLojasKdData($orderVO, $order);
        }

        return $orderVO;
    }

    /**
     * @param CreateOrderVO $orderVO
     *
     * @return integer|null
     *
     * Insert order on database
     */
    public function insertOrder(CreateOrderVO $orderVO)
    {
        try {
            $this->logger->info('API_PEDIDOS - Inserting order in database...');
            // Generate the order on b2c_pedped
            /* @var $order B2cPedped */
            $order = $this->entityManager->getRepository('AppBundle:B2cPedped')->generateOrder($orderVO);

            $sealsProducts = array();
            // For each product from order
            /* @var $product CreateProductVO */
            foreach ($orderVO->getProducts() as $product) {
                // Generate the product order on b2c_pedprd
                $orderProduct = $this->entityManager->getRepository('AppBundle:B2cPedprd')
                    ->generateProductsOrder($order, $product);
                $order->addProduct($orderProduct);

                // Get seals and product
                if ($this->saveSeals) {
                    $sealsProducts[] = $this->entityManager->getRepository('AppBundle:B2cSelos')
                        ->getSealsProducts($product->getId());
                }
            }

            // Persist the order in database
            $this->entityManager->persist($order);
            $this->entityManager->flush();

            // Insert seals and product
            if ($sealsProducts) {
                $this->entityManager->getRepository('AppBundle:B2cPedprdSelos')
                    ->insertOrderProductsSeals($order->getIdPedped(), $sealsProducts);
            }

            // Set order ID and create order status
            $orderVO->setOrderId($order->getIdPedped());
            // Create order deadlines
            $this->orderDateService->createOrderDeadline($orderVO);

            // if marketplace set code order b2c_pedped_marketplace
            if ($orderVO->getType() == 'marketplace') {
                $this->logger->info('API_PEDIDOS - Saving marketplace on database...');
                $this->entityManager->getRepository('AppBundle:B2cPedpedMarketplace')
                    ->insertOrderMarketplace($order, $orderVO->getMarketplace());
            }

            $this->logger->info('API_PEDIDOS - Reducing products stock...');
            // Reduce the stock of products order
            $this->productService->reduceStock($order->getIdPedped(), $order->getIdMarketplace(), $orderVO);

            $this->logger->info('API_PEDIDOS - Sending customer email...');
            // If the order is assistance, buyback or marketplace, It does not send the email
            if (!$order->getAssistencia() && !$order->getRecompra() && $orderVO->getType() != 'marketplace') {
                // Send order email to customer
                $this->emailService->sendOrderEmail($order, $orderVO->getCustomer());
            }

            // If a coupon was used, set it as used on database
            if ($orderVO->getCoupon()) {
                $this->logger->info('API_PEDIDOS - Saving coupon used on database...');
                $this->entityManager->getRepository('AppBundle:B2cPromoVale')
                    ->updateCouponSituation($orderVO->getCoupon()->getCode());
                $this->entityManager->getRepository('AppBundle:B2cPromoVale')
                    ->updateCouponUtilization($orderVO->getCoupon()->getCode());
            }
            // Save order log
            $this->orderLogService->insertCreateOrderLog($orderVO);

            $this->eventService->send('OrderCreate', $orderVO);

            // Return the order ID
            return $order->getIdPedped();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param ValidateOrderStatus $orderStatus
     *
     * @return UpdateOrderStatusVO
     *
     * Create an orderStatusVO and save the main order info
     */
    public function getCommonOrderStatusInfo(ValidateOrderStatus $orderStatus)
    {
        try {
            $updateOrderStatusVO = new UpdateOrderStatusVO();

            $updateOrderStatusVO->setOrderId($orderStatus->getOrderId());
            $updateOrderStatusVO->setStatus($orderStatus->getStatus());
            $updateOrderStatusVO->setInternalStatus($orderStatus->getInternalStatus());
            $updateOrderStatusVO->setDate(new \DateTime('now'));
            $updateOrderStatusVO->setClerk($orderStatus->getClerk());
            $updateOrderStatusVO->setTypeReversal($orderStatus->getTypeReversal());
            $updateOrderStatusVO->setTicket($orderStatus->getTicket());
            $updateOrderStatusVO->setOperator($orderStatus->getOperator());
            $updateOrderStatusVO->setTransaction($orderStatus->getTransaction());
            $updateOrderStatusVO->setOcrCode($orderStatus->getOcrCode());
            $updateOrderStatusVO->setCancellationIndicator($orderStatus->getCancellationIndicator());
            $updateOrderStatusVO->setTicketType($orderStatus->getTicketType());
            $updateOrderStatusVO->setHub($orderStatus->getHub());
            $updateOrderStatusVO->setShipper($orderStatus->getShipper());
            $updateOrderStatusVO->setInvoice($orderStatus->getInvoice());
            $updateOrderStatusVO->setProductCauser($orderStatus->getProductCauser());

            return $updateOrderStatusVO;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param UpdateOrderStatusVO $updateOrderStatusVO
     * @param OrderVO $orderVO
     *
     * @return bool
     *
     * Update order status
     */
    public function updateStatus(UpdateOrderStatusVO $updateOrderStatusVO, OrderVO $orderVO)
    {
        $this->logger->info('[Update Status] - Pedido '.$orderVO->getOrderId().' iniciou a atualização de status.');
        try {
            // Reopening order...
            if ($orderVO->getStatus()->getId() == B2cPedstatus::CANCELLED
                && $updateOrderStatusVO->getStatus() != B2cPedstatus::CANCELLED
            ) {
                // TODO Implements reopen order rule
            } else {
                // Changing order status...
                // Approved
                if ($updateOrderStatusVO->getStatus() == B2cPedstatus::APPROVED
                    && $orderVO->getStatus()->getId() != B2cPedstatus::APPROVED
                ) {
                    $this->logger->info('[Update Status] - Entrou para aprovar.');
                    //Update product approvalDate
                    $updateProductsDateVO = new UpdateProductsDateVO();
                    $updateProductsDateVO->setOrderId($orderVO->getOrderId());
                    $updateProductsDateVO->setClerk($updateOrderStatusVO->getClerk());
                    //create UpdateProductsDateTypeVO
                    $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
                    $updateProductsDateTypeVO->setType('approvalDate');
                    $updateProductsDateTypeVO->setDate(new \DateTime('now'));
                    //add UpdateProductsDateTypeVO
                    $updateProductsDateVO->addDateType($updateProductsDateTypeVO);

                    foreach ($orderVO->getProducts() as $product) {
                        /* @var $product OrderProductVO */
                        $updateProductsDateProductVO = new UpdateProductsDateProductVO();
                        $updateProductsDateProductVO->setId($product->getProductId());
                        $updateProductsDateVO->addProduct($updateProductsDateProductVO);
                    }
                    //update product date
                    $this->productService->updateDate($updateProductsDateVO, $orderVO);
                } elseif ($updateOrderStatusVO->getStatus() == B2cPedstatus::CANCELLED
                    && $orderVO->getStatus()->getId() != B2cPedstatus::CANCELLED
                ) {
                    $this->logger->info('[Update Status] - Entrou para cancelar.');
                    //Update product status
                    $updateProductStatusVO = new UpdateProductsStatusVO();

                    $updateProductStatusVO->setOrderId($updateOrderStatusVO->getOrderId());
                    $updateProductStatusVO->setClerk($updateOrderStatusVO->getClerk());
                    $updateProductStatusVO->setCancelled(true);
                    $updateProductStatusVO->setTicket($updateOrderStatusVO->getTicket());
                    $updateProductStatusVO->setOcrCode($updateOrderStatusVO->getOcrCode());
                    $updateProductStatusVO->setCancellationIndicator($updateOrderStatusVO->getCancellationIndicator());
                    $updateProductStatusVO->setTicketType($updateOrderStatusVO->getTicketType());
                    $updateProductStatusVO->setHub($updateOrderStatusVO->getHub());
                    $updateProductStatusVO->setShipper($updateOrderStatusVO->getShipper());
                    $updateProductStatusVO->setInvoice($updateOrderStatusVO->getInvoice());
                    $updateProductStatusVO->setProductCauser($updateOrderStatusVO->getProductCauser());

                    /* @var $product OrderProductVO */
                    foreach ($orderVO->getProducts() as $product) {
                        $updateProductStatusProductVO = new UpdateProductsStatusProductVO();
                        $updateProductStatusProductVO->setId($product->getProductId());
                        $updateProductStatusProductVO->setReference($product->getReference());
                        $updateProductStatusProductVO->setQuantity($product->getQuantity());
                        $updateProductStatusVO->addProduct($updateProductStatusProductVO);
                    }
                    //update product status
                    $this->productService->updateStatus($updateProductStatusVO, $orderVO);
                } elseif ($updateOrderStatusVO->getStatus() == B2cPedstatus::CANCELLED
                    && $orderVO->getStatus()->getId() == B2cPedstatus::CANCELLED
                    && $updateOrderStatusVO->getTypeReversal()
                ) {
                    $this->logger->info('[Update Status] - Entrou cancelamento / reversa.');
                    //Update product status
                    $updateProductStatusVO = new UpdateProductsReversalInfoVO();
                    $updateProductStatusVO->setOrderId($updateOrderStatusVO->getOrderId());
                    $updateProductStatusVO->setClerk($updateOrderStatusVO->getClerk());
                    $updateProductStatusVO->setTypeReversal($updateOrderStatusVO->getTypeReversal());
                    $updateProductStatusVO->setOperator($updateOrderStatusVO->getOperator());
                    $updateProductStatusVO->setTransaction($updateOrderStatusVO->getTransaction());

                    /* @var $product OrderProductVO */
                    foreach ($orderVO->getProducts() as $product) {
                        $updateProductStatusProductVO = new UpdateProductsStatusProductVO();
                        $updateProductStatusProductVO->setId($product->getProductId());
                        $updateProductStatusProductVO->setReference($product->getReference());
                        $updateProductStatusProductVO->setQuantity($product->getQuantity());
                        $updateProductStatusVO->addProduct($updateProductStatusProductVO);
                    }
                    //update product status
                    $this->productService->approveReversal($updateProductStatusVO, $orderVO);
                } else {
                    $this->logger->info('[Update Status] - Entrou no que sobrou.');
                    $this->orderStatusService->updateStatus($updateOrderStatusVO, $orderVO);
                }
            }
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return true;
    }

    /**
     * @param int $orderId
     *
     * @return array
     */
    public function getOrderInfo($orderId)
    {
        try {
            // Search the order
            $order = $this->entityManager->getRepository('AppBundle:B2cPedped')->getOrder($orderId);

            $productsInfo = array();

            // Loop into order products
            if (isset($order['products']) && count($order['products']) > 0) {
                foreach ($order['products'] as $product) {
                    // Get product order status
                    $pedprdstatus = $this->entityManager->getRepository('AppBundle:B2cPedprdstatus')
                        ->getProductOrderStatus($orderId, $product['idPrdprd']);

                    // Get order product status
                    $status = $this->entityManager->getRepository('AppBundle:B2cPedprd')
                        ->getStatus($product, $pedprdstatus);

                    // Get production date to product
                    $production = $this->entityManager->getRepository('AppBundle:B2cPedprd')
                        ->getProduction($pedprdstatus);

                    // Get polo
                    $polo = $this->entityManager->getRepository('AppBundle:B2cForfor')
                        ->getPolo($product['prdFornecedor']);

                    // Get all product invoices
                    $invoices = $this->entityManager->getRepository('AppBundle:B2cNotafiscal')
                        ->getInvoices($orderId, $product['prdReferencia']);

                    // Get Shipper info
                    $shipper = $this->entityManager->getRepository('AppBundle:B2cNotafiscalItem')
                        ->getShipper($orderId, $product['prdReferencia']);

                    // Get Branch Office Information
                    $branchOfficeInfo = $this->entityManager->getRepository('AppBundle:B2cPedprd')
                        ->getBranchOfficeInformation($orderId, $product['idPrdprd']);

                    // Format product info to response
                    $productInfo = $this->entityManager->getRepository('AppBundle:B2cPedprd')
                        ->formatProductInfo(
                            $order,
                            $product,
                            $pedprdstatus,
                            $status,
                            $production,
                            $polo,
                            $branchOfficeInfo,
                            $shipper,
                            $invoices
                        );

                    $productInfo['pdf_notafiscal'] = '';
                    if (!empty($shipper['pathPdf'])) {
                        $productInfo['pdf_notafiscal'] = $this->awsService->getS3AuthenticatedUrl($shipper['pathPdf']);
                    }

                    array_push($productsInfo, $productInfo);
                }
            }

            // Get marketplace info
            $marketPlaceInfo = array();
            if ($order['tipoPedido'] == 'marketplace') {
                $marketPlaceInfo = $this->entityManager
                    ->getRepository('AppBundle:B2cPedped')->getMarketPlaceOrder($orderId);
            }

            // Get order status
            list($order['situationName'], $order['internalStatusName']) = $this->entityManager->getRepository('AppBundle:B2cPedped')->getSituation($orderId);

            // Format order info to response
            $orderInfo = $this->entityManager
                ->getRepository('AppBundle:B2cPedped')->formatOrderInfo($order, $marketPlaceInfo);
            $orderInfo['produtos'] = $productsInfo;

            $this->entityManager->getConnection()->close();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $orderInfo;
    }

    /**
     * @param ValidatePurchaseOrder $validatePurchaseOrder
     *
     * @return CreatePurchaseOrderVO
     *
     * Create a createPurchaseOrderVO
     */
    public function getCommonPurchaseOrderInfo(ValidatePurchaseOrder $validatePurchaseOrder)
    {
        try {
            $createPurchaseOrderVO = new CreatePurchaseOrderVO();

            $createPurchaseOrderVO->setOrderId($validatePurchaseOrder->getOrderId());
            $createPurchaseOrderVO->setPurchaseOrderId($validatePurchaseOrder->getPurchaseOrderId());
            $createPurchaseOrderVO->setDate($validatePurchaseOrder->getDate());
            $createPurchaseOrderVO->setClerk($validatePurchaseOrder->getClerk());

            /* @var $product ValidatePurchaseOrderProduct */
            foreach ($validatePurchaseOrder->getProducts() as $product) {
                $createPurchaseOrderProductVO = new CreatePurchaseOrderProductVO();
                $createPurchaseOrderProductVO->setId($product->getId());
                $createPurchaseOrderVO->addProduct($createPurchaseOrderProductVO);
            }

            return $createPurchaseOrderVO;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param CreatePurchaseOrderVO $createPurchaseOrder
     * @param OrderVO $orderVO
     *
     * @return boolean
     *
     * Create Purchase Order
     */
    public function createPurchaseOrder(CreatePurchaseOrderVO $createPurchaseOrder, OrderVO $orderVO)
    {
        $this->entityManager->getConnection()->beginTransaction();

        try {
            // Update product date
            $updateProductsDateVO = new UpdateProductsDateVO();
            $updateProductsDateVO->setOrderId($orderVO->getOrderId());
            $updateProductsDateVO->setClerk($createPurchaseOrder->getClerk());
            //create UpdateProductsDateTypeVO
            $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
            $updateProductsDateTypeVO->setType('releaseDate');
            $updateProductsDateTypeVO->setDate($createPurchaseOrder->getDate());
            //add UpdateProductsDateTypeVO
            $updateProductsDateVO->addDateType($updateProductsDateTypeVO);

            /* @var $purchaseOrderProduct CreatePurchaseOrderProductVO */
            foreach ($createPurchaseOrder->getProducts() as $purchaseOrderProduct) {
                //find products
                $products = $this->entityManager->getRepository('AppBundle:B2cPedprd')
                    ->findBy(array(
                        'idPedped' => $createPurchaseOrder->getOrderId(),
                        'prdReferencia' => $purchaseOrderProduct->getId(),
                    ));
                //check product
                if ($products) {
                    /* @var $product B2cPedprd */
                    foreach ($products as $product) {
                        // create purchase order
                        if ($product->getOrdemCompra() != $createPurchaseOrder->getPurchaseOrderId() ) {
                            $product->setOrdemCompra($createPurchaseOrder->getPurchaseOrderId());
                            $product->setDataOrdemCompra($createPurchaseOrder->getDate());
                            //save
                            $this->entityManager->persist($product);
                            $this->entityManager->flush();
                            //add product to update date
                            $updateProductsDateProductVO = new UpdateProductsDateProductVO();
                            $updateProductsDateProductVO->setId($product->getIdPrdprd());
                            $updateProductsDateVO->addProduct($updateProductsDateProductVO);
                        }
                    }
                } else {
                    throw new Exception('Product not found');
                }
            }
            //try and commit the transaction
            $this->entityManager->getConnection()->commit();

            //set updated order date
            $this->entityManager->getRepository('AppBundle:B2cPedped')->updateOrderUpdateDate($orderVO->getOrderId());

            //check update products
            if (count($updateProductsDateVO->getProducts())) {
                //send event
                $this->eventService->send('PurchaseOrderCreate', $createPurchaseOrder);
                //update products dates
                $this->productService->updateDate($updateProductsDateVO, $orderVO);
            }
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollback();
            throw new Exception($e->getMessage());
        }

        return true;
    }

    /**
     * @param UpdateNewDeliveryDateVO $updateNewDeliveryDateVO
     *
     * @return bool
     *
     * Update new delivery date on order
     */
    public function updateNewDeliveryDate(UpdateNewDeliveryDateVO $updateNewDeliveryDateVO)
    {
        try {
            // Setting new delivery date
            if (!$updateNewDeliveryDateVO->isAccept()) {
                return true;
            }

            $order = $this->entityManager->getRepository('AppBundle:B2cPedped')
                ->findOneBy(array('idPedped' => $updateNewDeliveryDateVO->getOrderId()));

            if (empty($order)) {
                return true;
            }

            $this->entityManager->getRepository('AppBundle:B2cPedped')
                ->updateNewDeliveryDate(
                    $updateNewDeliveryDateVO->getOrderId(),
                    $updateNewDeliveryDateVO->getNewDeliveryDate()
                );

            $orderLogVO = new CreateOrderLogVO();
            $orderLogVO->setOrderId($updateNewDeliveryDateVO->getOrderId());
            $orderLogVO->setCreatedAt(new \Datetime('now'));
            $orderLogVO->setShow(false);
            $orderLogVO->setInternalStatus($order->getPedStaint());
            $orderLogVO->setStatus($order->getPedSituacao());
            $orderLogVO->setTitle(
                'Atualização da nova previsão de data de entrega feita por '.$updateNewDeliveryDateVO->getUser()
            );
            $orderLogVO->setText(
                'Nova previsão de data de entrega alterada para '.
                $updateNewDeliveryDateVO->getNewDeliveryDate()->format('d/m/Y')
            );

            $this->orderLogService->insertOrderLog($orderLogVO);

            $this->eventService->send('UpdateNewDeliveryDate', $updateNewDeliveryDateVO);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return true;
    }
}
