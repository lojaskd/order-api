<?php

namespace AppBundle\Service;

use JMS\Serializer\SerializerBuilder;
use AppBundle\Service\Event\IEvent;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class EventService
 * @package AppBundle\Service
 */
class EventService
{

    /**
     * @var IEvent
     */
    private $event;
    /**
     * @var \JMS\Serializer\Serializer
     */
    private $serializer;
    /**
     * @var Logger
     */
    private $logger;


    /**
     * @param IEvent $event
     * @param Logger $logger
     */
    public function __construct(IEvent $event, Logger $logger)
    {
        $this->event = $event;
        $this->logger = $logger;
        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * @param $route
     * @param $data
     * @return bool
     */
    public function send($route, $data)
    {
        // Send event
        try {
            $this->event->send($route, $this->serializer->serialize($data, 'json'));
        } catch (\Exception $e) {
            $this->logger->error('Event Service Error - ' . $e->getTraceAsString());
            return false;
        }
        return true;
    }
}
