<?php
namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class SearchService
 * @package AppBundle\Service
 */
class SearchService
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * @param EntityManager $entityManager
     * @param Logger $logger
     */
    public function __construct(EntityManager $entityManager, Logger $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param $term
     * @param string $type
     * @return array
     *
     * Search order from term and type
     */
    public function searchOrderFromTermAndType($term, $type)
    {
        $orders = array();

        switch ($type) {
            case 'order':
                $orders = $this->entityManager->getRepository('AppBundle:B2cPedped')->searchFromOrderId($term);
                break;
            case 'invoice':
                $orders = $this->entityManager->getRepository('AppBundle:B2cPedped')->searchFromInvoice($term);
                break;
            case 'email':
                $orders = $this->entityManager->getRepository('AppBundle:B2cPedped')->searchFromEmail($term);
                break;
            case 'document':
                $orders = $this->entityManager->getRepository('AppBundle:B2cPedped')->searchFromDocument($term);
                break;
            case 'phone':
                $orders = $this->entityManager->getRepository('AppBundle:B2cPedped')->searchFromPhoneNumber($term);
                break;
            case 'marketplace':
                $orders = $this->entityManager->getRepository('AppBundle:B2cPedped')->searchFromMarketPlaceOrder($term);
                break;
            case 'name':
                $orders = $this->entityManager->getRepository('AppBundle:B2cPedped')->searchFromCustomerName($term);
                break;
            default:
                break;
        }

        return $orders;
    }
}
