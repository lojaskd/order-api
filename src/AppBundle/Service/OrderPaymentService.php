<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class OrderPaymentService
 * @package AppBundle\Service
 */
class OrderPaymentService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var EventService
     */
    private $eventService;


    /**
     * @param EntityManager $entityManager
     * @param Logger $logger
     * @param EventService $eventService
     */
    public function __construct(EntityManager $entityManager, Logger $logger, EventService $eventService)
    {
        $this->entityManager = $entityManager;
        $this->logger        = $logger;
        $this->eventService  = $eventService;
    }

    /**
     * @param $orderId
     * @return array
     *
     * Get order payment info
     */
    public function getOrderPayment($orderId)
    {
        try {
            return $this->entityManager->getRepository('AppBundle:B2cPagamentoCartao')->findBy(array('idPedido' => $orderId));
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
