<?php

namespace AppBundle\Service;

use AppBundle\Entity\B2cPedstatus;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class OrderStatusService
 * @package AppBundle\Service
 */
class OrderStatusService
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var EventService
     */
    private $eventService;
    /**
     * @var OrderDateService
     */
    private $orderDateService;
    /**
     * @var ProductService
     */
    private $productService;


    /**
     * @param EntityManager $entityManager
     * @param Logger $logger
     * @param EventService $eventService
     * @param OrderDateService $orderDateService
     * @param ProductService $productService
     */
    public function __construct(
        EntityManager $entityManager,
        Logger $logger,
        EventService $eventService,
        OrderDateService $orderDateService,
        ProductService $productService
    ) {
        $this->entityManager = $entityManager;
        $this->eventService = $eventService;
        $this->logger = $logger;
        $this->orderDateService = $orderDateService;
        $this->productService = $productService;
    }

    /**
     * @param UpdateOrderStatusVO $updateOrderStatusVO
     * @param OrderVO $orderVO
     * @return bool
     *
     * Update order status
     */
    public function updateStatus(UpdateOrderStatusVO $updateOrderStatusVO, OrderVO $orderVO)
    {
        try {
            $statusUpdated = false;
            //update status
            if ($orderVO->getStatus()->getId() != $updateOrderStatusVO->getStatus()) {
                // Changing order status...
                switch ($updateOrderStatusVO->getStatus()) {
                    // Under Approval
                    case B2cPedstatus::UNDER_APPROVAL:
                        $this->underApprovalOrder($orderVO);
                        break;
                    // Approved
                    case B2cPedstatus::APPROVED:
                        $this->approveOrder($orderVO);
                        break;
                    default:
                        break;
                }

                // Update order status on b2c_pedped
                $this->entityManager->getRepository('AppBundle:B2cPedped')->updateOrderStatus($updateOrderStatusVO);

                $statusUpdated = true;
            }

            //update internal status
            if ($orderVO->getInternalStatus()->getId() != $updateOrderStatusVO->getInternalStatus()) {
                //update internal status
                $this->entityManager->getRepository('AppBundle:B2cPedped')
                    ->updateOrderInternalStatus($updateOrderStatusVO);
                $statusUpdated = true;
            }

            if ($statusUpdated) {
                if ($updateOrderStatusVO->getDate() instanceof \DateTime) {
                    // Update order status date on b2c_pedstatus
                    $this->entityManager->getRepository('AppBundle:B2cPedstatus')->updateDate($updateOrderStatusVO);
                }
                //send event
                $this->eventService->send('OrderChangeStatus', $updateOrderStatusVO);
                // Insert order status changes log on b2c_pedlog
                $this->entityManager->getRepository('AppBundle:B2cPedlog')->insertUpdateStatusLog($updateOrderStatusVO);
            }
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return true;
    }

    /**
     * @param OrderVO $orderVO
     *
     * Put order under approval
     * @return bool
     */
    private function underApprovalOrder(OrderVO $orderVO)
    {
        //update order deadline
        return $this->orderDateService->updateOrderDeadlines(
            $orderVO,
            $this->entityManager->getRepository('AppBundle:B2cViewProdutos')
                ->getDateAddNextBusinessDay(date('Y-m-d'), 1)
        );
    }

    /**
     *
     * @param OrderVO $orderVO
     *
     * Approve the order
     * @return bool
     */
    private function approveOrder(OrderVO $orderVO)
    {
        //update order deadline
        $this->orderDateService->updateOrderDeadlines($orderVO, date('Y-m-d'));

        $billingAccount = array();

        // Marketplace
        if (!empty($orderVO->getMarketplaceId())) {
            $channel = $this->entityManager->getRepository('AppBundle:B2cPedpedMarketplace')
                ->findOneBy(array(
                    'idMarketplace' => $orderVO->getMarketPlaceId(),
                    'idPedped' => $orderVO->getOrderId()
                ))->getIdChannel();

            # Get billing account to relate the channel marketplace
            $billingAccount = $this->entityManager->getRepository('AppBundle:B2cContaContabilPagamento')
                ->getAccountingMarketPlace($channel);
        }

        if (empty($billingAccount)) {
            if ($orderVO->getFirstPayment()->getId() == 2
                && !empty($orderVO->getFirstPayment()->getBillingAccount())
                && !empty($orderVO->getFirstPayment()->getBillingAccountName())
            ) {
                $billingAccount['codigo'] = $orderVO->getFirstPayment()->getBillingAccount();
                $billingAccount['cardcode'] = $orderVO->getFirstPayment()->getBillingAccountName();
            }

            if ($orderVO->getFirstPayment()->getId() != 2
                || empty($orderVO->getFirstPayment()->getBillingAccount())
                || empty($orderVO->getFirstPayment()->getBillingAccountName())
            ) {
                # Get billing account to relate the order
                $billingAccount = $this->entityManager->getRepository('AppBundle:B2cContaContabilPagamento')
                    ->getAccounting($orderVO->getFirstPayment()->getId());
            }
        }

        // Set billing account
        $this->entityManager->getRepository('AppBundle:B2cPedped')
            ->updateBillingAccount($orderVO->getOrderId(), $billingAccount['codigo'], $billingAccount['cardcode']);

        // Set second payment account
        if ($orderVO->getSecondPayment()->getId()) {
            $secondBillingAccount = array();

            # Get second payment billing account to relate the order
            if ($orderVO->getSecondPayment()->getId() == 2
                && !empty($orderVO->getSecondPayment()->getBillingAccount())
                && !empty($orderVO->getSecondPayment()->getBillingAccountName())
            ) {
                $secondBillingAccount['codigo'] = $orderVO->getSecondPayment()->getBillingAccount();
                $secondBillingAccount['cardcode'] = $orderVO->getSecondPayment()->getBillingAccountName();
            }

            if ($orderVO->getSecondPayment()->getId() != 2
                || empty($orderVO->getSecondPayment()->getBillingAccount())
                || empty($orderVO->getSecondPayment()->getBillingAccountName())
            ) {
                $secondBillingAccount = $this->entityManager->getRepository('AppBundle:B2cContaContabilPagamento')
                    ->getAccounting($orderVO->getSecondPayment()->getId());
            }

            // Set second payment billing account
            $this->entityManager->getRepository('AppBundle:B2cPedped')->updateSecondPaymentBillingAccount(
                $orderVO->getOrderId(),
                $secondBillingAccount['codigo'],
                $secondBillingAccount['cardcode']
            );
        }

        foreach ($orderVO->getProducts() as $product) {
            /* @var $product OrderProductVO */
            if (empty($product->getDestination())) {
                $this->entityManager->getRepository('AppBundle:B2cPedprd')
                    ->setDestination(
                        $product->getOrderId(),
                        $product->getProductId(),
                        $this->entityManager->getRepository('AppBundle:B2cFornecedorRegiao')
                            ->getDestinationBySupplierAndRegionName(
                                $product->getSupplierId(),
                                $orderVO->getRegionName()
                            )
                    );
            }
        }

        return true;
    }

    /**
     * @param OrderVO $orderVO
     * @param string $clerk
     * @return bool
     *
     * Update order status
     */
    public function autoUpdateDatesAndStatus(OrderVO $orderVO, $clerk = 'Sistema')
    {
        try {
            $currentStatus = $orderVO->getStatus()->getId();
            $currentInternalStatus = $orderVO->getInternalStatus()->getId();
            //check if order is cancelled
            if ($currentStatus != B2cPedstatus::CANCELLED) {
                list(
                    $totalProducts,
                    $totalProductsAvail,
                    $totalProductsCancelled,
                    $statusDates
                ) = $this->entityManager->getRepository('AppBundle:B2cPedprd')->getProductsDatesByStatus($orderVO);
                //set control $date
                $date = null;
                //set control $newStatus
                $newStatus = $currentStatus;
                //set control $newInternalStatus
                $newInternalStatus = $currentInternalStatus;

                //cancel order if all products are cancelled
                if ($totalProducts == $totalProductsCancelled) {
                    //set new status to cancelled
                    $newStatus = B2cPedstatus::CANCELLED;
                    $newInternalStatus = 0;
                }

                if ($totalProducts != $totalProductsCancelled) {
                    //dateConfig
                    $statusDateConfig = B2cPedstatus::STATUS_DATE_CONFIG;
                    //foreach status
                    foreach ($statusDates as $dateType => $statusDate) {
                        //set $allDates var
                        $allDates = false;
                        //set $partialDates var
                        $partialDates = false;
                        //default date
                        $date = null;
                        //check all
                        if (count($statusDate['dates']) > 0) {
                            //set max date
                            $date = max($statusDate['dates']);

                            if (count($statusDate['dates']) == $totalProductsAvail) {
                                $allDates = true;
                                //is partial
                            } else {
                                $partialDates = true;
                            }
                        }
                        //check status config
                        if (is_array($statusDateConfig[$dateType])) {
                            $updateOrderStatusVO = new UpdateOrderStatusVO();
                            $updateOrderStatusVO->setOrderId($orderVO->getOrderId());
                            //set status
                            $updateOrderStatusVO->setStatus($statusDateConfig[$dateType]['all']['status']);
                            //set internal status
                            $updateOrderStatusVO->setInternalStatus($statusDateConfig[$dateType]['all']['internal']);
                            //reset status date
                            $updateOrderStatusVO->setDate(null);
                            //all status
                            if ($allDates) {
                                //set status with cancel
                                if ($totalProductsCancelled > 0
                                    && isset($statusDateConfig[$dateType]['all_with_cancel'])
                                ) {
                                    //set status
                                    $updateOrderStatusVO
                                        ->setStatus($statusDateConfig[$dateType]['all_with_cancel']['status']);
                                    //set internal status
                                    $updateOrderStatusVO->setInternalStatus(
                                        $statusDateConfig[$dateType]['all_with_cancel']['internal']
                                    );
                                }
                                //set date
                                $updateOrderStatusVO->setDate($date);
                                //set new status
                                $newStatus = $updateOrderStatusVO->getStatus();
                                //set new internal status
                                $newInternalStatus = $updateOrderStatusVO->getInternalStatus();
                            } elseif ($partialDates && isset($statusDateConfig[$dateType]['partial'])) { //partial
                                //set status
                                $updateOrderStatusVO->setStatus($statusDateConfig[$dateType]['partial']['status']);
                                //set internal status
                                $updateOrderStatusVO->setInternalStatus(
                                    $statusDateConfig[$dateType]['partial']['internal']
                                );
                                //set Date
                                $updateOrderStatusVO->setDate($date);
                                //set new status
                                $newStatus = $updateOrderStatusVO->getStatus();
                                //set new internal status
                                $newInternalStatus = $updateOrderStatusVO->getInternalStatus();
                            }

                            $this->entityManager->getRepository('AppBundle:B2cPedstatus')
                                ->updateDate($updateOrderStatusVO);
                        }
                    }
                }

                //real order status
                $updateOrderStatusVO = new UpdateOrderStatusVO;
                $updateOrderStatusVO->setOrderId($orderVO->getOrderId());
                $updateOrderStatusVO->setStatus($newStatus);
                $updateOrderStatusVO->setOrderCancelled($newStatus == 7 ? 2 : 0);
                $updateOrderStatusVO->setClerk($clerk);
                $updateOrderStatusVO->setInternalStatus($newInternalStatus);

                $this->updateStatus($updateOrderStatusVO, $orderVO);
            }
        } catch (\Exception $e) {
            $this->logger->err($e->getTraceAsString());
            throw new Exception($e->getMessage());
        }

        return true;
    }
}
