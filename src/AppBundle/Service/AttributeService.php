<?php
namespace AppBundle\Service;

use AppBundle\Entity\AttributeJson;
use AppBundle\Entity\Attributes;
use AppBundle\Entity\OrderAttributes;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class AttributeService
 * @package AppBundle\Service
 */
class AttributeService
{

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * @param EntityManager $entityManager
     * @param Logger $logger
     */
    public function __construct(EntityManager $entityManager, Logger $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param integer $orderId
     * @param AttributeJson $attributeJson
     * @return boolean
     *
     * Create or update order attribute
     */
    public function updateAttributeOrder($orderId, AttributeJson $attributeJson)
    {
        /* @var $attribute Attributes */
        $attribute = $this->entityManager->find('AppBundle:Attributes', (int)$attributeJson->getIdAttr());

        if ($attribute->getAttrType() == 4) {
            /* @var $orderAttribute OrderAttributes */
            $orderAttributes = $this->entityManager->getRepository('AppBundle:OrderAttributes')->findBy(array('orderId' => $orderId, 'idAttr' => $attributeJson->getIdAttr()));

            try {
                if (!empty($orderAttributes)) {
                    foreach ($orderAttributes as $orderAttribute) {
                        $this->entityManager->remove($orderAttribute);
                        $this->entityManager->flush();
                    }
                }

                foreach ($attributeJson->getValue() as $value) {
                    $orderAttribute = new OrderAttributes();
                    $orderAttribute->setIdAttr($attributeJson->getIdAttr());
                    $orderAttribute->setOrderId($orderId);
                    $orderAttribute->setValue($value);
                    $orderAttribute->setCreatedAt(new \DateTime('now'));
                    $orderAttribute->setModifiedAt(new \DateTime('now'));

                    $this->entityManager->persist($orderAttribute);
                    $this->entityManager->flush();
                }
            } catch (\Exception $e) {
                throw new Exception('Error creating order attribute: ' . $e->getMessage());
            }
        }

        if ($attribute->getAttrType() != 4) {
            /* @var $orderAttribute OrderAttributes */
            $orderAttribute = $this->entityManager->getRepository('AppBundle:OrderAttributes')->findOneBy(array('orderId' => $orderId, 'idAttr' => $attributeJson->getIdAttr()));

            $attributeJsonValue = $attributeJson->getValue()[0];

            try {
                if (!$orderAttribute) {
                    $orderAttribute = new OrderAttributes();
                    $orderAttribute->setIdAttr($attributeJson->getIdAttr());
                    $orderAttribute->setOrderId($orderId);
                    $orderAttribute->setValue($attributeJsonValue);
                    $orderAttribute->setCreatedAt(new \DateTime('now'));
                    $orderAttribute->setModifiedAt(new \DateTime('now'));
                } else {
                    $orderAttribute->setValue($attributeJsonValue);
                    $orderAttribute->setModifiedAt(new \DateTime('now'));
                }

                $this->entityManager->persist($orderAttribute);
                $this->entityManager->flush();
            } catch (\Exception $e) {
                throw new Exception('Error creating order attribute: ' . $e->getMessage());
            }
        }

        return true;
    }
}
