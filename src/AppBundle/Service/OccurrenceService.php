<?php

namespace AppBundle\Service;

use Symfony\Bridge\Monolog\Logger;

/**
 * Class OccurrenceService
 *
 * @package AppBundle\Service
 */
class OccurrenceService
{

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var CurlService
     */
    private $curlService;

    /**
     * @var string
     */
    private $occurrencesApiUrl;


    /**
     * @param CurlService $curlService
     * @param Logger $logger
     * @param $occurrencesApiUrl
     */
    public function __construct(
        CurlService $curlService,
        Logger $logger,
        $occurrencesApiUrl
    ) {
        $this->curlService = $curlService;
        $this->logger = $logger;
        $this->occurrencesApiUrl = $occurrencesApiUrl;
    }

    /**
     * @param $taskKey
     * @return mixed
     * @throws \Exception
     */
    public function getTask($taskKey)
    {
        try {
            return $this->curlService->execute('GET', $this->occurrencesApiUrl . '/workflow/issues/' . $taskKey);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $filters
     * @return mixed
     * @throws \Exception
     */
    public function searchTask($filters)
    {
        try {
            return $this->curlService->execute('GET', $this->occurrencesApiUrl . '/workflow/tasks?' . $filters);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $task
     * @return mixed
     * @throws \Exception
     */
    public function createTask($task)
    {
        try {
            return $this->curlService->execute('POST', $this->occurrencesApiUrl . '/workflow/tasks', json_encode($task));
        } catch (\Exception $exception) {
            throw new \Exception('Error creating task: ' . $exception->getMessage());
        }
    }

    /**
     * @param string $taskKey
     * @param string $comment
     * @return mixed
     * @throws \Exception
     */
    public function commentTask($taskKey, $comment)
    {
        try {
            return $this->curlService->execute('POST', $this->occurrencesApiUrl . '/workflow/tasks/' . $taskKey . '/comments', json_encode(array('body' => $comment)));
        } catch (\Exception $exception) {
            throw new \Exception('Error commenting on task: ' . $exception->getMessage());
        }
    }

    /**
     * @param $link
     * @return mixed
     * @throws \Exception
     */
    public function linkTasks($link)
    {
        try {
            return $this->curlService->execute('POST', $this->occurrencesApiUrl . '/workflow/tasks/links', json_encode($link));
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $taskKey
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function updateTask($taskKey, $data)
    {
        try {
            return $this->curlService->execute('PUT', $this->occurrencesApiUrl . '/workflow/tasks/' . $taskKey, json_encode($data));
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $taskKey
     * @param $transitionId
     * @return mixed
     * @throws \Exception
     */
    public function transitionTask($taskKey, $transitionId)
    {
        try {
            return $this->curlService->execute('PUT', $this->occurrencesApiUrl . '/workflow/tasks/' . $taskKey . '/transitions/' . $transitionId);
        } catch (\Exception $exception) {
            throw new \Exception('Error transitioning task: ' . $exception->getMessage());
        }
    }

    /**
     * @param $ticketId
     * @return mixed
     * @throws \Exception
     */
    public function getTicket($ticketId)
    {
        try {
            return $this->curlService->execute('GET', $this->occurrencesApiUrl . '/occurrences/' . $ticketId);
        } catch (\Exception $exception) {
            throw new \Exception('Error getting ticket: ' . $exception->getMessage());
        }
    }

    /**
     * @param $ticketId
     * @param $ticketData
     * @return mixed
     * @throws \Exception
     */
    public function updateTicket($ticketId, $ticketData)
    {
        try {
            return $this->curlService->execute('PUT', $this->occurrencesApiUrl . '/occurrences/' . $ticketId, json_encode($ticketData));
        } catch (\Exception $exception) {
            throw new \Exception('Error updating ticket: ' . $exception->getMessage());
        }
    }

    /**
     * Comments on ticket
     *
     * @param integer $ticketId
     * @param string $comment
     * @param boolean $type
     * @return bool
     * @throws \Exception
     */
    public function commentTicket($ticketId, $comment, $type)
    {
        try {
            $createComment = $this->curlService->execute(
                'PUT',
                $this->occurrencesApiUrl . '/occurrences/' . $ticketId . '/comment',
                json_encode(
                    array(
                        'user_id' => '8616913087',
                        'public' => $type,
                        'body' => $comment
                    )
                )
            );

            if (!isset($createComment['code']) || (isset($createComment['code']) && $createComment['code'] != 200)) {
                throw new \Exception('Error creating ticket comment: ' . serialize($createComment));
            }

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
