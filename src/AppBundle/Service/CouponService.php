<?php
namespace AppBundle\Service;

use AppBundle\Entity\VO\Create\CreateCouponVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class CouponService
 * @package AppBundle\Service
 */
class CouponService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Logger
     */
    private $logger;


    /**
     * @param EntityManager $entityManager
     * @param Logger $logger
     */
    public function __construct(EntityManager $entityManager, Logger $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param $discountCode
     * @param CreateOrderVO $order
     * @return CreateCouponVO
     *
     * Validate the coupon code
     */
    public function validateCoupon($discountCode, CreateOrderVO $order)
    {
        $this->logger->info('API_PEDIDOS - Validating coupon code ' . $discountCode);
        $couponVO = new CreateCouponVO();

        // Search on database from a valid coupon based on coupon code and order data
        $validCoupon = $this->entityManager->getRepository('AppBundle:B2cPromo')->getValidCouponFromCode(
            $discountCode,
            $order->getOrigin()->getSource(),
            $order->getOrigin()->getMedium(),
            $order->getOrigin()->getContent(),
            $order->getRegion()->getId(),
            $order->getCustomer()->getId(),
            implode(', ', $order->getProductsId()),
            implode(', ', $order->getPaymentsId()),
            implode(', ', $order->getCategories()),
            implode(', ', $order->getProviders()),
            $order->getSubtotal()
        );

        // If it's not payment made and it's a valid coupon, apply it
        if ($validCoupon && count($order->getPaymentsId()) == 1) {
            $this->logger->info('API_PEDIDOS - Coupon code (' . $discountCode . ') is valid and there is just one payment method.');
            $this->logger->info('API_PEDIDOS - Coupon info: ' . serialize($validCoupon));
            $couponDiscount = 0;

            // If coupon is used to a specific category
            if ($validCoupon['usarCategoria']) {
                // For each product, valid if coupon is valid
                /* @var $product CreateProductVO */
                foreach ($order->getProducts() as $product) {
                    $categoryCouponValid = $this->entityManager->getRepository('AppBundle:B2cPromoCat')->validateCategoryCoupon(
                        $validCoupon['idPromo'],
                        $product->getCategory()
                    );

                    // if category coupon is valid, add it to coupon discount amount
                    if ($categoryCouponValid) {
                        if ($validCoupon['descontoPerc'] > 0) {
                            $couponDiscount += ($product->getTotalprice() / $product->getQuantity()) * $validCoupon['descontoPerc'] / 100;
                        } elseif ($validCoupon['descontoValor'] > 0) {
                            $couponDiscount += $validCoupon['descontoValor'];
                        }
                    }
                }
                // If coupon is used to a specific provider
            } elseif ($validCoupon['usarFornecedor']) {
                // For each product, valid if coupon is valid
                /* @var $product CreateProductVO */
                foreach ($order->getProducts() as $product) {
                    $providerCouponValid = $this->entityManager->getRepository('AppBundle:B2cPromoFor')->validateProviderCoupon(
                        $validCoupon['idPromo'],
                        $product->getProvider()
                    );

                    // if provider coupon is valid, add it to coupon discount amount
                    if ($providerCouponValid) {
                        if ($validCoupon['descontoPerc'] > 0) {
                            $couponDiscount += ($product->getTotalprice() / $product->getQuantity()) * $validCoupon['descontoPerc'] / 100;
                        } elseif ($validCoupon['descontoValor'] > 0) {
                            $couponDiscount += $validCoupon['descontoValor'];
                        }
                    }
                }
            } else {
                // If it is a generic coupon, appply the discount and add it to coupon discount amount
                if ($validCoupon['descontoPerc'] > 0) {
                    $couponDiscount += $order->getSubtotal() * $validCoupon['descontoPerc'] / 100;
                } elseif ($validCoupon['descontoValor'] > 0) {
                    $couponDiscount += $validCoupon['descontoValor'];
                }
            }

            $validCouponCode = false;
            // If the coupon is used on payment method, validate it
            if ($validCoupon['aplicarPagamento']) {
                $paymentCouponValid = $this->entityManager->getRepository('AppBundle:B2cPromoPag')->validatePaymentCoupon(
                    $validCoupon['idPromo'],
                    $order->getFirstPayment()
                );

                if ($paymentCouponValid) {
                    $validCouponCode = true;
                }
                // If the coupon is used on delivery, validate it
            } elseif ($validCoupon['aplicarEntrega']) {
                $deliveryCouponValid = $this->entityManager->getRepository('AppBundle:B2cPromoReg')->validateDeliveryCoupon(
                    $validCoupon['idPromo'],
                    $order->getRegion()->getId()
                );

                if ($deliveryCouponValid) {
                    $validCouponCode = true;
                }
            } else {
                // If there's no coupon type, the coupon is set as valid
                $validCouponCode = true;
            }

            // If it is a valid coupon, return it
            if ($validCouponCode) {
                $this->logger->info('API_PEDIDOS - Coupon code match coupon rules, It will be applied.');

                $couponVO->setIdPromo($validCoupon['idPromo']);
                $couponVO->setCode($discountCode);
                $couponVO->setPromotionName('Vale-Compra');
                $couponVO->setDiscountValue($couponDiscount);
                $couponVO->setFreeShipping($validCoupon['freteGratis']);
                $couponVO->setFixedShipping($validCoupon['freteFixo']);
                $couponVO->setFixedValue($validCoupon['freteFixoValor']);
                $couponVO->setShippingIncluded($validCoupon['promoIncluiFrete']);
            } else {
                $this->logger->info('API_PEDIDOS - Coupon code will not be applied because the order info does not match coupon rules.');
            }
        } else {
            $this->logger->info('API_PEDIDOS - Coupon code is invalid or there is more than one payment method.');
        }

        return $couponVO;
    }
}
