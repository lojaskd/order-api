<?php

namespace AppBundle\Service;

use AppBundle\Entity\Validate\ValidateOrder;
use AppBundle\Entity\VO\Create\CreateMarketPlaceVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class MarketPlaceService
 *
 * @package AppBundle\Service
 */
class MarketPlaceService
{

    /**
     * Channel ID MarketPlace Americanas
     */
    const MKPAMERICANAS = 1;
    /**
     * Channel ID MarketPlace Submarino
     */
    const MKPSUBMARINO = 2;
    /**
     * Channel ID MarketPlace Shoptime
     */
    const MKPSHOPTIME = 3;
    /**
     * Channel ID MarketPlace Sou Barato
     */
    const MKPSOUBARATO = 4;
    /**
     * Channel ID MarketPlace Extra
     */
    const MKPEXTRA = 5;
    /**
     * Channel ID MarketPlace Casas Bahia
     */
    const MKPCASASBAHIA = 6;
    /**
     * Channel ID MarketPlace Ponto Frio
     */
    const MKPPONTOFRIO = 7;
    /**
     * Channel ID MarketPlace CDiscount
     */
    const MKPCDISCOUNT = 8;
    /**
     * Channel ID MarketPlace Walmart
     */
    const MKPWALMART = 9;
    /**
     * Channel ID MarketPlace Mercado Livre
     */
    const MKPMERCADOLIVRE = 10;
    /**
     * Channel ID MarketPlace Carrefour
     */
    const MKPCARREFOUR = 11;
    /**
     * Channel ID MarketPlace Zoom
     */
    const MKPZOOM = 12;
    /**
     * Attribute ID Mercado Livre Stock
     */
    const ATTRSTOCKMERCADOLIVRE = 334;
    /**
     * Attribute ID B2W Stock
     */
    const ATTRSTOCKB2W = 335;
    /**
     * Attribute ID CNova Stock
     */
    const ATTRSTOCKCNOVA = 336;
    /**
     * Attribute ID Walmart Stock
     */
    const ATTRSTOCKWALMART = 337;
    /**
     * Attribute ID Carrefour Stock
     */
    const ATTRSTOCKCARREFOUR = 338;
    /**
     * Attribute ID Zoom Stock
     */
    const ATTRSTOCKZOOM = 363;
    /**
     * Status without calculated freight
     */
    const WITHOUTCALCULATEDFREIGHT = 232;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     * @param $marketPlaceEmail
     * @param ShippingService $shippingService
     * @param ProductService $productService
     * @param CustomerService $customerService
     * @param Logger $logger
     */
    public function __construct(
        EntityManager $entityManager,
        $marketPlaceEmail,
        ShippingService $shippingService,
        ProductService $productService,
        CustomerService $customerService,
        Logger $logger
    ) {
        $this->entityManager = $entityManager;
        $this->marketPlaceEmail = $marketPlaceEmail;
        $this->shippingService = $shippingService;
        $this->productService = $productService;
        $this->customerService = $customerService;
        $this->logger = $logger;
    }

    /**
     * @param CreateOrderVO $orderVO
     * @param ValidateOrder $order
     *
     * @return array
     *
     * Generate the data (shipping, coupon, values, ...) to marketplace order and return them on OrderVO object
     */
    public function getMarketPlaceData(CreateOrderVO $orderVO, ValidateOrder $order)
    {
        $errors = array();

        $this->logger->info('API_PEDIDOS - Getting marketplace customer.');
        // Get marketplace customer info
        $customer = $this->entityManager->getRepository('AppBundle:B2cClicli')
            ->getMarketPlaceCustomer($order->getCustomer()->getCpf(), $order->getCustomer()->getEmail());

        try {
            // Create the marketplace customer if it does not exist
            $order->getCustomer()->setPassword($this->generateRandomPassword());
            $customerVO = $this->customerService
                ->getOrCreateCustomer($customer, $order->getCustomer(), $order->getDeliveryAddress());
            $orderVO->setCustomer($customerVO);

            $this->logger->info('API_PEDIDOS - MarketPlace customer found/created ('.$customerVO->getId().').');
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }

        // If there're no errors creating customer, get shipping and coupon info
        if (empty($errors)) {
            // Calculate shipping values to order
            $shippingVO = $this->shippingService->calculateShipping($orderVO);
            // Default status
            $orderVO->setStatusInternal(self::WITHOUTCALCULATEDFREIGHT);
            if ($shippingVO->getId()) {
                // Set the shipping price and type to that sent on request
                $orderVO->setStatusInternal(0);
            }
            $shippingVO->setPrice($order->getShipping()->getPrice());
            $shippingVO->setShippingType($order->getShipping()->getType());
            $orderVO->setShipping($shippingVO);

            // marketplace
            $marketplaceVO = new CreateMarketPlaceVO();
            $marketplaceVO->setId($order->getMarketplace()->getId());
            $marketplaceVO->setOrder($order->getMarketplace()->getOrder());
            $marketplaceVO->setChannelId($order->getMarketplace()->getChannelId());
            // set new delivery date
            if ($order->getMarketplace()->getNewDeliveryDate() instanceof \Datetime) {
                $marketplaceVO->setNewDeliveryDate($order->getMarketplace()->getNewDeliveryDate());
            }
            $orderVO->setMarketplace($marketplaceVO);

            // Compare the order values from request to the values from database
            $validateOrdervalues = $this->validateMarketPlaceOrderValues($orderVO, $order);
            if ($validateOrdervalues['errors']) {
                $errors[] = $validateOrdervalues['errors'];
            }

            $this->logger->info('API_PEDIDOS - MarketPlace total value ('.$validateOrdervalues['total'].
                ') and discount ('.$validateOrdervalues['discount'].') calculated.');
            $orderVO->setTotal($validateOrdervalues['total']);
            $orderVO->setDiscount($validateOrdervalues['discount']);
        }

        return array('errors' => $errors, 'orderVO' => $orderVO);
    }

    /**
     * @param CreateOrderVO $orderVO
     * @param ValidateOrder $inputOrder
     *
     * @return array
     *
     * Validate MarketPlace order values
     */
    public function validateMarketPlaceOrderValues(CreateOrderVO $orderVO, ValidateOrder $inputOrder)
    {
        $errors = array();

        $this->logger->info('API_PEDIDOS - Validating marketplace order values...');

        // Check if there is stock to the products
        $validStock = $this->productService->validateMarketPlaceProductStock(
            $orderVO->getProducts(),
            $orderVO->getMarketplace()->getId(),
            $orderVO->getMarketplace()->getChannelId()
        );
        if (!$validStock) {
            $errors[] = 'There is no available stock to all products.';
        }

        // Compare the subtotal from database with subtotal sent in request
        $inputOrderSubtotal = number_format($inputOrder->getSubtotal(), 2, '.', '');
        $subtotal = number_format($orderVO->getSubtotal(), 2, '.', '');
        if (round(floatval($inputOrderSubtotal) + 0.5) != round(floatval($subtotal) + 0.5)) {
            $errors[] = 'Subtotal value (R$ '.$inputOrderSubtotal.') does not match (R$ '.$subtotal.').';
        }

        // Generate the order total price
        $total = $orderVO->getSubtotal() - $inputOrder->getDiscount();
        if ($orderVO->getShipping() && $orderVO->getShipping()->getPrice()) {
            $total += $orderVO->getShipping()->getPrice();
        }
        $total = number_format($total, 2, '.', '');

        return array('errors' => $errors, 'total' => $total, 'discount' => $inputOrder->getDiscount());
    }

    /**
     * @param int $length
     *
     * @return string
     */
    private function generateRandomPassword($length = 10)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr(str_shuffle($chars), 0, $length);

        return $password;
    }

    /**
     * @param $channelId
     *
     * @return int
     *
     * Gets marketplace attribute ID to channel stock
     */
    public function getMarketplaceAttrStock($channelId)
    {
        $idAttrStock = 0;

        if (in_array(
            $channelId,
            array(self::MKPAMERICANAS, self::MKPSUBMARINO, self::MKPSHOPTIME, self::MKPSOUBARATO)
        )) {
            $idAttrStock = self::ATTRSTOCKB2W;
        } elseif (in_array(
            $channelId,
            array(self::MKPEXTRA, self::MKPCASASBAHIA, self::MKPPONTOFRIO, self::MKPCDISCOUNT)
        )) {
            $idAttrStock = self::ATTRSTOCKCNOVA;
        } elseif ($channelId == self::MKPWALMART) {
            $idAttrStock = self::ATTRSTOCKWALMART;
        } elseif ($channelId == self::MKPMERCADOLIVRE) {
            $idAttrStock = self::ATTRSTOCKMERCADOLIVRE;
        } elseif ($channelId == self::MKPCARREFOUR) {
            $idAttrStock = self::ATTRSTOCKCARREFOUR;
        } elseif ($channelId == self::MKPZOOM) {
            $idAttrStock = self::ATTRSTOCKZOOM;
        }

        return $idAttrStock;
    }
}
