<?php
namespace AppBundle\Service;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class QueueService
 * @package AppBundle\Service
 */
class QueueService
{

    /**
     * @var AMQPStreamConnection
     */
    private $connection;

    /**
     * @var AMQPChannel
     */
    private $channel;

    /**
     * @var bool
     */
    private $isConnected = false;

    /**
     * @var bool
     */
    private $disconnectOnSend = true;


    /**
     * @param $queueHost
     * @param $queueVhost
     * @param $queuePort
     * @param $queueUser
     * @param $queuePassword
     * @param $queueReduceStockExchange
     * @param $queueReduceStockRoute
     * @param $queueOrderEmailExchange
     * @param $queueOrderEmailRoute
     * @param $queueOrdersVHost
     */
    public function __construct(
        $queueHost,
        $queueVhost,
        $queuePort,
        $queueUser,
        $queuePassword,
        $queueReduceStockExchange,
        $queueReduceStockRoute,
        $queueOrderEmailExchange,
        $queueOrderEmailRoute,
        $queueOrdersVHost
    ) {
        $this->queueHost = $queueHost;
        $this->queueVhost = $queueVhost;
        $this->queuePort = $queuePort;
        $this->queueUser = $queueUser;
        $this->queuePassword = $queuePassword;
        $this->queueReduceStockExchange = $queueReduceStockExchange;
        $this->queueReduceStockRoute = $queueReduceStockRoute;
        $this->queueOrderEmailExchange = $queueOrderEmailExchange;
        $this->queueOrderEmailRoute = $queueOrderEmailRoute;
        $this->queueOrdersVHost = $queueOrdersVHost;
    }

    /**
     * @return bool
     *
     * Connect to queue
     */
    private function connect()
    {
        try {
            if (!$this->isConnected) {
                $this->connection = new AMQPStreamConnection($this->queueHost, $this->queuePort, $this->queueUser, $this->queuePassword, $this->queueVhost);
                $this->channel = $this->connection->channel();
            }
            $this->isConnected = true;
        } catch (\Exception $e) {
            throw new Exception('Error connecting to queue: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * @return bool
     *
     * Disconnect from queue
     */
    private function disconnect()
    {
        try {
            if($this->disconnectOnSend){
                $this->channel->close();
                $this->connection->close();
                $this->isConnected = false;
            }
        } catch (\Exception $e) {
            throw new Exception('Error disconnecting from queue: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * @param $exchange
     * @param $queue
     * @param $routingKey
     * @param string $typeExchange
     * @param array $queueParams
     * @return bool
     */
    public function createQueue($exchange, $queue, $routingKey, $typeExchange = 'direct', $queueParams = array())
    {
        try {
            if ($this->isConnected) {
                $this->channel->queue_declare($queue, false, true, false, false, false, $queueParams);
                $this->channel->exchange_declare($exchange, $typeExchange, false, true, false);
                $this->channel->queue_bind($queue, $exchange, $routingKey);

                return true;
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $message
     * @param $exchange
     * @param $routingKey
     * @return bool
     */
    public function publishMessage($message, $exchange, $routingKey)
    {
        try {
            if ($this->connect()) {
                $this->channel->basic_publish($message, $exchange, $routingKey);
                return true;
            }
            return false;
        } catch (Exception $e) {
            $this->disconnect();
            return false;
        }
    }

    /**
     * @param $message
     * @param $exchange
     * @param $routingKey
     * @return bool
     */
    public function publishOrderMessage($message, $exchange, $routingKey)
    {
        try {

            if (!$this->isConnected) {
                $this->connection = new AMQPStreamConnection($this->queueHost, $this->queuePort, $this->queueUser, $this->queuePassword, $this->queueOrdersVHost);
                $this->channel = $this->connection->channel();
                $this->isConnected = true;
            }

            $this->channel->basic_publish($message, $exchange, $routingKey);

            $this->disconnect();
            return true;
        } catch (Exception $e) {
            $this->disconnect();
            return false;
        }
    }

    /**
     * @param $msg
     * @param $exchange
     * @param string $route
     * @return bool
     */
    public function sendMsg($msg, $exchange, $route = '')
    {
        try {
            if ($this->connect()) {
                $msg = new AMQPMessage($msg, array('content_type' => 'text/plain', 'delivery_mode' => 1));
                $this->channel->basic_publish($msg, $exchange, $route);

                return true;
            }
            return false;
        } catch (Exception $e) {
            $this->disconnect();
            return false;
        }
    }

    /**
     * @param $msg
     * @param $exchange
     * @param string $route
     * @return bool
     */
    public function sendOrderMsg($msg, $exchange, $route = '')
    {
        try {

            if(!$this->isConnected){
                $this->connection = new AMQPStreamConnection($this->queueHost, $this->queuePort, $this->queueUser, $this->queuePassword, $this->queueOrdersVHost);
                $this->channel = $this->connection->channel();
                $this->isConnected = true;
            }

            $msg = new AMQPMessage($msg, array('content_type' => 'text/plain', 'delivery_mode' => 1));
            $this->channel->basic_publish($msg, $exchange, $route);

            $this->disconnect();
            return true;
        } catch (Exception $e) {
            $this->disconnect();
            return false;
        }
    }

    /**
     * @param $exchange
     * @param $queue
     * @param $retryExchange
     * @param $retryQueue
     * @param $failExchange
     * @param $failQueue
     * @param $routingKey
     * @throws \Exception
     */
    public function createQueues($exchange, $queue, $retryExchange, $retryQueue, $failExchange, $failQueue, $routingKey)
    {
        try {

            if(!$this->isConnected){
                $this->connection = new AMQPStreamConnection($this->queueHost, $this->queuePort, $this->queueUser, $this->queuePassword, $this->queueOrdersVHost);
                $this->channel = $this->connection->channel();
                $this->isConnected = true;
            }

            $queueParams = array(
                'x-dead-letter-exchange' => array(
                    'S',
                    $retryQueue,
                ),
            );

            $queueParamsRetry = array(
                'x-dead-letter-exchange' => array(
                    'S',
                    $queue,
                ),
                'x-message-ttl' => array(
                    'I',
                    100000,
                ),
            );
            // Creates work queue
            $this->createQueue($exchange, $queue, $routingKey, 'direct', $queueParams);
            // Creates retry queue
            $this->createQueue($retryExchange, $retryQueue, $routingKey, 'direct', $queueParamsRetry);
            // Creates fail queue
            $this->createQueue($failExchange, $failQueue, $routingKey, 'direct');

        } catch (\Exception $e) {
            $this->disconnect();
            throw $e;
        }
    }

    /**
     * @param $queue
     * @param $routingKey
     * @param int $quantity
     * @param array $callback
     * @return bool
     */
    public function getMsgs($queue, $routingKey, $quantity = 1, $callback = array())
    {
        try {

            $this->disconnectOnSend = false;

            if (!$this->isConnected) {
                $this->connection = new AMQPStreamConnection($this->queueHost, $this->queuePort, $this->queueUser, $this->queuePassword, $this->queueOrdersVHost);
                $this->channel = $this->connection->channel();
                $this->isConnected = true;
            }

            $this->channel->basic_qos(null, $quantity, null);
            $this->channel->basic_consume($queue, $routingKey, false, false, false, false, $callback);

            $shutdown = function ($channel, $connection) {
                /* @var $channel AMQPChannel */
                $channel->close();
                /* @var $connection AMQPStreamConnection */
                $connection->close();
            };

            register_shutdown_function($shutdown, $this->channel, $this->connection);

            // Loop as long as the channel has callbacks registered
            while (count($this->channel->callbacks)) {
                echo "---> Waiting for messages ... \n";
                $this->channel->wait();
            }
        } catch (Exception $e) {
            $this->disconnect();
            return false;
        }

        return true;
    }

    /**
     * @param $messageData
     * @return bool
     *
     * Send data to reduce stock queue
     */
    public function sendReduceStock($messageData)
    {
        try {
            if ($this->connect()) {
                $message = new AMQPMessage($messageData, array('content_type' => 'text/plain', 'delivery_mode' => 2));
                $this->channel->basic_publish($message, $this->queueReduceStockExchange, $this->queueReduceStockRoute);
                $this->disconnect();
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw new Exception('Error sending message to reduce stock queue: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * @param $messageData
     * @return bool
     *
     * Send data to customer order email queue
     */
    public function sendOrderEmail($messageData)
    {
        try {
            if ($this->connect()) {
                $message = new AMQPMessage($messageData, array('content_type' => 'text/plain', 'delivery_mode' => 2));
                $this->channel->basic_publish($message, $this->queueOrderEmailExchange, $this->queueOrderEmailRoute);
                $this->disconnect();
            } else {
                return false;
            }
        } catch (Exception $e) {
            throw new Exception('Error sending queue message to customer order email queue: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * @param array $message
     * @throws \Exception
     */
    public function publishPurchaseCancellationMsg($message)
    {
        try {
            $this->createQueues(
                'purchase.order',
                'purchase.order',
                'purchase.order.retry',
                'purchase.order.retry',
                'purchase.order.fail',
                'purchase.order.fail',
                ''
            );
            $this->sendOrderMsg(json_encode($message), 'purchase.order');
        } catch (\Exception $e) {
            throw new \Exception('Error sending purchase order message to queue: ' . $e->getMessage());
        }
    }

    /**
     * @param array $message
     * @throws \Exception
     */
    public function publishOrderShipmentMsg($message)
    {
        try {
            $this->createQueues(
                'order.shipment',
                'order.shipment',
                'order.shipment.retry',
                'order.shipment.retry',
                'order.shipment.fail',
                'order.shipment.fail',
                ''
            );
            $this->sendOrderMsg(json_encode($message), 'order.shipment');
        } catch (\Exception $e) {
            throw new \Exception('Error sending shipment order message to queue: ' . $e->getMessage());
        }
    }

    /**
     * @param array $message
     * @throws \Exception
     */
    public function publishDeliveredOrderMsg($message)
    {
        try {
            $this->createQueues(
                'order.delivery',
                'order.delivery',
                'order.delivery.retry',
                'order.delivery.retry',
                'order.delivery.fail',
                'order.delivery.fail',
                ''
            );
            $this->sendOrderMsg(json_encode($message), 'order.delivery');
        } catch (\Exception $e) {
            throw new \Exception('Error sending delivered order message to queue: ' . $e->getMessage());
        }
    }

    /**
     * Publish cancel order message on queue
     *
     * @param array $message
     * @throws \Exception
     */
    public function publishCancelOrderMsg($message)
    {
        try {
            $this->createQueues(
                'order.cancellation',
                'order.cancellation',
                'order.cancellation.retry',
                'order.cancellation.retry',
                'order.cancellation.fail',
                'order.cancellation.fail',
                ''
            );
            $this->sendOrderMsg(json_encode($message), 'order.cancellation');
        } catch (\Exception $e) {
            throw new \Exception('Error sending cancel order message to queue: ' . $e->getMessage());
        }
    }

    /**
     * Publish cancel SAP order message on queue
     *
     * @param array $message
     * @throws \Exception
     */
    public function publishCancelSAPOrderMsg($message)
    {
        try {
            $this->createQueues(
                'sap.order.cancellation',
                'sap.order.cancellation',
                'sap.order.cancellation.retry',
                'sap.order.cancellation.retry',
                'sap.order.cancellation.fail',
                'sap.order.cancellation.fail',
                ''
            );
            $this->sendOrderMsg(json_encode($message), 'sap.order.cancellation');
        } catch (\Exception $e) {
            throw new \Exception('Error sending cancel SAP order message to queue: ' . $e->getMessage());
        }
    }

    /**
     * Publish cancel Dropshipping order message on queue
     *
     * @param array $message
     * @throws \Exception
     */
    public function publishCancelDropshippingMsg($message)
    {
        try {
            $this->createQueues(
                'cancellation',
                'dropshipping.cancellation',
                'cancellation.retry',
                'dropshipping.cancellation.retry',
                'cancellation.fail',
                'dropshipping.cancellation.fail',
                'dropshipping'
            );

            $this->sendOrderMsg(json_encode($message), 'cancellation', 'dropshipping');
        } catch (\Exception $e) {
            throw new \Exception('Error sending cancel Dropshipping order message to queue: ' . $e->getMessage());
        }
    }
}
