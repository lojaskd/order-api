<?php

namespace AppBundle\Service;

use AppBundle\Entity\B2cNotafiscal;
use AppBundle\Entity\B2cNotafiscalItem;
use AppBundle\Entity\B2cPedprd;
use AppBundle\Entity\Validate\Create\CreateValidateInvoice;
use AppBundle\Entity\Validate\Create\CreateValidateInvoiceItem;
use AppBundle\Entity\Validate\Update\UpdateValidateInvoice;
use AppBundle\Entity\Validate\ValidateInvoiceDate;
use AppBundle\Entity\VO\Create\CreateInvoiceItemVO;
use AppBundle\Entity\VO\Create\CreateInvoiceVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateInvoiceDateVO;
use AppBundle\Entity\VO\Update\UpdateInvoiceVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateProductVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateTypeVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class InvoiceService
 * @package AppBundle\Service
 */
class InvoiceService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var EventService
     */
    private $event;

    /**
     * @param EntityManager $entityManager
     * @param Logger $logger
     * @param ProductService $productService
     * @param EventService $event
     */
    public function __construct(EntityManager $entityManager, Logger $logger, ProductService $productService, EventService $event)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->productService = $productService;
        $this->event = $event;
    }


    /**
     * Get invoice data from invoice and order IDs
     *
     * @param $invoiceId
     * @param $orderId
     * @return B2cNotafiscal|null|object
     */
    public function getInvoice($invoiceId, $orderId)
    {
        return $this->entityManager->getRepository('AppBundle:B2cNotafiscal')->findOneBy(array('idNotafiscal' => $invoiceId, 'idPedped' => $orderId));
    }

    /**
     * @param ValidateInvoiceDate $invoiceDate
     * @return UpdateInvoiceDateVO
     *
     * Create an InvoiceDateVO
     */
    public function getCommonInfoInvoiceDate(ValidateInvoiceDate $invoiceDate)
    {
        try {
            $invoiceDateVO = new UpdateInvoiceDateVO();

            $invoiceDateVO->setOrderId($invoiceDate->getOrderId());
            $invoiceDateVO->setInvoiceId($invoiceDate->getInvoiceId());
            $invoiceDateVO->setType($invoiceDate->getType());
            $invoiceDateVO->setDate($invoiceDate->getDate());
            $invoiceDateVO->setClerk($invoiceDate->getClerk());

            return $invoiceDateVO;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param CreateValidateInvoice $validateInvoice
     * @return CreateInvoiceVO
     *
     * Create an CreateInvoiceVO
     */
    public function getCommonInvoiceInfo(CreateValidateInvoice $validateInvoice)
    {
        try {
            $createInvoiceVO = new CreateInvoiceVO();
            $createInvoiceVO->setOrderId($validateInvoice->getOrderId());
            $createInvoiceVO->setBranchId($validateInvoice->getBranchId());
            $createInvoiceVO->setInvoiceId($validateInvoice->getInvoiceId());
            $createInvoiceVO->setCreatedDate($validateInvoice->getCreatedDate());
            $createInvoiceVO->setMarketplaceIntegration($validateInvoice->isMarketplaceIntegration());
            $createInvoiceVO->setKey($validateInvoice->getKey());
            $createInvoiceVO->setClerk($validateInvoice->getClerk());
            $createInvoiceVO->setShipperName($validateInvoice->getShipperName());
            $createInvoiceVO->setShipperDoc($validateInvoice->getShipperDoc());
            $createInvoiceVO->setHub($validateInvoice->getHub());

            if ($validateInvoice->isReturned() === null) {
                $createInvoiceVO->setReturned(false);
            }

            if ($validateInvoice->getPredictDeliveryDate() instanceof \DateTime) {
                $createInvoiceVO->setPredictDeliveryDate($validateInvoice->getPredictDeliveryDate());
            }
            if ($validateInvoice->getDeliveryDate() instanceof \DateTime) {
                $createInvoiceVO->setDeliveryDate($validateInvoice->getDeliveryDate());
            }
            if ($validateInvoice->getReturnedDate() instanceof \DateTime) {
                $createInvoiceVO->setReturnedDate($validateInvoice->getReturnedDate());
            }

            $createInvoiceVO->setSerie($validateInvoice->getSerie());

            /* @var $validateInvoiceItem CreateValidateInvoiceItem */
            foreach ($validateInvoice->getItens() as $validateInvoiceItem) {
                $createInvoiceItemVO = new CreateInvoiceItemVO();
                $createInvoiceItemVO->setProductReference($validateInvoiceItem->getProductReference());
                if ($validateInvoiceItem->isReturned() === null) {
                    $createInvoiceItemVO->setReturned(false);
                }
                if ($validateInvoiceItem->getReturnedDate() instanceof \DateTime) {
                    $createInvoiceItemVO->setReturnedDate($validateInvoiceItem->getReturnedDate());
                }
                $createInvoiceItemVO->setQty($validateInvoiceItem->getQty());
                $createInvoiceVO->addItem($createInvoiceItemVO);
            }

            return $createInvoiceVO;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param UpdateInvoiceDateVO $invoiceDate
     * @param OrderVO $orderVO
     *
     * Update Date
     * @return bool
     */
    public function updateDate(UpdateInvoiceDateVO $invoiceDate, OrderVO $orderVO)
    {
        //find Invoice to update
        $invoice = $this->entityManager->getRepository('AppBundle:B2cNotafiscal')->findOneBy(array(
            'idPedped' => $invoiceDate->getOrderId(),
            'idNotafiscal' => $invoiceDate->getInvoiceId()
        ));
        //check invoice
        if (!$invoice) {
            throw new Exception('Invoice not found');
        }

        //start transaction
        $this->entityManager->getConnection()->beginTransaction();
        try {
            //check type of update
            switch ($invoiceDate->getType()) {
                case 'shippingDate':
                    //set date
                    $invoice->setDataEmbarque($invoiceDate->getDate());
                    break;
                case 'deliveryDate':
                    //set date
                    $invoice->setDataEntregaRealizada($invoiceDate->getDate());
                    break;
                default:
                    break;
            }

            //get Products
            $products = $this->entityManager->getRepository('AppBundle:B2cPedprd')->findBy(
                array(
                    'idPedped' => $invoiceDate->getOrderId(),
                    'idNotafiscal' => $invoiceDate->getInvoiceId()
                )
            );

            if (!$products) {
                throw new Exception('Product not found');
            }

            //create new UpdateProductsDateVO object
            $updateProductDate = new UpdateProductsDateVO();
            //set orderId
            $updateProductDate->setOrderId($invoiceDate->getOrderId());
            //set Clerk
            $updateProductDate->setClerk($invoiceDate->getClerk());
            //create UpdateProductsDateTypeVO
            $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
            $updateProductsDateTypeVO->setType($invoiceDate->getType());
            $updateProductsDateTypeVO->setDate($invoiceDate->getDate());
            $updateProductDate->addDateType($updateProductsDateTypeVO);
            //for each itens
            foreach ($products as $product) {
                //add product to update date
                $updateProductsDateProductVO = new UpdateProductsDateProductVO();
                $updateProductsDateProductVO->setId($product->getIdPrdprd());
                $updateProductDate->addProduct($updateProductsDateProductVO);
            }
            //update date
            $this->productService->updateDate($updateProductDate, $orderVO);

            //save
            $this->entityManager->persist($invoice);
            $this->entityManager->flush();
            //try and commit the transaction
            $this->entityManager->getConnection()->commit();
        } catch (Exception $e) {
            //rollback the failed transaction attempt
            $this->entityManager->getConnection()->rollback();
            throw $e;
        }

        //send event
        $this->event->send('InvoiceChangeDate', $invoiceDate);

        return true;
    }

    /**
     * @param CreateInvoiceVO $createInvoiceVO
     * @param OrderVO $orderVO
     *
     * Update Date
     * @return bool
     */
    public function createInvoice(CreateInvoiceVO $createInvoiceVO, OrderVO $orderVO)
    {
        //find Invoice to update
        $invoice = $this->entityManager->getRepository('AppBundle:B2cNotafiscal')->findOneBy(
            array(
                'idPedped' => $createInvoiceVO->getOrderId(),
                'idNotafiscal' => $createInvoiceVO->getInvoiceId(),
                'idFilial' => $createInvoiceVO->getBranchId()
            )
        );
        //create UpdateProductsDateVO
        $updateProductsDateVO = new UpdateProductsDateVO();

        //check invoice
        if (!$invoice) {
            //start transaction
            $this->entityManager->getConnection()->beginTransaction();
            try {
                $invoice = $this->entityManager
                    ->getRepository('AppBundle:B2cNotafiscal')->createInvoice($createInvoiceVO);

                $updateProductsDateVO->setOrderId($createInvoiceVO->getOrderId());
                $updateProductsDateVO->setClerk($createInvoiceVO->getClerk());
                //create UpdateProductsDateTypeVO
                $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
                $updateProductsDateTypeVO->setType('invoiceDate');
                $updateProductsDateTypeVO->setDate($createInvoiceVO->getCreatedDate());
                //add UpdateProductsDateTypeVO
                $updateProductsDateVO->addDateType($updateProductsDateTypeVO);
                /* @var $createInvoiceItem CreateInvoiceItemVO */
                foreach ($createInvoiceVO->getItens() as $createInvoiceItem) {
                    //get Product
                    $product = $this->entityManager->getRepository('AppBundle:B2cPedprd')->findOneBy(
                        array(
                            'idPedped' => $createInvoiceVO->getOrderId(),
                            'prdReferencia' => $createInvoiceItem->getProductReference()
                        )
                    );

                    if ($product) {
                        //set invoice id at order product
                        $product->setIdNotafiscal($createInvoiceVO->getInvoiceId());
                        //save
                        $this->entityManager->persist($product);
                        $this->entityManager->flush();
                        //add product to update date
                        $updateProductsDateProductVO = new UpdateProductsDateProductVO();
                        $updateProductsDateProductVO->setId($product->getIdPrdprd());
                        $updateProductsDateVO->addProduct($updateProductsDateProductVO);
                    }
                }
                //try and commit the transaction
                $this->entityManager->getConnection()->commit();
                //set updated order date
                $this->entityManager->getRepository('AppBundle:B2cPedped')
                    ->updateOrderUpdateDate($orderVO->getOrderId());
                //send event
                $this->event->send('InvoiceCreated', $invoice);
            } catch (Exception $e) {
                //rollback
                $this->entityManager->getConnection()->rollback();
                throw $e;
            }

            if (count($updateProductsDateVO->getProducts()) > 0) {
                //update products dates
                $this->productService->updateDate($updateProductsDateVO, $orderVO);
            }
        } else {
            throw new Exception('Duplicated Invoice');
        }

        return true;
    }

    /**
     * @param UpdateValidateInvoice $validateInvoice
     * @return UpdateInvoiceVO
     * Create an UpdateInvoiceVO
     */
    public function getCommonUpdateInvoiceInfo(UpdateValidateInvoice $validateInvoice)
    {
        try {
            $updateInvoiceVO = new UpdateInvoiceVO();
            $updateInvoiceVO->setOrderId($validateInvoice->getOrderId());
            $updateInvoiceVO->setInvoiceId($validateInvoice->getInvoiceId());
            $updateInvoiceVO->setMarketplaceIntegration($validateInvoice->isMarketplaceIntegration());
            $updateInvoiceVO->setKey($validateInvoice->getKey());
            $updateInvoiceVO->setDateKey($validateInvoice->getDateKey());
            $updateInvoiceVO->setPathXml($validateInvoice->getPathXml());
            $updateInvoiceVO->setPathPdf($validateInvoice->getPathPdf());
            $updateInvoiceVO->setShipperName($validateInvoice->getShipperName());
            $updateInvoiceVO->setShipperDoc($validateInvoice->getShipperDoc());
            $updateInvoiceVO->setHub($validateInvoice->getHub());

            $updateInvoiceVO->setReturned($validateInvoice->isReturned());
            if (!$validateInvoice->getReturnedDate() instanceof \Datetime) {
                $validateInvoice->setReturnedDate(new \DateTime('now'));
            }
            $updateInvoiceVO->setReturnedDate($validateInvoice->getReturnedDate());
            $updateInvoiceVO->setClerk($validateInvoice->getClerk());

            return $updateInvoiceVO;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param UpdateInvoiceVO $updateInvoiceVO
     * @param OrderVO $orderVO
     * @return bool
     *
     * Update Invoice
     */
    public function updateInvoice(UpdateInvoiceVO $updateInvoiceVO, OrderVO $orderVO)
    {
        //start transaction
        $this->entityManager->getConnection()->beginTransaction();
        try {
            //find Invoice to update
            /* @var $invoice B2cNotafiscal */
            $invoice = $this->entityManager->getRepository('AppBundle:B2cNotafiscal')->findOneBy(
                array(
                    'idPedped' => $updateInvoiceVO->getOrderId(),
                    'idNotafiscal' => $updateInvoiceVO->getInvoiceId()
                )
            );

            if (!$invoice) {
                throw new Exception('Invoice not found');
            }

            if ($updateInvoiceVO->isMarketplaceIntegration()) {
                $invoice->setIntegracaoMarketplace($updateInvoiceVO->isMarketplaceIntegration());
            }

            if ($updateInvoiceVO->getKey()) {
                $invoice->setChave($updateInvoiceVO->getKey());
            }

            if ($updateInvoiceVO->getDateKey() instanceof \DateTime) {
                $invoice->setDataChave($updateInvoiceVO->getDateKey());
            }

            if ($updateInvoiceVO->getPathXml()) {
                $invoice->setPathXml($updateInvoiceVO->getPathXml());
            }

            if ($updateInvoiceVO->getPathPdf()) {
                $invoice->setPathPdf($updateInvoiceVO->getPathPdf());
            }
            //when cancel
            $cancelled = false;

            if ($updateInvoiceVO->isReturned()) {
                $cancelled = true;
                $invoice->setDevolvido($updateInvoiceVO->isReturned());
                $invoice->setDataDevolucao($updateInvoiceVO->getReturnedDate());
            }

            $nfItems = $this->entityManager->getRepository('AppBundle:B2cNotafiscalItem')->findBy(
                array(
                    'idPedped' => $updateInvoiceVO->getOrderId(),
                    'idNotafiscal' => $updateInvoiceVO->getInvoiceId()
                )
            );

            /* @var $nfItem B2cNotafiscalItem */
            foreach ($nfItems as $nfItem) {
                if ($updateInvoiceVO->getShipperName() != "") {
                    $nfItem->setNomeTransportadora($updateInvoiceVO->getShipperName());
                }
                if ($updateInvoiceVO->getShipperDoc() != "") {
                    $nfItem->setDocumentoTransportadora($updateInvoiceVO->getShipperDoc());
                }
                if ($updateInvoiceVO->getHub() != "") {
                    $nfItem->setHub($updateInvoiceVO->getHub());
                }
                //save
                $this->entityManager->persist($nfItem);
            }

            //save
            $this->entityManager->persist($invoice);
            $this->entityManager->flush();
            //commit
            $this->entityManager->getConnection()->commit();

            //set updated order date
            $this->entityManager->getRepository('AppBundle:B2cPedped')->updateOrderUpdateDate($orderVO->getOrderId());

            //send event
            $this->event->send('InvoiceUpdated', $updateInvoiceVO);

            if ($cancelled) {
                //create UpdateProductsDateVO
                $updateProductsDateVO = new UpdateProductsDateVO();
                $updateProductsDateVO->setOrderId($updateInvoiceVO->getOrderId());
                $updateProductsDateVO->setClerk($updateInvoiceVO->getClerk());
                //create UpdateProductsDateTypeVO
                $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
                $updateProductsDateTypeVO->setType('invoiceDate');
                $updateProductsDateTypeVO->setDate(null);
                $updateProductsDateVO->addDateType($updateProductsDateTypeVO);

                $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
                $updateProductsDateTypeVO->setType('shippingDate');
                $updateProductsDateTypeVO->setDate(null);
                $updateProductsDateVO->addDateType($updateProductsDateTypeVO);

                $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
                $updateProductsDateTypeVO->setType('deliveryDate');
                $updateProductsDateTypeVO->setDate(null);
                $updateProductsDateVO->addDateType($updateProductsDateTypeVO);
                //find products
                $products = $this->entityManager->getRepository('AppBundle:B2cPedprd')->findBy(array('idPedped' => $updateInvoiceVO->getOrderId(), 'idNotafiscal' => $updateInvoiceVO->getInvoiceId()));
                //check product
                if ($products) {
                    //start transaction
                    $this->entityManager->getConnection()->beginTransaction();
                    /* @var $product B2cPedprd */
                    foreach ($products as $product) {
                        // reset id NF
                        $product->setIdNotafiscal(null);
                        //save
                        $this->entityManager->persist($product);
                        $this->entityManager->flush();

                        //add product to update date
                        $updateProductsDateProductVO = new UpdateProductsDateProductVO();
                        $updateProductsDateProductVO->setId($product->getIdPrdprd());
                        $updateProductsDateVO->addProduct($updateProductsDateProductVO);
                    }
                    $this->entityManager->getConnection()->commit();
                    //update date
                    $this->productService->updateDate($updateProductsDateVO, $orderVO);
                }
            }

            return true;
        } catch (\Exception $e) {
            if ($this->entityManager->getConnection()->getTransactionNestingLevel()) {
                // Rollback the failed transaction attempt
                $this->entityManager->getConnection()->rollback();
            }
            throw new Exception($e->getMessage());
        }
    }
}
