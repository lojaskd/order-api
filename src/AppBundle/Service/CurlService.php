<?php

namespace AppBundle\Service;

use Symfony\Bridge\Monolog\Logger;

/**
 * Class CurlService
 * @package AppBundle\Service
 */
class CurlService
{

    /**
     * @var Logger
     */
    private $logger;


    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $method
     * @param $url
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function execute($method, $url, $data = '')
    {
        try {
            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
            curl_setopt($curl, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

            if (in_array($method, array('POST', 'PUT'))) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }

            $response = curl_exec($curl);
            $curlErrno = curl_errno($curl);
            $curlError = curl_error($curl);

            curl_close($curl);

            if (empty($response)) {
                throw new \Exception("No response from server: " . $url, 500);
            }

            if ($curlErrno) {
                $this->logger->error($curlError);
                throw new \Exception($curlError);
            }
        } catch (\Exception $e) {
            $this->logger->error('Error calling external resource: ' . $e->getMessage());
            throw new \Exception($e->getMessage());
        }

        return json_decode($response, true);
    }
}
