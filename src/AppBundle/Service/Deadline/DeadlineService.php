<?php

namespace AppBundle\Service\Deadline;

use Symfony\Bridge\Monolog\Logger;

/**
 * Class DeadlineService
 * @package AppBundle\Service\Deadline
 */
class DeadlineService
{

    /**
     * @var IDeadline
     */
    private $deadline;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param IDeadline $deadline
     * @param Logger $logger
     */
    public function __construct(IDeadline $deadline, Logger $logger)
    {
        $this->deadline = $deadline;
        $this->logger = $logger;
    }

    /**
     * @param $productId
     * @param $dateRef
     * @param $zip
     * @param $qty
     * @param $immediateDelivery
     * @param $supplierReservedStock
     * @return array
     *
     * Calculate product dates
     */
    public function calculate($productId, $dateRef, $zip, $qty, $immediateDelivery = null, $supplierReservedStock = null)
    {
        return $this->deadline->calculate($productId, $dateRef, $zip, $qty, $immediateDelivery, $supplierReservedStock);
    }
}
