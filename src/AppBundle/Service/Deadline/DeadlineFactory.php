<?php

namespace AppBundle\Service\Deadline;

/**
 * Class DeadlineFactory
 * @package AppBundle\ServiceIDeadline
 * @codeCoverageIgnore
 */
class DeadlineFactory
{

    /**
     * @param IDeadline $instance
     * @return IDeadline
     */
    public static function getInstance(IDeadline $instance)
    {
        return $instance;
    }
}
