<?php

namespace AppBundle\Service\Deadline;

/**
 * Interface IDeadline
 * @package AppBundle\Service\Deadline
 * @codeCoverageIgnore
 */
interface IDeadline
{

    /**
     * @param $productId
     * @param $dateRef
     * @param $zip
     * @param $qty
     * @param $immediateDelivery
     * @param $supplierReservedStock
     * @return array
     *
     * Calculate product dates
     */
    public function calculate($productId, $dateRef, $zip, $qty, $immediateDelivery = null, $supplierReservedStock = null);
}
