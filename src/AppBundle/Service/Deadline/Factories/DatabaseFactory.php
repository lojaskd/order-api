<?php

namespace AppBundle\Service\Deadline\Factories;

use AppBundle\Service\Deadline\IDeadline;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class DatabaseFactory
 * @package AppBundle\Service\Deadline\Factories
 */
class DatabaseFactory implements IDeadline
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Logger
     */
    private $logger;


    /**
     * DatabaseFactory constructor.
     * @param EntityManager $entityManager
     * @param Logger $logger
     */
    public function __construct(EntityManager $entityManager, Logger $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param $productId
     * @param $dateRef
     * @param $zip
     * @param $qty
     * @param null $immediateDelivery
     * @param null $supplierReservedStock
     * @return array|bool
     *
     * Calculate product dates
     */
    public function calculate($productId, $dateRef, $zip, $qty, $immediateDelivery = null, $supplierReservedStock = null)
    {
        if ($productDays = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getProductDeadline($productId, $dateRef, $zip)) {
            // Check if is is immediate delivery
            if ($immediateDelivery) {
                // Release Date
                $releaseDate = $dateRef;
                // Supplier Billing Date
                $supplierBillingDate = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay($releaseDate, ($productDays['prazo_entrega_imediata'] - $productDays['prazo_nf']));
                // Supplier Invoice Date
                $supplierInvoiceDate = $supplierBillingDate;
                // Invoice Date
                $invoiceDate = $supplierBillingDate;
                // Supplier Delivery Date
                $supplierDeliveryDate = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay($releaseDate, ($productDays['prazo_entrega_imediata']));
                //calc provider days
                $providerDays = 0;
                //normal delivery
            }

            if (!$immediateDelivery) {
                // Release Date
                $releaseDate = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay($dateRef, ($productDays['prazo_compra']));
                // Supplier Billing Date
                $supplierBillingDate = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay($releaseDate, ($productDays['prazo_fornecedor'] + $productDays['prazo_agenda']));
                // Supplier Invoice Date
                $supplierInvoiceDate = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay($supplierBillingDate, ($productDays['prazo_coleta'] + $productDays['prazo_recebimento']));
                // Invoice Date
                $invoiceDate = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay($supplierInvoiceDate, ($productDays['prazo_nf']));
                // Supplier Delivery Date
                $supplierDeliveryDate = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay($invoiceDate, 1);
                //calc provider days
                $providerDays = $productDays['prazo_fornecedor'];
            }
            // Shipping Date
            $shippingDate = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay($supplierDeliveryDate, ($productDays['prazo_separacao'] + $productDays['prazo_romaneio']));
            // Delivery Date
            $deliveryDate = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay($shippingDate, ($productDays['prazo_transporte']));
            // Calc delivery days
            $deliveryDays = $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateDiffBusinessDay($dateRef, $deliveryDate);

            return array(
                'providerDeadLine' => $providerDays,
                'availabilityDeadline' => $deliveryDays,
                'predictDates' => array(
                    'releaseDate' => date_create_from_format('Y-m-d', $releaseDate),
                    'supplierBillingDate' => date_create_from_format('Y-m-d', $supplierBillingDate),
                    'supplierInvoiceDate' => date_create_from_format('Y-m-d', $supplierInvoiceDate),
                    'supplierDeliveryDate' => date_create_from_format('Y-m-d', $supplierDeliveryDate),
                    'invoiceDate' => date_create_from_format('Y-m-d', $invoiceDate),
                    'shippingDate' => date_create_from_format('Y-m-d', $shippingDate),
                    'deliveryDate' => date_create_from_format('Y-m-d', $deliveryDate),
                ),
                'deadlines' => $productDays
            );
        }

        return false;
    }
}
