<?php

namespace AppBundle\Service\Deadline\Factories;

use AppBundle\Service\Deadline\IDeadline;
use AppBundle\Service\ProductAPIService;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class ProductApiFactory
 * @package AppBundle\Service\Deadline\Factories
 */
class ProductApiFactory implements IDeadline
{

    /**
     * @var ProductAPIService
     */
    private $productApiService;

    /**
     * @var Logger
     */
    private $logger;


    /**
     * ProductApiFactory constructor.
     * @param ProductAPIService $productApiService
     * @param Logger $logger
     */
    public function __construct(ProductAPIService $productApiService, Logger $logger)
    {
        $this->productApiService = $productApiService;
        $this->logger = $logger;
    }

    /**
     * @param $productId
     * @param $dateRef
     * @param $zip
     * @param $qty
     * @param bool $immediateDelivery
     * @param bool $supplierReservedStock
     * @return array|bool
     *
     * Calculate product dates
     */
    public function calculate($productId, $dateRef, $zip, $qty, $immediateDelivery = false, $supplierReservedStock = false)
    {
        $this->logger->info('API_PEDIDOS - calculate product dates API-PRODUTOS');
        if ($dates = $this->productApiService->getDeadlines($productId, $dateRef, $zip, $qty, $immediateDelivery, $supplierReservedStock)) {
            $result = array();
            $result['predictDates'] = array(
                'releaseDate' => date_create_from_format('Y-m-d', $dates['dates']['release']),
                'supplierBillingDate' => date_create_from_format('Y-m-d', $dates['dates']['supplier_billing']),
                'supplierInvoiceDate' => date_create_from_format('Y-m-d', $dates['dates']['supplier_invoice']),
                'supplierDeliveryDate' => date_create_from_format('Y-m-d', $dates['dates']['supplier_delivery']),
                'invoiceDate' => date_create_from_format('Y-m-d', $dates['dates']['invoice']),
                'shippingDate' => date_create_from_format('Y-m-d', $dates['dates']['shipping']),
                'deliveryDate' => date_create_from_format('Y-m-d', $dates['dates']['delivery'])
            );

            $result['providerDeadLine'] = $dates['deadlines']['supplier'];
            $result['availabilityDeadline'] = $dates['deadlines']['total'];
            $result['deadlines'] = $dates['deadlines'];

            return $result;
        }

        return false;
    }
}
