<?php

namespace AppBundle\Service;

use AppBundle\Entity\B2cPedprdstatus;
use AppBundle\Entity\Validate\Update\ValidateProviderBillingDate;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateProductVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateTypeVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateVO;
use AppBundle\Entity\VO\Update\UpdateProviderBillingDateVO;
use AppBundle\Entity\VO\Update\UpdatePurchaseOrderVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;
use AppBundle\Entity\B2cPedprd;
use AppBundle\Entity\B2cPedped;

/**
 * Class PurchaseOrderService
 * @package AppBundle\Service
 */
class PurchaseOrderService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var QueueService
     */
    private $queueService;

    /**
     * @var EventService
     */
    private $event;

    /**
     * @param EntityManager $entityManager
     * @param Logger $logger
     * @param ProductService $productService
     * @param QueueService $queueService
     * @param EventService $event
     */
    public function __construct(EntityManager $entityManager, Logger $logger, ProductService $productService, QueueService $queueService, EventService $event)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->productService = $productService;
        $this->queueService = $queueService;
        $this->event = $event;
    }

    /**
     * @param ValidateProviderBillingDate $providerBillingDate
     * @return UpdateProviderBillingDateVO
     */
    public function getPurchaseOrderDateInfo(ValidateProviderBillingDate $providerBillingDate)
    {
        try {
            $providerBillingDateVO = new UpdateProviderBillingDateVO();

            $providerBillingDateVO->setOrderId($providerBillingDate->getOrderId());
            $providerBillingDateVO->setPurchaseOrder($providerBillingDate->getPurchaseOrder());
            $providerBillingDateVO->setType($providerBillingDate->getType());
            $providerBillingDateVO->setDate($providerBillingDate->getDate());
            $providerBillingDateVO->setClerk($providerBillingDate->getClerk());

            return $providerBillingDateVO;
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * @param UpdateProviderBillingDateVO $providerBillingDate
     * @param OrderVO $orderVO
     *
     * Update Date
     * @return bool
     * @throws \Exception
     */
    public function updatePurchaseOrderDate(UpdateProviderBillingDateVO $providerBillingDate, OrderVO $orderVO)
    {
        try {
            //get Products
            $products = $this->entityManager->getRepository('AppBundle:B2cPedprd')->findBy(
                array(
                    'idPedped' => $providerBillingDate->getOrderId(),
                    'ordemCompra' => $providerBillingDate->getPurchaseOrder()
                )
            );

            if (!$products) {
                throw new Exception('Product not found');
            }

            //create new UpdateProductsDateVO object
            $updateProductDate = new UpdateProductsDateVO();
            //set orderId
            $updateProductDate->setOrderId($providerBillingDate->getOrderId());
            //set Clerk
            $updateProductDate->setClerk($providerBillingDate->getClerk());
            //create UpdateProductsDateTypeVO
            $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
            $updateProductsDateTypeVO->setType($providerBillingDate->getType());
            $updateProductsDateTypeVO->setDate($providerBillingDate->getDate());
            $updateProductDate->addDateType($updateProductsDateTypeVO);
            //for each itens
            foreach ($products as $product) {
                /* @var $productStatus B2cPedprdstatus */
                $productStatus = $this->entityManager->getRepository('AppBundle:B2cPedprdstatus')->findOneBy(array(
                    'idPedped' => $orderVO->getOrderId(),
                    'idPrdprd' => $product->getIdPrdprd()
                ));

                if (empty($productStatus->getDataFatFornR())) {
                    //add product to update date
                    $updateProductsDateProductVO = new UpdateProductsDateProductVO();
                    $updateProductsDateProductVO->setId($product->getIdPrdprd());
                    $updateProductDate->addProduct($updateProductsDateProductVO);
                }
            }
            //update date
            $this->productService->updateDate($updateProductDate, $orderVO);

        } catch (Exception $e) {
            throw $e;
        }

        //send event
        $this->event->send('ProviderBillingDateChange', $providerBillingDate);

        return true;
    }

    /**
     * @param UpdatePurchaseOrderVO $purchaseOrderVO
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updatePurchaseOrder(UpdatePurchaseOrderVO $purchaseOrderVO)
    {
        //find products
        $products = $this->entityManager->getRepository('AppBundle:B2cPedprd')
            ->findBy([
                'ordemCompra' => $purchaseOrderVO->getPurchaseOrder(),
                'idPedped' => $purchaseOrderVO->getOrders()
            ]);

        //check product
        if (empty($products)) {
            throw new Exception('Product not found');
        }

        /* @var $product B2cPedprd */
        foreach ($products as $product) {
            if (!empty($purchaseOrderVO->getPathXml())) {
                $product->setPathXmlOc($purchaseOrderVO->getPathXml());
                
                //save
                $this->entityManager->persist($product);
                $this->entityManager->flush($product);
            }
        }
        // Update date
        $this->entityManager->getRepository('AppBundle:B2cPedped')
            ->updateOrderUpdateDate($products[0]->getIdPedped()->getIdPedped());

        return true;
    }
}
