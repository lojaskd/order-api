<?php

namespace AppBundle\Service;

use AppBundle\Entity\Validate\ValidateOrder;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class LojasKdService
 *
 * @package AppBundle\Service
 */
class LojasKdService
{
    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * @param EntityManager $entityManager
     * @param CouponService $couponService
     * @param ShippingService $shippingService
     * @param ProductService $productService
     * @param CustomerService $customerService
     * @param Logger $logger
     */
    public function __construct(
        EntityManager $entityManager,
        CouponService $couponService,
        ShippingService $shippingService,
        ProductService $productService,
        CustomerService $customerService,
        Logger $logger
    ) {
        $this->entityManager = $entityManager;
        $this->couponService = $couponService;
        $this->shippingService = $shippingService;
        $this->productService = $productService;
        $this->customerService = $customerService;
        $this->logger = $logger;
    }

    /**
     * @param CreateOrderVO $orderVO
     * @param ValidateOrder $order
     *
     * @return array
     *
     * Generate order data (shipping, coupon, values, ...) and return them on OrderVO object
     */
    public function getLojasKdData(CreateOrderVO $orderVO, ValidateOrder $order)
    {
        $errors = array();

        $this->logger->info('API_PEDIDOS - Getting customer.');
        // Get customer info
        $customer = $this->entityManager->getRepository('AppBundle:B2cClicli')
            ->getCustomerById($order->getCustomer()->getId());

        try {
            // Create the customer if it does not exist
            $customerVO = $this->customerService
                ->getOrCreateCustomer($customer, $order->getCustomer(), $order->getDeliveryAddress());
            $orderVO->setCustomer($customerVO);

            $this->logger->info('API_PEDIDOS - Customer found/created ('.$customerVO->getId().').');
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }

        // If there're no errors creating customer, get shipping and coupon info
        if (empty($errors)) {
            // Status internal
            $orderVO->setStatusInternal(0);

            // Calculate shipping values to order
            $orderVO->setShipping($this->shippingService->calculateShipping($orderVO));

            // If a coupon is used
            if ($order->getCoupon() && !empty($order->getCoupon()->getCode())) {
                $this->logger->info('API-PEDIDOS - Coupon code used.');

                // Validate the coupon
                $orderVO->setCoupon($this->couponService->validateCoupon($order->getCoupon()->getCode(), $orderVO));

                // If the coupon is applied on shipping, save the values on shipping data
                if ($orderVO->getCoupon()->isFreeShipping() || $orderVO->getCoupon()->isFixedShipping()) {
                    $this->logger->info('API_PEDIDOS - Coupon used on shipping.');
                    $orderVO->setShipping($this->shippingService->calculateCouponShipping($orderVO));
                }
            }

            // Compare the order values from request to the values from database
            $validateOrdervalues = $this->validateOrderValues($orderVO, $order);

            $errors = $validateOrdervalues['errors'];

            $this->logger->info('API-PEDIDOS - Total value ('.$validateOrdervalues['total'].') and discount ('.
                $validateOrdervalues['discount'].') calculated.');

            /* @var $orderVO CreateOrderVO */
            $orderVO = $validateOrdervalues['orderVO'];
            $orderVO->setTotal($validateOrdervalues['total'] >= 0 ? $validateOrdervalues['total'] : 0);
            $orderVO->setDiscount($validateOrdervalues['discount']);
        }

        return array('errors' => $errors, 'orderVO' => $orderVO);
    }

    /**
     * @param CreateOrderVO $orderVO
     * @param ValidateOrder $inputOrder
     *
     * @return array
     *
     * Compare the order values from request to the values generated from database
     */
    public function validateOrderValues(CreateOrderVO $orderVO, ValidateOrder $inputOrder)
    {
        $errors = array();
        $promotionDiscount = 0;
        $this->logger->info('API_PEDIDOS - Validating order values...');

        // Check if there is stock to the products
        $validStock = $this->productService->validateProductStock($orderVO->getProducts());
        if (!$validStock) {
            $errors[] = 'There is no available stock to all products.';
        }

        // Compare the shipping price from database with shipping sent in request
        $inputOrderShippingPrice = number_format($inputOrder->getShipping()->getPrice(), 2, '.', '');
        $shippingPrice = number_format($orderVO->getShipping()->getPrice(), 2, '.', '');
        if ($shippingPrice != $inputOrderShippingPrice) {
            $errors[] = 'Shipping price (R$ '.$inputOrderShippingPrice.') is not the real value (R$ '.
                $shippingPrice.').';
        }

        // Compare the subtotal from database with subtotal sent in request
        $inputOrderSubtotal = number_format($inputOrder->getSubtotal(), 2, '.', '');
        $subtotal = number_format($orderVO->getSubtotal(), 2, '.', '');
        if (round(floatval($inputOrderSubtotal) + 0.5) != round(floatval($subtotal) + 0.5)) {
            $errors[] = 'Subtotal value (R$ '.$inputOrderSubtotal.') does not match R$ '.$subtotal.'.';
        }

        // If there is just one payment method
        if (count($orderVO->getPaymentsId()) == 1) {
            $this->logger->info('API_PEDIDOS - There is one payment method.');

            $promotionDiscount = 0;
            if ($orderVO->isPromotionPayment()) {
                array_map(function ($product) use (&$promotionDiscount) {
                    if($product->getPromotionPayment()) {
                        $promotionDiscount += $product->getPromotionPayment()->getDiscountProduct();
                    }
                }, $orderVO->getProducts());
            } else {
                // Search for active promotion discount on store for the payment type
                $promotion = $this->entityManager->getRepository('AppBundle:B2cPromo')->getActiveDiscountPromotion(
                    $orderVO->getFirstPayment()->getId(),
                    $orderVO->getFirstPayment()->getPaymentFormId()
                );
                // Get the value of store promotion discount
                if ($promotion && !$inputOrder->getInstallmentSapCoupon()) {
                    $this->logger->info('API_PEDIDOS - There is a promotion on store: '.serialize($promotion));
                    if ($promotion['descontoValor'] > 0) {
                        $promotionDiscount = number_format($promotion['descontoValor'], 2, '.', '');
                    } elseif ($promotion['descontoPerc'] > 0) {
                        $promotionDiscount = number_format(
                            ($orderVO->getSubtotal() * $promotion['descontoPerc'] / 100),
                            2,
                            '.',
                            ''
                        );
                    }
                }
            }

            // Compare the coupon value from database with the value sent in request
            $couponDiscount = ($orderVO->getCoupon() ?
                number_format($orderVO->getCoupon()->getDiscountValue(), 2, '.', '') : 0
            );
            $inputOrderCouponDiscount = number_format($inputOrder->getCoupon()->getValue(), 2, '.', '');
            if ($couponDiscount != $inputOrderCouponDiscount) {
                $errors[] = 'Coupon discount value (R$ '.$inputOrderCouponDiscount.') is not the real value (R$ '.
                    $couponDiscount.').';
            }

            $totalFormat =
                $orderVO->getSubtotal() - $promotionDiscount - $couponDiscount + $orderVO->getShipping()->getPrice();
            // Generate the order total price
            $total = number_format($totalFormat, 2, '.', '');

            // Calculate payment with interest
            if ($orderVO->getFirstPayment()->getIndexer() > 0) {
                // TODO Verificar se é necessário recalcular juros, se não recebemos o valor já pronto do site
                //$instalmentValue = $this->generateInterestPayment($total,
                // $orderVO->getFirstPayment()->getInstalment(), $orderVO->getFirstPayment()->getIndexer());

                //$orderVO->getFirstPayment()->setInstalmentValue($instalmentValue);
                $paymentMethodText = (in_array($orderVO->getFirstPayment()->getInstalment(), array(
                        0,
                        1,
                        null,
                    )) ? ' à vista' : ' em ').$orderVO->getFirstPayment()->getInstalment().'x de R$ ';
                $orderVO->getFirstPayment()
                    ->setPaymentFormDesc(
                        $paymentMethodText.
                        number_format($orderVO->getFirstPayment()->getInstalmentValue(), 2, ',', '.').
                        ' com juros'
                    );

                $total =
                    $orderVO->getFirstPayment()->getInstalmentValue() * $orderVO->getFirstPayment()->getInstalment();
            }
        } else {
            $this->logger->info('API_PEDIDOS - There is two payment methods.');

            // Calculate the total payment type amount
            $firstPaymentTotalValue =
                ($orderVO->getFirstPayment()->getInstalmentValue() * $orderVO->getFirstPayment()->getInstalment());
            $secondPaymentTotalValue =
                ($orderVO->getSecondPayment()->getInstalmentValue() * $orderVO->getSecondPayment()->getInstalment());

            // Generate the order total price
            $total = $firstPaymentTotalValue + $secondPaymentTotalValue;

            // Compare the total price from database with the amount sent in request
            $inputOrderTotal = number_format($inputOrder->getTotal(), 2, '.', '');
            if (round(floatval($total) + 0.5) != round(floatval($inputOrderTotal) + 0.5)) {
                $errors[] = 'Total amount (R$ '.$inputOrderTotal.') must be R$ '.$total.'.';
            }
        }

        return array('errors' => $errors, 'total' => $total, 'discount' => $promotionDiscount, 'orderVO' => $orderVO);
    }

    /**
     * @param $total
     * @param $instalment
     * @param $indexer
     *
     * @return float
     *
     * Generate interest to payment
     */
    public function generateInterestPayment($total, $instalment, $indexer)
    {
        $instalmentValue = $total;

        $indexer = $indexer / 100;
        if ($total > 0 && $instalment >= 1 && $indexer > 0) {
            $expo = pow((1 + $indexer), $instalment);
            $instalmentValue = round(100 * $total * (($indexer * $expo) / ($expo - 1))) / 100;
        }

        return $instalmentValue;
    }
}
