<?php
namespace AppBundle\Service\Event;

/**
 * Class EventFactory
 * @package AppBundle\Service\Event
 * @codeCoverageIgnore
 */
class EventFactory
{

    /**
     * @param IEvent $instance
     * @return IEvent
     */
    public static function getInstance(IEvent $instance)
    {
        return $instance;
    }
}
