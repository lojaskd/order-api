<?php
namespace AppBundle\Service\Event;

/**
 * Interface IEvent
 * @package AppBundle\Service\Event
 * @codeCoverageIgnore
 */
interface IEvent
{

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function send($key, $value);
}
