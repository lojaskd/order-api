<?php

namespace AppBundle\Service\Event\Factories;

use AppBundle\Service\Event\IEvent;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class EventRabbitFactory
 * @package AppBundle\Service\Event\Factories
 */
class EventRabbitFactory implements IEvent
{

    /**
     * @var
     */
    private $host;
    /**
     * @var
     */
    private $port;
    /**
     * @var
     */
    private $user;
    /**
     * @var
     */
    private $password;
    /**
     * @var
     */
    private $vHost;
    /**
     * @var
     */
    private $exchange;
    /**
     * @var
     */
    private $connection;
    /**
     * @var
     */
    private $channel;
    /**
     * @var bool
     */
    private $isConnected = false;


    /**
     * @param $host
     * @param $port
     * @param $user
     * @param $password
     * @param $vHost
     * @param $exchange
     */
    public function __construct($host, $port, $user, $password, $vHost, $exchange)
    {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
        $this->vHost = $vHost;
        $this->exchange = $exchange;
    }

    /**
     * @param $name
     * @param $data
     * @return bool
     *
     * Send message
     */
    public function send($name, $data)
    {
        try {
            if ($this->connect()) {
                $message = new AMQPMessage($data, array('content_type' => 'text/plain', 'delivery_mode' => 2));
                $this->channel->basic_publish($message, $this->exchange, $name);
                $this->disconnect();

                return true;
            }
        } catch (\Exception $e) {
            throw new Exception('Error sending message to reduce stock queue: ' . $e->getMessage());
        }

        return false;
    }

    /**
     * @return bool
     *
     * Connect to queue
     */
    private function connect()
    {
        try {
            if (!$this->isConnected) {
                $this->connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password, $this->vHost);
                $this->channel = $this->connection->channel();

                if (!$this->createExchange()) {
                    return false;
                }
            }
            $this->isConnected = true;
        } catch (\Exception $e) {
            throw new Exception('Error connecting to queue manager: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * @return bool
     *
     * Disconnect from queue
     */
    private function disconnect()
    {
        try {
            $this->channel->close();
            $this->connection->close();
            $this->isConnected = false;
        } catch (\Exception $e) {
            throw new Exception('Error disconnecting from queue: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * @return bool
     *
     * Create exchange
     */
    private function createExchange()
    {
        try {
            $this->channel->exchange_declare($this->exchange, 'direct', false, true, false);
        } catch (Exception $e) {
            throw new Exception('Error connecting to queue manager: ' . $e->getMessage());
        }

        return true;
    }
}
