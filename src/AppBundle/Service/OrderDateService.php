<?php

namespace AppBundle\Service;

use AppBundle\Entity\B2cPedstatus;
use AppBundle\Entity\B2cPedprdstatus;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateProductVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateTypeVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateVO;
use AppBundle\Service\Deadline\DeadlineService;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class OrderDateService
 * @package AppBundle\Service
 */
class OrderDateService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var DeadlineService
     */
    private $deadlineService;


    /**
     * OrderDateService constructor.
     * @param EntityManager $entityManager
     * @param Logger $logger
     * @param ProductService $productService
     * @param EventService $eventService
     * @param DeadlineService $deadlineService
     */
    public function __construct(EntityManager $entityManager, Logger $logger, ProductService $productService, EventService $eventService, DeadlineService $deadlineService)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->productService = $productService;
        $this->eventService = $eventService;
        $this->deadlineService = $deadlineService;
    }

    /**
     * @param $productId
     * @param $dateRef
     * @param $zip
     * @param $qty
     * @param $immediateDelivery
     * @param $supplierReservedStock
     * @return array
     *
     * Calculate product dates
     */
    public function calculateProductDates($productId, $dateRef, $zip, $qty, $immediateDelivery = null, $supplierReservedStock = null)
    {
        $this->logger->info('API_PEDIDOS - Getting product dates...');

        return  $this->deadlineService->calculate($productId, $dateRef, $zip, $qty, $immediateDelivery, $supplierReservedStock);
    }

    /**
     * @param OrderVO $orderVO
     * @param $dateRef
     * @return boolean
     *
     * Calculate order delivery date
     */
    public function updateOrderDeadlines(OrderVO $orderVO, $dateRef)
    {
        $this->logger->info('API_PEDIDOS - Updating Order delivery date...');

        $availabilityDaysArray = array();
        $deliveryDateArray = array();
        $productsPredictDates = array(
            'releaseDate' => array(),
            'supplierBillingDate' => array(),
            'supplierInvoiceDate' => array(),
            'supplierDeliveryDate' => array(),
            'invoiceDate' => array(),
            'shippingDate' => array(),
            'deliveryDate' => array()
        );
        $productsDeadlines = array();
        // For each product, change its deadline
        /* @var $product OrderProductVO */
        foreach ($orderVO->getProducts() as $product) {
            //calc product dates
            $productDates = $this->calculateProductDates(
                $product->getProductId(),
                $dateRef,
                $orderVO->getDeliveryAddress()->getZip(),
                $product->getQuantity(),
                $product->isImmediateDelivery(),
                $product->isReservedStockDelivery()
            );
            //set products dates
            $productsDeadlines[$product->getProductId()] = $productDates['deadlines'];
            //update b2cPedPrd deadlines
            $this->entityManager->getRepository('AppBundle:B2cPedprd')->setNewDeadline(
                $orderVO->getOrderId(),
                $product->getProductId(),
                $productDates['providerDeadLine'],
                $productDates['availabilityDeadline'],
                $productDates['predictDates']['supplierDeliveryDate']->format('Y-m-d')
            );

            //create UpdateProductsDateVO
            $updateProductsDateVO = new UpdateProductsDateVO();
            $updateProductsDateVO->setOrderId($orderVO->getOrderId());
            $updateProductsDateVO->setPredict(true);
            //create UpdateProductsDateProductVO
            $updateProductsDateProductVO = new UpdateProductsDateProductVO();
            $updateProductsDateProductVO->setId($product->getProductId());
            //add UpdateProductsDateProductVO
            $updateProductsDateVO->addProduct($updateProductsDateProductVO);

            //set product predict dates
            foreach ($productDates['predictDates'] as $productDateType => $productDate) {
                //create UpdateProductsDateTypeVO
                $updateProductsDateTypeVO = new UpdateProductsDateTypeVO();
                $updateProductsDateTypeVO->setType($productDateType);
                $updateProductsDateTypeVO->setDate($productDate);
                //add UpdateProductsDateTypeVO
                $updateProductsDateVO->addDateType($updateProductsDateTypeVO);

                $productsPredictDates[$productDateType][$product->getProductId()] = $productDate;
            }

            //update product predict date
            $this->productService->updateDate($updateProductsDateVO);

            $availabilityDaysArray[$product->getProductId()] = $productDates['availabilityDeadline'];
            $deliveryDateArray[$product->getProductId()] = $productDates['predictDates']['deliveryDate'];
        }

        // Get max availability days and delivery date to save on order
        $orderAvailabilityDays = max($availabilityDaysArray);
        $orderDeliveryDate = max($deliveryDateArray);

        // Set order deadline
        $this->entityManager->getRepository('AppBundle:B2cPedped')
            ->updateOrderDeadline($orderVO->getOrderId(), $orderAvailabilityDays, $orderDeliveryDate->format('Y-m-d'));

        // PREDICT DATES
        //for each predict date type
        foreach ($productsPredictDates as $dateType => $productPredictDates) {
            //get max date
            $maxDate = max($productPredictDates);

            switch ($dateType) {
                case 'releaseDate':
                    // PREDICT SUPPLIER BILLING DATE
                    $this->updateOrderStatusDate($orderVO->getOrderId(), B2cPedstatus::RELEASED, 0, $maxDate, true);
                    break;

                case 'supplierBillingDate':
                    // PREDICT SUPPLIER BILLING DATE
                    $this->updateOrderStatusDate($orderVO->getOrderId(), B2cPedstatus::RELEASED, 159, $maxDate, true);
                    break;

                case 'supplierInvoiceDate':
                    // PREDICT DIGITAL INPUT
                    $this->updateOrderStatusDate($orderVO->getOrderId(), B2cPedstatus::RELEASED, 86, $maxDate, true);
                    break;

                case 'supplierDeliveryDate':
                    // PREDICT PHYSICAL INPUT
                    $this->updateOrderStatusDate($orderVO->getOrderId(), B2cPedstatus::RELEASED, 158, $maxDate, true);
                    break;

                case 'invoiceDate':
                    // PREDICT INVOICE
                    $this->updateOrderStatusDate($orderVO->getOrderId(), B2cPedstatus::RELEASED, 72, $maxDate, true);
                    break;

                case 'shippingDate':
                    // PREDICT SHIPPED DATE
                    $this->updateOrderStatusDate($orderVO->getOrderId(), B2cPedstatus::SHIPPED, 0, $maxDate, true);
                    break;

                case 'deliveryDate':
                    // PREDICT SHIPPED DATE
                    $this->updateOrderStatusDate($orderVO->getOrderId(), B2cPedstatus::DELIVERED, 0, $maxDate, true);
                    break;

                default:
                    break;
            }
        }

        $this->entityManager->getRepository('AppBundle:B2cPedlog')->insertOrderDeadlineLog($orderVO, $orderDeliveryDate, $productsDeadlines);

        return true;
    }

    /**
     * @param CreateOrderVO $createOrderVO
     * @return boolean
     *
     * Set Order and product status predict deadline dates
     */
    public function createOrderDeadline(CreateOrderVO $createOrderVO)
    {
        $this->logger->info('API_PEDIDOS - Creating Order deadline date...');

        //get next business day
        $approvalDate = date_create_from_format('Y-m-d', $this->entityManager->getRepository('AppBundle:B2cViewProdutos')->getDateAddNextBusinessDay(date('Y-m-d'), 1));
        //get delivery date from Order
        $deliveryDate = $createOrderVO->getMaxDeliveryDate();

        // For each product, change its deadline
        /* @var $product CreateProductVO */
        foreach ($createOrderVO->getProducts() as $product) {
            // get pedprd status (WILL BE REMOVED WHEN THE TRIGGERS DOWN)
            $pedPrdStatus = $this->entityManager->getRepository('AppBundle:B2cPedprdstatus')->findOneBy(array('idPedped' => $createOrderVO->getOrderId(), 'idPrdprd' => $product->getId()));
            if (!$pedPrdStatus) {
                //create pedprdstatus
                $pedPrdStatus = new B2cPedprdstatus();
                $pedPrdStatus->setIdPedped($createOrderVO->getOrderId());
                $pedPrdStatus->setIdPrdprd($product->getId());
                $pedPrdStatus->setPrdReferencia($product->getReference());
                $pedPrdStatus->setDataEmbRNotificado('');
                $pedPrdStatus->setDataEntRNotificado('');
                $this->entityManager->persist($pedPrdStatus);
                $this->entityManager->flush();
            }
            //create UpdateProductsDateVO
            $updateProductsDateVO = new UpdateProductsDateVO();
            $updateProductsDateVO->setOrderId($createOrderVO->getOrderId());
            $updateProductsDateVO->setPredict(true);
            //create approvalDate UpdateProductsDateTypeVO
            $approvalDateTypeVO = new UpdateProductsDateTypeVO();
            $approvalDateTypeVO->setType('approvalDate');
            $approvalDateTypeVO->setDate($approvalDate);
            //add UpdateProductsDateTypeVO
            $updateProductsDateVO->addDateType($approvalDateTypeVO);

            //create deliveryDate UpdateProductsDateTypeVO
            $deliveryDateTypeVO = new UpdateProductsDateTypeVO();
            $deliveryDateTypeVO->setType('deliveryDate');
            $deliveryDateTypeVO->setDate($product->getDeliveryDate());
            //add UpdateProductsDateTypeVO
            $updateProductsDateVO->addDateType($deliveryDateTypeVO);

            //create UpdateProductsDateProductVO
            $updateProductsDateProductVO = new UpdateProductsDateProductVO();
            $updateProductsDateProductVO->setId($product->getId());
            //add UpdateProductsDateProductVO
            $updateProductsDateVO->addProduct($updateProductsDateProductVO);

            //update product predict date
            $this->productService->updateDate($updateProductsDateVO);
        }
        // Set Order predict approval date
        $this->updateOrderStatusDate($createOrderVO->getOrderId(), B2cPedstatus::APPROVED, 0, $approvalDate, true);
        // Set Order predict delivery date
        $this->updateOrderStatusDate($createOrderVO->getOrderId(), B2cPedstatus::DELIVERED, 0, $deliveryDate, true);

        return true;
    }

    /**
     * @param $orderId
     * @param $status
     * @param $internalStatus
     * @param $date
     * @param bool $predict
     * @return bool
     *
     * Update Order Status Date
     */
    private function updateOrderStatusDate($orderId, $status, $internalStatus, $date, $predict = false)
    {
        $updateOrderStatusVO = new UpdateOrderStatusVO();
        $updateOrderStatusVO->setOrderId($orderId);
        $updateOrderStatusVO->setStatus($status);
        $updateOrderStatusVO->setInternalStatus($internalStatus);
        $updateOrderStatusVO->setDate($date);
        $this->entityManager->getRepository('AppBundle:B2cPedstatus')->updateDate($updateOrderStatusVO, $predict);

        return true;
    }
}
