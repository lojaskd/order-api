<?php
namespace AppBundle\Service;

use AppBundle\Entity\B2cClicli;
use AppBundle\Entity\Validate\ValidateCustomer;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class CustomerService
 * @package AppBundle\Service
 */
class CustomerService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Logger
     */
    private $logger;


    /**
     * @param EntityManager $entityManager
     * @param Logger $logger
     */
    public function __construct(EntityManager $entityManager, Logger $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param $customer
     * @param ValidateCustomer $orderCustomer
     * @param $address
     * @return CreateCustomerVO
     *
     * Get the customer from database or create it if it does not exist
     */
    public function getOrCreateCustomer($customer, ValidateCustomer $orderCustomer, $address)
    {
        $customerVO = new CreateCustomerVO();

        // If no customer was passed, create it
        if (!$customer) {
            $this->logger->info('API_PEDIDOS - Customer does not exist, we will create it.');
            try {
                $customer = $this->entityManager->getRepository('AppBundle:B2cClicli')->addCustomer($orderCustomer, $address);
            } catch (\Exception $e) {
                throw new Exception('Error creating customer: ' . $e->getMessage());
            }
        }

        /* @var $customer B2cClicli*/
        $customerVO->setId($customer->getIdClicli());
        $customerVO->setFirstName($customer->getNome());
        $customerVO->setLastName($customer->getSobrenome());
        $customerVO->setEmail($customer->getEmail());
        $customerVO->setCpf($customer->getCpfCgc());
        $customerVO->setRg($customer->getRgIe());
        $customerVO->setPersonType($customer->getTipoPessoa());
        $customerVO->setGenre($customer->getSexo());
        $customerVO->setBirthday($customer->getDtaNasc());
        $customerVO->setContact($customer->getContato());

        $ip = ($orderCustomer->getIp() ? $orderCustomer->getIp() : (isset($_SERVER["HTTP_X_FORWARDED_FOR"]) && $_SERVER["HTTP_X_FORWARDED_FOR"] != "" ? $_SERVER["HTTP_X_FORWARDED_FOR"] : isset($_SERVER["REMOTE_ADDR"])) ? $_SERVER["REMOTE_ADDR"] : '127.0.0.1');
        $customerVO->setIp($ip);

        return $customerVO;
    }
}
