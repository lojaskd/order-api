<?php

namespace AppBundle\Service;

use AppBundle\Entity\VO\Create\CreateOrderLogVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class OrderLogService
 * @package AppBundle\Service
 */
class OrderLogService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @param EntityManager $entityManager
     * @param Logger $logger
     * @param EventService $eventService
     */
    public function __construct(EntityManager $entityManager, Logger $logger, EventService $eventService)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->eventService = $eventService;
        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * @param CreateOrderLogVO $orderLogVO
     * @return bool
     *
     * Insert log on b2c_pedlog
     */
    public function insertOrderLog(CreateOrderLogVO $orderLogVO)
    {
        try {
            $this->entityManager->getRepository('AppBundle:B2cPedlog')->insertOrderLog($orderLogVO);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return true;
    }

    /**
     * @param CreateOrderVO $createOrderVO
     * @return boolean
     *
     * Insert create order log
     */
    public function insertCreateOrderLog(CreateOrderVO $createOrderVO)
    {
        $orderArr = json_decode($this->serializer->serialize($createOrderVO, 'json'), true);
        $ip = isset($_SERVER["HTTP_X_FORWARDED_FOR"]) && $_SERVER["HTTP_X_FORWARDED_FOR"] != "" ? $_SERVER["HTTP_X_FORWARDED_FOR"] : isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '127.0.0.1';

        $orderLogVO = new CreateOrderLogVO();
        $orderLogVO->setOrderId($createOrderVO->getOrderId());
        $orderLogVO->setCreatedAt(new \DateTime('now'));
        $orderLogVO->setTitle('Novo Pedido IP: ' . $ip);
        $orderLogVO->setText($this->formatOrderObject($orderArr));
        $orderLogVO->setShow(0);
        $orderLogVO->setStatus(0);
        $orderLogVO->setInternalStatus(0);

        return $this->insertOrderLog($orderLogVO);
    }

    /**
     * @param $data
     * @return string
     */
    private function formatOrderObject($data)
    {
        $row = '';
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $row .= '-----' . $key . '-----' . PHP_EOL;
                $row .= $this->formatOrderObject($value) . PHP_EOL;
            }

            if (!is_array($value)) {
                $row .= $key . ': ' . $value . PHP_EOL;
            }
        }

        return $row;
    }
}
