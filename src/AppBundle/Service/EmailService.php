<?php
namespace AppBundle\Service;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

/**
 * Class EmailService
 * @package AppBundle\Service
 */
class EmailService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EngineInterface
     */
    private $templating;


    /**
     * @param EntityManager $entityManager
     * @param EngineInterface $templating
     * @param QueueService $queueService
     * @param $mailerHost
     * @param $mailerPort
     * @param $mailerUser
     * @param $mailerPassword
     */
    public function __construct(EntityManager $entityManager, EngineInterface $templating, QueueService $queueService, $mailerHost, $mailerPort, $mailerUser, $mailerPassword)
    {
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->queueService = $queueService;
        $this->mailerHost = $mailerHost;
        $this->mailerPort = $mailerPort;
        $this->mailerUser = $mailerUser;
        $this->mailerPassword = $mailerPassword;
    }

    /**
     * @return Swift_SmtpTransport
     *
     * Get the email transport
     */
    private function getTransport()
    {
        $transport = new Swift_SmtpTransport();

        $transport->setHost($this->mailerHost);
        $transport->setPort($this->mailerPort);
        $transport->setUsername($this->mailerUser);
        $transport->setPassword($this->mailerPassword);

        return $transport;
    }

    /**
     * @return Swift_Mailer
     *
     * Get the mailer instance
     */
    private function getMailer()
    {
        return new Swift_Mailer($this->getTransport());
    }

    /**
     * @return Swift_Message
     *
     * Get the email message
     */
    private function createMessage()
    {
        return new Swift_Message();
    }

    /**
     * @param $subject
     * @param $mailFrom
     * @param $mailTo
     * @param $mailToName
     * @param $template
     * @param $messageData
     * @return bool
     *
     * Sends the email
     */
    public function send($subject, $mailFrom, $mailTo, $mailToName, $template, $messageData)
    {
        // Render an template with parameter data
        $body = $this->templating->render($template, $messageData);

        // Create the email message
        $swiftMessage = $this->createMessage();

        $swiftMessage->setSubject($subject);
        $swiftMessage->setFrom($mailFrom, 'Seu Pedido LojasKD');
        $swiftMessage->setTo($mailTo, $mailToName);
        $swiftMessage->setBody($body);
        $swiftMessage->addPart($body, 'text/html');

        // Sends the email
        $this->getMailer()->send($swiftMessage, $failedRecipients);

        if (!empty($failedRecipients)) {
            throw new Exception('Error sending customer order email: ' . $failedRecipients[0]);
        }

        return true;
    }

    /**
     * @param B2cPedped $order
     * @param CreateCustomerVO $customer
     *
     * Send the email order info
     * @return bool
     */
    public function sendOrderEmail(B2cPedped $order, CreateCustomerVO $customer)
    {
        $result = false;
        try {
            // Check if customer accepts marketing emails
            $acceptNewsletter = $this->entityManager->getRepository('AppBundle:B2cClicli')->customerAcceptsNewsletter($order->getIdClicli());

            // Create data to send to queue
            $emailData = serialize(array(
                'id_pedped' => $order->getIdPedped(),
                'id_clicli' => $order->getIdClicli(),
                'cliente_nome' => $order->getEntNome(),
                'newsletterCli' => $acceptNewsletter
            ));

            // Send the email data to queue, if it fails send the email manually
            if (!$this->queueService->sendOrderEmail($emailData)) {
                $this->sendOrderEmailManually($order, $customer);
            }
            $result = true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $result;
    }

    /**
     * @param B2cPedped $order
     * @param CreateCustomerVO $customer
     *
     * Send the order email manually
     */
    public function sendOrderEmailManually(B2cPedped $order, CreateCustomerVO $customer)
    {
        $subject = 'Pedido #' . $order->getIdPedped() . ' realizado com sucesso';

        switch ($order->getPedTipoPagamento1()) {
            case 'Boleto Bancário':
                $paymentBottom1 = 'boleto';
                break;
            case 'Depósito Bancário':
                $paymentBottom1 = 'deposito';
                break;
            case 'PagSeguro':
                $paymentBottom1 = 'pagseguro';
                break;
            default:
                $paymentBottom1 = 'padrao';
        }

        switch ($order->getPedTipoPagamento2()) {
            case 'Boleto Bancário':
                $paymentBottom2 = 'boleto';
                break;
            case 'Depósito Bancário':
                $paymentBottom2 = 'deposito';
                break;
            case 'PagSeguro':
                $paymentBottom2 = 'pagseguro';
                break;
            default:
                $paymentBottom2 = 'padrao';
        }

        $emailData = array(
            'customerFirstName' => $customer->getFirstName(),
            'customerLastName' => $customer->getLastName(),
            'customerEmail' => $customer->getEmail(),
            'customerCpf' => $customer->getCpf(),
            'orderId' => $order->getIdPedped(),
            'orderDate' => $order->getPedDtaCompra()->format('d/m/Y'),
            'paymentMethod1' => $order->getPedTipoPagamento1(),
            'paymentForm1' => $order->getPedFormaPagamento1(),
            'paymentDescription1' => $order->getPedFormaPagdesc1(),
            'paymentInfo1' => '',
            'onlinePaymentBottom1' => $paymentBottom1,
            'paymentMethod2' => $order->getPedTipoPagamento2(),
            'paymentForm2' => $order->getPedFormaPagamento2(),
            'paymentDescription2' => $order->getPedFormaPagdesc2(),
            'paymentInfo2' => '',
            'onlinePaymentBottom2' => $paymentBottom2,
            'orderDeliveryDeadline' => $order->getPedPrazoEntrega(),
            'orderDeliveryCity' => $order->getEntEndCidade(),
            'subtotal' => $order->getPedValorSubtotal(),
            'shipping' => $order->getPedValorFrete(),
            'total' => $order->getPedValorTotal(),
            'deliveryAddress' => $order->getEntEndereco(),
            'deliveryAddressNumber' => $order->getEntEndNumero(),
            'deliveryAddressComplement' => $order->getEntEndComp(),
            'deliveryAddressNeighborhood' => $order->getEntEndBairro(),
            'deliveryAddressCity' => $order->getEntEndCidade(),
            'deliveryAddressState' => $order->getEntEndUf(),
            'deliveryAddressZip' => $order->getEntEndCep(),
            'deliveryAddressTelephoneA' => $order->getEntTelA(),
            'deliveryAddressTelephoneB' => $order->getEntTelB(),
            'deliveryAddressReference' => $order->getEntEndRef(),
            'products' => $order->getProducts()
        );

        // Calls the method to send the email order info
        $this->send($subject, 'sac@lojaskd.com.br', $customer->getEmail(), $customer->getFirstName(), 'AppBundle:Emails:order_confirmation.html.twig', $emailData);
    }
}
