<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\Validate\ValidatePayment;
use AppBundle\Repository\PaymentRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class PaymentRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class PaymentRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getPaymentInfo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn([
            'prazo' => 1,
            'nome' => 'test',
            'descConfirmacao' => 'test',
            'instalmentText' => 'test',
            'interestText' => 'test'
        ]);

        $classMap = new ClassMetadata(ValidatePayment::class);
        $repository = new PaymentRepository($entityManager, $classMap);
        
        $payment = new ValidatePayment();
        $payment->setId(2);

        $this->assertNotNull($repository->getPaymentInfo([$payment]));
    }
}
