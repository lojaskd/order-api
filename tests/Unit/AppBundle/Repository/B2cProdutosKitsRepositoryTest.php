<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cProdutosKits;
use AppBundle\Repository\B2cProdutosKitsRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cProdutosKitsRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cProdutosKitsRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getProductKit()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cProdutosKits::class);
        $repository = new B2cProdutosKitsRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getProductKit(1, 1));
    }
}
