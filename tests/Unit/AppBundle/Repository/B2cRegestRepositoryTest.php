<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cRegest;
use AppBundle\Repository\B2cRegestRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cRegestRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cRegestRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getIBGECode()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['codIbge' => '1234']);

        $classMap = new ClassMetadata(B2cRegest::class);
        $repository = new B2cRegestRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getIBGECode('Curitiba', 'PR'));
    }
}
