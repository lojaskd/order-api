<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cAtributoValor;
use AppBundle\Repository\B2cAtributoValorRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cAtributoValorRepositoryTest
 * @package Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cAtributoValorRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function updateMarketplaceStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cAtributoValor::class);
        $repository = new B2cAtributoValorRepository($entityManager, $classMap);
        $this->assertTrue($repository->updateMarketplaceStock(new B2cAtributoValor(), 1));
    }
}
