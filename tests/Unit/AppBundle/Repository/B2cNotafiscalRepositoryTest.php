<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cNotafiscal;
use AppBundle\Entity\VO\Create\CreateInvoiceItemVO;
use AppBundle\Entity\VO\Create\CreateInvoiceVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Repository\B2cNotafiscalRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cNotafiscalRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cNotafiscalRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function searchNFFromTerm()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([new CreateOrderVO()]);

        $classMap = new ClassMetadata(B2cNotafiscal::class);
        $repository = new B2cNotafiscalRepository($entityManager, $classMap);

        $this->assertNotNull($repository->searchNFFromTerm('xpto'));
    }

    /**
     * @test
     */
    public function createInvoice()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([new CreateOrderVO()]);

        $classMap = new ClassMetadata(B2cNotafiscal::class);
        $repository = new B2cNotafiscalRepository($entityManager, $classMap);

        $invoice = new CreateInvoiceVO();
        $invoice->setItens([new CreateInvoiceItemVO()]);

        $this->assertNotNull($repository->createInvoice($invoice));
    }
}
