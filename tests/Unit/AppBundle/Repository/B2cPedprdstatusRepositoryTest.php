<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPedprdstatus;
use AppBundle\Repository\B2cPedprdstatusRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPedprdstatusRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPedprdstatusRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getProductOrderStatus()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['polo' => 1]);

        $classMap = new ClassMetadata(B2cPedprdstatus::class);
        $repository = new B2cPedprdstatusRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getProductOrderStatus(1, 1));
    }
}
