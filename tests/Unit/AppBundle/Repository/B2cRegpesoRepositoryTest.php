<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cRegpeso;
use AppBundle\Repository\B2cRegpesoRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cRegpesoRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cRegpesoRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getMaxWeight()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['max_weight' => 3]);

        $classMap = new ClassMetadata(B2cRegpeso::class);
        $repository = new B2cRegpesoRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getMaxWeight());
        $this->assertInternalType('int', $repository->getMaxWeight());
    }
}
