<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPromoReg;
use AppBundle\Entity\B2cPromoVale;
use AppBundle\Repository\B2cPromoRegRepository;
use AppBundle\Repository\B2cPromoValeRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPromoRegRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPromoValeRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function updateCouponSituation()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPromoVale::class);
        $repository = new B2cPromoValeRepository($entityManager, $classMap);

        $this->assertTrue($repository->updateCouponSituation('xpto'));
    }

    /**
     * @test
     */
    public function updateCouponUtilization()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPromoVale::class);
        $repository = new B2cPromoValeRepository($entityManager, $classMap);

        $this->assertTrue($repository->updateCouponUtilization('xpto'));
    }
}
