<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\B2cPedpedMarketplace;
use AppBundle\Entity\VO\Create\CreateMarketPlaceVO;
use AppBundle\Repository\B2cPedpedMarketplaceRepository;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Mockery;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPedpedMarketplaceRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPedpedMarketplaceRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function insertOrderMarketplace()
    {
        $entityManager = $this->getEntityManager();

        $classMap = new ClassMetadata(B2cPedpedMarketplace::class);
        $repository = new B2cPedpedMarketplaceRepository($entityManager, $classMap);

        $order = new B2cPedped();
        $marketPlace = new CreateMarketPlaceVO();
        $marketPlace->setNewDeliveryDate(new DateTime());

        $this->assertTrue($repository->insertOrderMarketplace($order, $marketPlace));
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function insertOrderMarketplaceException()
    {
        $entityManager = Mockery::mock(EntityManager::class);

        $classMap = new ClassMetadata(B2cPedpedMarketplace::class);
        $repository = new B2cPedpedMarketplaceRepository($entityManager, $classMap);

        $order = new B2cPedped();
        $marketPlace = new CreateMarketPlaceVO();
        $marketPlace->setNewDeliveryDate(new DateTime());

        $repository->insertOrderMarketplace($order, $marketPlace);
    }
}
