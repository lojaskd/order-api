<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPedprdSelos;
use AppBundle\Repository\B2cPedprdSelosRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPedprdSelosRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPedprdSelosRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function insertOrderProductsSeals()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPedprdSelos::class);
        $repository = new B2cPedprdSelosRepository($entityManager, $classMap);

        $seal = [
            ['idPrdprd' => 1, 'id' => 1, 'nome' => 'test']
        ];
        $this->assertNotNull($repository->insertOrderProductsSeals(1, [$seal]));
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function insertOrderProductsSealsException()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPedprdSelos::class);
        $repository = new B2cPedprdSelosRepository($entityManager, $classMap);

        $seal = ['idPrdprd' => 1, 'id' => 1, 'nome' => 'test'];
        $repository->insertOrderProductsSeals(1, [$seal]);
    }
}
