<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPromoPag;
use AppBundle\Entity\VO\Create\CreatePaymentVO;
use AppBundle\Repository\B2cPromoPagRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPromoPagRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPromoPagRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function validatePaymentCoupon()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPromoPag::class);
        $repository = new B2cPromoPagRepository($entityManager, $classMap);

        $payment = new CreatePaymentVO();

        $this->assertNotNull($repository->validatePaymentCoupon(1, $payment));
    }
}
