<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPrdfrete;
use AppBundle\Repository\B2cPrdfreteRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPrdfreteRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPrdfreteRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getShippingProduct()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('in')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['value' => 18.9]);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPrdfrete::class);
        $repository = new B2cPrdfreteRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getShippingProduct(1, [1]));
    }
}
