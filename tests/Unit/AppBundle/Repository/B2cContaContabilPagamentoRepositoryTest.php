<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cContaContabilPagamento;
use AppBundle\Repository\B2cContaContabilPagamentoRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cContaContabilPagamentoRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cContaContabilPagamentoRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getAccounting()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cContaContabilPagamento::class);
        $repository = new B2cContaContabilPagamentoRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getAccounting(1));
    }

    /**
     * @test
     */
    public function getAccountingMarketPlace()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cContaContabilPagamento::class);
        $repository = new B2cContaContabilPagamentoRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getAccountingMarketPlace(1));
    }
}
