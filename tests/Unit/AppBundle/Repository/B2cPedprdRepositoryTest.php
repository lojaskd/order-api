<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\B2cPedprd;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Repository\B2cPedprdRepository;
use DateTime;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPedprdRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPedprdRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getStatusInRepository()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $product1 = ['dataCancelamentoSalesforce' => date('Y-m-d')];
        $product2 = ['dataCancelamentoSalesforce' => ''];
        $pedprdstatus = [
            'dataEntR' => date('Y-m-d'),
            'dataCteR' => date('Y-m-d'),
            'dataEntFornR' => date('Y-m-d'),
            'dataEntdFornR' => date('Y-m-d'),
            'dataFatFornR' => date('Y-m-d'),
            'dataLibR' => date('Y-m-d'),
            'dataAprR' => date('Y-m-d')
        ];

        $this->assertNotNull($repository->getStatus($product1, $pedprdstatus));
        $this->assertNotNull($repository->getStatus($product2, $pedprdstatus));
    }

    /**
     * @test
     */
    public function getProductionCaseOne()
    {
        $pedprdstatus = ['dataEntFornR' => 1];

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($this->getEntityManager(), $classMap);

        $this->assertNotNull($repository->getProduction($pedprdstatus));
    }

    /**
     * @test
     */
    public function getProductionCaseTwo()
    {
        $pedprdstatus = [
            'dataEntFornR' => 0,
            'dataEntdForn' => new DateTime(),
            'dataLibR' => new DateTime()
        ];

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($this->getEntityManager(), $classMap);

        $this->assertNotNull($repository->getProduction($pedprdstatus));
    }

    /**
     * @test
     */
    public function formatProductInfo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getMinStockAction')->andReturn(1);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $order = [
            'idPedped' => 1
        ];
        $product = [
            'id' => 1,
            'idPrdprd' => 1,
            'prdNome' => 'Test',
            'prdQtd' => 1,
            'prdData' => new DateTime(),
            'prdReferencia' => 1,
            'prdCategoria' => 1,
            'prdFornecedor' => 1,
            'prdFornNome' => 'Test',
            'prdPrazoForn' => 3,
            'prdPrazoDisp' => 6,
            'prdDataEntregaFornecedor' => new DateTime(),
            'prdDestaque' => false,
            'prdPeso' => 1,
            'prdEstilo' => 1,
            'prdPresente' => false,
            'prdStqLoja' => 1,
            'prdStqFornecedor' => '1',
            'prdEmbalagem' => '1',
            'prdTextoCartao' => '1',
            'prdAro' => '1',
            'prdImglistagem' => '1',
            'prdPromoDesc' => '1',
            'bpromocao' => '1',
            'prdDescPerc' => '1',
            'prdCubagem' => '1',
            'prdCor' => '1',
            'prdSexo' => '1',
            'prdTamanho' => '1',
            'prdPvreal' => '1',
            'prdPvpromocao' => '1',
            'prdValeCompra' => '1',
            'prdVndInfo' => '1',
            'prdDescValor' => '1',
            'prdPvtotal' => '1',
            'prdPromoFim' => new DateTime(),
            'prdCusto' => '1',
            'prdVndAberto' => '1',
            'prdVndJunta' => '1',
            'prdPromoIni' => new DateTime(),
            'sfOpportunitylineitem' => '1',
            'prdDataEntregaFornecedorRealizada' => new DateTime(),
            'idNotafiscal' => '1',
            'idTipoOcorrencia' => '1',
            'nomeOcorrencia' => '1',
            'grupoOcorrencia' => '1',
            'dataOcorrencia' => new DateTime(),
            'itemCancelado' => '1',
            'dataCancelamentoSalesforce' => new DateTime(),
            'ordemCompra' => '1',
            'dataPrevEntregaForn' => new DateTime(),
            'clusterRmkt' => '1',
            'dataOrdemCompra' => new DateTime(),
            'idFilialEstoque' => '1',
            'codigoTransportadorColeta' => 'xpto',
            'destination' => 'cdc',
            'tipoVendaProduto' => false,
            'prdValorRealPago' => 1,
            'prdFreteRealPago' => 1
        ];
        $pedpedstatus = [
            'dataAprR' => new DateTime(),
            'dataLibR' => new DateTime(),
            'statusMsg' => 'test',
            'dataApr' => new DateTime(),
            'dataLib' => new DateTime(),
            'dataFatForn' => new DateTime(),
            'dataFatFornR' => new DateTime(),
            'dataEntdFornR' => new DateTime(),
            'dataEntdForn' => new DateTime(),
            'dataEntForn' => new DateTime(),
            'dataEntFornR' => new DateTime(),
            'dataNf' => new DateTime(),
            'dataNfR' => new DateTime(),
            'dataEmb' => new DateTime(),
            'dataEmbR' => new DateTime(),
            'dataCte' => new DateTime(),
            'dataCteR' => new DateTime(),
            'dataEnt' => new DateTime(),
            'dataEntR' => new DateTime(),
            'idPrdprd' => 1
        ];
        $status = 1;
        $production = [];
        $polo = [];
        $branchOfficeInfo = [
            'id_filial' => 1,
            'id_filial_sap' => 1,
            'entrega_imediata' => true
        ];
        $shipper = [
            'nomeTransportadora' => 'Test',
            'hub' => '1'
        ];

        $this->assertNotNull($repository->formatProductInfo(
            $order,
            $product,
            $pedpedstatus,
            $status,
            $production,
            $polo,
            $branchOfficeInfo,
            $shipper,
            []
        ));
    }

    /**
     * @test
     */
    public function generateProductsOrder()
    {
        $entityManager = $this->getEntityManager();

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $b2cPedped = new B2cPedped();
        $createProductVO = new CreateProductVO();

        $this->assertNotNull($repository->generateProductsOrder($b2cPedped, $createProductVO));
    }

    /**
     * @test
     */
    public function setStockInfo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->setStockInfo(1, 1, 1, 1, 1));
    }

    /**
     * @test
     */
    public function setMarketplaceStockInfo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->setMarketplaceStockInfo(1, 1, 1));
    }

    /**
     * @test
     */
    public function setNewDeadline()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->setNewDeadline(1, 1, 1, 1, 1));
    }

    /**
     * @test
     */
    public function getOrderProductsStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('orWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getOrderProductsStock(1));
    }

    /**
     * @test
     */
    public function getBranchOfficeInformation()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('distinct')->andReturn($entityManager);
        $entityManager->shouldReceive('addSelect')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getSingleResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getBranchOfficeInformation(1, 1));
    }

    /**
     * @test
     */
    public function getProductsDatesByStatus()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('distinct')->andReturn($entityManager);
        $entityManager->shouldReceive('addSelect')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getSingleResult')->andReturn([]);
        $entityManager->shouldReceive('getProductOrderStatus')->andReturn([
            'shippingDate' => [
                'field' => new DateTime()
            ]
        ]);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $orderVO = new OrderVO();
        $orderVO->setProducts([new OrderProductVO()]);

        $this->assertNotNull($repository->getProductsDatesByStatus($orderVO));
    }

    /**
     * @test
     */
    public function cancelOrderProduct()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->cancelOrderProduct(1, 1));
    }

    /**
     * @test
     */
    public function getByOrderAndProduct()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getByOrderAndProduct(1, 1));
    }

    /**
     * @test
     */
    public function setDestination()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedprd::class);
        $repository = new B2cPedprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->setDestination(1, 1, 'cdc'));
    }
}
