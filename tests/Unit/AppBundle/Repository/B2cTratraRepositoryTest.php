<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\B2cTratra;
use AppBundle\Entity\VO\Create\CreateRegionVO;
use AppBundle\Repository\B2cTratraRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cTratraRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cTratraRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getShippingFromCategoriesAndRegion()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('addOrderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('in')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cTratra::class);
        $repository = new B2cTratraRepository($entityManager, $classMap);

        $region = new CreateRegionVO();
        $region->setChooseCustomer(true);

        $this->assertNotNull($repository->getShippingFromCategoriesAndRegion($region, [1]));
    }

    /**
     * @test
     */
    public function calculateFreightShipping()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('orWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('addOrderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('in')->andReturn($entityManager);
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('getDQL')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([['valor' => 12.9]]);

        $classMap = new ClassMetadata(B2cTratra::class);
        $repository = new B2cTratraRepository($entityManager, $classMap);

        $createRegionVO = new CreateRegionVO();
        $createRegionVO->setChooseCustomer(true);

        $this->assertNotNull($repository->calculateFreightShipping($createRegionVO, 1,1,3,1,1));
    }

    /**
     * @test
     */
    public function getShipperRegion()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('orWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('addOrderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('in')->andReturn($entityManager);
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn([['valor' => 12.9]]);

        $classMap = new ClassMetadata(B2cTratra::class);
        $repository = new B2cTratraRepository($entityManager, $classMap);

        $b2cPedped = new B2cPedped();

        $this->assertNotNull($repository->getShipperRegion($b2cPedped));
    }
}
