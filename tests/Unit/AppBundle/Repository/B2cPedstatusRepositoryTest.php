<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPedstatus;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use AppBundle\Repository\B2cPedstatusRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPedstatusRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPedstatusRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function updateApprovementDate()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPedstatus::class);
        $repository = new B2cPedstatusRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateApprovementDate(new UpdateOrderStatusVO()));
    }

    /**
     * @test
     */
    public function updateDateCaseOne()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cPedstatus());

        $classMap = new ClassMetadata(B2cPedstatus::class);
        $repository = new B2cPedstatusRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateDate(new UpdateOrderStatusVO(), true));
    }

    /**
     * @test
     */
    public function updateDateCaseTwo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findOneBy')->andReturn(null);

        $classMap = new ClassMetadata(B2cPedstatus::class);
        $repository = new B2cPedstatusRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateDate(new UpdateOrderStatusVO(), true));
    }
}
