<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPrdprd;
use AppBundle\Entity\B2cViewProdutos;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Repository\B2cViewProdutosRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cViewProdutosRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cViewProdutosRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getMinStockAction()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['prdStq' => 1, 'matStq' => 1]);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getMinStockAction(1));
    }

    /**
     * @test
     */
    public function getProductInfoFromId()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['prdStq' => 1, 'matStq' => 1]);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getProductInfoFromId(1));
    }

    /**
     * @test
     */
    public function getProductStockAndDeadline()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['prdStq' => 1, 'matStq' => 1]);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getProductStockAndDeadline(1, 1));
    }

    /**
     * @test
     */
    public function getProductDeadline()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('prepare')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('fetch')->andReturn(['prdStq' => 1, 'matStq' => 1]);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getProductDeadline(1, date('Y-m-d'), '88888888'));
    }

    /**
     * @test
     */
    public function getProviderStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['prdStq' => 1, 'matStq' => 1]);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getProviderStock(1));
    }

    /**
     * @test
     */
    public function updateProductStockInfo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $createProductVO = new CreateProductVO();
        $b2cPrdprd = new B2cPrdprd();

        $this->assertNotNull($repository->updateProductStockInfo($createProductVO, $b2cPrdprd));
    }

    /**
     * @test
     */
    public function updateProductAvailabilityInfo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $createProductVO = new CreateProductVO();

        $this->assertNotNull($repository->updateProductAvailabilityInfo(1, $createProductVO, 'site'));
    }

    /**
     * @test
     */
    public function getKit()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn(['prdStq' => 1, 'matStq' => 1]);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getKit(1));
    }

    /**
     * @test
     */
    public function updateKitStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $createProductVO = new CreateProductVO();

        $this->assertNotNull($repository->updateKitStock($createProductVO));
    }

    /**
     * @test
     */
    public function getDateAddNextBusinessDay()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('prepare')->andReturn($entityManager);
        $entityManager->shouldReceive('fetch')->andReturn([]);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getDateAddNextBusinessDay(date('Y-m-d'), 1));
    }

    /**
     * @test
     */
    public function getDateSubNextBusinessDay()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('prepare')->andReturn($entityManager);
        $entityManager->shouldReceive('fetch')->andReturn([]);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getDateSubNextBusinessDay(date('Y-m-d'), 1));
    }

    /**
     * @test
     */
    public function getDateDiffBusinessDay()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('prepare')->andReturn($entityManager);
        $entityManager->shouldReceive('fetch')->andReturn(1);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getDateDiffBusinessDay(date('Y-m-d'), date('Y-m-d')));
    }

    /**
     * @test
     */
    public function increaseProviderStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->increaseProviderStock(1,1,'site'));
    }

    /**
     * @test
     */
    public function increaseStoreStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cViewProdutos::class);
        $repository = new B2cViewProdutosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->increaseStoreStock(1,1,'site'));
    }
}
