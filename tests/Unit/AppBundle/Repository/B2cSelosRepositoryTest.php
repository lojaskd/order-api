<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cSelos;
use AppBundle\Repository\B2cSelosRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cSelosRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cSelosRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getSealsProducts()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn(['seal1' => '1234']);

        $classMap = new ClassMetadata(B2cSelos::class);
        $repository = new B2cSelosRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getSealsProducts(1));
    }
}
