<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\AttributesOptions;
use AppBundle\Repository\AttributesOptionsRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class AttributesOptionsRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class AttributesOptionsRepositoryTest extends PHPUnit_Framework_TestCase
{
    private function getEntityManager()
    {
        $attributesOptions = [
            'id' => 1
        ];

        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('createQueryBuilder')->andReturn($entityManager);
        $entityManager->shouldReceive('select')->andReturn($entityManager);
        $entityManager->shouldReceive('from')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getQuery')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([$attributesOptions]);

        return $entityManager;
    }

    /**
     * @test
     */
    public function getAttributeOptions()
    {
        $classMap = new ClassMetadata(AttributesOptions::class);

        $repository = new AttributesOptionsRepository($this->getEntityManager(), $classMap);
        $result = $repository->getAttributeOptions(1);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }
}
