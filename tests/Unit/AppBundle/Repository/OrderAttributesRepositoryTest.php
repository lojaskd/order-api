<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\OrderAttributes;
use AppBundle\Repository\OrderAttributesRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class OrderAttributesRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class OrderAttributesRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getOrderAttributes()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([['value' => '1']]);

        $classMap = new ClassMetadata(OrderAttributes::class);
        $repository = new OrderAttributesRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getOrderAttributes(1, 1));
    }
}
