<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\Attributes;
use AppBundle\Repository\AttributesRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class AttributesRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class AttributesRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getAttributes()
    {
        $classMap = new ClassMetadata(Attributes::class);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $repository = new AttributesRepository($entityManager, $classMap);
        $this->assertNotNull($repository->getAttributes());
    }
}
