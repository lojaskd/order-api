<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cForfor;
use AppBundle\Repository\B2cForforRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cForforRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cForforRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getPolo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['polo' => 1]);

        $classMap = new ClassMetadata(B2cForfor::class);
        $repository = new B2cForforRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getPolo(1));
    }
}
