<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPrdestoque;
use AppBundle\Repository\B2cPrdestoqueRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPrdestoqueRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPrdestoqueRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function updateBranchOfficeStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPrdestoque::class);
        $repository = new B2cPrdestoqueRepository($entityManager, $classMap);

        $this->assertTrue($repository->updateBranchOfficeStock(1, 1, 2));
    }

    /**
     * @test
     */
    public function increaseStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPrdestoque::class);
        $repository = new B2cPrdestoqueRepository($entityManager, $classMap);

        $this->assertTrue($repository->increaseStock(1, 1, 2));
    }
}
