<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cFornecedorRegiao;
use AppBundle\Repository\B2cFornecedorRegiaoRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cFornecedorRegiaoRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cFornecedorRegiaoRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getDestinationBySupplierAndRegionName()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getSingleResult')->andReturn(['destination' => 'test']);

        $classMap = new ClassMetadata(B2cFornecedorRegiao::class);
        $repository = new B2cFornecedorRegiaoRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getDestinationBySupplierAndRegionName(1, 1));
    }

    /**
     * @test
     */
    public function getDestinationBySupplierAndRegionNameException()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getSingleResult')->andReturn(null);

        $classMap = new ClassMetadata(B2cFornecedorRegiao::class);
        $repository = new B2cFornecedorRegiaoRepository($entityManager, $classMap);

        $repository->getDestinationBySupplierAndRegionName(1, 1);
    }
}
