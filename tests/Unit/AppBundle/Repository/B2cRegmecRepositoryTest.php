<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cRegmec;
use AppBundle\Repository\B2cRegmecRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cRegmecRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cRegmecRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getMaxCubage()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['max_cubage' => 3]);

        $classMap = new ClassMetadata(B2cRegmec::class);
        $repository = new B2cRegmecRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getMaxCubage());
        $this->assertInternalType('int', $repository->getMaxCubage());
    }
}
