<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPromo;
use AppBundle\Repository\B2cPromoRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPromoRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPromoRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getActiveShippingPromotions()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('createNativeQuery')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPromo::class);
        $repository = new B2cPromoRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getActiveShippingPromotions(1, 1, 1, 1, 1, 399.0));
    }

    /**
     * @test
     */
    public function getValidCouponFromCode()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('createNativeQuery')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPromo::class);
        $repository = new B2cPromoRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getValidCouponFromCode(
            'xpto',
            'site',
            'site',
            'content',
            1,
            1,
            1,
            1,
            1,
            1,
            33.90
        )
        );
    }

    /**
     * @test
     */
    public function getActiveDiscountPromotion()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('createNativeQuery')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPromo::class);
        $repository = new B2cPromoRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getActiveDiscountPromotion('xpto', 'site'));
    }
}
