<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPromoFor;
use AppBundle\Entity\B2cPromoPrd;
use AppBundle\Repository\B2cPromoForRepository;
use AppBundle\Repository\B2cPromoPrdRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPromoPrdRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPromoPrdRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function validateProductCoupon()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPromoPrd::class);
        $repository = new B2cPromoPrdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->validateProductCoupon(1, 1));
    }
}
