<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\VO\Create\CreateCouponVO;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
use AppBundle\Entity\VO\Create\CreateMarketPlaceVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreatePaymentVO;
use AppBundle\Entity\VO\Create\CreateRegionVO;
use AppBundle\Entity\VO\Create\CreateShippingVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use AppBundle\Repository\B2cPedpedRepository;
use DateTime;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\DataBuilder\CreateOrderVODataBuilder;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPedpedRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPedpedRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getOrder()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getOrder(1));
    }

    /**
     * @test
     */
    public function getOrders()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getOrders(1));
    }

    /**
     * @test
     */
    public function getMarketPlaceOrder()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getMarketPlaceOrder(1));
    }

    /**
     * @test
     */
    public function generateOrder()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $createOrderBuilder = new CreateOrderVODataBuilder();
        $createOrderVO = $createOrderBuilder->build();

        $this->assertNotNull($repository->generateOrder($createOrderVO));
    }

    /**
     * @test
     */
    public function updateBillingAccount()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateBillingAccount(1, 1, 'test'));
    }

    /**
     * @test
     */
    public function updateSecondPaymentBillingAccount()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateSecondPaymentBillingAccount(1, 1, 'test'));
    }

    /**
     * @test
     */
    public function updateOrderDeadline()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateOrderDeadline(1, 1, date('Y-m-d')));
    }

    /**
     * @test
     */
    public function updateOrderStatus()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateOrderStatus(new UpdateOrderStatusVO()));
    }

    /**
     * @test
     */
    public function updateOrderInternalStatus()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateOrderInternalStatus(new UpdateOrderStatusVO()));
    }

    /**
     * @test
     */
    public function updateNewDeliveryDate()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateNewDeliveryDate(1, new DateTime()));
    }

    /**
     * @test
     */
    public function updateOrderUpdateDate()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateOrderUpdateDate(1));
    }

    /**
     * @test
     */
    public function updateOrderShipping()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cPedped());
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $orderVO = new OrderVO();
        $orderVO->setRegion(new CreateRegionVO());

        $createShippingVO = new CreateShippingVO();
        $createShippingVO->setRuleFreight('fretinho');

        $this->assertNotNull($repository->updateOrderShipping($orderVO, $createShippingVO));
    }

    /**
     * @test
     */
    public function updateOrderShippingCaseTwo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findOneBy')->andReturn(null);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $orderVO = new OrderVO();
        $orderVO->setRegion(new CreateRegionVO());

        $createShippingVO = new CreateShippingVO();
        $createShippingVO->setRuleFreight('fretinho');

        $this->assertNotNull($repository->updateOrderShipping($orderVO, $createShippingVO));
    }

    /**
     * @test
     */
    public function searchOrderFromTerm()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->searchOrderFromTerm('test'));
    }

    /**
     * @test
     */
    public function searchFromOrderId()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->searchFromOrderId(1));
    }

    /**
     * @test
     */
    public function searchFromInvoice()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->searchFromInvoice(1));
    }

    /**
     * @test
     */
    public function searchFromEmail()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->searchFromEmail('test@teste.com.br'));
    }

    /**
     * @test
     */
    public function searchFromDocument()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->searchFromDocument('12312312312'));
    }

    /**
     * @test
     */
    public function searchFromPhoneNumber()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->searchFromPhoneNumber('12312312312'));
    }

    /**
     * @test
     */
    public function searchFromMarketPlaceOrder()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->searchFromMarketPlaceOrder(1));
    }

    /**
     * @test
     */
    public function searchFromCustomerName()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->searchFromCustomerName('Test'));
    }

    /**
     * @test
     */
    public function getOrdersByCustomer()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('addOrderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getOrdersByCustomer(1));
    }

    /**
     * @test
     */
    public function getRevenueReportByCustomer()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('prepare')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('fetch')->andReturn([]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getRevenueReportByCustomer(1));
    }

    /**
     * @test
     */
    public function getSituation()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('join')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('leftJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('orderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('addOrderBy')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn([
            'name' => 'test',
            'internalStatusName' => 'test',
            'staintNome' => 'test'
        ]);

        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getSituation(1));
    }

    /**
     * @test
     */
    public function formatOrderInfo()
    {
        $classMap = new ClassMetadata(B2cPedped::class);
        $repository = new B2cPedpedRepository($this->getEntityManager(), $classMap);

        $order = [
            'idPedped' => '1',
            'pedDtaCompra' => new DateTime(),
            'pedDtaAtualizacao' => new DateTime(),
            'atendente' => '1',
            'pedSituacao' => '1',
            'situationName' => '1',
            'internalStatusName' => '1',
            'pedStaint' => '1',
            'pedPendCanc' => '1',
            'idClicli' => '1',
            'cobNome' => '1',
            'cobSobrenome' => '1',
            'cobEmail' => '1',
            'cobCpfCgc' => '1',
            'cobRgIe' => '1',
            'cobTipoPessoa' => '1',
            'cobSexo' => '1',
            'cobDtaNasc' => new DateTime(),
            'cobContato' => '1',
            'cobEndereco' => '1',
            'cobEndNumero' => '1',
            'cobEndComp' => '1',
            'cobEndBairro' => '1',
            'cobEndCidade' => '1',
            'cobEndUf' => '1',
            'cobEndCep' => '1',
            'cobEndProvincia' => '1',
            'cobEndPais' => '1',
            'cobTelA' => '1',
            'cobTelB' => '1',
            'entNome' => '1',
            'entSobrenome' => '1',
            'entIdentificacao' => '1',
            'entEndereco' => '1',
            'entEndNumero' => '1',
            'entEndComp' => '1',
            'entEndBairro' => '1',
            'entEndCidade' => '1',
            'entCodIbge' => '1',
            'entEndUf' => '1',
            'entEndCep' => '1',
            'entEndRef' => '1',
            'entEndProvincia' => '1',
            'entEndPais' => '1',
            'entTelA' => '1',
            'entTelB' => '1',
            'pedObservacao' => '1',
            'pedTipoFrete' => '1',
            'pedTransportadora' => '1',
            'pedPrazoEntrega' => '1',
            'pedPrazoTransporte' => '1',
            'pedDataEntrega' => new DateTime(),
            'pedEntregaRegiao' => '1',
            'pedCodEnvio' => '1',
            'pedMontagem' => '1',
            'pedValorMontagem' => '1',
            'pedValorSubtotal' => '1',
            'pedValorEmbalagem' => '1',
            'pedValorCustofrete' => '1',
            'pedValorFrete' => '1',
            'pedValorDesconto' => '1',
            'pedValorDescontoLoja' => '1',
            'pedValorValecompra' => '1',
            'pedValorTotal' => '1',
            'idPagpag1' => '1',
            'idPagfor1' => '1',
            'pedValorTotal1' => '1',
            'pedValorReal1' => '1',
            'pagNumParcelas1' => '1',
            'pagValEntrada1' => '1',
            'pagValParcela1' => '1',
            'pagIndexador1' => '1',
            'boletoParcelado1' => '1',
            'boletoVencimento1' => new DateTime(),
            'ped_tipo_pagamento_1' => '1',
            'ped_forma_pagamento_1' => '1',
            'ped_forma_pagdesc_1' => '1',
            'ped_valor_desconto_loja_1' => '1',
            'contadorAlteracaoDescontoLoja1' => '1',
            'pagamentoConfirmado1' => '1',
            'idPagpag2' => '1',
            'idPagfor2' => '1',
            'pedValorTotal2' => '1',
            'pedValorReal2' => '1',
            'pagNumParcelas2' => '1',
            'pagValEntrada2' => '1',
            'pagValParcela2' => '1',
            'pagIndexador2' => '1',
            'boletoParcelado2' => '1',
            'boletoVencimento2' => new DateTime(),
            'pedTipoPagamento2' => '1',
            'pedFormaPagamento2' => '1',
            'pedFormaPagdesc2' => '1',
            'pedValorDescontoLoja2' => '1',
            'contadorAlteracaoDescontoLoja2' => '1',
            'pagamentoConfirmado2' => '1',
            'pedOrigem' => '1',
            'pedPrimeiraOrigem' => '1',
            'pedPrimeiraOrigemData' => '1',
            'pedReferer' => '1',
            'pedCupom' => '1',
            'pedPromocao' => '1',
            'ip' => '1',
            'csUsuario' => '1',
            'csStatus' => '1',
            'csScore' => '1',
            'csDiagnostico' => '1',
            'csSolicitacao' => '1',
            'seqped' => '1',
            'exportado' => '1',
            'pedTransportadoraIntegrado' => '1',
            'pedValorComissao' => '1',
            'pedDataAfiliado' => '1',
            'pedAfiliado' => '1',
            'ordemGeracaoBoleto' => '1',
            'analyticsNegativado' => '1',
            'pedEntregaData' => new DateTime(),
            'pedFaturado' => '1',
            'pagTel1' => '1',
            'pagTel2' => '1',
            'pagTitular1' => '1',
            'pagTitular2' => '1',
            'contaContabil1' => '1',
            'contaContabil2' => '1',
            'contaContabilNome1' => '1',
            'contaContabilNome2' => '1',
            'sfOpportunity' => '1',
            'sfDataSync' => new DateTime(),
            'sfCustoFinanceiroSync' => '1',
            'assistencia' => '1',
            'recompra' => '1',
            'troca' => '1',
            'pedNovaDataEntrega' => new DateTime(),
            'pedTipoPagamento1' => '1',
            'pedFormaPagamento1' => '1',
            'pedFormaPagdesc1' => '1',
            'pedValorDescontoLoja1' => '1',
            'tipoPedido' => 'marketplace',
            'status' => [[
                'pedSituacao' => 3,
                'pedStaint' => 0,
                'pedDtaSituacaoPrev' => new DateTime(),
                'pedDtaSituacao' => new DateTime()
            ]],
        ];

        $marketPlace = [
            'nome' => 'test',
            'slug' => 'test',
            'codOrder' => 1
        ];

        $this->assertNotNull($repository->formatOrderInfo($order, $marketPlace));
    }
}
