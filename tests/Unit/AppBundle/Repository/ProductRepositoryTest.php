<?php

namespace Unit\AppBundle\Repository;

use AppBundle\Entity\Validate\ValidateProduct;
use AppBundle\Repository\ProductRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class ProductRepositoryTest
 * @package Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class ProductRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getProductRequestInfo()
    {
        $entityManager = $this->getEntityManager();
        $classMap = new ClassMetadata(ValidateProduct::class);
        $repository = new ProductRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getProductRequestInfo([new ValidateProduct()]));
    }
}
