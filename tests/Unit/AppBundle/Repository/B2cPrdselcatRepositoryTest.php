<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPrdselcat;
use AppBundle\Repository\B2cPrdselcatRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPrdselcatRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPrdselcatRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getRelatedCategories()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getResult')->andReturn([new B2cPrdselcat()]);

        $classMap = new ClassMetadata(B2cPrdselcat::class);
        $repository = new B2cPrdselcatRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getRelatedCategories(1));
    }
}
