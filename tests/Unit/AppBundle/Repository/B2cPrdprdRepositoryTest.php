<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPrdprd;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Repository\B2cPrdprdRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPrdprdRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPrdprdRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function updateProviderStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPrdprd::class);
        $repository = new B2cPrdprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateProviderStock(1, 1));
    }

    /**
     * @test
     */
    public function increaseProviderStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPrdprd::class);
        $repository = new B2cPrdprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->increaseProviderStock(1, 1, 'site'));
    }

    /**
     * @test
     */
    public function getKit()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getArrayResult')->andReturn(['id' => 1]);

        $classMap = new ClassMetadata(B2cPrdprd::class);
        $repository = new B2cPrdprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getKit(1));
    }

    /**
     * @test
     */
    public function updateKitStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn(['id' => 1]);

        $classMap = new ClassMetadata(B2cPrdprd::class);
        $repository = new B2cPrdprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateKitStock(new CreateProductVO()));
    }

    /**
     * @test
     */
    public function updateProduct()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn(['id' => 1]);

        $classMap = new ClassMetadata(B2cPrdprd::class);
        $repository = new B2cPrdprdRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateProduct(1, 'site'));
    }
}
