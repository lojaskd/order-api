<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPedprdEstorno;
use AppBundle\Entity\VO\Update\UpdateProductsStatusVO;
use AppBundle\Repository\B2cPedprdEstornoRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Mockery;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPedprdEstornoRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPedprdEstornoRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function insertOrderProductReversal()
    {
        $entityManager = $this->getEntityManager();

        $classMap = new ClassMetadata(B2cPedprdEstorno::class);
        $repository = new B2cPedprdEstornoRepository($entityManager, $classMap);

        $updateProductStatusVO = new UpdateProductsStatusVO();
        $this->assertNotNull($repository->insertOrderProductReversal($updateProductStatusVO, 1));
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function insertOrderProductReversalException()
    {
        $entityManager = Mockery::mock(EntityManager::class);

        $classMap = new ClassMetadata(B2cPedprdEstorno::class);
        $repository = new B2cPedprdEstornoRepository($entityManager, $classMap);

        $updateProductStatusVO = new UpdateProductsStatusVO();
        $repository->insertOrderProductReversal($updateProductStatusVO, 1);
    }
}
