<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cClicli;
use AppBundle\Entity\B2cPromoFor;
use AppBundle\Entity\Validate\ValidateAddress;
use AppBundle\Entity\Validate\ValidateCustomer;
use AppBundle\Repository\B2cClicliRepository;
use AppBundle\Repository\B2cPromoForRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cClicliRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cClicliRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getCustomerById()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['nome' => 'image.jpg']);

        $classMap = new ClassMetadata(B2cClicli::class);
        $repository = new B2cClicliRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getCustomerById(1));
    }

    /**
     * @test
     */
    public function getMarketPlaceCustomer()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(new B2cClicli());

        $classMap = new ClassMetadata(B2cClicli::class);
        $repository = new B2cClicliRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getMarketPlaceCustomer('12312312312', 'test@teste.com.br'));
    }

    /**
     * @test
     */
    public function addCustomer()
    {
        $classMap = new ClassMetadata(B2cClicli::class);
        $repository = new B2cClicliRepository($this->getEntityManager(), $classMap);

        $this->assertNotNull($repository->addCustomer(new ValidateCustomer(), new ValidateAddress()));
    }

    /**
     * @test
     */
    public function customerAcceptsNewsletter()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['newsletter' => true]);

        $classMap = new ClassMetadata(B2cClicli::class);
        $repository = new B2cClicliRepository($entityManager, $classMap);

        $this->assertNotNull($repository->customerAcceptsNewsletter(1));
    }
}
