<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cNotafiscalItem;
use AppBundle\Repository\B2cNotafiscalItemRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cNotafiscalItemRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cNotafiscalItemRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getShipper()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setMaxResults')->andReturn($entityManager);
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('innerJoin')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['shipper' => 1]);

        $classMap = new ClassMetadata(B2cNotafiscalItem::class);
        $repository = new B2cNotafiscalItemRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getShipper(1, 1));
    }
}
