<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPrdprdMarketplace;
use AppBundle\Repository\B2cPrdprdMarketplaceRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPrdprdMarketplaceRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPrdprdMarketplaceRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function getProductStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('getOneOrNullResult')->andReturn(['estoque' => 2]);

        $classMap = new ClassMetadata(B2cPrdprdMarketplace::class);
        $repository = new B2cPrdprdMarketplaceRepository($entityManager, $classMap);

        $this->assertNotNull($repository->getProductStock(1, 1));
    }

    /**
     * @test
     */
    public function updateProductStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('where')->andReturn($entityManager);
        $entityManager->shouldReceive('andWhere')->andReturn($entityManager);
        $entityManager->shouldReceive('setParameter')->andReturn($entityManager);
        $entityManager->shouldReceive('execute')->andReturn($entityManager);
        $entityManager->shouldReceive('set')->andReturn($entityManager);
        $entityManager->shouldReceive('update')->andReturn($entityManager);
        $entityManager->shouldReceive('expr')->andReturn($entityManager);
        $entityManager->shouldReceive('literal')->andReturn($entityManager);

        $classMap = new ClassMetadata(B2cPrdprdMarketplace::class);
        $repository = new B2cPrdprdMarketplaceRepository($entityManager, $classMap);

        $this->assertNotNull($repository->updateProductStock(1, 1, 1));
    }
}
