<?php

namespace Tests\Unit\AppBundle\Repository;

use AppBundle\Entity\B2cPedlog;
use AppBundle\Entity\B2cPedsituacao;
use AppBundle\Entity\B2cStaint;
use AppBundle\Entity\VO\Create\CreateOrderLogVO;
use AppBundle\Entity\VO\OrderStatusVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use AppBundle\Repository\B2cPedlogRepository;
use DateTime;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class B2cPedlogRepositoryTest
 * @package Tests\Unit\AppBundle\Repository
 *
 * @group Unit
 * @group Repository
 */
class B2cPedlogRepositoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function insertUpdateStatusLog()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cPedsituacao(), new B2cStaint());

        $classMap = new ClassMetadata(B2cPedlog::class);
        $repository = new B2cPedlogRepository($entityManager, $classMap);

        $updateOrderStatusVO =new UpdateOrderStatusVO();
        $updateOrderStatusVO->setStatus(1);
        $updateOrderStatusVO->setInternalStatus(1);

        $this->assertNotNull($repository->insertUpdateStatusLog($updateOrderStatusVO));
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function insertUpdateStatusLogException()
    {
        $entityManager = $this->getEntityManager();

        $classMap = new ClassMetadata(B2cPedlog::class);
        $repository = new B2cPedlogRepository($entityManager, $classMap);

        $updateOrderStatusVO =new UpdateOrderStatusVO();
        $updateOrderStatusVO->setStatus(1);
        $updateOrderStatusVO->setInternalStatus(1);

        $repository->insertUpdateStatusLog($updateOrderStatusVO);
    }

    /**
     * @test
     */
    public function insertOrderDeadlineLog()
    {
        $entityManager = $this->getEntityManager();

        $classMap = new ClassMetadata(B2cPedlog::class);
        $repository = new B2cPedlogRepository($entityManager, $classMap);

        $orderVO = new OrderVO();
        $orderVO->setStatus(new OrderStatusVO());
        $orderVO->setInternalStatus(new OrderStatusVO());

        $orderDeliveryDate = new DateTime();
        $productsDeadlines = [['name' => 'teste']];

        $this->assertNotNull($repository->insertOrderDeadlineLog($orderVO, $orderDeliveryDate, $productsDeadlines));
    }

    /**
     * @test
     */
    public function insertOrderLog()
    {
        $entityManager = $this->getEntityManager();

        $classMap = new ClassMetadata(B2cPedlog::class);
        $repository = new B2cPedlogRepository($entityManager, $classMap);

        $createOrderLogVO = new CreateOrderLogVO();

        $this->assertNotNull($repository->insertOrderLog($createOrderLogVO));
    }
}
