<?php

namespace Unit\AppBundle\DQL;

use AppBundle\DQL\GroupConcat;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Parser;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class GroupConcatTest
 * @package Unit\AppBundle\DQL
 *
 * @group Unit
 * @group DQL
 */
class GroupConcatTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function checkParser()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConfiguration')->andReturn($entityManager);
        $entityManager->shouldReceive('getDefaultQueryHints')->andReturn($entityManager);
        $entityManager->shouldReceive('isSecondLevelCacheEnabled')->andReturn($entityManager);
        $entityManager->shouldReceive('getSecondLevelCacheConfiguration')->andReturn($entityManager);
        $entityManager->shouldReceive('getCacheLogger')->andReturn($entityManager);

        $groupConcat = new GroupConcat('test');

        $queryString = 'SELECT * FROM AppBundle:B2cPedped ORDER BY id DESC LIMIT 1';
        $query = new Query($entityManager);
        $query->setDQL($queryString);

        $groupConcat->parse(new Parser($query));
    }
}
