<?php

namespace Unit\AppBundle\DQL;

use AppBundle\DQL\GroupConcat;
use AppBundle\DQL\IfNull;
use AppBundle\DQL\Replace;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Parser;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class ReplaceTest
 * @package Unit\AppBundle\DQL
 *
 * @group Unit
 * @group DQL
 */
class ReplaceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function checkParser()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConfiguration')->andReturn($entityManager);
        $entityManager->shouldReceive('getDefaultQueryHints')->andReturn($entityManager);
        $entityManager->shouldReceive('isSecondLevelCacheEnabled')->andReturn($entityManager);
        $entityManager->shouldReceive('getSecondLevelCacheConfiguration')->andReturn($entityManager);
        $entityManager->shouldReceive('getCacheLogger')->andReturn($entityManager);

        $replace = new Replace('test');

        $queryString = 'SELECT COUNT(id), CONCAT(test.id, test.id), test.id FROM AppBundle:B2cPedped ORDER BY id DESC LIMIT 1';
        $query = new Query($entityManager);
        $query->setDQL($queryString);

        $replace->parse(new Parser($query));
    }
}
