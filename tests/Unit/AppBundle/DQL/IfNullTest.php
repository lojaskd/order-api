<?php

namespace Unit\AppBundle\DQL;

use AppBundle\DQL\IfNull;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Parser;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class IfNullTest
 * @package Unit\AppBundle\DQL
 *
 * @group Unit
 * @group DQL
 */
class IfNullTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function checkParser()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConfiguration')->andReturn($entityManager);
        $entityManager->shouldReceive('getDefaultQueryHints')->andReturn($entityManager);
        $entityManager->shouldReceive('isSecondLevelCacheEnabled')->andReturn($entityManager);
        $entityManager->shouldReceive('getSecondLevelCacheConfiguration')->andReturn($entityManager);
        $entityManager->shouldReceive('getCacheLogger')->andReturn($entityManager);

        $ifNull = new IfNull('test');

        $queryString = 'SELECT id FROM AppBundle:B2cPedped ORDER BY id DESC LIMIT 1';
        $query = new Query($entityManager);
        $query->setDQL($queryString);

        $ifNull->parse(new Parser($query));
    }
}
