<?php

namespace Tests\Unit\AppBundle\DataBuilder;

use AppBundle\Entity\VO\Create\CreateAddressVO;
use AppBundle\Entity\VO\Create\CreateCouponVO;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
use AppBundle\Entity\VO\Create\CreateMarketPlaceVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreateOriginVO;
use AppBundle\Entity\VO\Create\CreatePaymentVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\Create\CreateRegionVO;
use AppBundle\Entity\VO\Create\CreateShippingVO;
use Zend\Hydrator\ClassMethods;

/**
 * Class CreateOrderVOBuilder
 * @package Tests\AppBundle\DataBuilder
 *
 */
class CreateOrderVODataBuilder
{
    /**
     * @return CreateOrderVO
     */
    public function build()
    {
        $hydrator = new ClassMethods(false);

        $createRegionVO = new CreateRegionVO();
        $createRegionVO->setId(1);

        $createCustomerVO = new CreateCustomerVO();
        $createCustomerVO->setId(1);

        $createPaymentVO = new CreatePaymentVO();
        $createPaymentVO->setId(1);

        $createProductVOData = [
            'category' => 1,
            'relatedCategories' => [2],
            'quantity' => 2
        ];
        $createProductVO = new CreateProductVO();
        $hydrator->hydrate($createProductVOData, $createProductVO);

        $couponData = [
            'freeShipping' => true
        ];
        $coupon = new CreateCouponVO();
        $hydrator->hydrate($couponData, $coupon);

        $shipping = new CreateShippingVO();
        $shipping->setRuleFreight('fretinho');

        $createOriginVO = new CreateOriginVO();
        $createOriginVO->setSource('direct');
        $createOriginVO->setMedium('direct');
        $createOriginVO->setContent('direct');

        $createOrderVOData = [
            'region' => $createRegionVO,
            'products' => [$createProductVO],
            'customer' => $createCustomerVO,
            'firstPayment' => $createPaymentVO,
            'coupon' => $coupon,
            'shipping' => $shipping,
            'origin' => $createOriginVO,
            'deliveryAddress' => new CreateAddressVO(),
            'billingAddress' => new CreateAddressVO(),
            'secondPayment' => $createPaymentVO,
            'marketPlace' => new CreateMarketPlaceVO(),
            'assistence' => true
        ];
        $createOrderVO = new CreateOrderVO();
        $hydrator->hydrate($createOrderVOData, $createOrderVO);

        return $createOrderVO;
    }
}
