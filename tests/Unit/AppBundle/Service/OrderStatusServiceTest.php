<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\B2cContaContabilPagamento;
use AppBundle\Entity\B2cPedpedMarketplace;
use AppBundle\Entity\B2cPedstatus;
use AppBundle\Entity\VO\OrderPaymentVO;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderStatusVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use AppBundle\Service\EventService;
use AppBundle\Service\OrderDateService;
use AppBundle\Service\OrderStatusService;
use AppBundle\Service\ProductService;
use DateTime;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class OrderStatusServiceTest
 * @package Tests\Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class OrderStatusServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     */
    public function updateStatus()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getDateAddNextBusinessDay')->andReturn(date('Y-m-d'));
        $entityManager->shouldReceive('updateOrderStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderInternalStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('insertUpdateStatusLog')->andReturn($entityManager);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('updateOrderDeadlines')->andReturn(true);

        $productService = Mockery::mock(ProductService::class);

        $orderStatusService = new OrderStatusService(
            $entityManager,
            $this->getLogger(),
            $eventService,
            $orderDateService,
            $productService
        );

        $updateOrderStatusVO = new UpdateOrderStatusVO();
        $updateOrderStatusVO->setStatus(B2cPedstatus::UNDER_APPROVAL);

        $orderStatusVO = new OrderStatusVO();
        $orderStatusVO->setId(B2cPedstatus::CREATED);

        $orderVO = new OrderVO();
        $orderVO->setStatus($orderStatusVO);
        $orderVO->setInternalStatus($orderStatusVO);

        $result = $orderStatusService->updateStatus($updateOrderStatusVO, $orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function updateStatusApproved()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getDateAddNextBusinessDay')->andReturn(date('Y-m-d'));
        $entityManager->shouldReceive('updateOrderStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderInternalStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('insertUpdateStatusLog')->andReturn($entityManager);
        $entityManager->shouldReceive('updateBillingAccount')->andReturn($entityManager);
        $entityManager->shouldReceive('updateSecondPaymentBillingAccount')->andReturn($entityManager);
        $entityManager->shouldReceive('setDestination')->andReturn($entityManager);
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cPedpedMarketplace());
        $entityManager->shouldReceive('getAccountingMarketPlace')->andReturn(null);
        $entityManager->shouldReceive('getDestinationBySupplierAndRegionName')->andReturn('teste');
        $entityManager->shouldReceive('getAccounting')->andReturn([
            'codigo' => '12345',
            'cardcode' => '123456'
        ]);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('updateOrderDeadlines')->andReturn(true);

        $productService = Mockery::mock(ProductService::class);

        $orderStatusService = new OrderStatusService(
            $entityManager,
            $this->getLogger(),
            $eventService,
            $orderDateService,
            $productService
        );

        $updateOrderStatusVO = new UpdateOrderStatusVO();
        $updateOrderStatusVO->setStatus(B2cPedstatus::APPROVED);

        $orderStatusVO = new OrderStatusVO();
        $orderStatusVO->setId(B2cPedstatus::CREATED);

        $orderVO = new OrderVO();
        $orderVO->setStatus($orderStatusVO);
        $orderVO->setInternalStatus($orderStatusVO);
        $orderVO->setFirstPayment(new OrderPaymentVO());

        $secondPayment = new OrderPaymentVO();
        $secondPayment->setId(1);
        $orderVO->setSecondPayment($secondPayment);

        $orderVO->setProducts([new OrderProductVO()]);
        $orderVO->setMarketplaceId(1);

        $result = $orderStatusService->updateStatus($updateOrderStatusVO, $orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function updateStatusDefaultStatus()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getDateAddNextBusinessDay')->andReturn(date('Y-m-d'));
        $entityManager->shouldReceive('updateOrderStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderInternalStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('insertUpdateStatusLog')->andReturn($entityManager);
        $entityManager->shouldReceive('updateBillingAccount')->andReturn($entityManager);
        $entityManager->shouldReceive('setDestination')->andReturn($entityManager);
        $entityManager->shouldReceive('updateDate')->andReturn($entityManager);
        $entityManager->shouldReceive('getDestinationBySupplierAndRegionName')->andReturn('teste');
        $entityManager->shouldReceive('getAccounting')->andReturn([
            'codigo' => '12345',
            'cardcode' => '123456'
        ]);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('updateOrderDeadlines')->andReturn(true);

        $productService = Mockery::mock(ProductService::class);

        $orderStatusService = new OrderStatusService(
            $entityManager,
            $this->getLogger(),
            $eventService,
            $orderDateService,
            $productService
        );

        $updateOrderStatusVO = new UpdateOrderStatusVO();
        $updateOrderStatusVO->setStatus(B2cPedstatus::CREATED);
        $updateOrderStatusVO->setDate(new DateTime());

        $orderStatusVO = new OrderStatusVO();
        $orderStatusVO->setId(B2cPedstatus::DELIVERED);

        $orderVO = new OrderVO();
        $orderVO->setStatus($orderStatusVO);
        $orderVO->setInternalStatus($orderStatusVO);
        $orderVO->setFirstPayment(new OrderPaymentVO());
        $orderVO->setSecondPayment(new OrderPaymentVO());
        $orderVO->setProducts([new OrderProductVO()]);

        $result = $orderStatusService->updateStatus($updateOrderStatusVO, $orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function updateStatusException()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getDateAddNextBusinessDay')->andReturn(date('Y-m-d'));
        $entityManager->shouldReceive('updateOrderInternalStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('insertUpdateStatusLog')->andReturn($entityManager);
        $entityManager->shouldReceive('updateBillingAccount')->andReturn($entityManager);
        $entityManager->shouldReceive('updateDate')->andReturn($entityManager);
        $entityManager->shouldReceive('getDestinationBySupplierAndRegionName')->andReturn('teste');
        $entityManager->shouldReceive('getAccounting')->andReturn([
            'codigo' => '12345',
            'cardcode' => '123456'
        ]);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('updateOrderDeadlines')->andReturn(true);

        $productService = Mockery::mock(ProductService::class);

        $orderStatusService = new OrderStatusService(
            $entityManager,
            $this->getLogger(),
            $eventService,
            $orderDateService,
            $productService
        );

        $updateOrderStatusVO = new UpdateOrderStatusVO();
        $updateOrderStatusVO->setStatus(B2cPedstatus::CREATED);
        $updateOrderStatusVO->setDate(new DateTime());

        $orderStatusVO = new OrderStatusVO();
        $orderStatusVO->setId(B2cPedstatus::DELIVERED);

        $orderVO = new OrderVO();
        $orderVO->setStatus($orderStatusVO);
        $orderVO->setInternalStatus($orderStatusVO);
        $orderVO->setFirstPayment(new OrderPaymentVO());
        $orderVO->setSecondPayment(new OrderPaymentVO());
        $orderVO->setProducts([new OrderProductVO()]);

        $orderStatusService->updateStatus($updateOrderStatusVO, $orderVO);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function autoUpdateDatesAndStatusException()
    {
        $entityManager = $this->getEntityManager();
        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('updateOrderDeadlines')->andReturn(true);

        $productService = Mockery::mock(ProductService::class);

        $orderStatusService = new OrderStatusService(
            $entityManager,
            $this->getLogger(),
            $eventService,
            $orderDateService,
            $productService
        );

        $orderStatusVO = new OrderStatusVO();
        $orderStatusVO->setId(B2cPedstatus::CREATED);

        $orderVO = new OrderVO();
        $orderVO->setStatus($orderStatusVO);
        $orderVO->setInternalStatus($orderStatusVO);

        $orderStatusService->autoUpdateDatesAndStatus($orderVO);
    }

    /**
     * @test
     */
    public function autoUpdateDatesAndStatusForCancelledProducts()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getProductsDatesByStatus')->andReturn([1, 1, 1, 1]);
        $entityManager->shouldReceive('updateOrderStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderInternalStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('insertUpdateStatusLog')->andReturn($entityManager);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('updateOrderDeadlines')->andReturn(true);

        $productService = Mockery::mock(ProductService::class);

        $orderStatusService = new OrderStatusService(
            $entityManager,
            $this->getLogger(),
            $eventService,
            $orderDateService,
            $productService
        );

        $orderStatusVO = new OrderStatusVO();
        $orderStatusVO->setId(B2cPedstatus::CREATED);

        $orderVO = new OrderVO();
        $orderVO->setStatus($orderStatusVO);
        $orderVO->setInternalStatus($orderStatusVO);

        $result = $orderStatusService->autoUpdateDatesAndStatus($orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function autoUpdateDatesAndStatusAllDates()
    {
        $arrDates = [
            'approvalDate' => [
                'field' => 'dataAprR',
                'dates' => [new DateTime()],
            ],
            'releaseDate' => [
                'field' => 'dataLibR',
                'dates' => [new DateTime()],
            ],
            'supplierBillingDate' => [
                'field' => 'dataFatFornR',
                'dates' => [new DateTime()],
            ],
            'supplierInvoiceDate' => [
                'field' => 'dataEntdFornR',
                'dates' => [new DateTime()],
            ],
            'supplierDeliveryDate' => [
                'field' => 'dataEntFornR',
                'dates' => [new DateTime()],
            ],
            'invoiceDate' => [
                'field' => 'dataNfR',
                'dates' => [new DateTime()],
            ],
            'shippingDate' => [
                'field' => 'dataEmbR',
                'dates' => [new DateTime()],
            ],
            'deliveryDate' => [
                'field' => 'dataEntR',
                'dates' => [new DateTime()],
            ]
        ];

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getProductsDatesByStatus')->andReturn([1, 1, 0, $arrDates]);
        $entityManager->shouldReceive('updateOrderStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderInternalStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('insertUpdateStatusLog')->andReturn($entityManager);
        $entityManager->shouldReceive('updateDate')->andReturn($entityManager);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('updateOrderDeadlines')->andReturn(true);

        $productService = Mockery::mock(ProductService::class);

        $orderStatusService = new OrderStatusService(
            $entityManager,
            $this->getLogger(),
            $eventService,
            $orderDateService,
            $productService
        );

        $orderStatusVO = new OrderStatusVO();
        $orderStatusVO->setId(B2cPedstatus::CREATED);

        $orderVO = new OrderVO();
        $orderVO->setStatus($orderStatusVO);
        $orderVO->setInternalStatus($orderStatusVO);

        $result = $orderStatusService->autoUpdateDatesAndStatus($orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function autoUpdateDatesAndStatus()
    {
        $arrDates = [
            'approvalDate' => [
                'field' => 'dataAprR',
                'dates' => [new DateTime()],
                'all_with_cancel' => [
                    'status' => 1
                ]
            ],
            'releaseDate' => [
                'field' => 'dataLibR',
                'dates' => [new DateTime(), new DateTime()],
                'all_with_cancel' => [
                    'status' => 1
                ]
            ],
            'supplierBillingDate' => [
                'field' => 'dataFatFornR',
                'dates' => [new DateTime(), new DateTime()],
                'all_with_cancel' => [
                    'status' => 1
                ]
            ],
            'supplierInvoiceDate' => [
                'field' => 'dataEntdFornR',
                'dates' => [new DateTime()],
            ],
            'supplierDeliveryDate' => [
                'field' => 'dataEntFornR',
                'dates' => [new DateTime(), new DateTime()],
                'all_with_cancel' => [
                    'status' => 1
                ]
            ],
            'invoiceDate' => [
                'field' => 'dataNfR',
                'dates' => [new DateTime(), new DateTime()],
                'all_with_cancel' => [
                    'status' => 1
                ]
            ],
            'shippingDate' => [
                'field' => 'dataEmbR',
                'dates' => [new DateTime(), new DateTime()],
                'all_with_cancel' => [
                    'status' => 1
                ]
            ],
            'deliveryDate' => [
                'field' => 'dataEntR',
                'dates' => [new DateTime(), new DateTime()],
                'all_with_cancel' => [
                    'status' => 1
                ]
            ]
        ];

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getProductsDatesByStatus')->andReturn([2, 1, 1, $arrDates]);
        $entityManager->shouldReceive('updateOrderStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderInternalStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('insertUpdateStatusLog')->andReturn($entityManager);
        $entityManager->shouldReceive('updateDate')->andReturn($entityManager);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('updateOrderDeadlines')->andReturn(true);

        $productService = Mockery::mock(ProductService::class);

        $orderStatusService = new OrderStatusService(
            $entityManager,
            $this->getLogger(),
            $eventService,
            $orderDateService,
            $productService
        );

        $orderStatusVO = new OrderStatusVO();
        $orderStatusVO->setId(B2cPedstatus::CREATED);

        $orderVO = new OrderVO();
        $orderVO->setStatus($orderStatusVO);
        $orderVO->setInternalStatus($orderStatusVO);

        $result = $orderStatusService->autoUpdateDatesAndStatus($orderVO);

        $this->assertTrue($result);
    }

}
