<?php

namespace Tests\Unit\AppBundle\Service\Event\Factories;

use AppBundle\Service\Event\Factories\EventRabbitFactory;
use PHPUnit_Framework_TestCase;

/**
 * Class EventRabbitFactoryTest
 * @package Tests\Unit\AppBundle\Service\Event\Factories
 *
 * @group Unit
 * @group Service
 */
class EventRabbitFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @expectedException \Exception
     */
    public function send()
    {

        $event = new EventRabbitFactory(
            'http://test.com.br',
            '2222',
            'test',
            'test',
            '/',
            ''
        );

        $result = $event->send('test', ['ytes' => 'est']);

        $this->assertTrue($result);
    }
}
