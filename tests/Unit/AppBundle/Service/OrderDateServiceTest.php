<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\OrderAddressVO;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Service\Deadline\DeadlineService;
use AppBundle\Service\EventService;
use AppBundle\Service\OrderDateService;
use AppBundle\Service\ProductService;
use DateTime;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class OrderDateServiceTest
 * @package Tests\Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class OrderDateServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     */
    public function calculateProductDates()
    {
        $entityManager = $this->getEntityManager();
        $productService = Mockery::mock(ProductService::class);
        $eventService = Mockery::mock(EventService::class);
        $deadLineService = Mockery::mock(DeadlineService::class);
        $deadLineService->shouldReceive('calculate')->andReturn([]);

        $orderDateService = new OrderDateService(
            $entityManager,
            $this->getLogger(),
            $productService,
            $eventService,
            $deadLineService
        );

        $result = $orderDateService->calculateProductDates(1, date('Y-m-d'), '82900270', 1, true, true);

        $this->assertNotNull($result);
    }

    /**
     * @test
     * @covers \AppBundle\Service\OrderDateService::updateOrderDeadlines()
     */
    public function updateOrderDeadlines()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setNewDeadline')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderDeadline')->andReturn($entityManager);
        $entityManager->shouldReceive('updateDate')->andReturn($entityManager);
        $entityManager->shouldReceive('insertOrderDeadlineLog')->andReturn($entityManager);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('updateDate')->andReturn($productService);

        $eventService = Mockery::mock(EventService::class);
        $deadLineService = Mockery::mock(DeadlineService::class);
        $deadLineService->shouldReceive('calculate')->andReturn([
            'deadlines' => [],
            'providerDeadLine' => 10,
            'availabilityDeadline' => 5,
            'predictDates' => [
                'releaseDate' => new DateTime(),
                'supplierBillingDate' => new DateTime(),
                'supplierInvoiceDate' => new DateTime(),
                'supplierDeliveryDate' => new DateTime(),
                'invoiceDate' => new DateTime(),
                'shippingDate' => new DateTime(),
                'deliveryDate' => new DateTime()
            ]
        ]);

        $orderDateService = new OrderDateService(
            $entityManager,
            $this->getLogger(),
            $productService,
            $eventService,
            $deadLineService
        );

        $productVo = new OrderProductVO();

        $orderVO = new OrderVO();
        $orderVO->setProducts([$productVo]);
        $orderVO->setDeliveryAddress(new OrderAddressVO());

        $result = $orderDateService->updateOrderDeadlines($orderVO, date('Y-m-d'));

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function createOrderDeadline()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('setNewDeadline')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderDeadline')->andReturn($entityManager);
        $entityManager->shouldReceive('updateDate')->andReturn($entityManager);
        $entityManager->shouldReceive('insertOrderDeadlineLog')->andReturn($entityManager);
        $entityManager->shouldReceive('getDateAddNextBusinessDay')->andReturn(date('Y-m-d'));
        $entityManager->shouldReceive('findOneBy')->andReturn(null);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('updateDate')->andReturn($productService);

        $eventService = Mockery::mock(EventService::class);
        $deadLineService = Mockery::mock(DeadlineService::class);
        $deadLineService->shouldReceive('calculate')->andReturn([
            'deadlines' => [],
            'providerDeadLine' => 10,
            'availabilityDeadline' => 5,
            'predictDates' => [
                'releaseDate' => new DateTime(),
                'supplierBillingDate' => new DateTime(),
                'supplierInvoiceDate' => new DateTime(),
                'supplierDeliveryDate' => new DateTime(),
                'invoiceDate' => new DateTime(),
                'shippingDate' => new DateTime(),
                'deliveryDate' => new DateTime()
            ]
        ]);

        $orderDateService = new OrderDateService(
            $entityManager,
            $this->getLogger(),
            $productService,
            $eventService,
            $deadLineService
        );

        $productVo = new CreateProductVO();

        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setProducts([$productVo]);

        $result = $orderDateService->createOrderDeadline($createOrderVO);

        $this->assertTrue($result);
    }
}
