<?php

namespace Tests\Unit\AppBundle\Service;

use Mockery;
use Symfony\Bridge\Monolog\Logger;

/**
 * Trait LoggerTestTrait
 * @package Unit\AppBundle\Service
 */
trait LoggerTestTrait
{
    public function getLogger()
    {
        $logger = Mockery::mock(Logger::class);
        $logger->shouldReceive('info')->andReturn($logger);
        $logger->shouldReceive('alert')->andReturn($logger);
        $logger->shouldReceive('err')->andReturn($logger);
        $logger->shouldReceive('error')->andReturn($logger);
        $logger->shouldReceive('warning')->andReturn($logger);
        $logger->shouldReceive('critical')->andReturn($logger);
        $logger->shouldReceive('debug')->andReturn($logger);

        return $logger;
    }
}
