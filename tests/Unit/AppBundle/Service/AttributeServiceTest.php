<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\AttributeJson;
use AppBundle\Entity\Attributes;
use AppBundle\Entity\OrderAttributes;
use AppBundle\Service\AttributeService;
use Doctrine\ORM\EntityManager;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class AttributeServiceTest
 * @package Tests\Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class AttributeServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     */
    public function updateAttributeOrderWithStatus4()
    {
        $attributeJson = new AttributeJson();
        $attributeJson->setIdAttr(1);
        $attributeJson->setValue(['test' => 'test']);

        $attributes = new Attributes();
        $attributes->setAttrType(4);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('find')->andReturn($attributes);

        $orderAttributes = new OrderAttributes();
        $entityManager->shouldReceive('findBy')->andReturn([$orderAttributes]);

        $attributeService = new AttributeService($entityManager, $this->getLogger());

        $result = $attributeService->updateAttributeOrder(1, $attributeJson);

        $this->assertTrue($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function updateAttributeException()
    {
        $orderAttributes = new OrderAttributes();
        $attributes = new Attributes();
        $attributes->setAttrType(4);

        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('getRepository')->andReturn($entityManager);
        $entityManager->shouldReceive('findBy')->andReturn([$orderAttributes]);
        $entityManager->shouldReceive('find')->andReturn($attributes);

        $attributeJson = new AttributeJson();
        $attributeJson->setIdAttr(1);
        $attributeJson->setValue(['test', 'test2']);
        $attributeService = new AttributeService($entityManager, $this->getLogger());
        $attributeService->updateAttributeOrder(1, $attributeJson);
    }

    /**
     * @test
     */
    public function updateAttributeOrder()
    {
        $attributeJson = new AttributeJson();
        $attributeJson->setIdAttr(1);
        $attributeJson->setValue(['test', 'test2']);

        $attributes = new Attributes();
        $attributes->setAttrType(2);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('find')->andReturn($attributes);

        $orderAttributes = new OrderAttributes();
        $entityManager->shouldReceive('findBy')->andReturn([$orderAttributes]);
        $entityManager->shouldReceive('findOneBy')->andReturn($orderAttributes);

        $attributeService = new AttributeService($entityManager, $this->getLogger());

        $result = $attributeService->updateAttributeOrder(1, $attributeJson);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function updateAttributeOrderWithoutOrderAttribute()
    {
        $attributeJson = new AttributeJson();
        $attributeJson->setIdAttr(1);
        $attributeJson->setValue(['test', 'test2']);

        $attributes = new Attributes();
        $attributes->setAttrType(2);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('find')->andReturn($attributes);

        $orderAttributes = new OrderAttributes();
        $entityManager->shouldReceive('findBy')->andReturn([$orderAttributes]);
        $entityManager->shouldReceive('findOneBy')->andReturn(null);

        $attributeService = new AttributeService($entityManager, $this->getLogger());

        $result = $attributeService->updateAttributeOrder(1, $attributeJson);

        $this->assertTrue($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function updateExceptioninAttributeOrderWithoutOrderAttribute()
    {
        $orderAttributes = new OrderAttributes();
        $attributes = new Attributes();
        $attributes->setAttrType(2);

        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('getRepository')->andReturn($entityManager);
        $entityManager->shouldReceive('findBy')->andReturn([$orderAttributes]);
        $entityManager->shouldReceive('findOneBy')->andReturn($orderAttributes);
        $entityManager->shouldReceive('find')->andReturn($attributes);

        $attributeJson = new AttributeJson();
        $attributeJson->setIdAttr(1);
        $attributeJson->setValue(['test', 'test2']);
        $attributeService = new AttributeService($entityManager, $this->getLogger());
        $attributeService->updateAttributeOrder(1, $attributeJson);
    }
}
