<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\B2cClicli;
use AppBundle\Entity\Validate\ValidateCoupon;
use AppBundle\Entity\Validate\ValidateOrder;
use AppBundle\Entity\Validate\ValidateShipping;
use AppBundle\Entity\VO\Create\CreateCouponVO;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreatePaymentVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\Create\CreateShippingVO;
use AppBundle\Service\CouponService;
use AppBundle\Service\CustomerService;
use AppBundle\Service\LojasKdService;
use AppBundle\Service\ProductService;
use AppBundle\Service\ShippingService;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class LojasKdServiceTest
 * @package Tests\Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class LojasKdServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     * @covers \AppBundle\Service\LojasKdService::getLojasKdData()
     * @covers \AppBundle\Service\LojasKdService::__construct()
     */
    public function getLojasKdData()
    {
        $createCouponVO = new CreateCouponVO();
        $createCouponVO->setCode('12345');
        $createCouponVO->setFreeShipping(true);

        $createShippingVO = new CreateShippingVO();

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getCustomerById')->andReturn(new B2cClicli());
        $entityManager->shouldReceive('getActiveDiscountPromotion')->andReturn([
            'descontoValor' => 1.00,
            'descontoPerc' => 1
        ]);

        $couponService = Mockery::mock(CouponService::class);
        $couponService->shouldReceive('validateCoupon')->andReturn($createCouponVO);

        $shippingService = Mockery::mock(ShippingService::class);
        $shippingService->shouldReceive('calculateShipping')->andReturn($shippingService);
        $shippingService->shouldReceive('calculateCouponShipping')->andReturn($createShippingVO);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('validateProductStock')->andReturn(true);

        $customerService = Mockery::mock(CustomerService::class);
        $customerService->shouldReceive('getOrCreateCustomer')->andReturn(new CreateCustomerVO());

        $lojasKdService = new LojasKdService(
            $entityManager,
            $couponService,
            $shippingService,
            $productService,
            $customerService,
            $this->getLogger()
        );

        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setCoupon($createCouponVO);
        $createOrderVO->setProducts([new CreateProductVO()]);

        $createPaymentVO = new CreatePaymentVO();
        $createPaymentVO->setIndexer(2);
        $createOrderVO->setFirstPayment($createPaymentVO);

        $validateCoupon = new ValidateCoupon();
        $validateCoupon->setCode('1234');

        $validateOrder = new ValidateOrder();
        $validateOrder->setCoupon($validateCoupon);

        $result = $lojasKdService->getLojasKdData($createOrderVO, $validateOrder);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('orderVO', $result);
        $this->assertInstanceOf(CreateOrderVO::class, $result['orderVO']);
    }

    /**
     * @test
     * @covers \AppBundle\Service\LojasKdService::getLojasKdData()
     * @covers \AppBundle\Service\LojasKdService::__construct()
     */
    public function getLojasKdDataEdgeCase()
    {
        $createCouponVO = new CreateCouponVO();
        $createCouponVO->setCode('12345');
        $createCouponVO->setFreeShipping(true);

        $createShippingVO = new CreateShippingVO();

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getCustomerById')->andReturn(new B2cClicli());
        $entityManager->shouldReceive('getActiveDiscountPromotion')->andReturn([
            'descontoValor' => 1.00,
            'descontoPerc' => 1
        ]);

        $couponService = Mockery::mock(CouponService::class);
        $couponService->shouldReceive('validateCoupon')->andReturn($createCouponVO);

        $shippingService = Mockery::mock(ShippingService::class);
        $shippingService->shouldReceive('calculateShipping')->andReturn($shippingService);
        $shippingService->shouldReceive('calculateCouponShipping')->andReturn($createShippingVO);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('validateProductStock')->andReturn(true);

        $customerService = Mockery::mock(CustomerService::class);

        $lojasKdService = new LojasKdService(
            $entityManager,
            $couponService,
            $shippingService,
            $productService,
            $customerService,
            $this->getLogger()
        );

        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setCoupon($createCouponVO);
        $createOrderVO->setProducts([new CreateProductVO()]);

        $createPaymentVO = new CreatePaymentVO();
        $createPaymentVO->setIndexer(2);
        $createOrderVO->setFirstPayment($createPaymentVO);

        $validateCoupon = new ValidateCoupon();
        $validateCoupon->setCode('1234');

        $validateOrder = new ValidateOrder();
        $validateOrder->setCoupon($validateCoupon);

        $result = $lojasKdService->getLojasKdData($createOrderVO, $validateOrder);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('orderVO', $result);
        $this->assertInstanceOf(CreateOrderVO::class, $result['orderVO']);
    }

    /**
     * @test
     */
    public function validateOrderValues()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getActiveDiscountPromotion')->andReturn([
            'descontoValor' => 1.00,
            'descontoPerc' => 1
        ]);

        $couponService = Mockery::mock(CouponService::class);
        $shippingService = Mockery::mock(ShippingService::class);
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('validateProductStock')->andReturn(true);
        $customerService = Mockery::mock(CustomerService::class);

        $lojasKdService = new LojasKdService(
            $entityManager,
            $couponService,
            $shippingService,
            $productService,
            $customerService,
            $this->getLogger()
        );

        $createCouponVO = new CreateCouponVO();
        $createCouponVO->setCode('12345');
        $createCouponVO->setFreeShipping(true);
        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setCoupon($createCouponVO);
        $createOrderVO->setProducts([new CreateProductVO()]);

        $createPaymentVO = new CreatePaymentVO();
        $createPaymentVO->setIndexer(2);
        $createOrderVO->setFirstPayment($createPaymentVO);

        $shippingCreateVO = new CreateShippingVO();
        $shippingCreateVO->setPrice(39.90);
        $createOrderVO->setShipping($shippingCreateVO);

        $validateCoupon = new ValidateCoupon();
        $validateCoupon->setCode('1234');

        $validateOrder = new ValidateOrder();
        $validateOrder->setCoupon($validateCoupon);

        $shipping = new ValidateShipping();
        $shipping->setPrice(39.90);
        $validateOrder->setShipping($shipping);

        $result = $lojasKdService->validateOrderValues($createOrderVO, $validateOrder);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function validateOrderValuesWithTwoPayments()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getActiveDiscountPromotion')->andReturn([
            'descontoValor' => 1.00,
            'descontoPerc' => 1
        ]);

        $couponService = Mockery::mock(CouponService::class);
        $shippingService = Mockery::mock(ShippingService::class);
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('validateProductStock')->andReturn(true);
        $customerService = Mockery::mock(CustomerService::class);

        $lojasKdService = new LojasKdService(
            $entityManager,
            $couponService,
            $shippingService,
            $productService,
            $customerService,
            $this->getLogger()
        );

        $createCouponVO = new CreateCouponVO();
        $createCouponVO->setCode('12345');
        $createCouponVO->setFreeShipping(true);
        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setCoupon($createCouponVO);
        $createOrderVO->setProducts([new CreateProductVO()]);

        $createPaymentVO = new CreatePaymentVO();
        $createPaymentVO->setIndexer(2);
        $createOrderVO->setFirstPayment($createPaymentVO);
        $createOrderVO->setSecondPayment($createPaymentVO);

        $shippingCreateVO = new CreateShippingVO();
        $shippingCreateVO->setPrice(39.90);
        $createOrderVO->setShipping($shippingCreateVO);

        $validateCoupon = new ValidateCoupon();
        $validateCoupon->setCode('1234');

        $validateOrder = new ValidateOrder();
        $validateOrder->setCoupon($validateCoupon);

        $shipping = new ValidateShipping();
        $shipping->setPrice(39.90);
        $validateOrder->setShipping($shipping);

        $result = $lojasKdService->validateOrderValues($createOrderVO, $validateOrder);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function validateOrderValuesEdgeCasesWithOnePayment()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getActiveDiscountPromotion')->andReturn([
            'descontoValor' => 0,
            'descontoPerc' => 1
        ]);

        $couponService = Mockery::mock(CouponService::class);
        $shippingService = Mockery::mock(ShippingService::class);
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('validateProductStock')->andReturn(false);
        $customerService = Mockery::mock(CustomerService::class);

        $lojasKdService = new LojasKdService(
            $entityManager,
            $couponService,
            $shippingService,
            $productService,
            $customerService,
            $this->getLogger()
        );

        $createCouponVO = new CreateCouponVO();
        $createCouponVO->setCode('12345');
        $createCouponVO->setFreeShipping(true);
        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setCoupon($createCouponVO);
        $createOrderVO->setProducts([new CreateProductVO()]);

        $createPaymentVO = new CreatePaymentVO();
        $createPaymentVO->setIndexer(2);
        $createOrderVO->setFirstPayment($createPaymentVO);
        $createOrderVO->setTotal(43.90);

        $shippingCreateVO = new CreateShippingVO();
        $shippingCreateVO->setPrice(38.90);
        $createOrderVO->setShipping($shippingCreateVO);

        $validateCoupon = new ValidateCoupon();
        $validateCoupon->setCode('1234');
        $validateCoupon->setValue(12.90);

        $validateOrder = new ValidateOrder();
        $validateOrder->setCoupon($validateCoupon);
        $validateOrder->setSubtotal(35.9);
        $validateOrder->setTotal(42.90);

        $shipping = new ValidateShipping();
        $shipping->setPrice(39.90);
        $validateOrder->setShipping($shipping);

        $result = $lojasKdService->validateOrderValues($createOrderVO, $validateOrder);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function validateOrderValuesEdgeCasesWithTwoPayments()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getActiveDiscountPromotion')->andReturn([
            'descontoValor' => 0,
            'descontoPerc' => 1
        ]);

        $couponService = Mockery::mock(CouponService::class);
        $shippingService = Mockery::mock(ShippingService::class);
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('validateProductStock')->andReturn(false);
        $customerService = Mockery::mock(CustomerService::class);

        $lojasKdService = new LojasKdService(
            $entityManager,
            $couponService,
            $shippingService,
            $productService,
            $customerService,
            $this->getLogger()
        );

        $createCouponVO = new CreateCouponVO();
        $createCouponVO->setCode('12345');
        $createCouponVO->setFreeShipping(true);
        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setCoupon($createCouponVO);
        $createOrderVO->setProducts([new CreateProductVO()]);

        $createPaymentVO = new CreatePaymentVO();
        $createPaymentVO->setIndexer(2);
        $createOrderVO->setFirstPayment($createPaymentVO);
        $createOrderVO->setSecondPayment($createPaymentVO);
        $createOrderVO->setTotal(43.90);

        $shippingCreateVO = new CreateShippingVO();
        $shippingCreateVO->setPrice(38.90);
        $createOrderVO->setShipping($shippingCreateVO);

        $validateCoupon = new ValidateCoupon();
        $validateCoupon->setCode('1234');
        $validateCoupon->setValue(12.90);

        $validateOrder = new ValidateOrder();
        $validateOrder->setCoupon($validateCoupon);
        $validateOrder->setSubtotal(35.9);
        $validateOrder->setTotal(42.90);

        $shipping = new ValidateShipping();
        $shipping->setPrice(39.90);
        $validateOrder->setShipping($shipping);

        $result = $lojasKdService->validateOrderValues($createOrderVO, $validateOrder);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function generateInterestPayment()
    {
        $entityManager = $this->getEntityManager();
        $couponService = Mockery::mock(CouponService::class);
        $shippingService = Mockery::mock(ShippingService::class);
        $productService = Mockery::mock(ProductService::class);
        $customerService = Mockery::mock(CustomerService::class);

        $lojasKdService = new LojasKdService(
            $entityManager,
            $couponService,
            $shippingService,
            $productService,
            $customerService,
            $this->getLogger()
        );

        $result = $lojasKdService->generateInterestPayment(100.00, 2, 2);

        $this->assertNotNull($result);
        $this->assertInternalType('float', $result);
    }
}
