<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Service\ProductAPIService;
use PHPUnit_Framework_TestCase;

/**
 * Class ProductAPIServiceTest
 * @package Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class ProductAPIServiceTest extends PHPUnit_Framework_TestCase
{
    use LoggerTestTrait;

    /**
     * @test
     */
    public function getDeadlines()
    {
        $productApiService = $this
            ->getMockBuilder(ProductAPIService::class)
            ->disableOriginalConstructor()
            ->setMethods(['call'])
            ->getMock();

        $productApiService->method('call')->willReturn($this->returnValue((object) []));

        $result = $productApiService->getDeadlines(110821, date('Y-m-d'), '82900270', 1, false, false);

        // TODO - After a better coverage, change the internal request from cURL to Guzzle or another requester
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getProductInfo()
    {
        $productApiService = $this
            ->getMockBuilder(ProductAPIService::class)
            ->disableOriginalConstructor()
            ->setMethods(['call'])
            ->getMock();

        $productApiService->method('call')->willReturn($this->returnValue((object) []));

        $result = $productApiService->getProductInfo(110821);

        // TODO - After a better coverage, change the internal request from cURL to Guzzle or another requester
        $this->assertNotNull($result);
    }
}
