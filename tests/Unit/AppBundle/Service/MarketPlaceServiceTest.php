<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\B2cClicli;
use AppBundle\Entity\Validate\ValidateMarketPlace;
use AppBundle\Entity\Validate\ValidateOrder;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\Create\CreateShippingVO;
use AppBundle\Service\CustomerService;
use AppBundle\Service\MarketPlaceService;
use AppBundle\Service\ProductService;
use AppBundle\Service\ShippingService;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class MarketPlaceServiceTest
 * @package Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class MarketPlaceServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     */
    public function getMarketPlaceData()
    {
        $marketPlaceEmail = 'mkplc@lojaskd.com.br';
        $createShippingVO = new CreateShippingVO();
        $createShippingVO->setId(1);

        $shippingService = Mockery::mock(ShippingService::class);
        $shippingService->shouldReceive('calculateShipping')->andReturn($createShippingVO);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('validateMarketPlaceProductStock')->andReturn(true);

        $customerService = Mockery::mock(CustomerService::class);
        $customerService->shouldReceive('getOrCreateCustomer')->andReturn(new CreateCustomerVO());

        $customer = new B2cClicli();

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getMarketPlaceCustomer')->andReturn($customer);

        $marketPlaceService = new MarketPlaceService(
            $entityManager,
            $marketPlaceEmail,
            $shippingService,
            $productService,
            $customerService,
            $this->getLogger()
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setTotalprice(222.9);

        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setProducts([$createProductVO]);
        $validateOrder = new ValidateOrder();
        $marketPlace = new ValidateMarketplace();
        $marketPlace->setOrder('12345');
        $validateOrder->setMarketplace($marketPlace);

        $result = $marketPlaceService->getMarketPlaceData($createOrderVO, $validateOrder);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function getMarketplaceAttrStock()
    {
        $marketPlaceEmail = 'mkplc@lojaskd.com.br';
        $shippingService = Mockery::mock(ShippingService::class);
        $productService = Mockery::mock(ProductService::class);
        $customerService = Mockery::mock(CustomerService::class);
        $entityManager = $this->getEntityManager();

        $marketPlaceService = new MarketPlaceService(
            $entityManager,
            $marketPlaceEmail,
            $shippingService,
            $productService,
            $customerService,
            $this->getLogger()
        );

        $b2w = $marketPlaceService->getMarketplaceAttrStock(MarketPlaceService::MKPAMERICANAS);
        $this->assertNotNull($b2w);
        $this->assertEquals(MarketPlaceService::ATTRSTOCKB2W, $b2w);

        $cnova = $marketPlaceService->getMarketplaceAttrStock(MarketPlaceService::MKPCASASBAHIA);
        $this->assertNotNull($cnova);
        $this->assertEquals(MarketPlaceService::ATTRSTOCKCNOVA, $cnova);

        $walmart = $marketPlaceService->getMarketplaceAttrStock(MarketPlaceService::MKPWALMART);
        $this->assertNotNull($walmart);
        $this->assertEquals(MarketPlaceService::ATTRSTOCKWALMART, $walmart);

        $mercadoLivre = $marketPlaceService->getMarketplaceAttrStock(MarketPlaceService::MKPMERCADOLIVRE);
        $this->assertNotNull($mercadoLivre);
        $this->assertEquals(MarketPlaceService::ATTRSTOCKMERCADOLIVRE, $mercadoLivre);

        $carrefour = $marketPlaceService->getMarketplaceAttrStock(MarketPlaceService::MKPCARREFOUR);
        $this->assertNotNull($carrefour);
        $this->assertEquals(MarketPlaceService::ATTRSTOCKCARREFOUR, $carrefour);

        $zoom = $marketPlaceService->getMarketplaceAttrStock(MarketPlaceService::MKPZOOM);
        $this->assertNotNull($zoom);
        $this->assertEquals(MarketPlaceService::ATTRSTOCKZOOM, $zoom);
    }
}
