<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\B2cPedprd;
use AppBundle\Entity\B2cPedprdstatus;
use AppBundle\Entity\Validate\Update\ValidateProviderBillingDate;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateProviderBillingDateVO;
use AppBundle\Service\EventService;
use AppBundle\Service\ProductService;
use AppBundle\Service\PurchaseOrderService;
use AppBundle\Service\QueueService;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class PurchaseOrderServiceTest
 * @package Tests\Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class PurchaseOrderServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     */
    public function getPurchaseOrderDateInfo()
    {
        $entityManager = $this->getEntityManager();
        $productService = Mockery::mock(ProductService::class);
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $providerBillingDate = new ValidateProviderBillingDate();

        $purchaseOrderService = new PurchaseOrderService(
            $entityManager,
            $this->getLogger(),
            $productService,
            $queueService,
            $eventService
        );
        $result = $purchaseOrderService->getPurchaseOrderDateInfo($providerBillingDate);

        $this->assertNotNull($result);
        $this->assertInstanceOf(UpdateProviderBillingDateVO::class, $result);
    }

    /**
     * @test
     */
    public function updatePurchaseOrderDate()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findBy')->andReturn([new B2cPedprd()]);
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cPedprdstatus());

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('updateDate')->andReturn(true);

        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $providerBillingDate = new UpdateProviderBillingDateVO();
        $orderVO = new OrderVO();

        $purchaseOrderService = new PurchaseOrderService(
            $entityManager,
            $this->getLogger(),
            $productService,
            $queueService,
            $eventService
        );
        $result = $purchaseOrderService->updatePurchaseOrderDate($providerBillingDate, $orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function updatePurchaseOrderDateException()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findBy')->andReturn(null);

        $productService = Mockery::mock(ProductService::class);
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $providerBillingDate = new UpdateProviderBillingDateVO();
        $orderVO = new OrderVO();

        $purchaseOrderService = new PurchaseOrderService(
            $entityManager,
            $this->getLogger(),
            $productService,
            $queueService,
            $eventService
        );
        $purchaseOrderService->updatePurchaseOrderDate($providerBillingDate, $orderVO);
    }
}
