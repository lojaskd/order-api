<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\B2cPagamentoCartao;
use AppBundle\Service\EventService;
use AppBundle\Service\OrderPaymentService;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class OrderPaymentServiceTest
 * @package Tests\Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class OrderPaymentServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     */
    public function getOrderPayment()
    {
        $eventService = Mockery::mock(EventService::class);
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findBy')->andReturn([new B2cPagamentoCartao()]);

        $orderPaymentService = new OrderPaymentService($entityManager, $this->getLogger(), $eventService);

        $result = $orderPaymentService->getOrderPayment(1);

        $this->assertNotNull($result);
        $this->assertInstanceOf(B2cPagamentoCartao::class, current($result));
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function getOrderPaymentException()
    {
        $eventService = Mockery::mock(EventService::class);
        $orderPaymentService = new OrderPaymentService($this->getEntityManager(), $this->getLogger(), $eventService);
        $orderPaymentService->getOrderPayment(1);
    }
}
