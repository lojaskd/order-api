<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\B2cPedped;
use AppBundle\Service\SearchService;
use PHPUnit_Framework_TestCase;

/**
 * Class SearchServiceTest
 * @package Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class SearchServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    private function getData()
    {
        $b2cPedped = new B2cPedped();
        return [$b2cPedped];
    }

    /**
     * @test
     */
    public function searchOrderFromTermAndTypeWithOrderAsParameter()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('searchFromOrderId')->andReturn($this->getData());

        $service = new SearchService($entityManager, $this->getLogger());

        $result = $service->searchOrderFromTermAndType(1, 'order');

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function searchOrderFromTermAndTypeWithInvoiceAsParameter()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('searchFromInvoice')->andReturn($this->getData());

        $service = new SearchService($entityManager, $this->getLogger());

        $result = $service->searchOrderFromTermAndType(1, 'invoice');

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function searchOrderFromTermAndTypeWithEmailAsParameter()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('searchFromEmail')->andReturn($this->getData());

        $service = new SearchService($entityManager, $this->getLogger());

        $result = $service->searchOrderFromTermAndType('ti@teste.com.br', 'email');

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function searchOrderFromTermAndTypeWithDocumentAsParameter()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('searchFromDocument')->andReturn($this->getData());

        $service = new SearchService($entityManager, $this->getLogger());

        $result = $service->searchOrderFromTermAndType('000.000.000-00', 'document');

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function searchOrderFromTermAndTypeWithPhoneAsParameter()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('searchFromPhoneNumber')->andReturn($this->getData());

        $service = new SearchService($entityManager, $this->getLogger());

        $result = $service->searchOrderFromTermAndType('(41) 4141-4141', 'phone');

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function searchOrderFromTermAndTypeWithMarketPlaceAsParameter()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('searchFromMarketPlaceOrder')->andReturn($this->getData());

        $service = new SearchService($entityManager, $this->getLogger());

        $result = $service->searchOrderFromTermAndType('xpto', 'marketplace');

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function searchOrderFromTermAndTypeWithNameAsParameter()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('searchFromCustomerName')->andReturn($this->getData());

        $service = new SearchService($entityManager, $this->getLogger());

        $result = $service->searchOrderFromTermAndType('TI Teste', 'name');

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function searchOrderFromTermAndTypeWithNonFilteredTermAsParameter()
    {
        $service = new SearchService($this->getEntityManager(), $this->getLogger());

        $result = $service->searchOrderFromTermAndType('Teste', 'last_name');

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
        $this->assertEmpty($result);
    }
}
