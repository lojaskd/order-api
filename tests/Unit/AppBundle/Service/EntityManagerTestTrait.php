<?php

namespace Tests\Unit\AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Mockery;

/**
 * Trait EntityManagerTestTrait
 * @package Unit\AppBundle\Service
 */
trait EntityManagerTestTrait
{
    public function getEntityManager()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('persist')->andReturn($entityManager);
        $entityManager->shouldReceive('remove')->andReturn($entityManager);
        $entityManager->shouldReceive('flush')->andReturn($entityManager);
        $entityManager->shouldReceive('getRepository')->andReturn($entityManager);
        $entityManager->shouldReceive('createQueryBuilder')->andReturn($entityManager);
        $entityManager->shouldReceive('select')->andReturn($entityManager);
        $entityManager->shouldReceive('from')->andReturn($entityManager);
        $entityManager->shouldReceive('getQuery')->andReturn($entityManager);
        return $entityManager;
    }
}
