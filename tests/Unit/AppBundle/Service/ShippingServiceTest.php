<?php

namespace Tests\Unit\AppBundle\Unit\Service;

use AppBundle\Entity\VO\Create\CreateShippingVO;
use AppBundle\Service\ShippingService;
use Doctrine\ORM\EntityManager;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\DataBuilder\CreateOrderVODataBuilder;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;
use Tests\Unit\AppBundle\Service\LoggerTestTrait;

/**
 * Class ShippingServiceTest
 * @package AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class ShippingServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    protected function getCustomEntityManager()
    {
        $productStockAndDeadline = [
            'deadline' => 12
        ];

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getMaxCubage')->andReturn(1);
        $entityManager->shouldReceive('getMaxWeight')->andReturn(1);
        $entityManager->shouldReceive('getProductStockAndDeadline')->andReturn($productStockAndDeadline);

        return $entityManager;
    }

    private function getB2CData()
    {
        return [
            'idRegreg' => 1,
            'idTratra' => 1234,
            'valorFreteFixo' => 0,
            'valor_frete_fixo' => 0,
            'tipo_frete' => 2,
            'tipoFrete' => 2,
            'seguro' => 6.55,
            'nomePublico' => 'Transportadora Teste',
            'padrao' => false,
            'prazoFreteGratuito' => 25,
            'prazoTransportadora' => 10,
            'prazoFreteFixo' => 12
        ];
    }

    private function getPromoData()
    {
        return [
            [
                'usarCategoria' => true,
                'idPromo' => 1,
                'freteFixo' => false,
                'freteFixoValor' => 65.90,
                'freteGratis' => false
            ]
        ];
    }

    private function prepareService($entityManager = null)
    {
        if (!$entityManager) {
            $entityManager = $this->getCustomEntityManager();
        }

        return new ShippingService(
            $entityManager,
            false,
            0,
            '',
            $this->getLogger(),
            true
        );
    }

    /**
     * @test
     */
    public function calculateShippingForEmployees()
    {
        $entityManager = $this->getCustomEntityManager();
        $entityManager->shouldReceive('getShippingFromCategoriesAndRegion')->andReturn([$this->getB2CData()]);
        $entityManager->shouldReceive('calculateFreightShipping')->andReturn(0);
        $entityManager->shouldReceive('getActiveShippingPromotions')->andReturn($this->getPromoData());
        $entityManager->shouldReceive('getPromotionCategories')->andReturn([1]);

        $order = (new CreateOrderVODataBuilder())->build();
        $result = $this->prepareService($entityManager)->calculateShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }

    /**
     * @test
     */
    public function calculateShippingWithPromotion()
    {
        $entityManager = $this->getCustomEntityManager();
        $entityManager->shouldReceive('getShippingFromCategoriesAndRegion')->andReturn([$this->getB2CData()]);
        $entityManager->shouldReceive('calculateFreightShipping')->andReturn(63.9);
        $entityManager->shouldReceive('getActiveShippingPromotions')->andReturn($this->getPromoData());
        $entityManager->shouldReceive('getPromotionCategories')->andReturn([1]);

        $order = (new CreateOrderVODataBuilder())->build();
        $result = $this->prepareService($entityManager)->calculateShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }

    /**
     * @test
     */
    public function calculateShippingForProductsWithoutRelatedCategories()
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getCustomEntityManager();
        $entityManager->shouldReceive('getShippingFromCategoriesAndRegion')->andReturn([$this->getB2CData()]);
        $entityManager->shouldReceive('calculateFreightShipping')->andReturn(63.9);
        $entityManager->shouldReceive('getActiveShippingPromotions')->andReturn($this->getPromoData());
        $entityManager->shouldReceive('getPromotionCategories')->andReturn([1, 5]);

        $orderDataBuilder = new CreateOrderVODataBuilder();
        $order = $orderDataBuilder->build();
        $order->getProducts()[0]->setRelatedCategories(null);

        $result = $this->prepareService($entityManager)->calculateShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }

    /**
     * @test
     */
    public function calculateShippingNamedFretinho()
    {
        $promoData = $this->getPromoData();
        $promoData[0]['freteFixo'] = true;
        $promoData[0]['freteFixoValor'] = 67.9;
        $promoData[0]['freteGratis'] = true;

        /** @var EntityManager $entityManager */
        $entityManager = $this->getCustomEntityManager();
        $entityManager->shouldReceive('getShippingFromCategoriesAndRegion')->andReturn([$this->getB2CData()]);
        $entityManager->shouldReceive('calculateFreightShipping')->andReturn(63.9);
        $entityManager->shouldReceive('getActiveShippingPromotions')->andReturn($promoData);
        $entityManager->shouldReceive('getPromotionCategories')->andReturn([1]);

        $orderDataBuilder = new CreateOrderVODataBuilder();
        $order = $orderDataBuilder->build();
        $order->getProducts()[0]->setRelatedCategories(null);

        $result = $this->prepareService($entityManager)->calculateShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }

    /**
     * @test
     */
    public function calculateShippingWithGlobalExtraShippingValue()
    {
        $b2cTratra = $this->getB2CData();
        $b2cTratra['valorFreteFixo'] = 63.9;
        $b2cTratra['valor_frete_fixo'] = 63.9;

        $entityManager = $this->getCustomEntityManager();
        $entityManager->shouldReceive('getShippingFromCategoriesAndRegion')->andReturn([$b2cTratra]);
        $entityManager->shouldReceive('calculateFreightShipping')->andReturn(63.9);
        $entityManager->shouldReceive('getActiveShippingPromotions')->andReturn($this->getPromoData());
        $entityManager->shouldReceive('getPromotionCategories')->andReturn([1]);

        $order = (new CreateOrderVODataBuilder())->build();
        $result = $this->prepareService($entityManager)->calculateShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }

    /**
     * @test
     */
    public function calculateShippingWithItemExtraShippingValue()
    {
        $b2cTratra = $this->getB2CData();
        $b2cTratra['valorFreteFixo'] = 63.9;
        $b2cTratra['valor_frete_fixo'] = 63.9;

        $entityManager = $this->getCustomEntityManager();
        $entityManager->shouldReceive('getShippingFromCategoriesAndRegion')->andReturn([$b2cTratra]);
        $entityManager->shouldReceive('calculateFreightShipping')->andReturn(63.9);
        $entityManager->shouldReceive('getActiveShippingPromotions')->andReturn($this->getPromoData());
        $entityManager->shouldReceive('getPromotionCategories')->andReturn([1]);
        $entityManager->shouldReceive('getShippingProduct')
            ->andReturn(array('price' => 47.61, 'productId' => 21675));

        $order = (new CreateOrderVODataBuilder())->build();
        $service = new ShippingService(
            $entityManager,
            true,
            1.99,
            1,
            $this->getLogger(),
            true
        );

        $result = $service->calculateShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }

    /**
     * @test
     */
    public function calculateShippingWithFixedValue()
    {
        $b2cTratra = $this->getB2CData();
        $b2cTratra['valorFreteFixo'] = 63.9;
        $b2cTratra['valor_frete_fixo'] = 63.9;
        $b2cTratra['tipoFrete'] = 3;
        $b2cTratra['tipo_frete'] = 3;

        $promoData = $this->getPromoData();
        $promoData[0]['freteFixo'] = true;
        $promoData[0]['freteGratis'] = false;

        $entityManager = $this->getCustomEntityManager();
        $entityManager->shouldReceive('getShippingFromCategoriesAndRegion')->andReturn([$b2cTratra]);
        $entityManager->shouldReceive('calculateFreightShipping')->andReturn(63.9);
        $entityManager->shouldReceive('getActiveShippingPromotions')->andReturn($promoData);
        $entityManager->shouldReceive('getPromotionCategories')->andReturn([1]);
        $entityManager->shouldReceive('getShippingProduct')
            ->andReturn(array('price' => 47.61, 'productId' => 21675));

        $order = (new CreateOrderVODataBuilder())->build();
        $result = $this->prepareService($entityManager)->calculateShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }

    /**
     * @test
     */
    public function calculateShippingAddingShippingCompanyDeadline()
    {
        $b2cTratra = $this->getB2CData();
        $b2cTratra['valorFreteFixo'] = 63.9;
        $b2cTratra['valor_frete_fixo'] = 63.9;
        $b2cTratra['tipoFrete'] = 3;
        $b2cTratra['tipo_frete'] = 3;

        $entityManager = $this->getCustomEntityManager();
        $entityManager->shouldReceive('getShippingFromCategoriesAndRegion')->andReturn([$b2cTratra]);
        $entityManager->shouldReceive('calculateFreightShipping')->andReturn(63.9);
        $entityManager->shouldReceive('getActiveShippingPromotions')->andReturn($this->getPromoData());
        $entityManager->shouldReceive('getPromotionCategories')->andReturn([1]);
        $entityManager->shouldReceive('getShippingProduct')
            ->andReturn(array('price' => 47.61, 'productId' => 21675));

        $order = (new CreateOrderVODataBuilder())->build();
        $result = $this->prepareService($entityManager)->calculateShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }

    /**
     * @test
     */
    public function calculateShippingWithDefaultDeadline()
    {
        $b2cTratra = $this->getB2CData();
        $b2cTratra['valorFreteFixo'] = 63.9;
        $b2cTratra['valor_frete_fixo'] = 63.9;
        $b2cTratra['tipoFrete'] = 4;
        $b2cTratra['tipo_frete'] = 4;

        /** @var EntityManager $entityManager */
        $entityManager = $this->getCustomEntityManager();
        $entityManager->shouldReceive('getShippingFromCategoriesAndRegion')->andReturn([$b2cTratra]);
        $entityManager->shouldReceive('calculateFreightShipping')->andReturn(63.9);
        $entityManager->shouldReceive('getActiveShippingPromotions')->andReturn($this->getPromoData());
        $entityManager->shouldReceive('getPromotionCategories')->andReturn([1]);
        $entityManager->shouldReceive('getShippingProduct')
            ->andReturn(array('price' => 47.61, 'productId' => 21675));

        $order = (new CreateOrderVODataBuilder())->build();
        $result = $this->prepareService($entityManager)->calculateShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }

    /**
     * @test
     */
    public function calculateCouponShippingWithFreeShipping()
    {
        $order = (new CreateOrderVODataBuilder())->build();

        $result = $this->prepareService()->calculateCouponShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }

    /**
     * @test
     */
    public function calculateCouponShippingWithFixedShipping()
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getCustomEntityManager();

        $shippingService = new ShippingService(
            $entityManager,
            true,
            1.99,
            1,
            $this->getLogger(),
            true
        );

        $order = (new CreateOrderVODataBuilder())->build();
        $coupon = $order->getCoupon();
        $coupon->setFreeShipping(false);
        $coupon->setFixedShipping(true);
        $order->setCoupon($coupon);

        $result = $shippingService->calculateCouponShipping($order);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateShippingVO::class, $result);
    }
}
