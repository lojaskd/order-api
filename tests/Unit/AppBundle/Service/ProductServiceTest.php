<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\B2cAtributoValor;
use AppBundle\Entity\B2cForfor;
use AppBundle\Entity\B2cPedprd;
use AppBundle\Entity\B2cPedprdEstorno;
use AppBundle\Entity\B2cPedprdstatus;
use AppBundle\Entity\B2cPrdprdPrecosAgressivos;
use AppBundle\Entity\B2cProdutosKits;
use AppBundle\Entity\B2cViewProdutos;
use AppBundle\Entity\Validate\Update\UpdateValidateProductReversalInfo;
use AppBundle\Entity\Validate\Update\UpdateValidateProductStatus;
use AppBundle\Entity\Validate\Update\UpdateValidateProductStatusProduct;
use AppBundle\Entity\Validate\ValidateProductDate;
use AppBundle\Entity\Validate\ValidateProductDateProduct;
use AppBundle\Entity\Validate\ValidateProductDateType;
use AppBundle\Entity\VO\Create\CreateMarketPlaceVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderStatusVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateProductVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateTypeVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateVO;
use AppBundle\Entity\VO\Update\UpdateProductsReversalInfoVO;
use AppBundle\Entity\VO\Update\UpdateProductsStatusProductVO;
use AppBundle\Entity\VO\Update\UpdateProductsStatusVO;
use AppBundle\Service\AwsService;
use AppBundle\Service\EventService;
use AppBundle\Service\MarketPlaceService;
use AppBundle\Service\OrderDateService;
use AppBundle\Service\OrderLogService;
use AppBundle\Service\OrderStatusService;
use AppBundle\Service\ProductService;
use AppBundle\Service\QueueService;
use DateTime;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class ProductServiceTest
 * @package Tests\Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class ProductServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    private function getBasicConfiguredService()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        return $productService;
    }

    /**
     * @test
     */
    public function formatProductInfo()
    {
        $this->assertNotNull($this->getBasicConfiguredService()->formatProductInfo());
        $this->assertInstanceOf(OrderProductVO::class, $this->getBasicConfiguredService()->formatProductInfo());
    }

    /**
     * @test
     */
    public function setOrderStatusService()
    {
        $orderStatusService = Mockery::mock(OrderStatusService::class);
        $this->assertNotNull($this->getBasicConfiguredService()->setOrderStatusService($orderStatusService));
    }

    /**
     * @test
     */
    public function setOrderDateService()
    {
        $orderDateService = Mockery::mock(OrderDateService::class);
        $this->assertNotNull($this->getBasicConfiguredService()->setOrderDateService($orderDateService));
    }

    /**
     * @test
     */
    public function setMarketPlaceService()
    {
        $marketPlaceService = Mockery::mock(MarketPlaceService::class);
        $this->assertNotNull($this->getBasicConfiguredService()->setMarketPlaceService($marketPlaceService));
    }

    /**
     * @test
     */
    public function validateProductStock()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getProductStockAndDeadline')->andReturn([
            'stock' => 2
        ]);
        $entityManager->shouldReceive('getProviderStock')->andReturn([
            'stock' => 3
        ]);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $product = new CreateProductVO();
        $product->setId(1);
        $product->setQuantity(1);

        $result = $productService->validateProductStock([$product]);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function validateMarketPlaceProductStockForMarketPlace()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $b2cAtributoValor = new B2cAtributoValor();
        $b2cAtributoValor->setRespostas('1');

        $entityManager->shouldReceive('findOneBy')->andReturn($b2cAtributoValor);
        $entityManager->shouldReceive('getProductStock')->andReturn(1);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $marketPlaceService = Mockery::mock(MarketPlaceService::class);
        $marketPlaceService->shouldReceive('getMarketplaceAttrStock')->andReturn(1);

        $productService->setMarketPlaceService($marketPlaceService);

        $product = new CreateProductVO();
        $product->setId(1);
        $product->setQuantity(1);

        $result = $productService->validateMarketPlaceProductStock([$product], 1, 1);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function validateMarketPlaceProductStockForMarketPlaceWithInvalidStock()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('findOneBy')->andReturn(null);
        $entityManager->shouldReceive('getProductStock')->andReturn(1);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $marketPlaceService = Mockery::mock(MarketPlaceService::class);
        $marketPlaceService->shouldReceive('getMarketplaceAttrStock')->andReturn(1);

        $productService->setMarketPlaceService($marketPlaceService);

        $product = new CreateProductVO();
        $product->setId(1);
        $product->setQuantity(2);

        $result = $productService->validateMarketPlaceProductStock([$product], 1, 1);
        $this->assertFalse($result);
    }

    /**
     * @test
     */
    public function getProductImage()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getProductListingImage')->andReturn('image.jpg');

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $image1 = $productService->getProductImage(1);
        $this->assertNotNull($image1);

        $image2 = $productService->getProductImage(102);
        $this->assertNotNull($image2);

        $image3 = $productService->getProductImage(1275);
        $this->assertNotNull($image3);

        $image4 = $productService->getProductImage(10975);
        $this->assertNotNull($image4);

        $imageDefault = $productService->getProductImage(999999999);
        $this->assertNotNull($imageDefault);
    }

    /**
     * @test
     */
    public function getProductImageWithoutImage()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getProductListingImage')->andReturn('');

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $image1 = $productService->getProductImage(1);
        $this->assertEmpty($image1);
    }

    /**
     * @test
     */
    public function getTotalPriceWithPromotion()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = true;
        $awsService = Mockery::mock(AwsService::class);

        $aggressivePriceEntity = new B2cPrdprdPrecosAgressivos();
        $aggressivePriceEntity->setPreco(18.9);
        $entityManager->shouldReceive('findOneBy')->andReturn($aggressivePriceEntity);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setQuantity(1);
        $createProductVO->setProductStock(2);

        $result = $productService->getTotalPrice($createProductVO);

        $this->assertNotNull($result);
        $this->assertInternalType('float', $result);
    }

    /**
     * @test
     */
    public function getTotalPriceWithoutPromotionInProduct()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $aggressivePriceEntity = new B2cPrdprdPrecosAgressivos();
        $aggressivePriceEntity->setPreco(18.9);
        $entityManager->shouldReceive('findOneBy')->andReturn($aggressivePriceEntity);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setQuantity(1);
        $createProductVO->setProductStock(2);
        $createProductVO->setRealPrice(29.9);

        $result = $productService->getTotalPrice($createProductVO);

        $this->assertNotNull($result);
        $this->assertInternalType('float', $result);
    }

    /**
     * @test
     */
    public function getTotalPriceWithPromotionInProduct()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $aggressivePriceEntity = new B2cPrdprdPrecosAgressivos();
        $aggressivePriceEntity->setPreco(18.9);
        $entityManager->shouldReceive('findOneBy')->andReturn($aggressivePriceEntity);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setQuantity(1);
        $createProductVO->setProductStock(2);
        $createProductVO->setPromotion(true);
        $createProductVO->setPromotionPrice(29.90);

        $result = $productService->getTotalPrice($createProductVO);

        $this->assertNotNull($result);
        $this->assertInternalType('float', $result);
    }

    /**
     * @test
     */
    public function getTotalPriceKit()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $aggressivePriceEntity = new B2cPrdprdPrecosAgressivos();
        $aggressivePriceEntity->setPreco(18.9);

        $entityManager->shouldReceive('findOneBy')->andReturn($aggressivePriceEntity);
        $entityManager->shouldReceive('getProductKit')->andReturn([
            'desconto' => 3,
            'quantidade' => 1
        ]);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setQuantity(1);
        $createProductVO->setProductStock(2);
        $createProductVO->setKitId(2);

        $result = $productService->getTotalPrice($createProductVO);

        $this->assertNotNull($result);
        $this->assertInternalType('float', $result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function reduceStockException()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createOrderVO = new CreateOrderVO();

        $productService->reduceStock(1, 1, $createOrderVO);
    }

    /**
     * @test
     */
    public function reduceStock()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('updateProductStock')->andReturn($entityManager);
        $entityManager->shouldReceive('getProductStock')->andReturn(4);
        $entityManager->shouldReceive('setMarketplaceStockInfo')->andReturn($entityManager);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $marketPlaceService = Mockery::mock(MarketPlaceService::class);
        $marketPlaceService->shouldReceive('getMarketplaceAttrStock')->andReturn(1);

        $productService->setMarketPlaceService($marketPlaceService);

        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setProducts([new CreateProductVO()]);

        $createMarketPlaceVO = new CreateMarketPlaceVO();
        $createMarketPlaceVO->setChannelId(1);

        $createOrderVO->setMarketplace($createMarketPlaceVO);

        $result = $productService->reduceStock(1, 1, $createOrderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function reduceStockWithoutMarketPlace()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('updateProductStock')->andReturn($entityManager);
        $entityManager->shouldReceive('getProductStock')->andReturn(4);
        $entityManager->shouldReceive('setMarketplaceStockInfo')->andReturn($entityManager);
        $entityManager->shouldReceive('updateProviderStock')->andReturn($entityManager);
        $entityManager->shouldReceive('updateBranchOfficeStock')->andReturn($entityManager);
        $entityManager->shouldReceive('updateProductStockInfo')->andReturn($entityManager);
        $entityManager->shouldReceive('updateKitStock')->andReturn($entityManager);
        $entityManager->shouldReceive('setStockInfo')->andReturn($entityManager);
        $entityManager->shouldReceive('getKit')->andReturn([[
            'pvreal' => 26.90,
            'stqminAcao' => 2,
            'promocaoini' => date('Y-m-d'),
            'promocaofim' => date('Y-m-d', strtotime('+5days')),
            'pvpromocao' => 1,
            'estoque' => 6,
            'stqfornecedor' => 6,
            'stqminimo' => 1,
            'obrigatorio' => true,
            'mostra' => true,
            'prazoFornecedor' => 10,
            'dispDias' => 5,
            'fornecedor' => 1,
            'disponivel' => true,
            'quantidade' => 1,
            'desconto' => 0.0
        ]]);

        $kit = new B2cProdutosKits();
        $kit->setIdKit(1);
        $entityManager->shouldReceive('findBy')->andReturn([$kit]);

        $provider = new B2cForfor();
        $entityManager->shouldReceive('findOneBy')->andReturn($provider);

        $entityManager->shouldReceive('getProductStockAndDeadline')->andReturn([
            'idFilial' => 1,
            'stock' => 1
        ]);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $marketPlaceService = Mockery::mock(MarketPlaceService::class);
        $marketPlaceService->shouldReceive('getMarketplaceAttrStock')->andReturn(1);

        $productService->setMarketPlaceService($marketPlaceService);

        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setProducts([new CreateProductVO()]);

        $createMarketPlaceVO = new CreateMarketPlaceVO();
        $createMarketPlaceVO->setChannelId(0);

        $createOrderVO->setMarketplace($createMarketPlaceVO);

        $entityManager->shouldReceive('getRelatedKitByProductId')->andReturn($entityManager);

        $result = $productService->reduceStock(1, 0, $createOrderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function reduceStockManually()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getProductStockAndDeadline')->andReturn([
            'idFilial' => 1,
            'stock' => 1
        ]);
        $entityManager->shouldReceive('updateProviderStock')->andReturn($entityManager);
        $entityManager->shouldReceive('updateBranchOfficeStock')->andReturn($entityManager);
        $entityManager->shouldReceive('updateProductStockInfo')->andReturn($entityManager);

        $provider = new B2cForfor();
        $provider->setPrazo(10);
        $entityManager->shouldReceive('findOneBy')->andReturn($provider);

        $kit = new B2cProdutosKits();
        $kit->setIdKit(1);
        $entityManager->shouldReceive('findBy')->andReturn([$kit]);
        $entityManager->shouldReceive('getKit')->andReturn([[
            'pvreal' => 26.90,
            'stqminAcao' => 2,
            'promocaoini' => date('Y-m-d'),
            'promocaofim' => date('Y-m-d', strtotime('+5days')),
            'pvpromocao' => 1,
            'estoque' => 6,
            'stqfornecedor' => 6,
            'stqminimo' => 1,
            'obrigatorio' => true,
            'mostra' => true,
            'prazoFornecedor' => 10,
            'dispDias' => 5,
            'fornecedor' => 1,
            'disponivel' => true,
            'quantidade' => 1,
            'desconto' => 0.0
        ]]);
        $entityManager->shouldReceive('updateKitStock')->andReturn($entityManager);
        $entityManager->shouldReceive('setStockInfo')->andReturn($entityManager);
        $entityManager->shouldReceive('getRelatedKitByProductId')->andReturn($entityManager);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setQuantity(2);
        $createProductVO->setProviderStock(18);
        $createProductVO->setProviderDeadline(1);

        $result = $productService->reduceStockManually(1, $createProductVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function getAvailabilityInfoStockEqualsToZeroAndMinStockActionTo1()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setMinStockAction(1);

        $result = $productService->getAvailabilityInfo($createProductVO);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getAvailabilityInfoStockEqualsToZeroAndMinStockActionTo2CaseOne()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setMinStockAction(2);

        $result = $productService->getAvailabilityInfo($createProductVO);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getAvailabilityInfoStockEqualsToZeroAndMinStockActionTo2CaseTwo()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setMinStockAction(2);
        $createProductVO->setProviderStock(2);

        $result = $productService->getAvailabilityInfo($createProductVO);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getAvailabilityInfoStockEqualsToZeroAndMinStockActionTo2CaseThree()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setMinStockAction(2);
        $createProductVO->setProviderStock(2);
        $createProductVO->setProviderDeadline(18);
        $createProductVO->setProductProviderDeadline(26);

        $result = $productService->getAvailabilityInfo($createProductVO);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getAvailabilityInfoStockGreaterThanZeroCaseOne()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setMinStockAction(2);
        $createProductVO->setProviderStock(2);
        $createProductVO->setProviderDeadline(18);
        $createProductVO->setProductProviderDeadline(26);
        $createProductVO->setProductStock(2);
        $createProductVO->setAvailable(true);

        $result = $productService->getAvailabilityInfo($createProductVO);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getAvailabilityInfoStockGreaterThanZeroCaseTwo()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setMinStockAction(2);
        $createProductVO->setProviderStock(2);
        $createProductVO->setProviderDeadline(18);
        $createProductVO->setProductProviderDeadline(26);
        $createProductVO->setProductStock(2);
        $createProductVO->setAvailable(true);
        $createProductVO->setAvailabilityDays(10);

        $result = $productService->getAvailabilityInfo($createProductVO);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getAvailabilityInfoStockGreaterThanZeroCaseThree()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setMinStockAction(2);
        $createProductVO->setProviderStock(2);
        $createProductVO->setProviderDeadline(18);
        $createProductVO->setProductProviderDeadline(26);
        $createProductVO->setProductStock(2);
        $createProductVO->setQuantity(3);
        $createProductVO->setAvailable(true);
        $createProductVO->setAvailabilityDays(10);

        $result = $productService->getAvailabilityInfo($createProductVO);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getAvailabilityInfoStockGreaterThanZeroCaseFour()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $createProductVO = new CreateProductVO();
        $createProductVO->setMinStockAction(2);
        $createProductVO->setProviderStock(2);
        $createProductVO->setProviderDeadline(18);
        $createProductVO->setProductProviderDeadline(26);
        $createProductVO->setProductStock(2);
        $createProductVO->setQuantity(3);
        $createProductVO->setAvailable(true);
        $createProductVO->setAvailabilityDays(0);

        $result = $productService->getAvailabilityInfo($createProductVO);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function manageKitStockInfo()
    {
        $entityManager = $this->getEntityManager();

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendReduceStock')->andReturn(false);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $provider = new B2cForfor();
        $provider->setPrazo(10);
        $entityManager->shouldReceive('findOneBy')->andReturn($provider);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $kit = [
            'pvreal' => 23.9,
            'stqminAcao' => 3,
            'promocaoini' => date('Y-m-d', strtotime('+2days')),
            'promocaofim' => date('Y-m-d', strtotime('+5days')),
            'pvpromocao' => 1,
            'estoque' => 1,
            'stqfornecedor' => 4,
            'stqminimo' => 1,
            'obrigatorio' => true,
            'mostra' => 'ok',
            'prazoFornecedor' => 10,
            'dispDias' => 5,
            'fornecedor' => 1,
            'disponivel' => true,
            'quantidade' => 1,
            'desconto' => 0.0
        ];

        $result = $productService->manageKitStockInfo([$kit, $kit, $kit]);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function reduceMarketPlaceStock()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('updateProductStock')->andReturn($entityManager);
        $entityManager->shouldReceive('getProductStock')->andReturn(1);
        $entityManager->shouldReceive('setMarketplaceStockInfo')->andReturn($entityManager);

        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = false;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $marketPlaceService = Mockery::mock(MarketPlaceService::class);
        $marketPlaceService->shouldReceive('getMarketplaceAttrStock')->andReturn(1);
        $productService->setMarketPlaceService($marketPlaceService);

        $createProductVO = new CreateProductVO();

        $result = $productService->reduceMarketPlaceStock(1, 1, 1, $createProductVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function reduceMarketPlaceStockCaseTwo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('updateProductStock')->andReturn($entityManager);
        $entityManager->shouldReceive('getProductStock')->andReturn(1);
        $entityManager->shouldReceive('setMarketplaceStockInfo')->andReturn($entityManager);
        $entityManager->shouldReceive('updateMarketplaceStock')->andReturn($entityManager);

        $b2cAtributoValor = new B2cAtributoValor();
        $b2cAtributoValor->setRespostas('1');

        $entityManager->shouldReceive('findOneBy')->andReturn($b2cAtributoValor);

        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $marketPlaceService = Mockery::mock(MarketPlaceService::class);
        $marketPlaceService->shouldReceive('getMarketplaceAttrStock')->andReturn(1);
        $productService->setMarketPlaceService($marketPlaceService);

        $createProductVO = new CreateProductVO();

        $result = $productService->reduceMarketPlaceStock(1, 1, 1, $createProductVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function getProductDatesImediateDelivery()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('calculateProductDates')->andReturn([
            'availabilityDeadline' => 1,
            'providerDeadLine' => 1,
            'predictDates' => [
                'supplierDeliveryDate' => date('Y-m-d'),
                'deliveryDate' => date('Y-m-d')
            ]
        ]);
        $productService->setOrderDateService($orderDateService);

        $createProductVO = new CreateProductVO();
        $createProductVO->setAvailability('IMEDIATA');
        $result = $productService->getProductDates($createProductVO, date('Y-m-d'), '82900270', 1);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getProductDates()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('calculateProductDates')->andReturn([
            'availabilityDeadline' => 1,
            'providerDeadLine' => 1,
            'predictDates' => [
                'supplierDeliveryDate' => date('Y-m-d'),
                'deliveryDate' => date('Y-m-d')
            ]
        ]);
        $productService->setOrderDateService($orderDateService);

        $createProductVO = new CreateProductVO();
        $result = $productService->getProductDates($createProductVO, date('Y-m-d'), '82900270', 1);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getCommonInfoProductStatusCaseOne()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $productStatus = new UpdateValidateProductStatus();
        $orderVO = new OrderVO();
        $orderVO->setProducts([new OrderProductVO()]);

        $result = $productService->getCommonInfoProductStatus($productStatus, $orderVO);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getCommonInfoProductStatusCaseTwo()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $productStatus = new UpdateValidateProductStatus();
        $validateProduct = new UpdateValidateProductStatusProduct();
        $validateProduct->setId(1);
        $productStatus->setProducts([$validateProduct]);

        $orderVO = new OrderVO();
        $orderProduct = new OrderProductVO();
        $orderProduct->setId(1);
        $orderProduct->setProductId(1);
        $orderProduct->setReference(1);
        $orderVO->setProducts([$orderProduct]);

        $result = $productService->getCommonInfoProductStatus($productStatus, $orderVO);

        $this->assertNotNull($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function getCommonInfoProductStatusException()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $productStatus = new UpdateValidateProductStatus();
        $validateProduct = new UpdateValidateProductStatusProduct();
        $validateProduct->setId(1);
        $productStatus->setProducts([$validateProduct]);

        $orderVO = new OrderVO();
        $orderProduct = new OrderProductVO();
        $orderVO->setProducts([$orderProduct]);

        $productService->getCommonInfoProductStatus($productStatus, $orderVO);
    }

    /**
     * @test
     */
    public function getCommonInfoProductReversalCaseOne()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $productStatus = new UpdateValidateProductReversalInfo();
        $orderVO = new OrderVO();
        $orderVO->setProducts([new OrderProductVO()]);

        $result = $productService->getCommonInfoProductReversal($productStatus, $orderVO);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getCommonInfoProductReversalCaseTwo()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $statusProduct = new UpdateValidateProductStatusProduct();
        $statusProduct->setId(1);

        $productStatus = new UpdateValidateProductReversalInfo();
        $productStatus->setProducts([$statusProduct]);

        $orderProduct = new OrderProductVO();
        $orderProduct->setId(1);
        $orderProduct->setReference('1');
        $orderProduct->setProductId(1);

        $orderVO = new OrderVO();
        $orderVO->setProducts([$orderProduct]);

        $result = $productService->getCommonInfoProductReversal($productStatus, $orderVO);

        $this->assertNotNull($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function getCommonInfoProductReversalException()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);
        $eventService = Mockery::mock(EventService::class);
        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;
        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $productStatus = new UpdateValidateProductReversalInfo();
        $productStatus->setProducts([new UpdateValidateProductStatusProduct()]);

        $orderVO = new OrderVO();
        $orderVO->setProducts([new OrderProductVO()]);

        $result = $productService->getCommonInfoProductReversal($productStatus, $orderVO);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function approveReversal()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cPedprdEstorno());

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $updateProductsReversalInfoVO = new UpdateProductsReversalInfoVO();
        $updateProduct = new UpdateProductsStatusProductVO();
        $updateProduct->setId(1);
        $updateProductsReversalInfoVO->setProducts([$updateProduct, new UpdateProductsStatusProductVO()]);

        $orderVO = new OrderVO();
        $orderProductVO = new OrderProductVO();
        $orderProductVO->setId(2);
        $orderVO->setProducts([$orderProductVO, new OrderProductVO()]);

        $result = $productService->approveReversal($updateProductsReversalInfoVO, $orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function approveReversalInternalException()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('getTransactionNestingLevel')->andReturn($entityManager);
        $entityManager->shouldReceive('rollback')->andReturn($entityManager);
        $entityManager->shouldReceive('findOneBy')->andReturn(null);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $updateProductsReversalInfoVO = new UpdateProductsReversalInfoVO();
        $updateProductsReversalInfoVO->setProducts([new UpdateProductsStatusProductVO()]);

        $orderVO = new OrderVO();
        $orderVO->setProducts([new OrderProductVO()]);

        $productService->approveReversal($updateProductsReversalInfoVO, $orderVO);
    }

    /**
     * @test
     */
    public function getProductOrderInfo()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getByOrderAndProduct')->andReturn([
                'idPrdprd' => 1,
                'prdReferencia' => 1,
                'prdFornecedor' => 1,
                'idPedped' => 1
            ]
        );
        $entityManager->shouldReceive('getProductOrderStatus')->andReturn([]);
        $entityManager->shouldReceive('getStatus')->andReturn([]);
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('close')->andReturn($entityManager);
        $entityManager->shouldReceive('getProduction')->andReturn([]);
        $entityManager->shouldReceive('getInvoices')->andReturn([]);
        $entityManager->shouldReceive('getShipper')->andReturn([]);
        $entityManager->shouldReceive('getPolo')->andReturn([]);
        $entityManager->shouldReceive('getBranchOfficeInformation')->andReturn([]);
        $entityManager->shouldReceive('formatProductInfo')->andReturn([]);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $result = $productService->getProductOrderInfo(1, 1);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function getProductOrderInfoWithInvalidProduct()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getByOrderAndProduct')->andReturn(null);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $result = $productService->getProductOrderInfo(1, 1);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
        $this->assertEmpty($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function getProductOrderInfoException()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $productService->getProductOrderInfo(1, 1);
    }

    /**
     * @test
     */
    public function getCommonInfoProductDateCaseOne()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $validateProductDate = new ValidateProductDateProduct();
        $validateProductDate->setId(1);

        $productDate = new ValidateProductDate();
        $productDate->setProducts([$validateProductDate]);
        $productDate->setDateTypes([new ValidateProductDateType()]);

        $orderProductVO = new OrderProductVO();
        $orderProductVO->setId(1);
        $orderProductVO->setReference(1);
        $orderProductVO->setProductId(1);

        $orderVO = new OrderVO();
        $orderVO->setProducts([$orderProductVO]);

        $result = $productService->getCommonInfoProductDate($productDate, $orderVO);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getCommonInfoProductDateCaseTwo()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $productDate = new ValidateProductDate();
        $productDate->setDateTypes([new ValidateProductDateType()]);

        $orderProductVO = new OrderProductVO();
        $orderProductVO->setId(1);
        $orderProductVO->setReference(1);
        $orderProductVO->setProductId(1);

        $orderVO = new OrderVO();
        $orderVO->setProducts([$orderProductVO]);

        $result = $productService->getCommonInfoProductDate($productDate, $orderVO);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function updateDateCaseOne()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderUpdateDate')->andReturn($entityManager);
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cPedprdstatus());

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $orderStatusService = Mockery::mock(OrderStatusService::class);
        $orderStatusService->shouldReceive('autoUpdateDatesAndStatus')->andReturn($orderStatusService);
        $productService->setOrderStatusService($orderStatusService);

        $updateProductsDateVO = new UpdateProductsDateVO();
        $updateProductsDateVO->setProducts([new UpdateProductsDateProductVO()]);

        $approvalDate = new UpdateProductsDateTypeVO();
        $approvalDate->setType('approvalDate');

        $releaseDate = new UpdateProductsDateTypeVO();
        $releaseDate->setType('releaseDate');

        $supplierBillingDate = new UpdateProductsDateTypeVO();
        $supplierBillingDate->setType('supplierBillingDate');

        $supplierInvoiceDate = new UpdateProductsDateTypeVO();
        $supplierInvoiceDate->setType('supplierInvoiceDate');

        $supplierDeliveryDate = new UpdateProductsDateTypeVO();
        $supplierDeliveryDate->setType('supplierDeliveryDate');

        $shippingDate = new UpdateProductsDateTypeVO();
        $shippingDate->setType('shippingDate');

        $invoiceDate = new UpdateProductsDateTypeVO();
        $invoiceDate->setType('invoiceDate');

        $deliveryDate = new UpdateProductsDateTypeVO();
        $deliveryDate->setType('deliveryDate');

        $defaultDate = new UpdateProductsDateTypeVO();

        $updateProductsDateVO->setDateTypes([
            $approvalDate,
            $releaseDate,
            $supplierBillingDate,
            $supplierInvoiceDate,
            $supplierDeliveryDate,
            $shippingDate,
            $invoiceDate,
            $deliveryDate,
            $defaultDate
        ]);

        $updateProductsDateVO->setPredict(true);

        $orderVO = null;

        $result = $productService->updateDate($updateProductsDateVO, $orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function updateDateCaseTwo()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderUpdateDate')->andReturn($entityManager);
        $entityManager->shouldReceive('getTransactionNestingLevel')->andReturn($entityManager);
        $entityManager->shouldReceive('rollback')->andReturn($entityManager);
        $entityManager->shouldReceive('getDateAddNextBusinessDay')->andReturn(date('Y-m-d'));

        $productStatus = new B2cPedprdstatus();
        $productStatus->setDataEmbR(new DateTime());
        $entityManager->shouldReceive('findOneBy')->andReturn($productStatus);

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $orderStatusService = Mockery::mock(OrderStatusService::class);
        $orderStatusService->shouldReceive('autoUpdateDatesAndStatus')->andReturn($orderStatusService);
        $productService->setOrderStatusService($orderStatusService);

        $updateProductsDateVO = new UpdateProductsDateVO();
        $updateProductsDateVO->setProducts([new UpdateProductsDateProductVO()]);

        $approvalDate = new UpdateProductsDateTypeVO();
        $approvalDate->setType('approvalDate');

        $releaseDate = new UpdateProductsDateTypeVO();
        $releaseDate->setType('releaseDate');

        $supplierBillingDate = new UpdateProductsDateTypeVO();
        $supplierBillingDate->setType('supplierBillingDate');

        $supplierInvoiceDate = new UpdateProductsDateTypeVO();
        $supplierInvoiceDate->setType('supplierInvoiceDate');

        $supplierDeliveryDate = new UpdateProductsDateTypeVO();
        $supplierDeliveryDate->setType('supplierDeliveryDate');

        $shippingDate = new UpdateProductsDateTypeVO();
        $shippingDate->setType('shippingDate');

        $invoiceDate = new UpdateProductsDateTypeVO();
        $invoiceDate->setType('invoiceDate');
        $invoiceDate->setDate(new DateTime());

        $deliveryDate = new UpdateProductsDateTypeVO();
        $deliveryDate->setType('deliveryDate');
        $deliveryDate->setDate(new DateTime());

        $defaultDate = new UpdateProductsDateTypeVO();

        $updateProductsDateVO->setDateTypes([
            $approvalDate,
            $releaseDate,
            $supplierBillingDate,
            $supplierInvoiceDate,
            $supplierDeliveryDate,
            $invoiceDate,
            $defaultDate
        ]);

        $updateProductsDateVO->setPredict(false);

        $orderProduct = new OrderProductVO();
        $orderProduct->setImmediateDelivery(true);

        $orderVO = new OrderVO();
        $orderVO->setProducts([$orderProduct]);

        $result = $productService->updateDate($updateProductsDateVO, $orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function updateDateExceptionForOrderStatusService()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;
        $orderLogService = Mockery::mock(OrderLogService::class);
        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $productStatus = new B2cPedprdstatus();
        $productStatus->setDataEmbR(new DateTime());

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $updateProductsDateVO = new UpdateProductsDateVO();
        $orderVO = new OrderVO();

        $productService->updateDate($updateProductsDateVO, $orderVO);
    }

    /**
     * @test
     */
    public function updateStatusCaseOne()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;

        $orderLogService = Mockery::mock(OrderLogService::class);
        $orderLogService->shouldReceive('insertOrderLog')->andReturn(true);

        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderUpdateDate')->andReturn($entityManager);
        $entityManager->shouldReceive('autoUpdateDatesAndStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('updateProductAvailabilityInfo')->andReturn($entityManager);
        $entityManager->shouldReceive('updateProduct')->andReturn($entityManager);
        $entityManager->shouldReceive('updateKitStock')->andReturn($entityManager);
        $entityManager->shouldReceive('insertOrderProductReversal')->andReturn($entityManager);
        $entityManager->shouldReceive('insertOrderLog')->andReturn($entityManager);
        $entityManager->shouldReceive('increaseProviderStock')->andReturn($entityManager);
        $entityManager->shouldReceive('increaseStock')->andReturn($entityManager);
        $entityManager->shouldReceive('increaseStoreStock')->andReturn($entityManager);
        $entityManager->shouldReceive('getKit')->andReturn([]);
        $entityManager->shouldReceive('findBy')->andReturn([new B2cProdutosKits()]);

        $b2cPedprd = new B2cPedprd();
        $b2cPedprd->setItemCancelado(false);

        $view = new B2cViewProdutos();

        $entityManager->shouldReceive('findOneBy')->andReturn($b2cPedprd, $view);

        $productStatus = new B2cPedprdstatus();
        $productStatus->setDataEmbR(new DateTime());

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $orderStatusService = Mockery::mock(OrderStatusService::class);
        $orderStatusService->shouldReceive('autoUpdateDatesAndStatus')->andReturn(true);

        $productService->setOrderStatusService($orderStatusService);

        $updateProductsStatusVO = new UpdateProductsStatusVO();
        $updateProductsStatusVO->setProducts([new UpdateProductsStatusProductVO()]);
        $updateProductsStatusVO->setCancelled(true);

        $orderVO = new OrderVO();
        $orderVO->setProducts([new OrderProductVO()]);
        $orderVO->setSubtotal(30.00);

        $orderStatus = new OrderStatusVO();
        $orderVO->setStatus($orderStatus);
        $orderVO->setInternalStatus($orderStatus);

        $product = new OrderProductVO();
        $product->setSupplierStock(3);
        $product->setProductStock(3);

        $orderVO->setProducts([$product]);

        $entityManager->shouldReceive('getRelatedKitByProductId')->andReturn($entityManager);

        $result = $productService->updateStatus($updateProductsStatusVO, $orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function updateStatusCaseTwo()
    {
        $entityManager = $this->getEntityManager();
        $queueService = Mockery::mock(QueueService::class);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $marketPlaceChannelStock = true;

        $orderLogService = Mockery::mock(OrderLogService::class);
        $orderLogService->shouldReceive('insertOrderLog')->andReturn(true);

        $aggressivePrice = false;

        $awsService = Mockery::mock(AwsService::class);

        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderUpdateDate')->andReturn($entityManager);
        $entityManager->shouldReceive('autoUpdateDatesAndStatus')->andReturn($entityManager);
        $entityManager->shouldReceive('updateProductAvailabilityInfo')->andReturn($entityManager);
        $entityManager->shouldReceive('updateProduct')->andReturn($entityManager);
        $entityManager->shouldReceive('updateKitStock')->andReturn($entityManager);
        $entityManager->shouldReceive('insertOrderProductReversal')->andReturn($entityManager);
        $entityManager->shouldReceive('insertOrderLog')->andReturn($entityManager);
        $entityManager->shouldReceive('increaseProviderStock')->andReturn($entityManager);
        $entityManager->shouldReceive('increaseStock')->andReturn($entityManager);
        $entityManager->shouldReceive('increaseStoreStock')->andReturn($entityManager);
        $entityManager->shouldReceive('getKit')->andReturn([]);
        $entityManager->shouldReceive('findBy')->andReturn([new B2cProdutosKits()]);

        $b2cPedprd = new B2cPedprd();
        $b2cPedprd->setItemCancelado(true);

        $view = new B2cViewProdutos();

        $entityManager->shouldReceive('findOneBy')->andReturn($b2cPedprd, $view);

        $productStatus = new B2cPedprdstatus();
        $productStatus->setDataEmbR(new DateTime());

        $productService = new ProductService(
            $entityManager,
            $queueService,
            $this->getLogger(),
            $eventService,
            $marketPlaceChannelStock,
            $orderLogService,
            $awsService,
            $aggressivePrice
        );

        $orderStatusService = Mockery::mock(OrderStatusService::class);
        $orderStatusService->shouldReceive('autoUpdateDatesAndStatus')->andReturn(true);

        $productService->setOrderStatusService($orderStatusService);

        $updateProductsStatusVO = new UpdateProductsStatusVO();
        $updateProductsStatusVO->setProducts([new UpdateProductsStatusProductVO()]);
        $updateProductsStatusVO->setCancelled(false);

        $orderVO = new OrderVO();
        $orderVO->setProducts([new OrderProductVO()]);
        $orderVO->setSubtotal(30.00);

        $orderStatus = new OrderStatusVO();
        $orderVO->setStatus($orderStatus);
        $orderVO->setInternalStatus($orderStatus);

        $product = new OrderProductVO();
        $product->setSupplierStock(3);
        $product->setProductStock(3);

        $orderVO->setProducts([$product]);

        $result = $productService->updateStatus($updateProductsStatusVO, $orderVO);

        $this->assertTrue($result);
    }
}
