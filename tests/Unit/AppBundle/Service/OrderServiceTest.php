<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\B2cFornecedorRegiao;
use AppBundle\Entity\B2cPromo;
use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\B2cPedprd;
use AppBundle\Entity\B2cPedsituacao;
use AppBundle\Entity\B2cPedstatus;
use AppBundle\Entity\B2cSelos;
use AppBundle\Entity\B2cStaint;
use AppBundle\Entity\B2cViewProdutos;
use AppBundle\Entity\Validate\ValidateOrder;
use AppBundle\Entity\Validate\ValidateOrderStatus;
use AppBundle\Entity\Validate\ValidateOrigin;
use AppBundle\Entity\Validate\ValidatePurchaseOrder;
use AppBundle\Entity\Validate\ValidatePurchaseOrderProduct;
use AppBundle\Entity\VO\Create\CreateAddressVO;
use AppBundle\Entity\VO\Create\CreateCouponVO;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\Create\CreatePurchaseOrderProductVO;
use AppBundle\Entity\VO\Create\CreatePurchaseOrderVO;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderStatusVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\ProductVO;
use AppBundle\Entity\VO\Update\UpdateNewDeliveryDateVO;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use AppBundle\Service\AwsService;
use AppBundle\Service\EmailService;
use AppBundle\Service\EventService;
use AppBundle\Service\LojasKdService;
use AppBundle\Service\MarketPlaceService;
use AppBundle\Service\OrderDateService;
use AppBundle\Service\OrderLogService;
use AppBundle\Service\OrderService;
use AppBundle\Service\OrderStatusService;
use AppBundle\Service\ProductService;
use AppBundle\Service\QueueService;
use AppBundle\Service\ShippingService;
use DateTime;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class OrderServiceTest
 * @package Tests\Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class OrderServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    private function getBasicService($entityManager = null, $saveSeals = false, $cashDiscount = false)
    {
        if (!$entityManager) {
            $entityManager = $this->getEntityManager();
        }

        $lojasKdService = Mockery::mock(LojasKdService::class);
        $lojasKdService->shouldReceive('getLojasKdData')->andReturn([]);

        $marketPlaceService = Mockery::mock(MarketPlaceService::class);
        $marketPlaceService->shouldReceive('getMarketPlaceData')->andReturn([]);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('reduceStock')->andReturn(true);
        $productService->shouldReceive('updateDate')->andReturn($productService);
        $productService->shouldReceive('updateStatus')->andReturn($productService);
        $productService->shouldReceive('approveReversal')->andReturn($productService);
        $productService->shouldReceive('getProductImage')->andReturn('teste.jpg');
        $productService->shouldReceive('getTotalPrice')->andReturn(23.90);
        $productService->shouldReceive('getProductDates')->andReturn(new CreateProductVO());

        $emailService = Mockery::mock(EmailService::class);
        $emailService->shouldReceive('sendOrderEmail')->andReturn(true);

        $queueService = Mockery::mock(QueueService::class);

        $orderStatusService = Mockery::mock(OrderStatusService::class);
        $orderStatusService->shouldReceive('updateStatus')->andReturn(true);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $orderDateService = Mockery::mock(OrderDateService::class);
        $orderDateService->shouldReceive('createOrderDeadline')->andReturn(true);

        $orderLogService = Mockery::mock(OrderLogService::class);
        $orderLogService->shouldReceive('insertOrderLog')->andReturn(true);
        $orderLogService->shouldReceive('insertCreateOrderLog')->andReturn(true);

        $shippingService = Mockery::mock(ShippingService::class);

        $awsService = Mockery::mock(AwsService::class);

        $orderService = new OrderService(
            $entityManager,
            $lojasKdService,
            $marketPlaceService,
            $productService,
            $emailService,
            $queueService,
            $this->getLogger(),
            $orderStatusService,
            $eventService,
            $orderDateService,
            $orderLogService,
            $saveSeals,
            $shippingService,
            $cashDiscount,
            $awsService
        );

        return $orderService;
    }

    /**
     * @test
     */
    public function generateOrderLojasKD()
    {
        $orderVO = new CreateOrderVO();
        $order = new ValidateOrder();
;
        $result = $this->getBasicService()->generateOrder($orderVO, $order);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function generateOrderMarketPlace()
    {
        $orderVO = new CreateOrderVO();
        $orderVO->setType('marketplace');
        $order = new ValidateOrder();

        $result = $this->getBasicService()->generateOrder($orderVO, $order);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function setRegion()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getRegion')->andReturn([
            'id' => 1,
            'regiao' => 'test',
            'dist' => 'test',
            'clienteEscolhe' => true
        ]);
        $entityManager->shouldReceive('getIBGECode')->andReturn(1234);

        $createAddressVO = new CreateAddressVO();
        $createAddressVO->setZip('88888888');

        $orderVO = new CreateOrderVO();
        $orderVO->setDeliveryAddress($createAddressVO);
        $this->assertNotNull($this->getBasicService($entityManager)->setRegion($orderVO));
    }

    /**
     * @test
     */
    public function getCommonPurchaseOrderInfo()
    {
        $validatePurchaseOrder = new ValidatePurchaseOrder();
        $validatePurchaseOrder->setProducts([new ValidatePurchaseOrderProduct()]);

        $result = $this->getBasicService()->getCommonPurchaseOrderInfo($validatePurchaseOrder);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getCommonOrderStatusInfo()
    {
        $orderStatus = new ValidateOrderStatus();

        $result = $this->getBasicService()->getCommonOrderStatusInfo($orderStatus);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function updateNewDeliveryDateCaseOne()
    {
        $updateNewDeliveryDateVO = new UpdateNewDeliveryDateVO();

        $result = $this->getBasicService()->updateNewDeliveryDate($updateNewDeliveryDateVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function updateNewDeliveryDateCaseTwo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cPedped());
        $entityManager->shouldReceive('updateNewDeliveryDate')->andReturn($entityManager);

        $updateNewDeliveryDateVO = new UpdateNewDeliveryDateVO();
        $updateNewDeliveryDateVO->setAccept(true);
        $updateNewDeliveryDateVO->setNewDeliveryDate(new DateTime());

        $result = $this->getBasicService($entityManager)->updateNewDeliveryDate($updateNewDeliveryDateVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function updateNewDeliveryDateCaseThree()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findOneBy')->andReturn(null);
        $entityManager->shouldReceive('updateNewDeliveryDate')->andReturn($entityManager);

        $updateNewDeliveryDateVO = new UpdateNewDeliveryDateVO();
        $updateNewDeliveryDateVO->setAccept(true);
        $updateNewDeliveryDateVO->setNewDeliveryDate(new DateTime());

        $result = $this->getBasicService($entityManager)->updateNewDeliveryDate($updateNewDeliveryDateVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function getOrderInfo()
    {
        $product = [
            'idPrdprd' => 1,
            'prdFornecedor' => 1,
            'prdReferencia' => 1
        ];
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getOrder')->andReturn([
            'products' => [$product],
            'tipoPedido' => 'marketplace'
        ]);
        $entityManager->shouldReceive('getProductOrderStatus')->andReturn([]);
        $entityManager->shouldReceive('getStatus')->andReturn([]);
        $entityManager->shouldReceive('getProduction')->andReturn([]);
        $entityManager->shouldReceive('getPolo')->andReturn([]);
        $entityManager->shouldReceive('getInvoices')->andReturn([]);
        $entityManager->shouldReceive('getShipper')->andReturn([]);
        $entityManager->shouldReceive('getBranchOfficeInformation')->andReturn([]);
        $entityManager->shouldReceive('formatProductInfo')->andReturn([]);
        $entityManager->shouldReceive('getMarketPlaceOrder')->andReturn([]);
        $entityManager->shouldReceive('getSituation')->andReturn([1, 1]);
        $entityManager->shouldReceive('formatOrderInfo')->andReturn([]);
        $entityManager->shouldReceive('getS3AuthenticatedUrl')->andReturn('');
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('close')->andReturn($entityManager);

        $result = $this->getBasicService($entityManager)->getOrderInfo(1);

        $this->assertNotNull($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function getOrderInfoException()
    {
        $product = [
            'idPrdprd' => 1,
            'prdFornecedor' => 1,
            'prdReferencia' => 1
        ];
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getOrder')->andReturn([
            'products' => [$product],
            'tipoPedido' => 'marketplace'
        ]);

        $this->getBasicService($entityManager)->getOrderInfo(1);
    }

    /**
     * @test
     */
    public function insertOrderCaseOne()
    {
        $b2Pedprd = new B2cPedped();
        $b2Pedprd->setIdPedped(1);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('generateOrder')->andReturn($b2Pedprd);
        $entityManager->shouldReceive('generateProductsOrder')->andReturn(new B2cPedprd());
        $entityManager->shouldReceive('getSealsProducts')->andReturn([new B2cSelos()]);
        $entityManager->shouldReceive('insertOrderProductsSeals')->andReturn($entityManager);
        $entityManager->shouldReceive('insertOrderMarketplace')->andReturn($entityManager);
        $entityManager->shouldReceive('updateCouponSituation')->andReturn($entityManager);
        $entityManager->shouldReceive('updateCouponUtilization')->andReturn($entityManager);

        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setProducts([new CreateProductVO()]);
        $createOrderVO->setCustomer(new CreateCustomerVO());
        $createOrderVO->setType('marketplace');
        $createOrderVO->setCoupon(new CreateCouponVO());

        $result = $this->getBasicService($entityManager, true, true)->insertOrder($createOrderVO);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function insertOrderCaseTwo()
    {
        $b2Pedprd = new B2cPedped();
        $b2Pedprd->setIdPedped(1);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('generateOrder')->andReturn($b2Pedprd);
        $entityManager->shouldReceive('generateProductsOrder')->andReturn(new B2cPedprd());
        $entityManager->shouldReceive('getSealsProducts')->andReturn([new B2cSelos()]);
        $entityManager->shouldReceive('insertOrderProductsSeals')->andReturn($entityManager);
        $entityManager->shouldReceive('insertOrderMarketplace')->andReturn($entityManager);

        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setProducts([new CreateProductVO()]);
        $createOrderVO->setCustomer(new CreateCustomerVO());
        $createOrderVO->setType('api');

        $result = $this->getBasicService($entityManager, true, true)->insertOrder($createOrderVO);
        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function updateStatusCaseOne()
    {
        $updateOrderStatusVO = new UpdateOrderStatusVO();
        $orderVO = new OrderVO();
        $status = new OrderStatusVO();
        $status->setId(B2cPedstatus::CANCELLED);
        $orderVO->setStatus($status);

        $result = $this->getBasicService()->updateStatus($updateOrderStatusVO, $orderVO);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function updateStatusCaseTwo()
    {
        $updateOrderStatusVO = new UpdateOrderStatusVO();
        $updateOrderStatusVO->setStatus(B2cPedstatus::APPROVED);

        $orderVO = new OrderVO();
        $status = new OrderStatusVO();
        $orderVO->setStatus($status);
        $orderVO->setProducts([new OrderProductVO()]);

        $result = $this->getBasicService()->updateStatus($updateOrderStatusVO, $orderVO);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function updateStatusCaseThree()
    {
        $updateOrderStatusVO = new UpdateOrderStatusVO();
        $updateOrderStatusVO->setStatus(B2cPedstatus::CANCELLED);

        $orderVO = new OrderVO();
        $status = new OrderStatusVO();
        $orderVO->setStatus($status);
        $orderVO->setProducts([new OrderProductVO()]);

        $result = $this->getBasicService()->updateStatus($updateOrderStatusVO, $orderVO);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function updateStatusCaseFour()
    {
        $updateOrderStatusVO = new UpdateOrderStatusVO();
        $updateOrderStatusVO->setStatus(B2cPedstatus::CANCELLED);
        $updateOrderStatusVO->setTypeReversal(2);

        $orderVO = new OrderVO();
        $status = new OrderStatusVO();
        $status->setId(B2cPedstatus::CANCELLED);
        $orderVO->setStatus($status);
        $orderVO->setProducts([new OrderProductVO()]);

        $result = $this->getBasicService()->updateStatus($updateOrderStatusVO, $orderVO);
        $this->assertTrue($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function createPurchaseOrderException()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('rollback')->andReturn($entityManager);

        $createPurchaseOrder = new CreatePurchaseOrderVO();
        $orderVO = new OrderVO();

        $this->getBasicService($entityManager)->createPurchaseOrder($createPurchaseOrder, $orderVO);
    }

    /**
     * @test
     */
    public function createPurchaseCaseOne()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('rollback')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderUpdateDate')->andReturn($entityManager);
        $entityManager->shouldReceive('findBy')->andReturn([new B2cPedprd()]);

        $createPurchaseOrder = new CreatePurchaseOrderVO();
        $createPurchaseOrder->setProducts([new CreatePurchaseOrderProductVO()]);
        $orderVO = new OrderVO();

        $result = $this->getBasicService($entityManager)->createPurchaseOrder($createPurchaseOrder, $orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function createPurchaseCaseTwo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('rollback')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderUpdateDate')->andReturn($entityManager);

        $b2cPedprd = new B2cPedprd();
        $b2cPedprd->setOrdemCompra(1);
        $entityManager->shouldReceive('findBy')->andReturn([$b2cPedprd]);

        $createPurchaseOrder = new CreatePurchaseOrderVO();
        $createPurchaseOrder->setProducts([new CreatePurchaseOrderProductVO()]);
        $orderVO = new OrderVO();

        $result = $this->getBasicService($entityManager)->createPurchaseOrder($createPurchaseOrder, $orderVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function getCommonOrderInfo()
    {
        $b2cPedprd = new B2cPedprd();
        $b2cPedprd->setPrdStqLoja(2);

        $b2cPedped = new B2cPedped();

        $b2cViewProdutos = new B2cViewProdutos();
        $b2cViewProdutos->setTamanho(25);

        $b2cPedped->setProducts([$b2cPedprd]);
        $status = new B2cPedsituacao();
        $internalStatus = new B2cStaint();

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findOneBy')->andReturn($b2cPedped, $status, $internalStatus, $b2cViewProdutos);
        $entityManager->shouldReceive('getRegion')->andReturn([
            'id' => 1,
            'regiao' => 'test',
            'dist' => 'test',
            'clienteEscolhe' => true
        ]);
        $entityManager->shouldReceive('getIBGECode')->andReturn('test');
        $entityManager->shouldReceive('getRelatedCategories')->andReturn([['category' => 1]]);

        $result = $this->getBasicService($entityManager)->getCommonOrderInfo(1);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getCommonCreateOrderInfo()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getRegion')->andReturn([
            'id' => 1,
            'regiao' => 'test',
            'dist' => 'test',
            'clienteEscolhe' => true
        ]);
        $entityManager->shouldReceive('getIBGECode')->andReturn('test');

        $product = [
            'id' => 1,
            'quantity' => 1,
            'kitId' => 1,
            'nome' => 'test',
            'referencia' => 1,
            'categoria' => '12',
            'destaque' => false
        ];

        $entityManager->shouldReceive('getProductRequestInfo')->andReturn([
            'products' => $product
        ]);
        $entityManager->shouldReceive('getProductInfoFromId')->andReturn([
            'id' => 1,
            'quantity' => 1,
            'kitId' => 1,
            'nome' => 'test',
            'referencia' => 1,
            'categoria' => '12',
            'destaque' => false,
            'estilo' => 1,
            'nomeCor' => '12',
            'sexo' => '12',
            'tamanho' => '12',
            'peso' => '12',
            'cubagem' => '12',
            'fornecedor' => '12',
            'nomeFornecedor' => '12',
            'prdPrazoFornecedor' => '12',
            'prazoFornecedor' => '12',
            'stqfornecedor' => '12',
            'stqfornReservado' => '12',
            'stqminAcao' => '12',
            'stqminimo' => '12',
            'acao' => '12',
            'mostra' => '12',
            'estoque' => '12',
            'disponivel' => '12',
            'esgotado' => '12',
            'disponibilidade' => '12',
            'dispDias' => '12',
            'bpromocao' => '12',
            'promocaoini' => '12',
            'promocaofim' => '12',
            'pvreal' => '12',
            'custo' => '12',
            'pvpromocao' => '12',
            'precoProduto' => '12'
        ]);

        $paymentInfo = [
            'id' => 1,
            'idPaymentForm' => 1,
            'indexer' => 1,
            'instalment' => 1,
            'instalmentValue' => 23.9,
            'name' => 'test',
            'description' => 'Test',
            'deadline' => 1,
            'paymentFormDesc' => 2
        ];

        $entityManager->shouldReceive('getPaymentInfo')->andReturn([$paymentInfo, $paymentInfo]);
        $entityManager->shouldReceive('getDateAddNextBusinessDay')->andReturn(date('Y-m-d'));
        $entityManager->shouldReceive('getRelatedCategories')->andReturn([['category' => 1]]);
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cFornecedorRegiao());

        $entityManager->shouldReceive('getActiveDiscountPromotionProduct')->andReturn([
            'idPromo' => '1',
            'descontoValor' => '1',
            'descontoPerc' => ''
        ]);

        $validateOrder = new ValidateOrder();
        $validateOrder->setOrigin(new ValidateOrigin());

        $result = $this->getBasicService($entityManager)->getCommonCreateOrderInfo($validateOrder);

        $this->assertNotNull($result);
    }
}
