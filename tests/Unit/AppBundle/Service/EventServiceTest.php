<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Service\Event\Factories\EventRabbitFactory;
use AppBundle\Service\EventService;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class EventServiceTest
 * @package Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class EventServiceTest extends PHPUnit_Framework_TestCase
{
    use LoggerTestTrait;

    /**
     * @test
     */
    public function send()
    {
        $event = Mockery::mock(EventRabbitFactory::class);
        $event->shouldReceive('send')->andReturn(true);
        $eventService = new EventService($event, $this->getLogger());

        $result = $eventService->send('/', ['test' => 'test']);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function sendFail()
    {
        $event = Mockery::mock(EventRabbitFactory::class);
        $eventService = new EventService($event, $this->getLogger());

        $result = $eventService->send('/', ['test' => 'test']);

        $this->assertFalse($result);
    }
}
