<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Service\CouponService;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\DataBuilder\CreateOrderVODataBuilder;

/**
 * Class CouponServiceTest
 * @package Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class CouponServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait {
        getEntityManager as protected traitEntityManager;
    }
    use LoggerTestTrait;

    private function getEntityManager()
    {
        $entityManager = $this->traitEntityManager();
        $entityManager->shouldReceive('validateCategoryCoupon')->andReturn(true);
        $entityManager->shouldReceive('validatePaymentCoupon')->andReturn([]);
        $entityManager->shouldReceive('validateDeliveryCoupon')->andReturn(true);
        $entityManager->shouldReceive('validateProviderCoupon')->andReturn(true);

        return $entityManager;
    }

    private function createOrderVO()
    {
        $createOrderVODataBuilder = new CreateOrderVODataBuilder();
        return $createOrderVODataBuilder->build();
    }

    /**
     * @test
     */
    public function validateCouponByCategoryWithDiscountPercentageGreaterThanZero()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getValidCouponFromCode')->andReturn([
            'usarCategoria' => 1,
            'idPromo' => 1,
            'descontoPerc' => 5.0,
            'descontoValor' => 1.22,
            'aplicarPagamento' => true
        ]);

        $couponService = new CouponService($entityManager, $this->getLogger());

        $result = $couponService->validateCoupon('abc123', $this->createOrderVO());

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function validateCouponByCategoryWithDiscountAmountGreaterThanZero()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getValidCouponFromCode')->andReturn([
            'usarCategoria' => 1,
            'idPromo' => 1,
            'descontoPerc' => 0,
            'descontoValor' => 1.22,
            'aplicarPagamento' => true
        ]);

        $couponService = new CouponService($entityManager, $this->getLogger());

        $result = $couponService->validateCoupon('abc123', $this->createOrderVO());

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function validateCouponBySupplierWithDiscountPercentageGreaterThanZero()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getValidCouponFromCode')->andReturn([
            'usarCategoria' => false,
            'usarFornecedor' => true,
            'idPromo' => 1,
            'descontoPerc' => 5.0,
            'descontoValor' => 1.22,
            'aplicarPagamento' => true
        ]);

        $couponService = new CouponService($entityManager, $this->getLogger());

        $result = $couponService->validateCoupon('abc123', $this->createOrderVO());

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function validateCouponBySupplierWithDiscountAmoountGreaterThanZero()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getValidCouponFromCode')->andReturn([
            'usarCategoria' => false,
            'usarFornecedor' => true,
            'idPromo' => 1,
            'descontoPerc' => 0,
            'descontoValor' => 1.22,
            'aplicarPagamento' => true
        ]);

        $couponService = new CouponService($entityManager, $this->getLogger());

        $result = $couponService->validateCoupon('abc123', $this->createOrderVO());

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function validateCouponByGenericCouponWithDiscountPercentageGreaterThanZero()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getValidCouponFromCode')->andReturn([
            'usarCategoria' => false,
            'usarFornecedor' => false,
            'idPromo' => 1,
            'descontoPerc' => 5.0,
            'descontoValor' => 1.22,
            'aplicarPagamento' => true
        ]);

        $couponService = new CouponService($entityManager, $this->getLogger());

        $result = $couponService->validateCoupon('abc123', $this->createOrderVO());

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function validateCouponByGenericCouponWithDiscountAmountGreaterThanZero()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getValidCouponFromCode')->andReturn([
            'usarCategoria' => false,
            'usarFornecedor' => false,
            'idPromo' => 1,
            'descontoPerc' => 0,
            'descontoValor' => 1.22,
            'aplicarPagamento' => true
        ]);

        $couponService = new CouponService($entityManager, $this->getLogger());

        $result = $couponService->validateCoupon('abc123', $this->createOrderVO());

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function validateCouponWithDiscountInShipAmount()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getValidCouponFromCode')->andReturn([
            'usarCategoria' => 1,
            'idPromo' => 1,
            'descontoPerc' => 5.0,
            'descontoValor' => 1.22,
            'aplicarPagamento' => false,
            'aplicarEntrega' => true,
            'freteGratis' => true,
            'freteFixo' => false,
            'freteFixoValor' => 29.9,
            'promoIncluiFrete' => true
        ]);

        $couponService = new CouponService($entityManager, $this->getLogger());

        $result = $couponService->validateCoupon('abc123', $this->createOrderVO());

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function validateCouponWithoutDiscountInShippingAmount()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getValidCouponFromCode')->andReturn([
            'usarCategoria' => 1,
            'idPromo' => 1,
            'descontoPerc' => 5.0,
            'descontoValor' => 1.22,
            'aplicarPagamento' => false,
            'aplicarEntrega' => false,
            'freteGratis' => true,
            'freteFixo' => false,
            'freteFixoValor' => 29.9,
            'promoIncluiFrete' => true
        ]);

        $couponService = new CouponService($entityManager, $this->getLogger());

        $result = $couponService->validateCoupon('abc123', $this->createOrderVO());

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function validateCouponWithAnInvalidCouponCode()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getValidCouponFromCode')->andReturn(false);

        $couponService = new CouponService($entityManager, $this->getLogger());

        $result = $couponService->validateCoupon('abc123', $this->createOrderVO());

        $this->assertNotNull($result);
    }
}
