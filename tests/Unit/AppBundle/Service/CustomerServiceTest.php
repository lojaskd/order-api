<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\B2cClicli;
use AppBundle\Entity\Validate\ValidateCustomer;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
use AppBundle\Service\CustomerService;
use PHPUnit_Framework_TestCase;

/**
 * Class CustomerServiceTest
 * @package Tests\Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class CustomerServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     */
    public function getOrCreateCustomer()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('addCustomer')->andReturn(new B2cClicli());

        $customerService = new CustomerService($entityManager, $this->getLogger());
        $customer = null;
        $orderCustomer = new ValidateCustomer();
        $address = '';

        $result = $customerService->getOrCreateCustomer($customer, $orderCustomer,$address);

        $this->assertNotNull($result);
        $this->assertInstanceOf(CreateCustomerVO::class, $result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function getOrCreateCustomerException()
    {
        $entityManager = $this->getEntityManager();

        $customerService = new CustomerService($entityManager, $this->getLogger());
        $customer = null;
        $orderCustomer = new ValidateCustomer();
        $address = '';

        $customerService->getOrCreateCustomer($customer, $orderCustomer,$address);
    }
}
