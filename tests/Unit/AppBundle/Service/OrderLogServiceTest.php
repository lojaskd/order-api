<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\VO\Create\CreateOrderLogVO;
use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Service\EventService;
use AppBundle\Service\OrderLogService;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class OrderLogServiceTest
 * @package Tests\Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class OrderLogServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     */
    public function insertOrderLog()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('insertOrderLog')->andReturn($entityManager);

        $event = Mockery::mock(EventService::class);
        $orderLogVO = new CreateOrderLogVO();

        $orderLogService = new OrderLogService($entityManager, $this->getLogger(), $event);
        $result = $orderLogService->insertOrderLog($orderLogVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function insertOrderLogException()
    {
        $entityManager = $this->getEntityManager();

        $event = Mockery::mock(EventService::class);
        $orderLogVO = new CreateOrderLogVO();

        $orderLogService = new OrderLogService($entityManager, $this->getLogger(), $event);
        $result = $orderLogService->insertOrderLog($orderLogVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function insertCreateOrderLog()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('insertOrderLog')->andReturn($entityManager);

        $event = Mockery::mock(EventService::class);
        $createOrderVO = new CreateOrderVO();
        $createOrderVO->setTotal(22.90);
        $createOrderVO->setProducts([new CreateProductVO()]);

        $orderLogService = new OrderLogService($entityManager, $this->getLogger(), $event);
        $result = $orderLogService->insertCreateOrderLog($createOrderVO);

        $this->assertTrue($result);
    }
}
