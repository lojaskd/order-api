<?php

namespace Unit\AppBundle\Service;

use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
use AppBundle\Service\EmailService;
use AppBundle\Service\QueueService;
use DateTime;
use Mockery;
use PHPUnit_Framework_TestCase;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;

/**
 * Class EmailServiceTest
 * @package Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class EmailServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function sendOrderEmail()
    {
        $engine = Mockery::mock(TwigEngine::class);

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendOrderEmail')->andReturn(true);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('customerAcceptsNewsletter')->andReturn(true);

        $emailService = new EmailService(
            $entityManager,
            $engine,
            $queueService,
            'smtp.google.com',
            '587',
            'user',
            '123456'
        );

        $b2cPedped = new B2cPedped();
        $createCustomerVO = new CreateCustomerVO();

        $result = $emailService->sendOrderEmail($b2cPedped, $createCustomerVO);

        $this->assertTrue($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function sendOrderEmailForBoletoBancario()
    {
        $engine = Mockery::mock(TwigEngine::class);
        $engine->shouldReceive('render')->andReturn($engine);

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendOrderEmail')->andReturn(false);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('customerAcceptsNewsletter')->andReturn(true);

        $emailService = new EmailService(
            $entityManager,
            $engine,
            $queueService,
            'smtp.google.com',
            '587',
            'user',
            '123456'
        );

        $createCustomerVO = new CreateCustomerVO();

        $pedBoletoBancario = new B2cPedped();
        $pedBoletoBancario->setPedDtaCompra(new DateTime());
        $pedBoletoBancario->setPedTipoPagamento1('Boleto Bancário');
        $pedBoletoBancario->setPedTipoPagamento2('Boleto Bancário');
        $boletoBancario = $emailService->sendOrderEmail($pedBoletoBancario, $createCustomerVO);
        $this->assertTrue($boletoBancario);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function sendOrderEmailForDepositoBancario()
    {
        $engine = Mockery::mock(TwigEngine::class);
        $engine->shouldReceive('render')->andReturn($engine);

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendOrderEmail')->andReturn(false);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('customerAcceptsNewsletter')->andReturn(true);

        $emailService = new EmailService(
            $entityManager,
            $engine,
            $queueService,
            'smtp.google.com',
            '587',
            'user',
            '123456'
        );

        $createCustomerVO = new CreateCustomerVO();

        $pedDepositoBancario = new B2cPedped();
        $pedDepositoBancario->setPedTipoPagamento1('Depósito Bancário');
        $pedDepositoBancario->setPedTipoPagamento2('Depósito Bancário');
        $pedDepositoBancario->setPedDtaCompra(new DateTime());
        $deposito = $emailService->sendOrderEmail($pedDepositoBancario, $createCustomerVO);
        $this->assertTrue($deposito);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function sendOrderEmailForPagSeguro()
    {
        $engine = Mockery::mock(TwigEngine::class);
        $engine->shouldReceive('render')->andReturn($engine);

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendOrderEmail')->andReturn(false);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('customerAcceptsNewsletter')->andReturn(true);

        $emailService = new EmailService(
            $entityManager,
            $engine,
            $queueService,
            'smtp.google.com',
            '587',
            'user',
            '123456'
        );

        $createCustomerVO = new CreateCustomerVO();

        $pedPagSeguro = new B2cPedped();
        $pedPagSeguro->setPedTipoPagamento1('PagSeguro');
        $pedPagSeguro->setPedTipoPagamento2('PagSeguro');
        $pedPagSeguro->setPedDtaCompra(new DateTime());
        $pagseguro = $emailService->sendOrderEmail($pedPagSeguro, $createCustomerVO);
        $this->assertTrue($pagseguro);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function sendOrderEmailManually()
    {
        $engine = Mockery::mock(TwigEngine::class);
        $engine->shouldReceive('render')->andReturn($engine);

        $queueService = Mockery::mock(QueueService::class);
        $queueService->shouldReceive('sendOrderEmail')->andReturn(false);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('customerAcceptsNewsletter')->andReturn(true);

        $emailService = new EmailService(
            $entityManager,
            $engine,
            $queueService,
            'smtp.google.com',
            '587',
            'user',
            '123456'
        );

        $b2cPedped = new B2cPedped();
        $b2cPedped->setPedDtaCompra(new DateTime());
        $createCustomerVO = new CreateCustomerVO();

        $result = $emailService->sendOrderEmail($b2cPedped, $createCustomerVO);

        $this->assertTrue($result);
    }
}
