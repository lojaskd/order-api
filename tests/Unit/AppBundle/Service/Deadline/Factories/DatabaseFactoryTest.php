<?php

namespace Tests\Unit\AppBundle\Service\Deadline\Factories;

use AppBundle\Service\Deadline\Factories\DatabaseFactory;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;
use Tests\Unit\AppBundle\Service\LoggerTestTrait;

/**
 * Class DatabaseFactoryTest
 * @package Tests\Unit\AppBundle\Service\Deadline\Factories
 *
 * @group Unit
 * @group Service
 */
class DatabaseFactoryTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     */
    public function calculateInvalid()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getProductDeadline')->andReturn(null);

        $databaseFactory = new DatabaseFactory($entityManager, $this->getLogger());

        $this->assertFalse($databaseFactory->calculate(1, date('Y-m-d'), '8888888', 1));
    }

    /**
     * @test
     */
    public function calculate()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getProductDeadline')->andReturn([
            'prazo_compra' => 1,
            'prazo_fornecedor' => 2,
            'prazo_agenda' => 5,
            'prazo_coleta' => 2,
            'prazo_recebimento' => 2,
            'prazo_nf' => 2,
            'prazo_separacao' => 1,
            'prazo_romaneio' => 2,
            'prazo_transporte' => 8
        ]);
        $entityManager->shouldReceive('getDateAddNextBusinessDay')->andReturn(date('Y-m-d'));
        $entityManager->shouldReceive('getDateDiffBusinessDay')->andReturn(2);

        $databaseFactory = new DatabaseFactory($entityManager, $this->getLogger());

        $result = $databaseFactory->calculate(1, date('Y-m-d'), '8888888', 1);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }

    /**
     * @test
     */
    public function calculateForImediateDelivery()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getProductDeadline')->andReturn([
            'prazo_compra' => 1,
            'prazo_fornecedor' => 2,
            'prazo_agenda' => 5,
            'prazo_coleta' => 2,
            'prazo_recebimento' => 2,
            'prazo_nf' => 2,
            'prazo_separacao' => 1,
            'prazo_romaneio' => 2,
            'prazo_transporte' => 8,
            'prazo_entrega_imediata' => 2
        ]);
        $entityManager->shouldReceive('getDateAddNextBusinessDay')->andReturn(date('Y-m-d'));
        $entityManager->shouldReceive('getDateDiffBusinessDay')->andReturn(2);

        $databaseFactory = new DatabaseFactory($entityManager, $this->getLogger());

        $result = $databaseFactory->calculate(1, date('Y-m-d'), '8888888', 1, true);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }
}
