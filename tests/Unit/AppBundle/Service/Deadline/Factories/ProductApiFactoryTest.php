<?php

namespace Tests\Unit\AppBundle\Service\Deadline\Factories;

use AppBundle\Service\Deadline\Factories\ProductApiFactory;
use AppBundle\Service\ProductAPIService;
use Mockery;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\LoggerTestTrait;

/**
 * Class ProductApiFactoryTest
 * @package Tests\Unit\AppBundle\Service\Deadline\Factories
 *
 * @group Unit
 * @group Service
 */
class ProductApiFactoryTest extends PHPUnit_Framework_TestCase
{
    use LoggerTestTrait;

    /**
     * @test
     */
    public function calculateFail()
    {
        $productApiService = Mockery::mock(ProductAPIService::class);
        $productApiService->shouldReceive('getDeadlines')->andReturn(null);

        $productApiFactory = new ProductApiFactory($productApiService, $this->getlogger());

        $this->assertFalse($productApiFactory->calculate(1, date('Y-m-d'), '888888', 1));
    }

    /**
     * @test
     */
    public function calculate()
    {
        $productApiService = Mockery::mock(ProductAPIService::class);
        $productApiService->shouldReceive('getDeadlines')->andReturn([
            'dates' => [
                'release' => date('Y-m-d'),
                'supplier_billing' => date('Y-m-d'),
                'supplier_invoice' => date('Y-m-d'),
                'supplier_delivery' => date('Y-m-d'),
                'invoice' => date('Y-m-d'),
                'shipping' => date('Y-m-d'),
                'delivery' => date('Y-m-d')
            ],
            'deadlines' => [
                'supplier' => 3,
                'total' => 5
            ]
        ]);

        $productApiFactory = new ProductApiFactory($productApiService, $this->getlogger());
        $result = $productApiFactory->calculate(1, date('Y-m-d'), '888888', 1);

        $this->assertNotNull($result);
        $this->assertInternalType('array', $result);
    }
}
