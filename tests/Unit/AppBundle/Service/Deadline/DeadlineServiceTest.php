<?php

namespace Tests\Unit\AppBundle\Service\Deadline;

use AppBundle\Service\Deadline\DeadlineService;
use AppBundle\Service\Deadline\Factories\DatabaseFactory;
use PHPUnit_Framework_TestCase;
use Tests\Unit\AppBundle\Service\EntityManagerTestTrait;
use Tests\Unit\AppBundle\Service\LoggerTestTrait;

/**
 * Class DeadlineServiceTest
 * @package Tests\Unit\AppBundle\Service\Deadline
 *
 * @group Unit
 * @group Service
 */
class DeadlineServiceTest extends PHPUnit_Framework_TestCase
{
    use LoggerTestTrait;
    use EntityManagerTestTrait;

    /**
     * @test
     */
    public function calculate()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getProductDeadline')->andReturn(null);

        $deadline = new DatabaseFactory($entityManager, $this->getLogger());
        $deadlineService = new DeadlineService($deadline, $this->getLogger());
        $this->assertNotNull($deadlineService->calculate(1, date('Y-m-d'), '8888888', 1));
    }
}
