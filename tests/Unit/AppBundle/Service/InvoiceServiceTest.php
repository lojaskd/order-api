<?php

namespace Tests\Unit\AppBundle\Service;

use AppBundle\Entity\B2cNotafiscal;
use AppBundle\Entity\B2cNotafiscalItem;
use AppBundle\Entity\B2cPedprd;
use AppBundle\Entity\Validate\Create\CreateValidateInvoice;
use AppBundle\Entity\Validate\Create\CreateValidateInvoiceItem;
use AppBundle\Entity\Validate\Update\UpdateValidateInvoice;
use AppBundle\Entity\Validate\ValidateInvoiceDate;
use AppBundle\Entity\VO\Create\CreateInvoiceItemVO;
use AppBundle\Entity\VO\Create\CreateInvoiceVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateInvoiceDateVO;
use AppBundle\Entity\VO\Update\UpdateInvoiceVO;
use AppBundle\Service\EventService;
use AppBundle\Service\InvoiceService;
use AppBundle\Service\ProductService;
use DateTime;
use Mockery;
use PHPUnit_Framework_TestCase;

/**
 * Class invoiceServiceTest
 * @package Unit\AppBundle\Service
 *
 * @group Unit
 * @group Service
 */
class InvoiceServiceTest extends PHPUnit_Framework_TestCase
{
    use EntityManagerTestTrait;
    use LoggerTestTrait;

    /**
     * @test
     */
    public function getCommonInfoInvoiceDate()
    {
        $productService = Mockery::mock(ProductService::class);
        $eventService = Mockery::mock(EventService::class);

        $invoiceService = new InvoiceService($this->getEntityManager(), $this->getLogger(), $productService, $eventService);

        $result = $invoiceService->getCommonInfoInvoiceDate(new ValidateInvoiceDate());

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function getCommonInvoiceInfo()
    {
        $productService = Mockery::mock(ProductService::class);
        $eventService = Mockery::mock(EventService::class);

        $invoiceService = new InvoiceService($this->getEntityManager(), $this->getLogger(), $productService, $eventService);

        $createValidateInvoice = new CreateValidateInvoice();
        $createValidateInvoice->setCreatedDate(new DateTime());
        $createValidateInvoice->setPredictDeliveryDate(new DateTime());
        $createValidateInvoice->setDeliveryDate(new DateTime());
        $createValidateInvoice->setReturnedDate(new DateTime());

        $item = new CreateValidateInvoiceItem();
        $item->setReturnedDate(new DateTime());

        $createValidateInvoice->setItens([$item]);

        $result = $invoiceService->getCommonInvoiceInfo($createValidateInvoice);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function updateDate()
    {
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('updateDate')->andReturn($productService);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cNotafiscal());
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('findBy')->andReturn([new B2cPedprd()]);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);

        $invoiceService = new InvoiceService($entityManager, $this->getLogger(), $productService, $eventService);

        $invoiceDate = new UpdateInvoiceDateVO();
        $orderVO = new OrderVO();

        $result = $invoiceService->updateDate($invoiceDate, $orderVO);

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function createInvoice()
    {
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('updateDate')->andReturn($productService);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('findOneBy')->andReturn(null, new B2cPedprd());
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('createInvoice')->andReturn(new B2cNotafiscal());
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderUpdateDate')->andReturn($entityManager);

        $invoiceService = new InvoiceService($entityManager, $this->getLogger(), $productService, $eventService);

        $createInvoice = new CreateInvoiceVO();
        $createInvoice->setItens([new CreateInvoiceItemVO()]);

        $result = $invoiceService->createInvoice($createInvoice, new OrderVO());

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function getCommonUpdateInvoiceInfo()
    {
        $entityManager = $this->getEntityManager();
        $productService = Mockery::mock(ProductService::class);
        $eventService = Mockery::mock(EventService::class);

        $invoiceService = new InvoiceService($entityManager, $this->getLogger(), $productService, $eventService);

        $result = $invoiceService->getCommonUpdateInvoiceInfo(new UpdateValidateInvoice());

        $this->assertNotNull($result);
    }

    /**
     * @test
     */
    public function updateInvoice()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('getTransactionNestingLevel')->andReturn($entityManager);
        $entityManager->shouldReceive('rollback')->andReturn($entityManager);
        $entityManager->shouldReceive('commit')->andReturn($entityManager);
        $entityManager->shouldReceive('updateOrderUpdateDate')->andReturn($entityManager);
        $entityManager->shouldReceive('findOneBy')->andReturn(new B2cNotafiscal());

        $product = new B2cPedprd();
        $product->setIdPrdprd(1);

        $entityManager->shouldReceive('findBy')->andReturn([new B2cNotafiscalItem()], [$product]);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('updateDate')->andReturn($productService);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $invoiceService = new InvoiceService($entityManager, $this->getLogger(), $productService, $eventService);

        $updateInvoiceVO = new UpdateInvoiceVO();
        $updateInvoiceVO->setReturned(true);
        $updateInvoiceVO->setMarketplaceIntegration(true);
        $updateInvoiceVO->setKey('123456');
        $updateInvoiceVO->setDateKey(new DateTime());
        $updateInvoiceVO->setPathXml('/test.xml');
        $updateInvoiceVO->setPathPdf('/test.pdf');
        $updateInvoiceVO->setShipperName('TNT');
        $updateInvoiceVO->setShipperDoc('abc');
        $updateInvoiceVO->setHub('MAIN');

        $result = $invoiceService->updateInvoice($updateInvoiceVO, new OrderVO());

        $this->assertTrue($result);
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function updateInvoiceException()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('findOneBy')->andReturn(null);

        $product = new B2cPedprd();
        $product->setIdPrdprd(1);

        $entityManager->shouldReceive('findBy')->andReturn([new B2cNotafiscalItem()], [$product]);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('updateDate')->andReturn($productService);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $invoiceService = new InvoiceService($entityManager, $this->getLogger(), $productService, $eventService);

        $updateInvoiceVO = new UpdateInvoiceVO();

        $invoiceService->updateInvoice($updateInvoiceVO, new OrderVO());
    }

    /**
     * @test
     * @expectedException \Exception
     */
    public function updateInvoiceMainException()
    {
        $entityManager = $this->getEntityManager();
        $entityManager->shouldReceive('getConnection')->andReturn($entityManager);
        $entityManager->shouldReceive('beginTransaction')->andReturn($entityManager);
        $entityManager->shouldReceive('getTransactionNestingLevel')->andReturn($entityManager);
        $entityManager->shouldReceive('rollback')->andReturn($entityManager);

        $product = new B2cPedprd();
        $product->setIdPrdprd(1);

        $entityManager->shouldReceive('findBy')->andReturn([new B2cNotafiscalItem()], [$product]);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('updateDate')->andReturn($productService);

        $eventService = Mockery::mock(EventService::class);
        $eventService->shouldReceive('send')->andReturn(true);

        $invoiceService = new InvoiceService($entityManager, $this->getLogger(), $productService, $eventService);

        $updateInvoiceVO = new UpdateInvoiceVO();

        $invoiceService->updateInvoice($updateInvoiceVO, new OrderVO());
    }
}
