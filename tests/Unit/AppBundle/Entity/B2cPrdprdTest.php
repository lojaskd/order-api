<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdprd;
        
/**
 * Class B2cPrdprdTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdprdTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdprd();
                
        $entity->setMatriz('1');
        $entity->setDtaInclusao(new DateTime());
        $entity->setDtaAlteracao(new DateTime());
        $entity->setUsuarioAlteracao('1');
        $entity->setFilaInclusao('1');
        $entity->setFilaAtualizacao('1');
        $entity->setFilaExclusao('1');
        $entity->setNMostrar('1');
        $entity->setReferencia('1');
        $entity->setNome('1');
        $entity->setCategoria('1');
        $entity->setFornecedor('1');
        $entity->setCrossSaleGrupo1('1');
        $entity->setCrossSaleGrupo2('1');
        $entity->setIgnorarAutoCrosssale('1');
        $entity->setCrossSaleDisponivel('1');
        $entity->setChamada('1');
        $entity->setDescricao('1');
        $entity->setPchaves('1');
        $entity->setLink('1');
        $entity->setOrdemLst('1');
        $entity->setOrdemPrd('1');
        $entity->setDisponivel('1');
        $entity->setDispDias('1');
        $entity->setDestaque('1');
        $entity->setPrevenda('1');
        $entity->setLancamento('1');
        $entity->setDtiniciolancamento(new DateTime());
        $entity->setDtfimlancamento(new DateTime());
        $entity->setDtchegada(new DateTime());
        $entity->setPresente('1');
        $entity->setMaisvendido('1');
        $entity->setMaisVendOrdem('1');
        $entity->setTamanho('1');
        $entity->setSexo('1');
        $entity->setCor('1');
        $entity->setEstilo('1');
        $entity->setPeso('1');
        $entity->setCubagem('1');
        $entity->setItens('1');
        $entity->setVolumes('1');
        $entity->setEstoque('1');
        $entity->setStqminimo('1');
        $entity->setStqminAcao('1');
        $entity->setStqfornecedor('1');
        $entity->setCusto('1');
        $entity->setIcms('1');
        $entity->setPvreal('1');
        $entity->setPrazoFornecedor('1');
        $entity->setPromocao('1');
        $entity->setPvpromocao('1');
        $entity->setDescontoValor('1');
        $entity->setDescontoPerc('1');
        $entity->setUseDesc('1');
        $entity->setPromocaoini(new DateTime());
        $entity->setPromocaofim(new DateTime());
        $entity->setLinkVideo('1');
        $entity->setLinkExtra('1');
        $entity->setCorNome('1');
        $entity->setVndProduto1('1');
        $entity->setVndDesconto1('1');
        $entity->setVndProduto2('1');
        $entity->setVndDesconto2('1');
        $entity->setIgnorarVendaJunta('1');
        $entity->setProdutoKit('1');
        $entity->setListaPrdKit('1');
        $entity->setDescKit('1');
        $entity->setCodigoerp('1');
        $entity->setUnidadeFracionada('1');
        $entity->setAltura('1');
        $entity->setLargura('1');
        $entity->setProfundidade('1');
        $entity->setDiametro('1');
        $entity->setNomeUnico('1');
        $entity->setSubsidiarFrete('1');
        $entity->setSfProduct('1');
        $entity->setSfPricebook('1');
        $entity->setSfPricebookentry('1');
        $entity->setListaAmbiente('1');
        $entity->setNegociacaoIni(new DateTime());
        $entity->setNegociacaoFim(new DateTime());
        $entity->setSaindoDeLinhaIni(new DateTime());
        $entity->setSaindoDeLinhaFim(new DateTime());
        $entity->setTipoLst('1');
        $entity->setTooltipSaindoDeLinha('1');
        $entity->setTooltipNegociacao('1');
        $entity->setTooltipPromocao('1');
        $entity->setTooltipCor('1');
        $entity->setRelogioPromocao('1');
        $entity->setStqfornilimitado('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getMatriz());
        $this->assertNotNull($entity->getDtaInclusao());
        $this->assertNotNull($entity->getDtaAlteracao());
        $this->assertNotNull($entity->getUsuarioAlteracao());
        $this->assertNotNull($entity->getFilaInclusao());
        $this->assertNotNull($entity->getFilaAtualizacao());
        $this->assertNotNull($entity->getFilaExclusao());
        $this->assertNotNull($entity->getNMostrar());
        $this->assertNotNull($entity->getReferencia());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getCategoria());
        $this->assertNotNull($entity->getFornecedor());
        $this->assertNotNull($entity->getCrossSaleGrupo1());
        $this->assertNotNull($entity->getCrossSaleGrupo2());
        $this->assertNotNull($entity->getIgnorarAutoCrosssale());
        $this->assertNotNull($entity->getCrossSaleDisponivel());
        $this->assertNotNull($entity->getChamada());
        $this->assertNotNull($entity->getDescricao());
        $this->assertNotNull($entity->getPchaves());
        $this->assertNotNull($entity->getLink());
        $this->assertNotNull($entity->getOrdemLst());
        $this->assertNotNull($entity->getOrdemPrd());
        $this->assertNotNull($entity->getDisponivel());
        $this->assertNotNull($entity->getDispDias());
        $this->assertNotNull($entity->getDestaque());
        $this->assertNotNull($entity->getPrevenda());
        $this->assertNotNull($entity->getLancamento());
        $this->assertNotNull($entity->getDtiniciolancamento());
        $this->assertNotNull($entity->getDtfimlancamento());
        $this->assertNotNull($entity->getDtchegada());
        $this->assertNotNull($entity->getPresente());
        $this->assertNotNull($entity->getMaisvendido());
        $this->assertNotNull($entity->getMaisVendOrdem());
        $this->assertNotNull($entity->getTamanho());
        $this->assertNotNull($entity->getSexo());
        $this->assertNotNull($entity->getCor());
        $this->assertNotNull($entity->getEstilo());
        $this->assertNotNull($entity->getPeso());
        $this->assertNotNull($entity->getCubagem());
        $this->assertNotNull($entity->getItens());
        $this->assertNotNull($entity->getVolumes());
        $this->assertNotNull($entity->getEstoque());
        $this->assertNotNull($entity->getStqminimo());
        $this->assertNotNull($entity->getStqminAcao());
        $this->assertNotNull($entity->getStqfornecedor());
        $this->assertNotNull($entity->getCusto());
        $this->assertNotNull($entity->getIcms());
        $this->assertNotNull($entity->getPvreal());
        $this->assertNotNull($entity->getPrazoFornecedor());
        $this->assertNotNull($entity->getPromocao());
        $this->assertNotNull($entity->getPvpromocao());
        $this->assertNotNull($entity->getDescontoValor());
        $this->assertNotNull($entity->getDescontoPerc());
        $this->assertNotNull($entity->getUseDesc());
        $this->assertNotNull($entity->getPromocaoini());
        $this->assertNotNull($entity->getPromocaofim());
        $this->assertNotNull($entity->getLinkVideo());
        $this->assertNotNull($entity->getLinkExtra());
        $this->assertNotNull($entity->getCorNome());
        $this->assertNotNull($entity->getVndProduto1());
        $this->assertNotNull($entity->getVndDesconto1());
        $this->assertNotNull($entity->getVndProduto2());
        $this->assertNotNull($entity->getVndDesconto2());
        $this->assertNotNull($entity->getIgnorarVendaJunta());
        $this->assertNotNull($entity->getProdutoKit());
        $this->assertNotNull($entity->getListaPrdKit());
        $this->assertNotNull($entity->getDescKit());
        $this->assertNotNull($entity->getCodigoerp());
        $this->assertNotNull($entity->getUnidadeFracionada());
        $this->assertNotNull($entity->getAltura());
        $this->assertNotNull($entity->getLargura());
        $this->assertNotNull($entity->getProfundidade());
        $this->assertNotNull($entity->getDiametro());
        $this->assertNotNull($entity->getNomeUnico());
        $this->assertNotNull($entity->getSubsidiarFrete());
        $this->assertNotNull($entity->getSfProduct());
        $this->assertNotNull($entity->getSfPricebook());
        $this->assertNotNull($entity->getSfPricebookentry());
        $this->assertNotNull($entity->getListaAmbiente());
        $this->assertNotNull($entity->getNegociacaoIni());
        $this->assertNotNull($entity->getNegociacaoFim());
        $this->assertNotNull($entity->getSaindoDeLinhaIni());
        $this->assertNotNull($entity->getSaindoDeLinhaFim());
        $this->assertNotNull($entity->getTipoLst());
        $this->assertNotNull($entity->getTooltipSaindoDeLinha());
        $this->assertNotNull($entity->getTooltipNegociacao());
        $this->assertNotNull($entity->getTooltipPromocao());
        $this->assertNotNull($entity->getTooltipCor());
        $this->assertNotNull($entity->getRelogioPromocao());
        $this->assertNotNull($entity->getStqfornilimitado());
    }
}
