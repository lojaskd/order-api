<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\OrderPaymentVO;
        
/**
 * Class OrderPaymentVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class OrderPaymentVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new OrderPaymentVO();
                
        $entity->setId('1');
        $entity->setIdPaymentForm('1');
        $entity->setIndexer('1');
        $entity->setInstalment('1');
        $entity->setInstalmentValue('1');
        $entity->setName('1');
        $entity->setDescription('1');
        $entity->setDeadline('1');
        $entity->setPaymentFormDesc('1');
        $entity->setBillingAccount('1');
        $entity->setBillingAccountName('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getIdPaymentForm());
        $this->assertNotNull($entity->getIndexer());
        $this->assertNotNull($entity->getInstalment());
        $this->assertNotNull($entity->getInstalmentValue());
        $this->assertNotNull($entity->getName());
        $this->assertNotNull($entity->getDescription());
        $this->assertNotNull($entity->getDeadline());
        $this->assertNotNull($entity->getPaymentFormDesc());
        $this->assertNotNull($entity->getPaymentTotalAmount());
        $this->assertNotNull($entity->getBillingAccount());
        $this->assertNotNull($entity->getBillingAccountName());
    }
}
