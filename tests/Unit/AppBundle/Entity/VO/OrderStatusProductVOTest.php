<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\OrderStatusProductVO;
        
/**
 * Class OrderStatusProductVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class OrderStatusProductVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new OrderStatusProductVO();
                
        $entity->setProductId('1');
        $entity->setQuantity('1');
        $entity->setProviderDays('1');
        $entity->setAvailabilityDays('1');
        $entity->setProviderDeadline('1');
        $entity->setProviderStock('1');
        $entity->setStoreStock('1');
        $entity->setBranchStockId('1');

        $this->assertNotNull($entity->getProductId());
        $this->assertNotNull($entity->getQuantity());
        $this->assertNotNull($entity->getProviderDays());
        $this->assertNotNull($entity->getAvailabilityDays());
        $this->assertNotNull($entity->getProviderDeadline());
        $this->assertNotNull($entity->getProviderStock());
        $this->assertNotNull($entity->getStoreStock());
        $this->assertNotNull($entity->getBranchStockId());
    }
}
