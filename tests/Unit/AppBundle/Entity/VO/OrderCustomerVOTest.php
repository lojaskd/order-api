<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\OrderCustomerVO;
        
/**
 * Class OrderCustomerVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class OrderCustomerVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new OrderCustomerVO();
                
        $entity->setId('1');
        $entity->setName('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getName());
    }
}
