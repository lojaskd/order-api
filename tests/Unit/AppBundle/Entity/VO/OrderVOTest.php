<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO;
        
use AppBundle\Entity\VO\Create\CreateRegionVO;
use AppBundle\Entity\VO\Create\CreateShippingVO;
use AppBundle\Entity\VO\OrderAddressVO;
use AppBundle\Entity\VO\OrderCustomerVO;
use AppBundle\Entity\VO\OrderPaymentVO;
use AppBundle\Entity\VO\OrderProductVO;
use AppBundle\Entity\VO\OrderStatusVO;
use AppBundle\Entity\VO\OrderVO;
use DateTime;
use PHPUnit_Framework_TestCase;

/**
 * Class OrderVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class OrderVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new OrderVO();
                
        $entity->setOrderId('1');
        $entity->setCustomer(new OrderCustomerVO());
        $entity->setProducts([new OrderProductVO()]);
        $entity->setDeliveryAddress(new OrderAddressVO());
        $entity->setBillingAddress(new OrderAddressVO());
        $entity->setStatus(new OrderStatusVO());
        $entity->setInternalStatus(new OrderStatusVO());
        $entity->setMarketplaceId('1');

        $firstPayment = new OrderPaymentVO();
        $firstPayment->setId(1);
        $entity->setFirstPayment($firstPayment);

        $secondPayment = new OrderPaymentVO();
        $secondPayment->setId(2);
        $entity->setSecondPayment($secondPayment);

        $entity->setPredictDeliveryDate(new DateTime());
        $entity->setSubtotal('1');
        $entity->setTotal('1');
        $entity->setDiscount('1');
        $entity->setCoupon('1');
        $entity->setCouponAmount('1');
        $entity->setShippingAmount('1');
        $entity->setRegionName('1');
        $entity->setShipping(new CreateShippingVO);
        $entity->setRegion(new CreateRegionVO);
        $entity->addProduct(new OrderProductVO());
        $entity->addProduct(new OrderProductVO(), 0);

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getCustomer());
        $this->assertNotNull($entity->getProducts());
        $this->assertNotNull($entity->getProductsId());
        $this->assertNotNull($entity->getProductsQuantity());
        $this->assertNotNull($entity->getTotalCubage());
        $this->assertNotNull($entity->getTotalWeight());
        $this->assertNotNull($entity->getTotalQuantity());
        $this->assertNotNull($entity->getCategories());
        $this->assertNotNull($entity->getProviders());
        $this->assertNotNull($entity->getPaymentsId());
        $this->assertNotNull($entity->getDeliveryAddress());
        $this->assertNotNull($entity->getBillingAddress());
        $this->assertNotNull($entity->getStatus());
        $this->assertNotNull($entity->getInternalStatus());
        $this->assertNotNull($entity->getMarketplaceId());
        $this->assertNotNull($entity->getFirstPayment());
        $this->assertNotNull($entity->getSecondPayment());
        $this->assertNotNull($entity->getPredictDeliveryDate());
        $this->assertNotNull($entity->getSubtotal());
        $this->assertNotNull($entity->getTotal());
        $this->assertNotNull($entity->getDiscount());
        $this->assertNotNull($entity->getCoupon());
        $this->assertNotNull($entity->getCouponAmount());
        $this->assertNotNull($entity->getShippingAmount());
        $this->assertNotNull($entity->getRegionName());
        $this->assertNotNull($entity->getShipping());
        $this->assertNotNull($entity->getRegion());
        $this->assertNotNull($entity->getPaymentsId());
    }
}
