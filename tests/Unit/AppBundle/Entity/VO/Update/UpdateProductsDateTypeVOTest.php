<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateProductsDateTypeVO;
        
/**
 * Class UpdateProductsDateTypeVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateProductsDateTypeVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateProductsDateTypeVO();
                
        $entity->setType('1');
        $entity->setDate('1');

        $this->assertNotNull($entity->getType());
        $this->assertNotNull($entity->getDate());
    }
}
