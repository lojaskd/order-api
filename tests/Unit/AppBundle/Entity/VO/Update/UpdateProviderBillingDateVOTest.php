<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateProviderBillingDateVO;
        
/**
 * Class UpdateProviderBillingDateVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateProviderBillingDateVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateProviderBillingDateVO();
                
        $entity->setOrderId('1');
        $entity->setPurchaseOrder('1');
        $entity->setType('1');
        $entity->setDate('1');
        $entity->setClerk('1');

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getPurchaseOrder());
        $this->assertNotNull($entity->getType());
        $this->assertNotNull($entity->getDate());
        $this->assertNotNull($entity->getClerk());
    }
}
