<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use AppBundle\Entity\VO\Update\UpdateProductsStatusProductVO;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateProductsStatusVO;
        
/**
 * Class UpdateProductsStatusVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateProductsStatusVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateProductsStatusVO();
                
        $entity->setOrderId('1');
        $entity->setStatus('1');
        $entity->setClerk('1');
        $entity->setCancelled('1');
        $entity->setTicket('1');
        $entity->setProducts([new UpdateProductsStatusProductVO()]);
        $entity->setOcrCode('1');
        $entity->setCancellationIndicator('1');
        $entity->setTicketType('1');
        $entity->setHub('1');
        $entity->setShipper('1');
        $entity->setInvoice('1');
        $entity->setProductCauser('1');
        $entity->addProduct(new UpdateProductsStatusProductVO());

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getStatus());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->isCancelled());
        $this->assertNotNull($entity->getTicket());
        $this->assertNotNull($entity->getProducts());
        $this->assertNotNull($entity->getOcrCode());
        $this->assertNotNull($entity->getCancellationIndicator());
        $this->assertNotNull($entity->getTicketType());
        $this->assertNotNull($entity->getHub());
        $this->assertNotNull($entity->getShipper());
        $this->assertNotNull($entity->getInvoice());
        $this->assertNotNull($entity->getProductCauser());
        $this->assertNotNull($entity->getCancelled());
    }
}
