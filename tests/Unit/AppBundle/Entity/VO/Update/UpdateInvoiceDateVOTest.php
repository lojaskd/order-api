<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateInvoiceDateVO;
        
/**
 * Class UpdateInvoiceDateVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateInvoiceDateVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateInvoiceDateVO();
                
        $entity->setOrderId('1');
        $entity->setInvoiceId('1');
        $entity->setType('1');
        $entity->setDate('1');
        $entity->setClerk('1');

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getInvoiceId());
        $this->assertNotNull($entity->getType());
        $this->assertNotNull($entity->getDate());
        $this->assertNotNull($entity->getClerk());
    }
}
