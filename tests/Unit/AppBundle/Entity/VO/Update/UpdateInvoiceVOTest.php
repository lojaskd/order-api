<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateInvoiceVO;
        
/**
 * Class UpdateInvoiceVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateInvoiceVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateInvoiceVO();
                
        $entity->setOrderId('1');
        $entity->setInvoiceId('1');
        $entity->setKey('1');
        $entity->setDateKey(new DateTime());
        $entity->setMarketplaceIntegration('1');
        $entity->setPathXml('1');
        $entity->setPathPdf('1');
        $entity->setReturnedDate(new DateTime());
        $entity->setReturned('1');
        $entity->setClerk('1');
        $entity->setShipperName('1');
        $entity->setShipperDoc('1');
        $entity->setHub('1');

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getInvoiceId());
        $this->assertNotNull($entity->getKey());
        $this->assertNotNull($entity->getDateKey());
        $this->assertNotNull($entity->getReturnedDate());
        $this->assertNotNull($entity->isReturned());
        $this->assertNotNull($entity->isMarketplaceIntegration());
        $this->assertNotNull($entity->getPathXml());
        $this->assertNotNull($entity->getPathPdf());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->getShipperName());
        $this->assertNotNull($entity->getShipperDoc());
        $this->assertNotNull($entity->getHub());
        $this->assertNotNull($entity->getMarketplaceIntegration());
        $this->assertNotNull($entity->getReturned());
    }
}
