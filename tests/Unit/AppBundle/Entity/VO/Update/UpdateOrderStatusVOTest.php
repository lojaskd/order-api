<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
        
/**
 * Class UpdateOrderStatusVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateOrderStatusVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateOrderStatusVO();
                
        $entity->setOrderId('1');
        $entity->setOrderCancelled('1');
        $entity->setStatus('1');
        $entity->setClerk('1');
        $entity->setInternalStatus('1');
        $entity->setDate('1');
        $entity->setTypeReversal('1');
        $entity->setTicket('1');
        $entity->setOperator('1');
        $entity->setTransaction('1');
        $entity->setOcrCode('1');
        $entity->setCancellationIndicator('1');
        $entity->setTicketType('1');
        $entity->setHub('1');
        $entity->setShipper('1');
        $entity->setInvoice('1');
        $entity->setProductCauser('1');

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getOrderCancelled());
        $this->assertNotNull($entity->getStatus());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->getInternalStatus());
        $this->assertNotNull($entity->getDate());
        $this->assertNotNull($entity->getTypeReversal());
        $this->assertNotNull($entity->getTicket());
        $this->assertNotNull($entity->getOperator());
        $this->assertNotNull($entity->getTransaction());
        $this->assertNotNull($entity->getOcrCode());
        $this->assertNotNull($entity->getCancellationIndicator());
        $this->assertNotNull($entity->getTicketType());
        $this->assertNotNull($entity->getHub());
        $this->assertNotNull($entity->getShipper());
        $this->assertNotNull($entity->getInvoice());
        $this->assertNotNull($entity->getProductCauser());
    }
}
