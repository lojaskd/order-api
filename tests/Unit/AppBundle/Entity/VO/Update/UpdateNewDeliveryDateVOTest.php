<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateNewDeliveryDateVO;
        
/**
 * Class UpdateNewDeliveryDateVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateNewDeliveryDateVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateNewDeliveryDateVO();
                
        $entity->setOrderId('1');
        $entity->setNewDeliveryDate('1');
        $entity->setAccept('1');
        $entity->setUser('1');

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getNewDeliveryDate());
        $this->assertNotNull($entity->isAccept());
        $this->assertNotNull($entity->getUser());
    }
}
