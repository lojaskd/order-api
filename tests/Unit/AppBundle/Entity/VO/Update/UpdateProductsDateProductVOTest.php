<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateProductsDateProductVO;
        
/**
 * Class UpdateProductsDateProductVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateProductsDateProductVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateProductsDateProductVO();
                
        $entity->setId('1');
        $entity->setDestination('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getDestination());
    }
}
