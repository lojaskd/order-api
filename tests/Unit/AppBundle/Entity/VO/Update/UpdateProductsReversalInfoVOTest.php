<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use AppBundle\Entity\VO\Update\UpdateProductsStatusProductVO;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateProductsReversalInfoVO;
        
/**
 * Class UpdateProductsReversalInfoVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateProductsReversalInfoVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateProductsReversalInfoVO();
                
        $entity->setOrderId('1');
        $entity->setClerk('1');
        $entity->setTypeReversal('1');
        $entity->setOperator('1');
        $entity->setTransaction('1');
        $entity->setProducts([new UpdateProductsStatusProductVO()]);
        $entity->addProduct(new UpdateProductsStatusProductVO());

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->getTypeReversal());
        $this->assertNotNull($entity->getOperator());
        $this->assertNotNull($entity->getTransaction());
        $this->assertNotNull($entity->getProducts());
    }
}
