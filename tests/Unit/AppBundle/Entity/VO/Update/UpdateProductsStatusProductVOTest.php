<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateProductsStatusProductVO;
        
/**
 * Class UpdateProductsStatusProductVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateProductsStatusProductVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateProductsStatusProductVO();
                
        $entity->setId('1');
        $entity->setQuantity('1');
        $entity->setReference('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getQuantity());
        $this->assertNotNull($entity->getReference());
    }
}
