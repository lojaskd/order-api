<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Update;
        
use AppBundle\Entity\VO\Update\UpdateProductsDateProductVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateTypeVO;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Update\UpdateProductsDateVO;
        
/**
 * Class UpdateProductsDateVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateProductsDateVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateProductsDateVO();
                
        $entity->setClerk('1');
        $entity->setOrderId('1');
        $entity->setProducts([new UpdateProductsDateProductVO()]);
        $entity->setDateType([new UpdateProductsDateTypeVO()]);
        $entity->setPredict('1');
        $entity->setDateTypes([new UpdateProductsDateTypeVO()]);
        $entity->addDateType(new UpdateProductsDateTypeVO());
        $entity->addProduct(new UpdateProductsDateProductVO());

        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getProducts());
        $this->assertNotNull($entity->getDateTypes());
        $this->assertNotNull($entity->isPredict());
        $this->assertNotNull($entity->getPredict());
    }
}
