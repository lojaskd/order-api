<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\ProductVO;
        
/**
 * Class ProductVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ProductVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ProductVO();
                
        $entity->setId('1');
        $entity->setName('1');
        $entity->setQuantity('1');
        $entity->setSize('1');
        $entity->setCubage('1');
        $entity->setWeight('1');
        $entity->setCategory('1');
        $entity->setRelatedCategories(['1']);
        $entity->setProvider('1');
        $entity->setProviderName('1');
        $entity->addRelatedCategory(2);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getName());
        $this->assertNotNull($entity->getQuantity());
        $this->assertNotNull($entity->getSize());
        $this->assertNotNull($entity->getCubage());
        $this->assertNotNull($entity->getWeight());
        $this->assertNotNull($entity->getCategory());
        $this->assertNotNull($entity->getRelatedCategories());
        $this->assertNotNull($entity->getProvider());
        $this->assertNotNull($entity->getProviderName());
    }
}
