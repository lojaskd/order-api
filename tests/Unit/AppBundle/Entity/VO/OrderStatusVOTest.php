<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\OrderStatusVO;
        
/**
 * Class OrderStatusVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class OrderStatusVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new OrderStatusVO();
                
        $entity->setId('1');
        $entity->setName('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getName());
    }
}
