<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\OrderProductVO;
        
/**
 * Class OrderProductVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class OrderProductVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new OrderProductVO();
                
        $entity->setProductId('1');
        $entity->setOrderId('1');
        $entity->setReference('1');
        $entity->setName('1');
        $entity->setSupplierStock('1');
        $entity->setSupplierReservedStock('1');
        $entity->setProductStock('1');
        $entity->setCancelled('1');
        $entity->setQuantity('1');
        $entity->setBranchStockId('1');
        $entity->setImmediateDelivery('1');
        $entity->setReservedStockDelivery('1');
        $entity->setDestination('1');
        $entity->setSupplierId('1');
        $entity->setId('1');
        $entity->setSize('1');
        $entity->setCubage('1');
        $entity->setWeight('1');
        $entity->setCategory('1');
        $entity->setRelatedCategories('1');
        $entity->setProvider('1');
        $entity->setProviderName('1');

        $this->assertNotNull($entity->getProductId());
        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getReference());
        $this->assertNotNull($entity->getName());
        $this->assertNotNull($entity->getSupplierStock());
        $this->assertNotNull($entity->getSupplierReservedStock());
        $this->assertNotNull($entity->getProductStock());
        $this->assertNotNull($entity->isCancelled());
        $this->assertNotNull($entity->getQuantity());
        $this->assertNotNull($entity->getBranchStockId());
        $this->assertNotNull($entity->isImmediateDelivery());
        $this->assertNotNull($entity->isReservedStockDelivery());
        $this->assertNotNull($entity->getDestination());
        $this->assertNotNull($entity->getSupplierId());
        $this->assertNotNull($entity->getCancelled());
        $this->assertNotNull($entity->getImmediateDelivery());
        $this->assertNotNull($entity->getReservedStockDelivery());
        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getSize());
        $this->assertNotNull($entity->getCubage());
        $this->assertNotNull($entity->getWeight());
        $this->assertNotNull($entity->getCategory());
        $this->assertNotNull($entity->getRelatedCategories());
        $this->assertNotNull($entity->getProvider());
        $this->assertNotNull($entity->getProviderName());
    }
}
