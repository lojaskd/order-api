<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreatePaymentVO;
        
/**
 * Class CreatePaymentVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreatePaymentVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreatePaymentVO();
                
        $entity->setId('1');
        $entity->setPaymentFormId('1');
        $entity->setIndexer('1');
        $entity->setInstalment('1');
        $entity->setInstalmentValue('1');
        $entity->setName('1');
        $entity->setDescription('1');
        $entity->setDeadline('1');
        $entity->setPaymentFormDesc('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getPaymentFormId());
        $this->assertNotNull($entity->getIndexer());
        $this->assertNotNull($entity->getInstalment());
        $this->assertNotNull($entity->getInstalmentValue());
        $this->assertNotNull($entity->getName());
        $this->assertNotNull($entity->getDescription());
        $this->assertNotNull($entity->getDeadline());
        $this->assertNotNull($entity->getPaymentFormDesc());
        $this->assertNotNull($entity->getPaymentTotalAmount());
    }
}
