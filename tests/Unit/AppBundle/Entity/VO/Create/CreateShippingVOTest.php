<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateShippingVO;
        
/**
 * Class CreateShippingVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateShippingVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateShippingVO();
                
        $entity->setId('1');
        $entity->setName('1');
        $entity->setPattern('1');
        $entity->setShippingType('1');
        $entity->setFixedDeadline('1');
        $entity->setFixedValue('1');
        $entity->setFreeDeadline('1');
        $entity->setShippingCompanyDeadline('1');
        $entity->setSafety('1');
        $entity->setSafetyPrice('1');
        $entity->setPrice('1');
        $entity->setCostPrice('1');
        $entity->setDeadline('1');
        $entity->setMounting('1');
        $entity->setMountingValue('1');
        $entity->setRuleFreight('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getName());
        $this->assertNotNull($entity->isPattern());
        $this->assertNotNull($entity->getShippingType());
        $this->assertNotNull($entity->getFixedDeadline());
        $this->assertNotNull($entity->getFixedValue());
        $this->assertNotNull($entity->getFreeDeadline());
        $this->assertNotNull($entity->getShippingCompanyDeadline());
        $this->assertNotNull($entity->isSafety());
        $this->assertNotNull($entity->getSafetyPrice());
        $this->assertNotNull($entity->getPrice());
        $this->assertNotNull($entity->getCostPrice());
        $this->assertNotNull($entity->getDeadline());
        $this->assertNotNull($entity->isMounting());
        $this->assertNotNull($entity->getMountingValue());
        $this->assertNotNull($entity->getRuleFreight());
        $this->assertNotNull($entity->getPattern());
        $this->assertNotNull($entity->getSafety());
        $this->assertNotNull($entity->getMounting());
    }
}
