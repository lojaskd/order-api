<?php

namespace Tests\Unit\AppBundle\Entity\VO\Create;

use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreatePromotionPaymentVO;

/**
 * Class CreatePromotionPaymentVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreatePromotionPaymentVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreatePromotionPaymentVO();

        $entity->setIdPromo('1');
        $entity->setDiscountValue('1');
        $entity->setDiscountPerc('1');
        $entity->setDiscountProdPrice('1');
        $entity->setDiscountProduct('1');

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getDiscountValue());
        $this->assertNotNull($entity->getDiscountPerc());
        $this->assertNotNull($entity->getDiscountProdPrice());
        $this->assertNotNull($entity->getDiscountProduct());
    }
}
