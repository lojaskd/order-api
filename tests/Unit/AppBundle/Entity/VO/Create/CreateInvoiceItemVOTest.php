<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateInvoiceItemVO;
        
/**
 * Class CreateInvoiceItemVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateInvoiceItemVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateInvoiceItemVO();
                
        $entity->setProductReference('1');
        $entity->setReturnedDate(new DateTime());
        $entity->setReturned('1');
        $entity->setQty('1');

        $this->assertNotNull($entity->getProductReference());
        $this->assertNotNull($entity->getReturnedDate());
        $this->assertNotNull($entity->isReturned());
        $this->assertNotNull($entity->getQty());
        $this->assertNotNull($entity->getReturned());
    }
}
