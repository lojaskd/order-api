<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
        
/**
 * Class CreateCustomerVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateCustomerVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateCustomerVO();
                
        $entity->setId('1');
        $entity->setFirstName('1');
        $entity->setLastName('1');
        $entity->setEmail('1');
        $entity->setCpf('1');
        $entity->setRg('1');
        $entity->setPersonType('1');
        $entity->setGenre('1');
        $entity->setBirthday('1');
        $entity->setContact('1');
        $entity->setIp('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getFirstName());
        $this->assertNotNull($entity->getLastName());
        $this->assertNotNull($entity->getEmail());
        $this->assertNotNull($entity->getCpf());
        $this->assertNotNull($entity->getRg());
        $this->assertNotNull($entity->getPersonType());
        $this->assertNotNull($entity->getGenre());
        $this->assertNotNull($entity->getBirthday());
        $this->assertNotNull($entity->getContact());
        $this->assertNotNull($entity->getIp());
    }
}
