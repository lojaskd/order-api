<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateOrderLogVO;
        
/**
 * Class CreateOrderLogVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateOrderLogVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateOrderLogVO();
                
        $entity->setOrderId('1');
        $entity->setCreatedAt('1');
        $entity->setTitle('1');
        $entity->setText('1');
        $entity->setShow('1');
        $entity->setStatus('1');
        $entity->setInternalStatus('1');

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getCreatedAt());
        $this->assertNotNull($entity->getTitle());
        $this->assertNotNull($entity->getText());
        $this->assertNotNull($entity->isShow());
        $this->assertNotNull($entity->getStatus());
        $this->assertNotNull($entity->getInternalStatus());
        $this->assertNotNull($entity->getShow());
    }
}
