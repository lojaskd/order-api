<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateOriginVO;
        
/**
 * Class CreateOriginVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateOriginVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateOriginVO();
                
        $entity->setSource('1');
        $entity->setContent('1');
        $entity->setMedium('1');
        $entity->setId('1');

        $this->assertNotNull($entity->getSource());
        $this->assertNotNull($entity->getContent());
        $this->assertNotNull($entity->getMedium());
        $this->assertNotNull($entity->getId());
    }
}
