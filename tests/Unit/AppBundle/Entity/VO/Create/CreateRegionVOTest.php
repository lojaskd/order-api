<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateRegionVO;
        
/**
 * Class CreateRegionVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateRegionVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateRegionVO();
                
        $entity->setId('1');
        $entity->setName('1');
        $entity->setDist('1');
        $entity->setChooseCustomer('1');
        $entity->setIbgeCode('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getName());
        $this->assertNotNull($entity->getDist());
        $this->assertNotNull($entity->isChooseCustomer());
        $this->assertNotNull($entity->getIbgeCode());
        $this->assertNotNull($entity->getChooseCustomer());
    }
}
