<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use AppBundle\Entity\VO\Create\CreateAddressVO;
use AppBundle\Entity\VO\Create\CreateCouponVO;
use AppBundle\Entity\VO\Create\CreateCustomerVO;
use AppBundle\Entity\VO\Create\CreateMarketPlaceVO;
use AppBundle\Entity\VO\Create\CreateOriginVO;
use AppBundle\Entity\VO\Create\CreatePaymentVO;
use AppBundle\Entity\VO\Create\CreateProductVO;
use AppBundle\Entity\VO\Create\CreateRegionVO;
use AppBundle\Entity\VO\Create\CreateShippingVO;
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateOrderVO;
        
/**
 * Class CreateOrderVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateOrderVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateOrderVO();
                
        $entity->setOrderId('1');
        $entity->setCustomer(new CreateCustomerVO());
        $entity->setType('1');
        $entity->setTotal('1');
        $entity->setDiscount('1');
        $entity->setObservation('1');
        $entity->setRegion(new CreateRegionVO());
        $entity->setDeliveryAddress(new CreateAddressVO());
        $entity->setBillingAddress(new CreateAddressVO());
        $entity->setProducts([new CreateProductVO()]);

        $firstPayment = new CreatePaymentVO();
        $firstPayment->setId(1);
        $entity->setFirstPayment($firstPayment);

        $secondPayment = new CreatePaymentVO();
        $secondPayment->setId(2);
        $entity->setSecondPayment($secondPayment);

        $entity->setOrigin(new CreateOriginVO());
        $entity->setShipping(new CreateShippingVO());
        $entity->setCoupon(new CreateCouponVO());
        $entity->setAssistance('1');
        $entity->setExchange('1');
        $entity->setBuyback('1');
        $entity->setMarketplace(new CreateMarketPlaceVO());
        $entity->setDevice('1');
        $entity->setStatusInternal('1');

        $productOne = new CreateProductVO();
        $productOne->setAvailabilityDeadline(12);
        $productOne->setAvailabilityDays(4);
        $productOne->setDeliveryDate(new DateTime());

        $productTwo =new CreateProductVO();
        $productTwo->setAvailabilityDeadline(15);
        $productTwo->setAvailabilityDays(10);
        $productTwo->setDeliveryDate(new DateTime());

        $entity->addProduct($productOne);
        $entity->addProduct($productTwo);

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getCustomer());
        $this->assertNotNull($entity->getType());
        $this->assertNotNull($entity->getTotal());
        $this->assertNotNull($entity->getDiscount());
        $this->assertNotNull($entity->getObservation());
        $this->assertNotNull($entity->getRegion());
        $this->assertNotNull($entity->getDeliveryAddress());
        $this->assertNotNull($entity->getBillingAddress());
        $this->assertNotNull($entity->getProducts());
        $this->assertNotNull($entity->getFirstPayment());
        $this->assertNotNull($entity->getSecondPayment());
        $this->assertNotNull($entity->getOrigin());
        $this->assertNotNull($entity->getShipping());
        $this->assertNotNull($entity->getCoupon());
        $this->assertNotNull($entity->isAssistance());
        $this->assertNotNull($entity->isExchange());
        $this->assertNotNull($entity->isBuyback());
        $this->assertNotNull($entity->getMarketplace());
        $this->assertNotNull($entity->getProductsId());
        $this->assertNotNull($entity->getProductsQuantity());
        $this->assertNotNull($entity->getSubtotal());
        $this->assertNotNull($entity->getTotalCubage());
        $this->assertNotNull($entity->getTotalWeight());
        $this->assertNotNull($entity->getTotalQuantity());
        $this->assertNotNull($entity->getCategories());
        $this->assertNotNull($entity->getProviders());
        $this->assertNotNull($entity->getPaymentsId());
        $this->assertNotNull($entity->getDevice());
        $this->assertNotNull($entity->getStatusInternal());
        $this->assertNotNull($entity->getMaxDeliveryDeadline());
        $this->assertNotNull($entity->getMaxAvailabilityDays());
        $this->assertNotNull($entity->getMaxDeliveryDate());
        $this->assertNotNull($entity->getMaxShippingDeadline());
        $this->assertNotNull($entity->getAssistance());
        $this->assertNotNull($entity->getExchange());
        $this->assertNotNull($entity->getBuyback());
    }
}
