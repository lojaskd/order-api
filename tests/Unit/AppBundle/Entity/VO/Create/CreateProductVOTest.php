<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateProductVO;
        
/**
 * Class CreateProductVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateProductVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateProductVO();
                
        $entity->setKitId('1');
        $entity->setName('1');
        $entity->setImage('1');
        $entity->setReference('1');
        $entity->setFeatured('1');
        $entity->setStyle('1');
        $entity->setColorName('1');
        $entity->setGenre('1');
        $entity->setProviderDeadline('1');
        $entity->setProviderStock('1');
        $entity->setProviderStockReserved('1');
        $entity->setProductProviderDeadline('1');
        $entity->setMinStock('1');
        $entity->setMinStockAction('1');
        $entity->setAction('1');
        $entity->setShow('1');
        $entity->setProductStock('1');
        $entity->setProductDeadline('1');
        $entity->setOut('1');
        $entity->setAvailable('1');
        $entity->setAvailability('1');
        $entity->setAvailabilityDays('1');
        $entity->setProviderDeadlineDate('1');
        $entity->setProviderDeliveryDate('1');
        $entity->setDeliveryDays('1');
        $entity->setDeliveryDate('1');
        $entity->setAvailabilityDeadline('1');
        $entity->setPromotion('1');
        $entity->setInitialPromotion('1');
        $entity->setFinalPromotion('1');
        $entity->setDiscountValue('1');
        $entity->setDiscountPercentage('1');
        $entity->setRealPrice('1');
        $entity->setPromotionPrice('1');
        $entity->setProductPrice('1');
        $entity->setTotalprice('1');
        $entity->setCostPrice('1');
        $entity->setBranchOfficeId('1');
        $entity->setCollectionConveyorCode('1');
        $entity->setId('1');
        $entity->setQuantity('1');
        $entity->setSize('1');
        $entity->setCubage('1');
        $entity->setWeight('1');
        $entity->setCategory('1');
        $entity->setRelatedCategories('1');
        $entity->setProvider('1');
        $entity->setProviderName('1');

        $this->assertNotNull($entity->getKitId());
        $this->assertNotNull($entity->getName());
        $this->assertNotNull($entity->getImage());
        $this->assertNotNull($entity->getReference());
        $this->assertNotNull($entity->isFeatured());
        $this->assertNotNull($entity->getStyle());
        $this->assertNotNull($entity->getColorName());
        $this->assertNotNull($entity->getGenre());
        $this->assertNotNull($entity->getProviderDeadline());
        $this->assertNotNull($entity->getProviderStock());
        $this->assertNotNull($entity->getProviderStockReserved());
        $this->assertNotNull($entity->getProductProviderDeadline());
        $this->assertNotNull($entity->getMinStock());
        $this->assertNotNull($entity->getMinStockAction());
        $this->assertNotNull($entity->getAction());
        $this->assertNotNull($entity->isShow());
        $this->assertNotNull($entity->getProductStock());
        $this->assertNotNull($entity->getProductDeadline());
        $this->assertNotNull($entity->isOut());
        $this->assertNotNull($entity->isAvailable());
        $this->assertNotNull($entity->getAvailability());
        $this->assertNotNull($entity->getAvailabilityDays());
        $this->assertNotNull($entity->getProviderDeadlineDate());
        $this->assertNotNull($entity->getProviderDeliveryDate());
        $this->assertNotNull($entity->getDeliveryDays());
        $this->assertNotNull($entity->getDeliveryDate());
        $this->assertNotNull($entity->getAvailabilityDeadline());
        $this->assertNotNull($entity->isPromotion());
        $this->assertNotNull($entity->getInitialPromotion());
        $this->assertNotNull($entity->getFinalPromotion());
        $this->assertNotNull($entity->getDiscountValue());
        $this->assertNotNull($entity->getDiscountPercentage());
        $this->assertNotNull($entity->getRealPrice());
        $this->assertNotNull($entity->getPromotionPrice());
        $this->assertNotNull($entity->getProductPrice());
        $this->assertNotNull($entity->getTotalprice());
        $this->assertNotNull($entity->getCostPrice());
        $this->assertNotNull($entity->getBranchOfficeId());
        $this->assertNotNull($entity->getCollectionConveyorCode());
        $this->assertNotNull($entity->getFeatured());
        $this->assertNotNull($entity->getShow());
        $this->assertNotNull($entity->getOut());
        $this->assertNotNull($entity->getAvailable());
        $this->assertNotNull($entity->getPromotion());
        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getQuantity());
        $this->assertNotNull($entity->getSize());
        $this->assertNotNull($entity->getCubage());
        $this->assertNotNull($entity->getWeight());
        $this->assertNotNull($entity->getCategory());
        $this->assertNotNull($entity->getRelatedCategories());
        $this->assertNotNull($entity->getProvider());
        $this->assertNotNull($entity->getProviderName());
    }
}
