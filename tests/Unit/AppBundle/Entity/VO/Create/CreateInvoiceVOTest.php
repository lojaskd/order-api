<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use AppBundle\Entity\VO\Create\CreateInvoiceItemVO;
use AppBundle\Entity\VO\Create\CreateInvoiceVO;
use DateTime;
use PHPUnit_Framework_TestCase;

/**
 * Class CreateInvoiceVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateInvoiceVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateInvoiceVO();
                
        $entity->setOrderId('1');
        $entity->setInvoiceId('1');
        $entity->setBranchId('1');
        $entity->setSerie('1');
        $entity->setCreatedDate(new DateTime());
        $entity->setShippingDate(new DateTime());
        $entity->setPredictDeliveryDate(new DateTime());
        $entity->setReturnedDate(new DateTime());
        $entity->setReturned('1');
        $entity->setDeliveryDate(new DateTime());
        $entity->setKey('1');
        $entity->setMarketplaceIntegration('1');
        $entity->setClerk('1');
        $entity->setItens([new CreateInvoiceItemVO()]);
        $entity->setShipperName('1');
        $entity->setShipperDoc('1');
        $entity->setHub('1');
        $entity->addItem(new CreateInvoiceItemVO());

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getInvoiceId());
        $this->assertNotNull($entity->getBranchId());
        $this->assertNotNull($entity->getSerie());
        $this->assertNotNull($entity->getCreatedDate());
        $this->assertNotNull($entity->getShippingDate());
        $this->assertNotNull($entity->getPredictDeliveryDate());
        $this->assertNotNull($entity->getReturnedDate());
        $this->assertNotNull($entity->isReturned());
        $this->assertNotNull($entity->getDeliveryDate());
        $this->assertNotNull($entity->getKey());
        $this->assertNotNull($entity->isMarketplaceIntegration());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->getItens());
        $this->assertNotNull($entity->getShipperName());
        $this->assertNotNull($entity->getShipperDoc());
        $this->assertNotNull($entity->getHub());
        $this->assertNotNull($entity->getReturned());
        $this->assertNotNull($entity->getMarketplaceIntegration());
    }
}
