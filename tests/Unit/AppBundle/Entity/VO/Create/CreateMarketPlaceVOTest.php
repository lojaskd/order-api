<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateMarketPlaceVO;
        
/**
 * Class CreateMarketPlaceVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateMarketPlaceVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateMarketPlaceVO();
                
        $entity->setId('1');
        $entity->setOrder('1');
        $entity->setChannelId('1');
        $entity->setNewDeliveryDate('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getOrder());
        $this->assertNotNull($entity->getChannelId());
        $this->assertNotNull($entity->getNewDeliveryDate());
    }
}
