<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use AppBundle\Entity\VO\Create\CreatePurchaseOrderProductVO;
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreatePurchaseOrderVO;
        
/**
 * Class CreatePurchaseOrderVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreatePurchaseOrderVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreatePurchaseOrderVO();
                
        $entity->setOrderId('1');
        $entity->setPurchaseOrderId('1');
        $entity->setDate(new DateTime());
        $entity->setProducts([new CreatePurchaseOrderProductVO()]);
        $entity->setClerk('1');
        $entity->addProduct(new CreatePurchaseOrderProductVO());

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getPurchaseOrderId());
        $this->assertNotNull($entity->getDate());
        $this->assertNotNull($entity->getProducts());
        $this->assertNotNull($entity->getClerk());
    }
}
