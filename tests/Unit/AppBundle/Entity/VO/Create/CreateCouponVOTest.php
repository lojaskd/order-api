<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateCouponVO;
        
/**
 * Class CreateCouponVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateCouponVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateCouponVO();
                
        $entity->setIdPromo('1');
        $entity->setCode('1');
        $entity->setPromotionName('1');
        $entity->setFreeShipping('1');
        $entity->setFixedShipping('1');
        $entity->setFixedValue('1');
        $entity->setDiscountValue('1');
        $entity->setShippingIncluded('1');

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getCode());
        $this->assertNotNull($entity->getPromotionName());
        $this->assertNotNull($entity->isFreeShipping());
        $this->assertNotNull($entity->isFixedShipping());
        $this->assertNotNull($entity->getFixedValue());
        $this->assertNotNull($entity->getDiscountValue());
        $this->assertNotNull($entity->isShippingIncluded());
        $this->assertNotNull($entity->getFreeShipping());
        $this->assertNotNull($entity->getFixedShipping());
        $this->assertNotNull($entity->getShippingIncluded());
    }
}
