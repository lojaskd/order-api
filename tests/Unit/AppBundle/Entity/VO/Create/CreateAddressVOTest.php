<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreateAddressVO;
        
/**
 * Class CreateAddressVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateAddressVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateAddressVO();
                
        $entity->setId('1');
        $entity->setIdentification('1');
        $entity->setFirstName('1');
        $entity->setLastname('1');
        $entity->setAddress('1');
        $entity->setNumber('1');
        $entity->setComplement('1');
        $entity->setNeighborhood('1');
        $entity->setCity('1');
        $entity->setState('1');
        $entity->setZip('1');
        $entity->setCountry('1');
        $entity->setTelephoneA('1');
        $entity->setTelephoneB('1');
        $entity->setReference('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getIdentification());
        $this->assertNotNull($entity->getFirstName());
        $this->assertNotNull($entity->getLastname());
        $this->assertNotNull($entity->getAddress());
        $this->assertNotNull($entity->getNumber());
        $this->assertNotNull($entity->getComplement());
        $this->assertNotNull($entity->getNeighborhood());
        $this->assertNotNull($entity->getCity());
        $this->assertNotNull($entity->getState());
        $this->assertNotNull($entity->getZip());
        $this->assertNotNull($entity->getCountry());
        $this->assertNotNull($entity->getTelephoneA());
        $this->assertNotNull($entity->getTelephoneB());
        $this->assertNotNull($entity->getReference());
    }
}
