<?php 
      
namespace Tests\Unit\AppBundle\Entity\VO\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\VO\Create\CreatePurchaseOrderProductVO;
        
/**
 * Class CreatePurchaseOrderProductVOTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreatePurchaseOrderProductVOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreatePurchaseOrderProductVO();
                
        $entity->setId('1');

        $this->assertNotNull($entity->getId());
    }
}
