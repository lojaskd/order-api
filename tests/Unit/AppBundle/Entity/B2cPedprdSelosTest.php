<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPedprdSelos;
        
/**
 * Class B2cPedprdSelosTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPedprdSelosTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPedprdSelos();
                
        $entity->setIdPedped('1');
        $entity->setIdPrdprd('1');
        $entity->setIdSelo('1');
        $entity->setNome('1');

        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getIdSelo());
        $this->assertNotNull($entity->getNome());
    }
}
