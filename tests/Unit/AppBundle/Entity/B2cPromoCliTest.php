<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoCli;
        
/**
 * Class B2cPromoCliTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoCliTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoCli();
                
        $entity->setIdPromo('1');
        $entity->setIdCliente('1');

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getIdCliente());
    }
}
