<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use AppBundle\Entity\B2cRegcep;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cRegreg;
        
/**
 * Class B2cRegregTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cRegregTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cRegreg();
                
        $entity->setRegiao('1');
        $entity->setInternacional('1');
        $entity->setClienteEscolhe('1');
        $entity->setPaises('1');
        $entity->setId(1);
        $regCep = new ArrayCollection();
        $regCep->add(new B2cRegcep());
        $entity->setRegCep($regCep);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getRegiao());
        $this->assertNotNull($entity->getInternacional());
        $this->assertNotNull($entity->getClienteEscolhe());
        $this->assertNotNull($entity->getPaises());
    }
}
