<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use AppBundle\Entity\B2cPedped;
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPedprd;
        
/**
 * Class B2cPedprdTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPedprdTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPedprd();
                
        $entity->setIdPrdprd('1');
        $entity->setPrdNome('1');
        $entity->setPrdQtd('1');
        $entity->setPrdData(new DateTime());
        $entity->setPrdReferencia('1');
        $entity->setPrdCategoria('1');
        $entity->setPrdFornecedor('1');
        $entity->setPrdFornNome('1');
        $entity->setPrdPrazoForn('1');
        $entity->setPrdPrazoDisp('1');
        $entity->setPrdDataEntregaFornecedor(new DateTime());
        $entity->setPrdDestaque('1');
        $entity->setPrdPeso('1');
        $entity->setPrdEstilo('1');
        $entity->setPrdPresente('1');
        $entity->setPrdStqLoja('1');
        $entity->setPrdStqFornecedor('1');
        $entity->setPrdStqFornecedorReservado('1');
        $entity->setPrdStqMarketplace('1');
        $entity->setPrdEmbalagem('1');
        $entity->setPrdTextoCartao('1');
        $entity->setPrdAro('1');
        $entity->setPrdImglistagem('1');
        $entity->setPrdPromoDesc('1');
        $entity->setBpromocao('1');
        $entity->setPrdDescPerc('1');
        $entity->setPrdCubagem('1');
        $entity->setPrdCor('1');
        $entity->setPrdSexo('1');
        $entity->setPrdTamanho('1');
        $entity->setPrdPvreal('1');
        $entity->setPrdPvpromocao('1');
        $entity->setPrdValeCompra('1');
        $entity->setPrdVndInfo('1');
        $entity->setPrdDescValor('1');
        $entity->setPrdPvtotal('1');
        $entity->setPrdPromoFim(new DateTime());
        $entity->setPrdCusto('1');
        $entity->setPrdVndAberto('1');
        $entity->setPrdVndJunta('1');
        $entity->setPrdPromoIni(new DateTime());
        $entity->setSfOpportunitylineitem('1');
        $entity->setPrdDataEntregaFornecedorRealizada(new DateTime());
        $entity->setIdNotafiscal('1');
        $entity->setIdTipoOcorrencia('1');
        $entity->setNomeOcorrencia('1');
        $entity->setGrupoOcorrencia('1');
        $entity->setDataOcorrencia(new DateTime());
        $entity->setItemCancelado('1');
        $entity->setDataCancelamentoSalesforce(new DateTime());
        $entity->setOrdemCompra('1');
        $entity->setDataPrevEntregaForn(new DateTime());
        $entity->setClusterRmkt('1');
        $entity->setDataOrdemCompra(new DateTime());
        $entity->setIdFilialEstoque('1');
        $entity->setIdPedped(new B2cPedped());
        $entity->setDataCancelamento(new DateTime());
        $entity->setCodigoTransportadorColeta('1');
        $entity->setDestination('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getPrdNome());
        $this->assertNotNull($entity->getPrdQtd());
        $this->assertNotNull($entity->getPrdData());
        $this->assertNotNull($entity->getPrdReferencia());
        $this->assertNotNull($entity->getPrdCategoria());
        $this->assertNotNull($entity->getPrdFornecedor());
        $this->assertNotNull($entity->getPrdFornNome());
        $this->assertNotNull($entity->getPrdPrazoForn());
        $this->assertNotNull($entity->getPrdPrazoDisp());
        $this->assertNotNull($entity->getPrdDataEntregaFornecedor());
        $this->assertNotNull($entity->getPrdDestaque());
        $this->assertNotNull($entity->getPrdPeso());
        $this->assertNotNull($entity->getPrdEstilo());
        $this->assertNotNull($entity->getPrdPresente());
        $this->assertNotNull($entity->getPrdStqLoja());
        $this->assertNotNull($entity->getPrdStqFornecedor());
        $this->assertNotNull($entity->getPrdStqFornecedorReservado());
        $this->assertNotNull($entity->getPrdStqMarketplace());
        $this->assertNotNull($entity->getPrdEmbalagem());
        $this->assertNotNull($entity->getPrdTextoCartao());
        $this->assertNotNull($entity->getPrdAro());
        $this->assertNotNull($entity->getPrdImglistagem());
        $this->assertNotNull($entity->getPrdPromoDesc());
        $this->assertNotNull($entity->getBpromocao());
        $this->assertNotNull($entity->getPrdDescPerc());
        $this->assertNotNull($entity->getPrdCubagem());
        $this->assertNotNull($entity->getPrdCor());
        $this->assertNotNull($entity->getPrdSexo());
        $this->assertNotNull($entity->getPrdTamanho());
        $this->assertNotNull($entity->getPrdPvreal());
        $this->assertNotNull($entity->getPrdPvpromocao());
        $this->assertNotNull($entity->getPrdValeCompra());
        $this->assertNotNull($entity->getPrdVndInfo());
        $this->assertNotNull($entity->getPrdDescValor());
        $this->assertNotNull($entity->getPrdPvtotal());
        $this->assertNotNull($entity->getPrdPromoFim());
        $this->assertNotNull($entity->getPrdCusto());
        $this->assertNotNull($entity->getPrdVndAberto());
        $this->assertNotNull($entity->getPrdVndJunta());
        $this->assertNotNull($entity->getPrdPromoIni());
        $this->assertNotNull($entity->getSfOpportunitylineitem());
        $this->assertNotNull($entity->getPrdDataEntregaFornecedorRealizada());
        $this->assertNotNull($entity->getIdNotafiscal());
        $this->assertNotNull($entity->getIdTipoOcorrencia());
        $this->assertNotNull($entity->getNomeOcorrencia());
        $this->assertNotNull($entity->getGrupoOcorrencia());
        $this->assertNotNull($entity->getDataOcorrencia());
        $this->assertNotNull($entity->getItemCancelado());
        $this->assertNotNull($entity->getDataCancelamentoSalesforce());
        $this->assertNotNull($entity->getOrdemCompra());
        $this->assertNotNull($entity->getDataPrevEntregaForn());
        $this->assertNotNull($entity->getClusterRmkt());
        $this->assertNotNull($entity->getDataOrdemCompra());
        $this->assertNotNull($entity->getIdFilialEstoque());
        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getDataCancelamento());
        $this->assertNotNull($entity->getCodigoTransportadorColeta());
        $this->assertNotNull($entity->getDestination());
    }
}
