<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPedprdstatus;
        
/**
 * Class B2cPedprdstatusTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPedprdstatusTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPedprdstatus();
                
        $entity->setIdPedped('1');
        $entity->setIdPrdprd('1');
        $entity->setPrdReferencia('1');
        $entity->setStatusMsg('1');
        $entity->setStatusData('1');
        $entity->setDataApr('1');
        $entity->setDataAprR('1');
        $entity->setDataLib('1');
        $entity->setDataLibR('1');
        $entity->setDataFatForn('1');
        $entity->setDataFatFornR('1');
        $entity->setDataEntdForn('1');
        $entity->setDataEntdFornR('1');
        $entity->setDataEntForn('1');
        $entity->setDataEntFornR('1');
        $entity->setDataNf('1');
        $entity->setDataNfR('1');
        $entity->setDataEmb('1');
        $entity->setDataEmbR('1');
        $entity->setDataCte('1');
        $entity->setDataCteR('1');
        $entity->setDataEnt('1');
        $entity->setDataEntR('1');
        $entity->setDataEmbRNotificado('1');
        $entity->setDataEntRNotificado('1');
        $entity->setDataPrevEntregaTransp('1');

        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getPrdReferencia());
        $this->assertNotNull($entity->getStatusMsg());
        $this->assertNotNull($entity->getStatusData());
        $this->assertNotNull($entity->getDataApr());
        $this->assertNotNull($entity->getDataAprR());
        $this->assertNotNull($entity->getDataLib());
        $this->assertNotNull($entity->getDataLibR());
        $this->assertNotNull($entity->getDataFatForn());
        $this->assertNotNull($entity->getDataFatFornR());
        $this->assertNotNull($entity->getDataEntdForn());
        $this->assertNotNull($entity->getDataEntdFornR());
        $this->assertNotNull($entity->getDataEntForn());
        $this->assertNotNull($entity->getDataEntFornR());
        $this->assertNotNull($entity->getDataNf());
        $this->assertNotNull($entity->getDataNfR());
        $this->assertNotNull($entity->getDataEmb());
        $this->assertNotNull($entity->getDataEmbR());
        $this->assertNotNull($entity->getDataCte());
        $this->assertNotNull($entity->getDataCteR());
        $this->assertNotNull($entity->getDataEnt());
        $this->assertNotNull($entity->getDataEntR());
        $this->assertNotNull($entity->getDataEmbRNotificado());
        $this->assertNotNull($entity->getDataEntRNotificado());
        $this->assertNotNull($entity->getDataPrevEntregaTransp());
    }
}
