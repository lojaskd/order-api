<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cContaContabil;
        
/**
 * Class B2cContaContabilTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cContaContabilTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cContaContabil();
                
        $entity->setId('1');
        $entity->setCodigo('1');
        $entity->setNome('1');
        $entity->setCardcode('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getCodigo());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getCardcode());
    }
}
