<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cLogCartao;
        
/**
 * Class B2cLogCartaoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cLogCartaoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cLogCartao();
                
        $entity->setIdPedido('1');
        $entity->setIdPagamentoCartao('1');
        $entity->setCodigoLogCartao('1');
        $entity->setDescLogCartao('1');
        $entity->setTidLogCartao('1');
        $entity->setNomeOperadoraTransmissao('1');
        $entity->setDtaIncLogCartao('1');
        $entity->setGatewayTransmissao('1');
        $entity->setTitularCartao('1');
        $entity->setValidadeCartao('1');
        $entity->setBinCartao('1');
        $entity->setNumeroFinalCartao('1');
        $entity->setIdLogCartao('1');

        $this->assertNotNull($entity->getIdLogCartao());
        $this->assertNotNull($entity->getIdPedido());
        $this->assertNotNull($entity->getIdPagamentoCartao());
        $this->assertNotNull($entity->getCodigoLogCartao());
        $this->assertNotNull($entity->getDescLogCartao());
        $this->assertNotNull($entity->getTidLogCartao());
        $this->assertNotNull($entity->getNomeOperadoraTransmissao());
        $this->assertNotNull($entity->getDtaIncLogCartao());
        $this->assertNotNull($entity->getGatewayTransmissao());
        $this->assertNotNull($entity->getTitularCartao());
        $this->assertNotNull($entity->getValidadeCartao());
        $this->assertNotNull($entity->getBinCartao());
        $this->assertNotNull($entity->getNumeroFinalCartao());
    }
}
