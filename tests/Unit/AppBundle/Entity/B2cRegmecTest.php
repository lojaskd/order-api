<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cRegmec;
        
/**
 * Class B2cRegmecTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cRegmecTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cRegmec();
                
        $entity->setIdTrans('1');
        $entity->setIniMc('1');
        $entity->setFimMc('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getIdTrans());
        $this->assertNotNull($entity->getIniMc());
        $this->assertNotNull($entity->getFimMc());
    }
}
