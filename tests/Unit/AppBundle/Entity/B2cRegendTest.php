<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cRegend;
        
/**
 * Class B2cRegendTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cRegendTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cRegend();
                
        $entity->setIdRegiao('1');
        $entity->setIdRegest('1');
        $entity->setEstado('1');
        $entity->setUf('1');
        $entity->setCidade('1');
        $entity->setBairroInicio('1');
        $entity->setBairroFim('1');
        $entity->setEndereco('1');
        $entity->setComplemento('1');
        $entity->setCodIbge('1');
        $entity->setCodIbgeMunicipio('1');
        $entity->setCidadeMunicipio('1');
        $entity->setCep('1');
        $entity->setIniCep('1');
        $entity->setFimCep('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getIdRegiao());
        $this->assertNotNull($entity->getIdRegest());
        $this->assertNotNull($entity->getEstado());
        $this->assertNotNull($entity->getUf());
        $this->assertNotNull($entity->getCidade());
        $this->assertNotNull($entity->getBairroInicio());
        $this->assertNotNull($entity->getBairroFim());
        $this->assertNotNull($entity->getEndereco());
        $this->assertNotNull($entity->getComplemento());
        $this->assertNotNull($entity->getCodIbge());
        $this->assertNotNull($entity->getCodIbgeMunicipio());
        $this->assertNotNull($entity->getCidadeMunicipio());
        $this->assertNotNull($entity->getCep());
        $this->assertNotNull($entity->getIniCep());
        $this->assertNotNull($entity->getFimCep());
    }
}
