<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdpag;
        
/**
 * Class B2cPrdpagTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdpagTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdpag();
                
        $entity->setIdPrdprd('1');
        $entity->setIdPagpag('1');
        $entity->setIdPagfor('1');
        $entity->setPadrao('1');

        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getIdPagpag());
        $this->assertNotNull($entity->getIdPagfor());
        $this->assertNotNull($entity->getPadrao());
    }
}
