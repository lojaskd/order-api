<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdestoque;
        
/**
 * Class B2cPrdestoqueTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdestoqueTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdestoque();
                
        $entity->setIdPrdprd('1');
        $entity->setIdFilial('1');
        $entity->setQtd('1');

        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getIdFilial());
        $this->assertNotNull($entity->getQtd());
    }
}
