<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cFormasEstorno;
        
/**
 * Class B2cFormasEstornoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cFormasEstornoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cFormasEstorno();
                
        $entity->setNome('1');
        $entity->setAtivo('1');
        $entity->setIdForest(1);

        $this->assertNotNull($entity->getIdForest());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getAtivo());
    }
}
