<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPedprdstatuslog;
        
/**
 * Class B2cPedprdstatuslogTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPedprdstatuslogTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPedprdstatuslog();
                
        $entity->setTipo('1');
        $entity->setIdPedped('1');
        $entity->setIdPrdprd('1');
        $entity->setValorAntigo(new DateTime());
        $entity->setValorNovo(new DateTime());
        $entity->setDataTentativa(new DateTime());
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getTipo());
        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getValorAntigo());
        $this->assertNotNull($entity->getValorNovo());
        $this->assertNotNull($entity->getDataTentativa());
    }
}
