<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdlog;
        
/**
 * Class B2cPrdlogTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdlogTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdlog();
                
        $entity->setIdClicli('1');
        $entity->setIdPrdprd('1');
        $entity->setMatriz('1');
        $entity->setPrdNome('1');
        $entity->setCarrinho('1');
        $entity->setUrl('1');
        $entity->setDataLog(new DateTime());
        $entity->setIdPrdlog(1);

        $this->assertNotNull($entity->getIdPrdlog());
        $this->assertNotNull($entity->getIdClicli());
        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getMatriz());
        $this->assertNotNull($entity->getPrdNome());
        $this->assertNotNull($entity->getCarrinho());
        $this->assertNotNull($entity->getUrl());
        $this->assertNotNull($entity->getDataLog());
    }
}
