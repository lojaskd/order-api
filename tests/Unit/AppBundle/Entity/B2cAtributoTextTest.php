<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\B2cAtributoText;
use PHPUnit_Framework_TestCase;

/**
 * Class B2cAtributoTextTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cAtributoTextTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $b2cAtributoText = new B2cAtributoText();
        $b2cAtributoText->setIdAtributoValore(1)
            ->setRespostas('test, test, test')
            ->setIdAtributo(1)
            ->setB2cPrdprd(1);

        $this->assertNotNull($b2cAtributoText->getIdAtributoValore());
        $this->assertNotNull($b2cAtributoText->getRespostas());
        $this->assertEquals('test, test, test', $b2cAtributoText->getRespostas());
        $this->assertNotNull($b2cAtributoText->getIdAtributo());
        $this->assertNotNull($b2cAtributoText->getB2cPrdprd());
    }
}
