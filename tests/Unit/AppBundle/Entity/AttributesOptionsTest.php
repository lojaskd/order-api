<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\AttributesOptions;
use PHPUnit_Framework_TestCase;

/**
 * Class AttributesOptionsTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class AttributesOptionsTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $attributesOptions = new AttributesOptions();
        $attributesOptions->setIdAttr(1)->setLabel('Test')->setValue('test');

        $this->assertNotNull($attributesOptions->getIdAttr());
        $this->assertNotNull($attributesOptions->getLabel());
        $this->assertNotNull($attributesOptions->getValue());
    }
}
