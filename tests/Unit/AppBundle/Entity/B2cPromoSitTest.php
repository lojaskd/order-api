<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoSit;
        
/**
 * Class B2cPromoSitTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoSitTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoSit();
                
        $entity->setDestaque('1');
        $entity->setLancamento('1');
        $entity->setMaisvendido('1');
        $entity->setPrevenda('1');
        $entity->setPresentes('1');
        $entity->setIdPromo(1);

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getDestaque());
        $this->assertNotNull($entity->getLancamento());
        $this->assertNotNull($entity->getMaisvendido());
        $this->assertNotNull($entity->getPrevenda());
        $this->assertNotNull($entity->getPresentes());
    }
}
