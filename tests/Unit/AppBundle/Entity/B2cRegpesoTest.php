<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cRegpeso;
        
/**
 * Class B2cRegpesoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cRegpesoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cRegpeso();
                
        $entity->setIdTrans('1');
        $entity->setIniKg('1');
        $entity->setFimKg('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getIdTrans());
        $this->assertNotNull($entity->getIniKg());
        $this->assertNotNull($entity->getFimKg());
    }
}
