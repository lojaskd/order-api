<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPagpag;
        
/**
 * Class B2cPagpagTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPagpagTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPagpag();
                
        $entity->setAtivo('1');
        $entity->setPadrao('1');
        $entity->setPrioridade('1');
        $entity->setNome('1');
        $entity->setDescFechamento('1');
        $entity->setDescConfirmacao('1');
        $entity->setBandeira('1');
        $entity->setOperadora('1');
        $entity->setCapturar('1');
        $entity->setAutorizacao('1');
        $entity->setValMaximo('1');
        $entity->setValMinimo('1');
        $entity->setParMinimo('1');
        $entity->setValEntrada('1');
        $entity->setPrazo('1');
        $entity->setNomeLoja('1');
        $entity->setJurosMes('1');
        $entity->setJurosAno('1');
        $entity->setValorTac('1');
        $entity->setIof('1');
        $entity->setCarencia('1');
        $entity->setParcelasSemJuros('1');
        $entity->setDesconto('1');
        $entity->setConta1Ordem('1');
        $entity->setConta1Nome('1');
        $entity->setConta1Cedente('1');
        $entity->setConta1Agencia('1');
        $entity->setConta1Numero('1');
        $entity->setConta1Codigo('1');
        $entity->setConta1Maximo('1');
        $entity->setConta2Ordem('1');
        $entity->setConta2Nome('1');
        $entity->setConta2Cedente('1');
        $entity->setConta2Agencia('1');
        $entity->setConta2Numero('1');
        $entity->setConta2Codigo('1');
        $entity->setConta2Maximo('1');
        $entity->setConta3Ordem('1');
        $entity->setConta3Nome('1');
        $entity->setConta3Cedente('1');
        $entity->setConta3Agencia('1');
        $entity->setConta3Numero('1');
        $entity->setConta3Codigo('1');
        $entity->setConta3Maximo('1');
        $entity->setConta4Ordem('1');
        $entity->setConta4Nome('1');
        $entity->setConta4Cedente('1');
        $entity->setConta4Agencia('1');
        $entity->setConta4Numero('1');
        $entity->setConta4Codigo('1');
        $entity->setConta4Maximo('1');
        $entity->setComposto('1');
        $entity->setIdPagboleto('1');
        $entity->setIdPaypal('1');
        $entity->setSenhaPaypal('1');
        $entity->setAssinaturaPaypal('1');
        $entity->setIdPagpag(1);

        $this->assertNotNull($entity->getIdPagpag());
        $this->assertNotNull($entity->getAtivo());
        $this->assertNotNull($entity->getPadrao());
        $this->assertNotNull($entity->getPrioridade());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getDescFechamento());
        $this->assertNotNull($entity->getDescConfirmacao());
        $this->assertNotNull($entity->getBandeira());
        $this->assertNotNull($entity->getOperadora());
        $this->assertNotNull($entity->getCapturar());
        $this->assertNotNull($entity->getAutorizacao());
        $this->assertNotNull($entity->getValMaximo());
        $this->assertNotNull($entity->getValMinimo());
        $this->assertNotNull($entity->getParMinimo());
        $this->assertNotNull($entity->getValEntrada());
        $this->assertNotNull($entity->getPrazo());
        $this->assertNotNull($entity->getNomeLoja());
        $this->assertNotNull($entity->getJurosMes());
        $this->assertNotNull($entity->getJurosAno());
        $this->assertNotNull($entity->getValorTac());
        $this->assertNotNull($entity->getIof());
        $this->assertNotNull($entity->getCarencia());
        $this->assertNotNull($entity->getParcelasSemJuros());
        $this->assertNotNull($entity->getDesconto());
        $this->assertNotNull($entity->getConta1Ordem());
        $this->assertNotNull($entity->getConta1Nome());
        $this->assertNotNull($entity->getConta1Cedente());
        $this->assertNotNull($entity->getConta1Agencia());
        $this->assertNotNull($entity->getConta1Numero());
        $this->assertNotNull($entity->getConta1Codigo());
        $this->assertNotNull($entity->getConta1Maximo());
        $this->assertNotNull($entity->getConta2Ordem());
        $this->assertNotNull($entity->getConta2Nome());
        $this->assertNotNull($entity->getConta2Cedente());
        $this->assertNotNull($entity->getConta2Agencia());
        $this->assertNotNull($entity->getConta2Numero());
        $this->assertNotNull($entity->getConta2Codigo());
        $this->assertNotNull($entity->getConta2Maximo());
        $this->assertNotNull($entity->getConta3Ordem());
        $this->assertNotNull($entity->getConta3Nome());
        $this->assertNotNull($entity->getConta3Cedente());
        $this->assertNotNull($entity->getConta3Agencia());
        $this->assertNotNull($entity->getConta3Numero());
        $this->assertNotNull($entity->getConta3Codigo());
        $this->assertNotNull($entity->getConta3Maximo());
        $this->assertNotNull($entity->getConta4Ordem());
        $this->assertNotNull($entity->getConta4Nome());
        $this->assertNotNull($entity->getConta4Cedente());
        $this->assertNotNull($entity->getConta4Agencia());
        $this->assertNotNull($entity->getConta4Numero());
        $this->assertNotNull($entity->getConta4Codigo());
        $this->assertNotNull($entity->getConta4Maximo());
        $this->assertNotNull($entity->getComposto());
        $this->assertNotNull($entity->getIdPagboleto());
        $this->assertNotNull($entity->getIdPaypal());
        $this->assertNotNull($entity->getSenhaPaypal());
        $this->assertNotNull($entity->getAssinaturaPaypal());
    }
}
