<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\AxadoNotafiscalItem;
use DateTime;
use PHPUnit_Framework_TestCase;

/**
 * Class AxadoNotafiscalItemTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class AxadoNotafiscalItemTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $axadoNotafiscalItem = new AxadoNotafiscalItem();
        $axadoNotafiscalItem->setIdPedped(1)
            ->setIdFilial(1)
            ->setIdNotafiscal(1234)
            ->setIdItem(3187)
            ->setQuantidade(1)
            ->setValor(359.9)
            ->setComprimento(35.0)
            ->setLargura(35.0)
            ->setPeso(1.5)
            ->setCreatedAt(new DateTime())
            ->setDataSincronizado(new DateTime());

        $this->assertNotNull($axadoNotafiscalItem->getIdPedped());
        $this->assertNotNull($axadoNotafiscalItem->getIdFilial());
        $this->assertNotNull($axadoNotafiscalItem->getIdNotafiscal());
        $this->assertNotNull($axadoNotafiscalItem->getIdItem());
        $this->assertNotNull($axadoNotafiscalItem->getQuantidade());
        $this->assertNotNull($axadoNotafiscalItem->getValor());
        $this->assertNotNull($axadoNotafiscalItem->getComprimento());
        $this->assertNotNull($axadoNotafiscalItem->getLargura());
        $this->assertNotNull($axadoNotafiscalItem->getPeso());
        $this->assertNotNull($axadoNotafiscalItem->getCreatedAt());
        $this->assertNotNull($axadoNotafiscalItem->getDataSincronizado());
    }
}
