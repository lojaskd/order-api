<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\B2cAtributoValor;
use PHPUnit_Framework_TestCase;

/**
 * Class B2cAtributoValorTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cAtributoValorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cAtributoValor();

        $entity->setIdAtributoValore(1);
        $entity->setIdAtributo(1);
        $entity->setB2cPrdprd(1);
        $entity->setRespostas('teste');
        $entity->setSlug('teste');


        $this->assertNotNull($entity->getIdAtributoValore());
        $this->assertNotNull($entity->getIdAtributo());
        $this->assertNotNull($entity->getB2cPrdprd());
        $this->assertNotNull($entity->getRespostas());
        $this->assertNotNull($entity->getSlug());

    }
}
