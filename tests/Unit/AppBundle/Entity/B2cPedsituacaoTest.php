<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPedsituacao;
        
/**
 * Class B2cPedsituacaoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPedsituacaoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPedsituacao();
                
        $entity->setId('1');
        $entity->setName('1');
        $entity->setCreatedAt('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getName());
        $this->assertNotNull($entity->getCreatedAt());
    }
}
