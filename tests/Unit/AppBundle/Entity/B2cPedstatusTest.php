<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPedstatus;
        
/**
 * Class B2cPedstatusTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPedstatusTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPedstatus();
                
        $entity->setIdPedped('1');
        $entity->setPedSituacao('1');
        $entity->setPedStaint('1');
        $entity->setPedDtaSituacao('1');
        $entity->setPedDtaSituacaoPrev('1');

        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getPedSituacao());
        $this->assertNotNull($entity->getPedStaint());
        $this->assertNotNull($entity->getPedDtaSituacao());
        $this->assertNotNull($entity->getPedDtaSituacaoPrev());
    }
}
