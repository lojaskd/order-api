<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoLog;
        
/**
 * Class B2cPromoLogTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoLogTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoLog();
                
        $entity->setIdPromo('1');
        $entity->setIdCfgusu('1');
        $entity->setOcorrido(new DateTime());
        $entity->setPost('1');
        $entity->setGet('1');
        $entity->setCookies('1');
        $entity->setSession('1');
        $entity->setIdPromoLog(1);

        $this->assertNotNull($entity->getIdPromoLog());
        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getIdCfgusu());
        $this->assertNotNull($entity->getOcorrido());
        $this->assertNotNull($entity->getPost());
        $this->assertNotNull($entity->getGet());
        $this->assertNotNull($entity->getCookies());
        $this->assertNotNull($entity->getSession());
    }
}
