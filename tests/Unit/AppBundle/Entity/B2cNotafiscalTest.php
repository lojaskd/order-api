<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cNotafiscal;
        
/**
 * Class B2cNotafiscalTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cNotafiscalTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cNotafiscal();
                
        $entity->setIdFilial('1');
        $entity->setIdNotafiscal('1');
        $entity->setIdPedped('1');
        $entity->setSerieNotafiscal('1');
        $entity->setDataEmbarque('1');
        $entity->setDataEntregaProvavel('1');
        $entity->setDataEntregaRealizada('1');
        $entity->setDataCriacao('1');
        $entity->setDataDevolucao('1');
        $entity->setDevolvido('1');
        $entity->setChave('1');
        $entity->setIntegracaoMarketplace('1');
        $entity->setDataChave('1');
        $entity->setPathXml('1');
        $entity->setPathPdf('1');

        $this->assertNotNull($entity->getIdFilial());
        $this->assertNotNull($entity->getIdNotafiscal());
        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getSerieNotafiscal());
        $this->assertNotNull($entity->getDataEmbarque());
        $this->assertNotNull($entity->getDataEntregaProvavel());
        $this->assertNotNull($entity->getDataEntregaRealizada());
        $this->assertNotNull($entity->getDataCriacao());
        $this->assertNotNull($entity->getDataDevolucao());
        $this->assertNotNull($entity->isDevolvido());
        $this->assertNotNull($entity->getChave());
        $this->assertNotNull($entity->getIntegracaoMarketplace());
        $this->assertNotNull($entity->getDataChave());
        $this->assertNotNull($entity->getPathXml());
        $this->assertNotNull($entity->getPathPdf());
    }
}
