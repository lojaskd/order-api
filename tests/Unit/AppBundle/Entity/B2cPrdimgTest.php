<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdimg;
        
/**
 * Class B2cPrdimgTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdimgTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdimg();
                
        $entity->setIdPrdprd('1');
        $entity->setNome('1');
        $entity->setTipo('1');
        $entity->setThumb('1');
        $entity->setOrdem('1');
        $entity->setIdImgdesc('1');
        $entity->setIdPrdimg(1);

        $this->assertNotNull($entity->getIdPrdimg());
        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getTipo());
        $this->assertNotNull($entity->getThumb());
        $this->assertNotNull($entity->getOrdem());
        $this->assertNotNull($entity->getIdImgdesc());
    }
}
