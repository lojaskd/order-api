<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPedlog;
        
/**
 * Class B2cPedlogTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPedlogTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPedlog();
                
        $entity->setIdPedped('1');
        $entity->setTitulo('1');
        $entity->setTexto('1');
        $entity->setData(new DateTime());
        $entity->setMostrarAreavip('1');
        $entity->setStatusAtual('1');
        $entity->setStatusInternosAtual('1');
        $entity->setIdPedlog(1);

        $this->assertNotNull($entity->getIdPedlog());
        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getTitulo());
        $this->assertNotNull($entity->getTexto());
        $this->assertNotNull($entity->getData());
        $this->assertNotNull($entity->getMostrarAreavip());
        $this->assertNotNull($entity->getStatusAtual());
        $this->assertNotNull($entity->getStatusInternosAtual());
    }
}
