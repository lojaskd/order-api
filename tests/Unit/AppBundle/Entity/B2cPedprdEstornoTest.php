<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPedprdEstorno;
        
/**
 * Class B2cPedprdEstornoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPedprdEstornoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPedprdEstorno();
                
        $entity->setId('1');
        $entity->setIdPedped('1');
        $entity->setIdPrdprd('1');
        $entity->setIdForest('1');
        $entity->setTicket('1');
        $entity->setNumTransacao('1');
        $entity->setOperadora('1');
        $entity->setValorSugerido('1');
        $entity->setDtaCad('1');
        $entity->setValorProduto('1');
        $entity->setValorFrete('1');
        $entity->setEstornoLiberado('1');
        $entity->setOcrCode('1');
        $entity->setCancellationIndicator('1');
        $entity->setTicketType('1');
        $entity->setHub('1');
        $entity->setShipper('1');
        $entity->setInvoice('1');
        $entity->setProductCauser('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getIdForest());
        $this->assertNotNull($entity->getTicket());
        $this->assertNotNull($entity->getNumTransacao());
        $this->assertNotNull($entity->getOperadora());
        $this->assertNotNull($entity->getValorSugerido());
        $this->assertNotNull($entity->getDtaCad());
        $this->assertNotNull($entity->getValorProduto());
        $this->assertNotNull($entity->getValorFrete());
        $this->assertNotNull($entity->getEstornoLiberado());
        $this->assertNotNull($entity->getOcrCode());
        $this->assertNotNull($entity->getCancellationIndicator());
        $this->assertNotNull($entity->getTicketType());
        $this->assertNotNull($entity->getHub());
        $this->assertNotNull($entity->getShipper());
        $this->assertNotNull($entity->getInvoice());
        $this->assertNotNull($entity->getProductCauser());
    }
}
