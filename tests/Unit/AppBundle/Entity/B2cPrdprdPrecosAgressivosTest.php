<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdprdPrecosAgressivos;
        
/**
 * Class B2cPrdprdPrecosAgressivosTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdprdPrecosAgressivosTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdprdPrecosAgressivos();
                
        $entity->setPreco('1');
        $entity->setTipoDesconto('1');
        $entity->setValorDesconto('1');
        $entity->setTipoLimiteDesconto('1');
        $entity->setLimiteDesconto('1');
        $entity->setDtaCad(new DateTime());
        $entity->setDtaAlt(new DateTime());
        $entity->setDtaLimite(new DateTime());
        $entity->setDtaAltDesconton(new DateTime());
        $entity->setDtaPrimeiroDesconto(new DateTime());
        $entity->setUsuario('1');
        $entity->setAtivo('1');
        $entity->setIdPrdprd(1);

        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getPreco());
        $this->assertNotNull($entity->getTipoDesconto());
        $this->assertNotNull($entity->getValorDesconto());
        $this->assertNotNull($entity->getTipoLimiteDesconto());
        $this->assertNotNull($entity->getLimiteDesconto());
        $this->assertNotNull($entity->getDtaCad());
        $this->assertNotNull($entity->getDtaAlt());
        $this->assertNotNull($entity->getDtaLimite());
        $this->assertNotNull($entity->getDtaAltDesconton());
        $this->assertNotNull($entity->getDtaPrimeiroDesconto());
        $this->assertNotNull($entity->getUsuario());
        $this->assertNotNull($entity->getAtivo());
    }
}
