<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\AttributeJson;
use AppBundle\Entity\AttributesJson;
use PHPUnit_Framework_TestCase;

/**
 * Class AttributesJsonTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class AttributesJsonTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $attributesJson = new AttributesJson();
        $attributesJson->setId(1)->setAttributes([new AttributeJson()]);

        $this->assertNotNull($attributesJson->getId());
        $this->assertNotNull($attributesJson->getAttributes());
        $this->assertInternalType('array', $attributesJson->getAttributes());
        $this->assertInstanceOf(AttributeJson::class, $attributesJson->getAttributes()[0]);
    }
}
