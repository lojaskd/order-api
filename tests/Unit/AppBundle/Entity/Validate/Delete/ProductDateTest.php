<?php

namespace Tests\Unit\AppBundle\Entity\Validate\Delete;

use AppBundle\Entity\Validate\Delete\ProductDate;
use AppBundle\Entity\Validate\Delete\ProductDateType;
use AppBundle\Entity\Validate\ValidateProductDateProduct;
use PHPUnit_Framework_TestCase;

/**
 * Class ProductDateTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ProductDateTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ProductDate();

        $entity->setOrderId('1');
        $entity->setDateTypes([new ProductDateType()]);
        $entity->setProducts([new ValidateProductDateProduct()]);
        $entity->setClerk('1');

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getDateTypes());
        $this->assertNotNull($entity->getProducts());
        $this->assertNotNull($entity->getClerk());
    }
}
