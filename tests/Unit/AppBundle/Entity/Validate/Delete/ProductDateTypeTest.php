<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate\Delete;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\Delete\ProductDateType;
        
/**
 * Class ProductDateTypeTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ProductDateTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ProductDateType();
                
        $entity->setType('1');

        $this->assertNotNull($entity->getType());
    }
}
