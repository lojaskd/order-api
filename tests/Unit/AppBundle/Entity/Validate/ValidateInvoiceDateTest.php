<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidateInvoiceDate;
        
/**
 * Class ValidateInvoiceDateTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateInvoiceDateTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateInvoiceDate();
                
        $entity->setOrderId('1');
        $entity->setInvoiceId('1');
        $entity->setType('1');
        $entity->setDate(new DateTime());
        $entity->setClerk('1');

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getInvoiceId());
        $this->assertNotNull($entity->getType());
        $this->assertNotNull($entity->getDate());
        $this->assertNotNull($entity->getClerk());
    }
}
