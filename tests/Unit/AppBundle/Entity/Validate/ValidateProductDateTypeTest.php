<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidateProductDateType;
        
/**
 * Class ValidateProductDateTypeTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateProductDateTypeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateProductDateType();
                
        $entity->setType('1');
        $entity->setDate('1');

        $this->assertNotNull($entity->getType());
        $this->assertNotNull($entity->getDate());
    }
}
