<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidatePayment;
        
/**
 * Class ValidatePaymentTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidatePaymentTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidatePayment();
                
        $entity->setId('1');
        $entity->setAmount('1');
        $entity->setInstalment('1');
        $entity->setInstalmentValue('1');
        $entity->setBilletDeadline('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getAmount());
        $this->assertNotNull($entity->getInstalment());
        $this->assertNotNull($entity->getInstalmentValue());
        $this->assertNotNull($entity->getBilletDeadline());
    }
}
