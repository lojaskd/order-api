<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate\Create;
        
use AppBundle\Entity\Validate\Create\CreateValidateInvoiceItem;
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\Create\CreateValidateInvoice;
        
/**
 * Class CreateValidateInvoiceTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateValidateInvoiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateValidateInvoice();
                
        $entity->setOrderId('1');
        $entity->setInvoiceId('1');
        $entity->setBranchId('1');
        $entity->setSerie('1');
        $entity->setCreatedDate(new DateTime());
        $entity->setShippingDate(new DateTime());
        $entity->setPredictDeliveryDate(new DateTime());
        $entity->setReturnedDate(new DateTime());
        $entity->setReturned('1');
        $entity->setDeliveryDate(new DateTime());
        $entity->setKey('1');
        $entity->setMarketplaceIntegration('1');
        $entity->setClerk('1');
        $entity->setShipperName('1');
        $entity->setShipperDoc('1');
        $entity->setHub('1');
        $entity->setItens([new CreateValidateInvoiceItem()]);
        $entity->addItem(new CreateValidateInvoiceItem());

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getInvoiceId());
        $this->assertNotNull($entity->getBranchId());
        $this->assertNotNull($entity->getSerie());
        $this->assertNotNull($entity->getCreatedDate());
        $this->assertNotNull($entity->getShippingDate());
        $this->assertNotNull($entity->getPredictDeliveryDate());
        $this->assertNotNull($entity->getReturnedDate());
        $this->assertNotNull($entity->isReturned());
        $this->assertNotNull($entity->getDeliveryDate());
        $this->assertNotNull($entity->getKey());
        $this->assertNotNull($entity->isMarketplaceIntegration());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->getShipperName());
        $this->assertNotNull($entity->getShipperDoc());
        $this->assertNotNull($entity->getHub());
        $this->assertNotNull($entity->getItens());
    }
}
