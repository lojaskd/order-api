<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate\Create;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\Create\CreateValidateOrderLog;
        
/**
 * Class CreateValidateOrderLogTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateValidateOrderLogTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateValidateOrderLog();
                
        $entity->setId('1');
        $entity->setTitle('1');
        $entity->setText('1');
        $entity->setShow('1');
        $entity->setStatus('1');
        $entity->setInternalStatus('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getTitle());
        $this->assertNotNull($entity->getText());
        $this->assertNotNull($entity->isShow());
        $this->assertNotNull($entity->getStatus());
        $this->assertNotNull($entity->getInternalStatus());
    }
}
