<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate\Create;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\Create\CreateValidateInvoiceItem;
        
/**
 * Class CreateValidateInvoiceItemTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CreateValidateInvoiceItemTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CreateValidateInvoiceItem();
                
        $entity->setProductReference('1');
        $entity->setReturnedDate(new DateTime());
        $entity->setReturned('1');
        $entity->setQty('1');

        $this->assertNotNull($entity->getProductReference());
        $this->assertNotNull($entity->getReturnedDate());
        $this->assertNotNull($entity->isReturned());
        $this->assertNotNull($entity->getQty());
    }
}
