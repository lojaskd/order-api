<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidateCustomer;
        
/**
 * Class ValidateCustomerTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateCustomerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateCustomer();
                
        $entity->setId('1');
        $entity->setEmail('1');
        $entity->setCpf('1');
        $entity->setFirstName('1');
        $entity->setLastName('1');
        $entity->setAcceptsMarketing('1');
        $entity->setBirthday(new DateTime());
        $entity->setTypeRegister('1');
        $entity->setRgIe('1');
        $entity->setPersonType('1');
        $entity->setGenre('1');
        $entity->setIp('1');
        $entity->setPassword('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getEmail());
        $this->assertNotNull($entity->getCpf());
        $this->assertNotNull($entity->getFirstName());
        $this->assertNotNull($entity->getLastName());
        $this->assertNotNull($entity->isAcceptsMarketing());
        $this->assertNotNull($entity->getBirthday());
        $this->assertNotNull($entity->getTypeRegister());
        $this->assertNotNull($entity->getRgIe());
        $this->assertNotNull($entity->getPersonType());
        $this->assertNotNull($entity->getGenre());
        $this->assertNotNull($entity->getIp());
        $this->assertNotNull($entity->getPassword());
    }
}
