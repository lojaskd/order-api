<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use AppBundle\Entity\Validate\ValidateAddress;
use AppBundle\Entity\Validate\ValidateCoupon;
use AppBundle\Entity\Validate\ValidateCustomer;
use AppBundle\Entity\Validate\ValidateMarketPlace;
use AppBundle\Entity\Validate\ValidateOrigin;
use AppBundle\Entity\Validate\ValidatePayment;
use AppBundle\Entity\Validate\ValidateProduct;
use AppBundle\Entity\Validate\ValidateShipping;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidateOrder;
        
/**
 * Class ValidateOrderTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateOrderTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateOrder();
                
        $entity->setId('1');
        $entity->setClerk('1');
        $entity->setCustomer(new ValidateCustomer());
        $entity->setProducts([new ValidateProduct()]);
        $entity->setSubtotal('1');
        $entity->setTotal('1');
        $entity->setDiscount('1');
        $entity->setShipping(new ValidateShipping());
        $entity->setCoupon(new ValidateCoupon());
        $entity->setOrigin(new ValidateOrigin());
        $entity->setObservation('1');
        $entity->setOrderType('1');
        $entity->setPayments([new ValidatePayment()]);
        $entity->setBillingAddress(new ValidateAddress());
        $entity->setDeliveryAddress(new ValidateAddress());
        $entity->setMarketplace(new ValidateMarketplace());
        $entity->setDevice('1');
        $entity->addProducts(new ValidateProduct());
        $entity->addPayments(new ValidatePayment());

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->getCustomer());
        $this->assertNotNull($entity->getProducts());
        $this->assertNotNull($entity->getSubtotal());
        $this->assertNotNull($entity->getTotal());
        $this->assertNotNull($entity->getDiscount());
        $this->assertNotNull($entity->getShipping());
        $this->assertNotNull($entity->getCoupon());
        $this->assertNotNull($entity->getOrigin());
        $this->assertNotNull($entity->getObservation());
        $this->assertNotNull($entity->getOrderType());
        $this->assertNotNull($entity->getPayments());
        $this->assertNotNull($entity->getBillingAddress());
        $this->assertNotNull($entity->getDeliveryAddress());
        $this->assertNotNull($entity->getMarketplace());
        $this->assertNotNull($entity->getDevice());
    }
}
