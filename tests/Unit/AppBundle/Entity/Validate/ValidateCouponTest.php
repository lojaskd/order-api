<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidateCoupon;
        
/**
 * Class ValidateCouponTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateCouponTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateCoupon();
                
        $entity->setCode('1');
        $entity->setValue('1');

        $this->assertNotNull($entity->getCode());
        $this->assertNotNull($entity->getValue());
    }
}
