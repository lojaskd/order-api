<?php

namespace Tests\Unit\AppBundle\Entity\Validate;

use AppBundle\Entity\Validate\ValidateProductDate;
use AppBundle\Entity\Validate\ValidateProductDateProduct;
use AppBundle\Entity\Validate\ValidateProductDateType;
use PHPUnit_Framework_TestCase;

/**
 * Class ValidateProductDateTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateProductDateTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateProductDate();

        $entity->setOrderId('1');
        $entity->setDateTypes([new ValidateProductDateType()]);
        $entity->setProducts([new ValidateProductDateProduct()]);
        $entity->setClerk('1');
        $entity->setPredict('1');
        $entity->addDateType(new ValidateProductDateType());
        $entity->addProduct(new ValidateProductDateProduct());

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getDateTypes());
        $this->assertNotNull($entity->getProducts());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->isPredict());
    }
}
