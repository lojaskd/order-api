<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidateMarketPlace;
        
/**
 * Class ValidateMarketPlaceTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateMarketPlaceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateMarketPlace();
                
        $entity->setId('1');
        $entity->setOrder('1');
        $entity->setChannelId('1');
        $entity->setNewDeliveryDate(new DateTime());

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getOrder());
        $this->assertNotNull($entity->getChannelId());
        $this->assertNotNull($entity->getNewDeliveryDate());
    }
}
