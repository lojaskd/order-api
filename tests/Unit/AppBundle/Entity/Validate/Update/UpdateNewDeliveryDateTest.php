<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate\Update;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\Update\UpdateNewDeliveryDate;
        
/**
 * Class UpdateNewDeliveryDateTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateNewDeliveryDateTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateNewDeliveryDate();
                
        $entity->setNewDeliveryDate('1');
        $entity->setAccept('1');
        $entity->setUser('1');

        $this->assertNotNull($entity->getNewDeliveryDate());
        $this->assertNotNull($entity->isAccept());
        $this->assertNotNull($entity->getUser());
    }
}
