<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate\Update;
        
use AppBundle\Entity\Validate\Update\UpdateValidateProductStatusProduct;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\Update\UpdateValidateProductReversalInfo;
        
/**
 * Class UpdateValidateProductReversalInfoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateValidateProductReversalInfoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateValidateProductReversalInfo();
                
        $entity->setOrderId('1');
        $entity->setProductId('1');
        $entity->setClerk('1');
        $entity->setTypeReversal('1');
        $entity->setOperator('1');
        $entity->setTransaction('1');
        $entity->setProducts([new UpdateValidateProductStatusProduct()]);
        $entity->addProduct(new UpdateValidateProductStatusProduct());

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getProductId());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->getTypeReversal());
        $this->assertNotNull($entity->getOperator());
        $this->assertNotNull($entity->getTransaction());
        $this->assertNotNull($entity->getProducts());
    }
}
