<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate\Update;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\Update\UpdateValidateInvoice;
        
/**
 * Class UpdateValidateInvoiceTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateValidateInvoiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateValidateInvoice();
                
        $entity->setOrderId('1');
        $entity->setInvoiceId('1');
        $entity->setKey('1');
        $entity->setReturnedDate(new DateTime());
        $entity->setClerk('1');
        $entity->setReturned('1');
        $entity->setDateKey(new DateTime());
        $entity->setMarketplaceIntegration('1');
        $entity->setPathXml('1');
        $entity->setPathPdf('1');
        $entity->setShipperName('1');
        $entity->setShipperDoc('1');
        $entity->setHub('1');

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getInvoiceId());
        $this->assertNotNull($entity->getKey());
        $this->assertNotNull($entity->getReturnedDate());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->isReturned());
        $this->assertNotNull($entity->getDateKey());
        $this->assertNotNull($entity->isMarketplaceIntegration());
        $this->assertNotNull($entity->getPathXml());
        $this->assertNotNull($entity->getPathPdf());
        $this->assertNotNull($entity->getShipperName());
        $this->assertNotNull($entity->getShipperDoc());
        $this->assertNotNull($entity->getHub());
    }
}
