<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate\Update;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\Update\UpdateValidateProductStatusProduct;
        
/**
 * Class UpdateValidateProductStatusProductTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class UpdateValidateProductStatusProductTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new UpdateValidateProductStatusProduct();
                
        $entity->setId('1');

        $this->assertNotNull($entity->getId());
    }
}
