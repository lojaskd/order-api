<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate\Update;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\Update\ValidateProviderBillingDate;
        
/**
 * Class ValidateProviderBillingDateTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateProviderBillingDateTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateProviderBillingDate();
                
        $entity->setOrderId('1');
        $entity->setPurchaseOrder('1');
        $entity->setType('1');
        $entity->setClerk('1');
        $entity->setDate('1');

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getPurchaseOrder());
        $this->assertNotNull($entity->getType());
        $this->assertNotNull($entity->getClerk());
        $this->assertNotNull($entity->getDate());
    }
}
