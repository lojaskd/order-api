<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use AppBundle\Entity\Validate\ValidatePurchaseOrderProduct;
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidatePurchaseOrder;
        
/**
 * Class ValidatePurchaseOrderTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidatePurchaseOrderTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidatePurchaseOrder();
                
        $entity->setOrderId('1');
        $entity->setPurchaseOrderId('1');
        $entity->setDate(new DateTime());
        $entity->setProducts([new ValidatePurchaseOrderProduct()]);
        $entity->setClerk('1');
        $entity->addProduct(new ValidatePurchaseOrderProduct());

        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getPurchaseOrderId());
        $this->assertNotNull($entity->getDate());
        $this->assertNotNull($entity->getProducts());
        $this->assertNotNull($entity->getClerk());
    }
}
