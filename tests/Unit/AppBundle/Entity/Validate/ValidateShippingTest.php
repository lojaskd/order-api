<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidateShipping;
        
/**
 * Class ValidateShippingTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateShippingTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateShipping();
                
        $entity->setType('1');
        $entity->setPrice('1');

        $this->assertNotNull($entity->getType());
        $this->assertNotNull($entity->getPrice());
    }
}
