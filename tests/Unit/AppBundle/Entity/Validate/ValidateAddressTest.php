<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidateAddress;
        
/**
 * Class ValidateAddressTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateAddressTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateAddress();
                
        $entity->setIdentification('1');
        $entity->setFirstName('1');
        $entity->setLastName('1');
        $entity->setAddress('1');
        $entity->setNumber('1');
        $entity->setComplement('1');
        $entity->setNeighborhood('1');
        $entity->setCity('1');
        $entity->setState('1');
        $entity->setZip('1');
        $entity->setCountry('1');
        $entity->setTelephoneA('1');
        $entity->setTelephoneB('1');
        $entity->setReference('1');

        $this->assertNotNull($entity->getIdentification());
        $this->assertNotNull($entity->getFirstName());
        $this->assertNotNull($entity->getLastName());
        $this->assertNotNull($entity->getAddress());
        $this->assertNotNull($entity->getNumber());
        $this->assertNotNull($entity->getComplement());
        $this->assertNotNull($entity->getNeighborhood());
        $this->assertNotNull($entity->getCity());
        $this->assertNotNull($entity->getState());
        $this->assertNotNull($entity->getZip());
        $this->assertNotNull($entity->getCountry());
        $this->assertNotNull($entity->getTelephoneA());
        $this->assertNotNull($entity->getTelephoneB());
        $this->assertNotNull($entity->getReference());
    }
}
