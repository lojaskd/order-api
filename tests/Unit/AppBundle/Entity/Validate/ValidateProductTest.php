<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidateProduct;
        
/**
 * Class ValidateProductTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateProductTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateProduct();
                
        $entity->setId('1');
        $entity->setKitId('1');
        $entity->setName('1');
        $entity->setQuantity('1');
        $entity->setPrice('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getKitId());
        $this->assertNotNull($entity->getName());
        $this->assertNotNull($entity->getQuantity());
        $this->assertNotNull($entity->getPrice());
    }
}
