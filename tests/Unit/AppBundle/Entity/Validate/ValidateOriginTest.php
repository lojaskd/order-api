<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidateOrigin;
        
/**
 * Class ValidateOriginTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidateOriginTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidateOrigin();
                
        $entity->setSource('1');
        $entity->setMedium('1');
        $entity->setContent('1');

        $this->assertNotNull($entity->getSource());
        $this->assertNotNull($entity->getMedium());
        $this->assertNotNull($entity->getContent());
    }
}
