<?php 
      
namespace Tests\Unit\AppBundle\Entity\Validate;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Validate\ValidatePurchaseOrderProduct;
        
/**
 * Class ValidatePurchaseOrderProductTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class ValidatePurchaseOrderProductTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new ValidatePurchaseOrderProduct();
                
        $entity->setId('1');

        $this->assertNotNull($entity->getId());
    }
}
