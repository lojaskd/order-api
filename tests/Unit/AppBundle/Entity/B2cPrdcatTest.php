<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdcat;
        
/**
 * Class B2cPrdcatTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdcatTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdcat();
                
        $entity->setOrdem('1');
        $entity->setVisivel('1');
        $entity->setShopping('1');
        $entity->setNome('1');
        $entity->setLink('1');
        $entity->setPai('1');
        $entity->setCodigoerp('1');
        $entity->setCadeiaCompleta('1');
        $entity->setNomeCompleto('1');
        $entity->setSlug('1');
        $entity->setSlugCat('1');
        $entity->setGoogleCategoryTaxonomy('1');
        $entity->setPrioridade('1');
        $entity->setCategoriaKit('1');
        $entity->setPchaves('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getOrdem());
        $this->assertNotNull($entity->getVisivel());
        $this->assertNotNull($entity->getShopping());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getLink());
        $this->assertNotNull($entity->getPai());
        $this->assertNotNull($entity->getCodigoerp());
        $this->assertNotNull($entity->getCadeiaCompleta());
        $this->assertNotNull($entity->getNomeCompleto());
        $this->assertNotNull($entity->getSlug());
        $this->assertNotNull($entity->getSlugCat());
        $this->assertNotNull($entity->getGoogleCategoryTaxonomy());
        $this->assertNotNull($entity->getPrioridade());
        $this->assertNotNull($entity->getCategoriaKit());
        $this->assertNotNull($entity->getPchaves());
    }
}
