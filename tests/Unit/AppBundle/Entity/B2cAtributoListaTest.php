<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\B2cAtributoLista;
use PHPUnit_Framework_TestCase;

/**
 * Class B2cAtributoListaTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cAtributoListaTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $b2cAtributoLista = new B2cAtributoLista();
        $b2cAtributoLista->setIdAtributo(1)
            ->setSlug('test')
            ->setDescricao('Description')
            ->setIdListaAtributo(1);

        $this->assertNotNull($b2cAtributoLista->getIdAtributo());
        $this->assertNotNull($b2cAtributoLista->getSlug());
        $this->assertNotNull($b2cAtributoLista->getDescricao());
        $this->assertNotNull($b2cAtributoLista->getIdListaAtributo());
    }
}
