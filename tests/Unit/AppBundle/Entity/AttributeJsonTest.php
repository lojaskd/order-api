<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\AttributeJson;
use PHPUnit_Framework_TestCase;

/**
 * Class AttributeJsonTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class AttributeJsonTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $attributeJson = new AttributeJson();

        $attributeJson->setValue(['value1' => '1'])->setIdAttr(1);

        $this->assertNotNull($attributeJson->getValue());
        $this->assertInternalType('array', $attributeJson->getValue());
        $this->assertNotNull($attributeJson->getIdAttr());
        $this->assertInternalType('int', $attributeJson->getIdAttr());
    }
}
