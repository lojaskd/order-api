<?php

namespace Tests\Unit\AppBundle\Entity;

use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cClicli;

/**
 * Class B2cClicliTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cClicliTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cClicli();

        $entity->setTipoPessoa('1');
        $entity->setCpfCgc('1');
        $entity->setNome('1');
        $entity->setSobrenome('1');
        $entity->setEmail('1');
        $entity->setSenha('1');
        $entity->setDtaNasc(new DateTime());
        $entity->setRgIe('1');
        $entity->setContato('1');
        $entity->setSexo('1');
        $entity->setNewsletter('1');
        $entity->setEndereco('1');
        $entity->setNumero('1');
        $entity->setComplemento('1');
        $entity->setBairro('1');
        $entity->setEstado('1');
        $entity->setProvincia('1');
        $entity->setCidade('1');
        $entity->setPais('1');
        $entity->setCep('1');
        $entity->setTelefoneA('1');
        $entity->setTelefoneB('1');
        $entity->setReferencia('1');
        $entity->setSenhaB64('1');
        $entity->setCadastro(new DateTime());
        $entity->setPrimeiroAcesso(new DateTime());
        $entity->setStatus('1');
        $entity->setDescStatus('1');
        $entity->setDtStatus(new DateTime());
        $entity->setDataAfiafi(new DateTime());
        $entity->setAfiliado('1');
        $entity->setPrimeiraOrigem('1');
        $entity->setCadastroOrigem('1');
        $entity->setClienteOrigem('1');
        $entity->setDataClienteOrigem(new DateTime());
        $entity->setPrimeiraOrigemAfiliado('1');
        $entity->setProcessadows('1');
        $entity->setDtaAtualizacao(new DateTime());
        $entity->setSfAccount('1');
        $entity->setSfDataSync(new DateTime());
        $entity->setTipoCadastro('1');
        $entity->setIdClicli(1);

        $this->assertNotNull($entity->getIdClicli());
        $this->assertNotNull($entity->getTipoPessoa());
        $this->assertNotNull($entity->getCpfCgc());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getSobrenome());
        $this->assertNotNull($entity->getEmail());
        $this->assertNotNull($entity->getSenha());
        $this->assertNotNull($entity->getDtaNasc());
        $this->assertNotNull($entity->getRgIe());
        $this->assertNotNull($entity->getContato());
        $this->assertNotNull($entity->getSexo());
        $this->assertNotNull($entity->getNewsletter());
        $this->assertNotNull($entity->getEndereco());
        $this->assertNotNull($entity->getNumero());
        $this->assertNotNull($entity->getComplemento());
        $this->assertNotNull($entity->getBairro());
        $this->assertNotNull($entity->getEstado());
        $this->assertNotNull($entity->getProvincia());
        $this->assertNotNull($entity->getCidade());
        $this->assertNotNull($entity->getPais());
        $this->assertNotNull($entity->getCep());
        $this->assertNotNull($entity->getTelefoneA());
        $this->assertNotNull($entity->getTelefoneB());
        $this->assertNotNull($entity->getReferencia());
        $this->assertNotNull($entity->getSenhaB64());
        $this->assertNotNull($entity->getCadastro());
        $this->assertNotNull($entity->getPrimeiroAcesso());
        $this->assertNotNull($entity->getStatus());
        $this->assertNotNull($entity->getDescStatus());
        $this->assertNotNull($entity->getDtStatus());
        $this->assertNotNull($entity->getDataAfiafi());
        $this->assertNotNull($entity->getAfiliado());
        $this->assertNotNull($entity->getPrimeiraOrigem());
        $this->assertNotNull($entity->getCadastroOrigem());
        $this->assertNotNull($entity->getClienteOrigem());
        $this->assertNotNull($entity->getDataClienteOrigem());
        $this->assertNotNull($entity->getPrimeiraOrigemAfiliado());
        $this->assertNotNull($entity->getProcessadows());
        $this->assertNotNull($entity->getDtaAtualizacao());
        $this->assertNotNull($entity->getSfAccount());
        $this->assertNotNull($entity->getSfDataSync());
        $this->assertNotNull($entity->getTipoCadastro());
    }
}
