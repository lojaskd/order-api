<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cProdutosKits;
        
/**
 * Class B2cProdutosKitsTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cProdutosKitsTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cProdutosKits();
                
        $entity->setIdKit('1');
        $entity->setIdProduto('1');
        $entity->setPrecoProduto('1');
        $entity->setQuantidade('1');
        $entity->setDesconto('1');
        $entity->setObrigatorio('1');
        $entity->setDesmarcar('1');
        $entity->setOrdem('1');
        $entity->setAutomatico('1');

        $this->assertNotNull($entity->getIdKit());
        $this->assertNotNull($entity->getIdProduto());
        $this->assertNotNull($entity->getPrecoProduto());
        $this->assertNotNull($entity->getQuantidade());
        $this->assertNotNull($entity->getDesconto());
        $this->assertNotNull($entity->getObrigatorio());
        $this->assertNotNull($entity->getDesmarcar());
        $this->assertNotNull($entity->getOrdem());
        $this->assertNotNull($entity->getAutomatico());
    }
}
