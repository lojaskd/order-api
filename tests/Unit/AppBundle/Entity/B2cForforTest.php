<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cForfor;
        
/**
 * Class B2cForforTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cForforTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cForfor();
                
        $entity->setNome('1');
        $entity->setMarca('1');
        $entity->setPrazo('1');
        $entity->setCodigoerp('1');
        $entity->setSlugFornecedor('1');
        $entity->setRepresentante('1');
        $entity->setCidade('1');
        $entity->setEstado('1');
        $entity->setPrazoColeta('1');
        $entity->setReputacao('1');
        $entity->setRazaoSocial('1');
        $entity->setPnSap('1');
        $entity->setCnpj('1');
        $entity->setStatusFornecedor('1');
        $entity->setPrazoPagamentoFornecedor('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getMarca());
        $this->assertNotNull($entity->getPrazo());
        $this->assertNotNull($entity->getCodigoerp());
        $this->assertNotNull($entity->getSlugFornecedor());
        $this->assertNotNull($entity->getRepresentante());
        $this->assertNotNull($entity->getCidade());
        $this->assertNotNull($entity->getEstado());
        $this->assertNotNull($entity->getPrazoColeta());
        $this->assertNotNull($entity->getReputacao());
        $this->assertNotNull($entity->getRazaoSocial());
        $this->assertNotNull($entity->getPnSap());
        $this->assertNotNull($entity->getCnpj());
        $this->assertNotNull($entity->getStatusFornecedor());
        $this->assertNotNull($entity->getPrazoPagamentoFornecedor());
    }
}
