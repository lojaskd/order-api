<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoCat;
        
/**
 * Class B2cPromoCatTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoCatTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoCat();
                
        $entity->setIdPromo('1');
        $entity->setIdCategoria('1');

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getIdCategoria());
    }
}
