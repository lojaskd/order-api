<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdprdMarketplace;
        
/**
 * Class B2cPrdprdMarketplaceTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdprdMarketplaceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdprdMarketplace();
                
        $entity->setIdMarketplace('1');
        $entity->setIdPrdprd('1');
        $entity->setEstoque('1');
        $entity->setPreco('1');
        $entity->setAcao('1');
        $entity->setPreco2('1');
        $entity->setPreco3('1');
        $entity->setPreco4('1');
        $entity->setDtaSync('1');
        $entity->setDtaCad('1');
        $entity->setDtaAlt('1');

        $this->assertNotNull($entity->getIdMarketplace());
        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getEstoque());
        $this->assertNotNull($entity->getPreco());
        $this->assertNotNull($entity->getAcao());
        $this->assertNotNull($entity->getPreco2());
        $this->assertNotNull($entity->getPreco3());
        $this->assertNotNull($entity->getPreco4());
        $this->assertNotNull($entity->getDtaSync());
        $this->assertNotNull($entity->getDtaCad());
        $this->assertNotNull($entity->getDtaAlt());
    }
}
