<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoReg;
        
/**
 * Class B2cPromoRegTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoRegTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoReg();
                
        $entity->setIdPromo('1');
        $entity->setIdRegiao('1');

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getIdRegiao());
    }
}
