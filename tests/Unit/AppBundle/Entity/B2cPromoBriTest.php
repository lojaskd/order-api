<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoBri;
        
/**
 * Class B2cPromoBriTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoBriTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoBri();
                
        $entity->setIdPromo('1');
        $entity->setIdBrinde('1');

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getIdBrinde());
    }
}
