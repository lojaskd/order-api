<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cFilial;
        
/**
 * Class B2cFilialTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cFilialTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cFilial();
                
        $entity->setNome('1');
        $entity->setCnpj('1');
        $entity->setMatriz('1');
        $entity->setAtivo('1');
        $entity->setLogisticaPrazoCompra('1');
        $entity->setLogisticaPrazoRecebimento('1');
        $entity->setLogisticaPrazoRomaneio('1');
        $entity->setLogisticaPrazoSeparacao('1');
        $entity->setCep('1');
        $entity->setPrioridade('1');
        $entity->setIdFilial(1);

        $this->assertNotNull($entity->getIdFilial());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getCnpj());
        $this->assertNotNull($entity->getMatriz());
        $this->assertNotNull($entity->getAtivo());
        $this->assertNotNull($entity->getLogisticaPrazoCompra());
        $this->assertNotNull($entity->getLogisticaPrazoRecebimento());
        $this->assertNotNull($entity->getLogisticaPrazoRomaneio());
        $this->assertNotNull($entity->getLogisticaPrazoSeparacao());
        $this->assertNotNull($entity->getCep());
        $this->assertNotNull($entity->getPrioridade());
    }
}
