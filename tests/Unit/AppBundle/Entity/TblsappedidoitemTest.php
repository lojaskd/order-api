<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\Tblsappedido;
use AppBundle\Entity\Tblsappedidoitem;
use DateTime;
use PHPUnit_Framework_TestCase;

/**
 * Class TblsappedidoitemTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class TblsappedidoitemTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new Tblsappedidoitem();

        $entity->setIdItem('1');
        $entity->setQtVendida('1');
        $entity->setVlUnitario('1');
        $entity->setPerDesconto('1');
        $entity->setDtDisponivel(new DateTime());
        $entity->setIdFilial('1');
        $entity->setStatus('1');
        $entity->setDtSupply(new DateTime());
        $entity->setEntregaImediata('1');
        $entity->setDestino('1');
        $entity->setIdPedido(new Tblsappedido());

        $this->assertNotNull($entity->getIdItem());
        $this->assertNotNull($entity->getQtVendida());
        $this->assertNotNull($entity->getVlUnitario());
        $this->assertNotNull($entity->getPerDesconto());
        $this->assertNotNull($entity->getDtDisponivel());
        $this->assertNotNull($entity->getIdFilial());
        $this->assertNotNull($entity->getStatus());
        $this->assertNotNull($entity->getDtSupply());
        $this->assertNotNull($entity->getEntregaImediata());
        $this->assertNotNull($entity->getDestino());
        $this->assertNotNull($entity->getIdPedido());
    }
}
