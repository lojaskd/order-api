<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\OrderAttributes;
        
/**
 * Class OrderAttributesTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class OrderAttributesTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new OrderAttributes();
                
        $entity->setId('1');
        $entity->setOrderId('1');
        $entity->setIdAttr('1');
        $entity->setValue('1');
        $entity->setCreatedAt('1');
        $entity->setModifiedAt('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getOrderId());
        $this->assertNotNull($entity->getIdAttr());
        $this->assertNotNull($entity->getValue());
        $this->assertNotNull($entity->getCreatedAt());
        $this->assertNotNull($entity->getModifiedAt());
    }
}
