<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cRegtransVal;
        
/**
 * Class B2cRegtransValTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cRegtransValTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cRegtransVal();
                
        $entity->setIdKg('1');
        $entity->setIdMc('1');
        $entity->setIdTrans('1');
        $entity->setTipo('1');
        $entity->setValor('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getIdKg());
        $this->assertNotNull($entity->getIdMc());
        $this->assertNotNull($entity->getIdTrans());
        $this->assertNotNull($entity->getTipo());
        $this->assertNotNull($entity->getValor());
    }
}
