<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPagamentoCartao;
        
/**
 * Class B2cPagamentoCartaoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPagamentoCartaoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPagamentoCartao();
                
        $entity->setIdTipoPagamento('1');
        $entity->setIdCliente('1');
        $entity->setIdPedido('1');
        $entity->setTid('1');
        $entity->setCodigoAutorizacaoPagamento('1');
        $entity->setNumeroComprovanteVenda('1');
        $entity->setPagamentoCapturado('1');
        $entity->setPago('1');
        $entity->setPagoOperadora('1');
        $entity->setNumeroPagamento('1');
        $entity->setValorPagamento('1');
        $entity->setQtdParcela('1');
        $entity->setStatusPagamento('1');
        $entity->setDataCriado(new DateTime());
        $entity->setIdPagamentoCartao('1');

        $this->assertNotNull($entity->getIdPagamentoCartao());
        $this->assertNotNull($entity->getIdTipoPagamento());
        $this->assertNotNull($entity->getIdCliente());
        $this->assertNotNull($entity->getIdPedido());
        $this->assertNotNull($entity->getTid());
        $this->assertNotNull($entity->getCodigoAutorizacaoPagamento());
        $this->assertNotNull($entity->getNumeroComprovanteVenda());
        $this->assertNotNull($entity->getPagamentoCapturado());
        $this->assertNotNull($entity->getPago());
        $this->assertNotNull($entity->getPagoOperadora());
        $this->assertNotNull($entity->getNumeroPagamento());
        $this->assertNotNull($entity->getValorPagamento());
        $this->assertNotNull($entity->getQtdParcela());
        $this->assertNotNull($entity->getStatusPagamento());
        $this->assertNotNull($entity->getDataCriado());
    }
}
