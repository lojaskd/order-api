<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPedpedMarketplace;
        
/**
 * Class B2cPedpedMarketplaceTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPedpedMarketplaceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPedpedMarketplace();
                
        $entity->setIdMarketplace('1');
        $entity->setIdPedped('1');
        $entity->setCodOrder('1');
        $entity->setIdChannel('1');
        $entity->setPedSituacao('1');
        $entity->setDtaCad('1');
        $entity->setDeliveryDate('1');

        $this->assertNotNull($entity->getIdMarketplace());
        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getCodOrder());
        $this->assertNotNull($entity->getIdChannel());
        $this->assertNotNull($entity->getPedSituacao());
        $this->assertNotNull($entity->getDtaCad());
        $this->assertNotNull($entity->getDeliveryDate());
    }
}
