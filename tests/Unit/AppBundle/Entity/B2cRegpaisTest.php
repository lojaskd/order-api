<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cRegpais;
        
/**
 * Class B2cRegpaisTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cRegpaisTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cRegpais();
                
        $entity->setNome('1');
        $entity->setIdPais(1);

        $this->assertNotNull($entity->getIdPais());
        $this->assertNotNull($entity->getNome());
    }
}
