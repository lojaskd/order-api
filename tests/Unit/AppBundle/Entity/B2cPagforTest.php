<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPagfor;
        
/**
 * Class B2cPagforTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPagforTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPagfor();
                
        $entity->setIdPagpag('1');
        $entity->setNome('1');
        $entity->setValMinimo('1');
        $entity->setNumParcelas('1');
        $entity->setIndexador('1');
        $entity->setPadrao('1');
        $entity->setPai('1');
        $entity->setIdPagfor('1');

        $this->assertNotNull($entity->getIdPagfor());
        $this->assertNotNull($entity->getIdPagpag());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getValMinimo());
        $this->assertNotNull($entity->getNumParcelas());
        $this->assertNotNull($entity->getIndexador());
        $this->assertNotNull($entity->getPadrao());
        $this->assertNotNull($entity->getPai());
    }
}
