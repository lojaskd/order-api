<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoPag;
        
/**
 * Class B2cPromoPagTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoPagTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoPag();
                
        $entity->setIdPromo('1');
        $entity->setIdPagpag('1');
        $entity->setIdPagfor('1');

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getIdPagpag());
        $this->assertNotNull($entity->getIdPagfor());
    }
}
