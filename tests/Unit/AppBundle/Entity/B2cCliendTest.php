<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cCliend;
        
/**
 * Class B2cCliendTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cCliendTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cCliend();
                
        $entity->setIdClicli('1');
        $entity->setIdentificacao('1');
        $entity->setNome('1');
        $entity->setSobrenome('1');
        $entity->setEndereco('1');
        $entity->setNumero('1');
        $entity->setComplemento('1');
        $entity->setBairro('1');
        $entity->setCidade('1');
        $entity->setEstado('1');
        $entity->setProvincia('1');
        $entity->setPais('1');
        $entity->setCep('1');
        $entity->setTelefoneA('1');
        $entity->setTelefoneB('1');
        $entity->setReferencia('1');
        $entity->setIdCliend(1);

        $this->assertNotNull($entity->getIdCliend());
        $this->assertNotNull($entity->getIdClicli());
        $this->assertNotNull($entity->getIdentificacao());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getSobrenome());
        $this->assertNotNull($entity->getEndereco());
        $this->assertNotNull($entity->getNumero());
        $this->assertNotNull($entity->getComplemento());
        $this->assertNotNull($entity->getBairro());
        $this->assertNotNull($entity->getCidade());
        $this->assertNotNull($entity->getEstado());
        $this->assertNotNull($entity->getProvincia());
        $this->assertNotNull($entity->getPais());
        $this->assertNotNull($entity->getCep());
        $this->assertNotNull($entity->getTelefoneA());
        $this->assertNotNull($entity->getTelefoneB());
        $this->assertNotNull($entity->getReferencia());
    }
}
