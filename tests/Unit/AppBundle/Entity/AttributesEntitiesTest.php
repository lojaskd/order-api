<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\AttributesEntities;
use DateTime;
use PHPUnit_Framework_TestCase;

/**
 * Class AttributesEntitiesTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class AttributesEntitiesTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $attributesEntities = new AttributesEntities();

        $attributesEntities->setId(1)
            ->setEntity('EntityName')
            ->setIdAttr(1)
            ->setCreatedAt(new DateTime())
            ->setModifiedAt(new DateTime());

        $this->assertNotNull($attributesEntities->getId());
        $this->assertNotNull($attributesEntities->getEntity());
        $this->assertNotNull($attributesEntities->getIdAttr());
        $this->assertNotNull($attributesEntities->getCreatedAt());
        $this->assertInstanceOf(DateTime::class, $attributesEntities->getCreatedAt());
        $this->assertNotNull($attributesEntities->getModifiedAt());
        $this->assertInstanceOf(DateTime::class, $attributesEntities->getModifiedAt());
    }
}
