<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromo;
        
/**
 * Class B2cPromoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromo();
                
        $entity->setAtivo('1');
        $entity->setPrioridade('1');
        $entity->setValeCompra('1');
        $entity->setDtaInclusao(new DateTime());
        $entity->setDtaAlteracao(new DateTime());
        $entity->setResponsavel('1');
        $entity->setDtaValidadeInicio(new DateTime());
        $entity->setHraValidadeInicio(new DateTime());
        $entity->setDtaValidadeFim(new DateTime());
        $entity->setHraValidadeFim(new DateTime());
        $entity->setNome('1');
        $entity->setDescricaoSimples('1');
        $entity->setDescricaoCompleta('1');
        $entity->setImgListagem('1');
        $entity->setImgDescricao('1');
        $entity->setDescontoPerc('1');
        $entity->setDescontoValor('1');
        $entity->setFreteGratis('1');
        $entity->setFreteFixo('1');
        $entity->setFreteFixoValor('1');
        $entity->setValorMinimo('1');
        $entity->setUsarTodaloja('1');
        $entity->setUsarProdutos('1');
        $entity->setUsarSituacao('1');
        $entity->setUsarCategoria('1');
        $entity->setUsarFornecedor('1');
        $entity->setAplicarCliente('1');
        $entity->setAplicarProdutos('1');
        $entity->setAplicarPagamento('1');
        $entity->setAplicarEntrega('1');
        $entity->setPromocaoBrinde('1');
        $entity->setAplicarCarrinho('1');
        $entity->setValorExato('1');
        $entity->setQtdMinima('1');
        $entity->setQtdExata('1');
        $entity->setQtdBrindes('1');
        $entity->setSorteio('1');
        $entity->setPromoIncluiFrete('1');
        $entity->setTipoVale('1');
        $entity->setIdPromo(1);

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getAtivo());
        $this->assertNotNull($entity->getPrioridade());
        $this->assertNotNull($entity->getValeCompra());
        $this->assertNotNull($entity->getDtaInclusao());
        $this->assertNotNull($entity->getDtaAlteracao());
        $this->assertNotNull($entity->getResponsavel());
        $this->assertNotNull($entity->getDtaValidadeInicio());
        $this->assertNotNull($entity->getHraValidadeInicio());
        $this->assertNotNull($entity->getDtaValidadeFim());
        $this->assertNotNull($entity->getHraValidadeFim());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getDescricaoSimples());
        $this->assertNotNull($entity->getDescricaoCompleta());
        $this->assertNotNull($entity->getImgListagem());
        $this->assertNotNull($entity->getImgDescricao());
        $this->assertNotNull($entity->getDescontoPerc());
        $this->assertNotNull($entity->getDescontoValor());
        $this->assertNotNull($entity->getFreteGratis());
        $this->assertNotNull($entity->getFreteFixo());
        $this->assertNotNull($entity->getFreteFixoValor());
        $this->assertNotNull($entity->getValorMinimo());
        $this->assertNotNull($entity->getUsarTodaloja());
        $this->assertNotNull($entity->getUsarProdutos());
        $this->assertNotNull($entity->getUsarSituacao());
        $this->assertNotNull($entity->getUsarCategoria());
        $this->assertNotNull($entity->getUsarFornecedor());
        $this->assertNotNull($entity->getAplicarCliente());
        $this->assertNotNull($entity->getAplicarProdutos());
        $this->assertNotNull($entity->getAplicarPagamento());
        $this->assertNotNull($entity->getAplicarEntrega());
        $this->assertNotNull($entity->getPromocaoBrinde());
        $this->assertNotNull($entity->getAplicarCarrinho());
        $this->assertNotNull($entity->getValorExato());
        $this->assertNotNull($entity->getQtdMinima());
        $this->assertNotNull($entity->getQtdExata());
        $this->assertNotNull($entity->getQtdBrindes());
        $this->assertNotNull($entity->getSorteio());
        $this->assertNotNull($entity->getPromoIncluiFrete());
        $this->assertNotNull($entity->getTipoVale());
    }
}
