<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoUtm;
        
/**
 * Class B2cPromoUtmTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoUtmTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoUtm();
                
        $entity->setIdPromo('1');
        $entity->setSource('1');
        $entity->setContent('1');
        $entity->setIdPromoUtm(1);

        $this->assertNotNull($entity->getIdPromoUtm());
        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getSource());
        $this->assertNotNull($entity->getContent());
    }
}
