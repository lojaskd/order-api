<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\AxadoNotafiscal;
use DateTime;
use PHPUnit_Framework_TestCase;

/**
 * Class AxadoNotafiscalTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class AxadoNotafiscalTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $axadoNotafiscal = new AxadoNotafiscal();
        $axadoNotafiscal->setCreatedAt(new DateTime())
            ->setDataModificado(new DateTime())
            ->setDataSincronizado(new DateTime())
            ->setIdPedped(1)
            ->setIdFilial(1)
            ->setIdNotafiscal(1234)
            ->setSerieNotafiscal(1)
            ->setValorNotafiscal(359.9)
            ->setValorIcms(42.9)
            ->setValorFrete(59.6)
            ->setTokenAxado('xpto');

        $this->assertNotNull($axadoNotafiscal->getCreatedAt());
        $this->assertNotNull($axadoNotafiscal->getDataModificado());
        $this->assertNotNull($axadoNotafiscal->getDataSincronizado());
        $this->assertNotNull($axadoNotafiscal->getIdPedped());
        $this->assertNotNull($axadoNotafiscal->getIdFilial());
        $this->assertNotNull($axadoNotafiscal->getIdNotafiscal());
        $this->assertNotNull($axadoNotafiscal->getSerieNotafiscal());
        $this->assertNotNull($axadoNotafiscal->getValorNotafiscal());
        $this->assertNotNull($axadoNotafiscal->getValorIcms());
        $this->assertNotNull($axadoNotafiscal->getValorFrete());
        $this->assertNotNull($axadoNotafiscal->getTokenAxado());
    }
}
