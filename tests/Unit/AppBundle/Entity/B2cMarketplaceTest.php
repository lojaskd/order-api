<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cMarketplace;
        
/**
 * Class B2cMarketplaceTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cMarketplaceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cMarketplace();
                
        $entity->setId('1');
        $entity->setNome('1');
        $entity->setIdConta('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getIdConta());
    }
}
