<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cTratra;
        
/**
 * Class B2cTratraTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cTratraTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cTratra();
                
        $entity->setIdRegreg('1');
        $entity->setNomeInterno('1');
        $entity->setNomePublico('1');
        $entity->setPadrao('1');
        $entity->setTipoFrete('1');
        $entity->setValorFreteFixo('1');
        $entity->setPrazoTransportadora('1');
        $entity->setPrazoFreteGratuito('1');
        $entity->setPrazoFreteFixo('1');
        $entity->setSeguro('1');
        $entity->setCategoria('1');
        $entity->setDescricao('1');
        $entity->setTipoServico('1');
        $entity->setLimitadorPeso('1');
        $entity->setCepReferencia('1');
        $entity->setIdFilial('1');
        $entity->setIdTratra(1);

        $this->assertNotNull($entity->getIdTratra());
        $this->assertNotNull($entity->getIdRegreg());
        $this->assertNotNull($entity->getNomeInterno());
        $this->assertNotNull($entity->getNomePublico());
        $this->assertNotNull($entity->getPadrao());
        $this->assertNotNull($entity->getTipoFrete());
        $this->assertNotNull($entity->getValorFreteFixo());
        $this->assertNotNull($entity->getPrazoTransportadora());
        $this->assertNotNull($entity->getPrazoFreteGratuito());
        $this->assertNotNull($entity->getPrazoFreteFixo());
        $this->assertNotNull($entity->getSeguro());
        $this->assertNotNull($entity->getCategoria());
        $this->assertNotNull($entity->getDescricao());
        $this->assertNotNull($entity->getTipoServico());
        $this->assertNotNull($entity->getLimitadorPeso());
        $this->assertNotNull($entity->getCepReferencia());
        $this->assertNotNull($entity->getIdFilial());
    }
}
