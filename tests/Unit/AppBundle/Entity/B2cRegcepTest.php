<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use AppBundle\Entity\B2cTratra;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cRegcep;
        
/**
 * Class B2cRegcepTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cRegcepTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cRegcep();

        $transportadoras = new ArrayCollection();
        $transportadoras->add(new B2cTratra());
                
        $entity->setIdReg('1');
        $entity->setIniCep('1');
        $entity->setFimCep('1');
        $entity->setTransportadoras($transportadoras);
        $entity->setIdCep(1);

        $this->assertNotNull($entity->getIdCep());
        $this->assertNotNull($entity->getIdReg());
        $this->assertNotNull($entity->getIniCep());
        $this->assertNotNull($entity->getFimCep());
        $this->assertNotNull($entity->getTransportadoras());
    }
}
