<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\Attributes;
use DateTime;
use PHPUnit_Framework_TestCase;

/**
 * Class AttributesTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class AttributesTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $attributes = new Attributes();

        $attributes->setId(1)
            ->setAttrType(1)
            ->setCreatedAt(new DateTime())
            ->setDefault('test')
            ->setHint(true)
            ->setModifiedAt(new DateTime())
            ->setMultiple(false)
            ->setName('test')
            ->setRequired(true);

        $this->assertNotNull($attributes->getId());
        $this->assertNotNull($attributes->getAttrType());
        $this->assertNotNull($attributes->getCreatedAt());
        $this->assertInstanceOf(DateTime::class, $attributes->getCreatedAt());
        $this->assertNotNull($attributes->getDefault());
        $this->assertNotNull($attributes->isHint());
        $this->assertNotNull($attributes->getModifiedAt());
        $this->assertInstanceOf(DateTime::class, $attributes->getModifiedAt());
        $this->assertNotNull($attributes->isMultiple());
        $this->assertNotNull($attributes->getName());
        $this->assertNotNull($attributes->isRequired());
    }
}
