<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdselcat;
        
/**
 * Class B2cPrdselcatTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdselcatTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdselcat();
                
        $entity->setIdPrd('1');
        $entity->setIdCat('1');

        $this->assertNotNull($entity->getIdPrd());
        $this->assertNotNull($entity->getIdCat());
    }
}
