<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\Tblsappedido;
        
/**
 * Class TblsappedidoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class TblsappedidoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new Tblsappedido();
                
        $entity->setIdCliente('1');
        $entity->setNomeCliente('1');
        $entity->setCpfCliente('1');
        $entity->setCnpjCliente('1');
        $entity->setRgCliente('1');
        $entity->setInscestCliente('1');
        $entity->setDddCliente('1');
        $entity->setFoneCliente('1');
        $entity->setEmailCliente('1');
        $entity->setNomeendereco('1');
        $entity->setLogradouro('1');
        $entity->setNomerua('1');
        $entity->setNumero('1');
        $entity->setComplemento('1');
        $entity->setBairro('1');
        $entity->setCep('1');
        $entity->setCidade('1');
        $entity->setEstado('1');
        $entity->setEntreganomeendereco('1');
        $entity->setEntregalogradouro('1');
        $entity->setEntreganomerua('1');
        $entity->setEntreganumero('1');
        $entity->setEntregacomplemento('1');
        $entity->setEntregabairro('1');
        $entity->setEntregacep('1');
        $entity->setEntregacidade('1');
        $entity->setEntregauf('1');
        $entity->setEntregacodibge('1');
        $entity->setDtPedido('1');
        $entity->setDtEntrega('1');
        $entity->setDtEntregaFornecedor('1');
        $entity->setDtRemetido('1');
        $entity->setObservacao('1');
        $entity->setCondPagamento('1');
        $entity->setCondPagamento2('1');
        $entity->setIdTransportadora('1');
        $entity->setTpFrete('1');
        $entity->setQtEmbalagem('1');
        $entity->setVlFrete('1');
        $entity->setFormaPgto('1');
        $entity->setFormaPgto2('1');
        $entity->setQtParcelas('1');
        $entity->setQtParcelas2('1');
        $entity->setVlParcelas('1');
        $entity->setVlParcelas2('1');
        $entity->setVlTotal('1');
        $entity->setPerDesconto('1');
        $entity->setNumComprovante('1');
        $entity->setNumComprovante2('1');
        $entity->setImportou('1');
        $entity->setDtImportacao('1');
        $entity->setNumAutorizacao('1');
        $entity->setNumAutorizacao2('1');
        $entity->setOperadora('1');
        $entity->setOperadora2('1');
        $entity->setCodCupom('1');
        $entity->setVlSubtotal('1');
        $entity->setContaContabil1('1');
        $entity->setContaContabil2('1');
        $entity->setStatus('1');
        $entity->setCupomCredito('1');
        $entity->setVlCupom('1');
        $entity->setContaContabilNome1('1');
        $entity->setContaContabilNome2('1');
        $entity->setDataCaptura1('1');
        $entity->setDataCaptura2('1');
        $entity->setDtPedidoVenda('1');

        $this->assertNotNull($entity->getIdPedido());
        $this->assertNotNull($entity->getIdCliente());
        $this->assertNotNull($entity->getNomeCliente());
        $this->assertNotNull($entity->getCpfCliente());
        $this->assertNotNull($entity->getCnpjCliente());
        $this->assertNotNull($entity->getRgCliente());
        $this->assertNotNull($entity->getInscestCliente());
        $this->assertNotNull($entity->getDddCliente());
        $this->assertNotNull($entity->getFoneCliente());
        $this->assertNotNull($entity->getEmailCliente());
        $this->assertNotNull($entity->getNomeendereco());
        $this->assertNotNull($entity->getLogradouro());
        $this->assertNotNull($entity->getNomerua());
        $this->assertNotNull($entity->getNumero());
        $this->assertNotNull($entity->getComplemento());
        $this->assertNotNull($entity->getBairro());
        $this->assertNotNull($entity->getCep());
        $this->assertNotNull($entity->getCidade());
        $this->assertNotNull($entity->getEstado());
        $this->assertNotNull($entity->getEntreganomeendereco());
        $this->assertNotNull($entity->getEntregalogradouro());
        $this->assertNotNull($entity->getEntreganomerua());
        $this->assertNotNull($entity->getEntreganumero());
        $this->assertNotNull($entity->getEntregacomplemento());
        $this->assertNotNull($entity->getEntregabairro());
        $this->assertNotNull($entity->getEntregacep());
        $this->assertNotNull($entity->getEntregacidade());
        $this->assertNotNull($entity->getEntregauf());
        $this->assertNotNull($entity->getEntregacodibge());
        $this->assertNotNull($entity->getDtPedido());
        $this->assertNotNull($entity->getDtEntrega());
        $this->assertNotNull($entity->getDtEntregaFornecedor());
        $this->assertNotNull($entity->getDtRemetido());
        $this->assertNotNull($entity->getObservacao());
        $this->assertNotNull($entity->getCondPagamento());
        $this->assertNotNull($entity->getCondPagamento2());
        $this->assertNotNull($entity->getIdTransportadora());
        $this->assertNotNull($entity->getTpFrete());
        $this->assertNotNull($entity->getQtEmbalagem());
        $this->assertNotNull($entity->getVlFrete());
        $this->assertNotNull($entity->getFormaPgto());
        $this->assertNotNull($entity->getFormaPgto2());
        $this->assertNotNull($entity->getQtParcelas());
        $this->assertNotNull($entity->getQtParcelas2());
        $this->assertNotNull($entity->getVlParcelas());
        $this->assertNotNull($entity->getVlParcelas2());
        $this->assertNotNull($entity->getVlTotal());
        $this->assertNotNull($entity->getPerDesconto());
        $this->assertNotNull($entity->getNumComprovante());
        $this->assertNotNull($entity->getNumComprovante2());
        $this->assertNotNull($entity->getImportou());
        $this->assertNotNull($entity->getDtImportacao());
        $this->assertNotNull($entity->getNumAutorizacao());
        $this->assertNotNull($entity->getNumAutorizacao2());
        $this->assertNotNull($entity->getOperadora());
        $this->assertNotNull($entity->getOperadora2());
        $this->assertNotNull($entity->getCodCupom());
        $this->assertNotNull($entity->getVlSubtotal());
        $this->assertNotNull($entity->getContaContabil1());
        $this->assertNotNull($entity->getContaContabil2());
        $this->assertNotNull($entity->getStatus());
        $this->assertNotNull($entity->getCupomCredito());
        $this->assertNotNull($entity->getVlCupom());
        $this->assertNotNull($entity->getContaContabilNome1());
        $this->assertNotNull($entity->getContaContabilNome2());
        $this->assertNotNull($entity->getDataCaptura1());
        $this->assertNotNull($entity->getDataCaptura2());
        $this->assertNotNull($entity->getDtPedidoVenda());
    }
}
