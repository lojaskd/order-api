<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cProcedaFileLines;
        
/**
 * Class B2cProcedaFileLinesTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cProcedaFileLinesTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cProcedaFileLines();
                
        $entity->setCnpj('1');
        $entity->setInvoiceId('1');
        $entity->setInvoiceSerie('1');
        $entity->setCode('1');
        $entity->setDate(new DateTime());
        $entity->setCreated(new DateTime());
        $entity->setUpdated(new DateTime());
        $entity->setObsCode('1');
        $entity->setText('1');
        $entity->setProcedaFileId('1');
        $entity->setStatus('1');
        $entity->setIdPedped('1');
        $entity->setLog('1');
        $entity->setFile('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getCnpj());
        $this->assertNotNull($entity->getInvoiceId());
        $this->assertNotNull($entity->getInvoiceSerie());
        $this->assertNotNull($entity->getCode());
        $this->assertNotNull($entity->getDate());
        $this->assertNotNull($entity->getCreated());
        $this->assertNotNull($entity->getUpdated());
        $this->assertNotNull($entity->getObsCode());
        $this->assertNotNull($entity->getText());
        $this->assertNotNull($entity->getProcedaFileId());
        $this->assertNotNull($entity->getStatus());
        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getLog());
        $this->assertNotNull($entity->getFile());
    }
}
