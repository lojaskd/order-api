<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\B2cAtributoTipoValor;
use PHPUnit_Framework_TestCase;

/**
 * Class B2cAtributoTipoValorTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cAtributoTipoValorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $b2cAtributoTipoValor = new B2cAtributoTipoValor();
        $b2cAtributoTipoValor->setDescricao('test')->setIdAtributoTipoValor(1);

        $this->assertNotNull($b2cAtributoTipoValor->getDescricao());
        $this->assertNotNull($b2cAtributoTipoValor->getIdAtributoTipoValor());
    }
}
