<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdselos;
        
/**
 * Class B2cPrdselosTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdselosTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdselos();
                
        $entity->setIdPrdprd('1');
        $entity->setIdSelo('1');
        $entity->setAtivo('1');

        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getIdSelo());
        $this->assertNotNull($entity->isAtivo());
    }
}
