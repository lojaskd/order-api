<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\CidadesIbge;
        
/**
 * Class CidadesIbgeTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class CidadesIbgeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new CidadesIbge();
                
        $entity->setEstado('1');
        $entity->setCidade('1');
        $entity->setCep('1');
        $entity->setIdRegest('1');
        $entity->setCodIbge('1');
        $entity->setIdRegreg('1');
        $entity->setRegiao('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getEstado());
        $this->assertNotNull($entity->getCidade());
        $this->assertNotNull($entity->getCep());
        $this->assertNotNull($entity->getIdRegest());
        $this->assertNotNull($entity->getCodIbge());
        $this->assertNotNull($entity->getIdRegreg());
        $this->assertNotNull($entity->getRegiao());
    }
}
