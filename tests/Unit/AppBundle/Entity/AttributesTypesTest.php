<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\AttributesTypes;
use PHPUnit_Framework_TestCase;

/**
 * Class AttributesTypesTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class AttributesTypesTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $attributesTypes = new AttributesTypes();
        $attributesTypes->setId(1)->setName('Test')->setDescription('Description');

        $this->assertNotNull($attributesTypes->getId());
        $this->assertNotNull($attributesTypes->getName());
        $this->assertNotNull($attributesTypes->getDescription());
    }
}
