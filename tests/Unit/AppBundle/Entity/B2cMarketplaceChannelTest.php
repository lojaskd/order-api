<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cMarketplaceChannel;
        
/**
 * Class B2cMarketplaceChannelTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cMarketplaceChannelTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cMarketplaceChannel();
                
        $entity->setId('1');
        $entity->setMarketplaceId('1');
        $entity->setNome('1');
        $entity->setSlug('1');
        $entity->setDtaCad('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getMarketplaceId());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getSlug());
        $this->assertNotNull($entity->getDtaCad());
    }
}
