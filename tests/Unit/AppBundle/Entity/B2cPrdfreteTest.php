<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPrdfrete;
        
/**
 * Class B2cPrdfreteTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPrdfreteTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPrdfrete();
                
        $entity->setIdPrdprd('1');
        $entity->setIdRegreg('1');
        $entity->setValor('1');

        $this->assertNotNull($entity->getIdPrdprd());
        $this->assertNotNull($entity->getIdRegreg());
        $this->assertNotNull($entity->getValor());
    }
}
