<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cSelos;
        
/**
 * Class B2cSelosTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cSelosTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cSelos();
                
        $entity->setId('1');
        $entity->setNome('1');
        $entity->setTexto('1');
        $entity->setPrioridade('1');
        $entity->setTooltip('1');
        $entity->setAtivo('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getTexto());
        $this->assertNotNull($entity->getPrioridade());
        $this->assertNotNull($entity->getTooltip());
        $this->assertNotNull($entity->isAtivo());
    }
}
