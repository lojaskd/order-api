<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cContaContabilPagamento;
        
/**
 * Class B2cContaContabilPagamentoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cContaContabilPagamentoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cContaContabilPagamento();
                
        $entity->setId('1');
        $entity->setIdConta('1');

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getIdConta());
    }
}
