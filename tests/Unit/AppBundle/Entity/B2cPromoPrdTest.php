<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoPrd;
        
/**
 * Class B2cPromoPrdTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoPrdTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoPrd();
                
        $entity->setIdPromo('1');
        $entity->setIdProduto('1');
        $entity->setAplicar('1');

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getIdProduto());
        $this->assertNotNull($entity->getAplicar());
    }
}
