<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\B2cAtributo;
use PHPUnit_Framework_TestCase;

/**
 * Class B2cAtributoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cAtributoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $b2cAtributo = new B2cAtributo();
        $b2cAtributo->setIdAtributo(1)
            ->setNomeAtributo('Test')
            ->setTipoCampo('test')
            ->setBuscavel(true)
            ->setAtributoGrupo(1)
            ->setFiltravel(1)
            ->setExpedicao(1)
            ->setSlug('test')
            ->setIdAtributoTipoValor(1)
            ->setNomeAtributoOtimizado('test')
            ->setOrdem(1)
            ->setVisivel(1)
            ->setOrdemAdm(1);

        $this->assertNotNull($b2cAtributo->getNomeAtributo());
        $this->assertNotNull($b2cAtributo->getIdAtributo());
        $this->assertNotNull($b2cAtributo->getTipoCampo());
        $this->assertNotNull($b2cAtributo->getBuscavel());
        $this->assertNotNull($b2cAtributo->getAtributoGrupo());
        $this->assertNotNull($b2cAtributo->getFiltravel());
        $this->assertNotNull($b2cAtributo->getExpedicao());
        $this->assertNotNull($b2cAtributo->getSlug());
        $this->assertNotNull($b2cAtributo->getIdAtributoTipoValor());
        $this->assertNotNull($b2cAtributo->getNomeAtributoOtimizado());
        $this->assertNotNull($b2cAtributo->getOrdem());
        $this->assertNotNull($b2cAtributo->getVisivel());
        $this->assertNotNull($b2cAtributo->getOrdemAdm());
    }
}
