<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cViewPrecos;
        
/**
 * Class B2cViewPrecosTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cViewPrecosTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cViewPrecos();
                
        $entity->setIdKit('1');
        $entity->setIdProduto('1');
        $entity->setQuantidade('1');
        $entity->setPrecoReal('1');
        $entity->setPrecoPromocao('1');
        $entity->setPrecoAvista('1');
        $entity->setPrecoParcelado('1');
        $entity->setQtdeParcelas('1');
        $entity->setDataAtualizacao('1');
        $entity->setDescontoAvista('1');
        $entity->setPrecoEspecialPromocao('1');
        $entity->setPrecoEspecialAvista('1');
        $entity->setPrecoEspecialKit('1');
        $entity->setQtdeParcelasJuros('1');
        $entity->setTaxaParcelasJuros('1');

        $this->assertNotNull($entity->getIdKit());
        $this->assertNotNull($entity->getIdProduto());
        $this->assertNotNull($entity->getQuantidade());
        $this->assertNotNull($entity->getPrecoReal());
        $this->assertNotNull($entity->getPrecoPromocao());
        $this->assertNotNull($entity->getPrecoAvista());
        $this->assertNotNull($entity->getPrecoParcelado());
        $this->assertNotNull($entity->getQtdeParcelas());
        $this->assertNotNull($entity->getDataAtualizacao());
        $this->assertNotNull($entity->getDescontoAvista());
        $this->assertNotNull($entity->getPrecoEspecialPromocao());
        $this->assertNotNull($entity->getPrecoEspecialAvista());
        $this->assertNotNull($entity->getPrecoEspecialKit());
        $this->assertNotNull($entity->getQtdeParcelasJuros());
        $this->assertNotNull($entity->getTaxaParcelasJuros());
    }
}
