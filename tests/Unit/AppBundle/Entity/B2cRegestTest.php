<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cRegest;
        
/**
 * Class B2cRegestTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cRegestTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cRegest();
                
        $entity->setNome('1');
        $entity->setUf('1');
        $entity->setCepInicio('1');
        $entity->setCepFim('1');
        $entity->setCodMunCapital('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getUf());
        $this->assertNotNull($entity->getCepInicio());
        $this->assertNotNull($entity->getCepFim());
        $this->assertNotNull($entity->getCodMunCapital());
    }
}
