<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cNotafiscalItem;
        
/**
 * Class B2cNotafiscalItemTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cNotafiscalItemTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cNotafiscalItem();
                
        $entity->setIdFilial('1');
        $entity->setIdNotafiscal('1');
        $entity->setIdPedped('1');
        $entity->setPrdReferencia('1');
        $entity->setPrdQtde('1');
        $entity->setDataCadastro('1');
        $entity->setDataDevolucao('1');
        $entity->setDevolvido('1');
        $entity->setNomeTransportadora('1');
        $entity->setDocumentoTransportadora('1');
        $entity->setHub('1');

        $this->assertNotNull($entity->getIdFilial());
        $this->assertNotNull($entity->getIdNotafiscal());
        $this->assertNotNull($entity->getIdPedped());
        $this->assertNotNull($entity->getPrdReferencia());
        $this->assertNotNull($entity->getPrdQtde());
        $this->assertNotNull($entity->getDataCadastro());
        $this->assertNotNull($entity->getDataDevolucao());
        $this->assertNotNull($entity->getDevolvido());
        $this->assertNotNull($entity->getNomeTransportadora());
        $this->assertNotNull($entity->getDocumentoTransportadora());
        $this->assertNotNull($entity->getHub());
    }
}
