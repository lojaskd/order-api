<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoVale;
        
/**
 * Class B2cPromoValeTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoValeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoVale();
                
        $entity->setIdPromo('1');
        $entity->setCodigo('1');
        $entity->setSituacao('1');
        $entity->setValeUtilizado('1');
        $entity->setIsColaborador('1');
        $entity->setTipo('1');
        $entity->setIdVale(1);

        $this->assertNotNull($entity->getIdVale());
        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getCodigo());
        $this->assertNotNull($entity->getSituacao());
        $this->assertNotNull($entity->getValeUtilizado());
        $this->assertNotNull($entity->getIsColaborador());
        $this->assertNotNull($entity->getTipo());
    }
}
