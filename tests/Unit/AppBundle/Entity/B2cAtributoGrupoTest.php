<?php

namespace Tests\Unit\AppBundle\Entity;

use AppBundle\Entity\B2cAtributoGrupo;
use PHPUnit_Framework_TestCase;

/**
 * Class B2cAtributoGrupoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cAtributoGrupoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $b2cAtributoGrupo = new B2cAtributoGrupo();
        $b2cAtributoGrupo->setNome('Test')->setIdAtributoGrupo(1);

        $this->assertNotNull($b2cAtributoGrupo->getNome());
        $this->assertNotNull($b2cAtributoGrupo->getIdAtributoGrupo());
    }
}
