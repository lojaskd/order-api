<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use DateTime;
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cProcedaFiles;
        
/**
 * Class B2cProcedaFilesTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cProcedaFilesTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cProcedaFiles();
                
        $entity->setFilename('1');
        $entity->setCreated(new DateTime());
        $entity->setUpdated(new DateTime());
        $entity->setStatus('1');
        $entity->setSender('1');
        $entity->setReceiver('1');
        $entity->setExchange('1');
        $entity->setLog('1');
        $entity->setOrigin('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getFilename());
        $this->assertNotNull($entity->getCreated());
        $this->assertNotNull($entity->getUpdated());
        $this->assertNotNull($entity->getStatus());
        $this->assertNotNull($entity->getSender());
        $this->assertNotNull($entity->getReceiver());
        $this->assertNotNull($entity->getExchange());
        $this->assertNotNull($entity->getLog());
        $this->assertNotNull($entity->getOrigin());
    }
}
