<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cPromoFor;
        
/**
 * Class B2cPromoForTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cPromoForTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cPromoFor();
                
        $entity->setIdPromo('1');
        $entity->setIdFornecedor('1');

        $this->assertNotNull($entity->getIdPromo());
        $this->assertNotNull($entity->getIdFornecedor());
    }
}
