<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cStaint;
        
/**
 * Class B2cStaintTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cStaintTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cStaint();
                
        $entity->setNome('1');
        $entity->setSituacao('1');
        $entity->setMostrar('1');
        $entity->setId(1);

        $this->assertNotNull($entity->getId());
        $this->assertNotNull($entity->getNome());
        $this->assertNotNull($entity->getSituacao());
        $this->assertNotNull($entity->getMostrar());
    }
}
