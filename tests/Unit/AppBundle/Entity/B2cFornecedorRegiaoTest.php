<?php 
      
namespace Tests\Unit\AppBundle\Entity;
        
use PHPUnit_Framework_TestCase;
use AppBundle\Entity\B2cFornecedorRegiao;
        
/**
 * Class B2cFornecedorRegiaoTest
 * @package Tests\Unit\AppBundle\Entity
 *
 * @group Unit
 * @group Entity
 */
class B2cFornecedorRegiaoTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        $entity = new B2cFornecedorRegiao();
                
        $entity->setIdForfor('1');
        $entity->setIdRegreg('1');
        $entity->setIdFilial('1');
        $entity->setPrazoColeta('1');
        $entity->setPrazoRecebimento('1');
        $entity->setPrazoSeparacao('1');
        $entity->setPrazoRomaneio('1');
        $entity->setPrioridade('1');
        $entity->setCodigoTransportadorColeta('1');
        $entity->setDestination('1');

        $this->assertNotNull($entity->getIdForfor());
        $this->assertNotNull($entity->getIdRegreg());
        $this->assertNotNull($entity->getIdFilial());
        $this->assertNotNull($entity->getPrazoColeta());
        $this->assertNotNull($entity->getPrazoRecebimento());
        $this->assertNotNull($entity->getPrazoSeparacao());
        $this->assertNotNull($entity->getPrazoRomaneio());
        $this->assertNotNull($entity->getPrioridade());
        $this->assertNotNull($entity->getCodigoTransportadorColeta());
        $this->assertNotNull($entity->getDestination());
    }
}
