<?php

namespace Tests\Unit\AppBundle\Controller;

use AppBundle\Entity\B2cNotafiscal;
use AppBundle\Entity\B2cPedped;
use AppBundle\Entity\B2cPedsituacao;
use AppBundle\Entity\B2cProcedaFileLines;
use AppBundle\Entity\B2cStaint;
use AppBundle\Entity\B2cViewProdutos;
use AppBundle\Entity\VO\Create\CreateInvoiceVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateInvoiceDateVO;
use AppBundle\Entity\VO\Update\UpdateInvoiceVO;
use AppBundle\Service\InvoiceService;
use AppBundle\Service\OrderService;
use Doctrine\ORM\EntityManager;
use Mockery;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class OrderInvoiceControllerTest
 * @package Tests\Unit\AppBundle\Controller
 *
 * @group Unit
 * @group Controller
 */
class OrderInvoiceControllerTest extends ControllerTestCase
{
    /**
     * @test
     */
    public function getOrderInvoiceAction()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('findBy')->andReturn([new B2cNotafiscal()]);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/1/invoices');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderInvoiceTrackingAction()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('findBy')->andReturn([new B2cProcedaFileLines()]);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/1/invoices/1/tracking');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderInvoiceDatesAction()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('getMetadataFactory')->andReturn($entityManager);
        $entityManager->shouldReceive('isTransient')->andReturn($entityManager);
        $entityManager->shouldReceive('findBy')->andReturn([new B2cProcedaFileLines()]);
        $entityManager->shouldReceive('findOneBy')->andReturn(
            new B2cPedped(),
            new B2cPedsituacao(),
            new B2cStaint(),
            new B2cViewProdutos()
        );

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());

        $this->client->getContainer()->set('order_service', $orderService);

        $invoiceService = Mockery::mock(InvoiceService::class);
        $invoiceService->shouldReceive('updateDate')->andReturn($invoiceService);
        $invoiceService->shouldReceive('getCommonInfoInvoiceDate')->andReturn(new UpdateInvoiceDateVO());

        $this->client->getContainer()->set('invoice_service', $invoiceService);

        $this->overrideEntityManager($entityManager);

        $data = [
            'order_id' => 1,
            'invoice_id' => 1,
            'type' => 'shippingDate',
            'date' => date('Y-m-d'),
            'clerk' => 'site'
        ];

        $this->client->request('PUT', '/v1/orders/1/invoices/1/dates', [], [], [], json_encode($data));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderInvoiceDatesActionError()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('getMetadataFactory')->andReturn($entityManager);
        $entityManager->shouldReceive('isTransient')->andReturn($entityManager);
        $entityManager->shouldReceive('findBy')->andReturn([new B2cProcedaFileLines()]);
        $entityManager->shouldReceive('findOneBy')->andReturn(
            new B2cPedped(),
            new B2cPedsituacao(),
            new B2cStaint(),
            new B2cViewProdutos()
        );

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());

        $this->client->getContainer()->set('order_service', $orderService);

        $invoiceService = Mockery::mock(InvoiceService::class);
        $invoiceService->shouldReceive('updateDate')->andReturn($invoiceService);
        $invoiceService->shouldReceive('getCommonInfoInvoiceDate')->andReturn(new UpdateInvoiceDateVO());

        $this->client->getContainer()->set('invoice_service', $invoiceService);

        $this->overrideEntityManager($entityManager);

        $data = [
            'order_id' => 1,
            'invoice_id' => 1,
            'type' => '1',
            'date' => date('Y-m-d'),
            'clerk' => 'site'
        ];

        $this->client->request('PUT', '/v1/orders/1/invoices/1/dates', [], [], [], json_encode($data));
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function postInvoiceAction()
    {
        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());

        $invoiceService = Mockery::mock(InvoiceService::class);
        $invoiceService->shouldReceive('getCommonInvoiceInfo')->andReturn(new CreateInvoiceVO());
        $invoiceService->shouldReceive('createInvoice')->andReturn($invoiceService);

        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);

        $this->client->getContainer()->set('order_service', $orderService);
        $this->client->getContainer()->set('invoice_service', $invoiceService);
        $this->client->getContainer()->set('validator', $validator);

        $this->client->request('POST', '/v1/orders/1/invoices', [], [], [], json_encode([]));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function postInvoiceActionError()
    {
        $this->client->request('POST', '/v1/orders/1/invoices', [], [], [], json_encode([]));
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderInvoiceAction()
    {
        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $invoiceService = Mockery::mock(InvoiceService::class);
        $invoiceService->shouldReceive('getCommonUpdateInvoiceInfo')->andReturn(new UpdateInvoiceVO());
        $invoiceService->shouldReceive('updateInvoice')->andReturn($invoiceService);
        $this->client->getContainer()->set('invoice_service', $invoiceService);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $this->client->getContainer()->set('order_service', $orderService);

        $this->client->request('PUT', '/v1/orders/1/invoices/1', [], [], [], json_encode([]));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderInvoiceActionError()
    {
        $error = Mockery::mock(ValidatorInterface::class);
        $error->shouldReceive('getMessage')->andReturn('test');

        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn([$error]);
        $this->client->getContainer()->set('validator', $validator);

        $invoiceService = Mockery::mock(InvoiceService::class);
        $invoiceService->shouldReceive('getCommonUpdateInvoiceInfo')->andReturn(new UpdateInvoiceVO());
        $this->client->getContainer()->set('invoice_service', $invoiceService);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $this->client->getContainer()->set('order_service', $orderService);

        $this->client->request('PUT', '/v1/orders/1/invoices/1', [], [], [], json_encode([]));
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }
}
