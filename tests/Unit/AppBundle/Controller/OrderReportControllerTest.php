<?php

namespace Tests\Unit\AppBundle\Controller;
use Doctrine\ORM\EntityManager;
use Mockery;

/**
 * Class OrderReportControllerTest
 * @package Tests\Unit\AppBundle\Controller
 *
 * @group Unit
 * @group Controller
 */
class OrderReportControllerTest extends ControllerTestCase
{
    /**
     * @test
     */
    public function getOrdersReportAction()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('getRevenueReportByCustomer')->andReturn([]);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/report?type=revenue&customer_id=1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrdersReportActionException()
    {
        $entityManager = Mockery::mock(EntityManager::class);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/report?type=revenue&customer_id=1');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
    }
}
