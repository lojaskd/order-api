<?php

namespace Tests\Unit\AppBundle\Controller;

use AppBundle\Entity\VO\Create\CreateOrderVO;
use AppBundle\Entity\VO\Create\CreatePurchaseOrderVO;
use AppBundle\Entity\VO\Create\CreateShippingVO;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateOrderStatusVO;
use AppBundle\Repository\B2cPedpedRepository;
use AppBundle\Service\OrderService;
use AppBundle\Service\ShippingService;
use Doctrine\ORM\EntityManager;
use Mockery;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class OrderControllerTest
 * @package Tests\Unit\AppBundle\Controller
 *
 * @group Unit
 * @group Controller
 */
class OrderControllerTest extends ControllerTestCase
{
    /**
     * @test
     */
    public function getPedidoAction()
    {
        $order = [
            'products' => [
                [
                    'idPrdprd' => 1,
                    'prdReferencia' => 1,
                    'prdFornecedor' => 1
                ]
            ],
            'tipoPedido' => 'marketplace'
        ];

        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('getOrder')->andReturn($order);
        $entityManager->shouldReceive('getProductOrderStatus')->andReturn([]);
        $entityManager->shouldReceive('getStatus')->andReturn([]);
        $entityManager->shouldReceive('getProduction')->andReturn([]);
        $entityManager->shouldReceive('getInvoices')->andReturn([]);
        $entityManager->shouldReceive('getShipper')->andReturn([]);
        $entityManager->shouldReceive('getPolo')->andReturn([]);
        $entityManager->shouldReceive('getBranchOfficeInformation')->andReturn([]);
        $entityManager->shouldReceive('formatProductInfo')->andReturn([]);
        $entityManager->shouldReceive('getMarketPlaceOrder')->andReturn([]);
        $entityManager->shouldReceive('getSituation')->andReturn(['test', 'internalStatusName']);
        $entityManager->shouldReceive('formatOrderInfo')->andReturn([]);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/pedido/1');
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function getPedidoActionError()
    {
        $entityManager = Mockery::mock(EntityManager::class);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/pedido/1');
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderAction()
    {
        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getOrderInfo')->andReturn([]);

        $this->client->getContainer()->set('order_service', $orderService);
        $this->client->request('GET', '/v1/orders/1');
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderActionError()
    {
        $this->client->request('GET', '/v1/orders/1');
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrdersActionNotFound()
    {
        $this->client->request('GET', '/v1/orders');
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function postOrderAction()
    {
        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonCreateOrderInfo')->andReturn(new CreateOrderVO());
        $orderService->shouldReceive('generateOrder')->andReturn([
            'errors' => [],
            'orderVO' => new CreateOrderVO()
        ]);
        $orderService->shouldReceive('insertOrder')->andReturn($orderService);
        $this->client->getContainer()->set('order_service', $orderService);

        $this->client->request('POST', '/v1/orders', [], [], [], json_encode(['teste' => 'teste']));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function postOrderActionError()
    {
        $this->client->request('POST', '/v1/orders', [], [], [], json_encode(['teste' => 'teste']));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderStatusAction()
    {
        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $createShippingVO = new CreateShippingVO();
        $createShippingVO->setId(1);

        $shippingService = Mockery::mock(ShippingService::class);
        $shippingService->shouldReceive('calculateShipping')->andReturn($createShippingVO);
        $this->client->getContainer()->set('shipping_service', $shippingService);

        $orderVO = new OrderVO();
        $orderVO->setShipping(new CreateShippingVO());
        $orderVO->setMarketplaceId(1);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn($orderVO);
        $orderService->shouldReceive('getCommonOrderStatusInfo')->andReturn(new UpdateOrderStatusVO());
        $orderService->shouldReceive('generateOrder')->andReturn([
            'errors' => [],
            'orderVO' => new CreateOrderVO()
        ]);
        $orderService->shouldReceive('updateStatus')->andReturn($orderService);
        $this->client->getContainer()->set('order_service', $orderService);

        $orderRepository = Mockery::mock(B2cPedpedRepository::class);
        $orderRepository->shouldReceive('updateOrderShipping')->andReturn(true);
        $this->overrideEntityManager($orderRepository);

        $this->client->request('PUT', '/v1/orders/1/status', [], [], [], json_encode(['teste' => 'teste']));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderStatusActionError()
    {
        $this->client->request('PUT', '/v1/orders/1/status', [], [], [], json_encode(['teste' => 'teste']));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function postPurchaseOrderActionError()
    {
        $this->client->request('POST', '/v1/orders/1/purchaseOrder', [], [], [], json_encode(['teste' => 'teste']));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function postPurchaseOrderAction()
    {
        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $createShippingVO = new CreateShippingVO();
        $createShippingVO->setId(1);

        $shippingService = Mockery::mock(ShippingService::class);
        $shippingService->shouldReceive('calculateShipping')->andReturn($createShippingVO);
        $this->client->getContainer()->set('shipping_service', $shippingService);

        $orderVO = new OrderVO();
        $orderVO->setShipping(new CreateShippingVO());
        $orderVO->setMarketplaceId(1);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonPurchaseOrderInfo')->andReturn(new CreatePurchaseOrderVO());
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn($orderVO);
        $orderService->shouldReceive('createPurchaseOrder')->andReturn([
            'errors' => [],
            'orderVO' => new CreateOrderVO()
        ]);
        $orderService->shouldReceive('updateStatus')->andReturn($orderService);
        $this->client->getContainer()->set('order_service', $orderService);

        $this->client->request('POST', '/v1/orders/1/purchaseOrder', [], [], [], json_encode(['teste' => 'teste']));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putNewDeliveryDateActionError()
    {
        $this->client->request('PUT', '/v1/orders/1/dates/delivery', [], [], [], json_encode(['teste' => 'teste']));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putNewDeliveryDateAction()
    {
        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $createShippingVO = new CreateShippingVO();
        $createShippingVO->setId(1);

        $shippingService = Mockery::mock(ShippingService::class);
        $shippingService->shouldReceive('calculateShipping')->andReturn($createShippingVO);
        $this->client->getContainer()->set('shipping_service', $shippingService);

        $orderVO = new OrderVO();
        $orderVO->setShipping(new CreateShippingVO());
        $orderVO->setMarketplaceId(1);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('createPurchaseOrder')->andReturn([
            'errors' => [],
            'orderVO' => new CreateOrderVO()
        ]);
        $orderService->shouldReceive('updateNewDeliveryDate')->andReturn($orderService);
        $this->client->getContainer()->set('order_service', $orderService);

        $this->client->request('PUT', '/v1/orders/1/dates/delivery', [], [], [], json_encode(['teste' => 'teste']));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }
}
