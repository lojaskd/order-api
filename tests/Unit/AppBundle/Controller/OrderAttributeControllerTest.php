<?php

namespace Tests\Unit\AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Mockery;

/**
 * Class OrderAttributeControllerTest
 * @package Tests\Unit\AppBundle\Controller
 *
 * @group Unit
 * @group Controller
 */
class OrderAttributeControllerTest extends ControllerTestCase
{
    /**
     * @test
     */
    public function getOrderAttributesActionGet()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('getAttributes')->andReturn([[
            'type_name' => 'Choice',
            'id_attr' => 1
        ]]);
        $entityManager->shouldReceive('getAttributeOptions')->andReturn([]);
        $entityManager->shouldReceive('getOrderAttributes')->andReturn([]);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/1/attributes');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderAttributesActionGetException()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('getRepository')->andReturn($entityManager);
        $entityManager->shouldReceive('clear')->andReturn($entityManager);
        $entityManager->shouldReceive('getAttributes')->andReturn([[
            'type_name' => 'Choice',
            'id_attr' => 1
        ]]);
        $entityManager->shouldReceive('getAttributeOptions')->andReturn([]);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/1/attributes');
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderAttributesActionGetNotFoundAttribute()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('getRepository')->andReturn($entityManager);
        $entityManager->shouldReceive('clear')->andReturn($entityManager);
        $entityManager->shouldReceive('getAttributes')->andReturn(null);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/1/attributes');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderAttributesActionPut()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('getRepository')->andReturn($entityManager);
        $entityManager->shouldReceive('getMetadataFactory')->andReturn($entityManager);
        $entityManager->shouldReceive('isTransient')->andReturn($entityManager);
        $entityManager->shouldReceive('clear')->andReturn($entityManager);
        $entityManager->shouldReceive('getAttributes')->andReturn([[
            'type_name' => 'Choice',
            'id_attr' => 1
        ]]);
        $entityManager->shouldReceive('getAttributeOptions')->andReturn([]);
        $entityManager->shouldReceive('getOrderAttributes')->andReturn([]);

        $this->overrideEntityManager($entityManager);

        $data = [
            'orderId' => 1
        ];

        $this->client->request('PUT', '/v1/orders/1/attributes', [], [], [], json_encode([$data]));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}
