<?php

namespace Tests\Unit\AppBundle\Controller;

/**
 * Class HomeControllerTest
 * @package Tests\Unit\AppBundle\Controller
 *
 * @group Unit
 * @group Controller
 */
class HomeControllerTest extends ControllerTestCase
{
    /**
     * @test
     */
    public function indexAction()
    {
        $this->client->request('GET', '/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }


    /**
     * @test
     */
    public function resetCacheAction()
    {
        $this->client->request('GET', '/reset_cache');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}
