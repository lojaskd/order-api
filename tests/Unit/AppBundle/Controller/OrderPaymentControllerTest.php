<?php

namespace Tests\Unit\AppBundle\Controller;

use AppBundle\Service\OrderPaymentService;

/**
 * Class OrderPaymentControllerTest
 * @package Tests\Functional\AppBundle\Controller
 *
 * @group Unit
 * @group Controller
 */
class OrderPaymentControllerTest extends ControllerTestCase
{
    /**
     * @test
     */
    public function getOrdersReportAction()
    {
        $orderPaymentService = \Mockery::mock(OrderPaymentService::class);
        $orderPaymentService->shouldReceive('getOrderPayment')->andReturn([]);
        $this->client->getContainer()->set('order_payment_service', $orderPaymentService);

        $this->client->request('GET', '/v1/orders/1/payments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}
