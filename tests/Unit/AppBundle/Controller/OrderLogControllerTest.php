<?php

namespace Tests\Unit\AppBundle\Controller;

use AppBundle\Entity\B2cPedlog;
use AppBundle\Service\OrderLogService;
use Doctrine\ORM\EntityManager;
use Mockery;

/**
 * Class OrderLogControllerTest
 * @package Tests\Functional\AppBundle\Controller
 *
 * @group Unit
 * @group Controller
 */
class OrderLogControllerTest extends ControllerTestCase
{
    /**
     * @test
     */
    public function getOrdersReportActionGetError()
    {
        $entityManager = Mockery::mock(EntityManager::class);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/1/logs');
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrdersReportActionGet()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('findBy')->andReturn([new B2cPedlog()]);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/1/logs');
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function postOrdersReportActionPost()
    {
        $data = [
            'title' => 'test',
            'text' => 'text',
            'show' => 1,
            'status' => 1,
            'internal_status' => 1
        ];

        $orderLogService = Mockery::mock(OrderLogService::class);
        $orderLogService->shouldReceive('insertOrderLog')->andReturn(true);
        $this->client->getContainer()->set('order_log_service', $orderLogService);

        $this->client->request('POST', '/v1/orders/1/logs',
            [],
            [],
            [],
            json_encode($data)
        );
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('true', $response->getContent());
    }

    /**
     * @test
     */
    public function postOrdersReportActionPostWithErrors()
    {
        $data = [
            'title' => 'test',
            'text' => 'text',
            'show' => 1,
            'status' => 1
        ];

        $this->client->request('POST', '/v1/orders/1/logs',
            [],
            [],
            [],
            json_encode($data)
        );
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }
}
