<?php

namespace Tests\Unit\AppBundle\Controller;

use AppBundle\Entity\B2cPrdprd;
use AppBundle\Entity\Validate\Update\UpdateValidateProductStatus;
use AppBundle\Entity\VO\OrderVO;
use AppBundle\Entity\VO\Update\UpdateProductsDateVO;
use AppBundle\Entity\VO\Update\UpdateProductsReversalInfoVO;
use AppBundle\Entity\VO\Update\UpdateProductsStatusVO;
use AppBundle\Entity\VO\Update\UpdateProviderBillingDateVO;
use AppBundle\Service\OrderService;
use AppBundle\Service\ProductService;
use AppBundle\Service\PurchaseOrderService;
use Doctrine\ORM\EntityManager;
use Mockery;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class OrderProductControllerTest
 * @package Tests\Unit\AppBundle\Controller
 *
 * @group Unit
 * @group Controller
 */
class OrderProductControllerTest extends ControllerTestCase
{
    /**
     * @test
     */
    public function getOrderProductsAction()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('findBy')->andReturn([new B2cPrdprd()]);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/1/products');
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderProductsActionError()
    {
        $entityManager = Mockery::mock(EntityManager::class);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/1/products');
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderProductsActionNotFound()
    {
        $entityManager = Mockery::mock(EntityManager::class);
        $entityManager->shouldReceive('findBy')->andReturn(null);

        $this->overrideEntityManager($entityManager);

        $this->client->request('GET', '/v1/orders/1/products');
        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderProductAction()
    {
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('getProductOrderInfo')->andReturn(['test' => 'test']);

        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('GET', '/v1/orders/1/products/1');
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderProductActionEmpty()
    {
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('getProductOrderInfo')->andReturn([]);

        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('GET', '/v1/orders/1/products/1');
        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function getOrderProductActionError()
    {
        $productService = Mockery::mock(ProductService::class);
        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('GET', '/v1/orders/1/products/1');
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderProductsDatesAction()
    {
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('updateDate')->andReturn($productService);
        $productService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $productService->shouldReceive('getCommonInfoProductDate')->andReturn(new UpdateProductsDateVO());
        $this->client->getContainer()->set('product_service', $productService);

        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $orderService->shouldReceive('getCommonInfoProductDate')->andReturn(new UpdateProductsDateVO());
        $this->client->getContainer()->set('order_service', $orderService);

        $this->client->request('PUT', '/v1/orders/1/products/dates', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderProductsDatesActionNotFound()
    {
        $productService = Mockery::mock(ProductService::class);
        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('PUT', '/v1/orders/1/products/dates', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function deleteOrderProductsDatesActionAction()
    {
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('deleteDate')->andReturn($productService);
        $productService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $productService->shouldReceive('getCommonInfoRemoveDate')->andReturn(new UpdateProductsDateVO());
        $this->client->getContainer()->set('product_service', $productService);

        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $orderService->shouldReceive('getCommonInfoRemoveDate')->andReturn(new UpdateProductsDateVO());
        $this->client->getContainer()->set('order_service', $orderService);

        $this->client->request('DELETE', '/v1/orders/1/products/dates', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function deleteOrderProductsDatesActionNotFound()
    {
        $productService = Mockery::mock(ProductService::class);
        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('DELETE', '/v1/orders/1/products/dates', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderProductDatesAction()
    {
        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('getCommonInfoProductDate')->andReturn(new UpdateProductsDateVO());
        $productService->shouldReceive('updateDate')->andReturn($productService);
        $this->client->getContainer()->set('product_service', $productService);

        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $this->client->getContainer()->set('order_service', $orderService);

        $this->client->request('PUT', '/v1/orders/1/products/1/dates', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderProductDatesActionError()
    {
        $productService = Mockery::mock(ProductService::class);
        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('PUT', '/v1/orders/1/products/1/dates', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderProductStatusActionErrorOne()
    {
        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $this->client->getContainer()->set('order_service', $orderService);

        $this->createMock(ProductService::class)->method('updateProductStatus')->willThrowException(new \Exception);

        $this->client->request('PUT', '/v1/orders/1/products/1/status', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderProductStatusAction()
    {
        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $this->client->getContainer()->set('order_service', $orderService);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('getCommonInfoProductStatus')->andReturn(new UpdateProductsStatusVO());
        $productService->shouldReceive('updateProductStatus')->andReturn(null);
        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('PUT', '/v1/orders/1/products/1/status', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putOrderProductStatusActionErrorTwo()
    {
        $productService = Mockery::mock(ProductService::class);
        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('PUT', '/v1/orders/1/products/1/status', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putApproveReversalAction()
    {
        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $this->client->getContainer()->set('order_service', $orderService);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('approveReversal')->andReturn($productService);
        $productService->shouldReceive('getCommonInfoProductReversal')->andReturn(new UpdateProductsReversalInfoVO());
        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('PUT', '/v1/orders/1/products/1/reversal', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putApproveReversalActionError()
    {
        $productService = Mockery::mock(ProductService::class);
        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('PUT', '/v1/orders/1/products/1/reversal', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putProviderBillingDateActionError()
    {
        $productService = Mockery::mock(ProductService::class);
        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('PUT', '/v1/orders/1/purchase_order/1/dates', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function putProviderBillingDateAction()
    {
        $validator = Mockery::mock(ValidatorInterface::class);
        $validator->shouldReceive('validate')->andReturn(null);
        $this->client->getContainer()->set('validator', $validator);

        $purchaseOrderService = Mockery::mock(PurchaseOrderService::class);
        $purchaseOrderService->shouldReceive('getPurchaseOrderDateInfo')->andReturn(new UpdateProviderBillingDateVO());
        $purchaseOrderService->shouldReceive('updatePurchaseOrderDate')->andReturn($purchaseOrderService);
        $this->client->getContainer()->set('purchase_order_service', $purchaseOrderService);

        $orderService = Mockery::mock(OrderService::class);
        $orderService->shouldReceive('getCommonOrderInfo')->andReturn(new OrderVO());
        $this->client->getContainer()->set('order_service', $orderService);

        $productService = Mockery::mock(ProductService::class);
        $productService->shouldReceive('approveReversal')->andReturn($productService);
        $productService->shouldReceive('getCommonInfoProductReversal')->andReturn(new UpdateProductsReversalInfoVO());
        $this->client->getContainer()->set('product_service', $productService);

        $this->client->request('PUT', '/v1/orders/1/purchase_order/1/dates', [], [], [], json_encode([]));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }
}
