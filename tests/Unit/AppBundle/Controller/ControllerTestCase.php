<?php

namespace Tests\Unit\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\Unit\AppBundle\Service\LoggerTestTrait;

/**
 * Class ControllerTestCase
 * @package Tests\Unit\AppBundle\Controller
 */
class ControllerTestCase extends WebTestCase
{
    use LoggerTestTrait;

    /**
     * @var Client $client
     */
    protected $client = null;

    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->client->getContainer()->set('logger', $this->getLogger());
    }

    protected function tearDown()
    {
        $this->client = null;
        parent::tearDown();
    }

    protected function overrideEntityManager($entityManager)
    {
        $entityManager->shouldReceive('clear')->andReturn($entityManager);
        $entityManager->shouldReceive('getRepository')->andReturn($entityManager);

        $this->client->getContainer()->set('doctrine.orm.default_entity_manager', $entityManager);
    }
}
