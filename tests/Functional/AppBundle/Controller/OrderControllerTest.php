<?php

namespace Tests\Functional\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OrderControllerTest
 * @package Tests\Functional\AppBundle\Controller
 *
 * @group Functional
 * @group Controller
 */
class OrderControllerTest extends WebTestCase
{

    public function testCreateOrder()
    {

        $_SERVER["REMOTE_ADDR"] = "127.0.0.1";
        $client = static::createClient();
        $data = array(
            "clerk" => "Site",
            "customer" => array(
                "id" => "133811",
                "email" => "ti@lojaskd.com.br",
                "cpf" => "333.822.147-92",
                "first_name" => "TI - Teste",
                "last_name" => "e Nome dos Grandes",
                "accepts_marketing" => "1",
                "birthday" => "1980-01-01",
                "rg_ie" => "12345678988",
                "person_type" => "F",
                "gender" => "M",
                "ip" => "192.168.5.1"
            ),
            "products" => array(
                array(
                    "id" => "64330",
                    "kit_id" => 0,
                    "name" => "Banqueta BA 17 Alta Cromado/Preto - Unimóvel",
                    "quantity" => 1,
                    "price" => 163.84
                ),
                array(
                    "id" => "110457",
                    "kit_id" => 0,
                    "name" => "Balcão para Pia 2 Portas e 
                    3 Gavetas Sem Tampo VT-1201 Fruits Avelã/Maracujá - Urbe Móveis",
                    "quantity" => 1,
                    "price" => 466.33
                )
            ),
            "subtotal" => 556.29,
            "total" => 676.61,
            "discount" => 0,
            "shipping" => array(
                "type" => 2,
                "price" => 120.32
            ),
            "coupon" => array(
                "code" => "",
                "value" => 0
            ),
            "observation" => "Pedido de teste",
            "order_type" => "pedido",
            "origin" => array(
                "source" => "",
                "medium" => "",
                "content" => ""
            ),
            "payments" => array(
                array(
                    "id" => 3,
                    "amount" => 676.61,
                    "instalment" => 1,
                    "instalment_value" => 676.61,
                    "billet_deadline" => date('Y-m-d')
                )
            ),
            "delivery_address" => array(
                "identification" => "Meu Endereço",
                "first_name" => "TI - Teste",
                "last_name" => "e Nome dos Grandes",
                "address" => "R Ortílio G Primo",
                "number" => "123",
                "complement" => "",
                "neighborhood" => "Ipê",
                "city" => "Curitiba",
                "state" => "PR",
                "zip" => "83055100",
                "country" => "Brasil",
                "telephone_a" => "(41) 3333-3333",
                "telephone_b" => "(41) 9999-9999",
            ),
            "billing_address" => array(
                "first_name" => "TI - Teste",
                "last_name" => "e Nome dos Grandes",
                "address" => "R Ortílio G Primo",
                "number" => "123",
                "complement" => "",
                "neighborhood" => "Ipê",
                "city" => "Curitiba",
                "state" => "PR",
                "zip" => "83055100",
                "country" => "Brasil",
                "telephone_a" => "(41) 3333-3333",
                "telephone_b" => "(41) 9999-9999"
            )
        );

        $client->request(
            'POST',
            '/v1/orders',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());
        return $client->getResponse()->getContent();
    }

    /**
     * @depends testCreateOrder
     */
    public function testGetOrder($orderId)
    {
        $client = $this->getOrder($orderId, true);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains('Content-Type', 'application/json; charset=UTF-8')
        );
        return $orderId;
    }

    private function getOrder($orderId, $returnClient = false)
    {
        $client = static::createClient();
        $client->request('GET', '/v1/orders/' . $orderId);

        if ($returnClient) {
            return $client;
        }
        if ($client->getResponse()->getStatusCode() == 200) {
            return json_decode($client->getResponse()->getContent());
        }
    }

    /**
     * @depends testCreateOrder
     */
    public function testApproveOrder($orderId)
    {
        $client = static::createClient();
        //update status data
        $data = array(
            "status" => 3,
            "internal_status" => 0,
            "clerk" => "teste"
        );
        //update status
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/status',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'check approve order result');
        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(3, $order->ped_situacao, 'Order status approved');
        //check internal status
        $this->assertEquals(0, $order->ped_staint, 'Check order internal status');
        //check status date
        $this->assertEquals(
            date('Y-m-d'),
            date_create($order->realizada_aprovacao->date)->format('Y-m-d'),
            'Order status approved date'
        );
        //check product dates
        foreach ($order->produtos as $product) {
            //check approval date
            $this->assertEquals(
                date('Y-m-d'),
                $product->data_apr_r,
                'Check product ' . $product->id_prdprd . ' approval date'
            );
            //check predict dates
            $this->assertNotNull($product->data_apr, 'Check product ' . $product->id_prdprd . ' predict approval date');
            $this->assertNotNull($product->data_lib, 'Check product ' . $product->id_prdprd . ' predict release date');
            $this->assertNotNull(
                $product->data_fat_forn,
                'Check product ' . $product->id_prdprd . ' predict billing date'
            );
            $this->assertNotNull(
                $product->data_entd_forn,
                'Check product ' . $product->id_prdprd . ' predict supplier invoice date'
            );
            $this->assertNotNull(
                $product->data_ent_forn,
                'Check product ' . $product->id_prdprd . ' predict supplier delivery date'
            );
            $this->assertNotNull($product->data_nf, 'Check product ' . $product->id_prdprd . ' predict invoice date');
            $this->assertNotNull($product->data_emb, 'Check product ' . $product->id_prdprd . ' predict shipping date');
            $this->assertNotNull($product->data_ent, 'Check product ' . $product->id_prdprd . ' predict delivery date');
        }

        return $orderId;
    }

    /**
     * @depends testApproveOrder
     */
    public function testCreatePurchaseOrder($orderId)
    {
        $client = static::createClient();
        //create purchase order date
        $data = array(
            "purchase_order_id" => 1234,
            "clerk" => "teste",
            "date" => date('Y-m-d'),
            "products" => array(
                array(
                    "id" => "64330"
                ),
                array(
                    "id" => "110457"
                )
            )
        );
        //create purchase order
        $client->request(
            'POST',
            '/v1/orders/' . $orderId . '/purchaseOrder',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'check create purchase order result');
        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(4, $order->ped_situacao, 'Check order status released');
        //check internal status
        $this->assertEquals(0, $order->ped_staint, 'Check order internal status');
        //check status date
        $this->assertEquals(
            date('Y-m-d'),
            date_create($order->realizada_compra->date)->format('Y-m-d'),
            'Order status released date'
        );
        //check product dates
        foreach ($order->produtos as $product) {
            //check approval date
            $this->assertEquals(
                date('Y-m-d'),
                $product->data_lib_r,
                'Check product ' . $product->id_prdprd . ' released date'
            );
            //check purchaseOrder id
            $this->assertEquals(
                1234,
                $product->ordem_compra,
                'Check product ' . $product->id_prdprd . ' purchase order'
            );
            //check purchaseOrder date
            $this->assertEquals(
                date('Y-m-d'),
                date_create($product->data_ordem_compra->date)->format('Y-m-d'),
                'Check product ' . $product->id_prdprd . ' purchase order date'
            );
        }

        return $orderId;
    }

    /**
     * @depends testCreatePurchaseOrder
     */
    public function testSupplierBillingDate($orderId)
    {
        $client = static::createClient();
        //create update date request
        $data = array(
            "clerk" => "teste",
            "date_types" => array(
                array(
                    "type" => "supplierBillingDate",
                    "date" => date('Y-m-d')
                )
            ),
            "products" => array(
                array(
                    "id" => 64330
                ),
                array(
                    "id" => 110457
                )
            )
        );

        //create purchase order
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/products/dates',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'check supplier billing update date result');
        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(4, $order->ped_situacao, 'Check order status released');
        //check internal status
        $this->assertEquals(159, $order->ped_staint, 'Check order internal status supplier billing');
        //check status date
        $this->assertEquals(
            date('Y-m-d'),
            date_create($order->realizado_faturamento_industria->date)->format('Y-m-d'),
            'Order status supplier billing date'
        );
        //check product dates
        foreach ($order->produtos as $product) {
            //check date
            $this->assertEquals(
                date('Y-m-d'),
                $product->data_fat_forn_r,
                'Check product ' . $product->id_prdprd . ' supplier billing date'
            );
        }
        return $orderId;
    }

    /**
     * @depends testSupplierBillingDate
     */
    public function testSupplierInvoiceDate($orderId)
    {
        $client = static::createClient();
        //create update date request
        $data = array(
            "clerk" => "teste",
            "date_types" => array(
                array(
                    "type" => "supplierInvoiceDate",
                    "date" => date('Y-m-d')
                )
            ),
            "products" => array(
                array(
                    "id" => 64330
                ),
                array(
                    "id" => 110457
                )
            )
        );

        //create purchase order
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/products/dates',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'check supplier invoice update date result');
        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(4, $order->ped_situacao, 'Check order status released');
        //check internal status
        $this->assertEquals(86, $order->ped_staint, 'Check order internal status supplier invoice');
        //check status date
        $this->assertEquals(
            date('Y-m-d'),
            date_create($order->realizado_faturamento_industria->date)->format('Y-m-d'),
            'Order status supplier invoice date'
        );
        //check product dates
        foreach ($order->produtos as $product) {
            //check date
            $this->assertEquals(
                date('Y-m-d'),
                $product->data_entd_forn_r,
                'Check product ' . $product->id_prdprd . ' supplier invoice date'
            );
        }

        return $orderId;
    }

    /**
     * @depends testSupplierInvoiceDate
     */
    public function testSupplierDeliveryDate($orderId)
    {
        $client = static::createClient();
        //create update date request
        $data = array(
            "clerk" => "teste",
            "date_types" => array(
                array(
                    "type" => "supplierDeliveryDate",
                    "date" => date('Y-m-d')
                )
            ),
            "products" => array(
                array(
                    "id" => 64330
                ),
                array(
                    "id" => 110457
                )
            )
        );

        //create purchase order
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/products/dates',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'check supplier delivery update date result');
        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(4, $order->ped_situacao, 'Check order status released');
        //check internal status
        $this->assertEquals(158, $order->ped_staint, 'Check order internal status supplier delivery');
        //check status date
        $this->assertEquals(
            date('Y-m-d'),
            date_create($order->realizado_faturamento_industria->date)->format('Y-m-d'),
            'Order status supplier delivery date'
        );
        //check product dates
        foreach ($order->produtos as $product) {
            //check date
            $this->assertEquals(
                date('Y-m-d'),
                $product->data_ent_forn_r,
                'Check product ' . $product->id_prdprd . ' supplier delivery date'
            );
        }

        return $orderId;
    }

    /**
     * @depends testSupplierDeliveryDate
     */
    public function testCreateInvoice($orderId)
    {
        $client = static::createClient();
        //create purchase order date
        $data = array(
            "invoice_id" => 443214,
            "branch_id" => "1",
            "created_date" => date('Y-m-d'),
            "clerk" => "teste",
            "itens" => array(
                array(
                    "product_reference" => 64330,
                    "qty" => 1
                ),
                array(
                    "product_reference" => 110457,
                    "qty" => 1
                )
            )
        );

        //create purchase order
        $client->request(
            'POST',
            '/v1/orders/' . $orderId . '/invoices',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'check create invoice result');
        //get order
        $invoices = $this->getInvoice($orderId);
        //check id
        $this->assertEquals(443214, $invoices[0]->id_notafiscal, 'check invoice number');
        //check create date
        $this->assertEquals(
            date('Y-m-d'),
            date_create($invoices[0]->data_criacao)->format('Y-m-d'),
            'check invoice date'
        );
        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(4, $order->ped_situacao, 'Check order status released');
        //check internal status
        $this->assertEquals(72, $order->ped_staint, 'Check order internal status invoice');
        //check status date
        $this->assertEquals(
            date('Y-m-d'),
            date_create($order->realizado_nf->date)->format('Y-m-d'),
            'Order status invoice date'
        );
        //check product dates
        foreach ($order->produtos as $product) {
            //check invoice number
            $this->assertEquals(443214, $product->id_notafiscal, 'check invoice number');
            //check date
            $this->assertEquals(
                date('Y-m-d'),
                $product->data_nf_r,
                'Check product ' . $product->id_prdprd . ' invoice date'
            );
        }

        return $orderId;
    }

    private function getInvoice($orderId, $returnClient = false)
    {
        $client = static::createClient();
        $client->request('GET', '/v1/orders/' . $orderId . '/invoices');

        if ($returnClient) {
            return $client;
        }
        if ($client->getResponse()->getStatusCode() == 200) {
            return json_decode($client->getResponse()->getContent());
        }
    }

    /**
     * @depends testCreateInvoice
     */
    public function testShippingDate($orderId)
    {
        $client = static::createClient();
        // partial shipping
        $data = array(
            "clerk" => "teste",
            "date_types" => array(
                array(
                    "type" => "shippingDate",
                    "date" => date('Y-m-d')
                )
            ),
            "products" => array(
                array(
                    "id" => 64330
                )
            )
        );

        //create purchase order
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/products/dates',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'check partial shipping update date result');
        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(5, $order->ped_situacao, 'Check order status shipping');
        //check internal status
        $this->assertEquals(77, $order->ped_staint, 'Check order internal status partial shipping');
        //check status date
        $this->assertNull($order->realizado_embarque, 'Order status partial shipping date');
        //check product dates
        foreach ($order->produtos as $product) {
            if ($product->id_prdprd == 64330) {
                //check date
                $this->assertEquals(
                    date('Y-m-d'),
                    $product->data_emb_r,
                    'Check product ' . $product->id_prdprd . ' shipping date'
                );
            }
        }

        // full shipping
        $data = array(
            "clerk" => "teste",
            "date_types" => array(
                array(
                    "type" => "shippingDate",
                    "date" => date('Y-m-d')
                )
            ),
            "products" => array(
                array(
                    "id" => 110457
                )
            )
        );

        //create purchase order
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/products/dates',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'check partial shipping update date result');
        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(5, $order->ped_situacao, 'Check order status shipping');
        //check internal status
        $this->assertEquals(0, $order->ped_staint, 'Check order internal status shipping');
        //check status date
        $this->assertEquals(
            date('Y-m-d'),
            date_create($order->realizado_embarque->date)->format('Y-m-d'),
            'Order status shipping date'
        );
        //check product dates
        foreach ($order->produtos as $product) {
            //check date
            $this->assertEquals(
                date('Y-m-d'),
                $product->data_emb_r,
                'Check product ' . $product->id_prdprd . ' shipping date'
            );
        }

        return $orderId;
    }

    /**
     * @depends testShippingDate
     */
    public function testDeliveryDate($orderId)
    {
        $client = static::createClient();
        //partial delivery
        $data = array(
            "clerk" => "teste",
            "date_types" => array(
                array(
                    "type" => "deliveryDate",
                    "date" => date('Y-m-d')
                )
            ),
            "products" => array(
                array(
                    "id" => 64330
                )
            )
        );

        //create purchase order
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/products/dates',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(6, $order->ped_situacao, 'Check order status delivery');
        //check internal status
        $this->assertEquals(78, $order->ped_staint, 'Check order internal status partial delivery');
        //check status date
        $this->assertNull($order->realizada_entrega, 'Order status partial delivery date');
        //check product dates
        foreach ($order->produtos as $product) {
            if ($product->id_prdprd == 64330) {
                //check date
                $this->assertEquals(
                    date('Y-m-d'),
                    $product->data_ent_r,
                    'Check product ' . $product->id_prdprd . ' delivery date'
                );
            }
        }

        //full delivery
        $data = array(
            "clerk" => "teste",
            "date_types" => array(
                array(
                    "type" => "deliveryDate",
                    "date" => date('Y-m-d')
                )
            ),
            "products" => array(
                array(
                    "id" => 110457
                )
            )
        );

        //create purchase order
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/products/dates',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(6, $order->ped_situacao, 'Check order status delivery');
        //check internal status
        $this->assertEquals(0, $order->ped_staint, 'Check order internal status delivery');
        //check status date
        $this->assertEquals(
            date('Y-m-d'),
            date_create($order->realizada_entrega->date)->format('Y-m-d'),
            'Order status shipping date'
        );
        //check product dates
        foreach ($order->produtos as $product) {
            $this->assertEquals(
                date('Y-m-d'),
                $product->data_ent_r,
                'Check product ' . $product->id_prdprd . ' delivery date'
            );
        }

        return $orderId;
    }

    /**
     * @depends testDeliveryDate
     */
    public function testCancelItem($orderId)
    {
        $client = static::createClient();

        //update status data
        $data = array(
            "cancelled" => true,
            "clerk" => "teste",
            "ticket" => "#1433423"
        );
        //update status
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/products/64330/status',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());

        //get order
        $order = $this->getOrder($orderId);
        //check product status
        foreach ($order->produtos as $product) {
            if ($product->id_prdprd == '64330') {
                $this->assertEquals(true, $product->item_cancelado);
            }
        }

        return $orderId;
    }

    /**
     * @depends testCancelItem
     */
    public function testUpdateReversalInfos($orderId)
    {
        $client = static::createClient();

        //update status data
        $data = array(
            'clerk' => 'teste',
            'typeReversal' => 1,
            'operator' => '12312312',
            'transaction' => 'Visa'
        );
        //update status
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/products/64330/status',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());

        //get order
        $order = $this->getOrder($orderId);
        //check product status
        foreach ($order->produtos as $product) {
            if ($product->id_prdprd == '64330') {
                $this->assertEquals(false, $product->item_cancelado);
            }
        }

        return $orderId;
    }

    /**
     * @depends testUpdateReversalInfos
     */
    public function testRevokeCancelItem($orderId)
    {
        $client = static::createClient();

        //update status data
        $data = array(
            "cancelled" => false,
            "clerk" => "teste"
        );
        //update status
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/products/64330/status',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        //get order
        $order = $this->getOrder($orderId);
        //check product status
        foreach ($order->produtos as $product) {
            if ($product->id_prdprd == '64330') {
                $this->assertEquals(false, $product->item_cancelado);
            }
        }

        return $orderId;
    }

    /**
     * @depends testRevokeCancelItem
     */
    public function testCancelOrder($orderId)
    {
        $client = static::createClient();
        //update status data
        $data = array(
            "status" => 7,
            "internal_status" => 0,
            "clerk" => "teste"
        );
        //update status
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/status',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //get order
        $order = $this->getOrder($orderId);
        //check status
        $this->assertEquals(7, $order->ped_situacao);
        //check product status
        foreach ($order->produtos as $product) {
            $this->assertEquals(true, $product->item_cancelado);
        }

        return $orderId;
    }

    /**
     * @depends testApproveOrder
     */
    public function testUpdateNewDeliveryDate($orderId)
    {
        $client = static::createClient();

        $newDeliveryDate = date('Y-m-d', strtotime("+20 days"));
        //create new delivery date
        $data = array(
            "new_delivery_date" => $newDeliveryDate,
            "accept" => true
        );
        //create purchase order
        $client->request(
            'PUT',
            '/v1/orders/' . $orderId . '/dates/delivery',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'check update new delivery date result');
        //get order
        $order = $this->getOrder($orderId);
        //check new delivery date
        $this->assertEquals(
            $newDeliveryDate,
            date_create($order->ped_nova_data_entrega->date)->format('Y-m-d'),
            'New delivery date'
        );

        return $orderId;
    }

    /**
     * @depends testCreateOrder
     */
    public function testCreateOrderLog($orderId)
    {
        $client = static::createClient();

        //create new delivery date
        $data = array(
            'title' => 'Teste de Inserção de logs',
            'text' => 'Inserindo log de teste...',
            'show' => false,
            'status' => 2,
            'internal_status' => 0
        );
        //create purchase order
        $client->request(
            'POST',
            '/v1/orders/' . $orderId . '/logs',
            array(),
            array(),
            array(),
            json_encode($data)
        );
        //check response code
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'check insert order log result');

        //get order logs
        $orderLogs = $this->getOrderLogs($orderId);
        $orderLog = $orderLogs->data[0];

        //check log order id
        $this->assertEquals($orderId, $orderLog->id_pedped, 'Check order id');
        //check log title
        $this->assertEquals('Teste de Inserção de logs', $orderLog->titulo, 'Check title');
        //check log text
        $this->assertEquals('Inserindo log de teste...', $orderLog->texto, 'Check text');
        //check show log to customer
        $this->assertEquals(false, $orderLog->mostrar_areavip, 'Check show to customer');
        //check status
        $this->assertEquals(2, $orderLog->status_atual, 'Check status');
        //check internal status
        $this->assertEquals(0, $orderLog->status_internos_atual, 'Check internal status');

        return $orderId;
    }

    /**
     * @param $orderId
     * @return bool|mixed
     */
    private function getOrderLogs($orderId)
    {
        $client = static::createClient();
        $client->request('GET', '/v1/orders/' . $orderId . '/logs');

        if ($client->getResponse()->getStatusCode() == 200) {
            return json_decode($client->getResponse()->getContent());
        }

        return false;
    }
}
